<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    protected $appends = [
        'firstPhoto',
    ];

    function getfirstPhotoAttribute()
    {        
        return $this->photos->first();	  
    }
	
	protected $fillable = [
	    'uuid',
	    'user_id',
            'title', 
		'description',
		//'votes',
		//'amount',
        ];
        
    public function properties()
    {
        return $this->hasMany('App\Property', 'group_id');
    }    
    
    public function photos()
    {
        return $this->morphMany('App\Image', 'photoable');
    }
     public function photosFirst()
    {
        return $this->morphMany('App\Image', 'photoable');
    }
}
