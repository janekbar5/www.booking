<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SeasonGroup extends Model
{
    protected $fillable = [
	    'user_id', 
        'title', 
		'description',
		//'votes',
		//'amount',
    ];
	
	
	/////////////////////////////////////////////////////////////////HASMANY
    public function seasons()
    {
        return $this->hasMany('App\Season', 'seasongroup_id');		
    }
    
    public function seasons2($start_date,$end_date)
    {
        return $this->belongsTo('App\SeasonGroup', 'seasongroup_id')
                ->where('start_date', '<', $end_date)
                ->where('end_date', '>=', $start_date)
                ->get();
    }
}
