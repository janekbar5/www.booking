<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    
   

    protected $fillable = [
        'user_id',
        'first_name_t',
        'last_name_t',
        'phone_t',
        'email_t',
       
    ];
    
     
    
    protected $appends = [
    'firstPhoto','text',    
	'text','address_line1','address_line2','city','postcode','lat','lng'	
	];  
	 public function getAddressline1Attribute()
	{        
		return $this->address->address_line1;
    }
	public function getAddressline2Attribute()
	{        
		return $this->address->address_line2;
    }
	public function getCityAttribute()
	{        
		return $this->address->city;
    }
	public function getPostcodeAttribute()
	{        
		return $this->address->postcode;
    }
	public function getLatAttribute()
	{        
		return $this->address->lat;
    }
	public function getLngAttribute()
	{        
		return $this->address->lng;
    } 
	///   
    function getfirstPhotoAttribute()
    {        
        return $this->photos->first();	  
    }
    public function getTextAttribute()
    {
        return $this->attributes['first_name_t']. ' - '.$this->attributes['last_name_t'];
    }
    
    
    
    
    /////////////////////////////////////////////////////////////Address
    public function address()
    {
        return $this->morphOne('App\Address', 'addressable');
    }
    /////////////////////////////////////////////////////////////////HASMANY
    public function bookings()
    {
        return $this->hasMany('App\Booking', 'customer_id');
    }
    /////////////////////////////////////////////////////////////////BELONGSTO
    // User-->hasMany-->Property   <==>  Property-->belongsTo-->User 
    public function user()
    {
        return $this->belongsTo('App\User');
    }
    ///////////////////////////////////////////////////////////////Morph
    public function photos()
    {
        return $this->morphMany('App\Image', 'photoable');
    }

}
