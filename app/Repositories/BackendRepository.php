<?php

namespace App\Repositories;

use App\{Property,Season,SeasonGroup,Group,Booking,Inventory,Advantage,BookingStatus,BookingPayment,User,Customer,Address};
use Spatie\Permission\Models\Role;

use App\Repositories\Interfaces\BackendRepositoryInterface;

class BackendRepository implements BackendRepositoryInterface
{
     
    ////////////////////////////////////////////////////////////////////////////////FILTERS
  public function ownFilterLoop($get,$var){
        $count = 0;
      foreach($get as $key => $value){
      $count++;
        if($count > 4){ //skipping first 4        
          if(strpos($key, '_t') || strpos($key, '_n') ){
           $var = $var->where($key,'LIKE', '%'.$value.'%');       
          }       
        }
      } 
    }


     /////////////////////////////////////////////////////////////////////////////////Address    
     public function getUserAddressById($type,$id){         
        return Address::where('addressable_id', '=', $id) 
                    ->where('addressable_type', '=', $type)                  
                    ->first();
      }
    /////////////////////////////////////////////////////////////////////////////////CUSTOMERS
    
    public function getUserCustomerById($id){         
        return Customer::where('id', '=', $id) 
                    ->with('photos')                  
                    ->first();
      }
    /////////////////////////////////////////////////////////////////////////////////ADMIN USERS
    public function getAdminUsers(){
        return User::
                  with('roles')
                  ->with('photos')
                  ->orderBy('created_at', 'desc')
                  ->paginate(10); 
    }
    public function getAdminUsersById($id){         
        return User::where('id', '=', $id) 
                    ->with('photos')                  
                    ->first();
      }
     /////////////////////////////////////////////////////////////////////////////////ADMIN ROLES
     public function getAdminRoles(){
        return Role::  
                  orderBy('created_at', 'desc')
                  ->with('permissions')
                  ->paginate(10); 
    }
    public function getAdminRolesById($id){         
        return Role::where('id', '=', $id)                                    
                    ->first();
      }
   
    /////////////////////////////////////////////////////////////////////////////////SEASONS 
   public function getSeasonByGroupId($id){         
      return Season::where('seasongroup_id', '=', $id)->get();
   }
    public function getUserSeasonById($id){         
      return Season::where('id', '=', $id)                   
                    ->first();
    }
    /////////////////////////////////////////////////////////////////////////////////SEASONS GROUPS
   
    public function getUserSeasonGroups($user_id){
       return SeasonGroup::where('user_id', '=', $user_id)->paginate(10);  
    }
    public function getSeasonGroupById($id){
    return SeasonGroup::where('id', '=', $id)->first();
    }
    
    ////////////////////////////////////////////////////////////////////////////////GROUPS
    public function getUserGroups($id){
        return Group::where('user_id', '=', $id)
                  ->with('photosFirst')
                  ->orderBy('created_at', 'desc')
                  ->paginate(10);
                  //->get();
    }
    public function getUserGroupById($id){
        return Group::where('id', '=', $id)
                 ->with('photos')
                 ->first();
    }    
    
    /////////////////////////////////////////////////////////////////////////////////PROPERTIES
    public function getUserProperties($id){   
        return Property::where('user_id', '=', $id)
                //->with('photosFirst')
                ->with('groups')
                ->with('seasongroups')
                ->with('inventories')
                ->with('advantages')               
                ->paginate(10);        
    }
    public function getUserPropertyById($id){  
        return Property::where('id', '=', $id)
                    ->with('photos')         
                    ->with('inventories')
                    ->with('advantages')
                    ->first();
    }
    public function getUserPropertyByGroupId($id){  
        return Group::where('id', '=', $id)
                    ->with('properties')
                    ->first();
    }
    //////////////////////////////////////////////////////////////////////////////////BOOKINGS
    public function getUserBookingById($id){        
        return Booking::where('id', '=', $id)
                    ->with('customer')         
                    ->with('property')
                    ->with('status')
                    ->with('payment')                    
                    ->first();
     }
     public function getUserBookings($id){
        return Booking::where('user_id', '=', $id)
                      ->with('customer')
                      ->with('property')
                      ->with('status')
                      ->with('payment'); 
                      //->orderBy('created_at', 'desc')
                      //->paginate(10);                     
    }
    //////////////////////////////////////////////////////////////////////////////////INVENTORIES
    public function getUserInventoriesById($id){        
        return Inventory::where('id', '=', $id)
                    //->with('customer')         
                    //->with('property')
                    //statuses
                    //payments
                    ->first();
     }
     public function getUserInventories($id){
        return Inventory::where('user_id', '=', $id)                      
                      ->orderBy('created_at', 'desc')
                      ->paginate(10);
                      //->get();
        }

    //////////////////////////////////////////////////////////////////////////////////ADVANTAGES

    //////////////////////////////////////////////////////////////////////////////////BOOKING STATUSES
    public function getUserBookingStatuses($id){
        return BookingStatus::where('user_id', '=', $id)                 
                  ->orderBy('created_at', 'desc')
                  ->paginate(10);
                  //->get();
    }
    public function getUserBookingStatusById($id){
        return BookingStatus::where('id', '=', $id)                 
                 ->first();
    }
    //////////////////////////////////////////////////////////////////////////////////BOOKING PAYMENTS
    public function getUserBookingPayments($id){
        return BookingPayment::where('user_id', '=', $id)                 
                  ->orderBy('created_at', 'desc')
                  ->paginate(10);
                  //->get();
    }
    
    
}
