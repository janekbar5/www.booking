<?php

namespace App\Repositories\Interfaces;

//use App\User;

interface BackendRepositoryInterface
{
    ///////////////////////////////////////////////////SeasonGroups
    //public function getAllSeasonGroups();
    public function getSeasonGroupById($id);
    public function getUserSeasonGroups($user_id);
    ///////////////////////////////////////////////////Seasons
   
   ////////////////////////////////////////////////////Groups
    public function getUserGroups($id);
    public function getUserGroupById($id);

    ///////////////////////////////////////////////////Properties
    public function getUserProperties($id);
    public function getUserPropertyById($id);
    //public function all();    
    //public function getUserById($id);
     ///////////////////////////////////////////////////Bookings
    public function getUserBookingById($id);
}
