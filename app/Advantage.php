<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Advantage extends Model
{
    protected $fillable = [
	    'user_id',
        'title', 
		'description',	
		
    ];
	// Property-->belongsToMany-->Advantages   <==>  Advantages-->belongsToMany-->Property
	public function properties()    {
        
		return $this->belongsToMany(Property::class, 'property_advantage', 'property_id', 'advantage_id');
	}
	
}
