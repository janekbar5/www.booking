<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

use Spatie\Permission\Traits\HasRoles;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable implements MustVerifyEmail 
{
    use Notifiable;
    use HasRoles;
    
    protected $appends = [
        'firstPhoto',
    ];

    function getfirstPhotoAttribute()
    {        
        return $this->photos->first();	  
    }
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name','email','password','email_verified_at'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'properties' => 'array'
    ];
    /////////////////////////////////////////////////////////////////HASMANY
    // User-->hasMany-->Booking   <==>  Booking-->belongsTo-->hasMany
    public function booking()
    {
        return $this->hasMany('App\Booking', 'user_id');
    }
    // User-->hasMany-->Customers   <==>  Customers-->belongsTo-->User
    public function customer()
    {
        return $this->hasMany('App\Customer', 'user_id');
    }
    public function photos()
    {
        return $this->morphMany('App\Image', 'photoable');
    }
    /////////////////////////////////////////////////////////////Images
    // public function photosFirst()
    // {  
    //     return $this->title;       
    // }
}
