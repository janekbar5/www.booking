<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Inventory extends Model
{
    
    public function setTranslationsAttribute($value)
    {
        $translations = [];
        foreach ($value as $array_item) {
            if (!is_null($array_item['key'])) {
                $translations[] = $array_item;
            }
        }
        $this->attributes['translations'] = json_encode($translations);
    }
    
    protected $fillable = [
        'user_id',
        'title',
        //'translations',
        'description',
    ];

    // Property-->belongsToMany-->Inventory   <==>  Inventory-->belongsToMany-->Property
    public function properties()
    {
        return $this->belongsToMany(Property::class, 'property_inventory', 'property_id', 'inventory_id');
    }

    protected $casts = [       
        'translations' => 'array'
    ];
}
