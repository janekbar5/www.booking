<?php
namespace App\Http\Requests;
use Illuminate\Foundation\Http\FormRequest;
class UserStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'time_to' => 'required',
            'time_from' => 'required',
           
            'first_name' => 'required',
            'last_name' => 'required'
        ];
    }
     /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages()
    {
        return [
            'time_to.required' => 'time_to is required!',
            'time_from.required' => 'time_from is required!',
            'first_name.required' => 'first_name is required!',
            'last_name.required' => 'last_name is required!'
        ];
    }
}