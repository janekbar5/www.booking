<?php

namespace App\Http\Controllers;

use App\BookingStatus;
use Illuminate\Http\Request;
use App\Repositories\ValidationRepository;
use App\Repositories\Interfaces\BackendRepositoryInterface;

class BookingStatusController extends Controller
{
    function __construct(ValidationRepository $vr, BackendRepositoryInterface $br)
    {         
        $this->br = $br;
        $this->vr = $vr;
    }
    /**/////////////////////////////////////////////////////////////////////////////////////////////1 INDEX
    public function index()
    {       
        $bookingStatuses = $this->br->getUserBookingStatuses(\Auth::user()->id);       
        return response()
            ->json(['results' => $bookingStatuses]);     
    }
    /**/////////////////////////////////////////////////////////////////////////////////////////////2 EDIT
    public function edit($id)
    {        
        $bookingStatus = $this->br->getUserBookingStatusById($id);               
        return response()->json([
            'form' => $bookingStatus,           
            ]);         
    }
    /**/////////////////////////////////////////////////////////////////////////////////////////////3 CREATE
   public function create()
   {      
      return response()->json([
          'form' => '',          
          ]);
   }
    /**/////////////////////////////////////////////////////////////////////////////////////////////4 CREATE POST
    public function store(Request $request)
    {        
        $fv = $this->validate($request, $this->vr->bookingStatusUpdate());        
        $bookingStatus = BookingStatus::create(array_merge($request->all(), [ 'user_id' => \Auth::user()->id ]));      
        return ['created' => 'true','id' => $bookingStatus->id];
    }
    /**/////////////////////////////////////////////////////////////////////////////////////////////5 UPDATE POST
    public function update(Request $request, $id)
    {
        //dd($request->all());
        $bookingStatus = $this->br->getUserBookingStatusById($id);
        $fv = $this->validate($request, $this->vr->bookingStatusUpdate());       
        $bookingStatus->update($request->all());      
        return ['saved' => 'true','id' => $bookingStatus->id];        
    }
     /**/////////////////////////////////////////////////////////////////////////////////////////////6 DESTROY   
     public function destroy($id)
     {
         $bookingStatus = $this->br->getUserBookingStatusById($id);
         //$this->im->destroyAllImages($type='App\Group',$id);
         $bookingStatus->delete();
         return response()
             ->json(['deleted' => true]);
     }
  
}
