<?php

namespace App\Http\Controllers;

use App\{Customer,Address};
use Illuminate\Http\Request;
use App\Repositories\ValidationRepository;
use App\Repositories\Interfaces\BackendRepositoryInterface;

class CustomerController extends Controller
{

    function __construct(ValidationRepository $vr, BackendRepositoryInterface $br, ImageController $im)
    {        
        $this->middleware('permission:customers-list');
        $this->middleware('permission:customers-create', ['only' => ['create', 'store']]);
        $this->middleware('permission:customers-edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:customers-delete', ['only' => ['destroy']]);        
        $this->br = $br;
        $this->vr = $vr;
        $this->im = $im;
    }   
    /**/////////////////////////////////////////////////////////////////////////////////////////////1 INDEX    
    public function index(){ 
        $customers = Customer::with('photos');       
        ////////////////////////////////////////////////////////////Own Filter Loop 
        $this->br->ownFilterLoop($_GET,$customers);       
        ////////////////////////////////////////////////////////////Own Order ALWAYS EXIST
        $customers = $customers->orderBy(request('field'),request('order'));        
        /////////////////////////////////////////////////////////////Foreign Order
       /* if(request('field')=='reccurence_id_n'){
            $clients = $clients
            ->join('reccurences','reccurences.id','=','clients.reccurence_id_n')->select('reccurences.title as regionName','clients.*')
            ->orderBy('regionName',request('order'));
        }   */     
        $customers = $customers->paginate(request('perPage'));  
        return response()->json(['rows' => $customers]);      
    }
    /**/////////////////////////////////////////////////////////////////////////////////////////////2 EDIT
    public function edit($id)
    {        
        //$customer = Customer::findOrFail($id)->with('address')->first();  
        //$customer = Customer::where('id','=',$id)->first();  
        $customer = $this->br->getUserCustomerById($id);    
        return response()->json([
            'form' => $customer,            
            ]);
    }
     /**/////////////////////////////////////////////////////////////////////////////////////////////3 CREATE
     public function create()
     {        
        // $allinventories = Inventory::all();
        // $alladvantages = Advantage::all();
        // $allgroups = $this->br->getUserGroups(\Auth::user()->id);
        // $seasonGroups = $this->br->getUserSeasonGroups(\Auth::user()->id);        
        return response()->json([
            'form' => '',            
            ]);
     }
     /**/////////////////////////////////////////////////////////////////////////////////////////////4 CREATE POST
    public function store(Request $request)
    {        
        //dd($request->all());
        $fv = $this->validate($request, $this->vr->customerUpdate());  
        $customer = Customer::create(array_merge($request->all(), ['user_id' => \Auth::user()->id]));        
        $address =   Address::create(array_merge($request->all(), ['addressable_type' => 'App\Customer','addressable_id' => $customer->id,'user_id' => \Auth::user()->id]));
       
        return ['created' => 'true','id' => $customer->id];
    }
    
     /**/////////////////////////////////////////////////////////////////////////////////////////////5 UPDATE POST
     public function update(Request $request, $id)
     {
        //dd( $request->all()['address'] );
         $customer = $this->br->getUserCustomerById($id);        
         $fv = $this->validate($request, $this->vr->customerUpdate());  
         $customer->update($request->all());  
         $address = $this->br->getUserAddressById($type='App\Customer',$id);
         //dd($address);
         //$address->update($request->all()['address']);  
         $address->update($request->all()); 
         return ['saved' => 'true','id' => $customer->id];        
     }
     /**/////////////////////////////////////////////////////////////////////////////////////////////6 DESTROY   
   public function destroy($id)
   {
       $client = $this->br->getUserCustomerById($id);    
       $client->delete();
       return response()
           ->json(['deleted' =>'true']);
   }
    /**/////////////////////////////////////////////////////////////////////////////////////////////
    public function search()
    {
        $results = Customer::orderBy('first_name_t')->with('address')
            ->when(request('q'), function($query) {
                $query->where('first_name_t', 'like', '%'.request('q').'%')
                    ->orWhere('last_name_t', 'like', '%'.request('q').'%')
                    //->orWhere('email', 'like', '%'.request('q').'%')
                    ;
            })
            //->limit(6)
            ->get();

        return response()
            ->json(['results' => $results]);
    }


}
