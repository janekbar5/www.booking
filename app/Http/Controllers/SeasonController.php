<?php

namespace App\Http\Controllers;

use App\Season;
use Illuminate\Http\Request;
use App\Repositories\ValidationRepository;
use App\Repositories\Interfaces\BackendRepositoryInterface;
use Response;

class SeasonController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct(BackendRepositoryInterface $br, ValidationRepository $vr)
    {
         $this->middleware('permission:property-list');
         //$this->middleware('permission:property-create', ['only' => ['create','store']]);
         //$this->middleware('permission:property-edit', ['only' => ['edit','update']]);
         //$this->middleware('permission:property-delete', ['only' => ['destroy']]);
         $this->br = $br;
         $this->vr = $vr;
    }
     /**/////////////////////////////////////////////////////////////////////////////////////////////1 INDEX
     public function getSeasonByGroupId($id)
     {       
         $querry = $this->br->getSeasonByGroupId($id);  
         $events = array();
         $resources = array();

        foreach ($querry as $season) {
            $r = new \stdClass();
            $r->id = $season['id'];
            $r->resource = $season['id'];
            $r->start = $season['start_date'];
            $r->end = $season['end_date'];            
            $r->text = $season['title'];
            $r->price = $season['price'];
            $r->colour = $season['colour'];               
            $events[] = $r;
        }
        foreach ($querry as $season) {
            $r = new \stdClass();
            $r->name = $season['title'];
            $r->id = $season['id'];
            $resources[] = $r;
        }
        //header('Content-Type: application/json');
        //return $seasons;
        return response()->json(['events' => $events,'resources' => $resources]);
     }
     /**/////////////////////////////////////////////////////////////////////////////////////////////2 EDIT
     public function edit($id)
     {        
        $season = $this->br->getUserSeasonById($id);  
        return response()->json([
            'form' => $season,           
            ]);      
     }
     /**/////////////////////////////////////////////////////////////////////////////////////////////3 CREATE
     public function create()
     {        
        $allinventories = Inventory::all();
        $alladvantages = Advantage::all();
        $allgroups = $this->br->getUserGroups(\Auth::user()->id);
        $seasonGroups = $this->br->getUserSeasonGroups(\Auth::user()->id);        
        return response()->json([
            'form' => '',
            'allinventories' => $allinventories,
            'alladvantages' => $alladvantages,
            'allgroups' => $allgroups,
            'seasonGroups' => $seasonGroups,
            ]);
     }
     /**/////////////////////////////////////////////////////////////////////////////////////////////4 CREATE POST
    public function store(Request $request)
    {        
        //dd($request->all());
        $fv = $this->validate($request, $this->vr->propertyUpdate());        
        $property = Property::create(array_merge($request->all(), ['user_id' => \Auth::user()->id]));
        if($request->has('selectedInventories')){
            $property->inventories()->sync($request->input('selectedInventories'));
        }
        if($request->has('selectedAdvantages')){
            $property->advantages()->sync($request->input('selectedAdvantages'));
        }
        return ['created' => 'true','id' => $property->id];
    }
     /**/////////////////////////////////////////////////////////////////////////////////////////////5 UPDATE POST
     public function update(Request $request, $id)
     {
         //dd($request->all());         
         $season = $this->br->getUserSeasonById($id);
          
         $fv = $this->validate($request, $this->vr->seasonUpdate());       
         $season->update($request->all());        
         return ['success' => 'true','id' => $season->id];        
     }
     /**/////////////////////////////////////////////////////////////////////////////////////////////6 DESTROY   
 
     public function destroy($id)
     {
         $property = $this->br->getUserPropertyById($id);
         $this->im->destroyAllImages($type='App\Property',$id);
         $property->delete();
         return response()
             ->json(['deleted' => true]);
     }
      /**/////////////////////////////////////////////////////////////////////////////////////////////doMoveorResize
    public function doMoveorResize(Request $request)
    {
        //dd($request->all());
        try
        {
            $season = Season::where('id', '=', (int) $request->Input('resource'))->first();
            //dd($request->all());
            $season->start_date = $request->input('newStart');
            $season->end_date = $request->input('newEnd');
            $season->update();
            return Response::json(['success' => '1', 'object' => $request->input('dates')]);
        } catch (\Exception $e)
        {
            return Response::json(['errors' => "errrrr"]);
        }
    }


	
}
