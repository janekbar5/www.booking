<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
//use Spatie\Permission\Models\Role;
//use Spatie\Permission\Models\Permission;
use App\Property;
use App\Group;

class HelperController extends Controller
{

    public function newBookingsPost(Request $request)
    {        
       $fv = $this->validate($request, $this->vr->newBookings());   
            if ($request->input('customer_id') == '')
            { //new customer
                //dd($request->all());    
                $customer = Customer::create((array_merge($request->all(), ['user_id' => \Auth::user()->id])));
                Booking::create(array_merge($request->all(), ['customer_id' => $customer->id, 'price' => 11, 'uuid' => $request->input('group_id'), 'property_id' => $request->input('resource_id'), 'user_id' => \Auth::user()->id]));
            } else
            { //existing
                //dd($request->all());    
                Booking::create(array_merge($request->all(), ['price' => 11, 'uuid' => $request->input('group_id'), 'property_id' => $request->input('resource_id'), 'user_id' => \Auth::user()->id]));
            }
            return response()->json(['success' => 'Done!']);       
    }





    
    public function Groups()
    {
        return Group::where('user_id', '=', \Auth::user()->id)->get();
    }

    public function filterlist(Request $request)
    {

        $properties = Property::where('user_id', '=', \Auth::user()->id)
                ->where('group_id', '=', $request->Input('group_id'))
                ->paginate(20);
        $groups = $this->Groups();
        return view('backend.properties.index', compact('properties', 'groups'));
    }

    public function index()
    {
        $groups = Group::where('user_id', '=', \Auth::user()->id)->paginate(10);
        return view('backend.groups.index', compact('groups'));
    }

    public function duplicate(Request $request, $id)
    {
        //dd($request->all());
        $property_template = Property::find($id);
        $property = new Property();
        $property->title = 'Duplicated---' . $property_template->title;
        $property->user_id = $property_template->user_id;
        $property->seasongroup_id = $property_template->seasongroup_id;
        $property->group_id = $property_template->group_id;
        $property->amount = $property_template->amount;
        $property->description = $property_template->description;
        $property->area = $property_template->area;
        $property->room_nubr = $property_template->room_nubr;
        $property->max_pers = $property_template->max_pers;
        $property->save();
        $property->inventories()->sync($property_template->inventories);
        //Session::flash('alert-success', 'Record duplicated successfully');

        return redirect()->back()->with('success', 'Property successfully duplicated');
    }

}
