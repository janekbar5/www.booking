<?php

namespace App\Http\Controllers;

use App\SeasonGroup;
use App\Season;
use Illuminate\Http\Request;
use App\Repositories\ValidationRepository;
use App\Repositories\Interfaces\BackendRepositoryInterface;

class SeasonGroupController extends Controller
{
    protected $backendRepository;
    /**
     * //////////////////////////////////////////////
     * //////////////////////////////////////////////
     * //////////////////////////////////////////////
     * //////////////////////////////////////////////
     * //////////////////////////////////////////////
     */
    function __construct(ValidationRepository $vr, BackendRepositoryInterface $br)
    {
        $this->vr = $vr;
        $this->br = $br;
        $this->middleware('permission:seasongroups-list');
        $this->middleware('permission:seasongroups-create', ['only' => ['create', 'store']]);
        $this->middleware('permission:seasongroups-edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:seasongroups-delete', ['only' => ['destroy']]);
    }

    /**/////////////////////////////////////////////////////////////////////////////////////////////INDEX
    public function index()
    {       
        $seasongroups = $this->br->getUserSeasonGroups(\Auth::user()->id);       
        return response()->json(['results' => $seasongroups]);
    }
    /**/////////////////////////////////////////////////////////////////////////////////////////////EDIT
    public function edit($id)
    {        
        $seasongroup = $this->br->getSeasonGroupById($id);                  
        return response()->json([
            'form' => $seasongroup,           
            ]);         
    }
    /**/////////////////////////////////////////////////////////////////////////////////////////////CREATE
    public function create()
    {      
       return response()->json([
           'form' => '',          
           ]);
    }
   /**/////////////////////////////////////////////////////////////////////////////////////////////CREATE POST
   public function store(Request $request)
   {        
       $fv = $this->validate($request, $this->vr->seasongroupValidator());        
       $seasongroup = SeasonGroup::create(array_merge($request->all(), ['user_id' => \Auth::user()->id]));      
       return ['created' => 'true','id' => $seasongroup->id];
   }
   /**/////////////////////////////////////////////////////////////////////////////////////////////UPDATE POST
   public function update(Request $request, $id)
   {
       //dd($request->all());
       //$property = Property::findOrFail($id);
       $seasongroup = $this->br->getSeasonGroupById($id);
       $fv = $this->validate($request, $this->vr->seasongroupValidator());       
       $seasongroup->update($request->all());      
       return ['saved' => 'true','id' => $seasongroup->id];        
   }
   /**/////////////////////////////////////////////////////////////////////////////////////////////   

   public function destroy($id)
   {
       $seasongroup = $this->br->getSeasonGroupById($id);
       //$this->im->destroyAllImages($type='App\Property',$id);
       $seasongroup->delete();
       return response()
           ->json(['deleted' => true]);
   }

}
