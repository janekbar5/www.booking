<?php

namespace App\Http\Controllers;

use App\BookingPayment;
use Illuminate\Http\Request;

use App\Repositories\ValidationRepository;
use App\Repositories\Interfaces\BackendRepositoryInterface;

class BookingPaymentController extends Controller
{
/**/////////////////////////////////////////////////////////////////////////////////////////////1 INDEX
    function __construct(ValidationRepository $vr, BackendRepositoryInterface $br)
    {         
        $this->br = $br;
        $this->vr = $vr;
    }
    
    public function index()
    {       
        $bookingPayments = $this->br->getUserBookingPayments(\Auth::user()->id);       
        return response()
            ->json(['results' => $bookingPayments]);     
    }
}
