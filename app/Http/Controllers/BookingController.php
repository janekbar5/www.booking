<?php

namespace App\Http\Controllers;
use App\{Property,Group,Season,Booking,User,Customer,Address};
use App\Repositories\ValidationRepository;
use App\Repositories\Interfaces\BackendRepositoryInterface;
use Illuminate\Http\Request;
use Response;
use Carbon\CarbonPeriod;
use DateTime;

class BookingController extends Controller
{
    public function __construct(ValidationRepository $vr,BackendRepositoryInterface $br)
    {
        $this->vr = $vr;
        $this->br = $br;
    }
    /**/////////////////////////////////////////////////////////////////////////////////////////////1 INDEX
   /* public function index()
    {       
        $bookings = $this->br->getUserBookings(\Auth::user()->id);       
        return response()->json(['results' => $bookings]);
    }*/
    /**/////////////////////////////////////////////////////////////////////////////////////////////1 INDEX    
    public function index(){ 
        $bookings = $this->br->getUserBookings(\Auth::user()->id);       
        ////////////////////////////////////////////////////////////Own Filter Loop 
        $this->br->ownFilterLoop($_GET,$bookings);       
        ////////////////////////////////////////////////////////////Own Order ALWAYS EXIST
        $bookings = $bookings->orderBy(request('field'),request('order'));        
        /////////////////////////////////////////////////////////////Foreign Order
        if(request('field')=='customer_id'){
            $bookings = $bookings
            ->join('customers','customers.id','=','bookings.customer_id')->select('customers.first_name_t as regionName','bookings.*')
            ->orderBy('regionName',request('order'));
        }else{
        ////////////////////////////////////////////////////////////Own Order ALWAYS EXIST
             //$bookings = $bookings->orderBy(request('field'),request('order'));       
        }   

        $bookings = $bookings->paginate(request('perPage'));  
        return response()->json(['rows' => $bookings]);      
    }
    /**/////////////////////////////////////////////////////////////////////////////////////////////2 EDIT
    public function edit($id)
    {
        return $this->br->getUserBookingById($id);
    }
     /**/////////////////////////////////////////////////////////////////////////////////////////////3 CREATE


    /**/////////////////////////////////////////////////////////////////////////////////////////////4 CREATE POST
    public function store(Request $request)
    {        
       $fv = $this->validate($request, $this->vr->newBookings());   
            if ($request->input('customer_id') == '')
            { //new customer
                //dd($request->all());    
                $customer = Customer::create(array_merge($request->all(), ['user_id' => \Auth::user()->id]));
                Address::create(array_merge($request->all(), ['user_id' => \Auth::user()->id, 'addressable_type'=> 'App\Customer', 'addressable_id'=> $customer->id ] ));
                Booking::create(array_merge($request->all(), ['customer_id' => $customer->id, 'price' => 11, 'uuid' => $request->input('group_id'), 'property_id' => $request->input('resource_id'), 'user_id' => \Auth::user()->id]));

            } else
            { //existing
                //dd($request->all());    
                Booking::create(array_merge($request->all(), ['price' => 11, 'uuid' => $request->input('group_id'), 'property_id' => $request->input('resource_id'), 'user_id' => \Auth::user()->id]));
            }
            return response()->json(['success' => 'Done!']);       
    }
    /**/////////////////////////////////////////////////////////////////////////////////////////////5 UPDATE POST


    /**/////////////////////////////////////////////////////////////////////////////////////////////6 DESTROY   
    public function doDeleteBooking(Request $request)
    {
        $booking = Booking::find($request->input('booking_id'));
        $booking->delete();
        return Response::json(['success' => 'deleted',]);
    }
    public function destroy(Request $request,$id)
    {
        $booking = Booking::find($id);
        $booking->delete();
        return Response::json(['success' => 'deleted',]);
    }





    
    /**/////////////////////////////////////////////////////////////////////////////////////////////doMoveorResize
    public function doMoveorResize(Request $request)
    {
        //dd($request->all());
        try
        {
            $booking = Booking::where('property_id', '=', (int) $request->Input('resource'))->first();
            $booking->time_from = $request->input('newStart');
            $booking->time_to = $request->input('newEnd');
            $booking->update();
            return Response::json(['success' => '1', 'object' => $request->input('dates')]);
        } catch (\Exception $e)
        {
            return Response::json(['errors' => "errrrr"]);
        }
    }

    public function getEventsResources()
    {       
        //////////////////////////////////////////Events
        $events = [];       
        $query = Booking::with('user', 'customer', 'status', 'payment')->get();
        $events_ok = $query->map(function($items) {
            $events['id'] = $items->id;
            $events['resource'] = (string) $items->property_id;
            $events['start'] = $items->time_from;
            $events['end'] = $items->time_to;
            $events['text'] = $items->customer->first_name_t . " " . $items->customer->last_name_t;
            $events['status'] = $items->status->title;
            $events['paid'] = $items->payment->value;
            $events['colour'] = $items->status->colour;
            return $events;
        });  
        //////////////////////////////////////////Resources    
        $groups = Group::where('user_id', '=', \Auth::user()->id)->with('properties')->get();  
        $resources = array();
        foreach ($groups as $group) {
            $r = new \stdClass();
            $r->name = $group['title'];
            $r->id = "G" . $group['id'];
            $r->expanded = "true";
            $r->uuid = $group['uuid'];
            $r->children = $this->children($group->properties, $group['uuid']);
            $resources[] = $r;
        }       
        return Response::json(['events' => $events_ok,'resources' => $resources, ]);
    }
     /**/////////////////////////////////////////////////////////////////////////////////////////////
    public function children($groups, $uuid)
    {
        $children = array();
        foreach ($groups as $prop) {
            $c = new \stdClass();
            $c->name = $prop['title'];
            $c->id = "$prop[id]";
            $c->columns = array(
                //array('html' => "Max Persons: " . $prop['max_pers'] . "<br> Area: " . $prop['area']),
                //array('html' => $prop['area']."<br>".$prop['area']), 
                //array('html' => "<a href="/properties2/edit/". $prop['max_pers'] . ")
                array('html' => "<a href=/properties/".$prop['id']."/edit>Edit</a>"),
                //array('html' => "<router-link to=/properties/".$prop['id']."/edit>Edit</router-link>"),
            );
            $c->uuid = $uuid;
            $c->seasongroup_id = $prop['seasongroup_id'];
            $children[] = $c;
        }
        return $children;
    }
    
    
    /**/////////////////////////////////////////////////////////////////////////////////////////////
    public function getPricesByGroupId()
    {
        $start_date = '2020-06-15'; 
        $end_date = '2020-10-20';
        $seasongroup = 2;          
        //////////////////////////do repo
        $seasons = Season::
                where('seasongroup_id', '=', $seasongroup)
                ->where('start_date', '<=', $end_date)
                ->where('end_date', '>=', $start_date)
                ->get();
        $totalDays = CarbonPeriod::create($start_date, $end_date);
        $days = array();
        foreach ($totalDays as $date) {
            $days[] = $date->format('Y-m-d');
            echo $date->format('Y-m-d').'</br>';
        }
       echo '///////////////////////////////////////////////////////<br>';
        ///////////////////////////////////////////
        $seasons_ok = array();
        foreach ($seasons as $season) {
            $period_2 = CarbonPeriod::create($season->start_date, $season->end_date);
            foreach ($period_2 as $date_2) {
                //$seasons_ok[] = $date_2->format('Y-m-d');
                echo $date_2->format('Y-m-d').'--'.$season->title.'</br>';
            }
        }
    }
    ///    
    public function getSeasonPrices(Request $request)
    {
        //dd($request->resource);
        $start_date = $request->start; 
        $end_date = $request->end; 
        ////repo        
        $property = $this->br->getUserPropertyById($request->resource); 
        //dd($property);    
        $seasongroup = $property->seasongroup_id;
        //////////////////////////do repo
        $seasons = Season::
                where('seasongroup_id', '=', $seasongroup)
                ->where('time_from', '<=', $end_date)
                ->where('time_to', '>=', $start_date)
                ->get();
        $totalDays = CarbonPeriod::create($start_date, $end_date);
        $days = array();
        foreach ($totalDays as $date) {
            $days[] = $date->format('Y-m-d');
        }
        ///////////////////////////////////////////
        $seasons_ok = array();
        foreach ($seasons as $season) {
            $period_2 = CarbonPeriod::create($season->start_date, $season->end_date);
            foreach ($period_2 as $date_2) {
                $seasons_ok[] = $date_2->format('Y-m-d');
            }
        }
        //////////////////////////////////////////
        $outside = array_diff($days, $seasons_ok);
        $inseasons = array_diff($days, $outside);
        //////////////////////////////////////////////////standard days
        $total_price_outside_season = 0;
        $standard_days_number = 0;
        $standarddays = array();
        foreach ($outside as $out) {
            if ($this->isFirstDay($start_date, $out) == 1)
            {
                //echo $out."</br>";    
            } else
            {
                $total_price_outside_season += $property->price;
                $standard_days_number++;
                $standarddays[] = array('day' => $out, 'seazon_name' => 'Cena domyslna', 'price_per_day' => $property->price);
            }
        }
        ///////////////////////////////////////////////
        $days_in_season = array();
        foreach ($inseasons as $in) {
            foreach ($seasons as $sea) {
                $in_or_out = $this->isDateBetween($sea->start_date, $in, $sea->end_date);
                if ($in_or_out == 1){
                    $days_in_season[] = array('day' => $in, 'price' => $sea->price, 'title' => $sea->title);
                } else  {
                    
                }
            }
        }
        ///////////////////////////////////////
        $tempArr = array_unique(array_column($days_in_season, 'day'));
        $unique_days_in_season = array_intersect_key($days_in_season, $tempArr);
        /////////////////////////////////////
        $season_days_number = 0;
        $total_price_inside_season = 0;
        $allseasons = array();
        foreach ($unique_days_in_season as $season) {
            if ($this->isFirstDay($start_date, $season['day']) == 1)
            {
                //echo $cos['day']."</br>";    
            } else
            {
                $season_days_number++;
                $total_price_inside_season += $season['price'];
                $e = new \stdClass();
                $e->day = $season['day'];
                $e->seazon_name = $season['title'];
                $e->price_per_day = $season['price'];               
                $allseasons[] = $e;
            }
        }
        ////////////////////////////////////////////////////////////
        $myObj = new \stdClass();
        $myObj->from = $start_date;
        $myObj->to = $end_date;
        $myObj->standard_days_number = $standard_days_number;
        $myObj->standard_days_price = $property->price;
        $myObj->standard_total_price = $total_price_outside_season;
        /////////////////////////////////////////////////////////////
        $myObj->season_days_number = $season_days_number;
        $myObj->season_total_price = $total_price_inside_season;
        //////////////////////////////////////////////////////////
        $myObj->total_price = $total_price_outside_season + $total_price_inside_season;
        /////////////////////////////////////////////////////////////        
        $myObj->standard_days = $standarddays;
        ///////////////////////////////////////////////////////////        
        $myObj->seasons_days = $allseasons;
        return response()->json($myObj);
    }
    /**/////////////////////////////////////////////////////////////////////////////////////////////
    public function isFirstDay($dt_start, $dt_check)
    {
        if (strtotime($dt_check) == strtotime($dt_start))
        {
            $first = 1;
        } else
        {
            $first = 0;
        }
        return $first;
    }
   /**/////////////////////////////////////////////////////////////////////////////////////////////
    public function isDateBetween($dt_start, $dt_check, $dt_end)
    {
        if (strtotime($dt_check) >= strtotime($dt_start) && strtotime($dt_check) <= strtotime($dt_end)){
            $costam = 1;
        } else   {
            $costam = 0;
        }
        return $costam;
    }

   

        
    

}
