<?php

namespace App\Http\Controllers;

use App\Advantage;
use Illuminate\Http\Request;

class AdvantageController extends Controller
{
    
    public function index()
    {
        $advantages = Advantage::where('user_id', '=', \Auth::user()->id)->paginate(10);	 
        return view('backend.advantages.index',compact('advantages'));
    }

    /**
     * ##################################################
     * ##################################################
     * ##################################################
     * ##################################################
     */
    public function create()
    {
        return view('backend.advantages.create');
    }

    /**
     * ##################################################
     * ##################################################
     * ##################################################
     * ##################################################
     */
    public function store(Request $request)
    {
        request()->validate([
            'title' => 'required',
            'description' => 'required',			
        ]);		
        Advantage::create($request->all());
        return redirect()->route('advantages.index')
                         ->with('success','Advantage created successfully.');
    }

    /**
     * ##################################################
     * ##################################################
     * ##################################################
     * ##################################################
     */
    public function show(Advantage $advantage)
    {
        //
    }

    /**
     * ##################################################
     * ##################################################
     * ##################################################
     * ##################################################
     */
    public function edit(Advantage $advantage)
    {
        return view('backend.advantages.edit',compact('advantage'));
    }

    /**
     * ##################################################
     * ##################################################
     * ##################################################
     * ##################################################
     */
    public function update(Request $request, Advantage $advantage)
    {
        request()->validate([
            'title' => 'required',
            'description' => 'required',
        ]);
		//dd($request->all());
		//dd($request->input('inventories', []));
		
		        
		$advantage->update($request->all());
        return redirect()->route('advantages.index')
                         ->with('success','Advantage updated successfully');
    }

    /**
     * ##################################################
     * ##################################################
     * ##################################################
     * ##################################################
     */
    public function destroy(Advantage $advantage)
    {
        //
    }
}
