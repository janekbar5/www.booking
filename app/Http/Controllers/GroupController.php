<?php

namespace App\Http\Controllers;

use App\Group;
use Illuminate\Http\Request;

use App\Repositories\ValidationRepository;
use App\Repositories\Interfaces\BackendRepositoryInterface;

class GroupController extends Controller
{
    function __construct(ValidationRepository $vr, BackendRepositoryInterface $br,ImageController $im)
    {         
        $this->br = $br;
        $this->vr = $vr;
        $this->im = $im;
    }
    /**/////////////////////////////////////////////////////////////////////////////////////////////1 INDEX
    public function index()
    {       
        $groups = $this->br->getUserGroups(\Auth::user()->id);       
        return response()
            ->json(['results' => $groups]);     
    }
    /**/////////////////////////////////////////////////////////////////////////////////////////////2 EDIT
    public function edit($id)
    {        
        $group = $this->br->getUserGroupById($id);               
        return response()->json([
            'form' => $group,           
            ]);         
    }
   /**/////////////////////////////////////////////////////////////////////////////////////////////3 CREATE
   public function create()
   {      
      return response()->json([
          'form' => '',          
          ]);
   }
    
   /**/////////////////////////////////////////////////////////////////////////////////////////////4 CREATE POST
   public function store(Request $request)
   {        
       $fv = $this->validate($request, $this->vr->groupUpdate());        
       $group = Group::create(array_merge($request->all(), [ 'user_id' => \Auth::user()->id, 'uuid' => \Str::uuid() ]));      
       return ['created' => 'true','id' => $group->id];
   }
    
    /**/////////////////////////////////////////////////////////////////////////////////////////////5 UPDATE POST
    public function update(Request $request, $id)
    {
        //dd($request->all());
        $group = $this->br->getUserGroupById($id);
        $fv = $this->validate($request, $this->vr->groupUpdate());       
        $group->update($request->all());      
        return ['saved' => 'true','id' => $group->id];        
    }
     /**/////////////////////////////////////////////////////////////////////////////////////////////6 DESTROY   
     public function destroy($id)
     {
         $group = $this->br->getUserGroupById($id);
         $propertiesgroup = $this->br->getUserPropertyByGroupId($id);
         //return $propertiesgroup->properties;
         //$this->im->destroyAllImages($type='App\Group',$id);  //delete group images
         $this->im->destroyImagesbyGroup($propertiesgroup->properties);   //delete group properties images
         //$this->im->destroyImagesbyGroup();   //delete group properties images
         //$group->delete();

        //  return response()
        //      ->json(['deleted' => true]);
     }

    
}
