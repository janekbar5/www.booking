<?php

namespace App\Http\Controllers;
use App\Advantage;
use Illuminate\Http\Request;
use App\Helpers\Repositories\ValidationRepository;
use App\{
    Property,
    Group,
    Season,
    Booking,
    User,
    Customer
};
use Response;
use File;
use Carbon\CarbonPeriod;
use Illuminate\Support\Facades\Validator;


class ApiController extends Controller
{

    public function __construct(ValidationRepository $vr)
    {
        $this->vr = $vr;
    }
    /**/////////////////////////////////////////////////////////////////////////////////////////////
    public function getEventsResources()
    {       
        //////////////////////////////////////////Events
        $events = [];       
        $query = Booking::with('user', 'customer', 'status', 'payment')->get();
        $events_ok = $query->map(function($items) {
            $events['id'] = $items->id;
            $events['resource'] = (string) $items->property_id;
            $events['start'] = $items->time_from;
            $events['end'] = $items->time_to;
            $events['text'] = $items->customer->first_name . " " . $items->customer->last_name;
            $events['status'] = $items->status->title;
            $events['paid'] = $items->payment->value;
            $events['colour'] = $items->status->colour;
            return $events;
        });  
        //////////////////////////////////////////Resources    
        $groups = Group::where('user_id', '=', \Auth::user()->id)->with('properties')->get();  
        $resources = array();
        foreach ($groups as $group) {
            $r = new \stdClass();
            $r->name = $group['title'];
            $r->id = "G" . $group['id'];
            $r->expanded = "true";
            $r->uuid = $group['uuid'];
            $r->children = $this->children($group->properties, $group['uuid']);
            $resources[] = $r;
        }       
        return Response::json(['events' => $events_ok,'resources' => $resources, ]);
    }
    /**/////////////////////////////////////////////////////////////////////////////////////////////
    public function store(Request $request)
    {
        $this->validate($request,[
            'time_to' => 'required',
            'time_from' => 'required',
            'bio' => 'required',
            'first_name' => 'required',
            'last_name' => 'required'
        ]);
        return User::create([
            'name' => $request['name'],
            'email' => $request['email'],
            'type' => $request['type'],
            'bio' => $request['bio'],
            'photo' => $request['photo'],
            'password' => Hash::make($request['password']),
        ]);
    }





















    public function getBookingDetails2($id)
    {
        $booking = Booking::where('id', '=', $id)
                ->with('customer')
                ->first();
        return $booking;
    }

    public function getBookingDetails($id)
    {

        $booking = Booking::where('id', '=', $id)->first();
        return view('backend._partial.modals.modal-edit', compact('booking'));
    }

    public function postBooking(Request $request)
    {
        $fv = $this->vr->bookingValidator($request->all());
        if ($fv->fails())
        {
            //return $fv->messages();
            //{"message":"The given data was invalid.","errors":{"email":["Pole email jest wymagane."],"name":["Pole name jest wymagane."]}}
            return response()->json(['message' => 'error', 'errors' => $fv->messages()]);
        } else
        {
            return response()->json(['message' => 'success']);
        }
    }

    /**
     * ##################################################
     * ##################################################
     * ##################################################
     * ##################################################
     */
    public function findAvailable(Request $request)
    {

        //    http://www.lar-booking-adminlte-3-production.test/v1/api/findavailable?max_pers=3&time_from=2019-10-17&time_to=2019-10-31

        $startTime = date('Y-m-d', strtotime($request->input('time_from')));
        $endTime = date('Y-m-d', strtotime($request->input('time_to')));
        $max_pers = $request->input('max_pers');

        $data = [];

        $properties4 = Property::
                where('max_pers', '=', $max_pers)
                ->with('booking')
                ->WhereDoesntHave('booking', function ($q) use ( $startTime, $endTime) {
                    $q
                    ->whereRaw("time_from >= '$startTime' AND time_from < '$endTime'")
                    ->orwhereRaw("time_from <= '$startTime' AND time_to > '$endTime'")
                    ->orwhereRaw("time_to > '$startTime' AND time_to <= '$endTime'")
                    ->orwhereRaw("time_from >= '$startTime' AND time_to <= '$endTime'")
                    ;
                })
                ->get();

        foreach ($properties4 as $property) {
            $property->seasons = $this->getSeasons($property->seasongroup_id, $startTime, $endTime);
            $property->duration = $this->daysBetween($startTime, $endTime);
        }

        //return Response::json(['data' => $properties4]);		
        //return response()->json(['data' => $properties4]);
        //return view('front.index2', compact('properties4'));
        return $properties4;
    }

    /**
     * ##################################################
     * ##################################################
     * ##################################################
     * ##################################################
     */
    public function findAvailableinGroups(Request $request)
    {
//    http://www.lar-booking-adminlte-3-production.test/v1/api/findavailableingroups?max_pers=2&time_from=2019-10-01&time_to=2019-10-15&uuid=a4e39510-2746-421c-a1d6-fdd35d13d8fd        
        $startTime = date('Y-m-d', strtotime($request->input('time_from')));
        $endTime = date('Y-m-d', strtotime($request->input('time_to')));
        $uuid = $request->input('uuid');
        $max_pers = $request->input('max_pers');

        $properties4 = Property::
                where('max_pers', '=', $max_pers)
                ///////////////////
                ->with('photos', 'inventories')
                ////////////////////
                ->with('booking')
                ->WhereDoesntHave('booking', function ($a) use ( $startTime, $endTime ) {
                    $a
                    ->whereRaw("time_from >= '$startTime' AND time_from < '$endTime'")
                    ->orwhereRaw("time_from <= '$startTime' AND time_to > '$endTime'")
                    ->orwhereRaw("time_to > '$startTime' AND time_to <= '$endTime'")
                    ->orwhereRaw("time_from >= '$startTime' AND time_to <= '$endTime'");
                })
                //////////////
                ->with('groups')->whereHas('groups', function($b) use($uuid) {
                    $b
                    ->where('uuid', 'LIKE', '%' . $uuid . '%');
                })
                ////////////
                ->get();
        foreach ($properties4 as $property) {
            $property->seasons = $this->getSeasons($property->seasongroup_id, $startTime, $endTime);
            $property->duration = $this->daysBetween($startTime, $endTime);
        }
        //return response()->json(['data' => $properties4]);
        //return view('front.index2', compact('properties4'));  
        return $properties4;
    }

    /**
     * ##################################################
     * ##################################################
     * ##################################################
     * ##################################################
     */
    public function getSeasons($seasongroup, $start_date, $end_date)
    {
        $seasons = Season::
                where('seasongroup_id', '=', $seasongroup)
                ->where('start_date', '<', $end_date)
                ->where('end_date', '>', $start_date)
                ->get();

        foreach ($seasons as $season) {
            $season->duration = $this->daysBetween($start_date, $end_date);
            $season->daysinseason = $this->daysBetween($season->start_date, $season->end_date);
            $season->daysspentinseason = $this->daysBetween($start_date, $season->end_date);
        }

        return $seasons;
    }

    /**
     * ##################################################
     * ##################################################
     * ##################################################
     * ##################################################
     */
    public function daysBetween($start_date, $end_date)
    {
        return date_diff(
                        date_create($end_date), date_create($start_date)
                )->format('%a');
    }

    /**
     * ##################################################
     * ##################################################
     * ##################################################
     * ##################################################
     */

    /**
     * ##################################################
     * ##################################################
     * ##################################################
     * ##################################################
     */
    public function getResourcesGroups()
    {
        $groups = Group::where('user_id', '=', \Auth::user()->id)->with('properties')->get();
        //dd($properties);

        $json3 = array();

        foreach ($groups as $group) {
            $r = new \stdClass();
            $r->name = $group['title'];
            $r->id = "G" . $group['id'];
            $r->expanded = "true";
            $r->uuid = $group['uuid'];
            $r->children = $this->children($group->properties, $group['uuid']);
            $json3[] = $r;
        }
        header('Content-Type: application/json');
        return $json3;

//        $path1 = '/storage/app/calendar_Ids.json';
//        $path = File::get("database/calendar_ids.json");
//        $content = json_decode(file_get_contents($path), true);
        //$path = storage_path() . "\app\calendar_ids.json"; // ie: /var/www/laravel/app/storage/json/filename.json
        //$json = json_decode(file_get_contents($path), true); 

        return $json3;
    }

    /**
     * ##################################################
     * ##################################################
     * ##################################################
     * ##################################################
     */
    public function children($groups, $uuid)
    {
        $children = array();
        foreach ($groups as $prop) {
            $c = new \stdClass();
            $c->name = $prop['title'];
            $c->id = "$prop[id]";
            $c->columns = array(
                array('html' => "Max Persons: " . $prop['max_pers'] . "<br> Area: " . $prop['area']),
                //array('html' => $prop['area']."<br>".$prop['area']), 
                array('html' => '<small class="label bg-yellow"><i class="fa fa-eye" aria-hidden="true"></i></small>')
            );
            $c->uuid = $uuid;
            $c->seasongroup_id = $prop['seasongroup_id'];
            $children[] = $c;
        }
        return $children;
    }

    /**
     * ##################################################
     * ##################################################
     * ##################################################
     * ##################################################
     */
    public function getEvents()
    {

        /////////////////////////2 queries 5ms

        $data = [];
        // Query the users table

        $query = Booking::with('user', 'customer', 'status', 'payment')->get();

        /////////////////////////6 queries 8ms
//        $query = Booking::where('user_id', '=', \Auth::user()->id)
//                        ->with('user')
//                        ->with('customer')
//                        ->with('status')
//                        ->with('payment')
//                        ->get();
        //Let's Map the results from [$query]
        $map = $query->map(function($items) {
            $data['id'] = $items->id;
            $data['resource'] = (string) $items->property_id;
            $data['start'] = $items->time_from;
            $data['end'] = $items->time_to;
            $data['text'] = $items->customer->first_name . " " . $items->customer->last_name;
            $data['status'] = $items->status->title;
            $data['paid'] = $items->payment->value;
            $data['colour'] = $items->status->colour;
            return $data;
        });        
        return $map;
    }

    /**
     * ##################################################
     * ##################################################
     * ##################################################
     * ##################################################
     */
    public function getSeasonPrices(Request $request)
    {
        $property = Property::where('id', '=', $request->vars['resource'])->first();
        return $this->getSeasons($property->seasongroup_id, $request->vars['start'], $request->vars['end']);
    }

    /**
     * ##################################################
     * ##################################################
     * ##################################################
     * ##################################################
     */
    public function getSeasonPrices2(Request $request)
    {
        dd($request->all());
        $start_date = $request->vars['start']; /* Lecture 19 */
        $end_date = $request->vars['end']; /* Lecture 19 */
        ////repo
        $property = Property::where('id', '=', $request->vars['resource'])->first();

        $seasongroup = $property->seasongroup_id;
        //////////////////////////do repo
        $seasons = Season::
                where('seasongroup_id', '=', $seasongroup)
                ->where('start_date', '<=', $end_date)
                ->where('end_date', '>=', $start_date)
                ->get();

        $totalDays = CarbonPeriod::create($start_date, $end_date);

        $days = array();
        foreach ($totalDays as $date) {
            $days[] = $date->format('Y-m-d');
        }

        $seasons_ok = array();
        foreach ($seasons as $season) {
            $period_2 = CarbonPeriod::create($season->start_date, $season->end_date);
            foreach ($period_2 as $date_2) {
                $seasons_ok[] = $date_2->format('Y-m-d');
            }
        }
        $outside = array_diff($days, $seasons_ok);
        $inseasons = array_diff($days, $outside);
        $price_outside_season = 0;
        $standard_days_number = 0;
        foreach ($outside as $out) {
            //echo $out." ".$property->price."</br>";
            $price_outside_season += $property->price;
            $standard_days_number++;
        }

        $price_inside_season = 0;
        foreach ($inseasons as $in) {
            foreach ($seasons as $sea) {
                $in_or_out = $this->isDateBetween($sea->start_date, $in, $sea->end_date);
                if ($in_or_out == 1)
                {
                    //echo $in." ".$in_or_out." ".$sea->price."</br>"; 
                    $price_inside_season += $sea->price;
                }
            }
        }
        foreach ($seasons as $sea) {
            $e = new \stdClass();
            $e->seazon_name = $sea->title;
            $e->price_per_day = $sea->price;
            $e->dni_w_sezonie = $this->daysBetween($sea->start_date, $sea->end_date);
            $e->total_in_season = $e->price_per_day * $e->dni_w_sezonie;
            $allseasons[] = $e;
        }

        $myObj = new \stdClass();
        $myObj->from = $start_date;
        $myObj->to = $end_date;
        $myObj->standard_days_number = $standard_days_number;
        $myObj->standard_days_price = $property->price;
        $myObj->total_price = $price_outside_season + $price_inside_season;
        $myObj->seasons = $allseasons;

        return response()->json($myObj);
    }

    /**
     * ##################################################
     * ##################################################
     * ##################################################
     * ##################################################
     */
    public function getSeasonPrices3(Request $request)
    {
        //dd($request->resource);
        $start_date = $request->start; /* Lecture 19 */
        $end_date = $request->end; /* Lecture 19 */
        ////repo
        $property = Property::where('id', '=', $request->resource)->first();
        //dd($property);    
        $seasongroup = $property->seasongroup_id;
        //////////////////////////do repo
        $seasons = Season::
                where('seasongroup_id', '=', $seasongroup)
                ->where('start_date', '<=', $end_date)
                ->where('end_date', '>=', $start_date)
                ->get();

        $totalDays = CarbonPeriod::create($start_date, $end_date);

        $days = array();
        foreach ($totalDays as $date) {
            $days[] = $date->format('Y-m-d');
        }

        $seasons_ok = array();
        foreach ($seasons as $season) {
            $period_2 = CarbonPeriod::create($season->start_date, $season->end_date);
            foreach ($period_2 as $date_2) {
                $seasons_ok[] = $date_2->format('Y-m-d');
            }
        }
        //////////////////////////////////////////
        $outside = array_diff($days, $seasons_ok);
        $inseasons = array_diff($days, $outside);

        //////////////////////////////////////////////////standard days
        $total_price_outside_season = 0;
        $standard_days_number = 0;
        $standarddays = array();
        foreach ($outside as $out) {
            if ($this->isFirstDay($start_date, $out) == 1)
            {
                //echo $out."</br>";    
            } else
            {
                $total_price_outside_season += $property->price;
                $standard_days_number++;
                $standarddays[] = array('day' => $out, 'seazon_name' => 'Cena domyslna', 'price_per_day' => $property->price);
            }
        }
        ///////////////////////////////////////////////
//        $price_inside_season = 0;
//        foreach ($inseasons as $in) {
//            foreach ($seasons as $sea) {
//                $in_or_out = $this->isDateBetween($sea->start_date, $in, $sea->end_date);
//                if ($in_or_out == 1)
//                {
//                    $price_inside_season += $sea->price;
//                }
//            }
//        }
        ////////////////////////////////////////////

        $days_in_season = array();
        foreach ($inseasons as $in) {
            foreach ($seasons as $sea) {
                $in_or_out = $this->isDateBetween($sea->start_date, $in, $sea->end_date);
                if ($in_or_out == 1)
                {
                    $days_in_season[] = array('day' => $in, 'price' => $sea->price, 'title' => $sea->title);
                } else
                {
                    
                }
            }
        }
        ///////////////////////////////////////
        $tempArr = array_unique(array_column($days_in_season, 'day'));
        $unique_days_in_season = array_intersect_key($days_in_season, $tempArr);
        /////////////////////////////////////
        $season_days_number = 0;
        $total_price_inside_season = 0;
        $allseasons = array();
        foreach ($unique_days_in_season as $season) {
            if ($this->isFirstDay($start_date, $season['day']) == 1)
            {
                //echo $cos['day']."</br>";    
            } else
            {
                $season_days_number++;
                $total_price_inside_season += $season['price'];
                $e = new \stdClass();
                $e->day = $season['day'];
                $e->seazon_name = $season['title'];
                $e->price_per_day = $season['price'];
                //$e->dni_w_sezonie = $season_days_number;
                //$e->total_in_season = $e->price_per_day * $e->dni_w_sezonie;
                $allseasons[] = $e;
            }
        }

        $myObj = new \stdClass();
        $myObj->from = $start_date;
        $myObj->to = $end_date;

        $myObj->standard_days_number = $standard_days_number;
        $myObj->standard_days_price = $property->price;
        $myObj->standard_total_price = $total_price_outside_season;
        //////////////////////////////////////////////////////////
        $myObj->season_days_number = $season_days_number;
        $myObj->season_total_price = $total_price_inside_season;
        //////////////////////////////////////////////////////////
        $myObj->total_price = $total_price_outside_season + $total_price_inside_season;
        /////////////////////////////////////////////////////////////        
        $myObj->standard_days = $standarddays;
        //////////////////////////////////////////        
        $myObj->seasons_days = $allseasons;

        return response()->json($myObj);
    }

    /**
     * ##################################################
     * ##################################################
     * ##################################################
     * ##################################################
     */
    public function isFirstDay($dt_start, $dt_check)
    {
        if (strtotime($dt_check) == strtotime($dt_start))
        {
            $first = 1;
        } else
        {
            $first = 0;
        }
        return $first;
    }

    /**
     * ##################################################
     * ##################################################
     * ##################################################
     * ##################################################
     */
    public function isDateBetween($dt_start, $dt_check, $dt_end)
    {
        if (strtotime($dt_check) >= strtotime($dt_start) && strtotime($dt_check) <= strtotime($dt_end))
        {
            $costam = 1;
        } else
        {
            $costam = 0;
        }
        return $costam;
    }

    /**
     * ##################################################
     * ##################################################
     * ##################################################
     * ##################################################
     */
    public function doMoveorResize(Request $request)
    {
        //dd($request->all());
        try
        {
            $booking = Booking::where('property_id', '=', (int) $request->Input('resource'))->first();
            $booking->time_from = $request->input('newStart');
            $booking->time_to = $request->input('newEnd');
            $booking->update();
            return Response::json(['success' => '1', 'object' => $request->input('dates')]);
        } catch (\Exception $e)
        {
            return Response::json(['errors' => "errrrr"]);
        }
    }

    /**
     * ##################################################
     * ##################################################
     * ##################################################
     * ##################################################
     */
    public function doDeleteBooking(Request $request)
    {
        $booking = Booking::find($request->input('booking_id'));
        $booking->delete();
        return Response::json(['success' => 'deleted',]);
    }
    /**
     * ##################################################
     * ##################################################
     * ##################################################
     * ##################################################
     */
    public function newBookingsPost(Request $request)
    {
        
       $fv = $this->validate($request, $this->vr->newBookings());          
      
       //$validator = Validator::make($request->all(), $fv);
       
       


      
            if ($request->input('customer_id') == '')
            { //new customer
                //dd($request->all());    
                $customer = Customer::create((array_merge($request->all(), ['user_id' => \Auth::user()->id])));
                Booking::create(array_merge($request->all(), ['customer_id' => $customer->id, 'price' => 11, 'uuid' => $request->input('group_id'), 'property_id' => $request->input('resource_id'), 'user_id' => \Auth::user()->id]));
            } else
            { //existing
                //dd($request->all());    
                Booking::create(array_merge($request->all(), ['price' => 11, 'uuid' => $request->input('group_id'), 'property_id' => $request->input('resource_id'), 'user_id' => \Auth::user()->id]));
            }
            return response()->json(['success' => 'Done!']);
       
    }
     /**
     * ##################################################
     * ##################################################
     * ##################################################
     * ##################################################
     */
    public function updateBookingsPost(Request $request)
    {
        dd($request->all());
    }

}