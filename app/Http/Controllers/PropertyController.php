<?php

namespace App\Http\Controllers;

use App\Property;
use Illuminate\Http\Request;
use App\{SeasonGroup,Group,Inventory,Advantage,User};
use App\Repositories\ValidationRepository;
use App\Repositories\Interfaces\BackendRepositoryInterface;

class PropertyController extends Controller
{
    //protected $backendRepository;
    
    function __construct(ValidationRepository $vr, BackendRepositoryInterface $br, ImageController $im)
    {        
        $this->middleware('permission:properties-list');
        $this->middleware('permission:properties-create', ['only' => ['create', 'store']]);
        $this->middleware('permission:properties-edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:properties-delete', ['only' => ['destroy']]);        
        $this->br = $br;
        $this->vr = $vr;
        $this->im = $im;
    }
    /**/////////////////////////////////////////////////////////////////////////////////////////////1 INDEX
    public function index()
    {       
        $properties = $this->br->getUserProperties(\Auth::user()->id);       
        return response()->json(['results' => $properties]);
    }
    /**/////////////////////////////////////////////////////////////////////////////////////////////2 EDIT
    public function edit($id)
    {        
        $property = $this->br->getUserPropertyById($id);   
        $allinventories = Inventory::all();
        $alladvantages = Advantage::all();
        $allgroups = $this->br->getUserGroups(\Auth::user()->id); 
        $seasonGroups = $this->br->getUserSeasonGroups(\Auth::user()->id);  
        return response()->json([
            'form' => $property,
            'allinventories' => $allinventories,
            'alladvantages' => $alladvantages,
            'allgroups' => $allgroups,
            'seasonGroups' => $seasonGroups,
            ]);         
    }
    /**/////////////////////////////////////////////////////////////////////////////////////////////3 CREATE
     public function create()
     {        
        $allinventories = Inventory::all();
        $alladvantages = Advantage::all();
        $allgroups = $this->br->getUserGroups(\Auth::user()->id);
        $seasonGroups = $this->br->getUserSeasonGroups(\Auth::user()->id);        
        return response()->json([
            'form' => '',
            'allinventories' => $allinventories,
            'alladvantages' => $alladvantages,
            'allgroups' => $allgroups,
            'seasonGroups' => $seasonGroups,
            ]);
     }
    /**/////////////////////////////////////////////////////////////////////////////////////////////4 CREATE POST
    public function store(Request $request)
    {        
        //dd($request->all());
        $fv = $this->validate($request, $this->vr->propertyUpdate());        
        $property = Property::create(array_merge($request->all(), ['user_id' => \Auth::user()->id]));
        if($request->has('selectedInventories')){
            $property->inventories()->sync($request->input('selectedInventories'));
        }
        if($request->has('selectedAdvantages')){
            $property->advantages()->sync($request->input('selectedAdvantages'));
        }
        return ['created' => 'true','id' => $property->id];
    }
    /**/////////////////////////////////////////////////////////////////////////////////////////////5 UPDATE POST
    public function update(Request $request, $id)
    {
        //dd($request->all());
        //$property = Property::findOrFail($id);
        $property = $this->br->getUserPropertyById($id);
        $fv = $this->validate($request, $this->vr->propertyUpdate());       
        $property->update($request->all());
        $property->inventories()->sync($request->input('selectedInventories'));
        $property->advantages()->sync($request->input('selectedAdvantages'));
        return ['saved' => 'true','id' => $property->id];        
    }
    /**/////////////////////////////////////////////////////////////////////////////////////////////6 DESTROY 
    public function destroy($id)
    {
        $property = $this->br->getUserPropertyById($id);
        $this->im->destroyAllImages($type='App\Property',$id);
        $property->delete();
        return response()
            ->json(['deleted' => true]);
    }

}
