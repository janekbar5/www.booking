<?php

namespace App\Http\Controllers;

use App\Inventory;
use Illuminate\Http\Request;
use App\Repositories\ValidationRepository;
use App\Repositories\Interfaces\BackendRepositoryInterface;


class InventoryController extends Controller
{

    function __construct(ValidationRepository $vr, BackendRepositoryInterface $br, ImageController $im)
    {        
        // $this->middleware('permission:property-list');
        // $this->middleware('permission:property-create', ['only' => ['create', 'store']]);
        // $this->middleware('permission:property-edit', ['only' => ['edit', 'update']]);
        // $this->middleware('permission:property-delete', ['only' => ['destroy']]);        
        $this->br = $br;
        $this->vr = $vr;
        $this->im = $im;
    }
    /**/////////////////////////////////////////////////////////////////////////////////////////////1 INDEX
    public function index()
    {       
        $inventories = $this->br->getUserInventories(\Auth::user()->id);       
        return response()->json(['results' => $inventories]);
    }
    /**/////////////////////////////////////////////////////////////////////////////////////////////2 EDIT
    public function edit($id)
    {        
        $inventory = $this->br->getUserInventoriesById($id); 
        return response()->json([
            'form' => $inventory,            
            ]);         
    }
    /**/////////////////////////////////////////////////////////////////////////////////////////////3 CREATE
     public function create()
     {   
        return response()->json([
            'form' => '',            
            ]);
     }
    /**/////////////////////////////////////////////////////////////////////////////////////////////4 CREATE POST
    public function store(Request $request)
    {        
        //dd($request->all());
        $fv = $this->validate($request, $this->vr->inventoryUpdate());        
        $inventory = Inventory::create(array_merge($request->all(), ['user_id' => \Auth::user()->id]));        
        return ['created' => 'true','id' => $inventory->id];
    }
    /**/////////////////////////////////////////////////////////////////////////////////////////////5 UPDATE POST
    public function update(Request $request, $id)
    {
        //dd($request->all());
        //$property = Property::findOrFail($id);
        $inventory = $this->br->getUserInventoriesById($id);
        $fv = $this->validate($request, $this->vr->inventoryUpdate());       
        $inventory->update($request->all());       
        return ['saved' => 'true','id' => $inventory->id];        
    }
    /**/////////////////////////////////////////////////////////////////////////////////////////////6 DESTROY   

    public function destroy($id)
    {
        $inventory = $this->br->getUserInventoriesById($id);
        //$this->im->destroyAllImages($type='App\Property',$id);
        $inventory->delete();
        return response()
            ->json(['deleted' => true]);
    }

}
