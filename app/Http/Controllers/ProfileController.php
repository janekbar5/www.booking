<?php

namespace App\Http\Controllers;

use App\Property;
use Illuminate\Http\Request;
use App\{
    SeasonGroup,
    Group,
    Inventory,
    Advantage,
    User
};
use App\Helpers\Repositories\ValidationRepository;
use App\Repositories\Interfaces\BackendRepositoryInterface;

class ProfileController extends Controller
{

    protected $backendRepository;
    protected $ImageController;

    function __construct(ValidationRepository $validationrepository, BackendRepositoryInterface $backendRepository, ImageController $ImageController)
    {
        $this->validationrepository = $validationrepository;
        $this->backendRepository = $backendRepository;
        $this->ImageController = $ImageController;
    }

    public function index()
    {
        $user = User::find(\Auth::user()->id);
        $dir = '../resources/lang';
        $languages = array_diff(scandir($dir), array('..', '.'));
        //dd($user);
        return view('backend.profiles.edit', compact('user', 'languages'));
    }

    public function edit($id)
    {
//        $user = User::find(\Auth::user()->id);
//        dd($user);
//        return view('backend.profiles.edit', compact('user'));
    }

    public function store(Request $request)
    {
        User::update($request->all());

        return redirect()->route('admin.products.index');
    }

    public function update(Request $request)
    {

        $user = User::find(\Auth::user()->id);
        $user->update($request->all());
        
        //dd($user->photos->first()->id);
        
        if ($request->file('file') !== null)
        {
            
           if($user->photos()->exists()){
            $this->ImageController->getImageDelete($user->photos->first()->id);
            $this->ImageController->postImageUpload($request);
           }else{
              $this->ImageController->postImageUpload($request); 
           }
            
        }
        
        

        //$user->photo()->exists();
        

        return redirect()->back();
    }

}
