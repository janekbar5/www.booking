<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BookingPayment extends Model
{
    protected $table = 'booking_payments';
    protected $fillable = [
        'user_id',
        'title',
        'value',               
    ];
	/////////////////////////////////////////////////////////////////BELONGSTO
    	
}
