<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Season extends Model
{
    protected $fillable = [
		'user_id',
		'seasongroup_id',
		'colour',
		'title', 
		'time_from',
		'time_to',
		'price',
		//'votes',
		//'amount',
    ];
	
	/////////////////////////////////////////////////////////////////HASMANY
	public function seasonsgroups()
    {
        return $this->belongsTo('App\SeasonGroup');		
    }
}
