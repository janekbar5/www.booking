<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\SeasonGroup;

class Property extends Model
{

    //
    protected $appends = [
            'firstPhoto',
    ];

    function getfirstPhotoAttribute()
    {        
        return $this->photos->first();	  
    }

    protected $fillable = [
        'user_id',
        'group_id',
        'seasongroup_id',
        'title',
        'description',
        'votes',
        'price',
        'area',
        'room_nubr',
        'max_pers',
    ];

    /////////////////////////////////////////////////////////////////HASMANY
    // Property-->hasMany-->Booking   <==>  Booking-->belongsTo-->Property 
    public function booking()
    {
        return $this->hasMany('App\Booking', 'property_id');
    }   
    /////////////////////////////////////////////////////////////Images
    public function photos()
    {
        return $this->morphMany('App\Image', 'photoable');
    }
    /////////////////////////////////////////////////////////////Images
    // public function photosFirst()
    // {  
    //     return $this->title;       
    // }
    
//    public function images()
//    {
//        return $this->hasMany('App\Image', 'property_id');
//    }
//
//    public function first_image()
//    {
//        return $this->hasMany('App\Image', 'property_id')->limit(1);
//    }
//
//    public function imagesBack()
//    {
//        return $this->hasMany('App\Image', 'property_id')->orderBy('image_order');
//    }
//
//    public function imagesFront()
//    {
//        return $this->hasMany('App\Image', 'property_id');
//    }
//
//    public function getFirst()
//    {
//        return $this->images()->first();
//    }

    /////////////////////////////////////////////////////////////////BELONGSTO
    // User-->hasMany-->Property   <==>  Property-->belongsTo-->User 
    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function seasongroups()
    {
        return $this->belongsTo('App\SeasonGroup', 'seasongroup_id');
    }

    public function groups()
    {
        return $this->belongsTo('App\Group', 'group_id');
    }

    /////////////////////////////////////////////////////////////////BELONGSTOMANY PIVOT
    // Property-->belongsToMany-->Inventory   <==>  Inventory-->belongsToMany-->Property
    public function inventories()
    {
        return $this->belongsToMany(Inventory::class, 'property_inventory', 'property_id', 'inventory_id');
    }

    public function advantages()
    {
        return $this->belongsToMany(Advantage::class, 'property_advantage', 'property_id', 'advantage_id');
    }

    public function scopeWithAndWhereHas($query, $relation, $constraint)
    {
        return $query->whereHas($relation, $constraint)
                        ->with([$relation => $constraint]);
    }

}
