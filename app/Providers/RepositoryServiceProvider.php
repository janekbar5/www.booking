<?php

namespace App\Providers;

// use App\Repositories\PropertyRepository;
// use App\Repositories\Interfaces\PropertyRepositoryInterface;

use App\Repositories\BackendRepository;
use App\Repositories\Interfaces\BackendRepositoryInterface;


use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        // $this->app->bind(
        //     PropertyRepositoryInterface::class, 
        //     PropertyRepository::class	
        //    );
        $this->app->bind(            		
	        BackendRepositoryInterface::class, 
            BackendRepository::class
           );
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
