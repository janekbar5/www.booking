<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Booking extends Model
{

    protected $table = 'bookings';
    protected $fillable = [
        'user_id',
        'customer_id',
        'property_id',
        'status_id',
        'payment_id',
        'uuid',
        'time_from',
        'time_to',
        'price',
    ];

    /////////////////////////////////////////////////////////////////HASMANY
    // Booking-->hasMany-->BookingStatus   <==>  BookingStatus-->belongsTo-->Booking 


    /////////////////////////////////////////////////////////////////BELONGSTO
    // Booking-->belongsTo-->Customer  <==>  Customer-->hasMany-->Booking 
    public function customer()
    {
        return $this->belongsTo('App\Customer');
    }
    // Booking-->belongsTo-->User <==> User-->hasMany-->Booking    
    public function user()
    {
        //return $this->belongsTo('App\User');
        return $this->belongsTo('App\User')->where('id', '=', \Auth::user()->id);
    }
    // Booking-->belongsTo-->Property <==> Property-->hasMany-->Booking    
    public function property()
    {
        //return $this->belongsTo('App\User');
        return $this->belongsTo('App\Property');
    }

    public function status()
    {
        return $this->belongsTo('App\BookingStatus');
    }

    public function payment()
    {
        return $this->belongsTo('App\BookingPayment');
    }

}
