<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePropertiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('properties', function (Blueprint $table) {
            $table->increments('id');
			
			$table->integer('user_id')->unsigned(); //unsigned only positive val
			$table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
			
			$table->integer('group_id')->unsigned(); //unsigned only positive val
			$table->foreign('group_id')->references('id')->on('groups')->onDelete('cascade');	
			
			$table->integer('seasongroup_id')->unsigned(); //unsigned only positive val				
			
			$table->string('title'); //varchar	
			$table->text('description');
			
			$table->text('area');
			$table->text('room_nubr');
			$table->text('max_pers');
			$table->float('price');			
            //$table->integer('votes');
            $table->timestamps();
        });
		
		/*
		App\Property::create(['user_id' => 1,'group_id' => 1,'seasongroup_id' => 1,'title' => 'Domek 1','description' => 'Domek 1 sd','amount' => 55.50]);
		App\Property::create(['user_id' => 1,'group_id' => 1,'seasongroup_id' => 1,'title' => 'Domek 2','description' => 'Domek 1 sd','amount' => 55.50]);
		App\Property::create(['user_id' => 1,'group_id' => 1,'seasongroup_id' => 1,'title' => 'Domek 3','description' => 'Domek 1 sd','amount' => 55.50]);
		
		App\Property::create(['user_id' => 1,'group_id' => 2,'seasongroup_id' => 2,'title' => 'Pokoj 11','description' => 'Domek 1 sd','amount' => 55.50]);
		App\Property::create(['user_id' => 1,'group_id' => 2,'seasongroup_id' => 2,'title' => 'Pokoj 12','description' => 'Domek 1 sd','amount' => 55.50]);
		App\Property::create(['user_id' => 1,'group_id' => 2,'seasongroup_id' => 2,'title' => 'Pokoj 13','description' => 'Domek 1 sd','amount' => 55.50]);
		
		App\Property::create(['user_id' => 1,'group_id' => 3,'seasongroup_id' => 3,'title' => 'Pokoj 111','description' => 'Domek 1 sd','amount' => 55.50]);
		App\Property::create(['user_id' => 1,'group_id' => 3,'seasongroup_id' => 3,'title' => 'Pokoj 112','description' => 'Domek 1 sd','amount' => 55.50]);
		App\Property::create(['user_id' => 1,'group_id' => 3,'seasongroup_id' => 3,'title' => 'Pokoj 113','description' => 'Domek 1 sd','amount' => 55.50]);
		
		
		App\Property::create(['user_id' => 1,'group_id' => 3,'seasongroup_id' => 3,'title' => 'Pokoj 111','description' => 'Domek A','amount' => 55.50]);
		App\Property::create(['user_id' => 1,'group_id' => 3,'seasongroup_id' => 3,'title' => 'Pokoj 112','description' => 'Domek B','amount' => 55.50]);
		App\Property::create(['user_id' => 1,'group_id' => 3,'seasongroup_id' => 3,'title' => 'Pokoj 113','description' => 'Domek C','amount' => 55.50]);
		*/
		
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('properties');
    }
}
