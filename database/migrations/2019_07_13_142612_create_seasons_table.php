<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSeasonsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('seasons', function (Blueprint $table) {
            $table->increments('id');	
			$table->integer('user_id')->unsigned(); //unsigned only positive val
			$table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
			
			$table->integer('seasongroup_id')->unsigned();	//unsigned only positive val
			$table->foreign('seasongroup_id')->references('id')->on('season_groups')->onDelete('cascade');
			$table->string('colour'); //varchar			
			$table->string('title'); //varchar
			
			$table->date('time_from'); 
			$table->date('time_to'); 
			$table->decimal('price', 10, 2); //DECIMAL equivalent with a precision and scale
            $table->timestamps();
        });
		



	///	Domki u Edka
	App\Season::create(['user_id' => 1,'seasongroup_id' => 1,'colour' => '#ff0000','title' => 'Sezon Letni', 'time_from' => '2020-06-19','time_to' => '2020-08-14','price' => '250.00']);	
	App\Season::create(['user_id' => 1,'seasongroup_id' => 1,'colour' => '#ffff26','title' => 'Weekend Sierpniowy','time_from' => '2020-08-14','time_to' => '2020-08-17','price' => '260.00']);
	App\Season::create(['user_id' => 1,'seasongroup_id' => 1,'colour' => '#ff0000','title' => 'Sezon Letni','time_from' => '2020-08-17','time_to' => '2020-09-01','price' => '350.00']);

	App\Season::create(['user_id' => 1,'seasongroup_id' => 1,'colour' => '#ffff26','title' => 'Sezon Jesienny','time_from' => '2020-09-01','time_to' => '2020-11-08','price' => '250.00']);
	App\Season::create(['user_id' => 1,'seasongroup_id' => 1,'colour' => '#ff0000','title' => 'Poza sezonem','time_from' => '2020-11-08','time_to' => '2020-11-11','price' => '210.00']);
	App\Season::create(['user_id' => 1,'seasongroup_id' => 1,'colour' => '#ffff26','title' => 'Poza sezonem','time_from' => '2020-11-11','time_to' => '2020-12-20','price' => '210.00']);

	App\Season::create(['user_id' => 1,'seasongroup_id' => 1,'colour' => '#ffff26','title' => 'Święta Bożego Narodzenia','time_from' => '2020-12-20','time_to' => '2020-12-28','price' => '450.00']);
	App\Season::create(['user_id' => 1,'seasongroup_id' => 1,'colour' => '#ff0000','title' => 'Sylwester','time_from' => '2020-12-28','time_to' => '2021-01-02','price' => '550.00']);
	App\Season::create(['user_id' => 1,'seasongroup_id' => 1,'colour' => '#ffff26','title' => 'Sezon Zimowy I','time_from' => '2021-01-02','time_to' => '2021-01-08','price' => '250.00']);
	
	/////////// Willa Anna
	App\Season::create(['user_id' => 1,'seasongroup_id' => 2,'colour' => '#ff0000','title' => 'Sezon Letni', 'time_from' => '2020-06-19','time_to' => '2020-08-14','price' => '250.00']);	
	App\Season::create(['user_id' => 1,'seasongroup_id' => 2,'colour' => '#ffff26','title' => 'Weekend Sierpniowy','time_from' => '2020-08-14','time_to' => '2020-08-17','price' => '260.00']);
	App\Season::create(['user_id' => 1,'seasongroup_id' => 2,'colour' => '#ff0000','title' => 'Sezon Letni','time_from' => '2020-08-17','time_to' => '2020-09-01','price' => '350.00']);

	App\Season::create(['user_id' => 1,'seasongroup_id' => 2,'colour' => '#ff0000','title' => 'Sezon Jesienny','time_from' => '2020-09-01','time_to' => '2020-11-08','price' => '250.00']);
	App\Season::create(['user_id' => 1,'seasongroup_id' => 2,'colour' => '#ffff26','title' => 'Poza sezonem','time_from' => '2020-11-08','time_to' => '2020-11-11','price' => '210.00']);
	App\Season::create(['user_id' => 1,'seasongroup_id' => 2,'colour' => '#ff0000','title' => 'Poza sezonem','time_from' => '2020-11-11','time_to' => '2020-12-20','price' => '210.00']);

	App\Season::create(['user_id' => 1,'seasongroup_id' => 2,'colour' => '#ff0000','title' => 'Święta Bożego Narodzenia','time_from' => '2020-12-20','time_to' => '2020-12-28','price' => '450.00']);
	App\Season::create(['user_id' => 1,'seasongroup_id' => 2,'colour' => '#ffff26','title' => 'Sylwester','time_from' => '2020-12-28','time_to' => '2021-01-02','price' => '550.00']);
	App\Season::create(['user_id' => 1,'seasongroup_id' => 2,'colour' => '#ff0000','title' => 'Sezon Zimowy','time_from' => '2021-01-02','time_to' => '2021-01-08','price' => '250.00']);
	
	/////////// Motel Poseidon
	App\Season::create(['user_id' => 1,'seasongroup_id' => 3,'colour' => '#ff0000','title' => 'Sezon Letni', 'time_from' => '2020-06-19','time_to' => '2020-08-14','price' => '250.00']);	
	App\Season::create(['user_id' => 1,'seasongroup_id' => 3,'colour' => '#ffff26','title' => 'Weekend Sierpniowy','time_from' => '2020-08-14','time_to' => '2020-08-17','price' => '260.00']);
	App\Season::create(['user_id' => 1,'seasongroup_id' => 3,'colour' => '#ff0000','title' => 'Sezon Letni','time_from' => '2020-08-17','time_to' => '2020-09-01','price' => '350.00']);

	App\Season::create(['user_id' => 1,'seasongroup_id' => 3,'colour' => '#ff0000','title' => 'Sezon Jesienny','time_from' => '2020-09-01','time_to' => '2020-11-08','price' => '250.00']);
	App\Season::create(['user_id' => 1,'seasongroup_id' => 3,'colour' => '#ffff26','title' => 'Poza sezonem','time_from' => '2020-11-08','time_to' => '2020-11-11','price' => '210.00']);
	App\Season::create(['user_id' => 1,'seasongroup_id' => 3,'colour' => '#ff0000','title' => 'Poza sezonem','time_from' => '2020-11-11','time_to' => '2020-12-20','price' => '210.00']);

	App\Season::create(['user_id' => 1,'seasongroup_id' => 3,'colour' => '#ff0000','title' => 'Święta Bożego Narodzenia','time_from' => '2020-12-20','time_to' => '2020-12-28','price' => '450.00']);
	App\Season::create(['user_id' => 1,'seasongroup_id' => 3,'colour' => '#ffff26','title' => 'Sylwester','time_from' => '2020-12-28','time_to' => '2021-01-02','price' => '550.00']);
	App\Season::create(['user_id' => 1,'seasongroup_id' => 3,'colour' => '#ff0000','title' => 'Sezon Zimowy','time_from' => '2021-01-02','time_to' => '2021-01-08','price' => '250.00']);
	
	
	/////////// user 2 Domki u Bolka
	App\Season::create(['user_id' => 2,'seasongroup_id' => 4,'colour' => '#ff0000','title' => 'Sezon Letni', 'time_from' => '2020-06-19','time_to' => '2020-08-14','price' => '250.00']);	
	App\Season::create(['user_id' => 2,'seasongroup_id' => 4,'colour' => '#ffff26','title' => 'Weekend Sierpniowy','time_from' => '2020-08-14','time_to' => '2020-08-17','price' => '260.00']);
	App\Season::create(['user_id' => 2,'seasongroup_id' => 4,'colour' => '#ff0000','title' => 'Sezon Letni','time_from' => '2020-08-17','time_to' => '2020-09-01','price' => '350.00']);

	App\Season::create(['user_id' => 2,'seasongroup_id' => 4,'colour' => '#ff0000','title' => 'Sezon Jesienny','time_from' => '2020-09-01','time_to' => '2020-11-08','price' => '250.00']);
	App\Season::create(['user_id' => 2,'seasongroup_id' => 4,'colour' => '#ffff26','title' => 'Poza sezonem','time_from' => '2020-11-08','time_to' => '2020-11-11','price' => '210.00']);
	App\Season::create(['user_id' => 2,'seasongroup_id' => 4,'colour' => '#ff0000','title' => 'Poza sezonem','time_from' => '2020-11-11','time_to' => '2020-12-20','price' => '210.00']);

	App\Season::create(['user_id' => 2,'seasongroup_id' => 4,'colour' => '#ff0000','title' => 'Święta Bożego Narodzenia','time_from' => '2020-12-20','time_to' => '2020-12-28','price' => '450.00']);
	App\Season::create(['user_id' => 2,'seasongroup_id' => 4,'colour' => '#ffff26','title' => 'Sylwester','time_from' => '2020-12-28','time_to' => '2021-01-02','price' => '550.00']);
	App\Season::create(['user_id' => 2,'seasongroup_id' => 4,'colour' => '#ff0000','title' => 'Sezon Zimowy','time_from' => '2021-01-02','time_to' => '2021-01-08','price' => '250.00']);
	// Jadzia
	App\Season::create(['user_id' => 2,'seasongroup_id' => 5,'colour' => '#ff0000','title' => 'Sezon Letni', 'time_from' => '2020-06-19','time_to' => '2020-08-14','price' => '250.00']);	
	App\Season::create(['user_id' => 2,'seasongroup_id' => 5,'colour' => '#ffff26','title' => 'Weekend Sierpniowy','time_from' => '2020-08-14','time_to' => '2020-08-17','price' => '260.00']);
	App\Season::create(['user_id' => 2,'seasongroup_id' => 5,'colour' => '#ff0000','title' => 'Sezon Letni','time_from' => '2020-08-17','time_to' => '2020-09-01','price' => '350.00']);

	App\Season::create(['user_id' => 2,'seasongroup_id' => 5,'colour' => '#ff0000','title' => 'Sezon Jesienny','time_from' => '2020-09-01','time_to' => '2020-11-08','price' => '250.00']);
	App\Season::create(['user_id' => 2,'seasongroup_id' => 5,'colour' => '#ffff26','title' => 'Poza sezonem','time_from' => '2020-11-08','time_to' => '2020-11-11','price' => '210.00']);
	App\Season::create(['user_id' => 2,'seasongroup_id' => 5,'colour' => '#ff0000','title' => 'Poza sezonem','time_from' => '2020-11-11','time_to' => '2020-12-20','price' => '210.00']);

	App\Season::create(['user_id' => 2,'seasongroup_id' => 5,'colour' => '#ff0000','title' => 'Święta Bożego Narodzenia','time_from' => '2020-12-20','time_to' => '2020-12-28','price' => '450.00']);
	App\Season::create(['user_id' => 2,'seasongroup_id' => 5,'colour' => '#ff0000','colour' => '#ffff26','title' => 'Sylwester','time_from' => '2020-12-28','time_to' => '2021-01-02','price' => '550.00']);
	App\Season::create(['user_id' => 2,'seasongroup_id' => 5,'colour' => '#ff0000','title' => 'Sezon Zimowy','time_from' => '2021-01-02','time_to' => '2021-01-08','price' => '250.00']);
		
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('seasons');
    }
}
