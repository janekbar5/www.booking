<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bookings', function (Blueprint $table) {
            $table->increments('id');
			
			$table->integer('user_id')->unsigned(); //unsigned only positive val
			$table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
			
			$table->integer('customer_id')->unsigned(); //unsigned only positive val
			$table->foreign('customer_id')->references('id')->on('customers')->onDelete('cascade');
			
			$table->integer('property_id')->unsigned(); //unsigned only positive val
			$table->foreign('property_id')->references('id')->on('properties')->onDelete('cascade');
			
			$table->integer('status_id')->unsigned(); //unsigned only positive val
			$table->foreign('status_id')->references('id')->on('booking_statuses')->onDelete('cascade');
			
			$table->integer('payment_id')->unsigned(); //unsigned only positive val
			$table->foreign('payment_id')->references('id')->on('booking_payments')->onDelete('cascade');
			
			$table->string('uuid'); 
			
            $table->date('time_from'); 
			$table->date('time_to');
			$table->decimal('price', 10, 2); //DECIMAL equivalent with a precision and scale
			//$table->integer('customer_id');	//INTEGER equivalent to the table 
			//$table->integer('property_id');	//INTEGER equivalent to the table 
			//$table->date('created_at');
			//$table->dateTime('updated_at');
            $table->timestamps();
        });
		//App\Booking::create(['user_id' => 1,'customer_id' => 1,'property_id' => 1,'time_from' => '2019-09-04','time_to' => '2019-09-24','price' => 250]);
		//App\Booking::create(['user_id' => 1,'customer_id' => 1,'property_id' => 1,'time_from' => '2019-08-04','time_to' => '2019-08-24','price' => 250]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bookings');
    }
}
