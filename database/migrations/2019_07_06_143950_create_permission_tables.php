<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePermissionTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $tableNames = config('permission.table_names');
        $columnNames = config('permission.column_names');

        Schema::create($tableNames['permissions'], function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('guard_name');
            $table->timestamps();
        });

        Schema::create($tableNames['roles'], function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('guard_name');
            $table->timestamps();
        });

        Schema::create($tableNames['model_has_permissions'], function (Blueprint $table) use ($tableNames, $columnNames) {
            $table->unsignedInteger('permission_id');

            $table->string('model_type');
            $table->unsignedBigInteger($columnNames['model_morph_key']);
            $table->index([$columnNames['model_morph_key'], 'model_type', ]);

            $table->foreign('permission_id')
                ->references('id')
                ->on($tableNames['permissions'])
                ->onDelete('cascade');

            $table->primary(['permission_id', $columnNames['model_morph_key'], 'model_type'],
                    'model_has_permissions_permission_model_type_primary');
        });

        Schema::create($tableNames['model_has_roles'], function (Blueprint $table) use ($tableNames, $columnNames) {
            $table->unsignedInteger('role_id');

            $table->string('model_type');
            $table->unsignedBigInteger($columnNames['model_morph_key']);
            $table->index([$columnNames['model_morph_key'], 'model_type', ]);

            $table->foreign('role_id')
                ->references('id')
                ->on($tableNames['roles'])
                ->onDelete('cascade');

            $table->primary(['role_id', $columnNames['model_morph_key'], 'model_type'],
                    'model_has_roles_role_model_type_primary');
        });

        Schema::create($tableNames['role_has_permissions'], function (Blueprint $table) use ($tableNames) {
            $table->unsignedInteger('permission_id');
            $table->unsignedInteger('role_id');

            $table->foreign('permission_id')
                ->references('id')
                ->on($tableNames['permissions'])
                ->onDelete('cascade');

            $table->foreign('role_id')
                ->references('id')
                ->on($tableNames['roles'])
                ->onDelete('cascade');

            $table->primary(['permission_id', 'role_id']);
        });

        app('cache')
            ->store(config('permission.cache.store') != 'default' ? config('permission.cache.store') : null)
            ->forget(config('permission.cache.key'));
			
		///////////////////////////////
		DB::table('roles')->insert(['name' => 'Admin','guard_name' => 'web']);
		DB::table('roles')->insert(['name' => 'Writer','guard_name' => 'web']);
		/////////////////////////////
		DB::table('model_has_roles')->insert(['role_id' => 1,'model_type' => 'App\User','model_id' => 1]);
		DB::table('model_has_roles')->insert(['role_id' => 2,'model_type' => 'App\User','model_id' => 6]);
		DB::table('model_has_roles')->insert(['role_id' => 2,'model_type' => 'App\User','model_id' => 7]);
		
        ///////////////////////////roles 1-4
		DB::table('permissions')->insert(['name' => 'roles-list','guard_name' => 'web']);
		DB::table('permissions')->insert(['name' => 'roles-create','guard_name' => 'web']);
		DB::table('permissions')->insert(['name' => 'roles-edit','guard_name' => 'web']);
		DB::table('permissions')->insert(['name' => 'roles-delete','guard_name' => 'web']);
		///////////////////////////permissions 5-8
		DB::table('permissions')->insert(['name' => 'permissions-list','guard_name' => 'web']);
		DB::table('permissions')->insert(['name' => 'permissions-create','guard_name' => 'web']);
		DB::table('permissions')->insert(['name' => 'permissions-edit','guard_name' => 'web']);
		DB::table('permissions')->insert(['name' => 'permissions-delete','guard_name' => 'web']);
        ///////////////////////////users 9-12
		DB::table('permissions')->insert(['name' => 'users-list','guard_name' => 'web']);
		DB::table('permissions')->insert(['name' => 'users-create','guard_name' => 'web']);
		DB::table('permissions')->insert(['name' => 'users-edit','guard_name' => 'web']);
		DB::table('permissions')->insert(['name' => 'users-delete','guard_name' => 'web']);
        ///////////////////////////properties 13-16
		DB::table('permissions')->insert(['name' => 'properties-list','guard_name' => 'web']);
		DB::table('permissions')->insert(['name' => 'properties-create','guard_name' => 'web']);
		DB::table('permissions')->insert(['name' => 'properties-edit','guard_name' => 'web']);
		DB::table('permissions')->insert(['name' => 'properties-delete','guard_name' => 'web']);
		///////////////////////////seasons 17-20
		DB::table('permissions')->insert(['name' => 'seasons-list','guard_name' => 'web']);
		DB::table('permissions')->insert(['name' => 'seasons-create','guard_name' => 'web']);
		DB::table('permissions')->insert(['name' => 'seasons-edit','guard_name' => 'web']);
		DB::table('permissions')->insert(['name' => 'seasons-delete','guard_name' => 'web']);
		///////////////////////////seasongroups 21-24
		DB::table('permissions')->insert(['name' => 'seasongroups-list','guard_name' => 'web']);
		DB::table('permissions')->insert(['name' => 'seasongroups-create','guard_name' => 'web']);
		DB::table('permissions')->insert(['name' => 'seasongroups-edit','guard_name' => 'web']);
		DB::table('permissions')->insert(['name' => 'seasongroups-delete','guard_name' => 'web']);		
		///////////////////////////bookings 25-28		
		DB::table('permissions')->insert(['name' => 'bookings-list','guard_name' => 'web']);
		DB::table('permissions')->insert(['name' => 'bookings-create','guard_name' => 'web']);
		DB::table('permissions')->insert(['name' => 'bookings-edit','guard_name' => 'web']);
		DB::table('permissions')->insert(['name' => 'bookings-delete','guard_name' => 'web']);
		///////////////////////////inventories 29-32
		DB::table('permissions')->insert(['name' => 'inventories-list','guard_name' => 'web']);
		DB::table('permissions')->insert(['name' => 'inventories-create','guard_name' => 'web']);
		DB::table('permissions')->insert(['name' => 'inventories-edit','guard_name' => 'web']);
		DB::table('permissions')->insert(['name' => 'inventories-delete','guard_name' => 'web']);
		///////////////////////////customers 33-36
		DB::table('permissions')->insert(['name' => 'customers-list','guard_name' => 'web']);
		DB::table('permissions')->insert(['name' => 'customers-create','guard_name' => 'web']);
		DB::table('permissions')->insert(['name' => 'customers-edit','guard_name' => 'web']);
		DB::table('permissions')->insert(['name' => 'customers-delete','guard_name' => 'web']);



		//////////////////////////////////roles 1-4		
		DB::table('role_has_permissions')->insert(['permission_id' => 1,'role_id' => 1]);
		DB::table('role_has_permissions')->insert(['permission_id' => 2,'role_id' => 1]);
		DB::table('role_has_permissions')->insert(['permission_id' => 3,'role_id' => 1]);
		DB::table('role_has_permissions')->insert(['permission_id' => 4,'role_id' => 1]);
        ///////////////////////////permissions 5-8
		DB::table('role_has_permissions')->insert(['permission_id' => 5,'role_id' => 1]);	
		DB::table('role_has_permissions')->insert(['permission_id' => 6,'role_id' => 1]);
		DB::table('role_has_permissions')->insert(['permission_id' => 7,'role_id' => 1]);
		DB::table('role_has_permissions')->insert(['permission_id' => 8,'role_id' => 1]);
       ///////////////////////////users 9-12
		DB::table('role_has_permissions')->insert(['permission_id' => 9,'role_id' => 1]);		
		DB::table('role_has_permissions')->insert(['permission_id' => 10,'role_id' => 1]);
		DB::table('role_has_permissions')->insert(['permission_id' => 11,'role_id' => 1]);
		DB::table('role_has_permissions')->insert(['permission_id' => 12,'role_id' => 1]);
       ///////////////////////////properties 13-16
		DB::table('role_has_permissions')->insert(['permission_id' => 13,'role_id' => 1]);
		DB::table('role_has_permissions')->insert(['permission_id' => 14,'role_id' => 1]);		
		DB::table('role_has_permissions')->insert(['permission_id' => 15,'role_id' => 1]);
		DB::table('role_has_permissions')->insert(['permission_id' => 16,'role_id' => 1]);
		///////////////////////////seasons 17-20
		DB::table('role_has_permissions')->insert(['permission_id' => 17,'role_id' => 1]);	
		DB::table('role_has_permissions')->insert(['permission_id' => 18,'role_id' => 1]);
		DB::table('role_has_permissions')->insert(['permission_id' => 19,'role_id' => 1]);
		DB::table('role_has_permissions')->insert(['permission_id' => 20,'role_id' => 1]);
        ///////////////////////////seasongroups 21-24
		DB::table('role_has_permissions')->insert(['permission_id' => 21,'role_id' => 1]);		
		DB::table('role_has_permissions')->insert(['permission_id' => 22,'role_id' => 1]);
		DB::table('role_has_permissions')->insert(['permission_id' => 23,'role_id' => 1]);
		DB::table('role_has_permissions')->insert(['permission_id' => 24,'role_id' => 1]);
		///////////////////////////bookings 25-28
		DB::table('role_has_permissions')->insert(['permission_id' => 25,'role_id' => 1]);
		DB::table('role_has_permissions')->insert(['permission_id' => 26,'role_id' => 1]);
		DB::table('role_has_permissions')->insert(['permission_id' => 27,'role_id' => 1]);
		DB::table('role_has_permissions')->insert(['permission_id' => 28,'role_id' => 1]);
		///////////////////////////inventories 29-32
		DB::table('role_has_permissions')->insert(['permission_id' => 29,'role_id' => 1]);
		DB::table('role_has_permissions')->insert(['permission_id' => 30,'role_id' => 1]);
		DB::table('role_has_permissions')->insert(['permission_id' => 31,'role_id' => 1]);
		DB::table('role_has_permissions')->insert(['permission_id' => 32,'role_id' => 1]);
		///////////////////////////customers 33-36
		DB::table('role_has_permissions')->insert(['permission_id' => 33,'role_id' => 1]);
		DB::table('role_has_permissions')->insert(['permission_id' => 34,'role_id' => 1]);
		DB::table('role_has_permissions')->insert(['permission_id' => 35,'role_id' => 1]);
		DB::table('role_has_permissions')->insert(['permission_id' => 36,'role_id' => 1]);

		//customers
		// DB::table('role_has_permissions')->insert(['permission_id' => 37,'role_id' => 1]);
		// DB::table('role_has_permissions')->insert(['permission_id' => 38,'role_id' => 1]);
		// DB::table('role_has_permissions')->insert(['permission_id' => 39,'role_id' => 1]);
		// DB::table('role_has_permissions')->insert(['permission_id' => 40,'role_id' => 1]);

		//DB::table('role_has_permissions')->insert(['permission_id' => 1,'role_id' => 2]);
		DB::table('role_has_permissions')->insert(['permission_id' => 9,'role_id' => 2]);
		DB::table('role_has_permissions')->insert(['permission_id' => 5,'role_id' => 2]);
		DB::table('role_has_permissions')->insert(['permission_id' => 14,'role_id' => 2]);
		DB::table('role_has_permissions')->insert(['permission_id' => 21,'role_id' => 2]);
		DB::table('role_has_permissions')->insert(['permission_id' => 17,'role_id' => 2]);	
			
			
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $tableNames = config('permission.table_names');

        Schema::drop($tableNames['role_has_permissions']);
        Schema::drop($tableNames['model_has_roles']);
        Schema::drop($tableNames['model_has_permissions']);
        Schema::drop($tableNames['roles']);
        Schema::drop($tableNames['permissions']);
    }
}
