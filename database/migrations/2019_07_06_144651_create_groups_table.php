<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Group;

class CreateGroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('groups', function (Blueprint $table) {
            $table->increments('id');			
			$table->string('uuid');
			$table->integer('user_id')->unsigned(); //unsigned only positive val
			$table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');	
			
			$table->string('title'); //varchar	
			$table->text('description');
            $table->timestamps();
        });
		
		App\Group::create(['uuid' => (string) Str::uuid(),'user_id' => 1, 'title' => 'Domki u Edka','description' => 'costam ...']);
		App\Group::create(['uuid' => (string) Str::uuid(),'user_id' => 1, 'title' => 'Willa Anna','description' => 'costam ...']);
		App\Group::create(['uuid' => (string) Str::uuid(),'user_id' => 1, 'title' => 'Motel Poseidon','description' => 'costam ...']);
		//user 2
		App\Group::create(['uuid' => (string) Str::uuid(),'user_id' => 2, 'title' => 'Domki u Bolka','description' => 'costam ...']);
		App\Group::create(['uuid' => (string) Str::uuid(),'user_id' => 2, 'title' => 'Willa Jadzia','description' => 'costam ...']);
		
	
	
	
    }

	

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('groups');
    }
}
