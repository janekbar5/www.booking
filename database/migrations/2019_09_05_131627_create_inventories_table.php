<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Inventory;

class CreateInventoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inventories', function (Blueprint $table) {
            $table->increments('id');
			
			$table->integer('user_id')->unsigned(); //unsigned only positive val
			$table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');	
			
			$table->string('title'); //varchar
			
			$table->text('description')->nullable(); //TEXT equivalent to the table
            $table->timestamps();
        });
 
		App\Inventory::create(['user_id' => 1,'title' => 'Telewizor z płaskim ekranem','description' => '']);
		App\Inventory::create(['user_id' => 1,'title' => 'Kanały kablowe','description' => '']);
		App\Inventory::create(['user_id' => 1,'title' => 'Klimatyzacja','description' => '']);
		App\Inventory::create(['user_id' => 1,'title' => 'Sejf','description' => '']);
		App\Inventory::create(['user_id' => 1,'title' => 'Biurko','description' => '']);
		App\Inventory::create(['user_id' => 1,'title' => 'Szafa','description' => '']);
		App\Inventory::create(['user_id' => 1,'title' => 'Łazienka z prysznicem','description' => '']);
		App\Inventory::create(['user_id' => 1,'title' => 'Suszarka do włosów','description' => '']);
		App\Inventory::create(['user_id' => 1,'title' => 'Szlafrok','description' => '']);
		App\Inventory::create(['user_id' => 1,'title' => 'Zestaw kosmetyków','description' => '']);
		App\Inventory::create(['user_id' => 1,'title' => 'Kapcie','description' => '']);
		App\Inventory::create(['user_id' => 1,'title' => 'Ręczniki','description' => '']);
		App\Inventory::create(['user_id' => 1,'title' => 'Internet w pokoju','description' => '']);
		App\Inventory::create(['user_id' => 1,'title' => 'Lodówka','description' => '']);
		App\Inventory::create(['user_id' => 1,'title' => 'TV','description' => '']);
		App\Inventory::create(['user_id' => 1,'title' => 'Kominek','description' => '']);
		
		

		Schema::create('property_inventory', function (Blueprint $table) {
			
            //$table->increments('id');	if on error	Multiple primary key defined	
			$table->integer('property_id')->unsigned(); //unsigned only positive val
			$table->foreign('property_id')->references('id')->on('properties')->onDelete('cascade');
			$table->primary(['property_id','inventory_id']); //prevent repeating (1,1  1,2  1,3  1,1)
			
			$table->integer('inventory_id')->unsigned();											
			$table->foreign('inventory_id')->references('id')->on('inventories')->onDelete('cascade');
            $table->timestamps();
		});
		
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inventories');
		Schema::dropIfExists('property_inventory');
    }
}
