<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customers', function (Blueprint $table) {
            $table->increments('id');
			$table->integer('user_id')->unsigned(); //unsigned only positive val
			$table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
			
			$table->string('first_name_t'); //varchar
			$table->string('last_name_t'); //varchar	
			$table->string('phone_t'); //varchar	
			$table->string('email_t'); //varchar			
            $table->timestamps();			
			
        });
		
		/*App\Customer::create(['user_id' => 1,'first_name' => 'Alicja','last_name' => 'Barska']);
		App\Customer::create(['user_id' => 1,'first_name' => 'Marian','last_name' => 'Bonek']);
		App\Customer::create(['user_id' => 1,'first_name' => 'Andzej','last_name' => 'Ucho']);
		App\Customer::create(['user_id' => 1,'first_name' => 'Leon','last_name' => 'Byk']);
		App\Customer::create(['user_id' => 1,'first_name' => 'Marian','last_name' => 'Ucho']);
		App\Customer::create(['user_id' => 1,'first_name' => 'Artur','last_name' => 'Dym']);
		App\Customer::create(['user_id' => 1,'first_name' => 'Jadwiga','last_name' => 'Wig']);
		App\Customer::create(['user_id' => 1,'first_name' => 'Borek','last_name' => 'Duck']);
		App\Customer::create(['user_id' => 1,'first_name' => 'Andzej','last_name' => 'Kij']);
		App\Customer::create(['user_id' => 1,'first_name' => 'Jarek','last_name' => 'Wasyl']);
		
		App\Customer::create(['user_id' => 2,'first_name' => 'Andzej','last_name' => 'Ucho']);
		App\Customer::create(['user_id' => 2,'first_name' => 'Mariusz','last_name' => 'Bobek']);
		App\Customer::create(['user_id' => 2,'first_name' => 'Andzej','last_name' => 'Ucho']);
		App\Customer::create(['user_id' => 2,'first_name' => 'Antoni','last_name' => 'Pata']);*/
		
		
    }

/**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customers');
    }
}
