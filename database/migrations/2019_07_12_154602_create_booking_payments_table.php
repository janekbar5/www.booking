<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookingPaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('booking_payments', function (Blueprint $table) {
            $table->increments('id');
            
            $table->integer('user_id')->unsigned(); //unsigned only positive val
			$table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            
			$table->string('title'); //varchar
			$table->string('value'); //varchar
            $table->timestamps();
        });
		
		App\BookingPayment::create(['user_id' => 1,'title' => 'Paid 20%','value' => '20']);	
		App\BookingPayment::create(['user_id' => 1,'title' => 'Paid 50%','value' => '50']);
		App\BookingPayment::create(['user_id' => 1,'title' => 'Paid 100%','value' => '100']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('booking_payments');
    }
}
