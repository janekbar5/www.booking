<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

use App\User;

class CreateUsersTable extends Migration

{

    /**
     * Run the migrations.
     *
     * @return void
     */

    public function up()

    {

        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email');
            $table->string('password');			
			$table->timestamp('email_verified_at')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
		
	
			App\User::create(['name' => 'jan 1','email' => 'janekbar5@interia.pl','password' => bcrypt('janekbar5@interia.pl'),'email_verified_at'=>'2019-09-17 00:00:00' ]);
		    App\User::create(['name' => 'jan 2','email' => 'janekbar5@gmx.com','password' => bcrypt('janekbar5@gmx.com'),'email_verified_at'=>'2019-09-17 00:00:00']);
			App\User::create(['name' => 'jan 3','email' => 'janekbarski@wp.pl','password' => bcrypt('janekbarski@wp.pl'),'email_verified_at'=>'2019-09-17 00:00:00']);
			App\User::create(['name' => 'Alicja','email' => 'alicjabarska1@gmail.com','password' => bcrypt('alicjabarska1@gmail.com'),'email_verified_at'=>'2019-09-17 00:00:00']);

	
	}



    /**

     * Reverse the migrations.

     *

     * @return void

     */

    public function down()
    {
        Schema::dropIfExists('users');
    }

}
