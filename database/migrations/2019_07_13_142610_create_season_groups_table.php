<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSeasonGroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('season_groups', function (Blueprint $table) {
            
			$table->increments('id');
			$table->integer('user_id')->unsigned(); //unsigned only positive val
			$table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
			
			$table->string('title'); //varchar
			$table->text('description');	//TEXT equivalent to the table 
            $table->timestamps();
        });
		
			
	App\SeasonGroup::create(['user_id' => 1,'title' => 'Sezon dla Domkow u Edka','description' => 'sdfgsgewr']);
	App\SeasonGroup::create(['user_id' => 1,'title' => 'Sezon dla Willi Anna','description' => 'sgdesrf erheh']);
	App\SeasonGroup::create(['user_id' => 1,'title' => 'Sezon dla Motelu Poseidon','description' => 'sederg srgerghy']);
	
	App\SeasonGroup::create(['user_id' => 2,'title' => 'Sezon dla Domki u Bolka','description' => 'sederg srgerghy']);
	App\SeasonGroup::create(['user_id' => 2,'title' => 'Sezon dla Willa Jadzia','description' => 'sederg srgerghy']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('season_groups');
    }
}
