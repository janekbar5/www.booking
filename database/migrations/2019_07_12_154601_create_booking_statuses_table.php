<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookingStatusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('booking_statuses', function (Blueprint $table) {
            $table->increments('id');
			
			$table->integer('user_id')->unsigned(); //unsigned only positive val
			$table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');	
			
			$table->string('title'); //varchar
			$table->string('colour'); //varchar
            $table->timestamps();
        });
		
		
		App\BookingStatus::create(['user_id' => 1,'title' => 'Expired (not confirmed in time)', 'colour' => '#f20a26']);	
		App\BookingStatus::create(['user_id' => 1,'title' => 'New','colour' => '#1cf20a']);
		App\BookingStatus::create(['user_id' => 1,'title' => 'Late arrival','colour' => '#d2f20a']);
		App\BookingStatus::create(['user_id' => 1,'title' => 'Confirmed','colour' => '#d108bd']);
	
	
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('booking_statuses');
    }
}
