<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdvantagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('advantages', function (Blueprint $table) {
            $table->increments('id');			
			$table->integer('user_id')->unsigned(); //unsigned only positive val
			$table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');				
			$table->string('title'); //varchar			
			$table->text('description'); //TEXT equivalent to the table
            $table->timestamps();
        });
		
		App\Advantage::create(['user_id' => 1,'title' => 'Balkon','description' => '']);
		App\Advantage::create(['user_id' => 1,'title' => 'Widok na gory','description' => '']);
		App\Advantage::create(['user_id' => 1,'title' => 'Silownia','description' => '']);
		App\Advantage::create(['user_id' => 1,'title' => 'Spa','description' => '']);
		App\Advantage::create(['user_id' => 1,'title' => 'Jacuzzi','description' => '']);
		App\Advantage::create(['user_id' => 1,'title' => 'Gym','description' => '']);
		
		Schema::create('property_advantage', function (Blueprint $table) {
			
            //$table->increments('id');	if on error	Multiple primary key defined	
			$table->integer('property_id')->unsigned(); //unsigned only positive val
			$table->foreign('property_id')->references('id')->on('properties')->onDelete('cascade');
			$table->primary(['property_id','advantage_id']); //prevent repeating (1,1  1,2  1,3  1,1)
			
			$table->integer('advantage_id')->unsigned();											
			$table->foreign('advantage_id')->references('id')->on('advantages')->onDelete('cascade');
			
			
            $table->timestamps();
		});
		
		
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('advantages');
		Schema::dropIfExists('property_advantage');
    }
}
