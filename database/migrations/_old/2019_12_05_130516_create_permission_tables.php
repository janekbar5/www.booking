<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePermissionTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $tableNames = config('permission.table_names');
        $columnNames = config('permission.column_names');

        Schema::create($tableNames['permissions'], function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('guard_name');
            $table->timestamps();
        });

        Schema::create($tableNames['roles'], function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('guard_name');
            $table->timestamps();
        });

        Schema::create($tableNames['model_has_permissions'], function (Blueprint $table) use ($tableNames, $columnNames) {
            $table->unsignedInteger('permission_id');

            $table->string('model_type');
            $table->unsignedBigInteger($columnNames['model_morph_key']);
            $table->index([$columnNames['model_morph_key'], 'model_type', ], 'model_has_permissions_model_id_model_type_index');

            $table->foreign('permission_id')
                ->references('id')
                ->on($tableNames['permissions'])
                ->onDelete('cascade');

            $table->primary(['permission_id', $columnNames['model_morph_key'], 'model_type'],
                    'model_has_permissions_permission_model_type_primary');
        });

        Schema::create($tableNames['model_has_roles'], function (Blueprint $table) use ($tableNames, $columnNames) {
            $table->unsignedInteger('role_id');

            $table->string('model_type');
            $table->unsignedBigInteger($columnNames['model_morph_key']);
            $table->index([$columnNames['model_morph_key'], 'model_type', ], 'model_has_roles_model_id_model_type_index');

            $table->foreign('role_id')
                ->references('id')
                ->on($tableNames['roles'])
                ->onDelete('cascade');

            $table->primary(['role_id', $columnNames['model_morph_key'], 'model_type'],
                    'model_has_roles_role_model_type_primary');
        });

        Schema::create($tableNames['role_has_permissions'], function (Blueprint $table) use ($tableNames) {
            $table->unsignedInteger('permission_id');
            $table->unsignedInteger('role_id');

            $table->foreign('permission_id')
                ->references('id')
                ->on($tableNames['permissions'])
                ->onDelete('cascade');

            $table->foreign('role_id')
                ->references('id')
                ->on($tableNames['roles'])
                ->onDelete('cascade');

            $table->primary(['permission_id', 'role_id'], 'role_has_permissions_permission_id_role_id_primary');
        });

        app('cache')
            ->store(config('permission.cache.store') != 'default' ? config('permission.cache.store') : null)
            ->forget(config('permission.cache.key'));
		/////////////////////////////////////////////////
		///////////////////////////////
		DB::table('roles')->insert(['name' => 'Admin','guard_name' => 'web']);
		DB::table('roles')->insert(['name' => 'Writer','guard_name' => 'web']);
		/////////////////////////////
		DB::table('model_has_roles')->insert(['role_id' => 1,'model_type' => 'App\User','model_id' => 1]); //Admin
		DB::table('model_has_roles')->insert(['role_id' => 2,'model_type' => 'App\User','model_id' => 2]); //writer
		////////////////////////////permissions
		DB::table('permissions')->insert(['name' => 'role-list','guard_name' => 'web']);
		DB::table('permissions')->insert(['name' => 'role-create','guard_name' => 'web']);
		DB::table('permissions')->insert(['name' => 'role-edit','guard_name' => 'web']);
		DB::table('permissions')->insert(['name' => 'role-delete','guard_name' => 'web']);
		
		DB::table('permissions')->insert(['name' => 'user-list','guard_name' => 'web']);
		DB::table('permissions')->insert(['name' => 'user-create','guard_name' => 'web']);
		DB::table('permissions')->insert(['name' => 'user-edit','guard_name' => 'web']);
		DB::table('permissions')->insert(['name' => 'user-delete','guard_name' => 'web']);
		
		DB::table('permissions')->insert(['name' => 'permission-list','guard_name' => 'web']);
		DB::table('permissions')->insert(['name' => 'permission-create','guard_name' => 'web']);
		DB::table('permissions')->insert(['name' => 'permission-edit','guard_name' => 'web']);
		DB::table('permissions')->insert(['name' => 'permission-delete','guard_name' => 'web']);
		///////////////////////////role->perm
		DB::table('role_has_permissions')->insert(['permission_id' => 1,'role_id' => 1]);
		DB::table('role_has_permissions')->insert(['permission_id' => 2,'role_id' => 1]);
		DB::table('role_has_permissions')->insert(['permission_id' => 3,'role_id' => 1]);
		DB::table('role_has_permissions')->insert(['permission_id' => 4,'role_id' => 1]);
		
		DB::table('role_has_permissions')->insert(['permission_id' => 5,'role_id' => 1]);
		DB::table('role_has_permissions')->insert(['permission_id' => 6,'role_id' => 1]);
		DB::table('role_has_permissions')->insert(['permission_id' => 7,'role_id' => 1]);
		DB::table('role_has_permissions')->insert(['permission_id' => 8,'role_id' => 1]);
		
		DB::table('role_has_permissions')->insert(['permission_id' => 9,'role_id' => 1]);
		DB::table('role_has_permissions')->insert(['permission_id' => 10,'role_id' => 1]);
		DB::table('role_has_permissions')->insert(['permission_id' => 11,'role_id' => 1]);
		DB::table('role_has_permissions')->insert(['permission_id' => 12,'role_id' => 1]);
		
		
		DB::table('role_has_permissions')->insert(['permission_id' => 1,'role_id' => 2]); //list only
		DB::table('role_has_permissions')->insert(['permission_id' => 5,'role_id' => 2]); //list only
		DB::table('role_has_permissions')->insert(['permission_id' => 9,'role_id' => 2]); //list only
			
			
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $tableNames = config('permission.table_names');

        Schema::drop($tableNames['role_has_permissions']);
        Schema::drop($tableNames['model_has_roles']);
        Schema::drop($tableNames['model_has_permissions']);
        Schema::drop($tableNames['roles']);
        Schema::drop($tableNames['permissions']);
    }
}
