<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(PropertiesTableSeeder::class);
		$this->call(ProductsTableSeeder::class);
		$this->call(InvoicesTableSeeder::class);
        $this->call(CustomersTableSeeder::class);
        $this->call(AddressesTableSeeder::class);
    }
}
