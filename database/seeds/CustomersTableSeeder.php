<?php

use Illuminate\Database\Seeder;

class CustomersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create();
		for($i=1; $i<=80; $i++):
            DB::table('customers')
                ->insert([
                    'user_id' => 1, 
					'first_name_t' => $faker->firstName,
					'last_name_t' => $faker->lastName,
                    'phone_t' => $faker->phoneNumber,
                    'email_t' => $faker->unique()->email,

                ]);
        endfor;
		
		/*for($i=0; $i<=50; $i++):
            DB::table('customers')
                ->insert([
                    'user_id' => 2, 
					'first_name' => $faker->firstName,
					'last_name' => $faker->lastName,					
                ]);
        endfor;*/
		
    }
}
