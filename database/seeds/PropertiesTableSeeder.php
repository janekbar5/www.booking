<?php

use Illuminate\Database\Seeder;

class PropertiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create();
		//$properties = ['Domki u Edka', 'Willa Anna ','Hotel Demeter'];
		$prices = [200,210,250,270,290,300,310];

		////////////////////////////////////////////////////////////////////////////////////////user 1
        for($i=0; $i<=20; $i++):
            DB::table('properties')
                ->insert([
                    'user_id' => 1,
                    'group_id' => 1,
					'seasongroup_id' => 1,
					//'title' => $faker->name,
					'title' => "Domki u Edka ".$i,
					'description' => $faker->text,					
					'area' => $faker->numberBetween($min = 50, $max = 100),
					'room_nubr' => $faker->numberBetween($min = 1, $max = 6),
					'max_pers' => $faker->numberBetween($min = 2, $max = 3),
					'price' => $faker->randomElement($prices),
                ]);
        endfor;
		
		
		
		
		for($i=0; $i<=20; $i++):
            DB::table('properties')
                ->insert([
                    'user_id' => 1,
                    'group_id' => 2,
					'seasongroup_id' => 2,
					//'title' => $faker->name,
					'title' => "Willa Anna ".$i,
					'description' => $faker->text,					
					'area' => $faker->numberBetween($min = 50, $max = 100),
					'room_nubr' => $faker->numberBetween($min = 1, $max = 6),
					'max_pers' => $faker->numberBetween($min = 2, $max = 3),
					'price' => $faker->randomElement($prices),
                ]);
        endfor;
		
		for($i=0; $i<=20; $i++):
            DB::table('properties')
                ->insert([
                    'user_id' => 1,
                    'group_id' => 3,
					'seasongroup_id' => 3,
					//'title' => $faker->name,
					'title' => "Hotel Demeter ".$i,
					'description' => $faker->text,					
					'area' => $faker->numberBetween($min = 50, $max = 100),
					'room_nubr' => $faker->numberBetween($min = 1, $max = 6),
					'max_pers' => $faker->numberBetween($min = 2, $max = 3),
					'price' => $faker->randomElement($prices),
                ]);
        endfor;
		
		////////////////////////////////////////////////////////////////////////////////////////user 2
		for($i=0; $i<=20; $i++):
            DB::table('properties')
                ->insert([
                    'user_id' => 2,
                    'group_id' => 4,
					'seasongroup_id' => 4,
					//'title' => $faker->name,
					'title' => "Domki u Bolka ".$i,
					'description' => $faker->text,					
					'area' => $faker->numberBetween($min = 50, $max = 100),
					'room_nubr' => $faker->numberBetween($min = 1, $max = 6),
					'max_pers' => $faker->numberBetween($min = 2, $max = 3),
					'price' => $faker->randomElement($prices),
                ]);
        endfor;
		
		
		
		
		for($i=0; $i<=20; $i++):
            DB::table('properties')
                ->insert([
                    'user_id' => 2,
                    'group_id' => 5,
					'seasongroup_id' => 5,
					//'title' => $faker->name,
					'title' => "Willa Jadzia ".$i,
					'description' => $faker->text,					
					'area' => $faker->numberBetween($min = 50, $max = 100),
					'room_nubr' => $faker->numberBetween($min = 1, $max = 6),
					'max_pers' => $faker->numberBetween($min = 2, $max = 3),
					'price' => $faker->randomElement($prices),
                ]);
        endfor;
		
		
    }
}
