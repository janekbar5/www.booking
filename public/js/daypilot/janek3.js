

if ("undefined" == typeof DayPilot.Global && (DayPilot.Global = {}), function() {
        "undefined" == typeof DayPilot.DatePicker && (DayPilot.DatePicker = function(e) {
            this.v = "2018.1.3151";
            var t = "navigator_" + (new Date).getTime(),
                i = this;
            this.onShow = null, this.onTimeRangeSelect = null, this.onTimeRangeSelected = null, this.prepare = function() {
                if (this.locale = "en-us", this.target = null, this.resetTarget = !0, this.pattern = this.zc.locale().datePattern, this.theme = null, this.patterns = [], e)
                    for (var t in e) this[t] = e[t];
                this.init()
            }, this.init = function() {
                this.date = new DayPilot.Date(this.date);
                var e = this.Bd();
                this.resetTarget && !e && this.Cd(this.date);
                var t = this.Dd();
                return t && DayPilot.re(t, "input", function() {
                    i.date = i.Bd()
                }), DayPilot.re(document, "mousedown", function() {
                    i.close()
                }), this
            }, this.close = function() {
                this.Vc && (this.navigator && this.navigator.dispose(), this.div.innerHTML = "", this.div && this.div.parentNode === document.body && document.body.removeChild(this.div))
            }, this.setDate = function(e) {
                this.date = new DayPilot.Date(e), this.Cd(this.date)
            }, this.Bd = function() {
                var e = this.Dd();
                if (!e) return this.date;
                var t = null;
                if (t = "INPUT" === e.tagName ? e.value : e.innerText, !t) return null;
                for (var n = DayPilot.Date.parse(t, i.pattern), a = 0; a < i.patterns.length; a++) {
                    if (n) return n;
                    n = DayPilot.Date.parse(t, i.patterns[a])
                }
                return n
            }, this.Cd = function(e) {
                var t = this.Dd();
                if (t) {
                    var n = e.toString(i.pattern, i.locale);
                    "INPUT" === t.tagName ? t.value = n : t.innerHTML = n
                }
            }, this.zc = {}, this.zc.locale = function() {
                return DayPilot.Locale.find(i.locale)
            }, this.Dd = function() {
                var e = this.target;
                return e && e.nodeType && 1 === e.nodeType ? e : document.getElementById(e)
            }, this.show = function() {
                var e = this.Dd(),
                    n = this.navigator,
                    n = new DayPilot.Navigator(t);
                n.api = 2, n.cssOnly = !0, n.theme = i.theme, n.weekStarts = "Auto", n.locale = i.locale, n.onTimeRangeSelected = function(e) {
                    i.date = e.start;
                    var t = e.start.addTime(n.Ed),
                        a = t.toString(i.pattern, i.locale),
                        e = {};
                    e.start = t, e.date = t, e.preventDefault = function() {
                        this.preventDefault.value = !0
                    }, "function" == typeof i.onTimeRangeSelect && (i.onTimeRangeSelect(e), e.preventDefault.value) || (i.Cd(a), i.close(), "function" == typeof i.onTimeRangeSelected && i.onTimeRangeSelected(e))
                }, this.navigator = n;
                var a = DayPilot.abs(e),
                    o = e.offsetHeight,
                    r = document.createElement("div");
                r.style.position = "absolute", r.style.left = a.x + "px", r.style.top = a.y + o + "px";
                var l = document.createElement("div");
                l.id = t, r.appendChild(l), DayPilot.re(r, "mousedown", function(e) {
                    var e = e || window.event;
                    e.cancelBubble = !0, e.stopPropagation && e.stopPropagation()
                }), document.body.appendChild(r), this.div = r;
                var s = i.Bd() || (new DayPilot.Date).getDatePart();
                n.startDate = s, n.Ed = s.getTimePart(), n.selectionStart = s.getDatePart(), n.init(), this.Vc = !0, this.onShow && this.onShow()
            }, this.prepare()
        })
    }(), 
    
    
"undefined" == typeof DayPilot) var DayPilot = {};

if ("undefined" == typeof DayPilot.Global && (DayPilot.Global = {}), function() {
        if ("undefined" == typeof DayPilot.Gantt) {
            var e = function() {};
            DayPilot.Gantt = function(t, i) {
                    this.v = "2018.1.3151";
                    var n = this;
                    this.id = t, this.isGantt = !0;
                    var a = new DayPilot.Scheduler(t);
                    this.scheduler = a, a.viewType = "Resources", a.onCallBackHeader = function(e) {
                        e.header.taskGroupMode = n.taskGroupMode, e.header.rowHeaderColumns = n.columns, e.header.clientState = n.clientState
                    }, a.onGetNodeState = function(e) {
                        var t = function(e) {
                                var t = {};
                                if (e.tags)
                                    for (var i in e.tags) t[i] = "" + e.tags[i];
                                return t
                            },
                            i = function(e) {
                                var i = e.task,
                                    n = {};
                                return n.start = i.start, n.end = i.end, n.id = i.id, n.complete = i.complete, n.text = i.text, n.type = i.type, n.expanded = e.expanded, n.loaded = e.loaded, n.tags = t(i), n.versions = i.versions, n.children = a.internal.getNodeChildren(e.children), n
                            };
                        e.result = i(e.row), e.preventDefault()
                    }, a.Fd = function(e) {}, a.onCallBackResult = function(e) {
                        var t = e.result;
                        e.preventDefault();
                        var i = function() {
                            t.scrollToTaskId && a.scrollToResource(t.scrollToTaskId)
                        };
                        if ("None" === t.updateType) return void i();
                        var o = function(e) {
                            for (var i = 0; i < e.length; i++) {
                                var a = e[i];
                                "undefined" != typeof t[a] && (n[a] = t[a])
                            }
                        };
                        n.links.list = t.links, n.tasks.list = t.tasks, n.startDate = new DayPilot.Date(t.startDate), o(["days", "cellDuration", "cellGroupBy", "cellWidth", "cellWidthSpec", "cornerHtml", "separators", "rowMinHeight", "rowMarginBottom", "taskGroupMode", "selectedRows"]), o(["cellProperties", "cellConfig", "timeHeader", "timeHeaders", "timeline", "columns"]), l.translate(), l.Gd(), l.Hd(), a.update(), a.show(), i()
                    }, this.taskGroupMode = "Auto", this.autoRefreshCommand = "refresh", this.autoRefreshEnabled = !1, this.autoRefreshInterval = 60, this.autoRefreshMaxCount = 20, this.autoScroll = "Drag", "function" == typeof DayPilot.Bubble ? (this.bubbleTask = new DayPilot.Bubble, this.bubbleCell = new DayPilot.Bubble, this.bubbleRow = new DayPilot.Bubble) : (this.bubbleTask = null, this.bubbleCell = null, this.bubbleRow = null), this.cellDuration = 1440, this.cellGroupBy = "Month", this.cellWidth = 40, this.cellWidthSpec = "Fixed", this.completeBarVisible = !0, this.completeBarHeight = 3, this.contextMenuTask = null, this.contextMenuRow = null, this.contextMenuLink = null, this.cornerHtml = "", this.crosshairColor = "Gray", this.crosshairOpacity = 20, this.crosshairType = "Header", this.doubleClickTimeout = 300, this.progressiveTaskRendering = "Progressive", this.progressiveTaskRenderingMargin = 500, this.progressiveTaskRenderingCacheSweeping = !1, this.progressiveTaskRenderingCacheSize = 200, this.floatingTasks = !0, this.floatingTimeHeaders = !0, this.headerHeight = 20, this.height = 300, this.heightSpec = "Max", this.hideUntilInit = !1, this.linkBottomMargin = 10, this.linkPointSize = 10, this.loadingLabelVisible = !0, this.loadingLabelText = "Loading...", this.locale = "en-us", this.messageBarPosition = "Top", this.messageHideAfter = 5e3, this.progressiveRowRendering = !0, this.progressiveRowRenderingPreload = 25, this.rowHeaderScrolling = !1, this.rowHeaderSplitterWidth = 3, this.rowHeaderHideIconEnabled = !0, this.rowHeaderWidth = 80, this.rowHeaderWidthAutoFit = !0, this.rowMarginBottom = 4, this.rowMinHeight = 0, this.scrollDelayTasks = 200, this.scrollDelayCells = 20, this.scrollDelayFloats = 0, this.scale = "Day", this.selectedRows = [], this.snapToGrid = !0, this.syncTasks = !0, this.syncLinks = !0, this.tapAndHoldTimeout = 300, this.taskHeight = 24, this.taskHtmlLeftMargin = 20, this.taskHtmlRightMargin = 20, this.taskResizeMargin = 5, this.taskMovingStartEndEnabled = !1, this.taskMovingStartEndFormat = "MMMM d, yyyy", this.taskResizingStartEndEnabled = !1, this.taskResizingStartEndFormat = "MMMM d, yyyy", this.theme = "gantt_default", this.treeAutoExpand = !0, this.treeIndent = 20, this.treeImageMarginLeft = 5, this.treeImageMarginTop = 5, this.timeline = null, this.timeHeaders = [{
                        "groupBy": "Month",
                        "format": "MMMM yyyy"
                    }, {
                        "groupBy": "Day",
                        "format": "d"
                    }], this.useEventBoxes = "Never", this.visible = !0, this.taskVersionsEnabled = !1, this.taskVersionHeight = 24, this.taskVersionMargin = 2, this.taskVersionPosition = "Above", this.taskMoveHandling = "Update", this.taskClickHandling = "Enabled", this.taskResizeHandling = "Update", this.linkCreateHandling = "Update", this.taskRightClickHandling = "ContextMenu", this.taskDoubleClickHandling = "Disabled", this.tasksLoadMethod = "GET", this.rowCreateHandling = "Disabled", this.rowMoveHandling = "Update", this.rowClickHandling = "Disabled", this.rowDoubleClickHandling = "Disabled", this.rowEditHandling = "Update", this.rowSelectHandling = "Update", this.clientState = {}, this.separators = [], this.members = {}, this.members.obsolete = [], this.members.ignore = ["members", "scheduler", "internal", "cellProperties"], this.members.noCssOnly = [], this.links = {}, this.links.list = [], this.links.add = function(e) {
                        if (e) {
                            var t = e.isLink ? e.data : e;
                            n.links.list.push(t), n.A && (l.Hd(), a.update()), n.kd.notify()
                        }
                    }, this.links.remove = function(e) {
                        if (e) {
                            var t;
                            t = e.isLink ? e.data : e;
                            var i = DayPilot.indexOf(n.links.list, t);
                            i !== -1 && (n.links.list.splice(i, 1), n.A && (l.Hd(), a.update()), n.kd.notify())
                        }
                    }, this.links.find = function(e) {
                        if (!DayPilot.isArray(n.links.list)) return null;
                        for (var t = 0; t < n.links.list.length; t++) {
                            var i = n.links.list[t];
                            if (i.id === e) return new DayPilot.Link(i, n)
                        }
                        return null
                    }, this.links.findFromTo = function(e, t) {
                        if (!DayPilot.isArray(n.links.list)) return null;
                        for (var i = 0; i < n.links.list.length; i++) {
                            var a = n.links.list[i];
                            if (a.from === e && a.to === t) return new DayPilot.Link(a, n)
                        }
                        return null
                    }, this.links.load = function(e, t, i) {
                        a.links.load(e, function(e) {
                            "function" == typeof t && (t(e), e.preventDefault.value) || (n.links.list = e.data)
                        }, i, {
                            "dontAddStartEnd": !0
                        })
                    }, this.Id = {};
                    var o = this.Id;
                    o.timeout = null, o.update = function() {
                        n.A && (window.clearTimeout(o.timeout), o.timeout = setTimeout(function() {
                            l.Gd(), a.update()
                        }, 0))
                    }, this.tasks = {}, this.tasks.list = [], this.tasks.add = function(e) {
                        if (e) {
                            if (e instanceof DayPilot.Event) throw "DayPilot.Task object required. You have supplied DayPilot.Event.";
                            var t = e.isTask ? e.data : e;
                            n.tasks.list.push(t), o.update(), n.kd.notify()
                        }
                    }, this.tasks.find = function(e) {
                        var t = r.findInCache(e);
                        return t ? new DayPilot.Task(t, n) : null
                    }, this.tasks.update = function(e) {
                        if (e) {
                            if (!e.isTask) throw "DayPilot.Task object expected";
                            e.commit(), o.update(), n.kd.notify()
                        }
                    }, this.tasks.remove = function(e) {
                        if (e) {
                            if (!e.isTask) throw "DayPilot.Task object expected";
                            var t = r.findParentArray(e.data);
                            if (t) {
                                var i = DayPilot.indexOf(t, e.data);
                                t.splice(i, 1), o.update(), n.kd.notify()
                            }
                        }
                    }, this.tasks.load = function(e, t, i) {
                        if (!e) throw new DayPilot.Exception("events.load(): 'url' parameter required");
                        var a = function(e) {
                                var t = {};
                                t.exception = e.exception, t.request = e.request, "function" == typeof i && i(t)
                            },
                            o = function(e) {
                                var i, o = e.request;
                                try {
                                    i = JSON.parse(o.responseText)
                                } catch (e) {
                                    var r = {};
                                    return r.exception = e, void a(r)
                                }
                                if (DayPilot.isArray(i)) {
                                    var l = {};
                                    if (l.preventDefault = function() {
                                            this.preventDefault.value = !0
                                        }, l.data = i, "function" == typeof t && t(l), l.preventDefault.value) return;
                                    n.tasks.list = i, n.A && n.update()
                                }
                            };
                        if (n.tasksLoadMethod && "POST" === n.tasksLoadMethod.toUpperCase()) DayPilot.ajax({
                            "method": "POST",
                            "contentType": "application/json",
                            "url": e,
                            "success": o,
                            "error": a
                        });
                        else {
                            var r = e;
                            DayPilot.ajax({
                                "method": "GET",
                                "url": r,
                                "success": o,
                                "error": a
                            })
                        }
                    }, this.visibleStart = function() {
                        return a.visibleStart()
                    }, this.visibleEnd = function() {
                        return a.visibleEnd()
                    }, this.onAfterRender = null, this.onAfterUpdate = null, this.onBeforeRowHeaderRender = null, this.onBeforeTaskRender = null, this.onBeforeTimeHeaderRender = null, this.onBeforeCellRender = null, this.onTaskClick = null, this.onTaskClicked = null, this.onTaskDoubleClick = null, this.onTaskDoubleClicked = null, this.onTaskRightClick = null, this.onTaskRightClicked = null, this.onRowCreate = null, this.onRowCreated = null, this.onRowMove = null, this.onRowMoved = null, this.onRowMoving = null, this.onRowClick = null, this.onRowClicked = null, this.onRowDoubleClick = null, this.onRowDoubleClicked = null, this.onRowEdit = null, this.onRowEdited = null, this.onRowSelect = null, this.onRowSelected = null, this.onTaskMove = null, this.onTaskMoved = null, this.onTaskMoving = null, this.onTaskResize = null, this.onTaskResized = null, this.onTaskResizing = null, this.onLinkCreate = null, this.onLinkCreated = null, this.r = function() {
                        return !!this.backendUrl || !("function" != typeof WebForm_DoCallback || !this.uniqueID)
                    }, this.Jd = function() {
                        if ("string" == typeof n.startDate && (n.startDate = new DayPilot.Date(n.startDate)), !n.startDate || !n.days) {
                            for (var e = DayPilot.Date.today(), t = e.addDays(1), i = null, o = null, r = 0; r < n.tasks.list.length; r++) {
                                var l = n.tasks.list[r];
                                e = new DayPilot.Date(l.start), t = new DayPilot.Date(l.end), (null === i || e.getTime() < i) && (i = e.getTime()), (null === o || t.getTime() > o) && (o = t.getTime())
                            }
                            i && o ? (e = new DayPilot.Date(i).getDatePart(), t = new DayPilot.Date(o).getDatePart().addDays(1), a.startDate = n.startDate || e, a.days = n.days || DayPilot.DateUtil.daysDiff(e, t)) : (a.startDate = e, a.days = 30)
                        }
                    }, this.commandCallBack = function(e, t) {
                        l.translate(), a.commandCallBack(e, t)
                    }, this.message = function(e, t, i, n) {
                        a.message(e, t, i, n)
                    }, this.setHeight = function(e) {
                        a.setHeight(e)
                    }, this.ud = function() {
                        return !!this.backendUrl && (!DayPilot.isArray(n.tasks.list) || 0 == n.tasks.list.length)
                    }, this.init = function() {
                        return l.translate(), l.Gd(), l.Hd(), this.Jd(), a.init(), this.A = !0, this.yd(), this
                    }, this.Kd = !1, this.dispose = function() {
                        a.dispose(), n.Kd = !0
                    }, this.update = function() {
                        if (n.Kd) throw new DayPilot.Exception("This DayPilot.Gantt instance has been disposed.");
                        l.translate(), l.Gd(), l.Hd(), n.Jd(), a.update()
                    }, this.scrollTo = function(e) {
                        a.scrollTo(e)
                    }, this.scrollToTask = function(e) {
                        a.scrollToResource(e)
                    }, this.Ld = {};
                    var r = this.Ld;
                    r.cache = {}, r.clearCache = function() {
                        r.cache = {}
                    }, r.addToCache = function(e, t) {
                        var i = e.id;
                        if (i) {
                            var n = {};
                            if (n.isTaskWrapper = !0, n.data = e, n.parent = t, r.cache[i]) throw "Duplicate task id detected";
                            r.cache[i] = n
                        }
                    }, r.findInCache = function(e) {
                        return e ? r.cache[e.toString()] : null
                    }, r.getProperty = function(e, t) {
                        return e.tags && e.tags[t] ? e.tags[t] : e[t]
                    }, r.findParentArray = function(e) {
                        return r.findInArray(n.tasks.list, e)
                    }, r.findInArray = function(e, t) {
                        if (DayPilot.indexOf(e, t) !== -1) return e;
                        for (var i = 0; i < e.length; i++) {
                            var n = e[i];
                            if (n.children && n.children.length > 0) {
                                var a = r.findInArray(n.children, t);
                                if (a) return a
                            }
                        }
                        return null
                    }, this.Md = {};
                    var l = this.Md;
                    l.translate = function() {
                        if (a.internal.gantt = n, a.durationBarMode = "PercentComplete", a.timeRangeSelectedHandling = "Disabled", a.treeEnabled = !0, a.rowCreateHtml = "", a.autoRefreshCommand = n.autoRefreshCommand, a.autoRefreshEnabled = n.autoRefreshEnabled, a.autoRefreshInterval = n.autoRefreshInterval, a.autoRefreshMaxCount = n.autoRefreshMaxCount, a.autoScroll = n.autoScroll, a.backendUrl = n.backendUrl, a.crosshairColor = n.crosshairColor, a.crosshairOpacity = n.crosshairOpacity, a.crosshairType = n.crosshairType, a.doubleClickTimeout = n.doubleClickTimeout, a.durationBarVisible = n.completeBarVisible, a.durationBarHeight = n.completeBarHeight, a.dynamicEventRendering = n.progressiveTaskRendering ? "Progressive" : "Disabled", a.dynamicEventRenderingMargin = n.progressiveTaskRenderingMargin, a.dynamicEventRenderingCacheSweeping = n.progressiveTaskRenderingCacheSweeping, a.dynamicEventRenderingCacheSize = n.progressiveTaskRenderingCacheSize, a.startDate = new DayPilot.Date(n.startDate), a.days = n.days, a.cellDuration = n.cellDuration, a.cellGroupBy = n.cellGroupBy, a.cellWidth = n.cellWidth, a.cellWidthSpec = n.cellWidthSpec, a.cornerHtml = n.cornerHtml, a.eventHeight = n.taskHeight, a.eventResizeMargin = n.taskResizeMargin, a.floatingEvents = n.floatingTasks, a.floatingTimeHeaders = n.floatingTimeHeaders, a.headerHeight = n.headerHeight, a.heightSpec = n.heightSpec, a.height = n.height, a.linkBottomMargin = n.linkBottomMargin, a.linkPointSize = n.linkPointSize, a.loadingLabelVisible = n.loadingLabelVisible, a.loadingLabelText = n.loadingLabelText, a.locale = n.locale, a.messageBarPosition = n.messageBarPosition, a.messageHideAfter = n.messageHideAfter, a.rowCreateHandling = n.rowCreateHandling, a.progressiveRowRendering = n.progressiveRowRendering, a.progressiveRowRenderingPreload = n.progressiveRowRenderingPreload, a.scale = n.scale, a.scrollDelayEvents = n.scrollDelayTasks, a.scrollDelayCells = n.scrollDelayCells, a.scrollDelayFloats = n.scrollDelayFloats, a.scrollX = n.scrollX, a.scrollY = n.scrollY, n.scrollToTaskId && (a.scrollToResourceId = n.scrollToTaskId), a.separators = n.separators, a.tapAndHoldTimeout = n.tapAndHoldTimeout, a.eventHtmlLeftMargin = n.taskHtmlLeftMargin, a.eventHtmlRightMargin = n.taskHtmlRightMargin, a.eventMovingStartEndEnabled = n.taskMovingStartEndEnabled, a.eventMovingStartEndFormat = n.taskMovingStartEndFormat, a.eventResizingStartEndEnabled = n.taskResizingStartEndEnabled, a.eventResizingStartEndFormat = n.taskResizingStartEndFormat, a.hideUntilInit = n.hideUntilInit, a.treeIndent = n.treeIndent, a.treeAutoExpand = n.treeAutoExpand, a.treeImageMarginLeft = n.treeImageMarginLeft, a.treeImageMarginTop = n.treeImageMarginTop, a.timeHeaders = n.timeHeaders, a.rowHeaderHideIconEnabled = n.rowHeaderHideIconEnabled, a.rowHeaderScrolling = n.rowHeaderScrolling, a.rowHeaderSplitterWidth = n.rowHeaderSplitterWidth, a.rowHeaderWidth = n.rowHeaderWidth, a.rowHeaderWidthAutoFit = n.rowHeaderWidthAutoFit, a.rowMarginBottom = n.rowMarginBottom, a.rowMinHeight = n.rowMinHeight, a.selectedRows = n.selectedRows, a.theme = n.theme, a.useEventBoxes = n.useEventBoxes, a.snapToGrid = n.snapToGrid, a.uniqueID = n.uniqueID, a.bubble = n.bubbleTask, a.cellBubble = n.bubbleCell, a.resourceBubble = n.bubbleRow, a.contextMenu = n.contextMenuTask, a.contextMenuResource = n.contextMenuRow, a.contextMenuLink = n.contextMenuLink, a.syncResourceTree = n.syncTasks, a.syncLinks = n.syncLinks, a.timeline = n.timeline, a.visible = n.visible, a.eventMoveHandling = n.taskMoveHandling, a.eventClickHandling = n.taskClickHandling, a.eventResizeHandling = n.taskResizeHandling, a.linkCreateHandling = n.linkCreateHandling, a.eventRightClickHandling = n.taskRightClickHandling, a.eventDoubleClickHandling = n.taskDoubleClickHandling, a.rowMoveHandling = n.rowMoveHandling, a.rowClickHandling = n.rowClickHandling, a.rowDoubleClickHandling = n.rowDoubleClickHandling, a.rowEditHandling = n.rowEditHandling, a.rowSelectHandling = n.rowSelectHandling, a.eventVersionsEnabled = n.taskVersionsEnabled, a.eventVersionHeight = n.taskVersionHeight, a.eventVersionMargin = n.taskVersionMargin, a.eventVersionPosition = n.taskVersionPosition, DayPilot.isArray(n.columns)) {
                            a.rowHeaderColumns = [];
                            for (var e = 0; e < n.columns.length; e++) {
                                var t = n.columns[e],
                                    i = {};
                                DayPilot.Util.copyProps(t, i, ["title", "width"]), a.rowHeaderColumns.push(i)
                            }
                        }
                        n.r() && (a.timeHeader = n.timeHeader, a.cellProperties = n.cellProperties, a.cellConfig = n.cellConfig), a.onRowCreate = function(e) {
                            "function" == typeof n.onRowCreate && n.onRowCreate(e)
                        }, a.onRowCreated = function(e) {
                            "function" == typeof n.onRowCreated && n.onRowCreated(e)
                        }, a.onAfterRender = function(e) {
                            "function" == typeof n.onAfterRender && n.onAfterRender(e)
                        }, a.onAfterUpdate = function(e) {
                            "function" == typeof n.onAfterUpdate && n.onAfterUpdate(e)
                        }, a.onRowHeaderResized = function(e) {
                            n.rowHeaderWidth = a.rowHeaderWidth
                        }, a.onAjaxError = function(e) {
                            "function" == typeof n.onAjaxError && n.onAjaxError(e)
                        }, a.onRowHeaderColumnResized = function(e) {
                            for (var t = e.column, i = DayPilot.indexOf(a.rowHeaderColumns, t), o = 0; o < n.columns.length; o++) {
                                var r = a.rowHeaderColumns[o];
                                n.columns[o].width = r.width
                            }
                            if ("function" == typeof n.onColumnResized) {
                                var l = {};
                                l.column = n.columns[i], n.onColumnResized(l)
                            }
                        }, a.onBeforeRowHeaderRender = function(e) {
                            e.task = new DayPilot.Task(e.row.$.row.task, n);
                            var t = e.row.$.row.events[0];
                            t && "Auto" === n.taskGroupMode && "Group" === t.data.type && (e.task.data.start = t.data.start, e.task.data.end = t.data.end);
                            var i = [];
                            if (DayPilot.isArray(n.columns))
                                for (var a = 0; a < n.columns.length; a++) {
                                    var o = n.columns[a],
                                        l = o.property,
                                        s = {};
                                    s.value = r.getProperty(e.task.data, l), e.task.data.row && e.task.data.row.columns && e.task.data.row.columns[a] ? s.html = e.task.data.row.columns[a].html : s.html = s.value, i.push(s)
                                }
                            if (e.row.columns = i, "function" == typeof n.onBeforeRowHeaderRender && n.onBeforeRowHeaderRender(e), DayPilot.isArray(n.columns))
                                for (var a = 0; a < n.columns.length; a++) {
                                    var d = i[a].html;
                                    0 === a ? e.row.html = d : e.row.columns[a - 1].html = d
                                }
                        }, a.onBeforeCellRender = function(e) {
                            e.task = n.tasks.find(e.cell.resource), delete e.cell.resource, "function" == typeof n.onBeforeCellRender && n.onBeforeCellRender(e)
                        }, a.onBeforeTimeHeaderRender = function(e) {
                            "function" == typeof n.onBeforeTimeHeaderRender && n.onBeforeTimeHeaderRender(e)
                        }, a.onEventClick = function(e) {
                            e.task = new DayPilot.Task(e.e, n), "function" == typeof n.onTaskClick && n.onTaskClick(e)
                        }, a.onEventClicked = function(e) {
                            "function" == typeof n.onTaskClicked && n.onTaskClicked(e)
                        }, a.onEventDelete = function(e) {
                            "function" == typeof n.onTaskDelete && n.onTaskDelete(e)
                        }, a.onEventDeleted = function(e) {
                            "function" == typeof n.onTaskDeleted && n.onTaskDeleted(e)
                        }, a.onEventDoubleClick = function(e) {
                            e.task = new DayPilot.Task(e.e, n), "function" == typeof n.onTaskDoubleClick && n.onTaskDoubleClick(e)
                        }, a.onEventDoubleClicked = function(e) {
                            "function" == typeof n.onTaskDoubleClicked && n.onTaskDoubleClicked(e)
                        }, a.onEventRightClick = function(e) {
                            e.task = new DayPilot.Task(e.e, n), "function" == typeof n.onTaskRightClick && n.onTaskRightClick(e)
                        }, a.onEventRightClicked = function(e) {
                            "function" == typeof n.onTaskRightClicked && n.onTaskRightClicked(e)
                        }, a.onRowMoving = function(e) {
                            e.Nd = e.source, e.Od = e.target, e.source = new DayPilot.Task(e.Nd.$.row.task, n), e.target = new DayPilot.Task(e.Od.$.row.task, n), "function" == typeof n.onRowMoving && n.onRowMoving(e)
                        }, a.onRowMove = function(e) {
                            e.Nd = e.source, e.Od = e.target, e.source = new DayPilot.Task(e.Nd.$.row.task, n), e.target = new DayPilot.Task(e.Od.$.row.task, n), "function" == typeof n.onRowMove && n.onRowMove(e), e.source = e.Nd, e.target = e.Od
                        }, a.onRowMoved = function(e) {
                            if ("Update" === n.rowMoveHandling || "Notify" === n.rowMoveHandling) {
                                var t = e.Nd.$.row.task,
                                    i = e.Od.$.row.task,
                                    a = e.position;
                                if ("forbidden" === a) return;
                                var o = e.Nd.calendar.internal.gantt,
                                    l = o.Ld.findParentArray(t);
                                if (!l) throw "Cannot find source node parent";
                                var s = DayPilot.indexOf(l, t);
                                l.splice(s, 1);
                                var d = r.findParentArray(i);
                                if (!d) throw "Cannot find target node parent";
                                var c = DayPilot.indexOf(d, i);
                                switch (a) {
                                    case "before":
                                        d.splice(c, 0, t);
                                        break;
                                    case "after":
                                        d.splice(c + 1, 0, t);
                                        break;
                                    case "child":
                                        i.children || (i.children = [], i.expanded = !0), i.children.push(t)
                                }
                                n.kd.notify(), n.update(), o !== n && o.update()
                            }
                            e.source = new DayPilot.Task(e.Nd.$.row.task, n), e.target = new DayPilot.Task(e.Od.$.row.task, n), "function" == typeof n.onRowMoved && n.onRowMoved(e)
                        }, a.onRowClick = function(e) {
                            e.task = new DayPilot.Task(e.resource.$.row.task, n), "function" == typeof n.onRowClick && n.onRowClick(e)
                        }, a.onRowClicked = function(e) {
                            "function" == typeof n.onRowClicked && n.onRowClicked(e)
                        }, a.onRowDoubleClick = function(e) {
                            e.task = new DayPilot.Task(e.resource.$.row.task, n), "function" == typeof n.onRowDoubleClick && n.onRowDoubleClick(e)
                        }, a.onRowDoubleClicked = function(e) {
                            "function" == typeof n.onRowDoubleClicked && n.onRowDoubleClicked(e)
                        }, a.onRowEdit = function(e) {
                            e.task = new DayPilot.Task(e.resource.$.row.task, n), "function" == typeof n.onRowEdit && n.onRowEdit(e)
                        }, a.onRowEdited = function(e) {
                            "function" == typeof n.onRowEdited && n.onRowEdited(e)
                        }, a.onRowSelect = function(e) {
                            e.task = new DayPilot.Task(e.row.$.row.task, n), "function" == typeof n.onRowSelect && n.onRowSelect(e)
                        }, a.onRowSelected = function(e) {
                            "function" == typeof n.onRowSelected && n.onRowSelected(e)
                        }, a.onEventMove = function(e) {
                            e.Pd = e.e, e.task = new DayPilot.Task(e.e, n), "function" == typeof n.onTaskMove && n.onTaskMove(e)
                        }, a.onEventMoved = function(e) {
                            if ("Auto" === n.taskGroupMode) {
                                var t = e.Pd,
                                    i = t.data.task;
                                for (i.start = t.start(), i.end = t.end(); t.data.parent;) {
                                    var o = a.events.find(t.data.parent.id),
                                        r = l.childrenStartEnd(t.data.parent);
                                    o.start(r.start), o.end(r.end), a.events.update(o), t = o
                                }
                            }
                            n.kd.notify(), "function" == typeof n.onTaskMoved && n.onTaskMoved(e)
                        }, a.onEventMoving = function(e) {
                            e.task = new DayPilot.Task(e.e, n), e.Pd = e.e, delete e.position, delete e.overlapping, delete e.resource, "function" == typeof n.onTaskMoving && n.onTaskMoving(e)
                        }, a.onEventResize = function(e) {
                            e.Pd = e.e, e.task = new DayPilot.Task(e.e, n), "function" == typeof n.onTaskResize && n.onTaskResize(e)
                        }, a.onEventResized = function(e) {
                            if ("Auto" === n.taskGroupMode) {
                                var t = e.Pd,
                                    i = t.data.task;
                                for (i.start = t.start(), i.end = t.end(); t.data.parent;) {
                                    var o = a.events.find(t.data.parent.id),
                                        r = l.childrenStartEnd(t.data.parent);
                                    o.start(r.start), o.end(r.end), a.events.update(o), t = o
                                }
                            }
                            n.kd.notify(), "function" == typeof n.onTaskResized && n.onTaskResized(e)
                        }, a.onEventResizing = function(e) {
                            e.task = new DayPilot.Task(e.e, n), e.Pd = e.e, "function" == typeof n.onTaskResizing && n.onTaskResizing(e)
                        }, a.onLinkCreate = function(e) {
                            e.source = n.tasks.find(e.from), e.target = n.tasks.find(e.to), "function" == typeof n.onLinkCreate && n.onLinkCreate(e)
                        }, a.onLinkCreated = function(e) {
                            "function" == typeof n.onLinkCreated && n.onLinkCreated(e)
                        }, a.onResourceExpand = function(e) {
                            var t = e.resource.$.row.task;
                            t.row || (t.row = {}), t.row.collapsed = !1
                        }, a.onResourceCollapse = function(e) {
                            var t = e.resource.$.row.task;
                            t.row || (t.row = {}), t.row.collapsed = !0
                        }
                    }, l.Qd = function(e) {
                        for (var t = 0; t < a.rowlist.length; t++) {
                            var i = a.rowlist[t];
                            if (i.task === e) return a.internal.createRowObject(i)
                        }
                        return null
                    }, l.Hd = function() {
                        a.links.list = n.links.list ? n.links.list : []
                    }, l.Gd = function() {
                        a.resources = [], a.events.list = [], r.clearCache(), l.Rd(n.tasks.list, a.resources, null), s.sheet && (s.sheet.commit(), s.sheet = null)
                    }, l.Sd = function(e) {
                        var t = e.type || "Task";
                        e.children && e.children.length && (t = "Group");
                        var i = {};
                        for (var a in e) "children" !== a && (i[a] = e[a]);
                        var o = {};
                        if (o.data = i, o.type = t, i.box || (i.box = {}), "undefined" == typeof i.box.html)
                            if ("Task" === o.type) {
                                var r = i.complete || 0;
                                i.box.html = r + "%"
                            } else i.box.html = "";
                        return "undefined" == typeof i.box.htmlRight && (i.box.htmlRight = i.text), "function" == typeof n.onBeforeTaskRender && n.onBeforeTaskRender(o), o.type = t, o
                    };
                    var s = {};
                    s.keys = {}, s.sheet = null, l.Td = function(e) {
                        if (e.data.box && !e.data.box.cssClass && e.data.box.backColor && "Task" !== e.type) {
                            var t = DayPilot.Util.normalizeColor(e.data.box.backColor),
                                i = e.type + "_" + t.replace("#", ""),
                                a = n.theme,
                                o = n.theme + "_" + i;
                            return s.keys[i] || (s.keys[i] = !0, s.sheet || (s.sheet = DayPilot.sheet()), "Group" === e.type ? (s.sheet.add("." + o + "." + a + "_task_group ." + a + "_event_inner", "position:absolute;top:5px;left:0px;right:0px;bottom:6px;overflow:hidden; background: " + t + "; filter: none; border: 0px none;"), s.sheet.add("." + o + "." + a + "_task_group." + a + "_event:before", "content:''; border-color: transparent transparent transparent " + t + "; border-style: solid; border-width: 6px; position: absolute; bottom: 0px;"), s.sheet.add("." + o + "." + a + "_task_group." + a + "_event:after", "content:''; border-color: transparent " + t + " transparent transparent; border-style: solid; border-width: 6px; position: absolute; bottom: 0px; right: 0px;")) : "Milestone" === e.type && (s.sheet.add("." + o + "." + a + "_task_milestone ." + a + "_event_inner", "position:absolute;top:16%;left:16%;right:16%;bottom:16%; background: " + t + "; border: 0px none; -webkit-transform: rotate(45deg);-moz-transform: rotate(45deg);-ms-transform: rotate(45deg);-o-transform: rotate(45deg); transform: rotate(45deg); filter: none;"), s.sheet.add("." + o + "." + a + "_browser_ie8 ." + a + "_task_milestone ." + a + "_event_inner", "-ms-filter: \"progid:DXImageTransform.Microsoft.Matrix(SizingMethod='auto expand', M11=0.7071067811865476, M12=-0.7071067811865475, M21=0.7071067811865475, M22=0.7071067811865476);\""))), o
                        }
                    }, l.Rd = function(t, i, o) {
                        if (DayPilot.isArray(t))
                            for (var s = 0; s < t.length; s++) {
                                var d = t[s];
                                r.addToCache(d, o);
                                var c = l.Sd(d),
                                    h = c.data,
                                    u = c.type,
                                    f = {};
                                if (DayPilot.Util.copyProps(h, f, ["id", "start", "end", "text", "complete", "tags", "versions"]), DayPilot.Util.copyProps(h.box, f), f.parent = o, f.task = d, f.resource = h.id, "Group" == u) {
                                    f.type = "Group", f.html = "";
                                    var v = l.Td(c);
                                    if (v && (f.cssClass = (f.cssClass || "") + " " + v), delete f.backColor, "Auto" === n.taskGroupMode) {
                                        var p = l.childrenStartEnd(d);
                                        f.start = p.start, f.end = p.end, f.resizeDisabled = !0, f.moveDisabled = !0
                                    }
                                } else if ("Milestone" === u) {
                                    f.html = "", d.end = d.start;
                                    var v = l.Td(c);
                                    v && (f.cssClass = (f.cssClass || "") + " " + v), h.end = h.start, f.end = h.start, f.barHidden = !0, f.resizeDisabled = !0, f.type = "Milestone", f.width = n.taskHeight, delete f.backColor
                                } else "Task" === u && e();
                                f.moveVDisabled = !0, f.htmlRight = h.box.htmlRight, f.htmlLeft = h.box.htmlLeft, a.events.list.push(f);
                                var g = {};
                                g.task = d, DayPilot.Util.copyProps(h.row, g), g.name = h.text, g.id = h.id, g.children = [], g.expanded = !h.row || !h.row.collapsed, d.children && d.children.length && l.Rd(d.children, g.children, d), i.push(g)
                            }
                    }, l.childrenStartEnd = function(e) {
                        if (!e.children || !e.children.length) {
                            var t = e.start,
                                i = e.end;
                            return "Milestone" === e.type && (i = t), {
                                "start": new DayPilot.Date(t),
                                "end": new DayPilot.Date(i)
                            }
                        }
                        for (var t = null, i = null, n = 0; n < e.children.length; n++) {
                            var a = l.childrenStartEnd(e.children[n]);
                            (!t || a.start.getTime() < t.getTime()) && (t = a.start), (!i || a.end.getTime() > i.getTime()) && (i = a.end)
                        }
                        return {
                            "start": t,
                            "end": i
                        }
                    }, this.rows = {}, this.rows.expand = function(e) {
                        a.rows.expand(e)
                    }, this.rows.expandAll = function() {
                        a.rows.expandAll()
                    }, this.rows.selection = {}, this.rows.selection.add = function(e) {
                        a.rows.find(e.data.id)
                    }, this.rows.selection.clear = function() {
                        a.rows.selection.clear()
                    }, this.rows.selection.get = function() {
                        return a.rows.selection.get().map(function(e) {
                            return new DayPilot.Task(e.$.row.task, n)
                        })
                    }, this.zd = null, this.Ad = function(e) {
                        var t = {
                            "tasks": {
                                "preInit": function() {
                                    var e = this.data;
                                    e && (DayPilot.isArray(e.list) ? n.tasks.list = e.list : n.tasks.list = e)
                                }
                            },
                            "links": {
                                "preInit": function() {
                                    var e = this.data;
                                    e && (DayPilot.isArray(e.list) ? n.links.list = e.list : n.links.list = e)
                                }
                            },
                            "scrollTo": {
                                "postInit": function() {
                                    this.data && n.scrollTo(this.data)
                                }
                            },
                            "scrollToTask": {
                                "postInit": function() {
                                    this.data && n.scrollToTask(this.data)
                                }
                            }
                        };
                        this.zd = t;
                        for (var i in e)
                            if (t[i]) {
                                var a = t[i];
                                a.data = e[i], a.preInit && a.preInit()
                            } else n[i] = e[i]
                    }, this.yd = function() {
                        var e = this.zd;
                        for (var t in e) {
                            var i = e[t];
                            i.postInit && i.postInit()
                        }
                    }, this.internal = {}, this.internal.initialized = function() {
                        return n.A
                    }, this.internal.rowObjectForTaskData = l.Qd, this.internal.loadOptions = n.Ad, this.kd = {}, this.kd.scope = null, this.kd.notify = function() {
                        n.kd.scope && n.kd.scope["$apply"]()
                    }, this.Ad(i)
                }, "undefined" != typeof jQuery && ! function(e) {
                    e.fn.daypilotGantt = function(e) {
                        var t = null,
                            i = this.each(function() {
                                if (!this.daypilot) {
                                    var i = new DayPilot.Gantt(this.id, e);
                                    i.init(), this.daypilot = i, t || (t = i)
                                }
                            });
                        return 1 === this.length ? t : i
                    }
                }(jQuery),
                function() {
                    var e = DayPilot.am();
                    e && e.directive("daypilotGantt", ["$parse", function(e) {
                        return {
                            "restrict": "E",
                            "template": "<div id='{{id}}'></div>",
                            "compile": function(t, i) {
                                return t.replaceWith(this["template"].replace("{{id}}", i["id"])),
                                    function(t, i, n) {
                                        var a = new DayPilot.Gantt(i[0]);
                                        a.kd.scope = t, a.init();
                                        var o = n["id"];
                                        o && (t[o] = a);
                                        var r = n["publishAs"];
                                        if (r) {
                                            (0, e(r).assign)(t, a)
                                        }
                                        for (var l in n)
                                            if (0 === l.indexOf("on")) {
                                                var s = DayPilot.Util.shouldApply(l);
                                                s ? ! function(i) {
                                                    a[i] = function(a) {
                                                        var o = e(n[i]);
                                                        t["$apply"](function() {
                                                            o(t, {
                                                                "args": a
                                                            })
                                                        })
                                                    }
                                                }(l) : ! function(i) {
                                                    a[i] = function(a) {
                                                        e(n[i])(t, {
                                                            "args": a
                                                        })
                                                    }
                                                }(l)
                                            } var d = t["$watch"],
                                            c = n["config"] || n["daypilotConfig"];
                                        d.call(t, c, function(e, t) {
                                            a.Ad(e), a.update()
                                        }, !0)
                                    }
                            }
                        }
                    }])
                }(), "undefined" != typeof Sys && Sys.Application && Sys.Application.notifyScriptLoaded && Sys.Application.notifyScriptLoaded()
        }
    }(), 
"undefined" == typeof DayPilot) var DayPilot = {};

//////////////////////////////janek3///////////////////////////////////


