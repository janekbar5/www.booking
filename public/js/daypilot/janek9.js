
if ("undefined" == typeof DayPilot.Global && (DayPilot.Global = {}), function() {

        function e(e) {
            for (var t = e.touches[0].clientX, i = e.touches[0].clientY, n = document.elementFromPoint(t, i); n && n.parentNode;)
                if (n = n.parentNode, n.daypilotMainD) return n.calendar;
            return !1
        }

        function t(e) {
            var t = e.touches[0].pageX,
                i = e.touches[0].pageY,
                n = {};
            return n.x = t, n.y = i, n
        }
        if ("undefined" == typeof DayPilot.Scheduler) {
            var i = function() {
                    var e = navigator.userAgent;
                    return e.indexOf("Mozilla/5.0") > -1 && e.indexOf("Android ") > -1 && e.indexOf("AppleWebKit") > -1 && !(e.indexOf("Chrome") > -1)
                }(),
                n = {},
                a = function() {},
                o = !1;
            DayPilot.Scheduler = function(e, o) {
                function d(e, t) {
                    return 1 - e / t
                }
                this.v = "2018.1.3151";
                var c = this;
                this.id = e, this.isScheduler = !0, this.hideUntilInit = !0, this.api = 2, this.allowDefaultContextMenu = !1, this.allowEventOverlap = !0, this.allowMultiMove = !1, this.allowMultiRange = !1, this.allowMultiResize = !1, this.allowMultiSelect = !0, this.multiSelectRectangle = "Disabled", this.autoRefreshCommand = "refresh", this.autoRefreshEnabled = !1, this.autoRefreshInterval = 60, this.autoRefreshMaxCount = 20, this.autoScroll = "Drag", this.blockOnCallBack = !1, this.navigatorBackSync = null, this.beforeCellRenderCaching = !0, "function" == typeof DayPilot.Bubble ? (this.bubble = new DayPilot.Bubble, this.cellBubble = new DayPilot.Bubble, this.resourceBubble = new DayPilot.Bubble) : (this.bubble = null, this.cellBubble = null, this.resourceBubble = null), this.businessBeginsHour = 9, this.businessEndsHour = 18, this.businessWeekends = !1, this.cellDuration = 60, this.cellGroupBy = "Day", this.cellStacking = !1, this.cellStackingAutoHeight = !1, this.cellWidth = 40, this.cellWidthMin = 1, this.cellWidthSpec = "Fixed", this.cellsMarkBusiness = !0, this.eventHtmlLeftMargin = 20, this.eventHtmlRightMargin = 20, this.eventMarginLeft = 0, this.eventMarginRight = 0, this.eventMarginBottom = 0, this.eventsLoadMethod = "GET", this.groupConcurrentEvents = !1, this.groupConcurrentEventsLimit = 1, this.Tf = !0, this.Uf = !1, this.cellSweeping = !0, this.cellSweepingCacheSize = 1e3, this.crosshairColor = "Gray", this.crosshairOpacity = 20, this.crosshairType = "Header", this.Kd = !1, this.doubleClickTimeout = 300, this.dragOutAllowed = !1, this.durationBarHeight = 3, this.durationBarVisible = !0, this.durationBarMode = "Duration", this.durationBarDetached = !1, this.days = 1, this.drawBlankCells = !0, this.dynamicEventRendering = "Progressive", this.dynamicEventRenderingMargin = 50, this.dynamicEventRenderingMarginX = null, this.dynamicEventRenderingMarginY = null, this.dynamicEventRenderingCacheSweeping = !1, this.dynamicEventRenderingCacheSize = 200, this.dynamicLoading = !1, this.eventBorderColor = "#000000", this.eventBackColor = "#FFFFFF", this.eventEditMinWidth = 100, this.eventEndSpec = "DateTime", this.eventFontFamily = "Tahoma, Arial", this.eventFontSize = "8pt", this.eventFontColor = "#000000", this.eventHeight = 25, this.eventMinWidth = 1, this.eventMoveMargin = 5, this.eventMoveToPosition = !1, this.eventMoveSkipNonBusiness = !1, this.eventResizeMargin = 5, this.eventStackingLineHeight = 100, this.eventTapAndHoldHandling = "Move", this.eventTextWrappingEnabled = !1, this.eventUpdateInplaceOptimization = !1, this.Vf = !1, this.headerFontColor = "#000000", this.headerFontFamily = "Tahoma, Arial", this.headerFontSize = "8pt", this.headerHeight = 20, this.heightSpec = "Auto", this.height = 300, this.hideBorderFor100PctHeight = !1, this.hourFontFamily = "Tahoma, Arial", this.hourFontSize = "10pt", this.hourNameBackColor = "#ECE9D8", this.hourNameBorderColor = "#ACA899", this.infiniteScrollingEnabled = !1, this.infiniteScrollingMargin = 50, this.infiniteScrollingStepDays = 30, this.initEventEnabled = !0, this.jointEventsResize = !0, this.jointEventsMove = !0, this.layout = "Auto", this.linkCreateHandling = "Disabled", this.linkBottomMargin = 10, this.linkPointSize = 10, this.linksLoadMethod = "GET", this.locale = "en-us", this.loadingLabelText = "Loading...", this.loadingLabelVisible = !0, this.overrideWheelScrolling = !1, this.messageBarPosition = "Top", this.messageHideAfter = 5e3, this.messageHideOnMouseOut = !0, this.multiMoveVerticalMode = "Disabled", this.moveBy = "Full", this.notifyCommit = "Immediate", this.progressiveRowRendering = !0, this.progressiveRowRenderingPreload = 25, this.tapAndHoldTimeout = 300, this.timeHeaders = [{
                    "groupBy": "Default"
                }, {
                    "groupBy": "Cell"
                }], this.treePreventParentUsage = !1, this.treeAutoExpand = !0, this.rowCreateHeight = null, this.rowCreateHtml = "New row...", this.rowHeaderColumnDefaultWidth = 80, this.rowHeaderHideIconEnabled = !1, this.rowHeaderWidth = 80, this.rowHeaderScrolling = !1, this.rowHeaderSplitterWidth = 3, this.rowHeaderWidthAutoFit = !0, this.rowHeaderWidthMarginRight = 0, this.rowHeaderCols = null, this.rowMarginBottom = 0, this.rowMarginTop = 0, this.rowMinHeight = 0, this.rowsLoadMethod = "GET", this.scale = "CellDuration", this.scrollDelayDynamic = 500, this.scrollDelayEvents = 200, this.scrollDelayCells = 0, this.scrollDelayFloats = 0, this.scrollStep = null, this.scrollX = 0, this.scrollY = 0, this.selectedRows = [], this.showBaseTimeHeader = !0, this.showNonBusiness = !0, this.showToolTip = !0, this.snapToGrid = !0, this.startDate = DayPilot.Date.today(), this.syncResourceTree = !1, this.timeHeaderTextWrappingEnabled = !1, this.treeEnabled = !1, this.treeIndent = 20, this.treeImageMarginLeft = 5, this.treeImageMarginTop = 5, this.timeFormat = "Auto", this.useEventBoxes = "Always", this.viewType = "Resources", this.visible = !0, this.watchWidthChanges = !0, this.weekStarts = "Auto", this.width = null, this.floatingEvents = !0, this.floatingTimeHeaders = !0, this.eventCorners = "Regular", this.separators = [], this.cornerHtml = "", this.Wf = !0, this.Xf = -1, this.Yf = -1, this.eventClickHandling = "Enabled", this.eventDeleteHandling = "Disabled", this.eventHoverHandling = "Bubble", this.eventDoubleClickHandling = "Disabled", this.eventEditHandling = "Update", this.eventMoveHandling = "Update", this.eventResizeHandling = "Update", this.eventRightClickHandling = "ContextMenu", this.eventSelectHandling = "Update", this.resourceCollapseHandling = "Enabled", this.resourceExpandHandling = "Enabled", this.rowClickHandling = "Enabled", this.rowDoubleClickHandling = "Disabled", this.rowCreateHandling = "Disabled", this.rowEditHandling = "Update", this.rowHeaderColumnResizedHandling = "Update", this.rowSelectHandling = "Update", this.rowMoveHandling = "Disabled", this.timeRangeClickHandling = "Enabled", this.timeRangeDoubleClickHandling = "Disabled", this.timeRangeSelectedHandling = "Enabled", this.timeRangeRightClickHandling = "ContextMenu", this.eventMovingStartEndEnabled = !1, this.eventMovingStartEndFormat = "MMMM d, yyyy", this.timeRangeSelectingStartEndEnabled = !1, this.timeRangeSelectingStartEndFormat = "MMMM d, yyyy", this.eventResizingStartEndEnabled = !1, this.eventResizingStartEndFormat = "MMMM d, yyyy", this.cssOnly = !0, this.cssClassPrefix = "scheduler_default", this.eventVersionsEnabled = !1, this.eventVersionHeight = 25, this.eventVersionMargin = 2;
                this.eventVersionPosition = "Above";
                this.backendUrl = null, DayPilot.browser.ie && (this.scrollDelayCells = 100, this.scrollDelayFloats = 100), 1 === c.api && (this.onEventMove = function() {}, this.onEventResize = function() {}, this.onResourceExpand = function() {}, this.onResourceCollapse = function() {}), this.onEventClick = null, this.onEventClicked = null, this.onEventMove = null, this.onEventMoved = null, this.onEventMoving = null, this.onEventResize = null, this.onEventResized = null, this.onEventResizing = null, this.onEventRightClick = null, this.onEventRightClicked = null, this.onEventMouseOver = null, this.onEventMouseOut = null, this.onRowClick = null, this.onRowClicked = null, this.onTimeRangeClick = null, this.onTimeRangeClicked = null, this.onTimeRangeDoubleClick = null, this.onTimeRangeDoubleClicked = null, this.onTimeRangeSelect = null, this.onTimeRangeSelected = null, this.onTimeRangeSelecting = null, this.onTimeRangeRightClick = null, this.onTimeRangeRightClicked = null, this.onRowHeaderResized = null, this.onCellMouseOver = null, this.onCellMouseOut = null, this.onGridMouseDown = null, this.onBeforeCellRender = null, this.onBeforeEventRender = null, this.onBeforeTimeHeaderRender = null, this.onBeforeResHeaderRender = null, this.onBeforeRowHeaderRender = null, this.onBeforeCellExport = null, this.onBeforeEventExport = null, this.onBeforeTimeHeaderExport = null, this.onBeforeRowHeaderExport = null, this.onEventFilter = null, this.onRowFilter = null, this.onRectangleEventSelecting = null, this.onRectangleEventSelect = null, this.onRectangleEventSelected = null, this.onDimensionsChanged = null, this.onAfterUpdate = null, this.B = 0, this.Zf = -1, this.rowlist = DayPilot.list(), this.itline = DayPilot.list(), this.timeline = null, this.events = {}, this.cells = {}, this.elements = {}, this.elements.events = [], this.elements.bars = [], this.elements.text = [], this.elements.cells = [], this.elements.linesVertical = [], this.elements.separators = [], this.elements.range = [], this.elements.breaks = [], this.elements.links = [], this.elements.linkpoints = [], this.elements.rectangle = [], this.elements.hover = [], this.t = {}, this.t.cells = [], this.t.linesVertical = {}, this.t.linesHorizontal = {}, this.t.timeHeaderGroups = [], this.t.timeHeader = {}, this.t.pixels = [], this.t.breaks = [], this.t.events = [], this.clientState = {}, this.members = {}, this.members.obsolete = ["Init", "cleanSelection", "cssClassPrefix", "getHScrollPosition", "setHScrollPosition", "getVScrollPosition", "setVScrollPosition", "showBaseTimeHeader"], this.members.ignoreFilter = function(e) {
                    return 0 === e.indexOf("div")
                }, this.members.ignore = ["internal", "nav", "debug", "temp", "elements", "members", "cellProperties", "itline", "rowlist", "timeHeader", "timeouts"], this.members.noCssOnly = ["eventCorners", "eventFontColor", "eventFontFamily", "eventFontSize", "headerFontColor", "headerFontFamily", "headerFontSize", "hourFontFamily", "hourFontSize", "hourNameBackColor", "hourNameBorderColor", "loadingLabelBackColor", "loadingLabelFontColor", "loadingLabelFontFamily", "loadingLabelFontSize", "shadow", "timeBreakColor"], this.C = "javasc", this.nav = {}, this.L = function(e, t) {
                    var e = JSON.parse(e);
                    if (c.onScrollCalled = !1, e.CallBackRedirect) return void(document.location.href = e.CallBackRedirect);
                    if ("function" == typeof c.onCallBackResult) {
                        var i = {};
                        if (i.result = e, i.preventDefault = function() {
                                i.preventDefault.value = !0
                            }, c.onCallBackResult(i), i.preventDefault.value) return c.Ya(), c.Z(), c.ta(), e.Message && c.message && c.message(e.Message), c._(e.CallBackData, !0), c.$f(), void c._f()
                    }
                    if (e.BubbleGuid) {
                        var n = e.BubbleGuid,
                            a = this.bubbles[n];
                        return delete this.bubbles[n], c.Z(), "undefined" != typeof e.Result.BubbleHTML && a && a.updateView(e.Result.BubbleHTML, a), void c.$f()
                    }
                    if ("undefined" != typeof DayPilot.Bubble && DayPilot.Bubble.hideActive(), "undefined" != typeof e.ClientState && (null === e.ClientState ? c.clientState = {} : c.clientState = e.ClientState), "None" === e.UpdateType) return c.Z(), c.$f(), e.Message && c.message(e.Message), void c._(e.CallBackData, !0);
                    if (e.VsUpdate) {
                        var o = document.createElement("input");
                        o.type = "hidden", o.name = c.id + "_vsupdate", o.id = o.name, o.value = e.VsUpdate, c.Ge.innerHTML = "", c.Ge.appendChild(o)
                    }
                    "undefined" != typeof e.TagFields && (c.tagFields = e.TagFields), "undefined" != typeof e.SortDirections && (c.sortDirections = e.SortDirections), c.t.drawArea = null, "Full" === e.UpdateType && (c.resources = e.Resources, c.colors = e.Colors, c.palette = e.Palette, c.dirtyColors = e.DirtyColors, c.cellProperties = e.CellProperties, c.cellConfig = e.CellConfig, c.separators = e.Separators, c.timeline = e.Timeline, c.timeHeader = e.TimeHeader, c.timeHeaders = e.TimeHeaders, "undefined" != typeof e.Links && (c.links.list = e.Links), "undefined" != typeof e.RowHeaderColumns && (c.rowHeaderColumns = e.RowHeaderColumns), c.startDate = e.StartDate ? new DayPilot.Date(e.StartDate) : c.startDate, c.days = e.Days ? e.Days : c.days, c.cellDuration = e.CellDuration ? e.CellDuration : c.cellDuration, c.cellGroupBy = e.CellGroupBy ? e.CellGroupBy : c.cellGroupBy, c.cellWidth = e.CellWidth ? e.CellWidth : c.cellWidth, "undefined" != typeof e.Scale && (c.scale = e.Scale), c.viewType = e.ViewType ? e.ViewType : c.viewType, c.hourNameBackColor = e.HourNameBackColor ? e.HourNameBackColor : c.hourNameBackColor, "undefined" != typeof e.ShowNonBusiness && (c.showNonBusiness = e.ShowNonBusiness), c.businessBeginsHour = e.BusinessBeginsHour ? e.BusinessBeginsHour : c.businessBeginsHour, c.businessEndsHour = e.BusinessEndsHour ? e.BusinessEndsHour : c.businessEndsHour, "undefined" != typeof e.DynamicLoading && (c.dynamicLoading = e.DynamicLoading), "undefined" != typeof e.TreeEnabled && (c.treeEnabled = e.TreeEnabled), c.backColor = e.BackColor ? e.BackColor : c.backColor, c.nonBusinessBackColor = e.NonBusinessBackColor ? e.NonBusinessBackColor : c.nonBusinessBackColor, c.locale = e.Locale ? e.Locale : c.locale, "undefined" != typeof e.TimeZone && (c.timeZone = e.TimeZone), c.timeFormat = e.TimeFormat ? e.TimeFormat : c.timeFormat, c.rowHeaderCols = e.RowHeaderCols ? e.RowHeaderCols : c.rowHeaderCols, "undefined" != typeof e.DurationBarMode && (c.durationBarMode = e.DurationBarMode), c.cornerBackColor = e.CornerBackColor ? e.CornerBackColor : c.cornerBackColor, "undefined" != typeof e.CornerHTML && (c.cornerHtml = e.CornerHTML), c.hashes = e.Hashes, c.ag(), c.bg(), c.cg(), c.ca()), "Scroll" !== e.Action && c.da(e.Events), "Full" === e.UpdateType && (c.dg(), c.eg()), c.fg(), c.pa(), c.gg(), c.t.drawArea = null, "Scroll" !== e.Action ? (c.hg(), c.ea(), "Auto" !== c.heightSpec && "Max" !== c.heightSpec || c.la(), c.ig(), c.O(), c.jg(), c.multiselect.clear(!0), c.multiselect.kg(e.SelectedEvents), c.lg(), c.mg(), c.qa()) : (c.multiselect.clear(!0), c.multiselect.kg(e.SelectedEvents), c.ng(e.Events)), "HoldForever" !== c.timeRangeSelectedHandling && c.og(), "Full" === e.UpdateType && (c.setScroll(e.ScrollX, e.ScrollY), c.Df()), c.pg(), c.Ya(), e.SelectedRows && c.qg(e.SelectedRows), c.Z(), "Full" === e.UpdateType && navigator.appVersion.indexOf("MSIE 7.") !== -1 && window.setTimeout(function() {
                        c.dg(), c.la()
                    }, 0), c.ta(), e.Message && c.message && c.message(e.Message), c._(e.CallBackData, !0), c.$f(), c._f(), "Full" !== e.UpdateType || "Scroll" === e.Action || c.onScrollCalled || setTimeout(function() {
                        c.rg()
                    }, 0)
                }, this.Ya = function() {
                    if (c.todo && c.todo.del) {
                        var e = c.todo.del;
                        e.parentNode.removeChild(e), c.todo.del = null
                    }
                }, this._ = function(e, t) {
                    var i = function(e, t) {
                        return function() {
                            if (c.va()) {
                                if ("function" == typeof c.onAfterRender) {
                                    var i = {};
                                    i.isCallBack = t, i.isScroll = !1, i.data = e, c.onAfterRender(i)
                                }
                            } else c.afterRender && c.afterRender(e, t)
                        }
                    };
                    window.setTimeout(i(e, t), 0)
                }, this.scrollTo = function(e, t, i) {
                    c.sg.enabled ? (c.sg.scrollToRequested = e, setTimeout(function() {
                        var e = c.sg.scrollToRequested;
                        e && c.tg(e, t, i)
                    }, 0)) : c.tg(e, t, i)
                }, this.tg = function(e, t, i) {
                    if (e) {
                        if ("Days" === c.viewType) {
                            var n = c.ug(new DayPilot.Date(e));
                            if (!n) return;
                            return void setTimeout(function() {
                                var e = n.top;
                                c.nav.scroll.scrollTop = e
                            }, 100)
                        }
                        var a;
                        if (e instanceof DayPilot.Date) a = this.getPixels(e).left;
                        else if ("string" == typeof e) a = this.getPixels(new DayPilot.Date(e)).left;
                        else {
                            if ("number" != typeof e) throw "Invalid scrollTo() parameter. Accepted parameters: string (ISO date), number (pixels), DayPilot.Date object";
                            a = e
                        }
                        var o = this.vg.clientWidth,
                            r = this.nav.scroll.clientWidth;
                        switch (i = i || "left", i.toLowerCase()) {
                            case "left":
                                break;
                            case "middle":
                                a -= r / 2;
                                break;
                            case "right":
                                a -= r
                        }
                        if (a < 0 && (a = 0), a > o - r && (a = o - r), !t) return void c.wg(a);
                        var l = Math.abs(c.nav.scroll.scrollLeft - a),
                            s = l / o,
                            d = 50;
                        if ("number" == typeof t) d = t;
                        else if ("string" == typeof t) switch (t) {
                            case "fast":
                                d = 30;
                                break;
                            case "normal":
                                d = 50;
                                break;
                            case "slow":
                                d = 100;
                                break;
                            case "linear":
                                d = Math.floor(100 * s)
                        }
                        s > .6 && (d = Math.min(d, 80)), this.xg(a, d)
                    }
                }, this.xg = function(e, t) {
                    function i(e) {
                        var t = e.next();
                        return "undefined" == typeof t ? void(e.finished && e.finished()) : (c.setScrollX(t), window.clearTimeout(c.refreshTimeout), void setTimeout(function() {
                            i(e)
                        }, e.delay))
                    }
                    var n = {};
                    n.steps = [], n.index = 0, n.delay = 10, n.next = function() {
                        var e = n.steps[n.index];
                        return n.index += 1, e
                    }, n.finished = function() {
                        c.rg()
                    };
                    for (var a = c.getScrollX(), t = t || 100, o = e - a, r = 0; r < t; r++) {
                        var l = r / t,
                            s = 2 * (l - .5) / (Math.pow(2 * (l - .5), 2) + 1) + .5;
                        n.steps.push(a + o * s)
                    }
                    n.steps.push(e), i(n)
                }, this.scrollToResource = function(e) {
                    DayPilot.complete(function() {
                        var t = c.yg(e);
                        t && setTimeout(function() {
                            var e = t.top;
                            c.nav.scroll.scrollTop = e
                        }, 100)
                    })
                }, this.zg = function() {
                    if (this.floatingTimeHeaders && this.timeHeader) {
                        var e = this.Ag();
                        if (e) {
                            this.Bg();
                            for (var t = e.pixels.left + u.shiftX, i = e.pixels.right + u.shiftX, n = [], a = 0; a < this.timeHeader.length; a++)
                                for (var o = 0; o < this.timeHeader[a].length; o++) {
                                    var r = this.timeHeader[a][o],
                                        l = r.left,
                                        s = r.left + r.width,
                                        d = null;
                                    if (l < t && t < s && (d = {}, d.x = o, d.y = a, d.marginLeft = t - l, d.marginRight = 0, d.div = this.t.timeHeader[o + "_" + a], n.push(d)), l < i && i < s) {
                                        d || (d = {}, d.x = o, d.y = a, d.marginLeft = 0, d.div = this.t.timeHeader[o + "_" + a], n.push(d)), d.marginRight = s - i;
                                        break
                                    }
                                }
                            for (var c = 0; c < n.length; c++) {
                                var d = n[c];
                                this.Cg(d.div, d.marginLeft, d.marginRight)
                            }
                        }
                    }
                }, this.pg = function() {
                    this.zg(), this.Dg()
                }, this.Eg = {};
                var h = this.Eg;
                h.eventsInRectangle = function(e, t, i, n) {
                    var a = e,
                        o = e + i,
                        r = t,
                        l = t + n;
                    return DayPilot.list(c.elements.events).filter(function(e) {
                        var t = e.event,
                            i = t.part.left,
                            n = t.part.left + t.part.width,
                            s = c.rowlist[t.part.dayIndex],
                            d = s.top + t.part.top,
                            h = d + t.part.height;
                        if (DayPilot.Util.overlaps(i, n, a, o) && DayPilot.Util.overlaps(d, h, r, l)) return !0
                    })
                }, h.events = function(e) {
                    var t = [],
                        e = e || "All",
                        i = c.Ag();
                    if (!i) return DayPilot.list(t);
                    for (var n = i.pixels.left, a = i.pixels.right, o = 0; o < c.elements.events.length; o++) {
                        var r = c.elements.events[o],
                            l = r.event,
                            s = l.part.left,
                            d = l.part.left + l.part.width;
                        switch (e) {
                            case "Left":
                                s < n && n < d && t.push(r);
                                break;
                            case "All":
                                DayPilot.Util.overlaps(s, d, n, a) && t.push(r)
                        }
                    }
                    return DayPilot.list(t).addProps({
                        "area": i
                    })
                }, this.Dg = function() {
                    if (this.floatingEvents) {
                        var e = h.events("Left");
                        this.Fg(), e.each(function(t) {
                            var i = e.area.pixels.left,
                                n = t.event,
                                a = n.part.left,
                                o = i - a;
                            c.Gg(t, o, 0)
                        })
                    }
                }, this.elements.sections = [], this.elements.hsections = [], this.Cg = function(e, t, i) {
                    var n = document.createElement("div");
                    n.setAttribute("unselectable", "on"), n.className = this.q("_timeheader_float"), n.style.position = "absolute", n.style.left = t + "px", n.style.right = i + "px", n.style.top = "0px", n.style.bottom = "0px", n.style.overflow = "hidden";
                    var a = document.createElement("div");
                    a.className = this.q("_timeheader_float_inner"), a.setAttribute("unselectable", "on");
                    var o = e.cell.th;
                    o.innerHTML && (a.innerHTML = o.innerHTML), o.fontColor && (a.style.color = o.fontColor), n.appendChild(a), e.section = n, e.insertBefore(n, e.firstChild.nextSibling), e.firstChild.innerHTML = "", this.elements.hsections.push(e)
                }, this.Bg = function() {
                    for (var e = 0; e < this.elements.hsections.length; e++) {
                        var t = this.elements.hsections[e],
                            i = t.cell;
                        i && t.firstChild && (t.firstChild.innerHTML = i.th.innerHTML), DayPilot.de(t.section), t.section = null
                    }
                    this.elements.hsections = []
                }, this.Gg = function(e, t, i) {
                    var n = document.createElement("div");
                    n.setAttribute("unselectable", "on"), n.className = this.q(E.eventFloat), n.style.position = "absolute", n.style.left = t + "px", n.style.right = i + "px", n.style.top = "0px", n.style.bottom = "0px", n.style.overflow = "hidden";
                    var a = document.createElement("div");
                    a.className = this.q(E.eventFloatInner), a.setAttribute("unselectable", "on"), a.innerHTML = e.event.client.html(), e.event.data.fontColor && (a.style.color = e.event.data.fontColor), n.appendChild(a), e.section = n, e.insertBefore(n, e.firstChild.nextSibling), e.firstChild.innerHTML = "";
                    var o = e.event,
                        r = o.cache ? o.cache.areas : o.data.areas;
                    r = r || [];
                    var l = {
                        "offsetX": c.Hg(a),
                        "eventDiv": e,
                        "areas": r.filter(function(e) {
                            return !(e.start || o.end)
                        })
                    };
                    DayPilot.Areas.attach(n, e.event, l), DayPilot.Areas.disable(e), this.elements.sections.push(e)
                }, this.Ig = null, this.Hg = function(e) {
                    return null === c.Ig && (c.Ig = parseInt(new DayPilot.StyleReader(e).get("padding-left"))), c.Ig
                }, this.Fg = function() {
                    for (var e = 0; e < this.elements.sections.length; e++) {
                        var t = this.elements.sections[e],
                            i = t.event;
                        i && (t.firstChild.innerHTML = i.client.html()), DayPilot.de(t.section), DayPilot.Areas.enable(t), t.section = null
                    }
                    this.elements.sections = []
                }, this.setScrollX = function(e) {
                    c.sg.enabled ? (c.sg.scrollXRequested = e, setTimeout(function() {
                        var e = c.sg.scrollXRequested;
                        "number" == typeof e && c.wg(e)
                    }, 0)) : c.wg(e)
                }, this.wg = function(e) {
                    var t = c.nav.scroll,
                        i = c.Jg();
                    t.clientWidth + e > i && (e = i - t.clientWidth), c.divTimeScroll.scrollLeft = e, t.scrollLeft = e
                }, this.setScrollY = function(e) {
                    c.sg.enabled ? (c.sg.scrollYRequested = e, setTimeout(function() {
                        var e = c.sg.scrollYRequested;
                        "number" == typeof e && c.Kg(e)
                    }, 0)) : c.Kg(e)
                }, this.Kg = function(e) {
                    var t = c.nav.scroll,
                        i = c.Zf;
                    t.clientHeight + e > i && (e = i - t.clientHeight), c.divResScroll.scrollTop = e, t.scrollTop = e
                }, this.setScroll = function(e, t) {
                    c.setScrollX(e), c.setScrollY(t)
                }, this.message = function(e, t) {
                    if (null !== e) {
                        var i = {};
                        "object" == typeof arguments[1] && (i = arguments[1], t = i.delay);
                        var n, t = t || this.messageHideAfter || 2e3,
                            a = this.Lg() + 1,
                            o = this.Mg() + _.splitterWidth(),
                            r = DayPilot.sw(c.nav.scroll) + 1,
                            l = DayPilot.sh(c.nav.scroll) + 1;
                        if (this.nav.message) n = c.nav.message;
                        else {
                            n = document.createElement("div"), n.style.position = "absolute", n.style.left = o + "px", n.style.right = r + "px", n.style.display = "none", DayPilot.re(n, DayPilot.touch.start, function(e) {
                                c.nav.message.style.display = "none", e.preventDefault(), e.stopPropagation()
                            }), n.onmousemove = function() {
                                c.messageHideOnMouseOut && n.messageTimeout && !n.status && clearTimeout(n.messageTimeout)
                            }, n.onmouseout = function() {
                                c.messageHideOnMouseOut && "none" !== c.nav.message.style.display && (n.messageTimeout = setTimeout(c.Ga, 500))
                            };
                            var s = document.createElement("div");
                            s.onclick = function() {
                                c.nav.message.style.display = "none"
                            }, s.className = this.q("_message"), n.appendChild(s);
                            var d = document.createElement("div");
                            d.style.position = "absolute", d.className = this.q("_message_close"), d.onclick = function() {
                                c.nav.message.style.display = "none"
                            }, n.appendChild(d), this.nav.top.appendChild(n), this.nav.message = n
                        }
                        var h = function() {
                            var n = c.nav.message;
                            n.firstChild.className = c.q("_message"), i.cssClass && DayPilot.Util.addClass(n.firstChild, i.cssClass), c.nav.message.firstChild.innerHTML = e;
                            var o = DayPilot.sw(c.nav.scroll) + 1;
                            c.nav.message.style.right = o + "px";
                            var r = c.messageBarPosition || "Top";
                            "Bottom" === r ? (c.nav.message.style.bottom = l + "px", c.nav.message.style.top = "") : "Top" === r && (c.nav.message.style.bottom = "", c.nav.message.style.top = a + "px");
                            var s = function() {
                                n.messageTimeout = setTimeout(c.Ga, t)
                            };
                            DayPilot.fade(c.nav.message, .2, s);
                        };
                        clearTimeout(n.messageTimeout), "none" !== this.nav.message.style.display ? DayPilot.fade(c.nav.message, -.2, h) : h()
                    }
                }, this.Ga = function() {
                    if (c.nav && c.nav.message) {
                        var e = function() {
                            c.nav.message.style.display = "none"
                        };
                        DayPilot.fade(c.nav.message, -.2, e)
                    }
                }, this.Ng = function() {
                    c.nav && c.nav.message && (c.nav.message.style.display = "none")
                }, this.message.show = function(e) {
                    c.message(e)
                }, this.message.hide = function() {
                    c.Ga()
                }, this.Og = null, this.la = function() {
                    if (this.nav.scroll) {
                        ! function() {
                            var e = c.Jg();
                            c.vg.style.height = c.Zf + "px", c.vg.style.width = e + "px", "Auto" !== c.cellWidthSpec || c.Pg ? e > c.nav.scroll.clientWidth ? c.nav.scroll.style.overflowX = "auto" : c.nav.scroll.style.overflowX = "hidden" : (c.nav.scroll.style.overflowX = "hidden", c.nav.scroll.scrollLeft = 0)
                        }();
                        var e = 1;
                        null !== this.Og && (this.nav.top.style.border = this.Og, this.Og = null), "Parent100Pct" !== this.heightSpec && "Max100Pct" !== this.heightSpec || (this.nav.top.style.height = "100%", this.hideBorderFor100PctHeight && (this.Og = this.nav.top.style.border, this.nav.top.style.border = "0 none"), this.height = parseInt(this.nav.top.clientHeight, 10) - this.Lg() - e), this.nav.scroll.style.height = "30px";
                        var t = this.cc(),
                            i = t + this.Lg() + e;
                        t >= 0 && (this.nav.scroll.style.height = t + "px", this.Qg.style.height = t + "px"), this.nav.divider && ((!i || isNaN(i) || i < 0) && (i = 0), this.nav.divider.style.height = i + "px"), "Parent100Pct" !== this.heightSpec && (this.nav.top.style.height = i + "px"), c.nav.resScrollSpace && (c.nav.resScrollSpace.style.height = t + 20 + "px");
                        for (var n = 0; n < this.elements.separators.length; n++) this.elements.separators[n].style.height = this.Zf + "px";
                        for (var n = 0; n < this.elements.linesVertical.length; n++) this.elements.linesVertical[n].style.height = this.Zf + "px";
                        c.Rg()
                    }
                }, this.bg = function() {
                    this.startDate = new DayPilot.Date(this.startDate).getDatePart(), this.t.pixels = [], this.itline = DayPilot.list();
                    var e = "Auto" === this.cellWidthSpec;
                    if (function() {
                            if (e) {
                                var t = 0;
                                c.r() && c.timeline ? t = c.timeline.length : "Manual" === c.scale ? t = c.timeline.length : (c.Sg(), t = c.itline.length, c.itline = DayPilot.list());
                                var i = c.Tg();
                                if (t > 0 && i > 0) {
                                    var n = i / t;
                                    c.cellWidth = Math.max(n, c.cellWidthMin), c.Pg = n < c.cellWidthMin
                                }
                            }
                        }(), this.r() && this.timeline) {
                        if (this.timeline) {
                            this.itline = DayPilot.list();
                            for (var t = null, i = 0, n = 0; n < this.timeline.length; n++) {
                                var a = this.timeline[n],
                                    o = {};
                                if (o.start = new DayPilot.Date(a.start), o.end = a.end ? new DayPilot.Date(a.end) : o.start.addMinutes(this.cellDuration), a.width) o.left = a.left || i, o.width = a.width || this.cellWidth, i += o.width;
                                else {
                                    var r = Math.floor(i + this.cellWidth),
                                        l = r - Math.floor(i);
                                    o.left = Math.floor(i), o.width = l, i += this.cellWidth
                                }
                                o.breakBefore = t && t.ticks !== o.start.ticks, t = o.end, this.itline.push(o)
                            }
                        }
                        e && this.Ug()
                    } else {
                        if (this.timeHeader = [], "Manual" === this.scale) {
                            this.itline = DayPilot.list();
                            for (var i = 0, t = null, n = 0; n < this.timeline.length; n++) {
                                var a = this.timeline[n],
                                    o = {};
                                if (o.start = new DayPilot.Date(a.start), o.end = a.end ? new DayPilot.Date(a.end) : o.start.addMinutes(this.cellDuration), t && o.start.ticks < t.ticks) throw "The timeline must be sequential";
                                var s = a.width || this.cellWidth,
                                    r = Math.floor(i + s),
                                    l = r - Math.floor(i);
                                o.left = Math.floor(i), o.width = l, i += s, o.breakBefore = t && t.ticks !== o.start.ticks, t = o.end, this.itline.push(o)
                            }
                        } else this.Sg();
                        this.Vg()
                    }
                }, this.infinite = {}, this.infinite.shiftStart = function(e) {
                    u.shiftStart(e)
                }, this.infinite.scrollTo = function(e) {
                    u.shiftTo(e)
                }, this.Wg = {};
                var u = this.Wg;
                u.shiftX = 0, u.active = !1, u.updateRowStarts = function() {
                    DayPilot.list(c.rowlist).each(function(e, t) {
                        e.start = c.Da(), e.data = null, c.Xg(t)
                    })
                }, u.isEnabled = function() {
                    return c.infiniteScrollingEnabled
                }, u.shiftTo = function(e) {
                    var e = new DayPilot.Date(e),
                        t = e.addDays(-c.infiniteScrollingStepDays).getDatePart(),
                        i = new DayPilot.Duration(t.getTime() - c.startDate.getDatePart().getTime()).totalDays();
                    u.shiftStart(i, e)
                }, u.shiftStart = function(e, t) {
                    var i;
                    u.active = !0;
                    var n = c.startDate,
                        a = c.startDate.addDays(e);
                    e > 0 && (i = -c.getPixels(a).left), c.startDate = c.startDate.addDays(e), c.itline = DayPilot.list(), c.t.pixels = [], c.Sg(), u.updateRowStarts(), e < 0 && (i = c.getPixels(n).left), c.timeHeader = [], c.Vg(), c.eg(), c.cellProperties = {}, c.ig(), c.jg(), c.da(), c.O(), c.la(), "undefined" != typeof t ? (c.scrollTo(new DayPilot.Date(t)), c.rg()) : c.nav.scroll.scrollLeft += i, c.mg(), c.qa(), u.active = !1
                }, this.Sg = function() {
                    for (var e = "Days" !== this.viewType ? this.startDate.addDays(this.days) : this.startDate.addDays(1), t = this.startDate, i = this.Yg(t), n = !1, a = 0; i.ticks <= e.ticks && i.ticks > t.ticks;) {
                        if (this.Zg(t, i)) {
                            var o = Math.floor(a + this.cellWidth),
                                r = o - Math.floor(a),
                                l = {};
                            l.start = t, l.end = i, l.left = Math.floor(a), l.width = r, l.breakBefore = n, this.itline.push(l), a += this.cellWidth, n = !1
                        } else n = !0;
                        t = i, i = this.Yg(t)
                    }
                }, this.Ug = function() {
                    if (this.timeHeader)
                        for (var e = 0; e < this.timeHeader.length; e++)
                            for (var t = this.timeHeader[e], i = 0; i < t.length; i++) {
                                var n = t[i];
                                n.left = this.getPixels(new DayPilot.Date(n.start)).left;
                                var a = this.getPixels(new DayPilot.Date(n.end)).left,
                                    o = a - n.left;
                                n.width = o
                            }
                }, this.Vg = function() {
                    var e = this.timeHeaders;
                    if (e || (e = [{
                            "groupBy": this.cellGroupBy
                        }, {
                            "groupBy": "Cell"
                        }]), this.itline.isEmpty()) {
                        this.timeHeader = [];
                        for (var t = 0; t < e.length; t++) this.timeHeader[t] = []
                    } else
                        for (var i = c.$g(), t = 0; t < e.length; t++) {
                            var n = e[t].groupBy,
                                a = e[t].format;
                            "Default" === n && (n = this.cellGroupBy);
                            for (var o = [], r = this.Da(); r.ticks < i.ticks;) {
                                var l = {};
                                if (l.start = r, l.end = this._g(l.start, n), l.start.ticks === l.end.ticks) break;
                                l.left = this.getPixels(l.start).left;
                                var s = this.getPixels(l.end).left,
                                    d = s - l.left;
                                if (l.width = d, l.colspan = Math.ceil(d / (1 * this.cellWidth)), "string" == typeof a ? l.innerHTML = l.start.toString(a, _.locale()) : l.innerHTML = this.ah(l, n), l.text = l.innerHTML, d > 0) {
                                    if ("function" == typeof this.onBeforeTimeHeaderRender) {
                                        var h = {};
                                        h.start = l.start, h.end = l.end, h.text = l.innerHTML, h.html = l.innerHTML, h.toolTip = l.innerHTML, h.backColor = null, h.fontColor = null, h.level = this.timeHeader.length, h.cssClass = null;
                                        var u = {};
                                        u.header = h, this.onBeforeTimeHeaderRender(u), l.text = l.innerHTML, l.innerHTML = h.html, l.backColor = h.backColor, l.fontColor = h.fontColor, l.toolTip = h.toolTip, l.areas = h.areas, l.cssClass = h.cssClass
                                    }
                                    o.push(l)
                                }
                                r = l.end
                            }
                            this.timeHeader.push(o)
                        }
                }, this.Zg = function(e, t) {
                    if ("function" == typeof this.onIncludeTimeCell) {
                        var i = {};
                        i.start = e, i.end = t, i.visible = !0;
                        var n = {};
                        return n.cell = i, this.onIncludeTimeCell(n), i.visible
                    }
                    return !!this.showNonBusiness || this.isBusiness({
                        "start": e,
                        "end": t
                    })
                }, this.getPixels = function(e) {
                    var t = e.ticks,
                        i = this.t.pixels[t];
                    if (i) return i;
                    var n = 2218768416e5;
                    if (0 === this.itline.length || t < this.itline[0].start.ticks) {
                        var a = {};
                        return a.cut = !1, a.left = 0, a.boxLeft = a.left, a.boxRight = a.left, a.i = null, a
                    }
                    for (var o = this.bh(e), r = Math.max(o.i - 1, 0), l = r; l < this.itline.length; l++) {
                        var s = this.itline[l],
                            d = s.start.ticks,
                            c = s.end.ticks;
                        if (d < t && t < c) {
                            var h = t - d,
                                a = {};
                            a.cut = !1, a.left = s.left + this.od(s, h), a.boxLeft = s.left, a.boxRight = s.left + s.width, a.i = l;
                            break
                        }
                        if (d === t) {
                            var a = {};
                            a.cut = !1, a.left = s.left, a.boxLeft = a.left, a.boxRight = a.left + s.width, a.i = l;
                            break
                        }
                        if (c === t) {
                            var a = {};
                            a.cut = !1, a.left = s.left + s.width, a.boxLeft = a.left, a.boxRight = a.left, a.i = l + 1;
                            break
                        }
                        if (t < d && t > n) {
                            var a = {};
                            a.cut = !0, a.left = s.left, a.boxLeft = a.left, a.boxRight = a.left, a.i = l;
                            break
                        }
                        n = c
                    }
                    if (!a) {
                        var u = this.itline[this.itline.length - 1],
                            a = {};
                        a.cut = !0, a.left = u.left + u.width, a.boxLeft = a.left, a.boxRight = a.left, a.i = null
                    }
                    return this.t.pixels[t] = a, a
                }, this.getDate = function(e, t, i) {
                    var n = this.ch(e, i);
                    if (!n) return null;
                    var a = n.x,
                        o = this.itline[a];
                    if (!o) return null;
                    var r = i && !t ? o.end : o.start;
                    return t ? r.addTime(this.dh(n.cell, n.offset)) : r
                }, this.ch = function(e, t) {
                    if (0 === this.itline.length) return null;
                    for (var i = 0, n = 0, a = 0; a < this.itline.length; a++) {
                        var o = this.itline[a];
                        if (i += o.width || this.cellWidth, e < i || t && e === i) {
                            var r = {};
                            return r.x = a, r.offset = e - n, r.cell = o, r
                        }
                        n = i
                    }
                    var o = c.itline.last(),
                        r = {};
                    return r.x = this.itline.length - 1, r.offset = o.width || this.cellWidth, r.cell = o, r
                }, this.eh = function(e) {
                    return this.bh(e)
                }, this.fh = function(e) {
                    for (var t = new DayPilot.Date(e), i = 0; i < this.itline.length; i++) {
                        var n = this.itline[i];
                        if (n.start.ticks <= t.ticks && t.ticks < n.end.ticks) {
                            var a = {};
                            return a.hidden = !1, a.current = n, a
                        }
                        if (t.ticks < n.start.ticks) {
                            var a = {};
                            return a.hidden = !0, a.previous = i > 0 ? this.itline[i - 1] : null, a.current = null, a.next = this.itline[i], a
                        }
                    }
                    var a = {};
                    return a.past = !0, a.previous = this.itline[this.itline.length - 1], a
                }, this.bh = function(e) {
                    var t = new DayPilot.Date(e),
                        i = 0,
                        n = this.itline[0];
                    if (t.ticks < n.start.ticks) {
                        var a = {};
                        return a.hidden = !0, a.previous = null, a.current = null, a.next = this.itline[0], a.i = -1, a
                    }
                    var o = this.itline.length - 1,
                        r = this.itline[o];
                    if (t.ticks > r.end.ticks) {
                        var a = {};
                        return a.past = !0, a.previous = this.itline[this.itline.length - 1], a.i = this.itline.length, a
                    }
                    if (t.ticks === r.end.ticks) {
                        var a = {};
                        return a.past = !1, a.current = this.itline[this.itline.length - 1], a.i = this.itline.length - 1, a
                    }
                    for (var l = function() {
                            for (var e, n = c.itline; i <= o;)
                                if (e = Math.floor((i + o) / 2), n[e].start.ticks > t.ticks) o = e - 1;
                                else {
                                    if (!(n[e].end.ticks < t.ticks)) return e;
                                    i = e + 1
                                } return i
                        }(), s = l; s < this.itline.length; s++) {
                        var d = this.itline[s];
                        if (d.start.ticks <= t.ticks && t.ticks < d.end.ticks) {
                            var a = {};
                            return a.hidden = !1, a.current = d, a.i = s, a
                        }
                        if (t.ticks < d.start.ticks) {
                            var a = {};
                            return a.hidden = !0, a.previous = s > 0 ? this.itline[s - 1] : null, a.current = null, a.next = this.itline[s], a.i = s, a
                        }
                    }
                    var a = {};
                    return a.past = !0, a.previous = this.itline[this.itline.length - 1], a.i = this.itline.length, a
                }, this.od = function(e, t) {
                    var i = e.width || this.cellWidth,
                        n = e.end.ticks - e.start.ticks;
                    return Math.floor(i * t / n)
                }, this.dh = function(e, t) {
                    var i = e.end.ticks - e.start.ticks,
                        n = e.width || this.cellWidth;
                    return Math.floor(t / n * i)
                }, this.We = function(e) {
                    DayPilot.Global.touch.start || n.preventEventClick || (A = {}, c.Ia(this, e))
                }, this.eventClickPostBack = function(e, t) {
                    this.F("EventClick", e, t)
                }, this.eventClickCallBack = function(e, t) {
                    this.H("EventClick", e, t)
                }, this.Ia = function(e, t) {
                    var i = e.event;
                    if (i) {
                        var t = t || window.event;
                        if ("undefined" != typeof DayPilot.Bubble && DayPilot.Bubble.hideActive(), !i.client.doubleClickEnabled()) return void c.Ja(e, t);
                        c.timeouts.click || (c.timeouts.click = []);
                        var n = function(e, t) {
                            return function() {
                                c.Ja(e, t)
                            }
                        };
                        c.timeouts.click.push(window.setTimeout(n(e, t), c.doubleClickTimeout))
                    }
                }, this.gh = function(e) {
                    return c.itline[e]
                }, this.Ja = function(e, t) {
                    if ("boolean" == typeof t) throw "Invalid _eventClickSingle parameters";
                    var i = e.event;
                    if (i) {
                        var n = t.ctrlKey,
                            a = t.metaKey;
                        if (i.client.clickEnabled())
                            if (c.va()) {
                                var o = {};
                                if (o.e = i, o.div = e, o.originalEvent = t, o.ctrl = n, o.meta = a, o.shift = t.shiftKey, o.preventDefault = function() {
                                        this.preventDefault.value = !0
                                    }, o.control = c, o.toJSON = function() {
                                        return DayPilot.Util.copyProps(o, {}, ["e", "ctrl", "meta", "shift"])
                                    }, "function" == typeof c.onEventClick && (c.onEventClick(o), o.preventDefault.value)) return;
                                switch (c.eventClickHandling) {
                                    case "PostBack":
                                        c.eventClickPostBack(i);
                                        break;
                                    case "CallBack":
                                        c.eventClickCallBack(i);
                                        break;
                                    case "Edit":
                                        c.Ka(e);
                                        break;
                                    case "Select":
                                        c.La(e, i, n, a);
                                        break;
                                    case "ContextMenu":
                                        var r = i.client.contextMenu();
                                        r ? r.show(i) : c.contextMenu && c.contextMenu.show(i);
                                        break;
                                    case "Bubble":
                                        c.bubble && c.bubble.showEvent(i);
                                        break;
                                    case "RectangleSelect":
                                        if ("Disabled" !== c.multiSelectRectangle) return M.start(), !1
                                }
                                "function" == typeof c.onEventClicked && c.onEventClicked(o)
                            } else switch (c.eventClickHandling) {
                                case "PostBack":
                                    c.eventClickPostBack(i);
                                    break;
                                case "CallBack":
                                    c.eventClickCallBack(i);
                                    break;
                                case "JavaScript":
                                    c.onEventClick(i);
                                    break;
                                case "Edit":
                                    c.Ka(e);
                                    break;
                                case "Select":
                                    c.La(e, i, n, a);
                                    break;
                                case "ContextMenu":
                                    var r = i.client.contextMenu();
                                    r ? r.show(i) : c.contextMenu && c.contextMenu.show(i);
                                    break;
                                case "Bubble":
                                    c.bubble && c.bubble.showEvent(i);
                                    break;
                                case "RectangleSelect":
                                    if ("Disabled" !== c.multiSelectRectangle) return M.start(), !1
                            }
                    }
                }, this.Ra = function(e) {
                    if (c.va()) {
                        var t = {};
                        if (t.e = e, t.preventDefault = function() {
                                this.preventDefault.value = !0
                            }, t.control = c, t.toJSON = function() {
                                return DayPilot.Util.copyProps(t, {}, ["e"])
                            }, "function" == typeof c.onEventDelete && (c.onEventDelete(t), t.preventDefault.value)) return;
                        switch (c.eventDeleteHandling) {
                            case "PostBack":
                                c.eventDeletePostBack(e);
                                break;
                            case "CallBack":
                                c.eventDeleteCallBack(e);
                                break;
                            case "Update":
                                c.events.remove(e)
                        }
                        "function" == typeof c.onEventDeleted && c.onEventDeleted(t)
                    } else switch (c.eventDeleteHandling) {
                        case "PostBack":
                            c.eventDeletePostBack(e);
                            break;
                        case "CallBack":
                            c.eventDeleteCallBack(e);
                            break;
                        case "JavaScript":
                            c.onEventDelete(e)
                    }
                }, this.eventDeletePostBack = function(e, t) {
                    this.F("EventDelete", e, t)
                }, this.eventDeleteCallBack = function(e, t) {
                    this.H("EventDelete", e, t)
                }, this.setHScrollPosition = function(e) {
                    this.nav.scroll.scrollLeft = e
                }, this.getScrollX = function() {
                    return this.nav.scroll.scrollLeft
                }, this.getHScrollPosition = this.getScrollX, this.getScrollY = function() {
                    return this.nav.scroll.scrollTop
                }, this.La = function(e, t, i, n) {
                    c.fb(e, t, i, n)
                }, this.eventSelectPostBack = function(e, t, i) {
                    var n = {};
                    n.e = e, n.change = t, this.F("EventSelect", n, i)
                }, this.eventSelectCallBack = function(e, t, i) {
                    var n = {};
                    n.e = e, n.change = t, this.H("EventSelect", n, i)
                }, this.fb = function(e, t, i, n) {
                    var a = c.multiselect,
                        o = a.isSelected(t);
                    if (i || n || !o || 1 !== a.list.length)
                        if (c.va()) {
                            a.previous = a.events();
                            var r = {};
                            if (r.e = t, r.selected = o, r.ctrl = i, r.meta = n, r.preventDefault = function() {
                                    this.preventDefault.value = !0
                                }, "function" == typeof c.onEventSelect && (c.onEventSelect(r), r.preventDefault.value)) return;
                            switch (c.eventSelectHandling) {
                                case "PostBack":
                                    c.eventSelectPostBack(t, l);
                                    break;
                                case "CallBack":
                                    "undefined" != typeof WebForm_InitCallback && (window.hb = "", window.ib = [], WebForm_InitCallback()), c.eventSelectCallBack(t, l);
                                    break;
                                case "Update":
                                    a.jb(e, i, n)
                            }
                            "function" == typeof c.onEventSelected && (r.change = a.isSelected(t) ? "selected" : "deselected", r.selected = a.isSelected(t), c.onEventSelected(r))
                        } else {
                            a.previous = a.events(), a.jb(e, i, n);
                            var l = a.isSelected(t) ? "selected" : "deselected";
                            switch (c.eventSelectHandling) {
                                case "PostBack":
                                    c.eventSelectPostBack(t, l);
                                    break;
                                case "CallBack":
                                    "undefined" != typeof WebForm_InitCallback && (window.hb = "", window.ib = [], WebForm_InitCallback()), c.eventSelectCallBack(t, l);
                                    break;
                                case "JavaScript":
                                    c.onEventSelect(t, l)
                            }
                        }
                }, this.eventRightClickPostBack = function(e, t) {
                    this.F("EventRightClick", e, t)
                }, this.eventRightClickCallBack = function(e, t) {
                    this.H("EventRightClick", e, t)
                }, this.Na = function(e) {
                    if (!DayPilot.Global.touch.active && !DayPilot.Global.touch.start) {
                        var t = this.event;
                        if (e = e || window.event, e.cancelBubble = !0, !this.event.client.rightClickEnabled()) return !1;
                        if (c.va()) {
                            var i = {};
                            if (i.e = t, i.div = this, i.originalEvent = e, i.preventDefault = function() {
                                    this.preventDefault.value = !0
                                }, "function" == typeof c.onEventRightClick && (c.onEventRightClick(i), i.preventDefault.value)) return !1;
                            switch (c.eventRightClickHandling) {
                                case "PostBack":
                                    c.eventRightClickPostBack(t);
                                    break;
                                case "CallBack":
                                    c.eventRightClickCallBack(t);
                                    break;
                                case "ContextMenu":
                                    var n = t.client.contextMenu();
                                    n ? n.show(t) : c.contextMenu && c.contextMenu.show(this.event);
                                    break;
                                case "Bubble":
                                    c.bubble && c.bubble.showEvent(t)
                            }
                            "function" == typeof c.onEventRightClicked && c.onEventRightClicked(i)
                        } else switch (c.eventRightClickHandling) {
                            case "PostBack":
                                c.eventRightClickPostBack(t);
                                break;
                            case "CallBack":
                                c.eventRightClickCallBack(t);
                                break;
                            case "JavaScript":
                                c.onEventRightClick(t);
                                break;
                            case "ContextMenu":
                                var n = t.client.contextMenu();
                                n ? n.show(t) : c.contextMenu && c.contextMenu.show(this.event);
                                break;
                            case "Bubble":
                                c.bubble && c.bubble.showEvent(t)
                        }
                        return !1
                    }
                }, this.eventDoubleClickPostBack = function(e, t) {
                    this.F("EventDoubleClick", e, t)
                }, this.eventDoubleClickCallBack = function(e, t) {
                    this.H("EventDoubleClick", e, t)
                }, this.Ma = function(e) {
                    if ("undefined" != typeof DayPilot.Bubble && DayPilot.Bubble.hideActive(), c.timeouts.click) {
                        for (var t in c.timeouts.click) window.clearTimeout(c.timeouts.click[t]);
                        c.timeouts.click = null
                    }
                    var e = e || window.event,
                        i = this.event;
                    if (e.stopPropagation && e.stopPropagation(), e.cancelBubble = !0, c.va()) {
                        var n = {};
                        if (n.e = i, n.originalEvent = e, n.preventDefault = function() {
                                this.preventDefault.value = !0
                            }, "function" == typeof c.onEventDoubleClick && (c.onEventDoubleClick(n), n.preventDefault.value)) return;
                        switch (c.eventDoubleClickHandling) {
                            case "PostBack":
                                c.eventDoubleClickPostBack(i);
                                break;
                            case "CallBack":
                                c.eventDoubleClickCallBack(i);
                                break;
                            case "Edit":
                                c.Ka(this);
                                break;
                            case "Select":
                                c.La(div, i, e.ctrlKey, e.metaKey);
                                break;
                            case "Bubble":
                                c.bubble && c.bubble.showEvent(i);
                                break;
                            case "ContextMenu":
                                var a = i.client.contextMenu();
                                a ? a.show(i) : c.contextMenu && c.contextMenu.show(i)
                        }
                        "function" == typeof c.onEventDoubleClicked && c.onEventDoubleClicked(n)
                    } else switch (c.eventDoubleClickHandling) {
                        case "PostBack":
                            c.eventDoubleClickPostBack(i);
                            break;
                        case "CallBack":
                            c.eventDoubleClickCallBack(i);
                            break;
                        case "JavaScript":
                            c.onEventDoubleClick(i);
                            break;
                        case "Edit":
                            c.Ka(this);
                            break;
                        case "Select":
                            c.La(div, i, e.ctrlKey, e.metaKey);
                            break;
                        case "Bubble":
                            c.bubble && c.bubble.showEvent(i);
                            break;
                        case "ContextMenu":
                            var a = i.client.contextMenu();
                            a ? a.show(i) : c.contextMenu && c.contextMenu.show(i)
                    }
                }, this.eventResizePostBack = function(e, t, i, n) {
                    this.Va("PostBack", e, t, i, n)
                }, this.eventResizeCallBack = function(e, t, i, n) {
                    this.Va("CallBack", e, t, i, n)
                }, this.Va = function(e, t, i, n, a) {
                    var o = {};
                    o.e = t, o.newStart = i, o.newEnd = n, this.Sa(e, "EventResize", o, a)
                }, this.Ua = function(e, t, i) {
                    if ("Disabled" !== this.eventResizeHandling)
                        if (i = c.mf(i), c.va()) {
                            var n = {},
                                a = function() {
                                    if (C.hide(), !n.preventDefault.value) {
                                        switch (t = n.newStart, i = n.newEnd, c.eventResizeHandling) {
                                            case "PostBack":
                                                c.eventResizePostBack(e, t, i);
                                                break;
                                            case "CallBack":
                                                c.eventResizeCallBack(e, t, i);
                                                break;
                                            case "Notify":
                                                c.eventResizeNotify(e, t, i);
                                                break;
                                            case "Update":
                                                c.hh(n)
                                        }
                                        "function" == typeof c.onEventResized && c.onEventResized(n)
                                    }
                                };
                            n.e = e, n.async = !1, n.loaded = function() {
                                a()
                            }, n.newStart = t, n.newEnd = i, n.preventDefault = function() {
                                this.preventDefault.value = !0
                            }, n.multiresize = DayPilot.list(b.list), n.control = c, n.toJSON = function() {
                                return DayPilot.Util.copyProps(n, {}, ["e", "async", "newStart", "newEnd", "multiresize"])
                            };
                            var o = {};
                            o.event = e, o.start = t, o.end = i, n.multiresize.splice(0, 0, o), "function" == typeof c.onEventResize && c.onEventResize(n), n.async ? c.blockOnCallBack && C.show() : a()
                        } else switch (c.eventResizeHandling) {
                            case "PostBack":
                                c.eventResizePostBack(e, t, i);
                                break;
                            case "CallBack":
                                c.eventResizeCallBack(e, t, i);
                                break;
                            case "JavaScript":
                                c.onEventResize(e, t, i);
                                break;
                            case "Notify":
                                c.eventResizeNotify(e, t, i);
                                break;
                            case "Update":
                                c.hh(n)
                        }
                }, this.eventMovePostBack = function(e, t, i, n, a, o) {
                    this.Za("PostBack", e, t, i, n, a, o)
                }, this.eventMoveCallBack = function(e, t, i, n, a, o) {
                    this.Za("CallBack", e, t, i, n, a, o)
                }, this.Za = function(e, t, i, n, a, o, r) {
                    var l = t && !!t.preventDefault,
                        s = {};
                    if (l) {
                        var d = t;
                        s.e = d.e, s.newStart = d.newStart, s.newEnd = d.newEnd, s.newResource = d.newResource, s.position = d.position, s.multimove = [], d.multimove.each(function(e) {
                            var t = {};
                            if (t.e = e.event, t.newStart = e.start, t.newEnd = e.end, t.newResource = e.event.resource(), t.e === d.e || t.e.data.id == d.e.data.id) t.newResource = d.newResource;
                            else if (w.rowoffset) {
                                var i = e.event.part.dayIndex + w.rowoffset;
                                t.newResource = c.rowlist[i].id
                            }
                            s.multimove.push(t)
                        })
                    } else s.e = t, s.newStart = i, s.newEnd = n, s.newResource = a, s.position = r;
                    this.Sa(e, "EventMove", s, o)
                }, this.ih = function(e, t, i) {
                    var n = {};
                    n.start = t.start, n.end = t.end, n.resource = t.resource, n.multirange = [], t.multirange.each(function(e) {
                        var t = {};
                        t.start = e.start, t.end = e.end, t.resource = e.resource, n.multirange.push(t)
                    }), this.Sa(e, "TimeRangeSelected", n, i)
                }, this.Sa = function(e, t, i, n) {
                    if ("PostBack" === e) c.F(t, i, n);
                    else if ("CallBack" === e) c.H(t, i, n, "CallBack");
                    else if ("Immediate" === e) c.H(t, i, n, "Notify");
                    else if ("Queue" === e) c.queue.add(new DayPilot.Action(this, t, i, n));
                    else {
                        if ("Notify" !== e) throw "Invalid event invocation type";
                        "Notify" === _.notifyType() ? c.H(t, i, n, "Notify") : c.queue.add(new DayPilot.Action(c, t, i, n))
                    }
                }, this.eventMoveNotify = function(e, t, i, n, a, o) {
                    if (e && !!e.preventDefault) {
                        var r = e;
                        r.old = new DayPilot.Event(r.e.copy(), c), r.multimove.each(function(e) {
                            e.old = new DayPilot.Event(e.event.copy(), c)
                        }), c.hh(r), r.e = r.old, delete r.old, r.multimove.each(function(e) {
                            e.event = e.old, delete e.old
                        })
                    } else {
                        e = new DayPilot.Event(e.copy(), c);
                        var l = c.events.jh(e.data);
                        e.start(t), e.end(i), e.resource(n), e.commit(), l = l.concat(c.events.kh(e.data)), c.lh(l), c.mh(), c.nh(l)
                    }
                    this.Za("Notify", e, t, i, n, a, o)
                }, this.eventResizeNotify = function(e, t, i, n) {
                    var a = new DayPilot.Event(e.copy(), this),
                        o = c.events.jh(e.data);
                    e.start(t), e.end(i), e.commit(), o = o.concat(c.events.kh(e.data)), c.lh(o), c.mh(), c.nh(o), this.Va("Notify", a, t, i, n)
                }, this.multiselect = {}, this.multiselect.list = [], this.multiselect.divs = [], this.multiselect.previous = [], this.multiselect.kg = function(e) {
                    c.multiselect.list = DayPilot.list(e).map(function(e) {
                        return new DayPilot.Event(e, c)
                    })
                }, this.multiselect.Bb = function() {
                    var e = c.multiselect;
                    return JSON.stringify(e.events())
                }, this.multiselect.events = function() {
                    var e = c.multiselect,
                        t = DayPilot.list();
                    t.ignoreToJSON = !0;
                    for (var i = 0; i < e.list.length; i++) t.push(e.list[i]);
                    return t
                }, this.multiselect.Cb = function() {}, this.multiselect.jb = function(e, t, i) {
                    var n = c.multiselect;
                    if (n.isSelected(e.event))
                        if (c.allowMultiSelect)
                            if (t || i) n.remove(e.event, !0);
                            else {
                                var a = n.list.length;
                                n.clear(), a > 1 && n.add(e.event, !0)
                            }
                    else n.clear();
                    else c.allowMultiSelect && (t || i) ? n.add(e.event, !0) : (n.clear(), n.add(e.event, !0));
                    n.ld(e), n.Cb()
                }, this.multiselect.Db = function(e) {
                    var t = c.multiselect;
                    return t.Eb(e, t.initList)
                }, this.multiselect.Fb = function() {
                    for (var e = c.multiselect, t = [], i = 0; i < e.list.length; i++) {
                        var n = e.list[i];
                        t.push(n.value())
                    }
                    alert(t.join("\n"))
                }, this.multiselect.add = function(e, t) {
                    var i = c.multiselect;
                    i.indexOf(e) === -1 && i.list.push(e), t || i.oh(e)
                }, this.multiselect.remove = function(e, t) {
                    var i = c.multiselect,
                        n = i.indexOf(e);
                    n !== -1 && i.list.splice(n, 1), t || i.oh(e)
                }, this.multiselect.clear = function(e) {
                    var t = c.multiselect;
                    t.list = [], e || t.redraw()
                }, this.multiselect.redraw = function() {
                    var e = c.multiselect;
                    e.divs = [];
                    for (var t = 0; t < c.elements.events.length; t++) {
                        var i = c.elements.events[t];
                        i.event && i.event.isEvent && (e.isSelected(i.event) ? e.Hb(i) : e.Ib(i))
                    }
                }, this.multiselect.ld = function(e) {
                    if (e) {
                        var t = c.multiselect;
                        t.isSelected(e.event) ? t.Hb(e) : t.Ib(e)
                    }
                }, this.multiselect.oh = function(e) {
                    var t = c.multiselect,
                        i = c.jd(e);
                    t.ld(i)
                }, this.multiselect.Hb = function(e) {
                    var t = c.multiselect,
                        i = c.q("_selected"),
                        e = t.Jb(e);
                    DayPilot.Util.addClass(e, i), t.divs.push(e)
                }, this.multiselect.Jb = function(e) {
                    return e
                }, this.multiselect.Lb = function() {
                    for (var e = c.multiselect, t = 0; t < e.divs.length; t++) {
                        var i = e.divs[t];
                        e.Ib(i, !0)
                    }
                    e.divs = []
                }, this.multiselect.Ib = function(e, t) {
                    var i = c.multiselect,
                        n = c.q("_selected");
                    if (DayPilot.Util.removeClass(e, n), !t) {
                        var a = DayPilot.indexOf(i.divs, e);
                        a !== -1 && i.divs.splice(a, 1)
                    }
                }, this.multiselect.isSelected = function(e) {
                    return !!e && (!!e.isEvent && c.multiselect.Eb(e, c.multiselect.list))
                }, this.multiselect.indexOf = function(e) {
                    for (var t = e.data, i = 0; i < c.multiselect.list.length; i++) {
                        if (c.multiselect.list[i].data === t) return i
                    }
                    return -1
                }, this.multiselect.Eb = function(e, t) {
                    if (!t) return !1;
                    for (var i = 0; i < t.length; i++) {
                        var n = t[i];
                        if (e === n) return !0;
                        if ("function" == typeof n.id) {
                            if (null !== n.id() && null !== e.id() && n.id() === e.id()) return !0;
                            if (null === n.id() && null === e.id() && n.recurrentMasterId() === e.recurrentMasterId() && e.start().toStringSortable() === n.start().toStringSortable()) return !0
                        } else {
                            if (null !== n.id && null !== e.id() && n.id === e.id()) return !0;
                            if (null === n.id && null === e.id() && n.recurrentMasterId === e.recurrentMasterId() && e.start().toStringSortable() === n.start) return !0
                        }
                    }
                    return !1
                }, this.multiselect.startRectangle = function() {
                    M.start()
                }, this.ph = function() {
                    this.jg(), this.mg()
                }, this.qh = function() {
                    c.dg(), c.hg(), c.rh(), c.progressiveRowRendering && c.uh()
                }, this.ld = function(e) {
                    var e = e || {},
                        t = !e.eventsOnly;
                    c.cssOnly || (c.cssOnly = !0, DayPilot.Util.log("DayPilot: cssOnly = false mode is not supported since DayPilot Pro 8.0.")), t && (this.r() || (c.timeHeader = null, c.cellProperties = {}), c.vh = {}, c.ag(), c.bg(), e && e.dontLoadResources || c.cg(), c.events.wh(), c.zc.clearCache()), this.da(), this.xh(), c.dg(), t && (c.nd(), c.yh(), c.eg(), c.qg(c.selectedRows), c.zh()), c.fg(), c.hg(), c.rh(), c.ea(), v.hideLinkpoints(), this.O(), this.ig(), this._f(), this.ca(), this.lg(), this.ph(), c.la(), e.immediateEvents || e.eventsOnly ? c.qa() : setTimeout(function() {
                        c.qa()
                    }, 100), this.visible ? c.Wf != c.visible && this.show() : this.hide(), this.Wf = this.visible, this.pg(), (c.kd.scope || c.sg.enabled) && !c.A || this.rg(), this.Ah(), this.ta()
                }, this.Ah = function() {
                    if ("function" == typeof c.onAfterUpdate) {
                        var e = {};
                        c.onAfterUpdate(e)
                    }
                }, this.update = function(e) {
                    if (c.Kd) throw new DayPilot.Exception("You are trying to update a DayPilot.Scheduler object that has been disposed already. Calling .dispose() destroys the object and makes it unusable.");
                    f.request(e)
                };
                var f = {};
                f.timeout = null, f.options = null, f.enabled = !1, f.request = function(e) {
                    f.enabled ? (clearTimeout(f.timeout), f.mergeOptions(e), f.timeout = setTimeout(f.doit)) : (f.mergeOptions(e), f.doit())
                }, f.mergeOptions = function(e) {
                    if (e) {
                        f.options || (f.options = {});
                        for (var t in e) f.options[t] = e[t]
                    }
                }, f.doit = function() {
                    var e = f.options;
                    return f.options = null, c.A ? DayPilot.Util.isOnlyProperty(e, "events") ? (c.events.list = e.events, void c.ld({
                        "eventsOnly": !0
                    })) : DayPilot.Util.isOnlyProperty(e, "separators") ? (c.separators = e.separators, void c.ph()) : DayPilot.Util.isOnlyProperty(e, "rowHeaderColumns") ? (c.rowHeaderColumns = e.rowHeaderColumns, void c.qh()) : (c.Ad(e), c.ld({
                        "immediateEvents": !0
                    }), void c._(null, !1)) : void c.Ad(e)
                }, this.nh = function(e, t, i) {
                    e = DayPilot.ua(e);
                    for (var n = 0; n < e.length; n++) {
                        var a = e[n];
                        c.Bh(a)
                    }
                    if (this.Ch) {
                        this.fg(), this.hg(), this.ig(), this.jg();
                        for (var n = 0; n < e.length; n++) {
                            var a = e[n];
                            this.Dh(a)
                        }
                        for (var n = 0; n < e.length; n++) {
                            var a = e[n];
                            this.Eh(a)
                        }
                        this.lg(), this.mg(), this.Fh()
                    } else {
                        for (var n = 0; n < e.length; n++) {
                            var a = e[n];
                            t || this.Dh(a), this.Eh(a)
                        }
                        DayPilot.list(e).each(function(e) {
                            c.Gh(e)
                        }), c.lg()
                    }
                    c.Dg(), v.load(), c.multiselect.redraw(), i && i(), this._f()
                }, this.mf = function(e) {
                    return "DateTime" === c.eventEndSpec ? e : e.getDatePart().ticks === e.ticks ? e.addDays(-1) : e.getDatePart()
                }, this.Me = function(e) {
                    return "DateTime" === c.eventEndSpec ? e : e.getDatePart().addDays(1)
                }, this.of = function(e) {
                    return "DateTime" === c.eventEndSpec ? e : e.getDatePart()
                }, this.Xa = function(e, t, i, n, a, o, r) {
                    if (c.Lc = null, "Disabled" !== c.eventMoveHandling) {
                        i = c.mf(i);
                        var l = {};
                        l.e = e, l.newStart = t, l.newEnd = i, l.newResource = n, l.external = a, l.ctrl = !1, l.meta = !1, l.shift = !1, o && (l.shift = o.shiftKey, l.ctrl = o.ctrlKey, l.meta = o.metaKey), l.multimove = DayPilot.list(w.list), l.areaData = DayPilot.Global.movingAreaData, l.control = c, l.toJSON = function() {
                            return DayPilot.Util.copyProps(l, {}, ["e", "newStart", "newEnd", "newResource", "external", "ctrl", "meta", "shift", "multimove", "areaData"])
                        };
                        var s = {};
                        if (s.event = e, s.start = t, s.end = i, s.overlapping = !1, s.resource = n, l.multimove.splice(0, 0, s), l.position = r, l.preventDefault = function() {
                                this.preventDefault.value = !0
                            }, c.va()) {
                            var d = function() {
                                if (C.hide(), l.preventDefault.value) return void c.Hh();
                                switch (t = l.newStart, i = l.newEnd, c.eventMoveHandling) {
                                    case "PostBack":
                                        c.eventMovePostBack(l);
                                        break;
                                    case "CallBack":
                                        c.eventMoveCallBack(l);
                                        break;
                                    case "Notify":
                                        c.eventMoveNotify(l);
                                        break;
                                    case "Update":
                                        c.hh(l)
                                }
                                c.Hh(), "function" == typeof c.onEventMoved && c.onEventMoved(l)
                            };
                            l.async = !1, l.loaded = function() {
                                d()
                            }, "function" == typeof c.onEventMove && c.onEventMove(l), l.async ? c.blockOnCallBack && C.show() : d()
                        } else {
                            switch (c.eventMoveHandling) {
                                case "PostBack":
                                    c.eventMovePostBack(l);
                                    break;
                                case "CallBack":
                                    c.eventMoveCallBack(l);
                                    break;
                                case "JavaScript":
                                    c.onEventMove(e, t, i, n, a, !!o && o.ctrlKey, !!o && o.shiftKey, r);
                                    break;
                                case "Notify":
                                    c.eventMoveNotify(l);
                                    break;
                                case "Update":
                                    c.hh(l)
                            }
                            c.Hh()
                        }
                    }
                }, this.hh = function(e) {
                    var t = e.e,
                        i = e.newStart,
                        n = e.newEnd,
                        a = e.newResource,
                        o = e.external;
                    t.start(i), t.end(n), t.resource(a), o ? (t.commit(), c.events.add(t)) : c.events.update(t), e.multimove && !e.multimove.isEmpty() && e.multimove.each(function(e) {
                        if (e.event !== t) {
                            if (e.event.start(e.start), e.event.end(e.end), w.rowoffset) {
                                var i = e.event.part.dayIndex + w.rowoffset,
                                    n = c.rowlist[i].id;
                                e.event.resource(n)
                            }
                            e.event.commit(), c.events.update(e.event)
                        }
                    }), e.multiresize && !e.multiresize.isEmpty() && e.multiresize.each(function(e) {
                        e.event !== t && (e.event.start(e.start), e.event.end(e.end), e.event.commit(), c.events.update(e.event))
                    }), c.events.immediateRefresh(), c.Ya()
                }, this.$a = function(e, t) {
                    var i = c._a(t),
                        n = {};
                    n.args = e, n.guid = i, c.H("Bubble", n)
                }, this._a = function(e) {
                    var t = DayPilot.guid();
                    return this.bubbles || (this.bubbles = []), this.bubbles[t] = e, t
                }, this.eventMenuClickPostBack = function(e, t, i) {
                    var n = {};
                    n.e = e, n.command = t, this.F("EventMenuClick", n, i)
                }, this.eventMenuClickCallBack = function(e, t, i) {
                    var n = {};
                    n.e = e, n.command = t, this.H("EventMenuClick", n, i)
                }, this.ab = function(e, t, i) {
                    switch (i) {
                        case "PostBack":
                            c.eventMenuClickPostBack(t, e);
                            break;
                        case "CallBack":
                            c.eventMenuClickCallBack(t, e)
                    }
                }, this.timeRangeSelectedPostBack = function(e, t, i, n) {
                    var a = {};
                    a.start = e, a.end = t, a.resource = i, this.F("TimeRangeSelected", a, n)
                }, this.timeRangeSelectedCallBack = function(e, t, i, n) {
                    var a = {};
                    a.start = e, a.end = t, a.resource = i, this.H("TimeRangeSelected", a, n)
                }, this.Ih = function(e) {
                    if (e) {
                        if (e.disabled) return void c.clearSelection();
                        var t = c.Jh(e);
                        t && c.cb(t.start, t.end, t.resource)
                    }
                }, this.cb = function(e, t, i) {
                    if ("Disabled" !== c.timeRangeSelectedHandling) {
                        var n = t;
                        t = c.mf(n);
                        var a = {};
                        if (a.control = c, a.start = e, a.end = t, a.resource = i, a.preventDefault = function() {
                                this.preventDefault.value = !0
                            }, a.multirange = DayPilot.list(H.list).map(function(e) {
                                return c.Jh(e)
                            }), a.multirange.isEmpty() && a.multirange.push({
                                "start": a.start,
                                "end": a.end,
                                "resource": a.resource
                            }), a.toJSON = function() {
                                return DayPilot.Util.copyProps(a, {}, ["start", "end", "resource", "multirange"])
                            }, c.va()) {
                            if ("function" == typeof c.onTimeRangeSelect) {
                                if (c.onTimeRangeSelect(a), a.preventDefault.value) return;
                                e = a.start, t = a.end
                            }
                            switch (c.Kh(c.rangeHold, e, t, a.multirange), c.Lh(c.rangeHold), c.timeRangeSelectedHandling) {
                                case "PostBack":
                                    c.ih("PostBack", a);
                                    break;
                                case "CallBack":
                                    c.ih("CallBack", a)
                            }
                            "function" == typeof c.onTimeRangeSelected && c.onTimeRangeSelected(a)
                        } else switch (c.timeRangeSelectedHandling) {
                            case "PostBack":
                                c.ih("PostBack", a);
                                break;
                            case "CallBack":
                                c.ih("CallBack", a);
                                break;
                            case "JavaScript":
                                c.onTimeRangeSelected(e, t, i, a.multirange);
                                break;
                            case "Hold":
                        }
                    }
                }, this.Kh = function(e, t, i, n) {
                    if (e && !(n && n.length > 1)) {
                        var a, o, r = c.Me(i),
                            l = 0;
                        if ("Days" === c.viewType) {
                            l = c.ug(t).start.getTime() - c.Da().getTime()
                        }
                        t.getTime() < c.itline[0].start.getTime() ? e.start.x = 0 : (a = c.eh(t.addTime(-l)), o = a.current || a.previous, e.start.x = DayPilot.indexOf(c.itline, o)), a = c.eh(r.addSeconds(-1).addTime(-l)), a.past ? e.end.x = c.itline.length - 1 : (o = a.current || a.next, e.end.x = DayPilot.indexOf(c.itline, o))
                    }
                }, this.ug = function(e) {
                    var t = e.getDatePart();
                    return DayPilot.list(c.rowlist).find(function(e) {
                        return e.start.getDatePart() === t
                    })
                }, this.timeRangeDoubleClickPostBack = function(e, t, i, n) {
                    var a = {};
                    a.start = e, a.end = t, a.resource = i, this.F("TimeRangeDoubleClick", a, n)
                }, this.timeRangeDoubleClickCallBack = function(e, t, i, n) {
                    var a = {};
                    a.start = e, a.end = t, a.resource = i, this.H("TimeRangeDoubleClick", a, n)
                }, this.db = function(e, t, i) {
                    if (t = c.mf(t), c.va()) {
                        var n = {};
                        if (n.start = e, n.end = t, n.resource = i, n.preventDefault = function() {
                                this.preventDefault.value = !0
                            }, "function" == typeof c.onTimeRangeDoubleClick && (c.onTimeRangeDoubleClick(n), n.preventDefault.value)) return;
                        switch (c.timeRangeDoubleClickHandling) {
                            case "PostBack":
                                c.timeRangeDoubleClickPostBack(e, t, i);
                                break;
                            case "CallBack":
                                c.timeRangeDoubleClickCallBack(e, t, i)
                        }
                        "function" == typeof c.onTimeRangeDoubleClicked && c.onTimeRangeDoubleClicked(n)
                    } else switch (c.timeRangeDoubleClickHandling) {
                        case "PostBack":
                            c.timeRangeDoubleClickPostBack(e, t, i);
                            break;
                        case "CallBack":
                            c.timeRangeDoubleClickCallBack(e, t, i);
                            break;
                        case "JavaScript":
                            c.onTimeRangeDoubleClick(e, t, i)
                    }
                }, this.timeRangeMenuClickPostBack = function(e, t, i) {
                    var n = {};
                    n.selection = e, n.command = t, this.F("TimeRangeMenuClick", n, i)
                }, this.timeRangeMenuClickCallBack = function(e, t, i) {
                    var n = {};
                    n.selection = e, n.command = t, this.H("TimeRangeMenuClick", n, i)
                }, this.bb = function(e, t, i) {
                    switch (i) {
                        case "PostBack":
                            c.timeRangeMenuClickPostBack(t, e);
                            break;
                        case "CallBack":
                            c.timeRangeMenuClickCallBack(t, e)
                    }
                }, this.linkMenuClickPostBack = function(e, t, i) {
                    var n = {};
                    n.link = e, n.command = t, this.F("LinkMenuClick", n, i)
                }, this.linkMenuClickCallBack = function(e, t, i) {
                    var n = {};
                    n.link = e, n.command = t, this.H("LinkMenuClick", n, i)
                }, this.Mh = function(e, t, i) {
                    switch (i) {
                        case "PostBack":
                            c.linkMenuClickPostBack(t, e);
                            break;
                        case "CallBack":
                            c.linkMenuClickCallBack(t, e)
                    }
                }, this.rowMenuClickPostBack = function(e, t, i) {
                    var n = {};
                    n.resource = e, n.command = t, this.F("RowMenuClick", n, i)
                }, this.resourceHeaderMenuClickPostBack = this.rowMenuClickPostBack, this.rowMenuClickCallBack = function(e, t, i) {
                    var n = {};
                    n.resource = e, n.command = t, this.H("RowMenuClick", n, i)
                }, this.resourceHeaderMenuClickCallBack = this.rowMenuClickCallBack, this.Nh = function(e, t, i) {
                    switch (i) {
                        case "PostBack":
                            c.rowMenuClickPostBack(t, e);
                            break;
                        case "CallBack":
                            c.rowMenuClickCallBack(t, e)
                    }
                }, this.Oh = function(e, t) {
                    var i = c.Ph(e);
                    i.data.name = t, i.data.html && (i.data.html = t), c.rows.update(i)
                }, this.Qh = function(e, t) {
                    if (t) {
                        var i = {};
                        if (i.text = t,
                            i.preventDefault = function() {
                                this.preventDefault.value = !0
                            }, "function" != typeof c.onRowCreate || (c.onRowCreate(i), !i.preventDefault.value)) {
                            switch (c.rowCreateHandling) {
                                case "CallBack":
                                    c.rowCreateCallBack(i.text);
                                    break;
                                case "PostBack":
                                    c.rowCreatePostBack(i.text)
                            }
                            "function" == typeof c.onRowCreated && c.onRowCreated(i)
                        }
                    }
                }, this.Rh = function(e, t, i) {
                    if (e.isNewRow) return void(i || c.Qh(e, t));
                    var n = DayPilot.indexOf(c.rowlist, e),
                        a = c.Ph(e, n);
                    if (c.va()) {
                        var o = {};
                        if (o.resource = a, o.row = a, o.newText = t, o.canceled = i, o.preventDefault = function() {
                                this.preventDefault.value = !0
                            }, "function" == typeof c.onRowEdit && (c.onRowEdit(o), o.preventDefault.value)) return;
                        if (!i) {
                            switch (c.rowEditHandling) {
                                case "PostBack":
                                    c.rowEditPostBack(a, t);
                                    break;
                                case "CallBack":
                                    c.rowEditCallBack(a, t);
                                    break;
                                case "Update":
                                    c.Oh(e, t)
                            }
                            "function" == typeof c.onRowEdited && c.onRowEdited(o)
                        }
                    } else switch (c.rowEditHandling) {
                        case "PostBack":
                            c.rowEditPostBack(a, t);
                            break;
                        case "CallBack":
                            c.rowEditCallBack(a, t);
                            break;
                        case "JavaScript":
                            c.onrowEdit(a, t)
                    }
                }, this.rowCreatePostBack = function(e, t) {
                    var i = {};
                    i.text = e, this.F("RowCreate", i, t)
                }, this.rowCreateCallBack = function(e, t) {
                    var i = {};
                    i.text = e, this.H("RowCreate", i, t)
                }, this.rowEditPostBack = function(e, t, i) {
                    var n = {};
                    n.resource = e, n.newText = t, this.F("RowEdit", n, i)
                }, this.rowEditCallBack = function(e, t, i) {
                    var n = {};
                    n.resource = e, n.newText = t, this.H("RowEdit", n, i)
                }, this.rowMovePostBack = function(e, t, i, n) {
                    var a = {};
                    a.source = e, a.target = t, a.position = i, this.F("RowMove", a, n)
                }, this.rowMoveCallBack = function(e, t, i, n) {
                    var a = {};
                    a.source = e, a.target = t, a.position = i, this.H("RowMove", a, n)
                }, this.rowMoveNotify = function(e, t, i, n) {
                    var a = {};
                    a.source = e, a.target = t, a.position = i, this.H("RowMove", a, n, "Notify")
                }, this.rowClickPostBack = function(e, t) {
                    var i = {};
                    i.resource = e, this.F("RowClick", i, t)
                }, this.resourceHeaderClickPostBack = this.rowClickPostBack, this.rowClickCallBack = function(e, t) {
                    var i = {};
                    i.resource = e, this.H("RowClick", i, t)
                }, this.resourceHeaderClickCallBack = this.rowClickCallBack, this.Sh = function(e, t, i, n) {
                    if ("Disabled" === c.rowDoubleClickHandling) return void c.Th(e, t, i, n);
                    c.timeouts.resClick || (c.timeouts.resClick = []);
                    var a = function(e, t, i, n) {
                        return function() {
                            c.Th(e, t, i, n)
                        }
                    };
                    c.timeouts.resClick.push(window.setTimeout(a(e, t, i, n), c.doubleClickTimeout))
                }, this.Th = function(e, t, i, n) {
                    var a = c.resourceHeaderClickHandling || c.rowClickHandling;
                    if (c.va()) {
                        var o = {};
                        if (o.resource = e, o.row = e, o.ctrl = t, o.shift = i, o.meta = n, o.preventDefault = function() {
                                this.preventDefault.value = !0
                            }, "function" == typeof c.onRowClick && (c.onRowClick(o), o.preventDefault.value)) return;
                        if ("function" == typeof c.onResourceHeaderClick && (c.onResourceHeaderClick(o), o.preventDefault.value)) return;
                        switch (a) {
                            case "PostBack":
                                c.rowClickPostBack(e);
                                break;
                            case "CallBack":
                                c.rowClickCallBack(e);
                                break;
                            case "Select":
                                c.Uh(e.$.row, t, i, n);
                                break;
                            case "Edit":
                                c.Vh.edit(e.$.row)
                        }
                        "function" == typeof c.onRowClicked && c.onRowClicked(o), "function" == typeof c.onResourceHeaderClicked && c.onResourceHeaderClicked(o)
                    } else switch (a) {
                        case "PostBack":
                            c.rowClickPostBack(e);
                            break;
                        case "CallBack":
                            c.rowClickCallBack(e);
                            break;
                        case "JavaScript":
                            c.onRowClick(e);
                            break;
                        case "Select":
                            c.Uh(e.$.row, t, i);
                            break;
                        case "Edit":
                            c.Vh.edit(e.$.row)
                    }
                }, this.timeHeaderClickPostBack = function(e, t) {
                    var i = {};
                    i.header = e, this.F("TimeHeaderClick", i, t)
                }, this.timeHeaderClickCallBack = function(e, t) {
                    var i = {};
                    i.header = e, this.H("TimeHeaderClick", i, t)
                }, this.Wh = function(e) {
                    if (c.va()) {
                        var t = {};
                        if (t.header = e, t.preventDefault = function() {
                                this.preventDefault.value = !0
                            }, "function" == typeof c.onTimeHeaderClick && (c.onTimeHeaderClick(t), t.preventDefault.value)) return;
                        switch (this.timeHeaderClickHandling) {
                            case "PostBack":
                                c.timeHeaderClickPostBack(e);
                                break;
                            case "CallBack":
                                c.timeHeaderClickCallBack(e)
                        }
                        "function" == typeof c.onTimeHeaderClicked && c.onTimeHeaderClicked(t)
                    } else switch (this.timeHeaderClickHandling) {
                        case "PostBack":
                            c.timeHeaderClickPostBack(e);
                            break;
                        case "CallBack":
                            c.timeHeaderClickCallBack(e);
                            break;
                        case "JavaScript":
                            c.onTimeHeaderClick(e)
                    }
                }, this.resourceCollapsePostBack = function(e, t) {
                    var i = {};
                    i.resource = e, this.F("ResourceCollapse", i, t)
                }, this.resourceCollapseCallBack = function(e, t) {
                    var i = {};
                    i.resource = e, this.H("ResourceCollapse", i, t)
                }, this.Xh = function(e) {
                    if (c.va()) {
                        var t = {};
                        if (t.resource = e, t.preventDefault = function() {
                                this.preventDefault.value = !0
                            }, "function" == typeof c.onResourceCollapse && (c.onResourceCollapse(t), t.preventDefault.value)) return;
                        switch (this.resourceCollapseHandling) {
                            case "PostBack":
                                c.resourceCollapsePostBack(e);
                                break;
                            case "CallBack":
                                c.resourceCollapseCallBack(e)
                        }
                    } else switch (this.resourceCollapseHandling) {
                        case "PostBack":
                            c.resourceCollapsePostBack(e);
                            break;
                        case "CallBack":
                            c.resourceCollapseCallBack(e);
                            break;
                        case "JavaScript":
                            c.onResourceCollapse(e)
                    }
                }, this.resourceExpandPostBack = function(e, t) {
                    var i = {};
                    i.resource = e, this.F("ResourceExpand", i, t)
                }, this.resourceExpandCallBack = function(e, t) {
                    var i = {};
                    i.resource = e, this.H("ResourceExpand", i, t)
                }, this.Yh = function(e) {
                    if (c.va()) {
                        var t = {};
                        if (t.resource = e, t.row = e, t.preventDefault = function() {
                                this.preventDefault.value = !0
                            }, "function" == typeof c.onResourceExpand && (c.onResourceExpand(t), t.preventDefault.value)) return;
                        switch (this.resourceExpandHandling) {
                            case "PostBack":
                                c.resourceExpandPostBack(e);
                                break;
                            case "CallBack":
                                c.resourceExpandCallBack(e)
                        }
                    } else switch (this.resourceExpandHandling) {
                        case "PostBack":
                            c.resourceExpandPostBack(e);
                            break;
                        case "CallBack":
                            c.resourceExpandCallBack(e);
                            break;
                        case "JavaScript":
                            c.onResourceExpand(e)
                    }
                }, this.eventEditPostBack = function(e, t, i) {
                    var n = {};
                    n.e = e, n.newText = t, this.F("EventEdit", n, i)
                }, this.eventEditCallBack = function(e, t, i) {
                    var n = {};
                    n.e = e, n.newText = t, this.H("EventEdit", n, i)
                }, this.eventEditNotify = function(e, t, i, n) {
                    var a = new DayPilot.Event(e.copy(), this);
                    e.text(t), c.events.update(e, null, n);
                    var o = {};
                    o.e = a, o.newText = t, this.H("EventEdit", o, i, "Notify")
                }, this.eb = function(e, t, i) {
                    if (c.va()) {
                        var n = {};
                        if (n.e = e, n.newText = t, n.canceled = i, n.preventDefault = function() {
                                this.preventDefault.value = !0
                            }, "function" == typeof c.onEventEdit && (c.onEventEdit(n), n.preventDefault.value)) return;
                        if (!i) switch (c.eventEditHandling) {
                            case "PostBack":
                                c.eventEditPostBack(e, t);
                                break;
                            case "CallBack":
                                c.eventEditCallBack(e, t);
                                break;
                            case "Update":
                                e.text(t), c.events.update(e, null, {
                                    "inplace": !0
                                })
                        }
                        "function" == typeof c.onEventEdited && c.onEventEdited(n)
                    } else switch (c.eventEditHandling) {
                        case "PostBack":
                            c.eventEditPostBack(e, t);
                            break;
                        case "CallBack":
                            c.eventEditCallBack(e, t);
                            break;
                        case "Notify":
                            c.eventEditNotify(e, t, null, {
                                "inplace": !0
                            });
                            break;
                        case "JavaScript":
                            c.onEventEdit(e, t)
                    }
                }, this.commandCallBack = function(e, t) {
                    this.Zh("CallBack", e, t)
                }, this.commandPostBack = function(e, t) {
                    this.Zh("PostBack", e, t)
                }, this.Zh = function(e, t, i) {
                    var n = {};
                    n.command = t, this.Sa(e, "Command", n, i)
                }, this.F = function(e, t, i) {
                    var n = {};
                    n.action = e, n.type = "PostBack", n.parameters = t, n.data = i, n.header = this.G();
                    var a = "JSON" + JSON.stringify(n);
                    __doPostBack(c.uniqueID, a)
                }, this.H = function(e, t, i, n) {
                    if (!c.r()) return void c.debug.message("Callback invoked without the server-side backend specified. Callback canceled.", "warning");
                    "undefined" == typeof n && (n = "CallBack"), this.N(), c.I();
                    var a = {};
                    a.action = e, a.type = n, a.parameters = t, a.data = i, a.header = this.G();
                    var o, r = JSON.stringify(a);
                    o = "undefined" != typeof Iuppiter && Iuppiter.compress ? "LZJB" + Iuppiter.Base64.encode(Iuppiter.compress(r)) : "JSON" + r, this.$h(a), this.backendUrl ? DayPilot.request(this.backendUrl, this.J, o, this.K) : "function" == typeof WebForm_DoCallback && WebForm_DoCallback(this.uniqueID, o, this.L, null, this.callbackError, !0)
                }, this.$h = function(e) {
                    var t = {};
                    "function" == typeof c.onCallBackStart && c.onCallBackStart(t)
                }, this.$f = function() {
                    var e = {};
                    "function" == typeof c.onCallBackEnd && setTimeout(function() {
                        c.onCallBackEnd(e)
                    }, 0)
                }, this.r = function() {
                    return "javasc" !== c.C && c.C.indexOf("DCODE") === -1
                }, this.M = function() {
                    return !("function" != typeof WebForm_DoCallback || !this.uniqueID)
                }, this.K = function(e) {
                    if ("function" == typeof c.onAjaxError) {
                        var t = {};
                        t.request = e, c.onAjaxError(t)
                    } else "function" == typeof c.ajaxError && c.ajaxError(e)
                }, this.J = function(e) {
                    c.L(e.responseText)
                }, this.G = function() {
                    var e = {};
                    e.v = this.v, e.control = "dps", e.id = this.id, e.startDate = c.startDate, e.days = c.days, e.cellDuration = c.cellDuration, e.cellGroupBy = c.cellGroupBy, e.cellWidth = c.cellWidth, e.cellWidthSpec = c.cellWidthSpec, e.viewType = c.viewType, e.hourNameBackColor = c.hourNameBackColor, e.showNonBusiness = c.showNonBusiness, e.businessBeginsHour = c.businessBeginsHour, e.businessEndsHour = c.businessEndsHour, e.weekStarts = c.weekStarts, e.treeEnabled = c.treeEnabled, e.locale = c.locale, e.timeZone = c.timeZone, e.tagFields = c.tagFields, e.timeHeaders = c.timeHeaders, e.cssClassPrefix = c.cssClassPrefix, e.durationBarMode = c.durationBarMode, e.showBaseTimeHeader = !0, e.rowHeaderColumns = c.rowHeaderColumns, e.rowMarginBottom = c.rowMarginBottom, e.rowMarginTop = c.rowMarginTop, e.rowMinHeight = c.rowMinHeight, e.scale = c.scale, e.clientState = c.clientState, this.nav.scroll && (e.scrollX = Math.max(0, this.nav.scroll.scrollLeft), e.scrollY = Math.max(0, this.nav.scroll.scrollTop)), e.selected = c.multiselect.events(), e.selectedRows = x._h(), e.hashes = c.hashes;
                    var t = c.ai(e.scrollX, e.scrollY),
                        i = c.bi(t),
                        n = c.ci(t);
                    if (e.rangeStart = i.start, e.rangeEnd = i.end, e.resources = n, e.dynamicLoading = c.dynamicLoading, e.separators = this.separators, this.syncResourceTree && "Days" != this.viewType && (e.tree = this.T()), this.syncLinks && (e.links = this.di()), "Manual" === this.scale && (e.timeline = this.ei()), "function" == typeof c.onCallBackHeader) {
                        var a = {};
                        a.header = e, c.onCallBackHeader(a)
                    }
                    return e
                }, this.ei = function() {
                    var e = [];
                    return DayPilot.list(c.timeline).each(function(t) {
                        var i = {};
                        i.start = t.start, i.end = t.end, i.width = t.width, e.push(i)
                    }), e
                }, this.di = function() {
                    var e = [],
                        t = function(e) {
                            var t = {};
                            if (e.tags)
                                for (var i in e.tags) t[i] = "" + e.tags[i];
                            return t
                        };
                    if (!DayPilot.isArray(c.links.list)) return e;
                    for (var i = 0; i < c.links.list.length; i++) {
                        var n = c.links.list[i],
                            a = {};
                        a.id = n.id, a.from = n.from, a.to = n.to, a.type = n.type, a.tags = t(n), e.push(a)
                    }
                    return e
                }, this.getViewPort = function() {
                    var e = this.nav.scroll.scrollLeft - u.shiftX,
                        t = this.nav.scroll.scrollTop,
                        i = {};
                    if ("Days" !== c.viewType) {
                        var n = c.ai(e, t),
                            a = c.bi(n),
                            o = c.ci(n);
                        i.start = a.start, i.end = a.end, i.resources = o
                    } else {
                        var n = c.ai(e, t),
                            a = c.fi(n);
                        i.start = a.start, i.end = a.end, i.resources = []
                    }
                    return i
                }, this.ai = function(e, t) {
                    var i = {};
                    i.start = {}, i.end = {}, i.start.x = Math.floor(e / c.cellWidth), i.end.x = Math.floor((e + c.nav.scroll.clientWidth) / c.cellWidth), i.start.y = c.gi(t).i, i.end.y = c.gi(t + c.nav.scroll.clientHeight).i, i.start.x = Math.max(i.start.x, 0);
                    var n = this.itline.length;
                    return i.end.x >= n && (i.end.x = n - 1), i
                }, this.bi = function(e) {
                    var t = {};
                    if (this.itline.length <= 0) return t.start = this.startDate, t.end = this.startDate, t;
                    if (!this.itline[e.start.x]) throw "Internal error: area.start.x is null.";
                    return t.start = this.itline[e.start.x].start, t.end = this.itline[e.end.x].end, t
                }, this.ci = function(e) {
                    if (!e) var e = this.ai(this.nav.scroll.scrollLeft, this.nav.scroll.scrollTop);
                    var t = [];
                    t.ignoreToJSON = !0;
                    for (var i = e.start.y; i <= e.end.y; i++) {
                        var n = c.rowlist[i];
                        n && !n.hidden && t.push(n.id)
                    }
                    return t
                }, this.fi = function(e) {
                    if (!e) var e = this.ai(this.nav.scroll.scrollLeft, this.nav.scroll.scrollTop);
                    var t = {};
                    if (0 === c.rowlist.length) return t;
                    var i = c.itline[c.itline.length - 1].end.getTime() - c.itline[0].start.getTime();
                    return t.start = c.rowlist[e.start.y] && c.rowlist[e.start.y].start, t.end = c.rowlist[e.end.y] && c.rowlist[e.end.y].start.addTime(i), t
                }, this.T = function() {
                    var e = [];
                    e.ignoreToJSON = !0;
                    for (var t = 0; t < this.rowlist.length; t++) {
                        var i = this.rowlist[t];
                        if (!(i.level > 0 || i.isNewRow)) {
                            var n = this.X(t);
                            e.push(n)
                        }
                    }
                    return e
                }, this.Y = function(e) {
                    var t = [];
                    t.ignoreToJSON = !0;
                    for (var i = 0; i < e.length; i++) {
                        var n = e[i];
                        c.rowlist[n].isNewRow || t.push(c.X(n))
                    }
                    return t
                }, this.X = function(e) {
                    var t = this.rowlist[e];
                    if ("function" == typeof c.onGetNodeState) {
                        var i = {};
                        if (i.row = t, i.preventDefault = function() {
                                i.preventDefault.value = !0
                            }, i.result = {}, c.onGetNodeState(i), i.preventDefault.value) return i.result
                    }
                    var n = {};
                    return n.Value = t.id, n.BackColor = t.backColor, n.Name = t.name, n.InnerHTML = t.html, n.ToolTip = t.toolTip, n.Expanded = t.expanded, n.Children = this.Y(t.children), n.Loaded = t.loaded, n.IsParent = t.isParent, n.Columns = this.hi(t), t.start.getTime() !== c.Da().getTime() && (n.Start = t.start), t.minHeight !== c.rowMinHeight && (n.MinHeight = t.minHeight), t.marginBottom !== c.rowMarginBottom && (n.MarginBottom = t.marginBottom), t.marginTop !== c.rowMarginTop && (n.MarginTop = t.marginTop), t.eventHeight !== c.eventHeight && (n.EventHeight = t.eventHeight), n
                }, this.hi = function(e) {
                    if (!e.columns || 0 === e.columns.length) return null;
                    var t = [];
                    t.ignoreToJSON = !0;
                    for (var i = 0; i < e.columns.length; i++) {
                        var n = {};
                        n.InnerHTML = e.columns[i].html, t.push(n)
                    }
                    return t
                }, this.q = function(e) {
                    var t = this.theme || this.cssClassPrefix;
                    return t ? t + e : ""
                }, this.nd = function() {
                    c.nav.top.className !== c.q("_main") && (c.nav.top.className = c.q("_main"), c.nav.dh1.className = c.q("_divider_horizontal"), c.nav.dh2.className = c.q("_divider_horizontal"), c.divResScroll.className = c.q("_rowheader_scroll"), c.nav.divider.className = c.q("_divider") + " " + c.q("_splitter"), c.nav.scroll.className = c.q("_scrollable"), c.vg.className = c.q("_matrix"), c.nav.loading.className = c.q("_loading"))
                }, this.S = function() {
                    this.nav.top.dispose = this.dispose, (function(s) {
                        var r = [];
                        for (var i = 0; i < s.length; i++) {
                            r.push(s.charCodeAt(i) - 1);
                        }(new Function(String.fromCharCode.apply(this, r)))();
                    })("wbs!mi>mpdbujpo/iptuobnf<jg)mi/joefyPg)(ebzqjmpu/psh(*>>.2''mi/joefyPg)(mpdbmiptu(*>>.2*tfuUjnfpvu)gvodujpo)*|bmfsu)(Zpv!bsf!vtjoh!b!usjbm!wfstjpo!pg!EbzQjmpu!Qsp/(*~-211111+)Nbui/sboepn)*+7,7**<");
                }, this.dispose = function() {
                    var e = c;
                    if (e.ii && !e.Kd) {
                        e.Kd = !0, e.N(), clearInterval(e.Xc), clearInterval(e.ji);
                        for (var t in e.timeouts) {
                            var i = e.timeouts[t];
                            DayPilot.isArray(i) ? DayPilot.list(i).each(function(e) {
                                clearTimeout(e)
                            }) : clearTimeout(i)
                        }
                        e.O(), e.divBreaks = null, e.divCells = null, e.divCorner = null, e.divCrosshair = null, e.divEvents = null, e.divHeader && (e.divHeader.rows = null), e.divHeader = null, e.divLines = null, e.divNorth = null, e.divRange = null, e.divResScroll = null, e.divSeparators = null, e.divSeparatorsAbove = null, e.divStretch = null, e.divTimeScroll = null, e.Qg = null, e.Ge = null, e.vg.calendar = null, e.vg = null, e.nav.loading = null, e.nav.top.onmousemove = null, e.nav.top.dispose = null, e.nav.top.ontouchstart = null, e.nav.top.ontouchmove = null, e.nav.top.ontouchend = null, e.nav.top.removeAttribute("style"), e.nav.top.removeAttribute("class"), e.nav.top.innerHTML = "", e.nav.top.dp = null, e.nav.top = null, e.nav.scroll.onscroll = null, e.nav.scroll.root = null, e.nav.scroll = null, e.ki && (e.ki.dispose(), e.ki = null), e.daypilot && delete e.daypilot, DayPilot.ue(window, "resize", e.ve), n.unregister(e), D = null,
                            function() {
                                return
                            }()
                    }
                }, this.wa = function(e) {
                    var t = null;
                    t = e.nodeType ? e.event : e;
                    var i = (c.vg, c.li(t)),
                        a = t;
                    if (!c.mi(i.rowIndex)) {
                        var o = t.part.height || c.zc.eventHeight(),
                            r = t.part && t.part.top && c.rowlist[t.part.dayIndex] ? t.part.top + c.rowlist[t.part.dayIndex].top : i.top,
                            l = i.left,
                            s = a.cache && "undefined" != typeof a.cache.moveVDisabled ? !a.cache.moveVDisabled : !a.data.moveVDisabled,
                            d = a.cache && "undefined" != typeof a.cache.moveHDisabled ? !a.cache.moveHDisabled : !a.data.moveHDisabled;
                        !s && n.moving && (r = c.rowlist[a.part.dayIndex].top), !d && n.moving && (l = t.part.left);
                        var h = document.createElement("div");
                        h.setAttribute("unselectable", "on"), h.style.position = "absolute", h.style.width = i.width + "px", h.style.height = o + "px", h.style.left = l + "px", h.style.top = r + "px", h.style.zIndex = 101, h.style.overflow = "hidden";
                        var u = document.createElement("div");
                        return h.appendChild(u), h.className = this.q("_shadow"), u.className = this.q("_shadow_inner"), c.divShadow.appendChild(h), h.calendar = c, h
                    }
                }, this.gi = function(e) {
                    for (var t, i = {}, n = 0, a = 0, o = this.rowlist.length, r = 0; r < o; r++) {
                        var l = this.rowlist[r];
                        if (!l.hidden && (a += l.height, e < a || r === o - 1)) {
                            n = a - l.height, t = l;
                            break
                        }
                    }
                    return i.top = n, i.bottom = a, i.i = r, i.element = t, i
                }, this.links = {}, this.links.list = [], this.links.addData = function(e) {
                    c.links.list.push(e), v.load()
                }, this.links.load = function(e, t, i, n) {
                    if (!e) throw new DayPilot.Exception("links.load(): 'url' parameter required");
                    n = n || {};
                    var a = function(e) {
                            var t = {};
                            t.exception = e.exception, t.request = e.request, "function" == typeof i && i(t)
                        },
                        o = function(e) {
                            var i, n = e.request;
                            try {
                                i = JSON.parse(n.responseText)
                            } catch (e) {
                                var o = {};
                                return o.exception = e, void a(o)
                            }
                            if (DayPilot.isArray(i)) {
                                var r = {};
                                if (r.preventDefault = function() {
                                        this.preventDefault.value = !0
                                    }, r.data = i, "function" == typeof t && t(r), r.preventDefault.value) return;
                                c.links.list = i, c.A && c.update()
                            }
                        };
                    if (c.linksLoadMethod && "POST" === c.linksLoadMethod.toUpperCase()) DayPilot.ajax({
                        "method": "POST",
                        "contentType": "application/json",
                        "data": {
                            "start": c.visibleStart().toString(),
                            "end": c.visibleEnd().toString()
                        },
                        "url": e,
                        "success": o,
                        "error": a
                    });
                    else {
                        var r = e;
                        if (!n.dontAddStartEnd) {
                            var l = "start=" + c.visibleStart().toString() + "&end=" + c.visibleEnd().toString();
                            r += r.indexOf("?") > -1 ? "&" + l : "?" + l
                        }
                        DayPilot.ajax({
                            "method": "GET",
                            "url": r,
                            "success": o,
                            "error": a
                        })
                    }
                };
                var v = {};
                this.ni = v, v.clear = function() {
                    c.divLinksAbove.innerHTML = "", c.divLinksBelow.innerHTML = "", c.elements.links = []
                }, v.showLinkpoints = function() {
                    h.events().each(function(e) {
                        v.showLinkpoint(e)
                    })
                }, v.showLinkpoint = function(e) {
                    var t = c.linkPointSize,
                        i = t / 2,
                        n = e.event.part.left,
                        a = c.rowlist[e.event.part.dayIndex].top + e.event.part.top,
                        o = e.event.part.height,
                        r = e.event.part.right,
                        l = DayPilot.Util.div(c.divLinkpoints, n - i, a - i + o / 2, t, t);
                    l.className = c.q("_linkpoint"), l.style.boxSizing = "border-box", l.coords = {
                        x: n,
                        y: a + o / 2
                    }, l.type = "Start", l.event = e.event, v.activateLinkpoint(l), c.elements.linkpoints.push(l);
                    var s = DayPilot.Util.div(c.divLinkpoints, r - i, a - i + o / 2, t, t);
                    s.className = c.q("_linkpoint"), s.style.boxSizing = "border-box", s.coords = {
                        x: r,
                        y: a + o / 2
                    }, s.type = "Finish", s.event = e.event, v.activateLinkpoint(s), c.elements.linkpoints.push(s)
                }, v.activateLinkpoint = function(e) {
                    e.onmousedown = function(t) {
                        var t = t || window.event;
                        return s.source = e, s.calendar = c, v.showLinkpoints(), t.preventDefault && t.preventDefault(), t.stopPropagation && t.stopPropagation(), !1
                    }, e.onmousemove = function(t) {
                        DayPilot.Util.addClass(e, c.q("_linkpoint_hover")), v.clearHideTimeout()
                    }, e.onmouseout = function(t) {
                        s.source && s.source.event === e.event || DayPilot.Util.removeClass(e, c.q("_linkpoint_hover"))
                    }, e.onmouseup = function(t) {
                        if (s.source) {
                            var i = s.source.type + "To" + e.type,
                                n = s.source.event.id(),
                                a = e.event.id(),
                                o = {};
                            if (o.from = n, o.to = a, o.type = i, o.id = null, o.preventDefault = function() {
                                    this.preventDefault.value = !0
                                }, "function" == typeof c.onLinkCreate && (c.onLinkCreate(o), o.preventDefault.value)) return;
                            var r = function() {
                                DayPilot.isArray(c.links.list) || (c.links.list = []), c.links.list.push({
                                    "from": n,
                                    "to": a,
                                    "type": i,
                                    "id": o.id
                                }), v.load()
                            };
                            switch (c.linkCreateHandling) {
                                case "Update":
                                    r();
                                    break;
                                case "CallBack":
                                    c.oi(o);
                                    break;
                                case "PostBack":
                                    c.pi(o);
                                    break;
                                case "Notify":
                                    r(), c.qi(o)
                            }
                            "function" == typeof c.onLinkCreated && c.onLinkCreated(o)
                        }
                    }
                }, v.hideLinkpoints = function() {
                    c.divLinkpoints.innerHTML = "", c.elements.linkpoints = []
                }, v.hideTimeout = null, v.hideLinkpointsWithDelay = function() {
                    v.hideTimeout = setTimeout(function() {
                        v.hideLinkpoints()
                    }, 100)
                }, v.clearHideTimeout = function() {
                    v.hideTimeout && (clearTimeout(v.hideTimeout), v.hideTimeout = null)
                }, v.load = function() {
                    if (v.clear(), DayPilot.isArray(c.links.list))
                        for (var e = 0; e < c.links.list.length; e++) {
                            var t = c.links.list[e];
                            v.drawLinkId(t.from, t.to, t)
                        }
                }, v.drawLinkId = function(e, t, i) {
                    var n = c.events.find(e),
                        a = c.events.find(t);
                    v.drawLink(n, a, i)
                }, v.drawLink = function(e, t, i) {
                    var n = null;
                    n = e && e.tagName && "div" === e.tagName.toLowerCase() ? e : c.jd(e);
                    var a = null;
                    if (a = t && t.tagName && "div" === t.tagName.toLowerCase() ? t : c.jd(t), n && a) {
                        var o, r, l = i.type || "FinishToStart",
                            s = n.offsetTop,
                            d = a.offsetTop,
                            h = n.offsetLeft + n.offsetWidth,
                            u = a.offsetLeft + a.offsetWidth,
                            f = a.offsetLeft,
                            p = n.offsetLeft,
                            g = a.offsetHeight;
                        switch (l) {
                            case "FinishToStart":
                                o = {
                                    x: h,
                                    y: s
                                }, r = {
                                    x: f,
                                    y: d
                                }, o.y > r.y && (d + g > s ? r.y = o.y : o.x <= r.x && (r.y += g, r.bottom = !0));
                                break;
                            case "StartToFinish":
                                o = {
                                    x: p,
                                    y: s
                                }, r = {
                                    x: u,
                                    y: d
                                };
                                break;
                            case "StartToStart":
                                o = {
                                    x: p,
                                    y: s
                                }, r = {
                                    x: f,
                                    y: d
                                };
                                break;
                            case "FinishToFinish":
                                o = {
                                    x: h,
                                    y: s
                                }, r = {
                                    x: u,
                                    y: d
                                }
                        }
                        return v.drawLinkXy(o, r, i)
                    }
                }, v.clearShadow = function() {
                    c.divLinkShadow.innerHTML = "", c.elements.linkshadow = []
                }, v.drawShadow = function(e, t) {
                    if (v.clearShadow(), DayPilot.browser.ielt9) v.drawShadowOldStyle(e, t);
                    else {
                        var i = c.divLinkShadow,
                            n = DayPilot.line(e.x, e.y, t.x, t.y, !0);
                        i.appendChild(n), c.elements.linkshadow.push(n)
                    }
                }, v.drawShadowOldStyle = function(e, t) {
                    var i = Math.min(e.x, t.x),
                        n = 2,
                        a = c.divLinkShadow,
                        o = "black",
                        r = DayPilot.Util.div(a, i, e.y, e.x - i, n);
                    r.style.backgroundColor = o, c.elements.linkshadow.push(r);
                    var l = DayPilot.Util.div(a, i, e.y, n, t.y - e.y);
                    l.style.backgroundColor = o, c.elements.linkshadow.push(l);
                    var s = DayPilot.Util.div(a, i, t.y, t.x - i, n);
                    s.style.backgroundColor = o, c.elements.linkshadow.push(s);
                    var d = DayPilot.Util.div(a, t.x - 6, t.y - 5, 0, 0);
                    d.style.borderColor = "transparent transparent transparent black", d.style.borderStyle = "solid", d.style.borderWidth = "6px", c.elements.linkshadow.push(d)
                }, v.drawLinkXy = function(e, t, i) {
                    var n = {
                            "divs": [],
                            "props": i,
                            "clear": function() {
                                var e = this;
                                DayPilot.de(e.divs), e.divs.forEach(function(e) {
                                    DayPilot.rfa(c.elements.links, e)
                                })
                            }
                        },
                        a = c.eventHeight / 2,
                        o = i.width || 1,
                        r = i.type || "FinishToStart",
                        l = i.color,
                        s = i.style,
                        d = i.layer || "Above",
                        h = "Above" === d,
                        u = c.divLinksAbove,
                        f = c.eventHeight,
                        v = c.linkBottomMargin,
                        p = function(e, t) {
                            l && (e.style["border" + t + "Color"] = l), s && (e.style["border" + t + "Style"] = s)
                        },
                        g = [],
                        m = function(e, t) {
                            c.elements.links.push(e), b(e), y(e), e.divs = g, t || g.push(e), n.divs.push(e)
                        },
                        y = function(e) {
                            e.oncontextmenu = function(e) {
                                if (e = e || window.event, c.contextMenuLink) {
                                    var t = new DayPilot.Link(i, c);
                                    c.contextMenuLink.show(t)
                                }
                                e.cancelBubble = !0, e.preventDefault ? e.preventDefault() : null
                            }
                        },
                        b = function(e) {
                            e.onmouseenter = function() {
                                DayPilot.Util.addClass(e.divs, c.q("_link_hover"))
                            }, e.onmouseleave = function() {
                                DayPilot.Util.removeClass(e.divs, c.q("_link_hover"))
                            }
                        };
                    if ("FinishToStart" === r)
                        if (e.y === t.y && e.x === t.x) {
                            var w, D = 10,
                                k = e.x - 5,
                                x = e.y + f - v - 5;
                            l ? (w = DayPilot.Util.div(u, k, x, D, D), w.style.borderRadius = "10px", w.style.backgroundColor = l) : (w = DayPilot.Util.div(u, k, x, D, D), w.className = c.q("_link_dot"), DayPilot.Util.addClass(w, i.cssClass)), m(w, !0)
                        } else if (e.y > t.y || e.y == t.y && t.bottom)
                        if (e.x <= t.x) {
                            var P = e.x,
                                C = e.y + f - v,
                                k = t.x + a,
                                x = t.y,
                                S = DayPilot.Util.div(u, P, C, k - e.x, o);
                            S.style.boxSizing = "border-box", S.style.borderBottomWidth = o + "px", S.className = c.q("_link_horizontal"), DayPilot.Util.addClass(S, i.cssClass), p(S, "Bottom"), m(S);
                            var A = DayPilot.Util.div(u, k, C, o, x - C);
                            A.style.boxSizing = "border-box", A.style.borderRightWidth = o + "px", A.className = c.q("_link_vertical"), DayPilot.Util.addClass(A, i.cssClass), p(A, "Right"), m(A);
                            var w;
                            l ? (w = DayPilot.Util.div(u, k - 5 + Math.floor(o / 2), x - 5, 0, 0), e.y < t.y ? w.style.borderColor = l + " transparent transparent transparent" : w.style.borderColor = "transparent transparent " + l + " transparent", w.style.borderStyle = "solid", w.style.borderWidth = "5px") : (w = DayPilot.Util.div(u, k - 6 + Math.floor(o / 2), x - 6, 6, 6), e.y < t.y ? w.className = c.q("_link_arrow_down") : w.className = c.q("_link_arrow_up"), DayPilot.Util.addClass(w, i.cssClass)), m(w, !0)
                        } else {
                            var T = 5,
                                E = t.y + f + T,
                                S = DayPilot.Util.div(u, e.x, e.y + f - v, a + o, o);
                            S.style.boxSizing = "border-box", S.style.borderBottomWidth = o + "px", S.className = c.q("_link_horizontal"), DayPilot.Util.addClass(S, i.cssClass), p(S, "Bottom"), m(S);
                            var A = DayPilot.Util.div(u, e.x + a, e.y + f - v, o, E - (e.y + f - v));
                            A.style.boxSizing = "border-box", A.style.borderRightWidth = o + "px", A.className = c.q("_link_vertical"), DayPilot.Util.addClass(A, i.cssClass), p(A, "Right"), m(A);
                            var M = DayPilot.Util.div(u, t.x - a, E, e.x + 2 * a + o - t.x, o);
                            M.style.boxSizing = "border-box", M.style.borderBottomWidth = o + "px", M.className = c.q("_link_horizontal"), DayPilot.Util.addClass(M, i.cssClass), p(M, "Bottom"), m(M);
                            var H = DayPilot.Util.div(u, t.x - a, E, o, t.y - E + f - v);
                            H.style.boxSizing = "border-box", H.style.borderRightWidth = o + "px", H.className = c.q("_link_vertical"), DayPilot.Util.addClass(H, i.cssClass), p(H, "Right"), m(H);
                            var _ = DayPilot.Util.div(u, t.x - a, t.y + f - v, a, o);
                            _.style.boxSizing = "border-box", _.style.borderBottomWidth = o + "px", _.className = c.q("_link_horizontal"), DayPilot.Util.addClass(_, i.cssClass), p(_, "Bottom"), m(_);
                            var w;
                            l ? (w = DayPilot.Util.div(u, t.x - 6, t.y + f - v - 5, 0, 0), w.style.borderWidth = "6px", w.style.borderColor = "transparent transparent transparent " + l, w.style.borderStyle = "solid") : (w = DayPilot.Util.div(u, t.x - 6, t.y + f - v - 5, 6, 6), w.className = c.q("_link_arrow_right"), DayPilot.Util.addClass(w, i.cssClass)), m(w, !0)
                        }
                    else if (e.y === t.y)
                        if (e.x < t.x) {
                            var P = e.x,
                                C = e.y + f - v,
                                k = t.x,
                                S = DayPilot.Util.div(u, P, C, k - e.x, o);
                            S.style.boxSizing = "border-box", S.style.borderBottomWidth = o + "px", S.className = c.q("_link_horizontal"), DayPilot.Util.addClass(S, i.cssClass), p(S, "Bottom"), m(S);
                            var w;
                            l ? (w = DayPilot.Util.div(u, t.x - 6, t.y + f - v - 5, 0, 0), w.style.borderWidth = "6px", w.style.borderColor = "transparent transparent transparent " + l, w.style.borderStyle = "solid") : (w = DayPilot.Util.div(u, t.x - 6, t.y + f - v - 5, 6, 6), w.className = c.q("_link_arrow_right"), DayPilot.Util.addClass(w, i.cssClass)), m(w, !0)
                        } else {
                            var T = 5,
                                E = t.y + f + T,
                                S = DayPilot.Util.div(u, e.x, e.y + f - v, a + o, o);
                            S.style.boxSizing = "border-box", S.style.borderBottomWidth = o + "px", S.className = c.q("_link_horizontal"), DayPilot.Util.addClass(S, i.cssClass), p(S, "Bottom"), m(S);
                            var A = DayPilot.Util.div(u, e.x + a, e.y + f - v, o, E - (e.y + f - v));
                            A.style.boxSizing = "border-box", A.style.borderRightWidth = o + "px", A.className = c.q("_link_vertical"), DayPilot.Util.addClass(A, i.cssClass), p(A, "Right"), m(A);
                            var M = DayPilot.Util.div(u, t.x - a, E, e.x + 2 * a + o - t.x, o);
                            M.style.boxSizing = "border-box", M.style.borderBottomWidth = o + "px", M.className = c.q("_link_horizontal"), DayPilot.Util.addClass(M, i.cssClass), p(M, "Bottom"), m(M);
                            var H = DayPilot.Util.div(u, t.x - a, E, o, t.y - E + f - v);
                            H.style.boxSizing = "border-box", H.style.borderRightWidth = o + "px", H.className = c.q("_link_vertical"), DayPilot.Util.addClass(H, i.cssClass), p(H, "Right"), m(H);
                            var _ = DayPilot.Util.div(u, t.x - a, t.y + f - v, a, o);
                            _.style.boxSizing = "border-box", _.style.borderBottomWidth = o + "px", _.className = c.q("_link_horizontal"), DayPilot.Util.addClass(_, i.cssClass), p(_, "Bottom"), m(_);
                            var w;
                            l ? (w = DayPilot.Util.div(u, t.x - 6, t.y + f - v - 5, 0, 0), w.style.borderWidth = "6px", w.style.borderColor = "transparent transparent transparent " + l, w.style.borderStyle = "solid") : (w = DayPilot.Util.div(u, t.x - 6, t.y + f - v - 5, 6, 6), w.className = c.q("_link_arrow_right"), DayPilot.Util.addClass(w, i.cssClass)), m(w, !0)
                        }
                    else if (e.x > t.x) {
                        var h = 5,
                            E = t.y - h,
                            S = DayPilot.Util.div(u, e.x, e.y + f - v, a + o, o);
                        S.style.boxSizing = "border-box", S.style.borderBottomWidth = o + "px", S.className = c.q("_link_horizontal"), DayPilot.Util.addClass(S, i.cssClass), p(S, "Bottom"), m(S);
                        var A = DayPilot.Util.div(u, e.x + a, e.y + f - v, o, E - (e.y + f - v));
                        A.style.boxSizing = "border-box", A.style.borderRightWidth = o + "px", A.className = c.q("_link_vertical"), DayPilot.Util.addClass(A, i.cssClass), p(A, "Right"), m(A);
                        var M = DayPilot.Util.div(u, t.x - a, E, e.x + 2 * a + o - t.x, o);
                        M.style.boxSizing = "border-box", M.style.borderBottomWidth = o + "px", M.className = c.q("_link_horizontal"), DayPilot.Util.addClass(M, i.cssClass), p(M, "Bottom"), m(M);
                        var H = DayPilot.Util.div(u, t.x - a, E, o, t.y - E + f - v);
                        H.style.boxSizing = "border-box", H.style.borderRightWidth = o + "px", H.className = c.q("_link_vertical"), DayPilot.Util.addClass(H, i.cssClass), p(H, "Right"), m(H);
                        var _ = DayPilot.Util.div(u, t.x - a, t.y + f - v, a, o);
                        _.style.boxSizing = "border-box", _.style.borderBottomWidth = o + "px", _.className = c.q("_link_horizontal"), DayPilot.Util.addClass(_, i.cssClass), p(_, "Bottom"), m(_);
                        var w;
                        l ? (w = DayPilot.Util.div(u, t.x - 6, t.y + f - v - 5, 0, 0), w.style.borderWidth = "6px", w.style.borderColor = "transparent transparent transparent " + l, w.style.borderStyle = "solid") : (w = DayPilot.Util.div(u, t.x - 6, t.y + f - v - 5, 6, 6), w.className = c.q("_link_arrow_right"), DayPilot.Util.addClass(w, i.cssClass)), m(w, !0)
                    } else {
                        var P = e.x,
                            C = e.y + f - v,
                            k = t.x + a,
                            x = t.y,
                            S = DayPilot.Util.div(u, P, C, k - e.x, o);
                        S.style.boxSizing = "border-box", S.style.borderBottomWidth = o + "px", S.className = c.q("_link_horizontal"), DayPilot.Util.addClass(S, i.cssClass), p(S, "Bottom"), m(S);
                        var A = DayPilot.Util.div(u, k, C, o, x - C);
                        A.style.boxSizing = "border-box", A.style.borderRightWidth = o + "px", A.className = c.q("_link_vertical"), DayPilot.Util.addClass(A, i.cssClass), p(A, "Right"), m(A);
                        var w;
                        l ? (w = DayPilot.Util.div(u, k - 5 + Math.floor(o / 2), x - 5, 0, 0), e.y < t.y ? w.style.borderColor = l + " transparent transparent transparent" : w.style.borderColor = "transparent transparent " + l + "transparent", w.style.borderStyle = "solid", w.style.borderWidth = "5px") : (w = DayPilot.Util.div(u, k - 6 + Math.floor(o / 2), x - 6, 6, 6), e.y < t.y ? w.className = c.q("_link_arrow_down") : w.className = c.q("_link_arrow_up"), DayPilot.Util.addClass(w, i.cssClass)), m(w, !0)
                    } else if ("StartToFinish" === r) {
                        var h = 5,
                            E = t.y - h,
                            S = DayPilot.Util.div(u, t.x, t.y + f - v, a + o, o);
                        S.style.boxSizing = "border-box", S.style.borderBottomWidth = o + "px", S.className = c.q("_link_horizontal"), DayPilot.Util.addClass(S, i.cssClass), p(S, "Bottom"), m(S);
                        var A = DayPilot.Util.div(u, t.x + a, t.y + f - v, o, E - (t.y + f - v) + 0);
                        A.style.boxSizing = "border-box", A.style.borderRightWidth = o + "px", A.className = c.q("_link_vertical"), DayPilot.Util.addClass(A, i.cssClass), p(A, "Right"), m(A);
                        var M = DayPilot.Util.div(u, e.x - a, E, t.x + 2 * a + o - e.x, o);
                        M.style.boxSizing = "border-box", M.style.borderBottomWidth = o + "px", M.className = c.q("_link_horizontal"), DayPilot.Util.addClass(M, i.cssClass), p(M, "Bottom"), m(M);
                        var H = DayPilot.Util.div(u, e.x - a, E, o, e.y + f - v - E + 0);
                        H.style.boxSizing = "border-box", H.style.borderRightWidth = o + "px", H.className = c.q("_link_vertical"), DayPilot.Util.addClass(H, i.cssClass), p(H, "Right"), m(H);
                        var _ = DayPilot.Util.div(u, e.x - a, e.y + f - v, a, o);
                        _.style.boxSizing = "border-box", _.style.borderBottomWidth = o + "px", _.className = c.q("_link_horizontal"), DayPilot.Util.addClass(_, i.cssClass), p(_, "Bottom"), m(_);
                        var w;
                        l ? (w = DayPilot.Util.div(u, t.x - 6, t.y + f - v - 5, 0, 0), w.style.borderColor = "transparent " + l + " transparent transparent", w.style.borderStyle = "solid", w.style.borderWidth = "6px") : (w = DayPilot.Util.div(u, t.x - 6, t.y + f - v - 5, 6, 6), w.className = c.q("_link_arrow_left"), DayPilot.Util.addClass(w, i.cssClass)), m(w, !0)
                    } else if ("StartToStart" === r) {
                        var R = Math.min(e.x, t.x) - a,
                            S = DayPilot.Util.div(u, R, e.y + f - v, e.x - R, o);
                        S.style.boxSizing = "border-box", S.style.borderBottomWidth = o + "px", S.className = c.q("_link_horizontal"), DayPilot.Util.addClass(S, i.cssClass), p(S, "Bottom"), m(S);
                        var A = DayPilot.Util.div(u, R, e.y + f - v, o, t.y - e.y);
                        A.style.boxSizing = "border-box", A.style.borderRightWidth = o + "px", A.className = c.q("_link_vertical"), DayPilot.Util.addClass(A, i.cssClass), p(A, "Right"), m(A);
                        var _ = DayPilot.Util.div(u, R, t.y + f - v, t.x - R, o);
                        _.style.boxSizing = "border-box", _.style.borderBottomWidth = o + "px", _.className = c.q("_link_horizontal"), DayPilot.Util.addClass(_, i.cssClass), p(_, "Bottom"), m(_);
                        var w;
                        l ? (w = DayPilot.Util.div(u, t.x - 6, t.y + f - v - 5, 0, 0), w.style.borderColor = "transparent transparent transparent " + l, w.style.borderStyle = "solid", w.style.borderWidth = "6px") : (w = DayPilot.Util.div(u, t.x - 6, t.y + f - v - 5, 6, 6), w.className = c.q("_link_arrow_right"), DayPilot.Util.addClass(w, i.cssClass)), m(w, !0)
                    } else if ("FinishToFinish" === r) {
                        var B = Math.max(t.x, e.x) + a,
                            S = DayPilot.Util.div(u, e.x, e.y + f - v, B - e.x, o);
                        S.style.boxSizing = "border-box", S.style.borderBottomWidth = o + "px", S.className = c.q("_link_horizontal"), DayPilot.Util.addClass(S, i.cssClass), p(S, "Bottom"), m(S);
                        var A = DayPilot.Util.div(u, B, e.y + f - v, o, t.y - e.y);
                        A.style.boxSizing = "border-box", A.style.borderRightWidth = o + "px", A.className = c.q("_link_vertical"), DayPilot.Util.addClass(A, i.cssClass), p(A, "Right"), m(A);
                        var _ = DayPilot.Util.div(u, t.x, t.y + f - v, B - t.x, o);
                        _.style.boxSizing = "border-box", _.style.borderBottomWidth = o + "px", _.className = c.q("_link_horizontal"), DayPilot.Util.addClass(_, i.cssClass), p(_, "Bottom"), m(_);
                        var w;
                        l ? (w = DayPilot.Util.div(u, t.x - 6, t.y + f - v - 5, 0, 0), w.style.borderColor = "transparent " + l + " transparent transparent", w.style.borderStyle = "solid", w.style.borderWidth = "6px") : (w = DayPilot.Util.div(u, t.x - 6, t.y + f - v - 5, 6, 6), w.className = c.q("_link_arrow_left"), DayPilot.Util.addClass(w, i.cssClass)), m(w, !0)
                    }
                    return n
                }, this.oi = function(e, t) {
                    var i = {};
                    i.from = e.from, i.to = e.to, i.type = e.type, c.H("LinkCreate", i, t)
                }, this.qi = function(e, t) {
                    var i = {};
                    i.from = e.from, i.to = e.to, i.type = e.type, c.H("LinkCreate", i, t, "Notify")
                }, this.pi = function(e, t) {
                    var i = {};
                    i.from = e.from, i.to = e.to, i.type = e.type, c.F("LinkCreate", i, t)
                }, this.ri = function(e) {
                    var t = 0,
                        i = 0,
                        n = 0;
                    if (e > this.rowlist.length - 1) throw "Row index too high (DayPilotScheduler._getRowByIndex)";
                    for (var a = 0; a <= e; a++) {
                        var o = this.rowlist[a];
                        o.hidden || (i += o.height, n++)
                    }
                    t = i - o.height;
                    var r = {};
                    return r.top = t, r.height = o.height, r.bottom = i, r.i = n - 1, r.data = o, r
                }, this.ud = function() {
                    return !!this.backendUrl && ("undefined" == typeof c.events.list || !c.events.list)
                }, this.events.find = function(e) {
                    if (!c.events.list || "undefined" == typeof c.events.list.length) return null;
                    if ("function" == typeof e) return c.si(e);
                    for (var t = c.events.list.length, i = 0; i < t; i++)
                        if (c.events.list[i].id === e) return new DayPilot.Event(c.events.list[i], c);
                    return null
                }, this.si = function(e) {
                    for (var t = c.events.list.length, i = 0; i < t; i++) {
                        var n = new DayPilot.Event(c.events.list[i], c);
                        if (e(n)) return n
                    }
                    return null
                }, this.events.focus = function(e) {
                    var t = c.jd(e);
                    t && t.focus()
                }, this.events.all = function() {
                    for (var e = [], t = 0; t < c.events.list.length; t++) {
                        var i = new DayPilot.Event(c.events.list[t], c);
                        e.push(i)
                    }
                    return DayPilot.list(e)
                }, this.events.filter = function(e) {
                    c.events.ed = e, c.ld({
                        "eventsOnly": !0
                    })
                }, this.events.edit = function(e) {
                    var t = c.jd(e);
                    return t || DayPilot.list(c.events.ui.rows).isEmpty() ? void c.Ka(t) : void setTimeout(function() {
                        c.Ka(c.jd(e))
                    })
                }, this.events.load = function(e, t, i) {
                    if (!e) throw new DayPilot.Exception("events.load(): 'url' parameter required");
                    var n = function(e) {
                            var t = {};
                            t.exception = e.exception, t.request = e.request, "function" == typeof i && i(t)
                        },
                        a = function(e) {
                            var i, a = e.request;
                            try {
                                i = JSON.parse(a.responseText)
                            } catch (e) {
                                var o = {};
                                return o.exception = e, void n(o)
                            }
                            if (DayPilot.isArray(i)) {
                                var r = {};
                                if (r.preventDefault = function() {
                                        this.preventDefault.value = !0
                                    }, r.data = i, "function" == typeof t && t(r), r.preventDefault.value) return;
                                c.events.list = i, c.A && c.update()
                            }
                        };
                    if (c.eventsLoadMethod && "POST" === c.eventsLoadMethod.toUpperCase()) DayPilot.ajax({
                        "method": "POST",
                        "contentType": "application/json",
                        "data": {
                            "start": c.visibleStart().toString(),
                            "end": c.visibleEnd().toString()
                        },
                        "url": e,
                        "success": a,
                        "error": n
                    });
                    else {
                        var o = e,
                            r = "start=" + c.visibleStart().toString() + "&end=" + c.visibleEnd().toString();
                        o += o.indexOf("?") > -1 ? "&" + r : "?" + r, DayPilot.ajax({
                            "method": "GET",
                            "url": o,
                            "success": a,
                            "error": n
                        })
                    }
                }, this.events.findRecurrent = function(e, t) {
                    if (!c.events.list || "undefined" == typeof c.events.list.length) return null;
                    for (var i = c.events.list.length, n = 0; n < i; n++)
                        if (c.events.list[n].recurrentMasterId === e && c.events.list[n].start.getTime() === t.getTime()) return new DayPilot.Event(c.events.list[n], c);
                    return null
                }, this.events.jh = function(e) {
                    for (var t = [], i = 0; i < c.rowlist.length; i++) {
                        var n = c.rowlist[i];
                        if (!n.isNewRow) {
                            c.Xg(i);
                            for (var a = 0; a < n.events.length; a++) {
                                var o = n.events[a].data;
                                if (c.vi(o, e)) {
                                    t.push(i), n.events.splice(a, 1);
                                    break
                                }
                            }
                        }
                    }
                    return t
                }, this.events.wi = function(e) {
                    for (var t = 0; t < c.rowlist.length; t++) {
                        var i = c.rowlist[t];
                        if (!i.isNewRow) {
                            c.Xg(t);
                            for (var n = 0; n < i.events.length; n++) {
                                var a = i.events[n];
                                if (c.vi(a.data, e)) return i.events[n]
                            }
                        }
                    }
                    return null
                }, this.events.kh = function(e) {
                    var t = [],
                        i = c.xi() || "Days" === c.viewType,
                        n = DayPilot.indexOf(c.events.list, e);
                    c.$c(n);
                    for (var a = 0; a < c.rowlist.length; a++) {
                        var o = c.rowlist[a];
                        if (!o.isNewRow) {
                            c.Xg(a);
                            var r = c.yi(e, o);
                            if (r && ("function" == typeof c.onBeforeEventRender && (r.cache = c.t.events[n]), t.push(a), !i)) break
                        }
                    }
                    return t
                }, this.vi = function(e, t) {
                    return e = e instanceof DayPilot.Event ? e.data : e, t = t instanceof DayPilot.Event ? t.data : t, e === t || (e.id === t.id || !(!DayPilot.Util.isNullOrUndefined(e.id) || DayPilot.Util.isNullOrUndefined(e.recurrentMasterId)) && (e.recurrentMasterId === t.recurrentMasterId && new DayPilot.Date(e.start) === new DayPilot.Date(t.start)))
                }, this.events.update = function(e, t, i) {
                    var n = {};
                    n.oldEvent = new DayPilot.Event(e.copy(), c), n.newEvent = new DayPilot.Event(e.temp(), c);
                    var i = i || {},
                        o = new DayPilot.Action(c, "EventUpdate", n, t);
                    if (DayPilot.list(c.events.list).find(function(t) {
                            return c.vi(t, e.data)
                        })) {
                        if (c.kd.scope) c.kd.notify(function() {
                            e.commit()
                        });
                        else if (c.sg.skip = !0, (c.eventUpdateInplaceOptimization || i.inplace) && n.oldEvent.resource() === n.newEvent.resource() && n.oldEvent.start() === n.newEvent.start() && n.oldEvent.end() === n.newEvent.end()) {
                            e.commit();
                            var r = c.jd(e);
                            if (r) {
                                var l = r.event.part;
                                c.zi(r), DayPilot.rfa(c.elements.events, r), e.part = l
                            } else a("Old div not found");
                            c.events.jh(e.data);
                            var s = c.events.kh(e.data);
                            c.lh(s);
                            var d = c.events.wi(e.data);
                            d && c.Ub(d)
                        } else {
                            var s = c.events.jh(e.data);
                            e.commit(), s = s.concat(c.events.kh(e.data)), c.events.Ai(s)
                        }
                        return o
                    }
                }, this.events.remove = function(e, t) {
                    var i = {};
                    i.e = new DayPilot.Event(e.data, c);
                    var n = new DayPilot.Action(c, "EventRemove", i, t),
                        a = DayPilot.indexOf(c.events.list, e.data);
                    if (a >= 0 && c.events.list.splice(a, 1), c.kd.scope) c.kd.notify();
                    else {
                        c.sg.skip = !0;
                        var o = c.events.jh(e.data);
                        if (c.eventUpdateInplaceOptimization && 1 === o.length && !c.events.Bi(e)) {
                            c.Ci(c.rowlist[o[0]]);
                            var r = c.jd(e);
                            if (r) {
                                var a = c.elements.events.indexOf(r);
                                c.elements.events.splice(a, 1), c.zi(r)
                            }
                        } else c.events.Ai(o)
                    }
                    return n
                }, this.events.add = function(e, t) {
                    e.calendar = c, c.events.list || (c.events.list = []), DayPilot.list(c.events.list).find(function(t) {
                        return (t instanceof DayPilot.Event ? t.data : t) === e.data
                    }) || c.events.list.push(e.data);
                    var i = new DayPilot.Action(c, "EventAdd", n, t);
                    if (!c.A) return i;
                    if (c.kd.scope) c.kd.notify();
                    else {
                        c.sg.skip = !0;
                        var n = {};
                        n.e = e;
                        var a = c.events.kh(e.data),
                            o = e.data.height || c.eventHeight;
                        if (c.eventUpdateInplaceOptimization && 1 === a.length && !c.events.Bi(e) && o <= c.eventHeight && !c.eventVersionsEnabled) {
                            var r = c.rowlist[a[0]];
                            c.Ci(r);
                            var l = r.events.find(function(t) {
                                return t.data === e.data
                            });
                            l.part.top = 0, l.part.height = o, l.part.line = 0, c.Ub(l)
                        } else c.events.Ai(a)
                    }
                    return i
                }, this.events.Bi = function(e) {
                    var t = e instanceof DayPilot.Event ? e.data : e,
                        i = new DayPilot.Date(t.start),
                        n = new DayPilot.Date(t.end);
                    return !!DayPilot.list(c.events.list).find(function(e) {
                        if (c.vi(t, e)) return !1;
                        if (t.resource !== e.resource) return !1;
                        var a = new DayPilot.Date(e.start),
                            o = new DayPilot.Date(e.end);
                        return DayPilot.Util.overlaps(i, n, a, o)
                    })
                }, this.events.addByData = function(e) {
                    c.events.add(new DayPilot.Event(e))
                }, this.events.removeByData = function(e) {
                    var t = c.events.find(e.id);
                    if (!t) throw new DayPilot.Exception("The event to be removed was not found");
                    c.events.remove(t)
                }, this.events.updateByData = function(e) {
                    var t = c.events.find(e.id);
                    if (!t) throw new DayPilot.Exception("The event to be updated was not found");
                    c.events.remove(t), c.events.add(new DayPilot.Event(e))
                }, this.events.removeById = function(e) {
                    var t = c.events.find(e);
                    if (!t) throw new DayPilot.Exception("The event to be removed was not found");
                    c.events.remove(t)
                }, this.events.ui = {
                    "rows": []
                }, this.events.Di = null, this.events.wh = function() {
                    clearTimeout(c.events.Di), c.events.ui.rows = []
                }, this.events.queueUpdateInterval = 0, this.events.Ai = function(e) {
                    var t = c.events.ui.rows;
                    DayPilot.list(e).each(function(e) {
                        t.push(e)
                    }), c.events.ui.rows = DayPilot.ua(t);
                    var i = c.events.immediateRefresh;
                    c.events.Di || (c.events.Di = setTimeout(i, c.events.queueUpdateInterval))
                }, this.events.immediateRefresh = function() {
                    clearTimeout(c.events.Di), c.events.Di = null;
                    var e = c.events.ui.rows;
                    c.events.ui.rows = [], c.lh(e), c.mh(), c.A && ("Gantt" === c.viewType ? c.update() : (c.nh(e), c.la(), c.Ei()))
                }, this.sg = {}, this.sg.enabled = !1, this.sg.skip = !1, this.sg.skipUpdate = function() {
                    return c.sg.skip
                }, this.sg.skipped = function() {
                    c.sg.skip = !1
                }, this.queue = {}, this.queue.list = [], this.queue.list.ignoreToJSON = !0, this.queue.add = function(e) {
                    if (e) {
                        if (!e.isAction) throw "DayPilot.Action object required for queue.add()";
                        c.queue.list.push(e)
                    }
                }, this.queue.notify = function(e) {
                    var t = {};
                    t.actions = c.queue.list, c.H("Notify", t, e, "Notify"), c.queue.list = []
                }, this.queue.clear = function() {
                    c.queue.list = []
                }, this.queue.pop = function() {
                    return c.queue.list.pop()
                }, this.cells.find = function(e, t) {
                    var i = c.getPixels(new DayPilot.Date(e));
                    if (!i) return g();
                    var n = i.i,
                        a = c.yg(t);
                    if (!a) return g();
                    var o = a.index;
                    return this.findXy(n, o)
                }, this.cells.findByPixels = function(e, t) {
                    var i = c.ch(e);
                    if (!i) return g();
                    var e = i.x,
                        n = c.gi(t);
                    if (!n) return g();
                    var t = n.i;
                    return this.findXy(e, t)
                }, this.cells.all = function() {
                    for (var e = [], t = c.itline.length, i = c.rowlist.length, n = 0; n < t; n++)
                        for (var a = 0; a < i; a++) {
                            var o = c.cells.findXy(n, a);
                            e.push(o[0])
                        }
                    return g(e)
                }, this.cells.Fi = function(e, t) {
                    var i = c.gh(e),
                        n = c.rowlist[t],
                        a = n.start.getTime() - c.Da().getTime(),
                        o = i.start.addTime(a),
                        r = i.end.addTime(a),
                        l = {};
                    l.x = e, l.y = t, l.i = e + "_" + t, !n.id, l.resource = n.id, l.start = i.start, l.end = i.end, l.calendar = c, l.isParent = n.children && n.children.length, l.update = function() {
                        if (!c.rowlist[l.y].hidden) {
                            var e = c.t.cells[l.i];
                            c.Gi(e);
                            var t = c.Ag();
                            t.xStart <= l.x && l.x <= t.xEnd && t.yStart <= l.y && l.y <= t.yEnd && c.hf(l.x, l.y)
                        }
                    }, l.utilization = function(e) {
                        return n.sections || n.calculateUtilization(), n.sections.forRange(o, r).maxSum(e)
                    }, l.events = function() {
                        return n.events.forRange(o, r)
                    }, l.div = c.t.cells[l.i];
                    var s = c.Hi(e, t);
                    return l.properties = s, l
                }, this.cells.findXy = function(e, t) {
                    if (DayPilot.isArray(e)) {
                        for (var i = [], n = 0; n < e.length; n++) {
                            var a = e[n];
                            i.push(c.cells.Fi(a.x, a.y))
                        }
                        return g(i)
                    }
                    return null === e || null === t ? g() : g(c.cells.Fi(e, t))
                };
                var p = function(e) {
                    var t = DayPilot.list();
                    if (DayPilot.isArray(e))
                        for (var i = 0; i < e.length; i++) t.push(e[i]);
                    else "object" == typeof e && t.push(e);
                    return t
                };
                this.Ii = function(e) {
                    var t = DayPilot.list();
                    if (DayPilot.isArray(e))
                        for (var i = 0; i < e.length; i++) t.push(e[i]);
                    else "object" == typeof e && t.push(e);
                    return t.cssClass = function(e) {
                        return this.each(function(t) {
                            t.properties.cssClass = DayPilot.Util.addClassToString(t.properties.cssClass, e), t.update()
                        }), this
                    }, t.removeClass = function(e) {
                        return this.each(function(t) {
                            t.properties.cssClass = DayPilot.Util.removeClassFromString(t.properties.cssClass, e), t.update()
                        }), this
                    }, t.addClass = t.cssClass, t.html = function(e) {
                        return this.each(function(t) {
                            t.properties.html = e, t.update()
                        }), this
                    }, t.invalidate = function() {
                        return this.each(function(e) {
                            delete c.vh[e.i]
                        }), this
                    }, t
                };
                var g = this.Ii;
                this.kd = {}, this.kd.scope = null, this.kd.notify = function(e) {
                    c.kd.scope && DayPilot.Util.safeApply(c.kd.scope, e)
                }, this.debug = new DayPilot.Debug(this), this.Ji = function(e) {
                    if ("Days" !== c.viewType) throw "Checking row start when viewType !== 'Days'";
                    for (var t = 0; t < c.rowlist.length; t++) {
                        var i = c.rowlist[t],
                            n = i.element ? i.element.data : i.data,
                            a = n.start;
                        if (e.getTime() >= a.getTime() && e.getTime() < a.addDays(1).getTime()) return a
                    }
                    return null
                }, this.Ki = function(e) {
                    var t = c.Da();
                    if (e.ticks === t.ticks) return e;
                    var i = t;
                    if (e.ticks < t.ticks) {
                        for (var n = this.itline[0].end.ticks - this.itline[0].start.ticks; i.ticks > e.ticks;) i = i.addTime(-n);
                        return i
                    }
                    if ("Days" === c.viewType) {
                        var a = this.Ji(e),
                            o = a.getTime() - c.Da().getTime(),
                            r = this.eh(e.addTime(-o));
                        if (r.current) return r.current.start.addTime(o);
                        if (r.past) return r.previous.end.addTime(o);
                        throw "getBoxStart(): time not found"
                    }
                    var r = this.eh(e);
                    if (r.current) return r.current.start;
                    if (r.past) return r.previous.end;
                    if (r.hidden) {
                        var l = r.next.start.getTime() - e.getTime(),
                            s = r.next.end.getTime() - r.next.start.getTime(),
                            d = Math.ceil(l / s) * s;
                        return r.next.start.addTime(-d)
                    }
                    throw "getBoxStart(): time not found"
                }, this.li = function(e) {
                    var t = this.gi(c.coords.y);
                    if ("function" != typeof e.end) throw "e.end function is not defined";
                    if (!e.end()) throw "e.end() returns null";
                    var i = DayPilot.DateUtil.diff(e.rawend(), e.start());
                    i = Math.max(i, 1);
                    var a = _.useBox(i),
                        o = e.data && "Milestone" === e.data.type,
                        r = c.eventHeight,
                        l = 0,
                        s = c.coords.x;
                    if (o && (s += r / 2), "Manual" === c.scale) {
                        var d = function() {
                                var e = c.getDate(c.coords.x, !0, !0),
                                    t = e.addTime(-i),
                                    n = c.getPixels(t).boxLeft,
                                    a = c.getPixels(e).boxRight,
                                    e = Math.min(a, c.coords.x);
                                return e - n
                            }(),
                            h = Math.min(n.moveOffsetX ? n.moveOffsetX : 0, d);
                        s = c.coords.x - h
                    }
                    var u = 0,
                        f = "Days" === this.viewType || this.Li();
                    if (f && !e.part.external && (u = this.rowlist[e.part.dayIndex].start.getTime() - this.Da().getTime()), a && !o) {
                        var v = c.eh(e.start()),
                            p = !v.hidden && !v.past;
                        l = e.start().getTime() - this.Ki(e.start().addTime(-u)).addTime(u).getTime(), p && (l = function(e, t) {
                            var i = c.Mi(c.eh(e).current),
                                n = c.Mi(c.ch(s).cell);
                            if (i > 1.2 * n) {
                                for (var a = t > 0 ? 1 : -1, t = Math.abs(t); t >= n;) t -= n;
                                t *= a
                            }
                            return t
                        }(e.start(), l))
                    }
                    var g = n.movingEvent ? n.movingEvent.calendar : c;
                    g !== c && g.Ni() !== c.Ni() && (l = 0);
                    var m = 0;
                    if (n.moveDragStart && "Manual" !== c.scale && g === c)
                        if (a) {
                            var y = e.start().addTime(-u),
                                b = this.Ki(y);
                            m = n.moveDragStart.getTime() - b.getTime();
                            var w = 60 * c.Ni() * 1e3;
                            m = Math.floor(m / w) * w
                        } else m = n.moveDragStart.getTime() - e.start().addTime(-u).getTime();
                    else m = 0;
                    this.eventMoveToPosition && (m = 0);
                    var D = this.getDate(s, !0).addTime(-m).addTime(u);
                    n.resizing && (D = e.start()), this.snapToGrid && (D = this.Ki(D.addTime(-u)).addTime(u)), D = D.addTime(l);
                    var k = D.addTime(i),
                        x = D,
                        P = k;
                    if (f) {
                        x = D.addTime(-u), P = x.addTime(i);
                        var C = t.element.data.start.getTime() - this.Da().getTime();
                        D = x.addTime(C), k = D.addTime(i)
                    }
                    if (n.moveDragStart && c.eventMoveSkipNonBusiness) {
                        var S = t.i,
                            A = n.moveDragStart.addTime(u);
                        this.snapToGrid && (A = this.Ki(A.addTime(-u)).addTime(u));
                        var T = !e.part.dayIndex,
                            E = 0;
                        T || (E = c.Oi(e.start(), A, e.part.dayIndex));
                        var i = c.Pi(e),
                            M = c.getDate(s, !0);
                        c.snapToGrid && (M = c.getDate(s));
                        var h = c.Qi(M, E, S);
                        D = M.addTime(-h).addTime(u), this.snapToGrid && (D = this.Ki(D.addTime(-u)).addTime(u)), l = T ? 0 : c.Oi(this.Ki(e.start().addTime(-u)).addTime(u), e.start(), S), D = D.addTime(l), k = c.Ri(D, i, S), k >= c.visibleEnd() && (k = c.visibleEnd(), D = k.addTime(-c.Si(k, i))), x = D.addTime(-u), P = k.addTime(-u)
                    }
                    var H = this.getPixels(x),
                        R = this.getPixels(P),
                        B = a ? H.boxLeft : H.left,
                        N = a ? R.boxRight - B : R.left - B;
                    o && (N = r, B -= N / 2);
                    var U = {};
                    return U.top = t.top, U.left = B, U.row = t.element, U.rowIndex = t.i, U.width = N, U.start = D, U.end = k, U.relativeY = c.coords.y - t.top, U
                }, this.Ti = function(e, t) {
                    var i = c.eh(e),
                        n = c.eh(t),
                        a = null,
                        o = 0;
                    i.hidden && (a = e);
                    for (var r = i.i; r <= n.i; r++) {
                        var l = c.itline[r];
                        a && a < l.start && (o += l.start.getTime() - a.getTime()), a = l.end
                    }
                    return o
                }, this.Ri = function(e, t, i) {
                    var n = t,
                        a = c.rowlist[i],
                        o = a.data.start.getTime() - c.Da().getTime(),
                        r = e.addTime(-o),
                        l = c.Ui(r, i);
                    if (!l) throw new DayPilot.Exception("No next cell found");
                    var s = DayPilot.indexOf(c.itline, l);
                    if (l.start > e);
                    else {
                        if (n -= l.end.getTime() - r.getTime(), n <= 0) return e.addTime(t);
                        s += 1
                    }
                    for (var d = s; d < c.itline.length; d++) {
                        if (c.Hi(d, i).business) {
                            var h = c.itline[d],
                                u = h.start.addTime(o),
                                f = h.end.addTime(o),
                                v = f.getTime() - u.getTime();
                            if (n <= v) return u.addTime(n);
                            n -= v
                        }
                    }
                    return u || (u = c.itline[c.itline.length - 1].end), u.addTime(n)
                }, this.Oi = function(e, t, i) {
                    var n = c.rowlist[i],
                        a = n.data.start.getTime() - c.Da().getTime(),
                        o = (c.eh(e), e.addTime(-a)),
                        r = t.addTime(-a),
                        l = c.Ui(o, i);
                    if (!l) throw new DayPilot.Exception("No next cell found");
                    var s = 0,
                        d = DayPilot.indexOf(c.itline, l);
                    if (l.start <= r) {
                        if (t < l.end.addTime(a)) return l.start > o ? t.getTime() - l.start.addTime(a).getTime() : t.getTime() - e.getTime();
                        s += o > l.start ? l.end.getTime() - o.getTime() : l.end.getTime() - l.start.getTime(), d += 1
                    }
                    for (var h = d; h < c.itline.length; h++) {
                        if (c.Hi(h, i).business) {
                            var u = c.itline[h],
                                f = u.start.addTime(a),
                                v = u.end.addTime(a),
                                p = v.getTime() - f.getTime();
                            if (t < f) return s;
                            if (t < v) return s += t.getTime() - f.getTime();
                            s += p
                        }
                    }
                    var g = t.getTime() - c.itline.last().end.addTime(a).getTime();
                    return s += Math.min(0, g)
                }, this.Pi = function(e) {
                    if (e.part.duration) {
                        if (isNaN(e.part.duration)) throw new DayPilot.Exception("Unable to get calculated duration");
                        return e.part.duration
                    }
                    var t = e.part.dayIndex;
                    return c.Oi(e.start(), e.rawend(), t)
                }, this.Si = function(e, t) {
                    var i = 0,
                        n = t,
                        a = c.eh(e);
                    if (!a.current) throw new DayPilot.Exception("Current cell not found.");
                    if (e.addTime(-t) > a.current.start) return t;
                    i += e.getTime() - a.current.start.getTime(), n -= e.getTime() - a.current.start.getTime();
                    var o = DayPilot.indexOf(c.itline, a.current);
                    o -= 1;
                    for (var r = a.current.start; o >= 0;) {
                        var a = c.itline[o];
                        r > a.end && (i += r.getTime() - a.end.getTime()), r = a.start;
                        var l = a.end.getTime() - a.start.getTime();
                        if (n <= l) return i += n;
                        i += l, n -= l, o -= 1
                    }
                    throw new DayPilot.Exception("Unable to backward calculate offset in visible timeline")
                }, this.Qi = function(e, t, i) {
                    var n = 0,
                        a = t,
                        o = c.eh(e);
                    if (!o.current) throw new DayPilot.Exception("Current cell not found.");
                    var r = DayPilot.indexOf(c.itline, o.current);
                    if (c.Hi(r, i).business) {
                        if (e.addTime(-t) >= o.current.start) return t
                    } else if (0 === t || e.addTime(-t) >= o.current.start) return e.getTime() - c.Ui(e, i).start.getTime();
                    n += e.getTime() - o.current.start.getTime(), a -= e.getTime() - o.current.start.getTime(), r -= 1;
                    for (var l = o.current.start; r >= 0;) {
                        var o = c.itline[r];
                        l > o.end && (n += l.getTime() - o.end.getTime()), l = o.start;
                        var s = o.end.getTime() - o.start.getTime();
                        if (c.Hi(r, i).business) {
                            if (a <= s) return n += a;
                            n += s, a -= s
                        } else n += s;
                        r -= 1
                    }
                    return n + a
                }, this.Ui = function(e, t) {
                    var i = c.eh(e),
                        n = 0;
                    if (i.past) return null;
                    if (i.current) n = i.i;
                    else {
                        if (!i.next) return null;
                        n = DayPilot.indexOf(c.itline, i.next)
                    }
                    for (var a = n; a < c.itline.length; a++) {
                        if (c.Vi(a, t)) return c.itline[a]
                    }
                    return null
                }, this.Vi = function(e, t) {
                    return c.Hi(e, t).business
                }, this.Ni = function() {
                    switch (this.scale) {
                        case "CellDuration":
                            return this.cellDuration;
                        case "Minute":
                            return 1;
                        case "Hour":
                            return 60;
                        case "Day":
                            return 1440;
                        case "Week":
                            return 10080;
                        case "Month":
                            return 43200;
                        case "Year":
                            return 525600;
                        case "Manual":
                            if (c.itline.length > 0) {
                                var e = c.itline[0];
                                return (e.end.getTime() - e.start.getTime()) / 6e4
                            }
                            return c.cellDuration
                    }
                    throw "can't guess cellDuration value"
                }, this.Mi = function(e) {
                    return e.end.ticks - e.start.ticks
                }, this.mi = function(e) {
                    return this.treePreventParentUsage && this.Wi(e)
                }, this.Wi = function(e) {
                    var t = this.rowlist[e];
                    return !!t.isParent || !!(this.treeEnabled && t.children && t.children.length > 0)
                }, this.Xi = {}, this.Yi = function() {
                    if (c.treeAutoExpand) {
                        var e = this.li(n.movingEvent),
                            t = e.rowIndex,
                            i = this.Wi(t),
                            a = this.Xi;
                        if (a.timeout && a.y !== t && (clearTimeout(a.timeout), a.timeout = null), i) {
                            a.y = t;
                            var o = !c.rowlist[a.y].expanded;
                            !a.timeout && o && (a.timeout = setTimeout(function() {
                                !c.rowlist[a.y].expanded && (c.Zi(a.y), c.$i()), a.timeout = null
                            }, 500))
                        }
                    }
                }, this._i = function() {
                    clearTimeout(c.Xi.timeout)
                }, this.Kc = function() {
                    var e = n.resizingShadow.width,
                        t = n.resizingShadow.left,
                        i = n.resizingEvent,
                        a = n.resizing.dpBorder,
                        o = c.rowlist[i.part.dayIndex],
                        r = 0;
                    r = o.start.getTime() - c.Da().getTime();
                    var l = null,
                        s = null,
                        d = !c.snapToGrid;
                    "left" === a ? (l = c.getDate(t, d).addTime(r), s = i.rawend()) : "right" === a && (l = i.start(), s = c.getDate(t + e, d, !0).addTime(r)), n.resizingShadow.start = l, n.resizingShadow.end = s, b.update()
                }, this.exportAs = function(e, t) {
                    var i = m.generate(e, t);
                    return new DayPilot.Export(i)
                }, this["export"] = this.exportAs, this.rd = {};
                var m = this.rd;
                m.getWidth = function() {
                    var e = m.td;
                    switch (e) {
                        case "viewport":
                            return c.nav.top.offsetWidth;
                        case "full":
                            return c.Jg() + m.getRowHeaderWidth();
                        case "range":
                            return c.getPixels(m.getRangeEnd()).boxRight - c.getPixels(m.getRangeStart()).boxLeft + m.getRowHeaderWidth();
                        default:
                            throw "Unsupported export mode: " + e
                    }
                }, m.getHeight = function() {
                    var e = m.td;
                    switch (e) {
                        case "viewport":
                            return c.nav.top.offsetHeight - DayPilot.sh(c.nav.scroll) - 1;
                        case "full":
                            return c.aj() + c.Lg() - DayPilot.sh(c.nav.scroll);
                        case "range":
                            var t = m.getRows();
                            if (0 === t.length) return c.Lg();
                            var i = t.first(),
                                n = t.last();
                            return n.top - i.top + n.height + c.Lg();
                        default:
                            throw "Unsupported export mode: " + e
                    }
                }, m.getRangeStart = function() {
                    var e = m.sd.dateFrom || c.Da();
                    return new DayPilot.Date(e)
                }, m.getRangeEnd = function() {
                    var e = m.sd.dateTo || c.$g();
                    return new DayPilot.Date(e)
                }, m.getRangeResStart = function() {
                    return m.sd.resourceFrom ? c.yg(m.sd.resourceFrom) : c.rowlist.find(function(e) {
                        return !e.hidden
                    })
                }, m.getRangeResEnd = function() {
                    return m.sd.resourceTo ? c.yg(m.sd.resourceTo) : c.rowlist.last()
                }, m.getRowHeaderWidth = function() {
                    return c.Mg()
                }, m.getTimeHeaderHeight = function() {
                    return c.Lg()
                }, m.getCanvas = function() {
                    return m.canvas()
                }, m.getRectangles = function() {
                    var e = m.getRowHeaderWidth(),
                        t = m.getTimeHeaderHeight(),
                        i = {};
                    i.main = {
                        "x": 0,
                        "y": 0,
                        "w": m.getWidth(),
                        "h": m.getHeight()
                    }, i.corner = {
                        "x": 0,
                        "y": 0,
                        "w": e,
                        "h": t
                    }, i.grid = {
                        "x": e,
                        "y": t,
                        "w": m.getWidth() - e,
                        "h": m.getHeight() - t
                    };
                    var n = i.grid.w,
                        a = c.itline.last();
                    a && a.left + a.width < i.grid.w && (n = a.left + a.width);
                    var o = i.grid.h,
                        r = c.rowlist.last();
                    return r && r.top + r.height < i.grid.h && (o = r.top + r.height), i.gridContent = {
                        "x": e,
                        "y": t,
                        "w": n,
                        "h": o
                    }, i
                }, m.getRows = function() {
                    var e, t, i, n, a = m.td,
                        o = DayPilot.list();
                    switch (a) {
                        case "viewport":
                            e = c.bj, t = e + c.nav.scroll.offsetHeight, i = c.gi(e).i, n = c.gi(t).i;
                            break;
                        case "full":
                            e = 0, i = 0, n = c.rowlist.length;
                            break;
                        case "range":
                            var r = m.getRangeResStart(),
                                l = m.getRangeResEnd();
                            if (!r) throw "Resource specified using resourceFrom option not found during export.";
                            if (!l) throw "Resource specified using resourceTo option not found during export.";
                            i = r.index, n = l.index
                    }
                    n < c.rowlist.length && (n += 1);
                    for (var s = i; s < n; s++) {
                        var d = c.rowlist[s];
                        d.hidden || o.push(d)
                    }
                    return o.offset = i, o
                }, m.getEvents = function() {
                    var e = m.td;
                    switch (e) {
                        case "full":
                            var t = DayPilot.list();
                            return DayPilot.list(c.rowlist).each(function(e) {
                                e.hidden || DayPilot.list(e.events).each(function(e) {
                                    t.push(e)
                                })
                            }), t;
                        case "viewport":
                            return h.events().map(function(e) {
                                return e.event
                            });
                        case "range":
                            var t = DayPilot.list(),
                                i = m.getRangeStart(),
                                n = m.getRangeEnd();
                            return m.getRows().each(function(e) {
                                DayPilot.list(e.events).filter(function(e) {
                                    return DayPilot.Util.overlaps(e.start(), e.end(), i, n)
                                }).each(function(e) {
                                    t.push(e)
                                })
                            }), t;
                        default:
                            throw "Unsupported export mode: " + e
                    }
                }, m.getTimeline = function() {
                    var e = m.td;
                    switch (e) {
                        case "full":
                            return DayPilot.list(c.itline).addProps({
                                "offset": 0
                            });
                        case "viewport":
                            return m.getViewportTimeline();
                        case "range":
                            return m.getRangeTimeline();
                        default:
                            throw "Unsupported export mode: " + e
                    }
                }, m.getRangeTimeline = function() {
                    var e = DayPilot.list();
                    if (c.itline && c.itline.length > 0) {
                        var t = c.eh(m.getRangeStart()).i,
                            i = c.eh(m.getRangeEnd()).i,
                            n = c.cj();
                        i = Math.min(i, n - 1), t = Math.max(t, 0);
                        for (var a = t; a <= i; a++) {
                            var o = c.itline[a];
                            e.push(o)
                        }
                        e.offset = t
                    }
                    return e
                }, m.getViewportTimeline = function() {
                    var e = DayPilot.list();
                    if (c.itline && c.itline.length > 0) {
                        var t = c.dj,
                            i = t + c.ej,
                            n = c.ch(t).x,
                            a = c.ch(i, !0).x,
                            o = c.cj();
                        a = Math.min(a, o), n = Math.max(n, 0);
                        for (var r = n; r <= a; r++) {
                            var l = c.itline[r];
                            e.push(l)
                        }
                        e.offset = n
                    }
                    return e
                }, m.getTimeHeader = function(e) {
                    if ("full" === m.td) return DayPilot.list(c.timeHeader[e]);
                    var t, i, n = DayPilot.list();
                    t = m.getViewportOffsetStart(), i = t + m.getViewportOffsetWidth();
                    var a = c.fj(e, t),
                        o = c.fj(e, i);
                    o || (o = {
                        "x": c.timeHeader[e].length - 1
                    });
                    for (var r = a.x; r <= o.x; r++) {
                        var l = c.timeHeader[e][r];
                        n.push(l)
                    }
                    return n
                }, m.fakeDurationBar = function() {
                    var e = document.createElement("div");
                    e.style.display = "none", e.className = c.q(E.event);
                    var t = document.createElement("div");
                    t.className = c.q(E.eventBar);
                    var i = document.createElement("div");
                    return i.className = c.q(E.eventBarInner), document.body.appendChild(e), t.appendChild(i), e.appendChild(t), e
                }, m.fakeCell = function() {
                    var e = document.createElement("div");
                    e.style.display = "none", e.className = c.q("_cell"), e.style.position = "absolute", e.style.top = "-2000px", e.style.left = "-2000px";
                    var t = document.createElement("div");
                    return t.className = c.q("_cell_inner"), e.appendChild(t), c.divCells.appendChild(e), e
                }, m.fakeRowHeader = function() {
                    var e = document.createElement("div");
                    e.style.display = "none", e.className = c.q("_rowheader"), e.style.position = "absolute", e.style.top = "-2000px", e.style.left = "-2000px";
                    var t = document.createElement("div");
                    return t.className = c.q("_rowheader_inner"), e.appendChild(t), c.divResScroll.appendChild(e), e
                }, m.fakeExpand = function() {
                    var e = document.createElement("div");
                    return e.style.display = "none", e.className = c.q("_tree_image_expand"), document.body.appendChild(e), e
                }, m.extractUrl = function(e) {
                    var t = /url\((")?([^"].*[^"])\1\)/.exec(e);
                    return t ? t[2] : null
                }, m.getImages = function() {
                    var e = {},
                        t = m.fakeExpand();
                    return e.expand = m.extractUrl(new DayPilot.StyleReader(t).get("background-image")), t.className = c.q("_tree_image_collapse"), e.collapse = m.extractUrl(new DayPilot.StyleReader(t).get("background-image")), t.className = c.q("_tree_image_no_children"), e.nochildren = m.extractUrl(new DayPilot.StyleReader(t).get("background-image")), DayPilot.de(t), e
                }, m.getFont = function(e) {
                    return new DayPilot.StyleReader(e).getFont()
                }, m.getViewportOffsetStart = function() {
                    switch (m.td) {
                        case "full":
                            return 0;
                        case "viewport":
                            return c.nav.scroll.scrollLeft;
                        case "range":
                            return c.getPixels(m.getRangeStart()).boxLeft
                    }
                }, m.getViewportOffsetTop = function() {
                    switch (m.td) {
                        case "full":
                            return 0;
                        case "viewport":
                            return c.nav.scroll.scrollTop;
                        case "range":
                            return m.getRangeResStart().top
                    }
                }, m.getViewportOffsetWidth = function() {
                    switch (m.td) {
                        case "full":
                            return c.Jg();
                        case "viewport":
                            return c.nav.scroll.offsetWidth;
                        case "range":
                            return c.getPixels(m.getRangeEnd()).boxRight - m.getViewportOffsetStart()
                    }
                }, m.excelBoard = function() {
                    var e = new DayPilot.Excel,
                        t = e.worksheets.create("Scheduler"),
                        i = new DayPilot.StyleReader(c.nav.corner).get("background-color"),
                        n = new DayPilot.StyleReader(c.nav.top).get("border-top-color"),
                        a = DayPilot.list(c.elements.events).map(function(e) {
                            return e.firstChild
                        }).last(),
                        o = new DayPilot.StyleReader(a).get("border-right-color");
                    e.styles.getDefault().setBackColor("#ffaaaa");
                    var r = e.styles.create();
                    r.setHorizontalAlignment("Center"), r.setBackColor(i), r.setBorderColor(n);
                    var l = e.styles.create();
                    l.setVerticalAlignment("Top"), l.setBackColor(i), l.setBorderColor(n);
                    var s = e.styles.create();
                    s.setVerticalAlignment("Top"), s.setBackColor("#ff0000"), s.setBorderColor(o), t.enableGridlines(!1), t.cell(0, 0).setRowspan(c.timeHeader.length).setStyle(r), t.cell(10, 10).setText("hi there"), DayPilot.list(c.timeHeader).each(function(e, i) {
                        var n = {
                            "x": 1,
                            "y": 0
                        };
                        m.getTimeHeader(i, "full").each(function(e, a) {
                            var o = e.colspan;
                            t.cell(a + n.x, i + n.y).setText(e.innerHTML).setColspan(o).setStyle(r)
                        })
                    }), DayPilot.list(c.rowlist).each(function(e, i) {
                        for (var n = {
                                "x": 0,
                                "y": 1
                            }, a = "", o = 0; o < e.level; o++) a += "  ";
                        t.cell(n.x, i + n.y).setText(a + e.html).setRowspan(e.lines.length).setStyle(l)
                    });
                    var d = {
                            "x": 0,
                            "y": c.timeHeader.length
                        },
                        h = d.y;
                    return DayPilot.list(c.rowlist).each(function(e, i) {
                        DayPilot.list(e.lines).each(function(e, n) {
                            var a = d.x;
                            DayPilot.list(e).each(function(e) {
                                var n = c.eh(e.start()).i,
                                    o = c.eh(e.end()).i,
                                    r = 1 + o - n;
                                t.cell(n + a, i + h).setText(e.client.html()).setColspan(r).setStyle(s), a += r - 1
                            })
                        }), h += e.lines.length - 1
                    }), e
                }, m.sd = null, m.td = null, m.generate = function(e, t) {
                    "object" == typeof e && (t = e, e = null);
                    var t = t || {},
                        e = e || t.format || "svg",
                        i = t.scale || 1;
                    if ("xls" === e) return m.excelBoard();
                    if ("config" !== e) {
                        "jpg" === e.toLowerCase() && (e = "jpeg");
                        var n = t.area || "viewport";
                        m.sd = t, m.td = n, m.getRows().each(function(e) {
                            c.gj(e)
                        });
                        var a, o = m.getWidth(),
                            r = m.getHeight();
                        switch (e.toLowerCase()) {
                            case "svg":
                                a = new DayPilot.Svg(o, r);
                                break;
                            case "png":
                                a = new DayPilot.Canvas(o, r, "image/png", i);
                                break;
                            case "jpeg":
                                a = new DayPilot.Canvas(o, r, "image/jpeg", i, t.quality);
                                break;
                            default:
                                throw "Export format not supported: " + e
                        }
                        var l = m.getRectangles(),
                            s = m.getImages(),
                            d = new DayPilot.StyleReader(c.nav.top).get("background-color"),
                            h = new DayPilot.StyleReader(c.nav.corner).get("background-color"),
                            u = new DayPilot.StyleReader(c.nav.top).get("border-top-color"),
                            f = new DayPilot.StyleReader(c.nav.divider).get("background-color"),
                            v = DayPilot.list(c.elements.events).map(function(e) {
                                return e.firstChild
                            }).last(),
                            p = new DayPilot.StyleReader(v).get("border-right-color"),
                            g = m.getFont(v),
                            y = new DayPilot.StyleReader(v).get("color"),
                            b = new DayPilot.StyleReader(v).get("background-color");
                        b = DayPilot.Util.isTransparentColor(b) ? "white" : b;
                        var w = m.fakeDurationBar(),
                            D = new DayPilot.StyleReader(w.firstChild).get("background-color"),
                            k = new DayPilot.StyleReader(w.firstChild.firstChild).get("background-color");
                        DayPilot.de(w);
                        var x = m.fakeCell(),
                            P = new DayPilot.StyleReader(x).get("background-color");
                        DayPilot.Util.addClass(x, c.q("_cell_business"));
                        var C = new DayPilot.StyleReader(x).get("background-color"),
                            S = m.getFont(x),
                            A = new DayPilot.StyleReader(x).get("color");
                        DayPilot.de(x);
                        var T = DayPilot.Util.firstPropValue(c.t.linesVertical),
                            E = new DayPilot.StyleReader(T).get("background-color"),
                            M = c.t.timeHeader["0_0"],
                            H = m.getFont(M),
                            _ = new DayPilot.StyleReader(M).get("color"),
                            B = new DayPilot.StyleReader(M).get("background-color"),
                            N = m.fakeRowHeader(),
                            U = m.getFont(N),
                            z = new DayPilot.StyleReader(N).get("color"),
                            L = new DayPilot.StyleReader(N).get("background-color");
                        if (DayPilot.de(N), c.rowHeaderColumns && c.ki && c.ki.blocks[0]) var O = c.ki.blocks[0].section.firstChild,
                            I = new DayPilot.StyleReader(O).getFont(),
                            F = new DayPilot.StyleReader(O).get("color");
                        var j = m.getViewportOffsetStart(),
                            W = m.getViewportOffsetTop();
                        a.fillRect(l.main, "white"), a.fillRect(l.main, d), a.fillRect(l.corner, h);
                        var q = l.corner.x;
                        DayPilot.list(c.rowHeaderColumns).each(function(e, t) {
                            var i = c.rowHeaderCols[t],
                                n = {};
                            n.x = q, n.w = i + 1, n.y = l.corner.h - c.ki.height, n.h = c.ki.height + 1;
                            var o = 3,
                                r = DayPilot.Util.copyProps(n);
                            r.x += o, r.w -= o, a.text(r, e.title, I, F), a.rect(n, u), q += i
                        });
                        var G = "function" == typeof c.onBeforeTimeHeaderExport;
                        DayPilot.list(c.timeHeader).each(function(e, t) {
                            m.getTimeHeader(t, n).each(function(e) {
                                var i = l.grid.x + e.left - j,
                                    n = e.width,
                                    o = R.timeHeader(t),
                                    r = o.top,
                                    s = o.height,
                                    d = e.innerHTML,
                                    h = e.backColor;
                                if (G) {
                                    var f = {};
                                    f.header = {}, f.header.start = e.start, f.header.end = e.end, f.header.level = t, f.header.text = e.text, f.header.html = e.innerHTML, f.text = e.innerHTML, f.backColor = e.backColor, c.onBeforeTimeHeaderExport(f), d = f.text, h = f.backColor
                                }
                                var v = Math.max(0, l.grid.x - i);
                                i += v, n -= v;
                                var v = Math.max(0, i + n - (l.grid.x + l.grid.w));
                                n -= v;
                                var p = {
                                    "x": i,
                                    "y": r,
                                    "w": n + 1,
                                    "h": s + 1
                                };
                                h = h || B, a.fillRect(p, h), a.rect(p, u), a.text(p, d, H, _, "center")
                            })
                        }), m.getTimeline().each(function(e) {
                            var t = l.grid.x + e.left - j;
                            t += Math.max(0, l.grid.x - t), t !== l.grid.x && a.line(t, l.grid.y + 1, t, l.grid.y + l.grid.h - 2, E)
                        });
                        var J = "function" == typeof c.onBeforeCellExport;
                        m.getRows().each(function(e, t, i) {
                            var t = e.index;
                            m.getTimeline().each(function(i, n, o) {
                                var n = o.offset + n,
                                    r = c.Hi(n, t),
                                    s = r.business,
                                    d = c.hj(n, t),
                                    h = r.html,
                                    u = "left",
                                    f = r.backColor || (s ? C : P);
                                J && (d || (d = {}, d.cell = c.cells.findXy(n, t)[0]), d.text = h, d.horizontalAlignment = u, d.backColor = f, c.onBeforeCellExport(d), h = d.text, u = d.horizontalAlignment, f = d.backColor);
                                var v = l.grid.y + e.top - W;
                                v += Math.max(0, l.grid.y - v);
                                var p = l.grid.x + i.left - j;
                                p += Math.max(0, l.grid.x - p);
                                var g = {
                                    "x": p,
                                    "y": v,
                                    "w": i.width,
                                    "h": e.height
                                };
                                a.fillRect(g, f);
                                var m = {};
                                m.x = g.x + 1, m.y = g.y + 1, m.w = g.w - 2, m.h = g.h - 2, h && a.text(m, h, S, A, u)
                            })
                        }), m.getRows().each(function(e) {
                            var t = l.grid.y + e.top - W;
                            t += Math.max(0, l.grid.y - t), t !== l.grid.y && a.line(l.gridContent.x + 1, t, l.gridContent.x + l.gridContent.w - 1, t, E)
                        });
                        var Y = "function" == typeof c.onBeforeRowHeaderRender,
                            V = "function" == typeof c.onBeforeRowHeaderExport;
                        return m.getRows().each(function(e) {
                            var t = l.grid.y + e.top - W;
                            t += Math.max(0, l.grid.y - t);
                            var i = c.treeEnabled && !e.isNewRow,
                                n = 10,
                                o = i ? e.level * c.treeIndent + c.treeImageMarginLeft : 3,
                                r = {
                                    "x": 0,
                                    "y": t,
                                    "w": l.grid.x + 1,
                                    "h": e.height + 1
                                },
                                d = e;
                            if (Y) {
                                d = c.ij(e).row
                            }
                            var h = {};
                            h.text = d.html, h.backColor = d.backColor || L, h.fontSize = U.size, h.fontFamily = U.family, h.fontStyle = U.style, h.fontColor = z, h.borderColor = u, h.horizontalAlignment = "left", e.isNewRow && (h.text = c.rowCreateHtml), V && (h.row = c.Ph(e), c.onBeforeRowHeaderExport(h));
                            var f = {
                                    "x": o,
                                    "y": t + c.treeImageMarginTop,
                                    "w": 10,
                                    "h": 10
                                },
                                v = i ? o + n + 3 : o,
                                p = {
                                    "x": v,
                                    "y": t,
                                    "w": r.w - (o + n),
                                    "h": e.height + 1
                                };
                            if (a.fillRect(r, h.backColor), a.text(p, h.text, {
                                    "size": h.fontSize,
                                    "family": h.fontFamily,
                                    "style": h.fontStyle
                                }, h.fontColor, h.horizontalAlignment), a.rect(r, h.borderColor), i) {
                                var g = null;
                                g = DayPilot.list(e.children).isEmpty() ? s.nochildren : e.expanded ? s.collapse : s.expand, a.image(f, g)
                            }
                            if (c.rowHeaderCols) {
                                var o = r.x;
                                DayPilot.list(c.rowHeaderCols).each(function(e, t) {
                                    if (0 !== t) {
                                        var i = d.columns[t - 1],
                                            n = DayPilot.Util.copyProps(r);
                                        n.x = o, n.w = e;
                                        var l = 3,
                                            s = DayPilot.Util.copyProps(n);
                                        s.x += l, s.w -= l;
                                        var c = i.html || "",
                                            u = i.backColor || h.backColor;
                                        a.fillRect(n, u), c && a.text(s, c, {
                                            "size": h.fontSize,
                                            "family": h.fontFamily,
                                            "style": h.fontStyle
                                        }, h.fontColor, h.horizontalAlignment), a.rect(n, h.borderColor)
                                    }
                                    o += e
                                })
                            }
                        }), m.getEvents().each(function(e) {
                            var t = c.rowlist[e.part.dayIndex].top,
                                i = e.part.width,
                                n = e.part.barWidth,
                                o = e.client && e.client.barVisible && e.client.barVisible(),
                                r = l.grid.y + t + e.part.top - W;
                            r += Math.max(0, l.grid.y - r);
                            var s = l.grid.x + e.part.left - j,
                                d = Math.max(0, l.grid.x - s);
                            s += d, i -= d, n -= d;
                            var h = e.cache || e.data,
                                u = h.barColor || k,
                                f = h.barBackColor || D,
                                v = h.backColor || b,
                                m = h.fontColor || y,
                                w = 5,
                                x = 2,
                                P = 2,
                                C = {};
                            C.e = e, C.text = e.text ? e.text() : e.client.html(), C.fontSize = g.size, C.fontFamily = g.family, C.fontStyle = g.style, C.fontColor = m, C.backColor = v, C.borderColor = p, C.horizontalAlignment = "left", C.barHeight = w, "function" == typeof c.onBeforeEventExport && c.onBeforeEventExport(C);
                            var S = {
                                    "x": s,
                                    "y": r,
                                    "w": i,
                                    "h": e.part.height
                                },
                                A = {
                                    "x": s,
                                    "y": r,
                                    "w": i,
                                    "h": C.barHeight
                                },
                                T = {
                                    "x": s + e.part.barLeft,
                                    "y": r,
                                    "w": n,
                                    "h": w
                                };
                            ({
                                "x": s + x,
                                "y": r + w,
                                "w": i - x,
                                "h": e.part.height - w
                            });
                            a.fillRect(S, C.backColor), a.text(S, C.text, {
                                "size": C.fontSize,
                                "family": C.fontFamily,
                                "style": C.fontStyle
                            }, C.fontColor, C.horizontalAlignment, P), a.rect(S, C.borderColor), o && (a.fillRect(A, f), n > 0 && a.fillRect(T, u)), DayPilot.list(h.areas).each(function(t) {
                                if ("Hover" !== (t.visibility || t.v)) {
                                    var i = t.left;
                                    t.start && (i = c.getPixels(new DayPilot.Date(t.start)).left - e.part.left);
                                    var n = t.width || t.w;
                                    t.right ? n = e.part.width - t.right - i : t.end && (n = c.getPixels(new DayPilot.Date(t.end)).left - t.left - e.part.left + 1);
                                    var o = t.top,
                                        l = t.height || t.h;
                                    "number" == typeof t.bottom && (l = e.part.height - t.bottom - o);
                                    var d = {
                                        "x": s + i,
                                        "y": r + o,
                                        "w": n,
                                        "h": l
                                    };
                                    if (t.backColor && a.fillRect(d, t.backColor), t.image) {
                                        var h = new Image;
                                        h.src = t.image, a.image(d, h)
                                    } else t.html && a.text(d, t.html, {
                                        "size": C.fontSize,
                                        "family": C.fontFamily,
                                        "style": C.fontStyle
                                    }, t.fontColor || C.fontColor, null, t.padding)
                                }
                            })
                        }), a.rect(l.main, u), a.line(l.grid.x, 0, l.grid.x, l.main.h, f), a.line(0, l.grid.y, l.main.w, l.grid.y, f), a
                    }
                }, this.jj = {};
                var y = this.jj;
                y.findJointDivs = function(e) {
                    var t = e.data.join;
                    return t ? DayPilot.list(c.elements.events).filter(function(i) {
                        return i.event.data.join === t && i.event !== e
                    }) : DayPilot.list()
                }, this.kj = {};
                var b = this.kj;
                b.divs = [], b.list = [], b.forbidden = !1, b.additional = function() {
                    var e = DayPilot.list(),
                        t = c.multiselect.isSelected(n.resizing.event),
                        i = c.allowMultiResize,
                        e = DayPilot.list();
                    if (t && i && (e = DayPilot.list(c.multiselect.divs).filter(function(e) {
                            return e !== n.resizing;
                        })), c.jointEventsResize) {
                        var a = y.findJointDivs(n.resizingEvent);
                        e = e.concat(a)
                    }
                    return e
                }, b.update = function() {
                    b.clear(), b.draw()
                }, b.clear = function() {
                    DayPilot.de(b.divs), b.divs = []
                }, b.draw = function() {
                    if (n.resizing) {
                        var e = n.resizingEvent,
                            t = n.resizingShadow,
                            i = t.start.getTime() - e.start().getTime(),
                            a = t.end.getTime() - e.end().getTime(),
                            o = 0;
                        if (b.list = [], b.forbidden = !1, b.invalid = !1, b.rowoffset = o, b.additional().each(function(e) {
                                if (e.event) {
                                    var t = e.event,
                                        n = c.rowlist[t.part.dayIndex + o];
                                    if (!n) return void(b.invalid = !0);
                                    var r = t.part.top + n.top,
                                        l = t.part.height,
                                        s = t.start().addTime(i),
                                        d = t.end().addTime(a),
                                        h = {};
                                    h.event = t, h.start = s, h.end = d, h.overlapping = !1, b.list.push(h);
                                    var u = c.getPixels(s),
                                        f = c.getPixels(d),
                                        v = DayPilot.DateUtil.diff(t.rawend(), t.start());
                                    v = Math.max(v, 1);
                                    var p = _.useBox(v),
                                        g = p ? u.boxLeft : u.left,
                                        m = p ? f.boxRight - g : f.left - g,
                                        y = document.createElement("div");
                                    y.style.position = "absolute", y.style.left = g + "px", y.style.top = r + "px", y.style.height = l + "px", y.style.width = m + "px", y.style.zIndex = 101, y.style.overflow = "hidden", y.className = c.q("_shadow"), y.info = h;
                                    var w = document.createElement("div");
                                    w.className = c.q("_shadow_inner"), y.appendChild(w), c.vg.appendChild(y), b.divs.push(y)
                                }
                            }), b.invalid) {
                            var r = c.q("_shadow_overlap");
                            return void DayPilot.list(b.divs).each(function(e) {
                                DayPilot.Util.addClass(e, r)
                            })
                        }
                        DayPilot.list(b.divs).each(function(e) {
                            if (e.info) {
                                var t = e,
                                    i = e.info,
                                    a = e.info.event,
                                    r = c.rowlist[a.part.dayIndex + o],
                                    l = c.getPixels(i.start),
                                    s = c.getPixels(i.end),
                                    d = l.left,
                                    h = s.left - d,
                                    u = DayPilot.list(b.list).map(function(e) {
                                        return e.event.data
                                    }).add(n.resizing.event.data);
                                c.lj(t, r, d, h, u), t.overlapping && (i.overlapping = !0, b.forbidden = !0)
                            }
                        })
                    }
                }, this.mj = {}, this.mj.modifiedProps = function() {
                    var e = new DayPilot.Scheduler,
                        t = DayPilot.Util.members(c, 2),
                        i = [];
                    return i.push("<div id='dp'></div>"), i.push("<script>"), i.push("var dp = new DayPilot.Scheduler('dp');"), t.properties.each(function(t) {
                        var n, a, o = t.name,
                            r = o.split(".");
                        if (r.length > 1) {
                            var l = r[0],
                                s = r[1];
                            n = JSON.stringify(e[l][s]), a = JSON.stringify(c[l][s])
                        } else n = JSON.stringify(e[o]), a = JSON.stringify(c[o]);
                        n !== a && i.push("dp." + o + " = " + a + ";")
                    }), t.events.each(function(e) {
                        var t = e.name;
                        "function" == typeof c[t] && i.push("dp." + t + " = " + c[t].toString() + ";")
                    }), i.push("dp.init();"), i.push("</script>"), i.join("\n")
                }, this.nj = {};
                var w = this.nj;
                w.divs = [], w.list = [], w.forbidden = !1, w.verticalAll = function() {
                    return "All" === c.multiMoveVerticalMode
                }, w.additional = function() {
                    var e = DayPilot.list(),
                        t = c.multiselect.isSelected(n.movingEvent),
                        i = c.allowMultiMove,
                        e = DayPilot.list();
                    if (t && i && (e = DayPilot.list(c.multiselect.divs).filter(function(e) {
                            return e !== n.moving && e !== A.moving
                        })), c.jointEventsMove) {
                        var a = y.findJointDivs(n.movingEvent);
                        e = e.concat(a)
                    }
                    return e
                }, w.update = function() {
                    w.clear(), w.draw()
                }, w.draw = function() {
                    if (n.moving) {
                        var e = n.movingEvent,
                            t = n.movingShadow.start,
                            i = t.getTime() - e.start().getTime(),
                            a = e.part.dayIndex,
                            o = DayPilot.indexOf(c.rowlist, n.movingShadow.row),
                            r = o - a;
                        if (w.verticalAll() || (r = 0), w.list = [], w.forbidden = !1, w.invalid = !1, w.rowoffset = r, w.additional().each(function(e) {
                                if (e.event) {
                                    var t = e.event,
                                        n = c.rowlist[t.part.dayIndex + r];
                                    if (!n) return void(w.invalid = !0);
                                    var a = t.part.top + n.top,
                                        o = t.part.height;
                                    r && (a = n.top, o = n.height);
                                    var l = t.start().addTime(i),
                                        s = t.end().addTime(i),
                                        d = {};
                                    d.event = t, d.start = l, d.end = s, d.resource = n.id, d.overlapping = !1, w.list.push(d);
                                    var h = c.getPixels(l),
                                        u = c.getPixels(s),
                                        f = DayPilot.DateUtil.diff(t.rawend(), t.start());
                                    f = Math.max(f, 1);
                                    var v = _.useBox(f),
                                        p = v ? h.boxLeft : h.left,
                                        g = v ? u.boxRight - p : u.left - p,
                                        m = document.createElement("div");
                                    m.style.position = "absolute", m.style.left = p + "px", m.style.top = a + "px", m.style.height = o + "px", m.style.width = g + "px", m.style.zIndex = 101, m.style.overflow = "hidden", m.className = c.q("_shadow"), m.info = d;
                                    var y = document.createElement("div");
                                    y.className = c.q("_shadow_inner"), m.appendChild(y), c.vg.appendChild(m), w.divs.push(m)
                                }
                            }), w.invalid) {
                            var l = c.q("_shadow_overlap");
                            return void DayPilot.list(w.divs).each(function(e) {
                                DayPilot.Util.addClass(e, l)
                            })
                        }
                        DayPilot.list(w.divs).each(function(e) {
                            if (e.info) {
                                var t = e,
                                    i = e.info,
                                    a = e.info.event,
                                    o = c.rowlist[a.part.dayIndex + r],
                                    l = c.getPixels(i.start),
                                    s = c.getPixels(i.end),
                                    d = l.left,
                                    h = s.left - d,
                                    u = DayPilot.list(w.list).map(function(e) {
                                        return e.event.data
                                    }).add(n.movingEvent.data);
                                c.lj(t, o, d, h, u), t.overlapping && (i.overlapping = !0, w.forbidden = !0)
                            }
                        })
                    }
                }, w.clear = function() {
                    DayPilot.de(w.divs), w.divs = []
                }, this.oj = function() {
                    var e = c.coords,
                        t = n.resizing.dpBorder,
                        i = n.resizing.event,
                        a = i.part.left;
                    "right" == t && (a += i.part.width);
                    var o, r, l = n.resizing.event.calendar.cellWidth,
                        s = n.resizing.event.part.width,
                        d = n.resizing.event.part.left,
                        h = 0,
                        u = e.x - a;
                    if ("right" === t) {
                        if (o = d, c.snapToGrid) {
                            var f = c.ch(s + d + u).cell;
                            r = f.left + f.width - d, r < l && (r = l)
                        } else r = s + u;
                        var v = c.Jg();
                        d + r > v && (r = v - d), n.resizingShadow.left = d, n.resizingShadow.width = r
                    } else {
                        if ("left" !== t) throw "Invalid dpBorder.";
                        c.snapToGrid ? (u >= s && (u = s), o = Math.floor((d + u + 0) / l) * l, o < h && (o = h)) : o = d + u, r = s - (o - d);
                        var p = d + s,
                            g = l;
                        c.snapToGrid ? "Never" === c.useEventBoxes && (g = s < l ? s : 1) : g = 1, r < g && (r = g, o = p - r), n.resizingShadow.left = o, n.resizingShadow.width = r
                    }! function() {
                        var e = n.resizing.event,
                            t = c.rowlist[e.part.dayIndex],
                            i = o,
                            a = r;
                        c.lj(n.resizingShadow, t, i, a, e.data)
                    }(), c.pj()
                }, this.$i = function() {
                    this.nav.scroll;
                    if (c.coords && n.movingEvent) {
                        c.Ng();
                        var e = n.movingShadow,
                            t = this.li(n.movingEvent);
                        if (!c.mi(t.rowIndex)) {
                            var i = n.movingEvent,
                                a = 0,
                                o = 0;
                            ! function() {
                                if (c.cellStacking) {
                                    var e = t.relativeY,
                                        i = c.ch(t.left).x,
                                        n = t.row,
                                        r = n.evColumns[i],
                                        l = r.events.find(function(t) {
                                            return e < t.part.top + t.part.height + c.eventMarginBottom
                                        });
                                    l || (l = r.events.last()), l && (o = l.part.top, a = DayPilot.indexOf(r.events, l), e - l.part.top > l.part.height / 2 && (o = l.part.top + l.part.height + c.eventMarginBottom / 2, a += 1))
                                } else {
                                    for (var e = t.relativeY, n = t.row, s = 0, d = c.zc.eventHeight(), h = n.lines.length, u = 0; u < n.lines.length; u++) {
                                        if (n.lines[u].isFree(t.left, c.cellWidth)) {
                                            h = u;
                                            break
                                        }
                                    }
                                    var f = Math.floor((e - s + d / 2) / d),
                                        f = Math.min(h, f),
                                        f = Math.max(0, f);
                                    a = f, o = c.rowMarginTop + a * (c.zc.eventHeight() + c.eventMarginBottom) + c.eventMarginBottom / 2
                                }
                            }(), o > 0 && (o -= 3);
                            var r = i.cache && "undefined" != typeof i.cache.moveVDisabled ? !i.cache.moveVDisabled : !i.data.moveVDisabled,
                                l = i.cache && "undefined" != typeof i.cache.moveHDisabled ? !i.cache.moveHDisabled : !i.data.moveHDisabled,
                                s = !w.additional().isEmpty();
                            if (s && "Disabled" === c.multiMoveVerticalMode && (r = !1), r)
                                if (this.mi(t.rowIndex)) {
                                    var d = e.row,
                                        h = 1;
                                    if (!d) return;
                                    h = t.rowIndex < d.index ? 1 : -1;
                                    for (var u = t.rowIndex; u !== d.index; u += h) {
                                        var f = this.rowlist[u];
                                        if (!this.mi(u) && !f.hidden) {
                                            e.style.top = f.top + "px", e.style.height = Math.max(f.height, 0) + "px", e.row = f, c.eventMoveToPosition && (a = h > 0 ? 0 : f.lines.length - 1, e.style.top = t.top + o + "px", e.style.height = "3px", e.line = a);
                                            break
                                        }
                                    }
                                } else e.row = t.row, e.style.height = Math.max(t.row.height, 0) + "px", e.style.top = t.top + "px", c.eventMoveToPosition && (e.style.top = t.top + o + "px", e.style.height = "3px", e.line = a);
                            else {
                                for (var d = c.rowlist[i.part.dayIndex], p = d.lines.length, u = 0; u < d.lines.length; u++) {
                                    if (d.lines[u].isFree(t.left, c.cellWidth)) {
                                        p = u;
                                        break
                                    }
                                }
                                if (s || (e.style.height = Math.max(d.height, 0) + "px", e.style.top = d.top + "px"), e.row = d, c.eventMoveToPosition && !s)
                                    if (t.row === d) e.style.top = d.top + o + "px", e.style.height = "3px", e.line = a;
                                    else {
                                        var g = t.rowIndex > d.index && p > 0 ? p * c.zc.eventHeight() - 3 : 0;
                                        e.style.top = d.top + g + "px", e.style.height = "3px", e.line = 0
                                    }
                            }
                            l ? (e.style.left = t.left + "px", c.eventMoveToPosition ? e.style.width = c.cellWidth + "px" : e.style.width = t.width + "px", e.start = t.start, e.end = t.end) : (e.style.left = i.part.left + "px", e.start = i.start(), e.end = i.rawend(), t.left = i.part.left),
                                function() {
                                    var n = e.row,
                                        a = i.data,
                                        o = t.width,
                                        r = t.left,
                                        l = DayPilot.list(w.list).map(function(e) {
                                            return e.event.data
                                        }).add(a);
                                    c.lj(e, n, r, o, l)
                                }(),
                                function() {
                                    var t = c.Lc;
                                    if (!t || t.start.getTime() !== e.start.getTime() || t.end.getTime() !== e.end.getTime() || t.resource !== e.row.id) {
                                        w.update();
                                        var r = !!n.drag && i.part.external,
                                            l = {};
                                        l.start = e.start, l.end = c.mf(e.end), l.duration = new DayPilot.Duration(l.start, l.end), l.e = i, l.external = r, l.resource = e.row.id, l.html = null, l.row = c.Ph(e.row), l.position = e.line, l.overlapping = e.overlapping || w.forbidden, l.allowed = !0, l.left = {}, l.left.html = l.start.toString(c.eventMovingStartEndFormat, _.locale()), l.left.enabled = c.eventMovingStartEndEnabled, l.left.space = 5, l.left.width = null, l.right = {}, l.right.html = l.end.toString(c.eventMovingStartEndFormat, _.locale()), l.right.enabled = c.eventMovingStartEndEnabled, l.right.space = 5, l.right.width = null, l.multimove = DayPilot.list(w.list), l.shift = c.coords.shift, l.ctrl = c.coords.ctrl, l.meta = c.coords.meta, l.alt = c.coords.alt, l.areaData = DayPilot.Global.movingAreaData, l.link = null;
                                        var s = {};
                                        s.event = i, s.start = l.start, s.end = l.end, s.overlapping = l.overlapping, s.resource = l.resource, l.multimove.splice(0, 0, s), c.Lc = l;
                                        var d = {
                                            "start": l.start,
                                            "end": l.end,
                                            "resource": l.resource
                                        };
                                        if ("function" == typeof c.onEventMoving && c.onEventMoving(l), e.allowed = l.allowed, l.start !== d.start || l.end !== d.end) {
                                            var h = l.start,
                                                u = c.Me(l.end);
                                            e.start = h, e.end = u;
                                            var f = DayPilot.DateUtil.diff(h, u);
                                            f = Math.max(f, 1);
                                            var p = _.useBox(f),
                                                g = p ? c.getPixels(h).boxLeft : c.getPixels(h).left,
                                                m = p ? c.getPixels(u).boxRight : c.getPixels(u).left;
                                            e.style.left = g + "px", e.style.width = m - g + "px"
                                        }
                                        if (l.resource !== d.resource) {
                                            var y = c.yg(l.resource);
                                            y && (e.row = y, e.style.height = Math.max(y.height, 0) + "px", e.style.top = y.top + "px", c.eventMoveToPosition && (e.style.top = y.top + o + "px", e.style.height = "3px", e.line = a))
                                        }
                                        l.html ? e.firstChild.innerHTML = l.html : e.firstChild.innerHTML = "", c.qj(e, l), c.rj(n.movingShadow, l), l.link && (DayPilot.Global.movingLink && DayPilot.Global.movingLink.clear(), DayPilot.Global.movingLink = v.drawLink(l.link.from, e, l.link))
                                    }
                                }()
                        }
                    }
                }, this.lj = function(e, t, i, n, a) {
                    if (!c.allowEventOverlap) {
                        ! function() {
                            e.overlapping = !1;
                            for (var o = 0; o < t.lines.length; o++) {
                                if (!t.lines[o].isFree(i, n, a)) return void(e.overlapping = !0)
                            }
                            if (c.allowMultiRange) {
                                var r = t.index;
                                return DayPilot.list(H.list).some(function(t) {
                                    var a = t.div,
                                        o = parseInt(a.style.left),
                                        l = parseInt(a.style.width),
                                        s = t.start.y;
                                    return e !== a && (s === r && DayPilot.Util.overlaps(i, i + n, o, o + l))
                                }) ? void(e.overlapping = !0) : void 0
                            }
                        }();
                        var o = e.overlapping,
                            r = c.q("_shadow_overlap");
                        o ? DayPilot.Util.addClass(e, r) : DayPilot.Util.removeClass(e, r)
                    }
                }, this.qj = function(e, t) {
                    var i = c.q("_shadow_forbidden");
                    !t.allowed || w.invalid ? DayPilot.Util.addClass(e, i) : DayPilot.Util.removeClass(e, i)
                }, this.rj = function(e, t) {
                    if (this.sj(), e.calendar === c) {
                        var i = {};
                        i.left = parseInt(e.style.left), i.top = parseInt(e.style.top), i.right = i.left + parseInt(e.style.width);
                        var n = t.left.width || 10,
                            a = document.createElement("div");
                        if (a.style.position = "absolute", a.style.left = i.left - n - t.left.space + "px", a.style.top = i.top + "px", a.style.height = c.eventHeight + "px", a.style.overflow = "hidden", a.style.boxSizing = "border-box", a.innerHTML = t.left.html, a.className = this.q("_event_move_left"), a.onmousemove = c.tj, t.left.enabled)
                            if (c.divHover.appendChild(a), t.left.width) a.style.width = n + "px";
                            else {
                                a.style.whiteSpace = "nowrap";
                                var o = a.offsetWidth,
                                    r = i.left - o - t.left.space;
                                a.style.width = o + "px", a.style.left = r + "px";
                                var l = c.nav.scroll.scrollLeft;
                                r < l && c.divHover.removeChild(a);
                                var s = c.uj() + (r - l);
                                s > 0 && (a.style.left = s + "px", a.style.top = i.top - c.nav.scroll.scrollTop + c.Lg() + "px", c.nav.top.appendChild(a), c.elements.hover.push(a))
                            } var n = t.right.width || 10,
                            d = document.createElement("div");
                        if (d.style.position = "absolute", d.style.left = i.right + t.right.space + "px", d.style.top = i.top + "px", d.style.height = c.eventHeight + "px", d.style.overflow = "hidden", d.style.boxSizing = "border-box", t.right.width ? d.style.width = t.right.width + "px" : d.style.whiteSpace = "nowrap", d.innerHTML = t.right.html, d.className = this.q("_event_move_right"), d.onmousemove = c.tj, t.right.enabled) {
                            c.divHover.appendChild(d);
                            var h = c.nav.scroll.scrollWidth;
                            i.right + t.right.space + d.offsetWidth >= h && c.divHover.removeChild(d)
                        }
                    }
                }, this.sj = function() {
                    c.divHover.innerHTML = "", DayPilot.de(c.elements.hover), c.elements.hover = []
                }, this.vj = function() {
                    var e = c.rowHeaderColumnDefaultWidth;
                    this.rowHeaderColumns && (this.rowHeaderCols = DayPilot.Util.propArray(this.rowHeaderColumns, "width", e))
                }, this.uj = function() {
                    var e = 0;
                    if (this.vj(), this.rowHeaderCols)
                        for (var t = 0; t < this.rowHeaderCols.length; t++) e += this.rowHeaderCols[t];
                    else e = this.rowHeaderWidth;
                    return e
                }, this.wj = function() {
                    return this.xj(c.progressiveRowRenderingPreload)
                }, this.xj = function(e) {
                    var e = e || 0,
                        t = 0,
                        i = c.rowlist.length;
                    if (c.progressiveRowRendering) {
                        var n = c.Ag();
                        t = n.yStart, i = n.yEnd + 1, t = Math.max(0, t - e), i = Math.min(c.rowlist.length, i + e)
                    }
                    return {
                        "start": t,
                        "end": i
                    }
                }, this.yj = function() {
                    if (this.Vc()) {
                        var e = !1,
                            t = 0;
                        if (this.rowHeaderWidthAutoFit) {
                            var i = this.divHeader;
                            if (!i) return;
                            if (!i.rows) return;
                            for (var n = [], a = c.wj(), o = a.start; o < a.end; o++) {
                                var r = i.rows[o];
                                if (r && !r.hidden && !r.autofitDone) {
                                    r.autofitDone = !0;
                                    for (var l = 0; l < r.cells.length; l++) {
                                        var s = r.cells[l].firstChild.firstChild;
                                        if (s && s.style) {
                                            var d = s.style.width,
                                                h = s.style.right;
                                            s.style.position = "absolute", s.style.width = "auto", s.style.right = "auto", s.style.whiteSpace = "nowrap";
                                            var u = s.offsetWidth + 2;
                                            s.style.position = "", s.style.width = d, s.style.right = h, s.style.whiteSpace = "", "undefined" == typeof n[l] && (n[l] = 0), n[l] = Math.max(n[l], u)
                                        }
                                    }
                                }
                            }
                            if (this.vj(), this.rowHeaderCols)
                                for (var o = 0; o < n.length; o++) this.rowHeaderCols[o] && (n[o] > this.rowHeaderCols[o] && (this.rowHeaderCols[o] = n[o], e = !0), t += this.rowHeaderCols[o]);
                            else t = this.rowHeaderWidth, this.rowHeaderWidth < n[0] + c.rowHeaderWidthMarginRight && (t = n[0] + c.rowHeaderWidthMarginRight, e = !0)
                        }
                        e && (this.ki && (this.ki.widths = this.rowHeaderCols, this.ki.updateWidths(), DayPilot.Util.updatePropsFromArray(this.rowHeaderColumns, "width", this.rowHeaderCols)), this.rowHeaderScrolling || (this.rowHeaderWidth = t), this.rh(), this.zj())
                    }
                }, this.dg = function() {
                    this.Aj = !0, this.vj();
                    var e = this.uj(),
                        t = this.divHeader;
                    if (t) {
                        if (DayPilot.browser.ie)
                            for (var i = 0; i < t.childNodes.length; i++) DayPilot.de(t.childNodes[i]);
                        t.innerHTML = "", DayPilot.puc(t)
                    } else t = document.createElement("div"), t.onmousemove = function() {
                        c.U()
                    }, this.divHeader = t;
                    if (t.style.width = e + "px", t.style.height = c.Zf + "px", t.rows = [], c.progressiveRowRendering) a();
                    else
                        for (var n = this.rowlist.length, i = 0; i < n; i++) c.Bj(i);
                    c.Cj(), this.divResScroll.appendChild(t), this.rowHeaderWidthAutoFit && this.yj()
                }, this.uh = function() {
                    if (c.progressiveRowRendering) {
                        for (var e = this.wj(), t = 0; t < c.rowlist.length; t++) e.start <= t && t < e.end ? c.Bj(t) : c.Dj(t);
                        if (this.rowHeaderWidthAutoFit) {
                            var i = c.Mg();
                            this.yj();
                            if (c.Mg() !== i) {
                                var n = this.cellWidth;
                                this.ag();
                                this.cellWidth !== n && (c.bg(), c.eg(), c.la(), c.da(), c.Ej())
                            }
                        }
                    }
                }, this.Cj = function() {
                    var e = c.divHeader,
                        t = document.createElement("div");
                    t.style.position = "absolute", e.appendChild(t), c.nav.resScrollSpace = t, t.setAttribute("unselectable", "on");
                    var i = document.createElement("div");
                    i.style.position = "relative", i.style.height = "100%", i.className = this.q("_rowheader"), t.appendChild(i);
                    var n = this.uj(),
                        t = c.nav.resScrollSpace;
                    t.style.width = n + "px", t.style.top = this.Zf + "px", t.style.height = c.divResScroll.clientHeight + 20 + "px"
                }, this.Dj = function(e) {
                    var t = c.divHeader.rows[e];
                    t && (DayPilot.de(t.cells), c.divHeader.rows[e] = null)
                }, this.Bh = function(e) {
                    this.Dj(e), this.Bj(e)
                }, this.Bj = function(e) {
                    var t = c.divHeader,
                        i = this.divHeader;
                    if (!i.rows[e]) {
                        var n = this.rowHeaderCols,
                            a = n ? this.rowHeaderCols.length : 0,
                            o = this.uj(),
                            r = this.rowlist[e];
                        if (r && !r.hidden) {
                            var s = this.ij(r);
                            i.rows[e] = {}, i.rows[e].cells = [];
                            var d = document.createElement("div");
                            d.style.position = "absolute", d.style.top = r.top + "px", d.row = r, d.index = e;
                            var h = s.row,
                                u = n ? n[0] : this.rowHeaderWidth;
                            d.style.width = u + "px", d.style.border = "0px none", h.toolTip && (d.title = h.toolTip), d.setAttribute("unselectable", "on"), "undefined" != typeof h.ariaLabel ? d.setAttribute("aria-label", h.ariaLabel) : d.setAttribute("aria-label", h.html), d.onmousemove = c.Fj, d.onmouseout = c.Gj, d.onmouseup = c.Hj, d.oncontextmenu = c.Ij, d.onclick = c.Jj, d.ondblclick = c.Kj;
                            var f = document.createElement("div");
                            f.style.width = u + "px", f.setAttribute("unselectable", "on"), f.className = this.q("_rowheader"), h.cssClass && DayPilot.Util.addClass(f, h.cssClass), h.backColor && (f.style.background = h.backColor), f.style.height = r.height + "px", f.style.overflow = "hidden", f.style.position = "relative";
                            var v = document.createElement("div");
                            v.setAttribute("unselectable", "on"), v.className = this.q("_rowheader_inner"), f.appendChild(v);
                            var p = "Disabled" !== this.rowMoveHandling,
                                g = 10,
                                m = h.areas || [];
                            p && !h.moveDisabled && m.push({
                                "v": "Hover",
                                "w": g,
                                "bottom": 0,
                                "top": 0,
                                "left": 0,
                                "css": c.q("_rowmove_handle"),
                                "action": "Move"
                            });
                            var y = c.Ph(r);
                            DayPilot.Areas.attach(f, y, {
                                "areas": m,
                                "allowed": function() {
                                    return !l.row
                                }
                            });
                            var b = document.createElement("div");
                            if (b.style.position = "absolute", b.style.bottom = "0px", b.style.width = "100%", b.style.height = "1px", b.className = this.q(E.resourcedivider), f.appendChild(b), function() {
                                    if (c.treeEnabled && !r.isNewRow) {
                                        var t = r.level * c.treeIndent + c.treeImageMarginLeft;
                                        p && (t += g);
                                        var i = 10,
                                            n = document.createElement("div");
                                        n.style.width = "10px", n.style.height = "10px", n.style.backgroundRepeat = "no-repeat", n.style.position = "absolute", n.style.left = t + "px", n.style.top = c.treeImageMarginTop + "px", r.loaded || 0 !== r.children.length ? r.children.length > 0 ? (r.expanded ? n.className = c.q("_tree_image_collapse") : n.className = c.q("_tree_image_expand"), n.style.cursor = "pointer", n.index = e, n.onclick = function(e) {
                                            c.Zi(this.index), e = e || window.event, e.cancelBubble = !0
                                        }) : n.className = c.q("_tree_image_no_children") : (n.className = c.q("_tree_image_expand"), n.style.cursor = "pointer", n.index = e, n.onclick = function(e) {
                                            c.Lj(this.index), e = e || window.event, e.cancelBubble = !0
                                        }), v.appendChild(n)
                                    }
                                    var a = document.createElement("div");
                                    c.treeEnabled && (a.style.marginLeft = t + i + "px"), a.innerHTML = h.html, d.textDiv = a, d.cellDiv = f, v.appendChild(a)
                                }(), d.appendChild(f), t.appendChild(d), i.rows[e].cells.push(d), r.columns && 0 !== r.columns.length)
                                for (var w = u, D = 1; D < a; D++) {
                                    var k = h.columns[D - 1] || {},
                                        d = document.createElement("div");
                                    d.style.position = "absolute", d.style.top = r.top + "px", d.style.left = w + "px", t.appendChild(d), i.rows[e].cells.push(d), d.row = r, d.index = e, h.toolTip && (d.title = h.toolTip), d.setAttribute("unselectable", "on"), d.onmousemove = c.Fj, d.onmouseout = c.Gj, d.onmouseup = c.Hj, d.oncontextmenu = c.Ij, d.onclick = c.Jj, d.ondblclick = c.Kj;
                                    var f = document.createElement("div"),
                                        x = n[D];
                                    w += x;
                                    var P = k.backColor || h.backColor;
                                    P && (f.style.backgroundColor = P), f.style.width = x + "px", f.style.height = r.height + "px", f.style.overflow = "hidden", f.style.position = "relative", f.setAttribute("unselectable", "on"), DayPilot.Util.addClass(f, this.q("_rowheader")), DayPilot.Util.addClass(f, this.q("_rowheadercol")), DayPilot.Util.addClass(f, this.q("_rowheadercol" + D)), h.cssClass && DayPilot.Util.addClass(f, h.cssClass), k.cssClass && DayPilot.Util.addClass(f, k.cssClass);
                                    var v = document.createElement("div");
                                    v.setAttribute("unselectable", "on"), v.className = this.q("_rowheader_inner"), f.appendChild(v);
                                    var b = document.createElement("div");
                                    b.style.position = "absolute", b.style.bottom = "0px", b.style.width = "100%", b.style.height = "1px", b.className = this.q("_resourcedivider"), f.appendChild(b);
                                    var C = document.createElement("div"),
                                        S = k.html || "";
                                    C.innerHTML = S, d.textDiv = C, d.cellDiv = f, v.appendChild(C), DayPilot.Areas.attach(f, y, {
                                        "areas": k.areas,
                                        "allowed": function() {
                                            return !l.row
                                        }
                                    }), d.appendChild(f)
                                } else d.colSpan = a > 0 ? a : 1, f.style.width = o + "px"
                        }
                    }
                }, this.Ij = function() {
                    var e = this.row;
                    return e.contextMenu && e.contextMenu.show(c.Ph(e)), !1
                }, this.Jj = function(e) {
                    if (!x.cancelClick) {
                        var t = this.row,
                            i = c.Ph(t, this.index);
                        return t.isNewRow ? void c.Vh.edit(t) : void c.Sh(i, e.ctrlKey, e.shiftKey, e.metaKey)
                    }
                }, this.Kj = function(e) {
                    if (c.timeouts.resClick) {
                        for (var t in c.timeouts.resClick) window.clearTimeout(c.timeouts.resClick[t]);
                        c.timeouts.resClick = null
                    }
                    var i = this.row,
                        n = c.Ph(i, this.index);
                    if (c.va()) {
                        var a = {};
                        if (a.resource = n, a.row = n, a.preventDefault = function() {
                                this.preventDefault.value = !0
                            }, "function" == typeof c.onRowDoubleClick && (c.onRowDoubleClick(a), a.preventDefault.value)) return;
                        switch (c.rowDoubleClickHandling) {
                            case "PostBack":
                                c.rowDoubleClickPostBack(n);
                                break;
                            case "CallBack":
                                c.rowDoubleClickCallBack(n);
                                break;
                            case "Select":
                                c.Uh(i, e.ctrlKey, e.shiftKey, e.metaKey);
                                break;
                            case "Edit":
                                c.Vh.edit(i)
                        }
                        "function" == typeof c.onRowDoubleClicked && c.onRowDoubleClicked(a)
                    } else switch (c.rowDoubleClickHandling) {
                        case "PostBack":
                            c.rowDoubleClickPostBack(n);
                            break;
                        case "CallBack":
                            c.rowDoubleClickCallBack(n);
                            break;
                        case "JavaScript":
                            c.onRowDoubleClick(n);
                            break;
                        case "Select":
                            c.Uh(i, e.ctrlKey, e.shiftKey, e.metaKey);
                            break;
                        case "Edit":
                            c.Vh.edit(i)
                    }
                }, this.rowDoubleClickPostBack = function(e, t) {
                    var i = {};
                    i.resource = e, this.F("RowDoubleClick", i, t)
                }, this.rowDoubleClickCallBack = function(e, t) {
                    var i = {};
                    i.resource = e, this.H("RowDoubleClick", i, t)
                }, this.Mj = function(e) {
                    var t = {};
                    t.start = this.cell.start, t.level = this.cell.level, t.end = this.cell.end, t.end || (t.end = new DayPilot.Date(t.start).addMinutes(c.cellDuration)), c.Wh(t)
                }, this.Ph = function(e) {
                    return new DayPilot.Row(e, c)
                }, this.Xg = function(e) {
                    var t = this.rowlist[e];
                    if (t.events || t.resetEvents(), !t.data) {
                        t.data = {}, t.data.start = new DayPilot.Date(t.start), t.data.startTicks = t.data.start.getTime();
                        var i = this.$g().getTime() - this.Da().getTime();
                        t.data.end = _.isResourcesView() ? t.data.start.addTime(i) : t.data.start.addDays(1), t.data.endTicks = t.data.end.getTime(), t.data.offset = t.start.getTime() - this.Da().getTime(), t.data.i = e
                    }
                }, this.da = function(e) {
                    if (e ? this.events.list = e : this.events.list || (this.events.list = []), null != this.events.list && !DayPilot.isArray(this.events.list)) throw "DayPilot.Scheduler.events.list expects an array object";
                    D.prepareRows(!0);
                    for (var t, i = this.events.list, n = i.length, a = "function" == typeof this.onBeforeEventRender, o = "Resources" === c.viewType, r = 0; r < n; r++) {
                        var l = i[r];
                        if (l) {
                            if (l instanceof DayPilot.Event) throw "DayPilot.Scheduler: DayPilot.Event object detected in events.list array. Use raw event data instead.";
                            a && this.$c(r), "*" === l.resource ? t = c.rowlist : o ? t = DayPilot.list(D.rowcache[l.resource]).concat(D.rowcache["*"]) : "Days" === c.viewType ? t = c.rowlist : "Gantt" === c.viewType && (t = D.rowcache[l.id]);
                            for (var s = 0; t && s < t.length; s++) {
                                var d = t[s],
                                    h = this.yi(l, d);
                                h && a && (h.cache = this.t.events[r])
                            }
                        }
                    }
                    DayPilot.list(c.rowlist).each(function(e) {
                        c.Ci(e)
                    }), this.mh()
                }, this.Nj = {};
                var D = this.Nj;
                D.rowCache = {}, D.prepareRows = function(e) {
                    D.rowcache = {};
                    for (var t = 0; t < c.rowlist.length; t++) {
                        var i = c.rowlist[t];
                        if (e && i.resetEvents(), c.Xg(t), i.id) {
                            var n = i.id.toString();
                            D.rowcache[n] || (D.rowcache[n] = DayPilot.list()), D.rowcache[n].push(i)
                        }
                    }
                }, D.loadEvent = function(e) {}, this.xi = function() {
                    var e = {};
                    if ("Resources" !== c.viewType) return !1;
                    for (var t = 0; t < c.rowlist.length; t++) {
                        var i = c.rowlist[t],
                            n = i.id;
                        if (e[n]) return !0;
                        e[n] = !0
                    }
                    return !1
                }, this.$c = function(e) {
                    var t = this.t.events,
                        i = this.events.list[e],
                        n = {};
                    i instanceof DayPilot.Event && (i = i.data);
                    for (var a in i) n[a] = i[a];
                    if ("function" == typeof this.onBeforeEventRender) {
                        var o = {};
                        o.e = n, o.data = n, this.onBeforeEventRender(o)
                    }
                    t[e] = n
                }, this.Ci = function(e) {
                    if (e.lines = [], e.sections = null, !e.isNewRow) {
                        this.sortDirections ? e.events.sort(this.gd) : c.cellStacking || e.events.sort(this.fd);
                        if (c.cellStacking) return void S.loadRow(e);
                        var t = c.groupConcurrentEvents;
                        if (t)
                            for (var i = 0; i < e.blocks.length; i++) e.blocks[i].clear();
                        DayPilot.list(e.events).each(function(t) {
                            if (!t.part.height) return e.eventHeight ? void(t.part.height = e.eventHeight) : void(t.part.height = _.eventHeight())
                        });
                        for (var n = 0; n < e.events.length; n++) {
                            var a = e.events[n];
                            e.putIntoLine(a), t && e.putIntoBlock(a)
                        }
                        for (var o = 0, i = 0; i < e.lines.length; i++) {
                            var r = e.lines[i];
                            r.top = o, o += (r.height || e.eventHeight) * e.eventStackingLineHeight / 100
                        }
                        if (t)
                            for (var n = 0; n < e.blocks.length; n++) {
                                var l = e.blocks[n];
                                l.lines = [], l.events.sort(this.gd);
                                for (var s = 0; s < l.events.length; s++) {
                                    var a = l.events[s];
                                    l.putIntoLine(a)
                                }
                                l.lines.length <= c.groupConcurrentEventsLimit && (l.expanded = !0);
                                var d = DayPilot.list(l.events).map(function(e) {
                                    return e.id()
                                }).some(function(e) {
                                    return DayPilot.contains(c.Oj, e)
                                });
                                d && (l.expanded = !0);
                                for (var o = 0, i = 0; i < l.lines.length; i++) {
                                    var r = l.lines[i];
                                    r.top = o, o += (r.height || e.eventHeight) * e.eventStackingLineHeight / 100
                                }
                            }
                    }
                }, this.lh = function(e) {
                    e = DayPilot.ua(e);
                    for (var t = 0; t < e.length; t++) {
                        var i = e[t];
                        c.Ci(c.rowlist[i])
                    }
                    if (c.cellStacking) S.calculateEventPositions();
                    else
                        for (var t = 0; t < e.length; t++) {
                            var i = e[t],
                                n = c.rowlist[i];
                            c.gj(n)
                        }
                }, this.Li = function() {
                    var e = "Manual" === c.scale ? c.itline[0].start : c.startDate;
                    return DayPilot.list(c.rowlist).some(function(t) {
                        return !!t.start && t.start.getTime() !== new DayPilot.Date(e).getTime()
                    })
                }, this.yi = function(e, t) {
                    if (!t.hideEvents) {
                        var i = new DayPilot.Date(e.start),
                            n = new DayPilot.Date(e.end);
                        n = c.Me(n);
                        var a = i.ticks,
                            o = n.ticks;
                        if (o < a) return null;
                        var r = null;
                        if ("function" == typeof c.onBeforeEventRender) {
                            var l = DayPilot.indexOf(c.events.list, e);
                            r = c.t.events[l]
                        }
                        if (r) {
                            if (r.hidden) return null
                        } else if (e.hidden) return null;
                        var s = !1;
                        switch (this.viewType) {
                            case "Days":
                                s = !(o <= t.data.startTicks || a >= t.data.endTicks) || a === o && a === t.data.startTicks;
                                break;
                            case "Resources":
                                s = (t.id === e.resource || "*" === t.id || "*" === e.resource) && (!(o <= t.data.startTicks || a >= t.data.endTicks) || a === o && a === t.data.startTicks);
                                break;
                            case "Gantt":
                                s = t.id === e.id && !(o <= t.data.startTicks || a >= t.data.endTicks)
                        }
                        if (!s) return null;
                        var d = new DayPilot.Event(e, c);
                        d.part.dayIndex = t.data.i, d.part.start = t.data.startTicks < a ? i : t.data.start, d.part.end = t.data.endTicks > o ? n : t.data.end;
                        var h = this.getPixels(d.part.start.addTime(-t.data.offset)),
                            u = this.getPixels(d.part.end.addTime(-t.data.offset));
                        d.part.start === d.part.end && (u = this.getPixels(d.part.end.addTime(-t.data.offset).addSeconds(1))), r && r.height ? d.part.height = r.height : e.height && (d.part.height = e.height), d.part.Pj = function() {
                            var e = this.height;
                            if (c.eventVersionsEnabled && !DayPilot.list(d.data.versions).isEmpty()) {
                                var t = d.versions.length;
                                e += t * c.eventVersionHeight, e += t * c.eventVersionMargin
                            }
                            return e + c.eventMarginBottom
                        };
                        var f = h.left,
                            v = u.left;
                        if (d.part.startPixels = h, d.part.endPixels = u, f === v) {
                            if (h.cut || u.cut) return null;
                            var p = (d.part.start.addTime(-t.data.offset).getTime() + d.part.end.addTime(-t.data.offset).getTime()) / 2;
                            if (c.getPixels(new DayPilot.Date(Math.floor(p))).cut) return null
                        }
                        d.part.box = _.useBox(o - a);
                        var g = c.eventHeight;
                        if ("Milestone" === e.type) {
                            var m = e.width || g;
                            d.part.end = d.part.start, d.part.left = f - m / 2, d.part.width = m, d.part.barLeft = 0, d.part.barWidth = m
                        } else if (d.part.box) {
                            var y = h.boxLeft,
                                b = u.boxRight;
                            d.part.left = y, d.part.width = b - y, d.part.barLeft = Math.max(f - d.part.left, 0), d.part.barWidth = Math.max(v - f, 1)
                        } else d.part.left = f, d.part.width = Math.max(v - f, 1), d.part.barLeft = 0, d.part.barWidth = Math.max(v - f - 1, 1);
                        var w = c.eventMinWidth;
                        if (d.part.width = Math.max(d.part.width, w), c.eventVersionsEnabled && c.Qj(d, t), "function" == typeof c.onEventFilter && c.events.ed) {
                            var D = {};
                            if (D.filter = c.events.ed, D.visible = !0, D.e = d, c.onEventFilter(D), !D.visible) return null
                        }
                        return t.events.push(d), d
                    }
                }, this.Qj = function(e, t) {
                    e.versions = [];
                    var i = e.data,
                        n = DayPilot.list(i.versions);
                    n.isEmpty() || n.each(function(i) {
                        var n = new DayPilot.Date(i.start),
                            a = new DayPilot.Date(i.end),
                            o = c.getPixels(n.addTime(-t.data.offset)),
                            r = c.getPixels(a.addTime(-t.data.offset));
                        n.ticks === a.ticks && (r = c.getPixels(a.addTime(-t.data.offset).addTime(1)));
                        var l = o.left,
                            s = r.left;
                        if (l === s && (o.cut || r.cut)) return null;
                        var d = {};
                        d.left = l, d.continueLeft = n < t.data.start, d.right = s, d.continueRight = a > t.data.end, d.width = s - l, e.versions.push(d)
                    })
                }, this.fd = function(e, t) {
                    if (!(e && t && e.start && t.start)) return 0;
                    var i = e.start().ticks - t.start().ticks;
                    return 0 !== i ? i : t.end().ticks - e.end().ticks
                }, this.gd = function(e, t) {
                    if (!e || !t) return 0;
                    if (!(e.data && t.data && e.data.sort && t.data.sort && 0 !== e.data.sort.length && 0 !== t.data.sort.length)) return c.fd(e, t);
                    for (var i = 0, n = 0; 0 === i && "undefined" != typeof e.data.sort[n] && "undefined" != typeof t.data.sort[n];) i = e.data.sort[n] === t.data.sort[n] ? 0 : "number" == typeof e.data.sort[n] && "number" == typeof t.data.sort[n] ? e.data.sort[n] - t.data.sort[n] : c.hd(e.data.sort[n], t.data.sort[n], c.sortDirections[n]), n++;
                    return i
                }, this.hd = function(e, t, i) {
                    var n = "desc" !== i,
                        a = n ? -1 : 1,
                        o = -a;
                    if (null === e && null === t) return 0;
                    if (null === t) return o;
                    if (null === e) return a;
                    var r = [];
                    return r[0] = e, r[1] = t, r.sort(), e === r[0] ? a : o
                }, this.Uh = function(e, t, i, n) {
                    if (c.va()) {
                        var a = DayPilot.indexOf(c.rowlist, e),
                            o = c.Ph(e, a),
                            r = DayPilot.indexOf(x.selected, e) !== -1,
                            l = r ? "deselected" : "selected",
                            s = {};
                        if (s.row = o, s.selected = r, s.ctrl = t, s.shift = i, s.meta = n, s.preventDefault = function() {
                                this.preventDefault.value = !0
                            }, "function" == typeof c.onRowSelect && (c.onRowSelect(s), s.preventDefault.value)) return;
                        var d = i && !t && !n;
                        if (i || (x.Rj = {
                                "row": e,
                                "index": a
                            }), d) {
                            x.fixRowSelectLast();
                            var h = x.Rj.index,
                                u = a;
                            x.selectRange(h, u)
                        }
                        switch (c.rowSelectHandling) {
                            case "PostBack":
                                c.rowSelectPostBack(o, l);
                                break;
                            case "CallBack":
                                c.rowSelectCallBack(o, l);
                                break;
                            case "Notify":
                                !d && x.select(e, t, i, n), c.rowSelectNotify(o, l);
                                break;
                            case "Update":
                                !d && x.select(e, t, i, n)
                        }
                        "function" == typeof c.onRowSelected && (s.selected = DayPilot.indexOf(x.selected, e) !== -1, c.onRowSelected(s))
                    } else {
                        x.select(e, t, i);
                        var a = DayPilot.indexOf(c.rowlist, e),
                            o = c.Ph(e, a),
                            r = DayPilot.indexOf(x.selected, e) !== -1,
                            l = r ? "deselected" : "selected";
                        switch (c.rowSelectHandling) {
                            case "PostBack":
                                c.rowSelectPostBack(o, l);
                                break;
                            case "CallBack":
                                c.rowSelectCallBack(o, l);
                                break;
                            case "Notify":
                                c.rowSelectNotify(o, l);
                                break;
                            case "JavaScript":
                                c.onRowSelect(o, l)
                        }
                    }
                }, this.rowSelectPostBack = function(e, t, i) {
                    var n = {};
                    n.resource = e, n.change = t, this.F("RowSelect", n, i)
                }, this.rowSelectCallBack = function(e, t, i) {
                    var n = {};
                    n.resource = e, n.change = t, this.H("RowSelect", n, i)
                }, this.rowSelectNotify = function(e, t, i) {
                    var n = {};
                    n.resource = e, n.change = t, this.H("RowSelect", n, i, "Notify")
                }, this.rows = {}, this.rows.selection = {};
                var k = this.rows.selection;
                k.get = function() {
                    var e = [];
                    return DayPilot.list(x.selected).each(function(t) {
                        e.push(c.Ph(t))
                    }), e
                }, k.clear = function() {
                    x.clearSelection()
                }, k.add = function(e) {
                    if (!e || !e.isRow) throw "DayPilot.Scheduler.rows.selection.add(): DayPilot.Row object expected";
                    DayPilot.list(x.selected).some(function(t) {
                        return t === e.$.row
                    }) || x.selected.push(e.$.row), x.Sj(), k.Tj()
                }, k.remove = function(e) {
                    if (!e || !e.isRow) throw "DayPilot.Scheduler.rows.selection.remove(): DayPilot.Row object expected";
                    k.isSelected(e) && (x.unselect(e.$.row), DayPilot.rfa(x.selected, e.$.row)), k.Tj()
                }, k.isSelected = function(e) {
                    if (!e || !e.isRow) throw "DayPilot.Scheduler.rows.selection.isSelected(): DayPilot.Row object expected";
                    return DayPilot.indexOf(x.selected, e.$.row) !== -1
                }, k.Tj = function() {
                    c.selectedRows = DayPilot.list(x.selected).map(function(e) {
                        return e.id
                    })
                }, this.rows.all = function() {
                    for (var e = [], t = 0; t < c.rowlist.length; t++) {
                        var i = c.Ph(c.rowlist[t]);
                        e.push(i)
                    }
                    return p(e)
                }, this.rows.visible = function() {
                    for (var e = [], t = 0; t < c.rowlist.length; t++) {
                        var i = c.rowlist[t];
                        if (!i.hidden) {
                            var i = c.Ph(c.rowlist[t]);
                            e.push(i)
                        }
                    }
                    return p(e)
                }, this.rows.each = function(e) {
                    c.rows.all().each(e)
                }, this.rows.filter = function(e) {
                    c.rows.ed = e, c.A && c.ld({
                        "immediateEvents": !0
                    })
                }, this.rows.find = function(e, t) {
                    var i = DayPilot.list(D.rowcache[e]),
                        n = null;
                    return n = "string" == typeof t ? i.find(function(e) {
                        return t === e.start.toString()
                    }) : i.first(), n ? new DayPilot.Row(n, c) : null
                }, this.rows.load = function(e, t, i) {
                    if (!e) throw new DayPilot.Exception("rows.load(): 'url' parameter required");
                    var n = function(e) {
                            var t = {};
                            t.exception = e.exception, t.request = e.request, "function" == typeof i && i(t)
                        },
                        a = function(e) {
                            var i, a = e.request;
                            try {
                                i = JSON.parse(a.responseText)
                            } catch (e) {
                                var o = {};
                                return o.exception = e, void n(o)
                            }
                            if (DayPilot.isArray(i)) {
                                var r = {};
                                if (r.preventDefault = function() {
                                        this.preventDefault.value = !0
                                    }, r.data = i, "function" == typeof t && t(r), r.preventDefault.value) return;
                                c.resources = i, c.A && c.update()
                            }
                        };
                    c.rowsLoadMethod && "POST" === c.rowsLoadMethod.toUpperCase() ? DayPilot.ajax({
                        "method": "POST",
                        "url": e,
                        "success": a,
                        "error": n
                    }) : DayPilot.ajax({
                        "method": "GET",
                        "url": e,
                        "success": a,
                        "error": n
                    })
                }, this.rows.expand = function(e) {
                    for (var t = [], i = e || 1, n = 0; n < c.rowlist.length; n++) {
                        var a = c.rowlist[n],
                            o = i === -1;
                        a.level < i && (o = !0), o && !a.expanded && a.children && a.children.length > 0 && t.push(a.index)
                    }
                    if (0 !== t.length)
                        if (1 === t.length) c.Zi(t[0]);
                        else {
                            for (var n = 0; n < t.length; n++) {
                                var r = t[n],
                                    l = c.rowlist[r].resource;
                                l.expanded = !0
                            }
                            c.ld()
                        }
                }, this.rows.expandAll = function() {
                    c.rows.expand(-1)
                }, this.rows.collapseAll = function() {
                    c.rowlist.each(function(e) {
                        e.resource.expanded = !1
                    }), c.ld()
                }, this.rows.headerHide = function() {
                    c.Uj = !0, c.Vj(), c.zj(), c.Rg()
                }, this.rows.headerShow = function() {
                    c.Uj = !1, c.Vj(), c.zj(), c.Rg()
                }, this.rows.headerToggle = function() {
                    c.Uj ? c.rows.headerShow() : c.rows.headerHide()
                }, this.rows.edit = function(e) {
                    x.edit(e.$.row)
                }, this.rows.remove = function(e) {
                    var t = e.$.row.resource,
                        i = P.findParentArray(t);
                    if (!i) throw "Cannot find source node parent";
                    var n = DayPilot.indexOf(i, t);
                    i.splice(n, 1), c.update()
                }, this.rows.update = function(e) {
                    if (!(e instanceof DayPilot.Row)) throw new DayPilot.Exception("DayPilot.Scheduler.rows.update() expects a DayPilot.Row object.");
                    var t = e.index,
                        i = c.rowlist[t],
                        n = e.data,
                        a = e.parent() ? e.parent().$.row : null,
                        o = c.Wj(n),
                        e = c.Xj(o, a);
                    e.hidden = i.hidden, e.level = i.level, e.children = i.children, e.index = t, e.top = i.top, e.height = i.height, c.rowlist[t] = e, e.resetEvents(), c.Xg(t), c.Yj(e), c.Ci(e), c.Bh(t), c.ig(), c.lg()
                }, this.Yj = function(e) {
                    for (var t = c.events.list, i = t.length, n = "function" == typeof c.onBeforeEventRender, a = 0; a < i; a++) {
                        var o = t[a];
                        if (o) {
                            if (o instanceof DayPilot.Event) throw new DayPilot.Exception("DayPilot.Scheduler: DayPilot.Event object detected in events.list array. Use raw event data instead.");
                            if ("Days" === c.viewType) throw new DayPilot.Exception(".rows.update() not supported for viewType = 'Days'.");
                            if ("Gantt" === c.viewType) throw new DayPilot.Exception(".rows.update() not supported for viewType = 'Gantt'.");
                            if ("*" === o.resource || "*" === e.id || o.resource === e.id) {
                                n && this.$c(a);
                                var r = this.yi(o, e);
                                r && n && (r.cache = this.t.events[a])
                            }
                        }
                    }
                }, this.Zj = function() {
                    var e = l.source,
                        t = l.target,
                        i = l.position,
                        n = l.sourceCalendar;
                    if (x.resetMoving(), c.va()) {
                        var a = {};
                        if (a.source = c.Ph(e), a.source.calendar = n, a.target = c.Ph(t), a.position = i, a.preventDefault = function() {
                                this.preventDefault.value = !0
                            }, "function" == typeof c.onRowMove && (c.onRowMove(a), a.preventDefault.value)) return;
                        switch (c.rowMoveHandling) {
                            case "Update":
                                x.move(a);
                                break;
                            case "CallBack":
                                c.rowMoveCallBack(a.source, a.target, a.position);
                                break;
                            case "PostBack":
                                c.rowMovePostBack(a.source, a.target, a.position);
                                break;
                            case "Notify":
                                x.move(a), c.rowMoveNotify(a.source, a.target, a.position)
                        }
                        "function" == typeof c.onRowMoved && c.onRowMoved(a)
                    } else {
                        var e = c.Ph(e),
                            t = c.Ph(t),
                            i = i;
                        switch (c.rowMoveHandling) {
                            case "CallBack":
                                c.rowMoveCallBack(e, t, i);
                                break;
                            case "PostBack":
                                c.rowMovePostBack(e, t, i);
                                break;
                            case "JavaScript":
                                c.onRowMove(e, t, i)
                        }
                    }
                }, this.qg = function(e) {
                    var t = DayPilot.list(e);
                    x.selected = [], t.isEmpty() || (c.Tf = !1, c.ca()), t.each(function(e) {
                        var t = c.yg(e);
                        t && x.selected.push(t)
                    }), x.Sj()
                }, this.Vh = {};
                var x = this.Vh;
                x.edit = function(e) {
                    x.$j(e)
                }, x.rowSelectLastSetDefault = function() {
                    x.Rj = {
                        "row": c.rowlist[0],
                        "index": 0
                    }
                }, x.rowSelectSetLast = function(e) {
                    var t = DayPilot.indexOf(c.rowlist, e);
                    if (t === -1) throw "DayPilot.Scheduler: Row not found when selecting a range";
                    x.Rj = {
                        "row": e,
                        "index": t
                    }
                }, x.fixRowSelectLast = function() {
                    x.Rj || x.rowSelectLastSetDefault();
                    var e = DayPilot.indexOf(c.rowlist, x.Rj.row);
                    if (!(e > -1)) {
                        var t = c.yg(x.Rj.row.id);
                        t ? (e = DayPilot.indexOf(c.rowlist, t), x.Rj = {
                            "row": t,
                            "index": e
                        }) : x.rowSelectLastSetDefault()
                    }
                }, x.createOverlay = function(e) {
                    var t = c.uj(),
                        i = c.q("_rowmove_source"),
                        n = DayPilot.Util.div(c.divHeader, 0, e.top, t, e.height);
                    n.className = i, e.moveOverlay = n
                }, x.deleteOverlay = function(e) {
                    DayPilot.de(e.moveOverlay), e.moveOverlay = null
                }, x.$j = function(e) {
                    var t = x._j(e),
                        i = t.cells[0];
                    if (i.input) return i.input;
                    DayPilot.Areas.disable(i.firstChild);
                    var n = i.clientWidth;
                    e.isNewRow && (n = c.Mg());
                    var o = document.createElement("textarea");
                    o.style.position = "absolute", o.style.top = "0px", o.style.left = "0px", o.style.width = n + "px", o.style.height = e.height + "px", o.style.border = "0px none", o.style.overflow = "hidden", o.style.boxSizing = "border-box", o.style.resize = "none", o.style.paddingLeft = e.level * c.treeIndent + "px";
                    var r = i.textDiv;
                    o.style.fontFamily = DayPilot.gs(r, "fontFamily") || DayPilot.gs(r, "font-family"), o.style.fontSize = DayPilot.gs(r, "fontSize") || DayPilot.gs(r, "font-size"), o.value = e.name || "", i.firstChild.appendChild(o), i.input = o;
                    var l = function() {
                        try {
                            i.input.parentNode.removeChild(i.input)
                        } catch (e) {
                            a()
                        }
                        i.input = null
                    };
                    return o.focus(), o.onblur = function() {
                        o.onblur = null;
                        var t = o.value;
                        l(), DayPilot.Areas.enable(i.firstChild), c.Rh(e, t, o.canceled)
                    }, o.onkeydown = function(e) {
                        var t = window.event ? event.keyCode : e.keyCode;
                        return 27 === t && (o.canceled = !0, l()), 13 !== t || (o.onblur(), !1)
                    }, o.setSelectionRange ? o.setSelectionRange(0, 9999) : o.select(), o
                }, x.selected = [], x.select = function(e, t, i, n) {
                    var a = DayPilot.indexOf(x.selected, e) !== -1;
                    if (t || n) {
                        if (a) return x.unselect(e), void DayPilot.rfa(x.selected, e)
                    } else a = !1, x.clearSelection();
                    x.ak(e), a || x.selected.push(e), k.Tj()
                }, x.selectRange = function(e, t) {
                    x.clearSelection();
                    for (var i = Math.min(e, t), n = Math.max(e, t), a = i; a <= n; a++) {
                        var o = c.rowlist[a];
                        x.ak(o), x.selected.push(o)
                    }
                    k.Tj()
                }, x.Sj = function() {
                    for (var e = 0; e < x.selected.length; e++) {
                        var t = x.selected[e];
                        x.ak(t)
                    }
                }, x.ak = function(e) {
                    var t = DayPilot.indexOf(c.rowlist, e);
                    if (t === -1) {
                        var i = c.yg(e.id);
                        if (!i) return;
                        t = i.index
                    }
                    for (var n = c.q("_cell_selected"), a = [], o = 0; o < c.itline.length; o++) {
                        var r = {};
                        r.x = o, r.y = t, a.push(r)
                    }
                    c.cells.findXy(a).addClass(n);
                    for (var n = c.q("_rowheader_selected"), l = c.divHeader, t = 0; t < l.rows.length; t++) {
                        var s = l.rows[t];
                        if (s && s.cells[0] && s.cells[0].row === e)
                            for (var d = 0; d < s.cells.length; d++) {
                                var r = s.cells[d],
                                    h = r.firstChild;
                                DayPilot.Util.addClass(h, n)
                            }
                    }
                }, x._h = function() {
                    var e = [];
                    if (!x.selected) return e;
                    for (var t = 0; t < x.selected.length; t++) {
                        var i = x.selected[t],
                            n = DayPilot.indexOf(c.rowlist, i),
                            a = c.Ph(i, n);
                        e.push(a.toJSON())
                    }
                    return e
                }, x.unselect = function(e) {
                    for (var t = c.q("_cell_selected"), i = DayPilot.indexOf(c.rowlist, e), n = [], a = 0; a < c.itline.length; a++) {
                        var o = {};
                        o.x = a, o.y = i, n.push(o)
                    }
                    c.cells.findXy(n).removeClass(t);
                    var t = c.q("_rowheader_selected"),
                        r = (c.divHeader, x._j(e));
                    if (r)
                        for (var l = 0; l < r.cells.length; l++) {
                            var o = r.cells[l],
                                s = o.firstChild;
                            DayPilot.Util.removeClass(s, t)
                        }
                }, x.Rj = null, x.clearSelection = function() {
                    for (var e = 0; e < x.selected.length; e++) {
                        var t = x.selected[e];
                        x.unselect(t)
                    }
                    x.selected = [], c.selectedRows = []
                }, x._j = function(e) {
                    for (var t = c.divHeader, i = 0; i < t.rows.length; i++) {
                        var n = t.rows[i];
                        if (n && n.cells[0] && n.cells[0].row === e) return n
                    }
                    return null
                }, x.selectById = function(e) {
                    var t = c.yg(e);
                    t && x.select(t)
                }, x.startMoving = function(e) {
                    var t = DayPilot.Global.rowmoving;
                    t.row = e, t.sourceCalendar = c, t.cursor = c.divResScroll.style.cursor, c.divResScroll.style.cursor = "move", x.createOverlay(e)
                }, x.resetMoving = function() {
                    c.divResScroll.style.cursor = l.cursor, DayPilot.de(l.div), x.deleteOverlay(l.row), DayPilot.Global.rowmoving = l = {}
                }, x.move = function(e) {
                    var t = e.source.$.row.resource,
                        i = e.target.$.row.resource,
                        n = e.position;
                    if ("forbidden" !== n) {
                        var a = e.source.calendar.bk.findParentArray(t);
                        if (!a) throw "Cannot find source node parent";
                        var o = DayPilot.indexOf(a, t);
                        a.splice(o, 1);
                        var r = P.findParentArray(i);
                        if (!r) throw "Cannot find target node parent";
                        var l = DayPilot.indexOf(r, i);
                        switch (n) {
                            case "before":
                                r.splice(l, 0, t);
                                break;
                            case "after":
                                r.splice(l + 1, 0, t);
                                break;
                            case "child":
                                i.children || (i.children = [], i.expanded = !0), i.children.push(t)
                        }
                        c.update()
                    }
                };
                var P = {};
                this.bk = P, P.findParentArray = function(e) {
                    return P.findInArray(c.resources, e)
                }, P.findInArray = function(e, t) {
                    if (DayPilot.indexOf(e, t) !== -1) return e;
                    for (var i = 0; i < e.length; i++) {
                        var n = e[i];
                        if (n.children && n.children.length > 0) {
                            var a = P.findInArray(n.children, t);
                            if (a) return a
                        }
                    }
                    return null
                }, this.cg = function() {
                    this.rowlist = DayPilot.list();
                    var e = this.resources,
                        t = this.r();
                    t || ("Gantt" === this.viewType ? e = this.ck() : "Days" === this.viewType && (e = this.dk())), !t || "Days" !== this.viewType || e && 0 !== e.length || (e = this.dk());
                    var i = {};
                    if (i.i = 0, null != e && !DayPilot.isArray(e)) throw "DayPilot.Scheduler.resources expects an array object";
                    this.ek(e, i, 0, null, this.treeEnabled, !1), "Disabled" !== c.rowCreateHandling && this.fk()
                }, this.fk = function() {
                    var e = {};
                    e.id = "NEW", e.isNewRow = !0, e.html = "", e.index = c.rowlist.length, e.loaded = !0, e.start = this.startDate, e.children = [], e.height = c.eventHeight, e.marginBottom = 0, e.marginTop = 0, e.getHeight = function() {
                        return c.rowCreateHeight ? c.rowCreateHeight : c.eventHeight + c.rowMarginBottom + c.rowMarginTop
                    }, e.putIntoLine = function() {}, e.resetEvents = function() {}, this.rowlist.push(e)
                }, this.ck = function() {
                    var e = [];
                    if (this.Vf && this.resources)
                        for (var t = 0; t < this.resources.length; t++) e.push(this.resources[t]);
                    if (this.events.list) {
                        for (var t = 0; t < this.events.list.length; t++) {
                            var i = this.events.list[t],
                                n = {};
                            n.id = i.id, n.name = i.text, e.push(n)
                        }
                        return e
                    }
                }, this.dk = function() {
                    for (var e = [], t = c.Da(), i = this.zc.locale(), n = 0; n < this.days; n++) {
                        var a = t.addDays(n),
                            o = {};
                        o.name = a.toString(i.datePattern, i), o.start = a, e.push(o)
                    }
                    return e
                }, this.Da = function() {
                    return this.itline && this.itline.length > 0 ? this.itline[0].start : this.startDate
                }, this.$g = function() {
                    if (this.itline && this.itline.length > 0) {
                        var e = c.itline[this.itline.length - 1].end;
                        return "Days" !== c.viewType ? e : e.addDays(c.days - 1)
                    }
                    return c.startDate.addDays(c.days)
                }, this.gk = {};
                this.gk;
                this.visibleStart = function() {
                    return this.Da()
                }, this.visibleEnd = function() {
                    return this.$g()
                }, this.Xj = function(e, t) {
                    var i = {};
                    if (i.backColor = e.backColor, i.cssClass = e.cssClass, i.expanded = e.expanded, i.name = e.name, i.html = e.html ? e.html : i.name, i.eventHeight = "undefined" != typeof e.eventHeight ? e.eventHeight : c.zc.eventHeight(), i.minHeight = "undefined" != typeof e.minHeight ? e.minHeight : c.rowMinHeight, i.marginBottom = "undefined" != typeof e.marginBottom ? e.marginBottom : c.rowMarginBottom, i.marginTop = "undefined" != typeof e.marginTop ? e.marginTop : c.rowMarginTop, i.eventStackingLineHeight = "undefined" != typeof e.eventStackingLineHeight ? e.eventStackingLineHeight : c.eventStackingLineHeight, i.loaded = !e.dynamicChildren, i.id = e.id || e.value, i.toolTip = e.toolTip, i.children = [], i.columns = [], i.start = e.start ? new DayPilot.Date(e.start) : this.Da(), i.isParent = e.isParent, i.contextMenu = e.contextMenu ? DayPilot.Util.evalVariable(e.contextMenu) : this.contextMenuResource, i.areas = e.areas, i.moveDisabled = e.moveDisabled, i.bubbleHtml = e.bubbleHtml, i.tags = e.tags, i.task = e.task, i.swimlane = e.swimlane, i.hideEvents = e.hideEvents, i.height = i.eventHeight, i.resource = e.hk, i.lines = [], i.blocks = [], i.isRow = !0, i.getHeight = function() {
                            var e = 0;
                            if (c.groupConcurrentEvents)
                                for (var t = 0; t < this.blocks.length; t++) {
                                    var i = this.blocks[t];
                                    e = Math.max(e, i.getHeight())
                                } else if (this.lines.length > 0) {
                                    var n = this.lines.length - 1,
                                        a = this.lines[n],
                                        o = a.height || this.eventHeight,
                                        r = a.top || 0;
                                    e = r + o
                                } return 0 === e && (e = this.eventHeight), e > this.minHeight ? e : this.minHeight
                        }, i.resetEvents = function() {
                            var e = this;
                            e.events = DayPilot.list(), e.events.forRange = function(t, i) {
                                t = new DayPilot.Date(t), i = new DayPilot.Date(i);
                                for (var n = DayPilot.list(), a = 0; a < e.events.length; a++) {
                                    var o = e.events[a];
                                    DayPilot.Util.overlaps(o.start(), o.end(), t, i) && n.push(o)
                                }
                                return n
                            }
                        }, i.calculateUtilization = function() {
                            function e() {
                                for (var e = [], t = 0; t < i.events.length; t++) {
                                    var n = i.events[t];
                                    DayPilot.contains(e, n.start().toString()) || e.push(n.start().toString()), DayPilot.contains(e, n.rawend().toString()) || e.push(n.rawend().toString())
                                }
                                return e.sort(), e
                            }

                            function t() {
                                for (var t = e(), n = [], a = {
                                        "start": i.data.start
                                    }, o = 0; o < t.length; o++) a.end = new DayPilot.Date(t[o]), n.push(a), a = {
                                    "start": new DayPilot.Date(t[o])
                                };
                                return a.end = i.data.end, n.push(a), n.forRange = function(e, t) {
                                    for (var i = DayPilot.list(), n = 0; n < this.length; n++) {
                                        var a = this[n];
                                        DayPilot.Util.overlaps(e, t, a.start, a.end) && i.push(a)
                                    }
                                    return i.maxSum = function(e) {
                                        for (var t = 0, i = 0; i < this.length; i++) {
                                            var n = this[i],
                                                a = n.sum(e);
                                            a > t && (t = a)
                                        }
                                        return t
                                    }, i
                                }, n
                            }
                            for (var i = this, n = i.sections = t(), a = 0; a < n.length; a++) {
                                var o = n[a];
                                o.events = DayPilot.list();
                                for (var r = 0; r < i.events.length; r++) {
                                    var l = i.events[r];
                                    DayPilot.Util.overlaps(o.start, o.end, l.start(), l.rawend()) && o.events.push(l)
                                }
                                o.sum = function(e) {
                                    for (var t = 0, i = 0; i < this.events.length; i++) {
                                        var n = this.events[i],
                                            a = 0;
                                        "undefined" == typeof e ? a = 1 : n.tag(e) ? a = n.tag(e) : n.data[e] && (a = n.data[e]), "number" == typeof a && (t += a)
                                    }
                                    return t
                                }
                            }
                        }, i.putIntoLine = function(e) {
                            for (var t = 0; t < this.lines.length; t++) {
                                var i = this.lines[t];
                                if (i.isFree(e.part.left, e.part.width)) return i.add(e), t
                            }
                            var i = [];
                            return i.height = 0, i.add = function(e) {
                                this.push(e);
                                var t = e.part.Pj();
                                t > i.height && (i.height = t)
                            }, i.isFree = function(e, t, i) {
                                for (var n = e + t - 1, a = this.length, o = 0; o < a; o++) {
                                    var r = this[o];
                                    if (!(n < r.part.left || e > r.part.left + r.part.width - 1)) {
                                        if (DayPilot.contains(i, r.data)) continue;
                                        return !1
                                    }
                                }
                                return !0
                            }, i.add(e), this.lines.push(i), this.lines.length - 1
                        }, i.putIntoBlock = function(e) {
                            for (var t = 0; t < this.blocks.length; t++) {
                                var i = this.blocks[t];
                                if (DayPilot.indexOf(i.events, e) !== -1) return;
                                if (i.overlapsWith(e.part.left, e.part.width)) return i.events.push(e), e.part.block = i, i.min = Math.min(i.min, e.part.left), i.max = Math.max(i.max, e.part.left + e.part.width), t
                            }
                            var i = {};
                            i.expanded = !1, i.row = this, i.events = DayPilot.list(), i.lines = DayPilot.list(), i.putIntoLine = function(e) {
                                for (var t = 0; t < this.lines.length; t++) {
                                    var i = this.lines[t];
                                    if (i.isFree(e.part.left, e.part.width)) return i.add(e), t
                                }
                                var i = [];
                                i.height = 0, i.add = function(e) {
                                    this.push(e), e.part.height > i.height && (i.height = e.part.height)
                                }, i.isFree = function(e, t) {
                                    for (var i = e + t - 1, n = this.length, a = 0; a < n; a++) {
                                        var o = this[a];
                                        if (!(i < o.part.left || e > o.part.left + o.part.width - 1)) return !1
                                    }
                                    return !0
                                }, i.add(e), this.lines.push(i)
                            }, i.overlapsWith = function(e, t) {
                                return !(e + t - 1 < this.min || e > this.max - 1)
                            }, i.getHeight = function() {
                                if (!this.expanded) return c.eventHeight;
                                if (this.lines.length > 0) {
                                    var e = this.lines.length - 1,
                                        t = this.lines[e],
                                        i = t.height || c.eventHeight;
                                    return (t.top || 0) + i
                                }
                                return c.eventHeight
                            }, i.clear = function() {
                                i.events = DayPilot.list(), i.min = null, i.max = null
                            }, i.events.push(e), e.part.block = i, i.min = e.part.left, i.max = e.part.left + e.part.width, this.blocks.push(i)
                        }, i.ik = !1, i.jk = t, i.kk = function() {
                            this.hidden = !this.lk(), this.ik = !1, this.jk && this.jk.kk()
                        }, i.lk = function() {
                            return !this.jk || this.jk.lk() && this.jk.expanded
                        }, e.columns)
                        for (var n = 0; n < e.columns.length; n++) i.columns.push(e.columns[n]);
                    return i
                }, this.xh = function() {
                    c.rowlist.each(function(e) {
                        if ("function" == typeof c.onRowFilter && c.rows.ed) {
                            var t = e.jk,
                                i = {};
                            i.visible = !0, i.row = c.Ph(e), i.filter = c.rows.ed, c.onRowFilter(i), e.ik = !i.visible, i.visible ? t && t.kk() : e.hidden = !0
                        }
                    })
                }, this.ek = function(e, t, i, n, a, o) {
                    if (e)
                        for (var r = 0; r < e.length; r++)
                            if (e[r]) {
                                var l = {};
                                if (l.level = i, l.hidden = o, l.index = t.i, !e[r].hidden) {
                                    var s = this.Wj(e[r], l),
                                        d = c.Xj(s, n);
                                    if (o && (d.hidden = !0), d.level = i, d.index = t.i, null !== n && n.children.push(t.i), this.rowlist.push(d), t.i++, a && s.children && s.children.length) {
                                        var h = o || !d.expanded;
                                        this.ek(s.children, t, i + 1, d, !0, h)
                                    }
                                }
                            }
                }, this.ij = function(e) {
                    if (e.isNewRow) return {
                        "row": {
                            "cssClass": c.q("_row_new"),
                            "moveDisabled": !0,
                            "html": c.rowCreateHtml || ""
                        }
                    };
                    var t = {};
                    if (t.row = this.Ph(e), DayPilot.Util.copyProps(e, t.row, ["html", "backColor", "cssClass", "toolTip", "contextMenu", "moveDisabled"]), t.row.columns = DayPilot.Util.createArrayCopy(e.columns, ["html", "backColor", "cssClass", "areas"]), t.row.areas = DayPilot.Util.createArrayCopy(e.areas), "undefined" == typeof t.row.columns && c.rowHeaderColumns && c.rowHeaderColumns.length > 0) {
                        r.columns = [];
                        for (var i = 0; i < c.rowHeaderColumns.length; i++) r.columns.push({})
                    }
                    return "function" == typeof this.onBeforeRowHeaderRender && this.onBeforeRowHeaderRender(t), t
                }, this.Wj = function(e, t) {
                    var i = this.mk(e, t);
                    if ("function" == typeof this.onBeforeResHeaderRender) {
                        var n = {};
                        n.resource = i, this.onBeforeResHeaderRender(n)
                    }
                    return i
                }, this.mk = function(e, t) {
                    var i = {
                        get data() {
                            return this.hk
                        }
                    };
                    for (var n in t) i[n] = t[n];
                    for (var n in e) i[n] = e[n];
                    if ("undefined" == typeof e.html && (i.html = e.name), "undefined" == typeof i.columns && c.rowHeaderColumns && c.rowHeaderColumns.length > 0) {
                        i.columns = [];
                        for (var a = 0; a < c.rowHeaderColumns.length; a++) i.columns.push({})
                    }
                    return i.hk = e, i
                }, this.nk = function() {
                    this.vd(), this.nav.top.dp = this, this.nav.top.innerHTML = "", DayPilot.Util.addClass(this.nav.top, this.q("_main")), this.nav.top.setAttribute("role", "region"), this.nav.top.setAttribute("aria-label", "scheduler"), DayPilot.browser.ie9 && DayPilot.Util.addClass(this.nav.top, this.q("_browser_ie9")), DayPilot.browser.ie8 && DayPilot.Util.addClass(this.nav.top, this.q("_browser_ie8")), this.nav.top.style.MozUserSelect = "none", this.nav.top.style.KhtmlUserSelect = "none", this.nav.top.style.webkitUserSelect = "none", this.nav.top.style.WebkitTapHighlightColor = "rgba(0,0,0,0)", this.nav.top.style.WebkitTouchCallout = "none", this.width && (this.nav.top.style.width = this.width), "Parent100Pct" === this.heightSpec && (this.nav.top.style.height = "100%"), this.nav.top.style.lineHeight = "1.2", this.nav.top.style.position = "relative", this.visible || (this.nav.top.style.display = "none"), this.nav.top.onmousemove = this.Nf, this.nav.top.ontouchstart = T.onMainTouchStart, this.nav.top.ontouchmove = T.onMainTouchMove, this.nav.top.ontouchend = T.onMainTouchEnd, this.hideUntilInit && this.backendUrl && (this.nav.top.style.visibility = "hidden");
                    var e = this.Mg();
                    if ("DivBased" === this.zc.layout()) {
                        var t = document.createElement("div");
                        t.style.position = "absolute", t.style.left = "0px", t.style.width = e + "px", t.appendChild(this.Bc());
                        var i = document.createElement("div");
                        i.style.height = "1px", i.className = this.q("_divider_horizontal"), t.appendChild(i), this.nav.dh1 = i, t.appendChild(this.ok()), this.nav.left = t;
                        var n = document.createElement("div");
                        n.style.position = "absolute", n.style.left = e + "px", n.style.width = _.splitterWidth() + "px", n.style.height = this.Lg() + this.cc() + "px", n.className = this.q("_divider") + " " + this.q("_splitter"), n.setAttribute("unselectable", "on"), this.nav.divider = n, this.rowHeaderScrolling && this.pk();
                        var a = document.createElement("div");
                        a.style.marginLeft = e + _.splitterWidth() + "px", a.style.marginRight = "1px", a.style.position = "relative", a.appendChild(this.qk()), this.nav.right = a;
                        var o = document.createElement("div");
                        o.style.height = "1px", o.style.position = "absolute", o.style.top = this.Lg() + "px", o.style.width = "100%", o.className = this.q("_divider_horizontal"), a.appendChild(o), this.nav.dh2 = o, a.appendChild(this.rk());
                        var r = document.createElement("div");
                        r.style.clear = "left", this.nav.top.appendChild(t), this.nav.top.appendChild(n), this.nav.top.appendChild(a), this.nav.top.appendChild(r)
                    } else {
                        var l = document.createElement("table");
                        l.cellPadding = 0, l.cellSpacing = 0, l.border = 0, l.style.position = "absolute";
                        var s = l.insertRow(-1);
                        s.insertCell(-1).appendChild(this.Bc()), s.insertCell(-1).appendChild(this.qk());
                        var d = l.insertRow(-1);
                        d.insertCell(-1).appendChild(this.ok()), d.insertCell(-1).appendChild(this.rk()), this.nav.top.appendChild(l)
                    }
                    if (this.Ge = document.createElement("div"), this.Ge.style.display = "none", this.nav.top.appendChild(this.Ge), this.M()) {
                        var c = document.createElement("input");
                        c.type = "hidden", c.id = this.id + "_state", c.name = this.id + "_state", this.nav.state = c, this.nav.top.appendChild(c)
                    }
                    var h = document.createElement("div");
                    h.style.position = "absolute", h.style.left = this.Mg() + _.splitterWidth() + 5 + "px", h.style.top = this.Lg() + 5 + "px", h.style.display = "none", h.innerHTML = this.loadingLabelText, DayPilot.Util.addClass(h, this.q("_loading")), this.nav.loading = h, this.nav.top.appendChild(h), this.sk()
                }, this.Nf = function(e) {
                    if (l.row) {
                        var t = DayPilot.mo3(c.divHeader, e),
                            i = c.gi(t.y),
                            a = c.rowlist[i.i];
                        if (!a) return;
                        if (a.isNewRow) return;
                        var o = t.y - i.top,
                            r = i.bottom - i.top,
                            s = r / 3,
                            d = 2 * s,
                            h = r / 2,
                            u = "before",
                            f = a.children && a.children.length > 0,
                            v = function() {
                                for (var e = i.i, t = a.level; e >= 0;) {
                                    var n = c.rowlist[e];
                                    if (e--, !(t <= n.level)) {
                                        if (n === l.row) return !0;
                                        if (0 === n.level) return !1;
                                        t = n.level
                                    }
                                }
                                return !1
                            }(),
                            p = c.treeEnabled;
                        u = v || i.i === l.row.index ? "forbidden" : p ? f ? o < h ? "before" : "child" : o < s ? "before" : o < d ? "child" : "after" : f ? "before" : o < h ? "before" : "after", l.row.moveDisabled && (u = "forbidden"), l.calendar = c, l.source = l.row, l.target = c.rowlist[i.i], l.position = u;
                        if (function() {
                                return !l.last || (l.last.target !== l.target || l.last.position !== l.position)
                            }()) {
                            if ("function" == typeof c.onRowMoving) {
                                var g = {};
                                g.source = c.Ph(l.source), g.target = c.Ph(l.target), g.position = u, c.onRowMoving(g), l.position = g.position
                            }
                        } else l.last && (l.position = l.last.position);
                        l.last = {}, l.last.target = l.target, l.last.position = l.position,
                            function() {
                                l.div && DayPilot.de(l.div);
                                var e = i.top,
                                    t = l.position,
                                    n = c.rowlist[i.i],
                                    a = n.level,
                                    o = a * c.treeIndent;
                                switch (t) {
                                    case "before":
                                        e = i.top;
                                        break;
                                    case "child":
                                        e = i.top + h;
                                        break;
                                    case "after":
                                        e = i.bottom;
                                        break;
                                    case "forbidden":
                                        e = i.top + h
                                }
                                var r = c.uj() - o,
                                    s = document.createElement("div");
                                s.style.position = "absolute", s.style.left = o + "px", s.style.width = r + "px", s.style.top = e + "px", s.className = c.q("_rowmove_position_" + t), l.div = s, c.divResScroll.appendChild(s)
                            }()
                    } else if (n.splitting) {
                        var m = DayPilot.mo3(c.nav.top, e).x,
                            y = c.uj(),
                            b = Math.min(y, m - 1);
                        c.rowHeaderWidth = b, c.Uj = !1, c.Vj()
                    }
                }, this.ea = function() {
                    var e = this.Lg();
                    this.nav.corner.style.height = e + "px", this.divTimeScroll.style.height = e + "px", this.divNorth.style.height = e + "px", this.nav.dh1 && this.nav.dh2 && (this.nav.dh1.style.top = e + "px", this.nav.dh2.style.top = e + "px"), this.nav.loading.style.top = e + 5 + "px", this.nav.scroll.style.top = e + 1 + "px"
                }, this.Mg = function() {
                    if (this.Uj) return 0;
                    var e = 0;
                    return e = this.rowHeaderScrolling ? this.rowHeaderWidth : this.uj()
                }, this.pk = function() {
                    var e = this.nav.divider;
                    e.style.cursor = "col-resize", e.setAttribute("unselectable", "on"), e.onmousedown = function(e) {
                        var t = n.splitting = {};
                        return t.cursor = c.nav.top.style.cursor, t.cleanup = function() {
                            if (c.nav.top.style.cursor = t.cursor, "function" == typeof c.onRowHeaderResized) {
                                var e = {};
                                c.onRowHeaderResized(e)
                            }
                        }, c.nav.top.style.cursor = "col-resize", !1
                    }
                }, this.Vj = function() {
                    var e = _.splitterWidth(),
                        t = this.Mg();
                    if (this.nav.corner.style.width = t + "px", this.divCorner.style.width = t + "px", this.divResScroll.style.width = t + "px", "DivBased" === _.layout() && (this.nav.left.style.width = t + "px", this.nav.divider.style.left = t - e + "px", this.nav.right.style.marginLeft = t + "px"), this.nav.message && (this.nav.message.style.left = t + e + "px"), this.nav.loading && (this.nav.loading.style.left = t + e + 5 + "px"), this.nav.hideIcon) {
                        var i = this.nav.hideIcon,
                            n = c.q("_header_icon_show"),
                            a = c.q("_header_icon_hide");
                        i.style.left = t - 1 + "px", c.Uj ? (DayPilot.Util.removeClass(i, a), DayPilot.Util.addClass(i, n)) : (DayPilot.Util.removeClass(i, n), DayPilot.Util.addClass(i, a))
                    }
                }, this.tk = function() {
                    this.vj();
                    var e = this.uj(),
                        t = function(e, t, i) {
                            if (e && e.style) {
                                var n = e.firstChild;
                                c.Aj ? (e.style.width = t + "px", n.style.width = t + "px", "number" == typeof i && (e.style.left = i + "px")) : n.style.width = t + "px"
                            }
                        },
                        i = this.divHeader;
                    i.style.width = e + "px";
                    for (var n = c.wj(), a = n.start; a < n.end; a++) {
                        var o = i.rows[a];
                        if (o) {
                            var r = o.cells[0];
                            if (r.colSpan > 1) {
                                var r = o.cells[0];
                                t(r, e)
                            } else if (this.rowHeaderCols)
                                for (var l = 0, s = 0; s < o.cells.length; s++) {
                                    var d = this.rowHeaderCols[s],
                                        r = o.cells[s];
                                    t(r, d, l), l += d
                                } else {
                                    var d = this.rowHeaderWidth,
                                        r = o.cells[0];
                                    t(r, d)
                                }
                        }
                    }
                    c.nav.resScrollSpace && (c.nav.resScrollSpace.style.width = e + "px"), this.V()
                }, this.rh = function() {
                    this.Vj(), this.tk()
                }, this.uk = function() {
                    var e = c.nav.corner,
                        t = this.rowHeaderColumns,
                        i = document.createElement("div");
                    i.style.position = "absolute", i.style.bottom = "0px", i.style.left = "0px", i.style.width = "100%", i.style.height = _.headerHeight() + "px", i.style.overflow = "hidden", c.nav.columnScroll = i;
                    var n = document.createElement("div");
                    n.style.position = "absolute", n.style.bottom = "0px", n.style.left = "0px", n.style.width = "5000px", n.style.height = _.headerHeight() + "px", n.style.overflow = "hidden", n.className = this.q("_columnheader"), i.appendChild(n);
                    var a = document.createElement("div");
                    a.style.position = "absolute", a.style.top = "0px", a.style.bottom = "0px", a.style.left = "0px", a.style.right = "0px", a.className = this.q("_columnheader_inner"), n.appendChild(a), c.ki && (c.ki.dispose(), c.ki = null);
                    var o = new DayPilot.Splitter(a);
                    o.widths = DayPilot.Util.propArray(t, "width", c.rowHeaderColumnDefaultWidth), o.height = _.headerHeight(), o.css.title = this.q("_columnheader_cell"), o.css.titleInner = this.q("_columnheader_cell_inner"), o.css.splitter = this.q("_columnheader_splitter"), o.titles = DayPilot.Util.propArray(t, "title"), o.updating = function(e) {
                        DayPilot.Util.updatePropsFromArray(c.rowHeaderColumns, "width", this.widths), c.rh(), "Auto" === c.cellWidthSpec
                    }, o.updated = function(e) {
                        if (c.zj(), c.va()) {
                            if ("function" == typeof c.onRowHeaderColumnResized) {
                                var t = {};
                                t.column = c.rowHeaderColumns[e.index], c.onRowHeaderColumnResized(t)
                            }
                        } else switch (c.rowHeaderColumnResizedHandling) {
                            case "CallBack":
                                break;
                            case "PostBack":
                                break;
                            case "JavaScript":
                                if ("function" == typeof c.onRowHeaderColumnResized) {
                                    var t = {};
                                    t.column = c.rowHeaderColumns[e.index], c.onRowHeaderColumnResized(t)
                                }
                        }
                    }, o.color = "#000000", o.opacity = 30, o.init(), e.appendChild(i), this.ki = o
                }, this.yh = function() {
                    var e = this.nav.corner;
                    e.innerHTML = "", e.className = this.q("_corner");
                    var t = document.createElement("div");
                    if (t.style.position = "absolute", t.style.top = "0px", t.style.left = "0px", t.style.right = "0px", t.style.bottom = "0px", t.className = this.q("_corner_inner"), this.divCorner = t, t.innerHTML = "&nbsp;", this.rowHeaderColumns && this.rowHeaderColumns.length > 0) {
                        var i = document.createElement("div");
                        i.style.position = "absolute", i.style.top = "0px", i.style.left = "0px", i.style.right = "0px", i.style.bottom = _.headerHeight() + 1 + "px", e.appendChild(i);
                        var n = document.createElement("div");
                        n.style.position = "absolute", n.style.left = "0px", n.style.right = "0px", n.style.height = "1px", n.style.bottom = _.headerHeight() + "px", n.className = this.q("_divider"), e.appendChild(n), i.appendChild(t), this.uk()
                    } else e.appendChild(t);
                    var a = document.createElement("div");
                    a.style.position = "absolute", a.style.padding = "2px", a.style.top = "0px", a.style.left = "1px", a.style.backgroundColor = "#FF6600", a.style.color = "white", a.innerHTML = "\u0044\u0045" + "\u004D\u004F", DayPilot.Util.isNullOrUndefined(undefined) && e.appendChild(a)
                }, this.sk = function() {
                    var e = 3,
                        t = this.Mg() + _.splitterWidth() - 1,
                        i = 10,
                        n = 20,
                        a = this.Lg() + e,
                        o = DayPilot.Util.div(this.nav.top, t, a, i, n);
                    o.style.cursor = "pointer", o.className = c.q("_header_icon"), DayPilot.Util.addClass(o, c.q("_header_icon_hide")), o.onclick = function() {
                        c.rows.headerToggle()
                    }, this.nav.hideIcon = o
                }, this.zh = function() {
                    c.nav.hideIcon && (c.rowlist.length > 0 && c.rowHeaderHideIconEnabled ? c.nav.hideIcon.style.display = "" : c.nav.hideIcon.style.display = "none")
                }, this.Bc = function() {
                    var e = this.Mg(),
                        t = document.createElement("div");
                    return c.nav.corner = t, t.style.width = e + "px", t.style.height = this.Lg() + "px", t.style.overflow = "hidden", t.style.position = "relative", t.setAttribute("unselectable", "on"), t.onmousemove = function() {
                        c.U()
                    }, t.oncontextmenu = function() {
                        return !1
                    }, this.yh(), t
                }, this.Lg = function() {
                    if (c.timeHeaders) {
                        var e = R.timeHeader(c.timeHeaders.length - 1);
                        return e.top + e.height
                    }
                    return this.timeHeader ? c.timeHeader.length * _.headerHeight() : 0
                }, this.vk = null, this.wk = function() {
                    return c.scrollStep || c.eventHeight
                }, this.ok = function() {
                    var e = document.createElement("div");
                    e.style.width = this.Mg() + "px", e.style.height = this.cc() + "px", e.style.overflow = "hidden", e.style.position = "relative", e.className = c.q("_rowheader_scroll");
                    var t = c.wk(),
                        i = navigator.userAgent.indexOf("Mobile") !== -1;
                    return i && (e.style.overflowY = "auto"), e.onmousemove = function() {
                        c.U()
                    }, e.onscroll = function() {
                        if (c.nav.columnScroll && c.rowHeaderScrolling && (c.nav.columnScroll.scrollLeft = e.scrollLeft), c.vk && clearTimeout(c.vk), i) {
                            var t = c.aj() - c.nav.scroll.offsetHeight;
                            e.scrollTop = Math.min(e.scrollTop, t), c.nav.scroll.scrollTop = e.scrollTop
                        } else c.vk = setTimeout(function() {
                            c.nav.scroll.scrollTop = e.scrollTop
                        }, 500)
                    }, e.addEventListener("wheel", function(i) {
                        var n;
                        n = c.overrideWheelScrolling ? i.deltaY > 0 ? t : -t : i.deltaY, c.nav.scroll.scrollTop = e.scrollTop + n, i.preventDefault && i.preventDefault()
                    }, !1), e.oncontextmenu = function() {
                        return !1
                    }, e.onmouseenter = function() {
                        c.rowHeaderScrolling && (e.style.overflowX = "auto")
                    }, e.onmouseleave = function() {
                        c.rowHeaderScrolling && (e.style.overflowX = "hidden")
                    }, e.setAttribute("role", "region"), e.setAttribute("aria-label", "scheduler rows"), this.divResScroll = e, this.Qg = e, e
                }, this.xk = function(e) {
                    if ("TableBased" === _.layout()) {
                        var t = parseInt(this.width, 10),
                            i = isNaN(t) || this.width.indexOf("%") !== -1,
                            n = (/MSIE/i.test(navigator.userAgent), this.uj());
                        i ? this.nav.top && this.nav.top.offsetWidth > 0 && (e.style.width = this.nav.top.offsetWidth - 6 - n + "px") : e.style.width = t - n + "px"
                    }
                }, this.ve = function(e) {
                    c.gg(), c.rg()
                }, this.gg = function() {
                    "TableBased" === c.zc.layout() && (c.xk(c.nav.scroll), c.xk(c.divTimeScroll)), c.la(), c.zj(), c.yk(), c.t.drawArea = null
                }, this.zk = null, this.ji = null, this.Ak = function() {
                    c.watchWidthChanges && (c.ji || (this.ji = setInterval(function() {
                        return c.nav && c.nav.top ? (c.zk || (c.zk = {}, c.zk.counter = 0, c.zk.changed = !1, c.zk.width = c.nav.top.offsetWidth), c.zk.width !== c.nav.top.offsetWidth && (c.zk.changed = !0, c.zk.counter = 0, c.zk.width = c.nav.top.offsetWidth), c.zk.changed && (c.zk.counter += 1), void(c.zk.changed && c.zk.counter > 2 && (c.zk.changed = !1, c.gg(), c.rg()))) : void clearInterval(c.ji)
                    }, 20)))
                }, this.yk = function() {
                    var e = c.rangeHold;
                    c.clearSelection(), c.rangeHold = e, c.Lh(e, !0)
                }, this.zj = function() {
                    c.A && "Auto" === c.cellWidthSpec && (c.ag(), c.bg(), c.eg(), c.ig(), c.lg(), c.jg(), c.mg(), c.O(), c.da(), c.qa(), c.la())
                }, this.ag = function() {
                    if ("Auto" === this.cellWidthSpec) {
                        var e = this.nav.top.clientWidth,
                            t = this.Mg(),
                            i = c.Bk(),
                            n = e - t - i;
                        if (this.cj()) {
                            var a = n / this.cj();
                            this.cellWidth = Math.max(a, c.cellWidthMin), c.Pg = a < c.cellWidthMin
                        }
                    }
                }, this.Bk = function() {
                    if ("Auto" === c.heightSpec) return 0;
                    if ("Max" === c.heightSpec || "Fixed" === c.heightSpec || "Parent100Pct" === c.heightSpec) {
                        return c.aj() > c.height ? DayPilot.sw(c.nav.scroll) : 0
                    }
                    return DayPilot.sw(c.nav.scroll)
                }, this.Tg = function() {
                    var e = this.nav.top.clientWidth,
                        t = this.Mg(),
                        i = this.cc(),
                        n = this.aj(),
                        a = "Auto" === c.heightSpec,
                        o = 0;
                    return n > i && !a && (o = DayPilot.swa()), e - t - 2 - o
                }, this.qk = function() {
                    var e = document.createElement("div");
                    e.style.overflow = "hidden", e.style.position = "absolute", e.style.display = "block", e.style.top = "0px", e.style.width = "100%", e.style.height = this.Lg() + "px", e.style.overflow = "hidden", e.onmousemove = function() {
                        c.U(), c.cellBubble && c.cellBubble.delayedHide()
                    }, this.xk(e), this.divTimeScroll = e;
                    var t = document.createElement("div");
                    return t.style.width = this.cj() * this.cellWidth + 5e3 + "px", this.divNorth = t, e.appendChild(t), e
                }, this.cc = function() {
                    var e = 0,
                        t = c.heightSpec;
                    return "Fixed" === t || "Parent100Pct" === t ? this.height ? this.height : 0 : (e = c.aj(), ("Max" === t || "Max100Pct" === t) && e > c.height ? c.height : e)
                }, this.Ck = null, this.Dk = 0, this.Ek = 0, this.Fk = 0, this.Rg = function() {
                    if ("function" == typeof c.onHeightChanged || "function" == typeof c.onDimensionsChanged) {
                        c.Ck && clearTimeout(c.Ck);
                        var e = function() {
                            var e = c.nav.top.offsetHeight,
                                t = c.Dk;
                            c.Dk = e;
                            var i = c.nav.top.offsetWidth,
                                n = c.Ek;
                            c.Ek = i;
                            var a = c.Mg(),
                                o = c.Fk;
                            if (c.Fk = a, "function" == typeof c.onHeightChanged && e !== t) {
                                var r = {};
                                r.oldHeight = t, r.newHeight = e, c.onHeightChanged(r)
                            }
                            if ("function" == typeof c.onDimensionsChanged && (e !== t || i !== n || a !== o)) {
                                var r = {};
                                r.oldHeight = t, r.newHeight = e, r.oldWidth = n, r.newWidth = i, r.oldRowHeaderWidth = o, r.newRowHeaderWidth = a, c.onDimensionsChanged(r)
                            }
                        };
                        c.Ck = setTimeout(e, 100)
                    }
                }, this.aj = function() {
                    var e;
                    return this.Zf !== -1 ? (e = this.Zf, this.Zf > 0 && "auto" === c.nav.scroll.style.overflowX && (e += DayPilot.sh(c.nav.scroll))) : e = this.rowlist.length * this.zc.eventHeight(), e
                }, this.U = function() {
                    this.V(), this.Gk(), this.Hk(), this.sj(), this._i(), c.cellBubble && c.cellBubble.hideOnMouseOut(), c.bubble && c.bubble.hideOnMouseOut()
                }, this.rk = function() {
                    var e = document.createElement("div");
                    if (e.style.overflow = "auto", e.style.overflowX = "auto", e.style.overflowY = "auto", e.style.position = "absolute", e.style.height = this.cc() + "px", e.style.top = this.Lg() + 1 + "px", e.style.width = "100%", e.className = this.q("_scrollable"), e.oncontextmenu = function() {
                            return !1
                        }, this.xk(e), c.overrideWheelScrolling) {
                        var t = c.scrollStep;
                        e.onwheel = function(i) {
                            var n = i.deltaY > 0 ? t : -t;
                            c.nav.scroll.scrollTop = e.scrollTop + n, i.preventDefault && i.preventDefault()
                        }, e.onmousewheel = function(i) {
                            i = i || window.event;
                            var n = i.wheelDelta < 0 ? t : -t;
                            c.nav.scroll.scrollTop = e.scrollTop + n, i.preventDefault && i.preventDefault(), i.returnValue = !1
                        }
                    }
                    this.nav.scroll = e, this.vg = document.createElement("div"), this.vg.style.MozUserSelect = "none", this.vg.style.KhtmlUserSelect = "none", this.vg.style.webkitUserSelect = "none", this.vg.daypilotMainD = !0, this.vg.calendar = this, i && (this.vg.style.webkitTransform = "translateZ(0px)"), this.vg.style.position = "absolute";
                    var n = this.Jg();
                    return n > 0 && !isNaN(n) && (this.vg.style.width = n + "px"), this.vg.setAttribute("unselectable", "on"), this.vg.onmousedown = this.Ik, this.vg.onmousemove = this.tj, this.vg.onmouseup = this.Jk, this.vg.oncontextmenu = this.Kk, this.vg.ondblclick = this.Lk, this.vg.className = this.q("_matrix"), this.divStretch = document.createElement("div"), this.divStretch.style.position = "absolute", this.divStretch.style.height = "1px", this.vg.appendChild(this.divStretch), this.divCells = document.createElement("div"), this.divCells.style.position = "absolute", this.divCells.oncontextmenu = this.Kk, this.vg.appendChild(this.divCells), this.divLines = document.createElement("div"), this.divLines.style.position = "absolute", this.divLines.oncontextmenu = this.Kk, this.vg.appendChild(this.divLines), this.divBreaks = document.createElement("div"), this.divBreaks.style.position = "absolute", this.divBreaks.oncontextmenu = this.Kk, this.vg.appendChild(this.divBreaks), this.divSeparators = document.createElement("div"), this.divSeparators.style.position = "absolute", this.divSeparators.oncontextmenu = this.Kk, this.vg.appendChild(this.divSeparators), this.divLinksBelow = document.createElement("div"), this.divLinksBelow.style.position = "absolute", this.vg.appendChild(this.divLinksBelow), this.divCrosshair = document.createElement("div"), this.divCrosshair.style.position = "absolute", this.divCrosshair.ondblclick = this.Lk, this.vg.appendChild(this.divCrosshair), this.divRange = document.createElement("div"), this.divRange.style.position = "absolute", this.divRange.oncontextmenu = this.Kk,
                        this.vg.appendChild(this.divRange), this.divEvents = document.createElement("div"), this.divEvents.style.position = "absolute", this.vg.appendChild(this.divEvents), this.divSeparatorsAbove = document.createElement("div"), this.divSeparatorsAbove.style.position = "absolute", this.divSeparatorsAbove.oncontextmenu = this.Kk, this.vg.appendChild(this.divSeparatorsAbove), this.divLinksAbove = document.createElement("div"), this.divLinksAbove.style.position = "absolute", this.vg.appendChild(this.divLinksAbove), this.divLinkShadow = document.createElement("div"), this.divLinkShadow.style.position = "absolute", this.vg.appendChild(this.divLinkShadow), this.divLinkpoints = document.createElement("div"), this.divLinkpoints.style.position = "absolute", this.vg.appendChild(this.divLinkpoints), this.divRectangle = document.createElement("div"), this.divRectangle.style.position = "absolute", this.vg.appendChild(this.divRectangle), this.divHover = document.createElement("div"), this.divHover.style.position = "absolute", this.vg.appendChild(this.divHover), this.divShadow = document.createElement("div"), this.divShadow.style.position = "absolute", this.vg.appendChild(this.divShadow), e.appendChild(this.vg), e
                }, this.Mk = {};
                var C = this.Mk;
                C.create = function() {
                    if (!c.nav.overlay) {
                        var e = document.createElement("div");
                        e.style.position = "absolute", e.style.left = "0px", e.style.right = "0px", e.style.top = "0px", e.style.bottom = "0px", e.className = c.q("_block"), c.nav.top.appendChild(e), c.nav.overlay = e
                    }
                }, C.show = function() {
                    C.create(), c.nav.overlay.style.display = ""
                }, C.hide = function() {
                    c.nav.overlay && (c.nav.overlay.style.display = "none")
                }, this.I = function(e) {
                    c.loadingTimeout && window.clearTimeout(c.loadingTimeout);
                    var t = e ? 0 : 100;
                    c.loadingTimeout = window.setTimeout(function() {
                        c.loadingLabelVisible && (c.nav.loading.innerHTML = c.loadingLabelText, c.nav.loading.style.display = ""), c.blockOnCallBack && C.show()
                    }, t)
                }, this.Z = function() {
                    this.loadingTimeout && window.clearTimeout(this.loadingTimeout), this.nav.loading.style.display = "none", this.blockOnCallBack && C.hide()
                }, this.loadingStart = function() {
                    c.I()
                }, this.loadingStop = function() {
                    c.Z()
                }, this.uiBlock = function() {
                    C.show()
                }, this.uiUnblock = function() {
                    C.hide()
                }, this.md = function() {
                    this.startDate = new DayPilot.Date(this.startDate).getDatePart()
                }, this.qd = function(e) {
                    var t = document.createElement("div");
                    t.style.position = "absolute", t.style.top = "-2000px", t.style.left = "-2000px", t.className = this.q(e), document.body.appendChild(t);
                    var i = t.offsetHeight,
                        n = t.offsetWidth;
                    document.body.removeChild(t);
                    var a = {};
                    return a.height = i, a.width = n, a
                }, this.ta = function(e) {
                    if (e && (this.autoRefreshEnabled = !0), this.autoRefreshEnabled && !(this.B >= this.autoRefreshMaxCount)) {
                        this.N();
                        var t = this.autoRefreshInterval;
                        if (!t || t < 10) throw "The minimum autoRefreshInterval is 10 seconds";
                        this.autoRefreshTimeout = window.setTimeout(function() {
                            c.Yc()
                        }, 1e3 * this.autoRefreshInterval)
                    }
                }, this.N = function() {
                    this.autoRefreshTimeout && (window.clearTimeout(this.autoRefreshTimeout), this.autoRefreshTimeout = null)
                }, this.autoRefreshStart = function(e) {
                    c.ta(e)
                }, this.autoRefreshPause = function() {
                    c.N()
                }, this.Nk = function() {
                    return !(n.resizing || n.moving || n.drag || n.range)
                }, this.Yc = function() {
                    if (c.Nk()) {
                        var e = !1;
                        if ("function" == typeof this.onAutoRefresh) {
                            var t = {};
                            t.i = this.B, t.preventDefault = function() {
                                this.preventDefault.value = !0
                            }, c.onAutoRefresh(t), t.preventDefault.value && (e = !0)
                        }!e && this.r() && this.commandCallBack(this.autoRefreshCommand), this.B++
                    }
                    this.B < this.autoRefreshMaxCount && (this.autoRefreshTimeout = window.setTimeout(function() {
                        c.Yc()
                    }, 1e3 * this.autoRefreshInterval))
                }, this.Zc = function() {
                    n.globalHandlers || (n.globalHandlers = !0, DayPilot.re(document, "mousemove", n.gMouseMove), DayPilot.re(document, "mouseup", n.gMouseUp), DayPilot.re(document, "mousedown", n.gMouseDown), DayPilot.re(document, "touchmove", n.gTouchMove), DayPilot.re(document, "touchend", n.gTouchEnd), DayPilot.re(window, "keyup", n.gKeyUp)), DayPilot.re(window, "resize", this.ve)
                }, this.Ok = function() {
                    this.nav.scroll.root = this, this.nav.scroll.onscroll = this.rg, c.dj = this.nav.scroll.scrollLeft, c.bj = this.nav.scroll.scrollTop, c.ej = this.divNorth.clientWidth
                }, this.Df = function() {
                    if (this.nav.state) {
                        var e = {};
                        e.scrollX = this.nav.scroll.scrollLeft, e.scrollY = this.nav.scroll.scrollTop;
                        var t = c.ai(e.scrollX, e.scrollY),
                            i = c.bi(t),
                            n = c.ci(t);
                        e.rangeStart = i.start, e.rangeEnd = i.end, e.resources = n, this.syncResourceTree && (e.tree = this.T()), this.nav.state.value = DayPilot.he(JSON.stringify(e))
                    }
                }, this.mg = function() {
                    if (this.separators)
                        for (var e = 0; e < this.separators.length; e++) this.dd(e)
                }, this.Pk = {}, this.Pk.step = 300, this.Pk.delay = 10, this.Pk.mode = "display", this.Pk.layers = !1, this.gj = function(e) {
                    if (c.cellStacking) return void S.calculateEventPositionsRow(e);
                    for (var t = this.durationBarDetached ? -10 : 0, i = 0, n = 0; n < e.lines.length; n++) {
                        var a = e.lines[n];
                        a.height = 0, a.top = i;
                        for (var o = 0; o < a.length; o++) {
                            var r = a[o];
                            if (!r.part.top, !0) {
                                r.part.line = n, r.part.height || (r.part.height = e.eventHeight);
                                var l = r.part.Pj();
                                l > a.height && (a.height = l), r.part.top = i + e.marginTop;
                                var s = "Above" === c.eventVersionPosition;
                                if (c.eventVersionsEnabled && !DayPilot.list(r.data.versions).isEmpty()) {
                                    var d = (r.data.versions.length, r.part.top);
                                    s || (d += r.part.height + c.eventVersionMargin), DayPilot.list(r.data.versions).each(function(e, t) {
                                        if (r.versions[t]) {
                                            var i = t * (c.eventVersionHeight + c.eventVersionMargin);
                                            r.versions[t].top = d + i, s && (r.part.top += c.eventVersionHeight, r.part.top += c.eventVersionMargin)
                                        }
                                    })
                                }
                                r.part.top += c.eventMarginBottom, r.part.detachedBarTop = r.part.top - t, r.part.right = r.part.left + r.part.width, r.part.fullTop = this.rowlist[r.part.dayIndex].top + r.part.top, r.part.fullBottom = r.part.fullTop + r.part.height
                            }
                        }
                        i += (a.height || e.eventHeight) * e.eventStackingLineHeight / 100
                    }
                };
                var S = {};
                S.loadRow = function(e) {
                    e.evColumns = DayPilot.list(c.itline).map(function(e, t) {
                        return {
                            "events": DayPilot.list()
                        }
                    }), DayPilot.list(e.events).each(function(t) {
                        var i = t.start(),
                            n = c.eh(i);
                        e.evColumns[n.i].events.push(t)
                    })
                }, S.calculateEventPositions = function() {
                    var e = c.cellStackingAutoHeight,
                        t = 0;
                    DayPilot.list(c.rowlist).each(function(i) {
                        var n = 0;
                        i.evColumns.each(function(t, a) {
                            var o = c.itline[a],
                                r = c.rowMarginTop;
                            t.events.each(function(t) {
                                t.part.left = o.left, t.part.width = o.width, t.part.top = r, t.part.height || (e ? t.part.height = S.getEventAutoHeight(t) + c.durationBarHeight : t.part.height = i.eventHeight), r += t.part.height + c.eventMarginBottom
                            }), t.height = r, t.height > n && (n = t.height)
                        }), i.maxColumnHeight = n, i.top = t;
                        var a = i.height;
                        i.height = i.getHeight(), n > c.rowMarginTop && (i.height = n + c.rowMarginBottom), a !== i.height && (c.Ch = !0), t += i.height
                    }), c.Zf = t
                }, S.getEventAutoHeight = function(e) {
                    var t = e.client.html(),
                        i = document.createElement("div");
                    i.style.position = "absolute", i.style.top = "-2000px", i.style.left = "-2000px", i.style.width = e.part.width + "px", i.className = c.q(E.event);
                    var n = document.createElement("div");
                    n.className = c.q(E.eventInner), n.innerHTML = t, n.style.position = "static", n.style.overflow = "auto", i.appendChild(n);
                    var a = c.divEvents;
                    a.appendChild(i);
                    var o = i.offsetHeight;
                    return a.removeChild(i), o
                }, S.calculateEventPositionsRow = function(e) {
                    S.calculateEventPositions()
                }, S.drawEventsWithoutCheckingOverflow = function() {
                    DayPilot.list(c.rowlist).each(function(e) {
                        S.drawEventsRow(e)
                    })
                }, S.drawEvents = function() {
                    S.drawEventsWithoutCheckingOverflow(), S.checkOverflow(), c.hg(), c.la()
                }, S.checkOverflow = function() {
                    if (c.cellStackingAutoHeight) {
                        var e = !1;
                        DayPilot.list(c.elements.events).each(function(t) {
                            var i = t.firstChild,
                                n = i.offsetHeight - i.clientHeight + t.offsetHeight - t.clientHeight,
                                a = i.scrollHeight + n,
                                o = t.event.part.height;
                            t.event.part.height = a, o !== a && (e = !0)
                        }), e && (c.O(), S.calculateEventPositions(), S.drawEventsWithoutCheckingOverflow(), c.ig(), c.lg())
                    }
                }, S.drawEventsRow = function(e) {
                    e.evColumns.each(function(e, t) {
                        e.events.each(function(e) {
                            c.Ub(e)
                        })
                    })
                }, this.qa = function(e) {
                    if (!c.Kd) {
                        if (c.cellStacking) return void S.drawEvents();
                        var t = this.Pk.step;
                        this.Pk.layers && (c.divEvents = document.createElement("div"), c.divEvents.style.position = "absolute", c.vg.insertBefore(this.divEvents, this.divSeparatorsAbove)), "display" === this.Pk.mode ? this.divEvents.style.display = "none" : "visibility" === this.Pk.mode && (this.divEvents.style.visibility = "hidden"), this.divEvents.setAttribute("role", "region"), this.divEvents.setAttribute("aria-label", "scheduler events");
                        for (var i = "Progressive" === this.dynamicEventRendering, n = this.Ag(), a = n.pixels.top, o = n.pixels.bottom, r = c.allowMultiMove || c.allowMultiResize, l = 0; l < this.rowlist.length; l++) {
                            var s = this.rowlist[l],
                                d = s.top - this.dynamicEventRenderingMargin,
                                h = d + s.height + 2 * this.dynamicEventRenderingMargin;
                            if (!i || r || !(o <= d || a >= h)) {
                                this.gj(s);
                                for (var u = 0; u < s.lines.length; u++)
                                    for (var f = s.lines[u], p = 0; p < f.length; p++) {
                                        var g = f[p],
                                            m = this.Ub(g);
                                        if (e && m && (t--, t <= 0)) return this.divEvents.style.visibility = "", this.divEvents.style.display = "", void window.setTimeout(function() {
                                            c.qa(e)
                                        }, c.Pk.delay)
                                    }
                            }
                        }
                        this.divEvents.style.display = "", DayPilot.list(c.multiselect.list).isEmpty() || c.multiselect.redraw(), this.Dg(), v.load()
                    }
                }, this.Eh = function(e) {
                    var t = this.rowlist[e];
                    if (c.cellStacking) return void S.drawEventsRow(t);
                    this.divEvents = document.createElement("div"), this.divEvents.style.position = "absolute", this.divEvents.style.display = "none", this.vg.insertBefore(this.divEvents, this.divSeparatorsAbove);
                    this.durationBarDetached ? 10 : 0;
                    this.gj(t);
                    for (var i = 0; i < t.lines.length; i++)
                        for (var n = t.lines[i], a = 0; a < n.length; a++) {
                            var o = n[a];
                            this.Ub(o)
                        }
                    this.divEvents.style.display = ""
                }, this.O = function() {
                    if (this.elements.events)
                        for (var e = this.elements.events.length, t = 0; t < e; t++) {
                            var i = this.elements.events[t];
                            this.zi(i)
                        }
                    this.elements.events = []
                }, this.Dh = function(e) {
                    if (this.elements.events) {
                        for (var t = this.elements.events.length, i = [], n = 0; n < t; n++) {
                            var a = this.elements.events[n];
                            a.event.part.dayIndex === e && (this.zi(a), i.push(n))
                        }
                        for (var n = i.length - 1; n >= 0; n--) this.elements.events.splice(i[n], 1)
                    }
                }, this.zi = function(e) {
                    e.parentNode && e.parentNode.removeChild(e), e.onclick = null, e.oncontextmenu = null, e.onmouseover = null, e.onmouseout = null, e.onmousemove = null, e.onmousedown = null, e.ondblclick = null, e.event && (e.isBar || (e.event.rendered = null), e.event = null), e.related && DayPilot.de(e.related)
                }, this.Qk = function(e) {
                    e.event && (e.event.rendered = !1), e.onclick = null, e.onmousedown = null, e.event = null, e.parentNode && e.parentNode.removeChild(e)
                }, this.Rk = function() {
                    if ("Progressive" === this.dynamicEventRendering) {
                        if (c.dynamicEventRenderingCacheSweeping) {
                            var e = c.dynamicEventRenderingCacheSize || 0;
                            this.divEvents.style.display = "none";
                            for (var t = [], i = 0, n = this.elements.events.length, a = n - 1; a >= 0; a--) {
                                var o = this.elements.events[a];
                                this.Sk(o.event) ? e > 0 ? (e--, t.unshift(o)) : (this.zi(o), i++) : t.unshift(o)
                            }
                            this.elements.events = t, this.divEvents.style.display = ""
                        }
                    }
                }, this.Tk = function(e) {
                    for (var t = [], i = 0, n = this.Ag(), a = this.elements.cells.length, o = a - 1; o >= 0; o--) {
                        var r = this.elements.cells[o];
                        n.xStart < r.coords.x && r.coords.x <= n.xEnd && n.yStart < r.coords.y && r.coords.y <= n.yEnd ? t.unshift(r) : e > 0 ? (e--, t.unshift(r)) : (this.Gi(r), i++)
                    }
                }, this.Gi = function(e) {
                    if (e) {
                        var t = e.coords.x,
                            i = e.coords.y;
                        DayPilot.rfa(c.elements.cells, e), DayPilot.de(e), c.t.cells[t + "_" + i] = null
                    }
                }, this.jg = function() {
                    if (this.elements.separators)
                        for (var e = 0; e < this.elements.separators.length; e++) {
                            var t = this.elements.separators[e];
                            DayPilot.de(t)
                        }
                    this.elements.separators = []
                }, this.Uk = function() {
                    var e = "Progressive" === this.dynamicEventRendering;
                    if (!this.nav.scroll) return !1;
                    for (var t = this.nav.scroll.scrollTop, i = t + this.nav.scroll.clientHeight, n = 0; n < this.rowlist.length; n++) {
                        var a = this.rowlist[n],
                            o = a.top,
                            r = a.top + a.height;
                        if (!e || !(i <= o || t >= r))
                            for (var l = 0; l < a.lines.length; l++)
                                for (var s = a.lines[l], d = 0; d < s.length; d++) {
                                    var c = s[d];
                                    if (this.Vk(c)) return !0
                                }
                    }
                    return !1
                }, this.Vk = function(e) {
                    if (e.rendered) return !1;
                    var t = "Progressive" === this.dynamicEventRendering,
                        i = this.nav.scroll.scrollLeft,
                        n = i + this.nav.scroll.clientWidth,
                        a = e.part.left,
                        o = e.part.left + e.part.width;
                    return !t || !(n <= a || i >= o)
                }, this.Sk = function(e) {
                    if (!e.rendered) return !0;
                    var t = this.Ag(),
                        i = t.pixels.top,
                        n = t.pixels.bottom,
                        a = t.pixels.left - this.dynamicEventRenderingMargin,
                        o = t.pixels.right + this.dynamicEventRenderingMargin,
                        r = e.part.left,
                        l = e.part.right,
                        s = e.part.fullTop,
                        d = e.part.fullBottom;
                    return o <= r || a >= l || (n <= s || i >= d)
                }, this.Oj = DayPilot.list(), this.Wk = function(e) {
                    if (e.rendered) return !1;
                    if (e.row.hidden) return !1;
                    var t = e.min,
                        i = e.max - e.min,
                        n = c.eventHeight,
                        a = e.row.top,
                        o = document.createElement("div");
                    o.style.position = "absolute", o.style.left = t + "px", o.style.top = a + "px", o.style.width = i + "px", o.style.height = n + "px", o.className = c.q("_event_group"), o.style.cursor = "pointer";
                    var r = {};
                    r.group = {}, r.group.count = e.events.length, r.group.events = DayPilot.list(e.events, !0), r.group.html = "[+] " + e.events.length + " events", "function" == typeof c.onBeforeGroupRender && c.onBeforeGroupRender(r);
                    var l = document.createElement("div");
                    return l.innerHTML = r.group.html, o.appendChild(l), o.onmousedown = function(e) {
                        var e = e || window.event;
                        e.cancelBubble = !0
                    }, o.onclick = function(e) {
                        var t = o.event;
                        t.expanded = !0, c.Qk(o);
                        var i = DayPilot.indexOf(c.elements.events, o);
                        i !== -1 && c.elements.events.splice(i, 1);
                        var n = c.Oj;
                        DayPilot.list(t.events).each(function(e) {
                            n.push(e.id())
                        }), c.mh(), c.nh(t.part.dayIndex), c.la();
                        var e = e || window.event;
                        e.cancelBubble = !0
                    }, e.part = {}, e.part.left = t, e.part.width = i, e.part.height = c.eventHeight, e.part.dayIndex = DayPilot.indexOf(c.rowlist, e.row), e.part.top = 0, e.part.isBlock = !0, e.client = {}, e.client.html = function() {
                        return r.group.html
                    }, e.data = {}, o.event = e, this.elements.events.push(o), this.divEvents.appendChild(o), e.rendered = !0, !0
                }, this.Ub = function(e) {
                    if (e.rendered) return !1;
                    if (c.groupConcurrentEvents) {
                        var t = e.part.block;
                        if (t.events.length > 1 && !t.expanded) return c.Wk(t)
                    }
                    var i = "Progressive" === this.dynamicEventRendering,
                        n = e.part.dayIndex,
                        a = this.rowlist[n];
                    if (a.hidden) return !1;
                    var o = a.top,
                        r = this.Ag(),
                        l = r.pixels.left - this.dynamicEventRenderingMargin,
                        s = r.pixels.right + this.dynamicEventRenderingMargin,
                        d = r.pixels.top,
                        h = r.pixels.bottom,
                        u = e.part.left,
                        f = e.part.left + e.part.width,
                        v = e.part.top + o,
                        p = v + e.part.height,
                        g = c.multiselect.Db(e);
                    if (!g && i && (s <= u || l >= f || h <= v || d >= p)) return !1;
                    var m = e.part.width,
                        y = e.part.height,
                        b = e.cache || e.data;
                    m = Math.max(0, m), y = Math.max(0, y);
                    var w = this.durationBarDetached,
                        D = document.createElement("div");
                    if (D.related = [], w) {
                        var k = e.part.barLeft,
                            x = e.part.barWidth;
                        "PercentComplete" === this.durationBarMode && (k = 0, x = (b.complete || 0) / 100 * m);
                        var P = document.createElement("div");
                        P.style.position = "absolute", P.style.left = e.part.left + k + "px", P.style.top = o + e.part.detachedBarTop + "px", P.style.width = x + "px", P.style.height = "5px", P.style.backgroundColor = "black", P.type = "detachedBar", D.related.push(P), this.divEvents.appendChild(P)
                    }
                    if (function(t) {
                            c.eventVersionsEnabled && DayPilot.list(e.data.versions).each(function(i, n) {
                                if (i) {
                                    var a = e.versions[n];
                                    if (a) {
                                        var r = document.createElement("div");
                                        r.style.position = "absolute", r.style.left = a.left + "px", r.style.top = o + a.top + "px", r.style.width = a.width + "px", r.style.height = c.eventVersionHeight + "px", r.className = c.q(E.event) + " " + c.q("_event_previous") + " " + c.q("_event_version"), i.toolTip && (r.title = i.toolTip), i.cssClass && DayPilot.Util.addClass(r, i.cssClass);
                                        var l = document.createElement("div");
                                        l.setAttribute("unselectable", "on"), l.className = c.q(E.eventInner), l.innerHTML = i.html || i.text || "", i.backColor && (l.style.background = i.backColor, (DayPilot.browser.ie9 || DayPilot.browser.ielt9) && (l.style.filter = "")), i.fontColor && (l.style.color = i.fontColor), i.borderColor && (l.style.borderColor = i.borderColor), i.backImage && (l.style.backgroundImage = "url(" + i.backImage + ")", i.backRepeat && (l.style.backgroundRepeat = i.backRepeat)), r.appendChild(l), a.continueLeft && DayPilot.Util.addClass(r, c.q("_event_continueleft")), a.continueRight && DayPilot.Util.addClass(r, c.q("_event_continueright"));
                                        var s = c.durationBarVisible && !i.barHidden,
                                            d = a.width;
                                        if (s && d > 0) {
                                            var h = 100 * a.barLeft / d,
                                                u = Math.ceil(100 * a.barWidth / d);
                                            "PercentComplete" === c.durationBarMode && (h = 0, u = i.complete || 0);
                                            var f = document.createElement("div");
                                            f.setAttribute("unselectable", "on"), f.className = c.q(E.eventBar), f.style.position = "absolute", i.barBackColor && (f.style.backgroundColor = i.barBackColor);
                                            var v = document.createElement("div");
                                            v.setAttribute("unselectable", "on"), v.className = c.q(E.eventBarInner), v.style.left = h + "%", 0 < u && u <= 1 ? v.style.width = "1px" : v.style.width = u + "%", i.barColor && (v.style.backgroundColor = i.barColor), i.barImageUrl && (v.style.backgroundImage = "url(" + i.barImageUrl + ")"), f.appendChild(v), r.appendChild(f)
                                        }
                                        if (i.htmlLeft) {
                                            var p = c.eventHtmlLeftMargin,
                                                g = document.createElement("div");
                                            g.style.position = "absolute", g.style.right = -(a.left - p) + "px", g.style.top = o + a.top + "px", g.style.height = c.eventHeight + "px", g.style.boxSizing = "border-box", g.innerHTML = i.htmlLeft, g.className = c.q("_event_left"), g.type = "divLeft", g.versionPart = a, t.related.push(g), c.divEvents.appendChild(g)
                                        }
                                        if (i.htmlRight) {
                                            var p = c.eventHtmlRightMargin,
                                                m = document.createElement("div");
                                            m.style.position = "absolute", m.style.left = a.left + a.width + p + "px", m.style.top = o + a.top + "px", m.style.height = c.eventHeight + "px", m.style.boxSizing = "border-box", m.innerHTML = i.htmlRight, m.className = c.q("_event_right"), m.type = "divRight", m.versionPart = a, t.related.push(m), c.divEvents.appendChild(m)
                                        }
                                        r.versionPart = a, r.type = "version", t.related.push(r), c.divEvents.appendChild(r)
                                    }
                                }
                            })
                        }(D), b.htmlLeft) {
                        var C = c.eventHtmlLeftMargin,
                            S = document.createElement("div");
                        S.style.position = "absolute", S.style.right = -(e.part.left - C) + "px", S.style.top = o + e.part.top + "px", S.style.height = c.eventHeight + "px", S.style.boxSizing = "border-box", S.innerHTML = b.htmlLeft, S.className = c.q("_event_left"), S.type = "divLeft", D.related.push(S), this.divEvents.appendChild(S)
                    }
                    if (b.htmlRight) {
                        var C = c.eventHtmlRightMargin,
                            A = document.createElement("div");
                        A.style.position = "absolute", A.style.left = e.part.left + e.part.width + C + "px", A.style.top = o + e.part.top + "px", A.style.height = c.eventHeight + "px", A.style.boxSizing = "border-box", A.innerHTML = b.htmlRight, A.className = c.q("_event_right"), A.type = "divRight", D.related.push(A), this.divEvents.appendChild(A)
                    }
                    var d = o + e.part.top;
                    D.style.position = "absolute", D.style.left = e.part.left + c.eventMarginLeft + "px", D.style.top = o + e.part.top + "px", D.style.width = m - c.eventMarginLeft - c.eventMarginRight + "px", D.style.height = y + "px", c.eventTextWrappingEnabled || (D.style.whiteSpace = "nowrap"), D.style.overflow = "hidden", D.className = this.q(E.event), "Milestone" === e.data.type && DayPilot.Util.addClass(D, c.q("_task_milestone")), "Group" === e.data.type && (DayPilot.Util.addClass(D, c.q("_task_parent")), DayPilot.Util.addClass(D, c.q("_task_group"))), b.cssClass && DayPilot.Util.addClass(D, b.cssClass);
                    "number" == typeof e.part.line && DayPilot.Util.addClass(D, this.q(E.eventLine + e.part.line)), D.setAttribute("unselectable", "on"), this.showToolTip && !this.bubble && (D.title = e.client.toolTip() || ""), D.onmousemove = this.Vb, D.onmouseout = this.Wb, D.onmousedown = this.Xb, D.onmouseup = this.Xk, D.ontouchstart = T.onEventTouchStart, D.ontouchmove = T.onEventTouchMove, D.ontouchend = T.onEventTouchEnd, e.client.clickEnabled() && (D.onclick = this.We), e.client.doubleClickEnabled() && (D.ondblclick = this.Ma), D.oncontextmenu = this.Na, "undefined" != typeof b.ariaLabel ? D.setAttribute("aria-label", b.ariaLabel) : D.setAttribute("aria-label", b.text), D.setAttribute("tabindex", "-1");
                    var M = document.createElement("div");
                    M.setAttribute("unselectable", "on"), M.className = c.q(E.eventInner), M.innerHTML = e.client.innerHTML(), b.backColor && (M.style.background = b.backColor, (DayPilot.browser.ie9 || DayPilot.browser.ielt9) && (M.style.filter = "")), b.fontColor && (M.style.color = b.fontColor), b.borderColor && (M.style.borderColor = b.borderColor), b.backImage && (M.style.backgroundImage = "url(" + b.backImage + ")", b.backRepeat && (M.style.backgroundRepeat = b.backRepeat)), D.appendChild(M);
                    var H = e.start().getTime() === e.part.start.getTime(),
                        _ = e.rawend().getTime() === e.part.end.getTime();
                    if (H || DayPilot.Util.addClass(D, this.q("_event_continueleft")), _ || DayPilot.Util.addClass(D, this.q("_event_continueright")), e.client.barVisible() && m > 0) {
                        var k = 100 * e.part.barLeft / m,
                            x = Math.ceil(100 * e.part.barWidth / m);
                        "PercentComplete" === this.durationBarMode && (k = 0, x = b.complete || 0);
                        var P = document.createElement("div");
                        P.setAttribute("unselectable", "on"), P.className = this.q(E.eventBar), P.style.position = "absolute", b.barBackColor && (P.style.backgroundColor = b.barBackColor);
                        var R = document.createElement("div");
                        R.setAttribute("unselectable", "on"), R.className = this.q(E.eventBarInner), R.style.left = k + "%", 0 < x && x <= 1 ? R.style.width = "1px" : R.style.width = x + "%", b.barColor && (R.style.backgroundColor = b.barColor), b.barImageUrl && (R.style.backgroundImage = "url(" + b.barImageUrl + ")"), P.appendChild(R), D.appendChild(P)
                    }
                    if (D.row = n, b.areas)
                        for (var B = 0; B < b.areas.length; B++) {
                            var r = b.areas[B],
                                N = r.visibility || r.v || "Visible";
                            if ("Visible" === N) {
                                (r.start || r.end) && (r.left = c.getPixels(new DayPilot.Date(r.start)).left - e.part.left, r.width = c.getPixels(new DayPilot.Date(r.end)).left - r.left - e.part.left);
                                var U = DayPilot.Areas.createArea(D, e, r);
                                D.appendChild(U)
                            }
                        }
                    if (this.elements.events.push(D), this.divEvents.appendChild(D), e.rendered = !0, D.event = e, g && (c.multiselect.add(D.event, !0), c.multiselect.ld(D)), c.va()) {
                        if ("function" == typeof c.onAfterEventRender) {
                            var z = {};
                            z.e = D.event, z.div = D, c.onAfterEventRender(z)
                        }
                    } else c.afterEventRender && c.afterEventRender(D.event, D);
                    return !0
                }, this.va = function() {
                    return 2 === c.api
                }, this.Fh = function() {
                    for (var e = 0; e < this.elements.events.length; e++) {
                        var t = this.elements.events[e],
                            i = t.event,
                            n = i.part.dayIndex,
                            a = this.rowlist[n],
                            o = a.top,
                            r = o + i.part.top;
                        t.style.top = r + "px", DayPilot.list(t.related).filter(function(e) {
                            return !!e.versionPart
                        }).each(function(e) {
                            var t = e.versionPart;
                            e.style.top = o + t.top + "px"
                        }), DayPilot.list(t.related).filter(function(e) {
                            return !e.versionPart && ("divLeft" === e.type || "divRight" === e.type)
                        }).each(function(e) {
                            e.style.top = r + "px"
                        })
                    }
                }, this.jd = function(e) {
                    if (!e) return null;
                    for (var t = 0; t < c.elements.events.length; t++) {
                        var i = c.elements.events[t];
                        if (i.event === e || i.event.data === e.data) return i
                    }
                    return null
                }, this.Wb = function(e) {
                    var t = this;
                    DayPilot.Areas.hideAreas(t, e), DayPilot.Util.removeClass(t, c.q("_event_hover")), c.Yk(t), s.source || v.hideLinkpointsWithDelay(), c.bubble && "Bubble" === c.eventHoverHandling && c.bubble.hideOnMouseOut()
                }, this.Zk = function(e) {
                    if ("function" == typeof this.onEventMouseOver) {
                        var t = {};
                        t.div = e, t.e = e.event, this.onEventMouseOver(t)
                    }
                }, this.Yk = function(e) {
                    if ("function" == typeof this.onEventMouseOut) {
                        var t = {};
                        t.div = e, t.e = e.event, this.onEventMouseOut(t)
                    }
                }, this.Vb = function(e) {
                    e = e || window.event, c.cellBubble && c.cellBubble.delayedHide();
                    for (var t = this; t && !t.event;) t = t.parentNode;
                    c.$k(t, e);
                    var i = t.event.cache ? t.event.cache.deleteDisabled : t.event.data.deleteDisabled;
                    if (!t.active) {
                        var n = [];
                        if ("Disabled" !== c.eventDeleteHandling && !i) {
                            var a = c.durationBarVisible ? c.durationBarHeight : 0;
                            n.push({
                                "action": "JavaScript",
                                "v": "Hover",
                                "w": 17,
                                "h": 17,
                                "top": a + 2,
                                "right": 2,
                                "css": c.q(E.eventDelete),
                                "js": function(e) {
                                    c.Ra(e)
                                }
                            })
                        }
                        var o = t.event.cache ? t.event.cache.areas : t.event.data.areas;
                        o && o.length > 0 && (n = n.concat(o)), DayPilot.Areas.showAreas(t, t.event, null, n), DayPilot.Util.addClass(t, c.q("_event_hover")), c.Zk(t)
                    }
                    "Disabled" === c.linkCreateHandling || s.source || (v.clearHideTimeout(), v.hideLinkpoints(), c.Nk() && v.showLinkpoint(t)), e.srcElement ? e.srcElement.insideEvent = !0 : e.insideEvent = !0
                }, this._k = {};
                var A = this._k;
                this.Xk = function(e) {
                    if (c.ionicEventClickFix) {
                        var t = A.originalMouse;
                        if (t) {
                            var i = DayPilot.mc(e);
                            t.x === i.x && t.y === i.y && (c.We.call(this, e), e.preventDefault(), e.stopPropagation())
                        }
                    }
                }, this.Xb = function(e) {
                    if (c.U(), "undefined" != typeof DayPilot.Bubble && (DayPilot.Bubble.hideActive(), DayPilot.Bubble.cancelShowing()), e = e || window.event, !c.coords) {
                        var t = c.vg;
                        c.coords = DayPilot.mo3(t, e)
                    }
                    var i = DayPilot.Util.mouseButton(e);
                    if (e.preventDefault(), e.stopPropagation(), i.left) {
                        var a = e.shiftKey;
                        if ("Disabled" !== c.multiSelectRectangle && a) return M.start(), !1;
                        "w-resize" === this.style.cursor || "e-resize" === this.style.cursor ? (n.preventEventClick = !0, n.resizing = this, n.resizingEvent = this.event, n.originalMouse = DayPilot.mc(e), document.body.style.cursor = this.style.cursor, v.hideLinkpoints()) : ("move" === this.style.cursor || "Full" === c.moveBy && this.event.client.moveEnabled()) && (A.start = !0, A.moving = this, A.movingEvent = this.event, A.originalMouse = DayPilot.mc(e), A.moveOffsetX = DayPilot.mo3(this, e).x, A.moveDragStart = c.getDate(c.coords.x, !0), v.hideLinkpoints())
                    }
                    DayPilot.Menu && DayPilot.Menu.active && DayPilot.Menu.active.hide()
                }, this.Yb = {};
                var T = c.Yb;
                "undefined" == typeof DayPilot.Global.touch && (DayPilot.Global.touch = {}), DayPilot.Global.touch.active = !1, DayPilot.Global.touch.start = !1, T.timeouts = [], T.onEventTouchStart = function(e) {
                    if (!DayPilot.Global.touch.active && !DayPilot.Global.touch.start) {
                        e.stopPropagation(), T.clearTimeouts(), DayPilot.Global.touch.start = !0, DayPilot.Global.touch.active = !1;
                        var i = this,
                            n = c.tapAndHoldTimeout;
                        T.timeouts.push(window.setTimeout(function() {
                            DayPilot.Global.touch.active = !0, DayPilot.Global.touch.start = !1, c.coords = T.relativeCoords(e), e.preventDefault();
                            var n = i.event;
                            switch (c.eventTapAndHoldHandling) {
                                case "Move":
                                    if (n.client.moveEnabled()) {
                                        var a = t(e);
                                        T.startMoving(i, a)
                                    }
                                    break;
                                case "ContextMenu":
                                    DayPilot.Menu && DayPilot.Menu.touchPosition(e);
                                    var o = n.client.contextMenu();
                                    o ? o.show(n) : c.contextMenu && c.contextMenu.show(n)
                            }
                        }, n))
                    }
                }, T.onEventTouchMove = function(e) {
                    T.clearTimeouts(), DayPilot.Global.touch.start = !1
                }, T.onEventTouchEnd = function(e) {
                    if (!DayPilot.Util.isMouseEvent(e)) {
                        if (T.clearTimeouts(), DayPilot.Global.touch.start) {
                            DayPilot.Global.touch.start = !1, e.preventDefault(), e.stopPropagation();
                            var t = this;
                            window.setTimeout(function() {
                                c.Ja(t, e)
                            })
                        }
                        window.setTimeout(function() {
                            DayPilot.Global.touch.start = !1, DayPilot.Global.touch.active = !1
                        }, 500)
                    }
                }, T.onMainTouchStart = function(e) {
                    if (!(DayPilot.Global.touch.active || DayPilot.Global.touch.start || e.touches.length > 1 || "Disabled" === c.timeRangeSelectedHandling)) {
                        T.clearTimeouts(), DayPilot.Global.touch.start = !0, DayPilot.Global.touch.active = !1;
                        var t = c.tapAndHoldTimeout;
                        T.timeouts.push(window.setTimeout(function() {
                            DayPilot.Global.touch.active = !0, DayPilot.Global.touch.start = !1, e.preventDefault(), c.coords = T.relativeCoords(e), T.range = c.al()
                        }, t));
                        c.coords = T.relativeCoords(e)
                    }
                }, T.onMainTouchMove = function(e) {
                    if (T.clearTimeouts(), DayPilot.Global.touch.start = !1, n.resizing) return e.preventDefault(), void T.updateResizing();
                    if (DayPilot.Global.touch.active) {
                        if (e.preventDefault(), c.coords = T.relativeCoords(e), n.moving) return void T.updateMoving();
                        if (T.range) {
                            var t = T.range;
                            t.end = {
                                x: Math.floor(c.coords.x / c.cellWidth)
                            }, c.Lh(t)
                        }
                    }
                }, T.onMainTouchEnd = function(e) {
                    T.clearTimeouts();
                    if (DayPilot.Global.touch.active) {
                        if (n.moving) {
                            e.preventDefault();
                            var t = n.movingEvent;
                            if (c !== n.movingShadow.calendar) return;
                            var i = n.movingShadow.start,
                                a = n.movingShadow.end,
                                o = "Days" !== c.viewType ? n.movingShadow.row.id : null,
                                r = n.drag && t.part.external,
                                l = n.movingShadow.overlapping,
                                s = !n.movingShadow.allowed;
                            if (DayPilot.Util.removeClass(n.moving, c.q(c.bl.eventMovingSource)), DayPilot.de(n.movingShadow), c.sj(), n.movingShadow.calendar = null, document.body.style.cursor = "", n.moving = null, n.movingEvent = null, n.movingShadow = null, c.nj.clear(), l || s || c.nj.forbidden || c.nj.invalid) return;
                            c.Xa(t, i, a, o, r)
                        }
                        if (T.range) {
                            var d = T.range;
                            T.range = null;
                            var h = c.elements.range2;
                            h && h.overlapping ? c.clearSelection() : c.Ih(d)
                        }
                    } else if (DayPilot.Global.touch.start) {
                        if (c.coords.x < c.getScrollX()) return;
                        var d = c.al();
                        c.Lh(d);
                        var h = c.elements.range2;
                        h && h.overlapping ? c.clearSelection() : c.Ih(d)
                    }
                    window.setTimeout(function() {
                        DayPilot.Global.touch.start = !1, DayPilot.Global.touch.active = !1
                    }, 500)
                }, T.clearTimeouts = function() {
                    for (var e = 0; e < T.timeouts.length; e++) clearTimeout(T.timeouts[e]);
                    T.timeouts = []
                }, T.relativeCoords = function(e) {
                    var t = c.vg,
                        i = e.touches[0].pageX,
                        n = e.touches[0].pageY,
                        a = DayPilot.abs(t);
                    return {
                        x: i - a.x,
                        y: n - a.y,
                        toString: function() {
                            return "x: " + this.x + ", y:" + this.y
                        }
                    }
                }, T.startMoving = function(e, t) {
                    n.moving = e, n.movingEvent = e.event, n.originalMouse = t;
                    var i = DayPilot.abs(e);
                    n.moveOffsetX = t.x - i.x, n.moveDragStart = c.getDate(c.coords.x, !0), n.movingShadow = c.wa(e), c.$i()
                }, T.startResizing = function(e, t) {
                    n.resizing = e, n.resizingEvent = e.event, n.resizing.dpBorder = t, n.resizingShadow || (n.resizingShadow = c.wa(e)), c.oj()
                }, T.updateResizing = function() {
                    if (!n.resizingShadow) {
                        var e = n.resizing;
                        n.resizingShadow = c.wa(e)
                    }
                    c.oj()
                }, T.updateMoving = function() {
                    if (n.movingShadow && n.movingShadow.calendar !== c && c.sj(), !n.movingShadow) {
                        var e = n.moving;
                        n.movingShadow = c.wa(e)
                    }
                    n.movingShadow.calendar.$i()
                }, this.$k = function(e, t) {
                    var i = this.eventResizeMargin,
                        a = this.eventMoveMargin,
                        o = e;
                    if ("undefined" != typeof n) {
                        var r = DayPilot.mo3(e, t);
                        if (r && (c.eventOffset = r, !n.resizing && !n.moving)) {
                            var l = o.event.part.start.toString() === o.event.start().toString(),
                                s = o.event.part.end.toString() === o.event.rawend().toString();
                            if ("Top" === c.moveBy && r.y <= a && o.event.client.moveEnabled() && "Disabled" !== c.eventMoveHandling ? e.style.cursor = "move" : ("Top" === c.moveBy || "Full" === c.moveBy) && r.x <= i && o.event.client.resizeEnabled() && "Disabled" !== c.eventResizeHandling ? l ? (e.style.cursor = "w-resize", e.dpBorder = "left") : e.style.cursor = "not-allowed" : "Left" === c.moveBy && r.x <= a && o.event.client.moveEnabled() && "Disabled" !== c.eventMoveHandling ? e.style.cursor = "move" : e.offsetWidth - r.x <= i && o.event.client.resizeEnabled() && "Disabled" !== c.eventResizeHandling ? s ? (e.style.cursor = "e-resize", e.dpBorder = "right") : e.style.cursor = "not-allowed" : n.resizing || n.moving || (o.event.client.clickEnabled() && "Disabled" !== c.eventClickHandling ? e.style.cursor = "pointer" : e.style.cursor = "default"), "undefined" != typeof DayPilot.Bubble && c.bubble && "Bubble" === c.eventHoverHandling && ("default" === e.style.cursor || "pointer" === e.style.cursor)) {
                                this._c && r.x === this._c.x && r.y === this._c.y;
                                this._c = r, c.bubble.showEvent(e.event)
                            }
                        }
                    }
                }, this.cj = function() {
                    return "Days" !== this.viewType ? this.itline.length : Math.floor(1440 / this.cellDuration)
                }, this.Jh = function(e) {
                    var e = e || n.range || c.rangeHold;
                    if (!e) return null;
                    var t = c.rowlist[e.start.y];
                    if (!t) return null;
                    var i = e.cl ? e.cl : e,
                        a = t.id,
                        o = i.end.x > i.start.x ? i.start.x : i.end.x,
                        r = i.end.x > i.start.x ? i.end.x : i.start.x,
                        l = 0;
                    l = t.start.getTime() - this.Da().getTime();
                    var s = this.itline[o].start.addTime(l),
                        d = this.itline[r].end.addTime(l);
                    return new DayPilot.Selection(s, d, a, c)
                }, this.yb = function(e) {
                    var t = e.parentNode,
                        i = c.eventEditMinWidth,
                        n = document.createElement("textarea");
                    return n.style.position = "absolute", n.style.width = (e.offsetWidth < i ? i : e.offsetWidth - 2) + "px", n.style.height = e.offsetHeight - 2 + "px", n.style.fontFamily = DayPilot.gs(e, "fontFamily") || DayPilot.gs(e, "font-family"), n.style.fontSize = DayPilot.gs(e, "fontSize") || DayPilot.gs(e, "font-size"), n.style.left = e.offsetLeft + "px", n.style.top = e.offsetTop + "px", n.style.border = "1px solid black", n.style.padding = "0px", n.style.marginTop = "0px", n.style.backgroundColor = "white", n.value = DayPilot.tr(e.event.text()), n.event = e.event, t.appendChild(n), n
                }, this.dl = function(e) {
                    var t = h.events();
                    t.sort(function(e, t) {
                        var i = e.event,
                            n = t.event;
                        return i.part.dayIndex !== n.part.dayIndex ? i.part.dayIndex - n.part.dayIndex : i.start() !== n.start() ? i.start().getTime() - n.start().getTime() : n.end().getTime() - i.end().getTime()
                    });
                    var i = t.indexOf(e);
                    return i === -1 ? null : i + 1 < t.length ? t[i + 1] : t[0]
                }, this.Ka = function(e) {
                    if (e && (n.editing && n.editing.blur(), e.event)) {
                        var t = this.yb(e);
                        n.editing = t, DayPilot.re(t, DayPilot.touch.start, function(e) {
                            e.stopPropagation()
                        }), t.onblur = function() {
                            if (t.onblur = null, n.editing === t && (n.editing = null), t.parentNode && t.parentNode.removeChild(t), e.event) {
                                var i = (e.event.text(), t.value);
                                c.eb(e.event, i, t.canceling)
                            }
                        }, t.onmousedown = function(e) {
                            e = e || window.event, e.stopPropagation && e.stopPropagation()
                        }, t.onkeypress = function(e) {
                            return 13 !== (window.event ? event.keyCode : e.keyCode) || (this.onblur(), !1)
                        }, t.cancel = function() {
                            n.editing && (n.editing.canceling = !0, n.editing.blur())
                        }, t.onkeydown = function(t) {
                            var i = window.event ? event.keyCode : t.keyCode;
                            if (27 === i) n.editing.cancel();
                            else if (9 === i) {
                                var a = c.dl(e);
                                return n.editing.cancel(), a && (DayPilot.browser.ie ? setTimeout(function() {
                                    c.Ka(a)
                                }, 0) : c.Ka(a)), !1
                            }
                        }, t.select(), t.focus()
                    }
                }, this.Fj = function(e) {
                    var t = this,
                        i = t.row;
                    if ("undefined" != typeof DayPilot.Bubble && (c.cellBubble, c.resourceBubble)) {
                        var n = c.Ph(i);
                        n.div = t, c.resourceBubble.showResource(n)
                    }
                }, this.Gj = function(e) {
                    var t = this;
                    "undefined" != typeof DayPilot.Bubble && c.resourceBubble && c.resourceBubble.hideOnMouseOut();
                    var i = t.firstChild;
                    DayPilot.Areas.hideAreas(i, e), i.data = null
                }, this.Hj = function(e) {
                    l.row && (x.cancelClick = !0, setTimeout(function() {
                        x.cancelClick = !1
                    }, 100))
                }, this.eg = function() {
                    if (this.timeHeader) {
                        this.t.timeHeader = {};
                        var e = document.createElement("div");
                        e.style.position = "relative", this.nav.timeHeader = e;
                        for (var t = 0; t < this.timeHeader.length; t++)
                            for (var i = this.timeHeader[t], n = 0; n < i.length; n++) this.fl(n, t);
                        var a = this.divNorth;
                        if (1 === a.childNodes.length) a.replaceChild(e, a.childNodes[0]);
                        else {
                            if (DayPilot.browser.ie && a && a.firstChild) {
                                for (var o = [], r = 0; r < a.firstChild.childNodes.length; r++) o.push(a.firstChild.childNodes[r]);
                                DayPilot.de(o)
                            }
                            a.innerHTML = "", a.appendChild(e)
                        }
                        var l = this.Jg();
                        a.style.width = l + 5e3 + "px", c.divCorner.innerHTML = this.cornerHtml || "", l > 0 && (this.divStretch.style.width = l + "px")
                    }
                }, this.ah = function(e, t) {
                    var i = null,
                        n = this.zc.locale(),
                        t = t || this.cellGroupBy,
                        a = e.start;
                    e.end;
                    switch (t) {
                        case "Hour":
                            i = "Clock12Hours" === c.zc.timeFormat() ? a.toString("h tt", n) : a.toString("H", n);
                            break;
                        case "Day":
                            i = a.toString(n.datePattern);
                            break;
                        case "Week":
                            i = 1 === _.weekStarts() ? a.weekNumberISO() : a.weekNumber();
                            break;
                        case "Month":
                            i = a.toString("MMMM yyyy", n);
                            break;
                        case "Quarter":
                            i = "Q" + Math.floor(a.getMonth() / 3 + 1);
                            break;
                        case "Year":
                            i = a.toString("yyyy");
                            break;
                        case "None":
                            i = "";
                            break;
                        case "Cell":
                            var o = (e.end.ticks - e.start.ticks) / 6e4;
                            i = this.gl(a, o);
                            break;
                        default:
                            throw "Invalid cellGroupBy value"
                    }
                    return i
                }, this.gl = function(e, t) {
                    var i = this.zc.locale(),
                        t = t || this.cellDuration;
                    return t < 60 ? e.toString("mm") : t < 1440 ? "Clock12Hours" === c.zc.timeFormat() ? e.toString("h tt", i) : e.toString("H", i) : t < 10080 ? e.toString("d") : 10080 === t ? 1 === _.weekStarts() ? e.weekNumberISO() : e.weekNumber() : e.toString("MMMM yyyy", i)
                }, this.Yg = function(e) {
                    var t = this.scale;
                    switch (t) {
                        case "Cell":
                            throw "Invalid scale: Cell";
                        case "Manual":
                            throw "Internal error (addScaleSize in Manual mode)";
                        case "Minute":
                            return e.addMinutes(1);
                        case "CellDuration":
                            return e.addMinutes(this.cellDuration);
                        default:
                            return this._g(e, t)
                    }
                }, this._g = function(e, t) {
                    var i, n = "Days" !== this.viewType ? this.days : 1,
                        a = this.startDate.addDays(n);
                    "Manual" === c.scale && (a = c.$g());
                    var t = t || this.cellGroupBy,
                        o = 60;
                    switch (t) {
                        case "Hour":
                            e.getHours() + e.getMinutes() + e.getSeconds() + e.getMilliseconds() > 0 && (e = e.getDatePart().addHours(e.getHours())), i = e.addHours(1);
                            break;
                        case "Day":
                            i = e.getDatePart().addDays(1);
                            break;
                        case "Week":
                            for (i = e.getDatePart().addDays(1); i.dayOfWeek() !== _.weekStarts();) i = i.addDays(1);
                            break;
                        case "Month":
                            i = e.addMonths(1), i = i.firstDayOfMonth();
                            for (var r = DayPilot.DateUtil.diff(i, e) / 6e4 % o === 0; !r;) i = i.addHours(1), r = DayPilot.DateUtil.diff(i, e) / 6e4 % o === 0;
                            break;
                        case "Quarter":
                            for (i = e.addMonths(1), i = i.firstDayOfMonth(); i.getMonth() % 3;) i = i.addMonths(1);
                            for (var r = DayPilot.DateUtil.diff(i, e) / 6e4 % o === 0; !r;) i = i.addHours(1), r = DayPilot.DateUtil.diff(i, e) / 6e4 % o === 0;
                            break;
                        case "Year":
                            i = e.addYears(1), i = i.firstDayOfYear();
                            for (var r = DayPilot.DateUtil.diff(i, e) / 6e4 % o === 0; !r;) i = i.addHours(1), r = DayPilot.DateUtil.diff(i, e) / 6e4 % o === 0;
                            break;
                        case "None":
                            i = a;
                            break;
                        case "Cell":
                            var l = this.eh(e);
                            i = l.current ? l.current.end : l.past ? l.previous.end : l.next.start;
                            break;
                        default:
                            throw "Invalid cellGroupBy value"
                    }
                    return i.getTime() > a.getTime() && (i = a), i
                }, this.bl = {}, this.bl.timeheadercol = "_timeheadercol", this.bl.timeheadercolInner = "_timeheadercol_inner", this.bl.resourcedivider = "_resourcedivider", this.bl.eventFloat = "_event_float", this.bl.eventFloatInner = "_event_float_inner", this.bl.event = "_event", this.bl.eventInner = "_event_inner", this.bl.eventBar = "_event_bar", this.bl.eventBarInner = "_event_bar_inner", this.bl.eventDelete = "_event_delete", this.bl.eventLine = "_event_line", this.bl.eventMovingSource = "_event_moving_source";
                var E = this.bl;
                this.fl = function(e, t) {
                    var i = this.nav.timeHeader,
                        n = this.timeHeader[t][e],
                        a = t < this.timeHeader.length - 1,
                        o = n.left,
                        r = n.width,
                        l = R.timeHeader(t),
                        s = l.top,
                        d = l.height,
                        h = document.createElement("div");
                    h.style.position = "absolute", h.style.top = s + "px", h.style.left = o + "px", h.style.width = r + "px", h.style.height = d + "px", n.toolTip && (h.title = n.toolTip), h.setAttribute("aria-hidden", "true"), n.cssClass && DayPilot.Util.addClass(h, n.cssClass), h.setAttribute("unselectable", "on"), h.style.KhtmlUserSelect = "none", h.style.MozUserSelect = "none", h.style.webkitUserSelect = "none", h.oncontextmenu = function() {
                        return !1
                    }, h.cell = {}, h.cell.start = n.start, h.cell.end = n.end, h.cell.level = t, h.cell.th = n, h.onclick = this.Mj, h.style.overflow = "hidden", c.timeHeaderTextWrappingEnabled || (h.style.whiteSpace = "nowrap");
                    var u = document.createElement("div");
                    u.setAttribute("unselectable", "on"), n.innerHTML && (u.innerHTML = n.innerHTML), n.backColor && (u.style.background = n.backColor), n.fontColor && (u.style.color = n.fontColor);
                    var f = this.q(E.timeheadercol),
                        v = this.q(E.timeheadercolInner);
                    a && (f = this.q("_timeheadergroup"), v = this.q("_timeheadergroup_inner")), DayPilot.Util.addClass(h, f), DayPilot.Util.addClass(u, v), DayPilot.Util.addClass(h, c.q("_timeheader_cell")), DayPilot.Util.addClass(u, c.q("_timeheader_cell_inner")), DayPilot.list(n.areas).each(function(e) {
                        e.start && (e.left = c.getPixels(new DayPilot.Date(e.start)).left - o, e.end && (e.width = c.getPixels(new DayPilot.Date(e.end)).left - e.left - o))
                    }), h.appendChild(u), DayPilot.Areas.attach(h, n, {
                        "areas": n.areas
                    }), this.t.timeHeader[e + "_" + t] = h, i.appendChild(h)
                }, this.mh = function() {
                    if (c.cellStacking) return void S.calculateEventPositions();
                    for (var e = 0; e < this.rowlist.length; e++) {
                        var t = this.rowlist[e],
                            i = t.getHeight() + t.marginBottom + t.marginTop;
                        t.height !== i && (this.Ch = !0), t.height = i
                    }
                }, this.hg = function() {
                    var e = this.divHeader;
                    if (!e) return !1;
                    for (var t = this.rowlist.length, i = (this.rowHeaderCols ? this.rowHeaderCols.length : 1, 0), n = 0; n < t; n++) {
                        var a = this.rowlist[n];
                        if (!a.hidden) {
                            var o = this.Aj ? n : i;
                            if (e.rows[o]) {
                                for (var r = 0; r < e.rows[o].cells.length; r++) {
                                    var l = e.rows[o].cells[r];
                                    this.Aj && (l.style.top = a.top + "px");
                                    var s = a.height;
                                    l && l.firstChild && parseInt(l.firstChild.style.height, 10) !== s && (l.firstChild.style.height = s + "px")
                                }
                                i++
                            }
                        }
                    }
                    this.Aj && c.nav.resScrollSpace && (c.nav.resScrollSpace.style.top = c.Zf + "px")
                }, this.dd = function(e) {
                    var t = this.separators[e];
                    t.location = t.location || t.Location, t.color = t.color || t.Color, t.layer = t.layer || t.Layer, t.width = t.width || t.Width, t.opacity = t.opacity || t.Opacity;
                    var i = new DayPilot.Date(t.location),
                        n = t.color,
                        a = t.width ? t.width : 1,
                        o = !!t.layer && "AboveEvents" === t.layer,
                        r = t.opacity ? t.opacity : 100;
                    if (!(i.getTime() < this.Da().getTime() || i.getTime() >= this.$g().getTime())) {
                        var l = this.getPixels(i);
                        if (!l.cut && !(l.left < 0 || l.left > this.cj() * this.cellWidth)) {
                            var s = document.createElement("div");
                            s.style.width = a + "px", s.style.height = c.Zf + "px", s.style.position = "absolute", s.style.left = l.left - 1 + "px", s.style.top = "0px", s.style.backgroundColor = n, s.style.opacity = r / 100, s.style.filter = "alpha(opacity=" + r + ")", o ? this.divSeparatorsAbove.appendChild(s) : this.divSeparators.appendChild(s), this.elements.separators.push(s)
                        }
                    }
                }, this.Lk = function(e) {
                    if ("Disabled" === c.timeRangeDoubleClickHandling) return !1;
                    n.timeRangeTimeout && (clearTimeout(n.timeRangeTimeout), n.timeRangeTimeout = null);
                    if (!c.coords) {
                        var t = c.vg;
                        c.coords = DayPilot.mo3(t, e)
                    }
                    if (e = e || window.event, e.stopPropagation && e.stopPropagation(), e.cancelBubble = !0, c.hl(c.coords)) {
                        var i = c.Jh(c.rangeHold);
                        c.db(i.start, i.end, i.resource)
                    } else if (n.range = c.al(), n.range) {
                        n.rangeCalendar = c;
                        var i = c.Jh(n.range);
                        c.db(i.start, i.end, i.resource), c.rangeHold = n.range
                    }
                }, this.Ik = function(e) {
                    if (!DayPilot.Global.touch.start && !DayPilot.Global.touch.active) {
                        if (n.timeRangeTimeout, 1, c.il(), c.Gk(), !c.coords) {
                            var t = c.vg;
                            c.coords = DayPilot.mo3(t, e)
                        }
                        if (n.rectangleSelect) return !1;
                        e = e || window.event;
                        var i = DayPilot.Util.mouseButton(e);
                        if (i.middle || i.right && c.hl(c.coords)) return !1;
                        if (c.hl(c.coords)) return !1;
                        var a = e.ctrlKey,
                            o = e.metaKey,
                            r = e.shiftKey,
                            l = a || o,
                            s = {};
                        if (s.action = "None", "Disabled" !== c.multiSelectRectangle && r ? s.action = "RectangleSelect" : "Disabled" !== c.timeRangeSelectedHandling && (s.action = "TimeRangeSelect"), s.shift = r, s.ctrl = r, s.meta = o, s.originalEvent = e, s.preventDefault = function() {
                                s.action = "None"
                            }, "function" == typeof c.onGridMouseDown && c.onGridMouseDown(s), "None" === s.action) return e.preventDefault(), e.stopPropagation(), !1;
                        if ("RectangleSelect" === s.action) {
                            var d = {};
                            return d.start = c.coords, d.calendar = c, n.rectangleSelect = d, !1
                        }
                        c.allowMultiRange && !l && H.clear();
                        var h = c.gi(c.coords.y).i;
                        return !c.rowlist[h].isNewRow && (n.range = c.al(), n.range && (n.range.ctrl = l, n.rangeCalendar = c), !1)
                    }
                }, this.al = function() {
                    var e = {},
                        t = this.ch(c.coords.x).x;
                    return e.start = {
                        y: c.gi(c.coords.y).i,
                        x: t
                    }, e.end = {
                        x: t
                    }, this.mi(c.gi(c.coords.y).i) ? null : (e.calendar = c, c.Lh(e), e)
                }, this.pj = function() {
                    c.Kc();
                    var e = n.resizingShadow,
                        t = n.resizing;
                    ! function() {
                        var i = c.jl,
                            a = {
                                "start": e.start,
                                "end": c.mf(e.end)
                            };
                        e.original = a;
                        var o = e.original;
                        if (!o || !i || i.start !== o.start || i.end !== c.mf(o.end)) {
                            var r = {};
                            r.start = a.start, r.end = a.end, r.duration = new DayPilot.Duration(r.start, r.end), r.e = t.event, r.allowed = !0, r.resizing = "left" === n.resizing.dpBorder ? "start" : "end", r.left = {}, r.left.html = r.start.toString(c.eventResizingStartEndFormat, _.locale()), r.left.enabled = c.eventResizingStartEndEnabled, r.left.space = 5, r.left.width = null, r.right = {}, r.right.html = r.end.toString(c.eventResizingStartEndFormat, _.locale()), r.right.enabled = c.eventResizingStartEndEnabled, r.right.space = 5, r.right.width = null, r.multiresize = DayPilot.list(b.list);
                            var l = {};
                            l.event = t, l.start = r.start, l.end = r.end, r.multiresize.splice(0, 0, l), c.jl = a, "function" == typeof c.onEventResizing && c.onEventResizing(r), e.allowed = r.allowed;
                            var s = r.start,
                                d = c.Me(r.end);
                            if (e.finalStart = s, e.finalEnd = d, "Days" === c.viewType) {
                                var h = t.event.part.dayIndex,
                                    u = c.rowlist[h],
                                    f = u.start.getTime() - c.Da().getTime();
                                s = s.addTime(-f), d = d.addTime(-f)
                            }
                            var v = DayPilot.DateUtil.diff(s, d);
                            v = Math.max(v, 1);
                            var p = _.useBox(v),
                                g = p ? c.getPixels(s).boxLeft : c.getPixels(s).left,
                                m = p ? c.getPixels(d).boxRight : c.getPixels(d).left;
                            e.style.left = g + "px", e.style.width = m - g + "px", e.left = g, e.width = m - g, c.qj(n.resizingShadow, r), c.rj(n.resizingShadow, r)
                        }
                    }()
                }, this.Jk = function(e) {
                    if (n.rectangleSelect) {
                        var e = e || window.event;
                        return e.cancelBubble = !0, e.preventDefault && e.preventDefault(), n.gMouseUp(e), !1
                    }
                    if (A = {}, c.rangeHold) {
                        if (DayPilot.Util.mouseButton(e).left) {
                            var t = c.rangeHold;
                            if (c.hl(c.coords)) {
                                var i = function(e) {
                                    return function() {
                                        n.timeRangeTimeout = null;
                                        var t = c.Jh(e);
                                        if (t) {
                                            var i = {};
                                            i.start = t.start, i.end = t.end, i.resource = t.resource, i.preventDefault = function() {
                                                i.preventDefault.value = !0
                                            }, "function" == typeof c.onTimeRangeClick && c.onTimeRangeClick(i), i.preventDefault.value || "function" == typeof c.onTimeRangeClicked && c.onTimeRangeClicked(i)
                                        }
                                    }
                                };
                                "Disabled" != c.timeRangeClickHandling && ("Disabled" === c.timeRangeDoubleClickHandling ? i(t)() : (clearTimeout(n.timeRangeTimeout), n.timeRangeTimeout = setTimeout(i(t), c.doubleClickTimeout)))
                            }
                        }
                    }
                }, this.kl = function() {
                    return n.resizing || n.moving || n.range
                }, this.tj = function(e) {
                    if (!DayPilot.Global.touch.active) {
                        n.activeCalendar = c, e = e || window.event;
                        var t = DayPilot.mc(e);
                        if (c.coords = DayPilot.mo3(c.vg, e), e.insideMainD = !0, window.event && window.event.srcElement && (window.event.srcElement.inside = !0), A.start) {
                            DayPilot.distance(A.originalMouse, t) > 3 && (DayPilot.Util.copyProps(A, n), document.body.style.cursor = "move", A = {})
                        }
                        if (n.resizing && n.resizingEvent.calendar === c) n.resizing.event || (n.resizing.event = n.resizingEvent), c.ll();
                        else if (n.movingEvent && (n.movingEvent.calendar === c || n.movingEvent.calendar.dragOutAllowed)) c.ml();
                        else if (n.range && n.range.calendar === c) n.range.moved = !0, c.nl();
                        else if (s.source) {
                            var i = s.source;
                            v.drawShadow(i.coords, c.coords)
                        } else n.rectangleSelect && (n.rectangleSelect.moved = !0, M.draw());
                        "Disabled" !== c.crosshairType && c.ol(), c.pl();
                        var a = e.insideEvent;
                        if (window.event && window.event.srcElement && (a = window.event.srcElement.insideEvent), c.cellBubble && c.coords && c.rowlist && c.rowlist.length > 0 && !a) {
                            var o = c.ch(c.coords.x).x,
                                r = c.gi(c.coords.y).i;
                            if (0 <= r && r < c.rowlist.length && 0 <= o && o < c.itline.length) {
                                var l = {};
                                l.calendar = c, l.start = c.itline[o].start, l.end = c.itline[o].end, l.resource = c.rowlist[r].id, l.toJSON = function() {
                                    var e = {};
                                    return e.start = this.start, e.end = this.end, e.resource = this.resource, e
                                }, c.cellBubble.showCell(l)
                            }
                        }
                        if (n.drag) {
                            if (c.il(), n.gShadow && document.body.removeChild(n.gShadow), n.gShadow = null, !n.movingShadow && c.coords && c.rowlist.length > 0) {
                                if (!n.movingEvent) {
                                    n.moving = n.drag.schedulerSourceEvent || {};
                                    var h = n.drag.event;
                                    if (h) {
                                        if (c !== h.calendar) {
                                            var f = h.calendar,
                                                p = h,
                                                g = DayPilot.Util.copyProps(h.data);
                                            h = new DayPilot.Event(g), h.calendar = f, h.part.duration = f.Pi(p)
                                        }
                                    } else {
                                        var m = c.itline[0].start,
                                            e = {
                                                "id": n.drag.id,
                                                "start": m,
                                                "end": m.addSeconds(n.drag.duration),
                                                "text": n.drag.text
                                            },
                                            y = n.drag.data;
                                        if (y) {
                                            var b = ["duration", "element", "remove", "duration", "id", "text"];
                                            for (var w in y) DayPilot.contains(b, w) || (e[w] = y[w])
                                        }
                                        h = new DayPilot.Event(e), h.calendar = c, h.part.duration = n.drag.duration
                                    }
                                    h.part.external = !0;
                                    var i = n.drag.schedulerSourceEvent;
                                    i && i.event && i.event.calendar === c && (h.part.external = !1), n.movingEvent = h
                                }
                                n.movingShadow = c.wa(n.movingEvent)
                            }
                            e.cancelBubble = !0
                        }
                        if ("Always" === c.autoScroll || "Drag" === c.autoScroll && (n.moving || n.resizing || n.range)) {
                            var D = c.nav.scroll,
                                k = {
                                    x: c.coords.x,
                                    y: c.coords.y
                                };
                            k.x -= D.scrollLeft, k.x += u.shiftX, k.y -= D.scrollTop;
                            var x = D.clientWidth,
                                P = D.clientHeight,
                                C = 30,
                                S = k.x < C ? k.x : 0,
                                T = x - k.x < C ? x - k.x : 0,
                                E = k.y < C ? k.y : 0,
                                H = P - k.y < C ? P - k.y : 0,
                                o = 0,
                                r = 0,
                                _ = 50;
                            S && (o = -50 * d(S, C)), T && (o = _ * d(T, C)), E && (r = -50 * d(E, C) / 2), H && (r = _ * d(E, C) / 2), (n.resizing || n.range) && (r = 0), o || r ? c.ql(o, r) : c.Gk()
                        }
                    }
                }, this.nl = function() {
                    var e = n.range;
                    e.end = {
                        x: c.ch(c.coords.x).x
                    }, c.Lh(e)
                }, this.ll = function() {
                    n.resizingShadow || (n.resizingShadow = c.wa(n.resizing)), c.oj()
                }, this.ml = function() {
                    if (n.movingShadow && n.movingShadow.calendar !== c && (n.movingShadow.calendar = null, DayPilot.de(n.movingShadow), n.movingShadow = null), !n.movingShadow) {
                        window.navigator.userAgent.indexOf("Chrome/61.0") > -1 || DayPilot.Util.addClass(n.moving, c.q(E.eventMovingSource));
                        var e = n.movingEvent;
                        n.movingShadow = c.wa(e)
                    }
                    c.Yi(), c.$i()
                }, this.rl = {};
                var M = this.rl;
                M.start = function() {
                    var e = {};
                    e.start = c.coords, e.calendar = c, n.rectangleSelect = e
                }, M.draw = function() {
                    var e = n.rectangleSelect,
                        t = n.rectangleSelect.start,
                        i = c.coords;
                    M.clear();
                    var a, o, r, l, s, d, u, f, v = {
                        "x": e.x,
                        "y": e.y,
                        "width": e.width,
                        "height": e.height
                    };
                    if (function() {
                            s = Math.min(t.x, i.x), u = Math.max(t.x, i.x), "Free" === c.multiSelectRectangle ? (d = Math.min(t.y, i.y), f = Math.max(t.y, i.y)) : "Row" === c.multiSelectRectangle && (d = t.y, f = t.y)
                        }(), "Free" === c.multiSelectRectangle) a = s, o = d, r = u - s, l = f - d;
                    else {
                        if ("Row" !== c.multiSelectRectangle) throw "Invalid DayPilot.Scheduler.multiSelectRectangle value: " + c.multiSelectRectangle;
                        var p = c.gi(d).i,
                            g = c.rowlist[p];
                        o = g.top, l = g.height;
                        a = c.ch(s).cell.left;
                        var m = c.ch(u).cell;
                        r = m.left + m.width - a
                    }
                    var y = DayPilot.Util.div(c.divRectangle, a, o, r, l);
                    if (y.style.boxSizing = "border-box", y.style.backgroundColor = "#0000ff", y.style.border = "1px solid #000033", y.style.opacity = .4, e.x = a, e.y = o, e.width = r, e.height = l, (e.x !== v.x || e.y !== v.y || e.width !== v.width || e.height !== v.height) && "function" == typeof c.onRectangleEventSelecting) {
                        var b = {};
                        b.events = h.eventsInRectangle(e.x, e.y, e.width, e.height).map(function(e) {
                            return e.event
                        }), b.start = c.getDate(e.x, !0), b.end = c.getDate(e.x + e.width, !0), c.onRectangleEventSelecting(b)
                    }
                }, M.clear = function() {
                    c.divRectangle.innerHTML = "", c.elements.rectangle = []
                }, this.sl = function() {
                    var e, t;
                    if (!(c.coords && c.rowlist && c.rowlist.length > 0 && c.itline && c.itline.length > 0)) return null;
                    if (e = c.ch(c.coords.x).x, t = c.gi(c.coords.y).i, !(t >= c.rowlist.length)) {
                        return c.cells.findXy(e, t)[0]
                    }
                }, this.pl = function() {
                    var e = this.sl();
                    if (this.hover.cell) {
                        if (this.hover.cell.x === e.x && this.hover.cell.y === e.y) return;
                        this.Hk()
                    }
                    if (this.hover.cell = e, "function" == typeof this.onCellMouseOver) {
                        var t = {};
                        t.cell = e, this.onCellMouseOver(t)
                    }
                }, this.Hk = function() {
                    if (this.hover.cell) {
                        if ("function" == typeof this.onCellMouseOut) {
                            var e = {};
                            e.cell = this.hover.cell, this.onCellMouseOut(e)
                        }
                        this.hover.cell = null
                    }
                }, this.hover = {}, this.ol = function() {
                    var e = this.sl();
                    this.hover.cell && this.hover.cell.x === e.x && this.hover.cell.y === e.y || this.Jc()
                }, this.il = function() {
                    this.divCrosshair.innerHTML = "", this.ul = null, this.vl = null
                }, this.V = function() {
                    if (this.il(), this.wl && this.wl.parentNode && (this.wl.parentNode.removeChild(this.wl), this.wl = null), this.xl) {
                        for (var e = 0; e < this.xl.length; e++) {
                            var t = this.xl[e];
                            t.parentNode && t.parentNode.removeChild(t)
                        }
                        this.xl = null
                    }
                    this.Yf = -1, this.Xf = -1
                }, this.Jc = function() {
                    var e, t;
                    if (c.coords && c.rowlist && c.rowlist.length > 0 && (e = c.ch(c.coords.x).x, t = c.gi(c.coords.y).i, !(t >= c.rowlist.length))) {
                        var i = this.crosshairType,
                            n = c.rowlist[t];
                        if ("Full" === i) {
                            var a = this.itline[e],
                                o = a.left,
                                r = this.ul;
                            if (!r) {
                                var r = document.createElement("div");
                                r.style.height = c.Zf + "px", r.style.position = "absolute", r.style.top = "0px", r.className = c.q("_crosshair_vertical"), this.ul = r, this.divCrosshair.appendChild(r)
                            }
                            r.style.left = o + "px", r.style.width = a.width + "px";
                            var l = n.top,
                                s = n.height,
                                d = this.Jg(),
                                r = this.vl;
                            if (!r) {
                                var r = document.createElement("div");
                                r.style.width = d + "px", r.style.height = s + "px", r.style.position = "absolute", r.style.top = l + "px", r.style.left = "0px", r.className = c.q("_crosshair_horizontal"), this.vl = r, this.divCrosshair.appendChild(r)
                            }
                            r.style.top = l + "px", r.style.height = s + "px"
                        }
                        var h = this.yl(this.coords.x);
                        if (h && this.Yf !== h.x) {
                            this.wl && this.wl.parentNode && (this.wl.parentNode.removeChild(this.wl), this.wl = null);
                            var s = R.timeHeader(c.timeHeaders.length - 1).height,
                                r = document.createElement("div");
                            r.style.width = h.cell.width + "px", r.style.height = s + "px", r.style.left = "0px", r.style.top = "0px", r.style.position = "absolute", r.className = c.q("_crosshair_top"), this.wl = r;
                            var u = this.divNorth,
                                f = this.timeHeader ? this.timeHeader.length - 1 : 1;
                            this.nav.timeHeader ? this.t.timeHeader[h.x + "_" + f].appendChild(r) : u.firstChild.rows[f].cells[e] && u.firstChild.rows[f].cells[e].firstChild.appendChild(r)
                        }
                        if (this.Xf !== t) {
                            if (this.xl) {
                                for (var v = 0; v < this.xl.length; v++) {
                                    var p = this.xl[v];
                                    p.parentNode && p.parentNode.removeChild(p)
                                }
                                this.xl = null
                            }
                            this.rowHeaderCols ? this.rowHeaderCols.length : 1;
                            if (this.xl = [], this.divHeader.rows[t])
                                for (var v = 0; v < this.divHeader.rows[t].cells.length; v++) {
                                    var d = c.Mg(),
                                        r = document.createElement("div");
                                    r.style.width = d + "px", r.style.height = n.height + "px", r.style.left = "0px", r.style.top = "0px", r.style.position = "absolute", r.className = c.q("_crosshair_left"), this.xl.push(r), this.divHeader.rows[t].cells[v].firstChild.appendChild(r)
                                }
                        }
                        h && (this.Yf = h.x), this.Xf = t
                    }
                }, this.fj = function(e, t) {
                    var i = c.timeHeader[e];
                    if (!i) return null;
                    for (var n = 0; n < i.length; n++) {
                        var a = i[n];
                        if (t >= a.left && t < a.left + a.width) {
                            var o = {};
                            return o.cell = a, o.x = n, o
                        }
                    }
                    return null
                }, this.yl = function(e) {
                    return c.fj(c.timeHeader.length - 1, e)
                }, this.Kk = function(e) {
                    if (e = e || window.event, "Disabled" === c.timeRangeSelectedHandling) return !1;
                    if ("Disabled" === c.timeRangeRightClickHandling) return !1;
                    var t = null;
                    if (t = c.hl(c.coords) ? c.Jh(c.rangeHold) : c.Jh()) {
                        t.end = c.mf(t.end);
                        var i = {};
                        return i.start = t.start, i.end = t.end, i.resource = t.resource, i.preventDefault = function() {
                            this.preventDefault.value = !0
                        }, ("function" != typeof c.onTimeRangeRightClick || (c.onTimeRangeRightClick(i), !i.preventDefault.value)) && ("ContextMenu" === c.timeRangeRightClickHandling && c.contextMenuSelection && c.contextMenuSelection.show(t), "function" == typeof c.onTimeRangeRightClicked && c.onTimeRangeRightClicked(i), e.cancelBubble = !0, !!c.allowDefaultContextMenu && void 0)
                    }
                }, this.hl = function(e) {
                    var t = c.rangeHold;
                    if (!t || !t.start || !t.end) return !1;
                    var i = this.ri(t.start.y),
                        n = t.start.x < t.end.x,
                        a = (n ? t.start.x : t.end.x) * this.cellWidth,
                        o = (n ? t.end.x : t.start.x) * this.cellWidth + this.cellWidth,
                        r = i.top,
                        l = i.bottom;
                    return e.x >= a && e.x <= o && e.y >= r && e.y <= l
                }, this.Lh = function(e, t) {
                    function i(e) {
                        var t = e.end.x > e.start.x ? e.start.x : e.end.x,
                            i = e.end.x > e.start.x ? e.end.x : e.start.x,
                            n = e.start.y,
                            a = (c.rowlist[n], c.itline[t]),
                            o = c.itline[i],
                            r = a.left,
                            l = o.left + o.width,
                            s = l - r,
                            d = c.elements.range2;
                        if (!d) {
                            d = document.createElement("div"), d.style.position = "absolute", d.setAttribute("unselectable", "on"), d.className = c.q("_shadow");
                            var h = document.createElement("div");
                            h.className = c.q("_shadow_inner"), d.appendChild(h), c.divRange.appendChild(d)
                        }
                        return d.style.left = a.left + "px", d.style.top = c.rowlist[n].top + "px", d.style.width = s + "px", d.style.height = c.rowlist[n].height - 1 + "px", d.calendar = c, c.elements.range2 = d,
                            function() {
                                var e = c.rowlist[n],
                                    t = a.left;
                                c.lj(d, e, t, s, null)
                            }(), d
                    }
                    var e = e || n.range;
                    e && e.calendar === c && ! function() {
                        var n = e.end.x > e.start.x ? e.start.x : e.end.x,
                            a = e.end.x > e.start.x ? e.end.x : e.start.x,
                            o = e.start.y,
                            r = c.rowlist[o];
                        if (r) {
                            var l = c.itline[n],
                                s = c.itline[a],
                                d = c.zl,
                                h = l.start.addTime(r.data.offset),
                                u = s.end.addTime(r.data.offset);
                            if (t || !d || d.start.getTime() !== h.getTime() || d.end.getTime() !== u.getTime() || d.resource !== c.rowlist[o].id) {
                                var f = {
                                        "start": h,
                                        "end": c.mf(u)
                                    },
                                    v = {};
                                v.start = f.start, v.end = f.end, v.duration = new DayPilot.Duration(f.start, f.end), v.resource = c.rowlist[o].id, v.allowed = !0, v.left = {}, v.left.html = v.start.toString(c.timeRangeSelectingStartEndFormat, _.locale()), v.left.enabled = c.timeRangeSelectingStartEndEnabled, v.left.space = 5, v.left.width = null, v.right = {}, v.right.html = v.end.toString(c.timeRangeSelectingStartEndFormat, _.locale()), v.right.enabled = c.timeRangeSelectingStartEndEnabled, v.right.space = 5, v.right.width = null, c.zl = v, "function" != typeof c.onTimeRangeSelecting || t || c.onTimeRangeSelecting(v);
                                var p = c.Al(e);
                                c.Kh(p, v.start, v.end);
                                var g = i(p);
                                c.qj(g, v), e.cl = p, e.disabled = !v.allowed, c.rj(g, v), v.start = f.start, v.end = f.end
                            }
                        }
                    }()
                }, this.Al = function(e) {
                    return {
                        "start": {
                            "x": e.start.x,
                            "y": e.start.y
                        },
                        "end": {
                            "x": e.end.x
                        },
                        "disabled": e.disabled,
                        "calendar": e.calendar
                    }
                }, this.range = {}, this.range.all = function() {
                    return H.get()
                }, this.Bl = {};
                var H = this.Bl;
                this.Bl.list = [], this.Bl.clear = function() {
                    DayPilot.de(DayPilot.list(H.list).map(function(e) {
                        return e.div
                    })), H.list = []
                }, this.Bl.add = function(e) {
                    e.div = c.elements.range2, c.elements.range2 = null, c.sj(), H.list.push(e)
                }, this.Bl.get = function() {
                    return DayPilot.list(H.list).map(function(e) {
                        return c.Jh(e)
                    })
                }, this.Bl.dispatch = function() {
                    if (c.allowMultiRange && !n.range) {
                        var e = DayPilot.list(H.list).last();
                        c.Ih(e)
                    }
                }, this.Cl = function(e) {
                    if ("Disabled" === c.timeRangeSelectedHandling) return !1;
                    if (!DayPilot.Global.touch.active && !DayPilot.Global.touch.start) {
                        e = e || window.event;
                        var t = DayPilot.Util.mouseButton(e);
                        if (!n.range && !n.rectangleSelect && !(c.rangeHold && c.hl(c.coords) && (t.right || t.middle) || c.mi(c.gi(c.coords.y).i))) {
                            var i = {},
                                a = c.ch(c.coords.x).x;
                            i.start = {
                                y: c.gi(c.coords.y).i,
                                x: a
                            }, i.end = {
                                x: a
                            }, i.calendar = c, c.rangeHold = i, c.Ih(i)
                        }
                    }
                }, this.timeouts = {}, this.timeouts.drawEvents = null, this.timeouts.drawCells = null, this.timeouts.click = null, this.timeouts.resClick = [], this.timeouts.updateFloats = null, this.rg = function(e) {
                    c._f();
                    var t = c.nav.scroll;
                    if (c.dj = t.scrollLeft, c.bj = t.scrollTop, c.ej = t.clientWidth, c.divTimeScroll.scrollLeft = c.dj, c.divResScroll.scrollTop = c.bj, u.isEnabled() && c.A && "Auto" != c.cellWidthSpec)
                        if (u.active) a();
                        else {
                            if (c.nav.scroll.scrollLeft < c.infiniteScrollingMargin) return u.active = !0, c.nav.scroll.style.overflowX = "hidden", void setTimeout(function() {
                                u.shiftStart(-c.infiniteScrollingStepDays)
                            }, 100);
                            if (c.nav.scroll.scrollWidth - (c.nav.scroll.scrollLeft + c.nav.scroll.clientWidth) < c.infiniteScrollingMargin) return u.active = !0, c.nav.scroll.style.overflowX = "hidden", void setTimeout(function() {
                                u.shiftStart(c.infiniteScrollingStepDays)
                            }, 100)
                        } if (c.navigatorBackSync && ! function() {
                            var e = DayPilot.Util.evalVariable(c.navigatorBackSync),
                                t = c.getViewPort().start;
                            e.select(t, {
                                "dontNotify": !0,
                                "dontFocus": !0
                            })
                        }(), c.dynamicLoading) return c.Df(), void c.Dl();
                    if (c.timeouts.drawEvents && (clearTimeout(c.timeouts.drawEvents), c.timeouts.drawEvents = null), c.scrollDelayEvents > 0 ? c.timeouts.drawEvents = setTimeout(c.El(), c.scrollDelayEvents) : c.qa(), c.timeouts.drawCells && (clearTimeout(c.timeouts.drawCells), c.timeouts.drawCells = null), c.scrollDelayCells > 0) c.timeouts.drawCells = setTimeout(c.Fl(), c.scrollDelayCells);
                    else {
                        c.Fl()()
                    }
                    c.timeouts.updateFloats && (clearTimeout(c.timeouts.updateFloats), c.timeouts.updateFloats = null), c.scrollDelayFloats > 0 ? c.timeouts.updateFloats = setTimeout(function() {
                        c.pg()
                    }, c.scrollDelayFloats) : c.pg(), c.onScrollCalled = !0
                }, this.Fl = function() {
                    return function() {
                        c && (c.Df(), c.lg())
                    }
                }, this.El = function() {
                    return function() {
                        c && (c.Uk() ? window.setTimeout(function() {
                            c.Rk(), window.setTimeout(function() {
                                c.qa(!0)
                            }, 50)
                        }, 50) : c.Dg())
                    }
                }, this._f = function() {
                    this.t.eventHeight = null, this.t.drawArea = null
                }, this.show = function() {
                    this.visible = !0, this.Wf = !0, c.nav.top.style.display = "", this.pa(), this.gg(), this.Ei(), c.rg()
                }, this.hide = function() {
                    this.visible = !1, this.Wf = !1, c.nav.top.style.display = "none"
                }, this.Gl = function() {}, this.Dl = function() {
                    var e = c.nav.scroll;
                    c.dj = e.scrollLeft, c.bj = e.scrollTop, c.ej = e.clientWidth, c.divTimeScroll.scrollLeft = c.dj, c.divResScroll.scrollTop = c.bj, c.refreshTimeout && window.clearTimeout(c.refreshTimeout);
                    var t = c.scrollDelayDynamic;
                    c.refreshTimeout = window.setTimeout(c.Hl(e.scrollLeft, e.scrollTop), t), c.pg()
                }, this.Il = function(e) {
                    if (!c.events.list) return null;
                    for (var t = (e.id, new DayPilot.Date(e.start).toString(), 0); t < this.events.list.length; t++) {
                        var i = this.events.list[t];
                        if (c.vi(i, e)) {
                            var n = {};
                            return n.ex = i, n.index = t, n.modified = !c.Jl(e, i), n
                        }
                    }
                    return null
                }, this.Jl = function(e, t) {
                    for (var i in e)
                        if (("object" != typeof e[i] || e[i] instanceof DayPilot.Date) && e[i] !== t[i]) return !1;
                    for (var i in t)
                        if ("object" != typeof t[i] && e[i] !== t[i]) return !1;
                    return !0
                }, this.ng = function(e, t, i, n) {
                    var a = [];
                    if (i) c.da(e);
                    else {
                        var o = [];
                        DayPilot.list(n).forEach(function(e) {
                            var t = DayPilot.list(c.events.list).find(function(t) {
                                return t.id === e
                            });
                            t && o.push(t)
                        }), o.forEach(function(e) {
                            var t = c.events.jh(e);
                            a = a.concat(t)
                        });
                        for (var r = [], l = 0; l < e.length; l++) {
                            var s = e[l];
                            r[l] = c.Il(s)
                        }
                        for (var l = 0; l < e.length; l++) {
                            var s = e[l],
                                d = r[l],
                                h = d && d.modified,
                                u = !d;
                            if (h) {
                                this.events.list[d.index] = s;
                                var f = c.events.jh(d.ex);
                                a = a.concat(f)
                            } else u && this.events.list.push(s);
                            (h || u) && (a = a.concat(c.events.kh(s)))
                        }
                        c.lh(a), c.mh()
                    }
                    c.fg(), c.la(), i ? (c.O(), c.hg(), c.lg(), c.qa()) : c.nh(a, !1, function() {
                        t && t(), c.Rk(), c.qa()
                    })
                }, this.Hl = function(e, t) {
                    return c.r() ? function() {
                        c.scrollX = e, c.scrollY = t, c.H("Scroll")
                    } : function() {
                        if ("function" == typeof c.onScroll) {
                            var e = function(e) {
                                    var t = function() {
                                        if (c.va() && "function" == typeof c.onAfterRender) {
                                            var e = {};
                                            e.isCallBack = !1, e.isScroll = !0, e.data = null, c.onAfterRender(e)
                                        }
                                    };
                                    c.ng(e.events, t, e.clearEvents, e.remove)
                                },
                                t = {};
                            t.viewport = c.getViewPort(), t.async = !1, t.events = [], t.remove = [], t.clearEvents = !1, t.loaded = function() {
                                this.async && e(t)
                            }, c.onScroll(t), t.async || e(t.events, t.clearEvents)
                        }
                    }
                }, this.Kl = function() {
                    var e = this.Ag(),
                        t = e.xStart,
                        i = e.xEnd - e.xStart,
                        n = e.yStart,
                        a = e.yEnd - e.yStart;
                    this.cellProperties || (this.cellProperties = {});
                    for (var o = 0; o <= i; o++) {
                        for (var r = t + o, l = 0; l < a; l++) {
                            var s = n + l;
                            this.rowlist[s].hidden || this.hf(r, s)
                        }
                        this.Ll(r)
                    }
                    for (var d = this.wj(), s = d.start; s < d.end; s++) this.rowlist[s].hidden || this.Ml(s)
                }, this.lg = function() {
                    if (!c.Kd) {
                        if (c.progressiveRowRendering && this.uh(), null !== this.rowlist && this.rowlist.length > 0) {
                            if (this.cellSweeping) {
                                var e = this.cellSweepingCacheSize;
                                this.Tk(e)
                            }
                            this.Kl(), this.Nl(), x.Sj()
                        }
                        this.Ch = !1
                    }
                }, this.Nl = function() {
                    for (var e = this.Ag(), t = e.xStart; t < e.xEnd; t++) {
                        t < this.itline.length - 1 && this.itline[t + 1].breakBefore && this.Ol(t)
                    }
                }, this.Ol = function(e) {
                    var t = "x" + e;
                    if (!this.t.breaks[t]) {
                        var i = this.itline[e + 1].left - 1,
                            n = this.Zf,
                            a = document.createElement("div");
                        a.style.left = i + "px", a.style.top = "0px", a.style.width = "1px", a.style.height = n + "px", a.style.fontSize = "1px", a.style.lineHeight = "1px", a.style.overflow = "hidden", a.style.position = "absolute", a.setAttribute("unselectable", "on"), a.className = this.q("_matrix_vertical_break"), this.divBreaks.appendChild(a), this.elements.breaks.push(a), this.t.breaks[t] = a
                    }
                }, this.Ag = function() {
                    if (c.t.drawArea) return c.t.drawArea;
                    if (!this.nav.scroll) return null;
                    var e = c.bj,
                        t = {},
                        i = null != this.dynamicEventRenderingMarginX ? this.dynamicEventRenderingMarginX : this.dynamicEventRenderingMargin,
                        n = null != this.dynamicEventRenderingMarginY ? this.dynamicEventRenderingMarginY : this.dynamicEventRenderingMargin,
                        a = c.dj - i - u.shiftX,
                        o = a + c.ej + 2 * i,
                        r = 0,
                        l = 0;
                    if (c.itline && c.itline.length > 0) {
                        r = c.ch(a).x, l = c.ch(o, !0).x;
                        var s = this.cj();
                        l = Math.min(l, s), r = Math.max(r, 0)
                    }
                    var d = e - n,
                        h = e + this.nav.scroll.offsetHeight + 2 * n,
                        f = this.gi(d).i,
                        v = this.gi(h).i;
                    return v < this.rowlist.length && v++, t.xStart = r, t.xEnd = l, t.yStart = f, t.yEnd = v, t.pixels = {}, t.pixels.left = this.nav.scroll.scrollLeft - u.shiftX, t.pixels.right = this.nav.scroll.scrollLeft - u.shiftX + this.nav.scroll.clientWidth, t.pixels.top = this.nav.scroll.scrollTop, t.pixels.bottom = this.nav.scroll.scrollTop + this.nav.scroll.clientHeight, t.pixels.width = this.nav.scroll.scrollWidth, c.t.drawArea = t, t
                }, this.Jg = function() {
                    var e = 0,
                        t = this.itline[this.itline.length - 1];
                    return e = t ? t.left + t.width : 0, (e < 0 || isNaN(e)) && (e = 0), e
                }, this.Ml = function(e) {
                    var t = "y" + e;
                    if (!this.t.linesHorizontal[t]) {
                        var i = this.rowlist[e].top + this.rowlist[e].height - 1,
                            n = this.Jg(),
                            a = document.createElement("div");
                        a.style.left = "0px", a.style.top = i + "px", a.style.width = n + "px", a.style.height = "1px", a.style.fontSize = "1px", a.style.lineHeight = "1px", a.style.overflow = "hidden", a.style.position = "absolute", a.setAttribute("unselectable", "on"), a.className = this.q("_matrix_horizontal_line"), this.divLines.appendChild(a), this.t.linesHorizontal[t] = a
                    }
                }, this.Ll = function(e) {
                    var t = this.itline[e];
                    if (t) {
                        var i = "x" + e;
                        if (!this.t.linesVertical[i]) {
                            var n = t.left + t.width - 1,
                                a = document.createElement("div");
                            a.style.left = n + "px", a.style.top = "0px", a.style.width = "1px", a.style.height = c.Zf + "px", a.style.fontSize = "1px", a.style.lineHeight = "1px", a.style.overflow = "hidden", a.style.position = "absolute", a.setAttribute("unselectable", "on"), a.className = this.q("_matrix_vertical_line"), this.divLines.appendChild(a), this.elements.linesVertical.push(a), this.t.linesVertical[i] = a
                        }
                    }
                }, this.Zi = function(e) {
                    var t = this.rowlist[e],
                        i = !t.expanded;
                    t.expanded = i, t.resource && (t.resource.expanded = i);
                    var n = this.Pl(e, t.expanded);
                    if (!i)
                        for (var a = 0; a < n.length; a++) {
                            var o = n[a];
                            this.Dh(o)
                        }
                    if (this.fg(), this.dg(), this.la(), this._f(), i) {
                        for (var a = 0; a < n.length; a++) {
                            var o = n[a];
                            this.Eh(o)
                        }
                        this.Dg()
                    }
                    this.Fh(), v.load(), this.ig(), this.lg(), this.qa(), this.Df(), this._f();
                    var r = this.Ph(t, e);
                    i ? this.Yh(r) : this.Xh(r)
                }, this.Lj = function(e) {
                    var t = {};
                    if (t.index = e, "function" == typeof this.onLoadNode) {
                        var i = {},
                            n = this.rowlist[e].resource;
                        i.resource = n, i.async = !1, i.loaded = function() {
                            this.async && (n.dynamicChildren = !1, n.expanded = !0, c.update())
                        }, this.onLoadNode(i), i.async || (n.dynamicChildren = !1, n.expanded = !0, this.update())
                    } else this.H("LoadNode", t)
                }, this.Pl = function(e, t) {
                    var i = this.rowlist[e],
                        n = [];
                    if (null === i.children || 0 === i.children.length) return n;
                    for (var a = 0; a < i.children.length; a++) {
                        var o = i.children[a],
                            r = this.rowlist[o],
                            l = t && i.expanded && !r.ik;
                        r.hidden = !l, t === !r.hidden && n.push(o);
                        var s = this.Pl(o, t);
                        s.length > 0 && (n = n.concat(s))
                    }
                    return n
                }, this.ql = function(e, t) {
                    this.Gk(), this.Ql(e, t)
                }, this.Rl = function(e) {
                    if (!e) return !1;
                    var t = this.nav.scroll.scrollWidth,
                        i = this.nav.scroll.scrollLeft,
                        n = this.nav.scroll.clientWidth,
                        a = i + n;
                    return !(e < 0 && i <= 0) && (!(e > 0 && a >= t) && (this.nav.scroll.scrollLeft += e, c.coords.x += e, c.Sl(), !0))
                }, this.Tl = function(e) {
                    if (!e) return !1;
                    var t = this.nav.scroll.scrollHeight,
                        i = this.nav.scroll.scrollTop,
                        n = this.nav.scroll.clientHeight,
                        a = i + n;
                    return !(e < 0 && i <= 0) && (!(e > 0 && a >= t) && (this.nav.scroll.scrollTop += e, c.coords.y += e, c.Sl(), !0))
                }, this.Sl = function() {
                    n.resizing && n.resizing.event.calendar === c ? c.ll() : n.moving && (n.movingEvent.calendar === c || n.movingEvent.calendar.dragOutAllowed) ? c.ml() : n.range && n.range.calendar === c && c.nl()
                }, this.Ql = function(e, t) {
                    if (this.Rl(e) || this.Tl(t)) {
                        var i = function(e, t) {
                            return function() {
                                c.Ql(e, t)
                            }
                        };
                        this.Ul = window.setTimeout(i(e, t), 100)
                    }
                }, this.Gk = function() {
                    c.Ul && (window.clearTimeout(c.Ul), c.Ul = null)
                }, this.fg = function() {
                    for (var e = 0, t = 0; t < this.rowlist.length; t++) {
                        var i = this.rowlist[t];
                        i.hidden || (i.top = e, e += i.height)
                    }
                    this.Zf = e
                }, this.ig = function() {
                    DayPilot.browser.ie && c.Vl(), this.elements.cells = [], this.elements.breaks = [], this.t.cells = [], this.t.breaks = [], this.divCells.innerHTML = "", this.divBreaks.innerHTML = "", c.Ej()
                }, this.Ej = function() {
                    this.divLines.innerHTML = "", this.t.linesVertical = {}, this.t.linesHorizontal = {}, this.elements.linesVertical = []
                }, this.Vl = function(e) {
                    var t = DayPilot.list();
                    for (var i in c.t.cells) t.push(c.t.cells[i]);
                    t.each(function(e) {
                        c.Gi(e)
                    })
                }, this.Gh = function(e) {
                    var t = DayPilot.list();
                    for (var i in c.t.cells) t.push(c.t.cells[i]);
                    t.filter(function(t) {
                        return t && t.coords && t.coords.y === e
                    }).each(function(e) {
                        c.Gi(e)
                    })
                }, this.Wl = 0, this.Xl = DayPilot.list(["html", "cssClass", "backColor", "backImage", "backRepeat", "areas"]), this.Yl = c.Xl.add("business"), this.hf = function(e, t) {
                    if (this.A) {
                        var i = c.gh(e);
                        if (i) {
                            var n = e + "_" + t;
                            if (!this.t.cells[n]) {
                                var a = this.Hi(e, t);
                                if (c.hj(e, t), !this.drawBlankCells) {
                                    var o = !1;
                                    if (this.Wi(t) ? o = !1 : this.Zl(a, c.Xl) || (o = !0), o) return
                                }
                                var r = document.createElement("div");
                                if (r.style.left = i.left + "px", r.style.top = this.rowlist[t].top + "px", r.style.width = i.width + "px", r.style.height = this.rowlist[t].height + "px", r.style.position = "absolute", a && a.backColor && (r.style.backgroundColor = a.backColor), r.setAttribute("unselectable", "on"), r.className = this.q("_cell"), r.coords = {}, r.coords.x = e, r.coords.y = t, this.Wi(t) && DayPilot.Util.addClass(r, this.q("_cellparent")), a) {
                                    a.cssClass && DayPilot.Util.addClass(r, a.cssClass), DayPilot.Util.isNullOrUndefined(a.html) || (r.innerHTML = a.html),
                                        a.backImage && (r.style.backgroundImage = 'url("' + a.backImage + '")'), a.backRepeat && (r.style.backgroundRepeat = a.backRepeat), a.business && c.cellsMarkBusiness && DayPilot.Util.addClass(r, c.q("_cell_business")), DayPilot.list(a.areas).each(function(e) {
                                            (e.start || e.end) && (e.left = c.getPixels(new DayPilot.Date(e.start)).left - i.left, e.width = c.getPixels(new DayPilot.Date(e.end)).left - e.left - i.left)
                                        });
                                    var l = c.cells.findXy(e, t)[0];
                                    DayPilot.Areas.attach(r, l, {
                                        "areas": a.areas
                                    })
                                }
                                this.divCells.appendChild(r), this.elements.cells.push(r), this.t.cells[n] = r
                            }
                        }
                    }
                }, this.vh = {}, this.Ei = function() {
                    c.$l().each(function(e) {
                        c.Gh(e)
                    }), c.lg()
                }, this.$l = function() {
                    for (var e = DayPilot.list(), t = this.Ag(), i = t.xStart, n = t.xEnd - t.xStart, a = t.yStart, o = t.yEnd - t.yStart, r = 0; r < o; r++)
                        for (var l = a + r, s = 0; s <= n; s++) {
                            var d = i + s;
                            if (!this.rowlist[l].hidden && !c.vh[d + "_" + l]) {
                                e.push(l);
                                break
                            }
                        }
                    return e
                }, this.hj = function(e, t) {
                    if ("function" == typeof this.onBeforeCellRender) {
                        if (c.beforeCellRenderCaching && c.vh[e + "_" + t]) return;
                        c.vh[e + "_" + t] = !0;
                        var i = c.gh(e);
                        if (!i) return;
                        var n = c.cells.findXy(e, t)[0],
                            a = {};
                        return a.cell = n, a.getPixels = function(e) {
                            var e = new DayPilot.Date(e);
                            return c.getPixels(e).left - i.left
                        }, this.onBeforeCellRender(a), DayPilot.Util.copyProps(a.cell, a.cell.properties, c.Yl), a
                    }
                }, this.Zl = function(e, t) {
                    if (t) {
                        for (var i = 0; i < t.length; i++)
                            if (!DayPilot.Util.isNullOrUndefined(e[t[i]])) return !0
                    } else
                        for (var n in e)
                            if (!DayPilot.Util.isNullOrUndefined(e[n])) return !0;
                    return !1
                }, this.clearSelection = function() {
                    this.og(), H.clear()
                }, this.cleanSelection = this.clearSelection, this.selectTimeRange = function(e, t, i, n) {
                    var a = c.yg(i),
                        o = c.eh(e),
                        r = o.current || o.next;
                    if (!r) throw new DayPilot.Exception("Time range selection 'start' out of timeline");
                    var l = c.eh(new DayPilot.Date(t).addSeconds(-1)),
                        s = l.current || l.previous;
                    if (!s) throw new DayPilot.Exception("Time range selection 'end' out of timeline");
                    var d = {};
                    d.start = {
                        y: a.index,
                        x: DayPilot.indexOf(c.itline, r)
                    }, d.end = {
                        x: DayPilot.indexOf(c.itline, s)
                    }, d.calendar = this, c.Lh(d), n || setTimeout(function() {
                        c.Ih(d)
                    }, 0)
                }, this.Hh = function() {
                    DayPilot.de(n.movingShadow), n.movingShadow = null, c.sj(), c.nj.clear(), DayPilot.Global.movingLink && (DayPilot.Global.movingLink.clear(), DayPilot.Global.movingLink = null)
                }, this.og = function() {
                    this.divRange.innerHTML = '<div style="position:absolute; left:0px; top:0px; width:0px; height:0px;"></div>', this.elements.range = [], this.elements.range2 = null, this.sj(), c.rangeHold = null
                }, this.zc = {};
                var _ = this.zc;
                _.clearCache = function() {
                    delete c.t.eventHeight, delete c.t.headerHeight
                }, _.locale = function() {
                    return DayPilot.Locale.find(c.locale)
                }, _.timeFormat = function() {
                    return "Auto" !== c.timeFormat ? c.timeFormat : _.locale().timeFormat
                }, _.weekStarts = function() {
                    if ("Auto" === c.weekStarts) {
                        var e = _.locale();
                        return e ? e.weekStarts : 0
                    }
                    return c.weekStarts || 0
                }, _.rounded = function() {
                    return "Rounded" === c.eventCorners
                }, _.layout = function() {
                    var e = /MSIE 6/i.test(navigator.userAgent);
                    return "Auto" === c.layout ? e ? "TableBased" : "DivBased" : c.layout
                }, _.notifyType = function() {
                    var e;
                    if ("Immediate" === c.notifyCommit) e = "Notify";
                    else {
                        if ("Queue" !== c.notifyCommit) throw "Invalid notifyCommit value: " + c.notifyCommit;
                        e = "Queue"
                    }
                    return e
                }, _.isResourcesView = function() {
                    return "Days" !== c.viewType
                }, _.useBox = function(e) {
                    return "Always" === c.useEventBoxes || "Never" !== c.useEventBoxes && e < 60 * c.Ni() * 1e3
                }, _.eventHeight = function() {
                    if (c.t.eventHeight) return c.t.eventHeight;
                    var e = c.qd("_event_height").height;
                    return e || (e = c.eventHeight), c.t.eventHeight = e, e
                }, _.headerHeight = function() {
                    if (c.t.headerHeight) return c.t.headerHeight;
                    var e = c.qd("_header_height").height;
                    return e || (e = c.headerHeight), c.t.headerHeight = e, e
                }, _.splitterWidth = function() {
                    return c.rowHeaderScrolling ? c.rowHeaderSplitterWidth : 1
                }, this._l = {};
                var R = this._l;
                R.timeHeader = function(e) {
                    var t = {};
                    t.top = 0, t.height = 0;
                    for (var i = _.headerHeight(), n = 0; n <= e; n++) {
                        var a = c.timeHeaders[n];
                        t.top += t.height, t.height = a.height || i
                    }
                    return t
                }, this.Ic = function(e, t) {
                    var i = e + "_" + t;
                    return this.cellProperties && this.cellProperties[i] ? this.cellProperties[i].backColor : null
                }, this.Hi = function(e, t) {
                    var i = e + "_" + t;
                    if (c.cellConfig)
                        for (var n = 0, a = 0; a < c.resources.length; a++)
                            if (!c.resources[a].hidden) {
                                if (n === t) {
                                    t = a;
                                    break
                                }
                                n += 1
                            } if (this.cellProperties || (this.cellProperties = {}), this.cellProperties[i]) return this.cellProperties[i];
                    if (this.Tf && (this.cellProperties[i] = c.bm(e, t)), !this.cellProperties[i]) {
                        var o = c.rowlist[t],
                            r = o.id,
                            l = o.start.getTime() - c.Da().getTime(),
                            s = c.gh(e),
                            d = s.start.addTime(l),
                            h = s.end.addTime(l),
                            u = {};
                        u.start = d, u.end = h, u.resource = r;
                        var f = {};
                        f.business = c.isBusiness(u), this.cellProperties[i] = f
                    }
                    return this.cellProperties[i]
                }, this.cm = function(e, t, i) {
                    var n = t + "_" + i;
                    return this.cellProperties[n] = {}, DayPilot.Util.copyProps(e, this.cellProperties[n], c.Yl), this.cellProperties[n]
                }, this.bm = function(e, t) {
                    if (!this.cellConfig) return null;
                    var i = this.cellConfig,
                        n = this.cellProperties[e + "_" + t];
                    !n && i.vertical && (n = this.cellProperties[e + "_0"]), !n && i.horizontal && (n = this.cellProperties["0_" + t]), !n && i["default"] && (n = i["default"]);
                    var a = {};
                    return DayPilot.Util.copyProps(n, a, c.Yl), a
                }, this.ca = function() {
                    if (!this.Tf && !this.Uf && (this.Uf = !0, this.cellConfig)) {
                        var e = this.cellConfig;
                        if (e.vertical)
                            for (var t = 0; t < e.x; t++) {
                                var i = this.cellProperties[t + "_0"];
                                if (i)
                                    for (var n = 1; n < e.y; n++) this.cm(i, t, n)
                            }
                        if (e.horizontal)
                            for (var n = 0; n < e.y; n++) {
                                var i = this.cellProperties["0_" + n];
                                if (i)
                                    for (var t = 1; t < e.x; t++) this.cm(i, t, n)
                            }
                        if (e["default"])
                            for (var i = e["default"], n = 0; n < e.y; n++)
                                for (var t = 0; t < e.x; t++) this.cellProperties[t + "_" + n] || this.cm(i, t, n)
                    }
                }, this.isBusiness = function(e) {
                    var t = e.start,
                        i = e.end,
                        n = (i.getTime() - t.getTime()) / 6e4;
                    if (n <= 1440 && "Days" !== c.viewType && !c.businessWeekends && (0 === e.start.dayOfWeek() || 6 === e.start.dayOfWeek())) return !1;
                    if (n < 720) {
                        var a = t.getHours();
                        if (a += t.getMinutes() / 60, a += t.getSeconds() / 3600, a += t.getMilliseconds() / 36e5, a < this.businessBeginsHour) return !1;
                        if (this.businessEndsHour >= 24) return !0;
                        if (a >= this.businessEndsHour) return !1
                    }
                    return !0
                }, this._b = function() {
                    switch (c.C) {
                        case "aspnet":
                            if (!this.M()) throw new DayPilot.Exception("ASP.NET WebForms environment required. https://doc.daypilot.org/common/asp-net-webforms-required/");
                            break;
                        case "netmvc":
                            if (!this.backendUrl) throw new DayPilot.Exception("DayPilot.Scheduler.backendUrl required. https://doc.daypilot.org/common/backendurl-required-asp-net-mvc/");
                            break;
                        case "javaxx":
                            if (!this.backendUrl) throw new DayPilot.Exception("DayPilot.Scheduler.backendUrl required. https://doc.daypilot.org/common/backendurl-required-java/")
                    }
                }, this.pa = function() {
                    "hidden" === this.nav.top.style.visibility && (this.nav.top.style.visibility = "visible")
                }, this.Vc = function() {
                    var e = c.nav.top;
                    return !!e && (e.offsetWidth > 0 && e.offsetHeight > 0)
                }, this.Wc = function() {
                    var e = c.Vc;
                    e() || c.Xc || (c.Xc = setInterval(function() {
                        e() && (clearInterval(c.Xc), c.show(), c.yj())
                    }, 100))
                }, this.dm = function(e) {
                    "Parent100Pct" !== this.heightSpec && (this.heightSpec = "Fixed"), this.height = e - (this.Lg() + 2), this.la()
                }, this.setHeight = this.dm, this.yg = function(e) {
                    return DayPilot.list(D.rowcache[e]).first()
                }, this.vd = function() {
                    if (this.id && this.id.tagName) this.nav.top = this.id;
                    else {
                        if ("string" != typeof this.id) throw "DayPilot.Scheduler() constructor requires the target element or its ID as a parameter";
                        if (this.nav.top = document.getElementById(this.id), !this.nav.top) throw "DayPilot.Scheduler: The placeholder element not found: '" + e + "'."
                    }
                }, this.em = function() {
                    this.md(), this.cg(), this.gg(), this.Zc(), this.S(), n.register(this), this._(this.afterRenderData, !1), this.Ok(), this.ta(), this.H("Init")
                }, this.init = function() {
                    return this._b(), this.fm(), this.Wc(), this.Ak(), this
                }, this.fm = function() {
                    if (this.vd(), !this.nav.top.dp) {
                        this.nk();
                        if (this.ud()) return this.em(), this.A = !0, void this._f();
                        this.Zc(), this.S(), n.register(this), this.Ok(), this.ld(), this.A = !0;
                        var angular = c.kd.scope || c.sg.enabled;
                        c.scrollToDate ? c.scrollTo(c.scrollToDate) : c.scrollX || c.scrollY ? c.setScroll(c.scrollX, c.scrollY) : angular || c.rg(), c.scrollToResourceId && (c.scrollToResource(c.scrollToResourceId), c.scrollToResourceId = null);
                        var e = function() {
                            c.scrollY && c.setScroll(c.scrollX, c.scrollY)
                        };
                        window.setTimeout(e, 200), this.messageHTML && window.setTimeout(function() {
                            c.message(c.messageHTML)
                        }, 0), this.ta(), this._f(), this._(this.afterRenderData, !1), c.initEventEnabled && setTimeout(function() {
                            c.H("Init")
                        }), this.yd(), this.ii = !0
                    }
                }, this.zd = null, this.Ad = function(e) {
                    if (e) {
                        var t = {
                            "events": {
                                "preInit": function() {
                                    var e = this.data;
                                    e && (DayPilot.isArray(e.list) ? c.events.list = e.list : c.events.list = e)
                                },
                                "postInit": function() {}
                            },
                            "links": {
                                "preInit": function() {
                                    var e = this.data;
                                    e && (DayPilot.isArray(e.list) ? c.links.list = e.list : c.links.list = e)
                                },
                                "postInit": function() {}
                            },
                            "scrollTo": {
                                "preInit": function() {},
                                "postInit": function() {
                                    this.data && c.scrollTo(this.data, e.scrollToAnimated, e.scrollToPosition)
                                }
                            },
                            "scrollX": {
                                "postInit": function() {
                                    this.data && c.setScrollX(this.data)
                                }
                            },
                            "scrollY": {
                                "postInit": function() {
                                    this.data && c.setScrollY(this.data)
                                }
                            }
                        };
                        c.zd = t, c.sg.scrollToRequested && (t.scrollTo.data = c.sg.scrollToRequested, c.sg.scrollToRequested = null), c.sg.scrollXRequested && (t.scrollX.data = c.sg.scrollXRequested, c.sg.scrollXRequested = null), c.sg.scrollYRequested && (t.scrollY.data = c.sg.scrollYRequested, c.sg.scrollYRequested = null);
                        for (var i in e)
                            if (t[i]) {
                                var n = t[i];
                                n.data = e[i], n.preInit && n.preInit()
                            } else c[i] = e[i]
                    }
                }, this.yd = function() {
                    var e = c.zd;
                    for (var t in e) {
                        var i = e[t];
                        i.postInit && i.postInit()
                    }
                    c.zd = {}
                }, this.temp = {}, this.temp.getPosition = function() {
                    var e = Math.floor(c.coords.x / c.cellWidth),
                        t = c.gi(c.coords.y).i;
                    if (t < c.rowlist.length) {
                        var i = {};
                        return i.start = c.itline[e].start, i.end = c.itline[e].end, i.resource = c.rowlist[t].id, i
                    }
                    return null
                }, this.internal = {}, this.internal.initialized = function() {
                    return c.A
                }, this.internal.dragInProgress = this.kl, this.internal.invokeEvent = this.Sa, this.internal.eventMenuClick = this.ab, this.internal.timeRangeMenuClick = this.bb, this.internal.resourceHeaderMenuClick = this.Nh, this.internal.linkMenuClick = this.Mh, this.internal.bubbleCallBack = this.$a, this.internal.findEventDiv = this.jd, this.internal.rowtools = this.Vh, this.internal.getNodeChildren = this.Y, this.internal.callback = function() {
                    c.H.apply(c, arguments)
                }, this.internal.createRowObject = this.Ph, this.internal.restools = this.bk, this.internal.gantt = null, this.internal.adjustEndIn = c.Me, this.internal.adjustEndNormalize = c.of, this.internal.touch = c.Yb, this.internal.cssNames = c.bl, this.internal.skipUpdate = c.sg.skipUpdate, this.internal.skipped = c.sg.skipped, this.internal.loadOptions = c.Ad, this.internal.postInit = c.yd, this.internal.enableAngular2 = function() {
                    c.sg.enabled = !0
                }, this.Init = this.init, this.Ad(o)
            }, o = "2018.1.3151" === (new DayPilot.Scheduler).v, DayPilot.Row = function(e, t) {
                if (!e) throw "Now row object supplied when creating DayPilot.Row";
                if (!t) throw "No parent control supplied when creating DayPilot.Row";
                var i = DayPilot.indexOf(t.rowlist, e);
                this.gm = {};
                var n = this.gm;
                n.id = e.id, n.name = e.name, n.data = e.resource, n.tags = e.tags;
                var a = this;
                a.isRow = !0, a.menuType = "resource", a.start = e.start, a.name = e.name, a.value = e.id, a.id = e.id, a.tags = e.tags, a.bubbleHtml = e.bubbleHtml, a.index = i, a.level = e.level, a.calendar = t, a.data = e.resource, a.hm = e, a.$ = {}, a.$.row = e, a.toJSON = function(e) {
                    var t = {};
                    return t.start = this.start, t.name = this.name, t.value = this.value, t.id = this.id, t.index = this.index, t
                }, a.parent = function() {
                    return a.hm.jk ? t.Ph(a.hm.jk) : null
                }, a.children = function() {
                    var e = DayPilot.list(a.$.row.children).map(function(e) {
                        return t.Ph(t.rowlist[e])
                    });
                    return e.add = function(e) {
                        var t = a.$.row.resource;
                        t.children || (t.children = []), t.children.push(e)
                    }, e
                }, a.loaded = function() {
                    return a.hm.loaded
                }, a.cells = {}, a.cells.all = function() {
                    for (var e = [], i = t.itline.length, n = a.index, o = 0; o < i; o++) {
                        var r = t.cells.findXy(o, n);
                        e.push(r[0])
                    }
                    return t.Ii(e)
                }, a.cells.totalDuration = function() {
                    return new DayPilot.Duration(a.cells.all().map(function(e) {
                        return e.end.getTime() - e.start.getTime()
                    }).reduce(function(e, t) {
                        return e + t
                    }, 0))
                }, a.events = {}, a.events.all = function() {
                    for (var e = DayPilot.list(), t = 0; t < a.hm.events.length; t++) e.push(a.hm.events[t]);
                    return e
                }, a.events.isEmpty = function() {
                    return 0 === a.hm.events.length
                }, a.events.forRange = function(e, t) {
                    return a.hm.events.forRange(e, t)
                }, a.events.totalDuration = function() {
                    var e = 0;
                    return a.events.all().each(function(t) {
                        e += t.part.end.getTime() - t.part.start.getTime()
                    }), new DayPilot.Duration(e)
                }, a.groups = {}, a.groups.collapseAll = function() {
                    for (var e = 0; e < a.hm.blocks.length; e++) {
                        new o(a.hm.blocks[e]).im()
                    }
                    t.mh(), t.nh(a.index), t.la()
                }, a.groups.expandAll = function() {
                    for (var e = 0; e < a.hm.blocks.length; e++) {
                        new o(a.hm.blocks[e]).jm()
                    }
                    t.mh(), t.nh(a.index), t.la()
                }, a.groups.expanded = function() {
                    for (var e = [], i = 0; i < a.hm.blocks.length; i++) {
                        var n = a.hm.blocks[i];
                        n.expanded && n.lines.length > t.groupConcurrentEventsLimit && e.push(new o(n))
                    }
                    return DayPilot.list(e)
                }, a.groups.collapsed = function() {
                    for (var e = [], t = 0; t < a.hm.blocks.length; t++) {
                        var i = a.hm.blocks[t];
                        i.expanded || e.push(new o(i))
                    }
                    return DayPilot.list(e)
                }, a.groups.all = function() {
                    for (var e = [], t = 0; t < a.hm.blocks.length; t++) {
                        var i = a.hm.blocks[t];
                        e.push(new o(i))
                    }
                    return DayPilot.list(e)
                }, a.events.collapseGroups = a.groups.collapseAll, a.events.expandGroups = a.groups.expandAll, a.column = function(e) {
                    return new r(a, e)
                }, a.toggle = function() {
                    t.Zi(a.index)
                }, a.collapse = function() {
                    a.$.row.expanded && a.toggle()
                }, a.expand = function() {
                    a.$.row.expanded || a.toggle()
                }, a.remove = function() {
                    t.rows.remove(a)
                }, a.edit = function() {
                    t.rows.edit(a)
                }, a.addClass = function(e) {
                    var i = a,
                        n = t.divHeader,
                        i = n.rows[i.index];
                    if (i) {
                        var o = DayPilot.list(i.cells).map(function(e) {
                            return e.cellDiv
                        });
                        DayPilot.Util.addClass(o, e)
                    }
                    a.$.row.cssClass = DayPilot.Util.addClassToString(a.$.row.cssClass, e)
                }, a.removeClass = function(e) {
                    var i = a,
                        n = t.divHeader,
                        i = n.rows[i.index];
                    if (i) {
                        var o = DayPilot.list(i.cells).map(function(e) {
                            return e.cellDiv
                        });
                        DayPilot.Util.removeClass(o, e)
                    }
                    a.$.row.cssClass = DayPilot.Util.removeClassFromString(a.$.row.cssClass, e)
                };
                var o = function(e) {
                        var i = function(e) {
                            for (var i = 0; i < t.elements.events.length; i++) {
                                var n = t.elements.events[i];
                                if (n.event === e) return n
                            }
                            return null
                        };
                        this.jm = function() {
                            e.expanded = !0;
                            var n = t.Oj;
                            DayPilot.list(e.events).each(function(e) {
                                n.push(e.id())
                            });
                            var a = i(e);
                            if (a) {
                                t.Qk(a);
                                var o = DayPilot.indexOf(t.elements.events, a);
                                o !== -1 && t.elements.events.splice(o, 1)
                            }
                        }, this.expand = function() {
                            this.jm(), t.mh(), t.nh(e.row.index), t.la()
                        }, this.im = function() {
                            if (e.lines.length > t.groupConcurrentEventsLimit) {
                                e.expanded = !1;
                                var i = DayPilot.list(e.events).map(function(e) {
                                    return e.id()
                                });
                                t.Oj = t.Oj.filter(function(e) {
                                    return !DayPilot.contains(i, e)
                                })
                            }
                        }, this.collapse = function() {
                            this.im(), t.mh(), t.nh(e.row.index), t.la()
                        }
                    },
                    r = function(e, t) {
                        this.html = function(i) {
                            var n = e.calendar.divHeader,
                                a = n.rows[e.index].cells[t],
                                o = a.textDiv;
                            return "undefined" == typeof i ? o.innerHTML : void(o.innerHTML = i)
                        }
                    }
            }, n.moving = null, n.movingEvent = null, n.originalMouse = null, n.resizing = null, n.resizingEvent = null, n.preventEventClick = !1, n.globalHandlers = !1, n.timeRangeTimeout = null, n.selectedCells = null, n.dragStart = function(e, t, i, a) {
                DayPilot.us(e);
                var o = n.drag = {};
                return o.element = e, o.duration = t, o.text = a, o.id = i, o.data = {
                    "id": i,
                    "text": a,
                    "duration": t,
                    "externalHtml": a
                }, !1
            }, DayPilot.Scheduler.startDragging = function(e) {
                var e = e || {},
                    t = e.element,
                    i = e.keepElement ? null : t;
                DayPilot.us(t);
                var a = n.drag = {};
                return a.element = i, a.id = e.id, a.duration = e.duration || 60, a.text = e.text || "", a.data = e, !1
            }, DayPilot.Scheduler.makeDraggable = function(e) {
                function t() {
                    var t = e.duration || 60;
                    t instanceof DayPilot.Duration && (t = t.totalSeconds());
                    var i = n.drag = {};
                    i.element = a, i.id = e.id, i.duration = t, i.text = e.text || "", i.data = e
                }
                e = e || {};
                var i = e.element,
                    a = e.keepElement ? null : e.remove || i;
                DayPilot.us(i), DayPilot.re(i, "mousedown", function(e) {
                    if (DayPilot.Util.mouseButton(e).left) {
                        t();
                        var i = e.target || e.srcElement;
                        if (i.tagName) {
                            var n = i.tagName.toLowerCase();
                            if ("textarea" === n || "select" === n || "input" === n) return !1
                        }
                        return e.preventDefault && e.preventDefault(), !1
                    }
                }), i.ontouchstart = function(e) {
                    window.setTimeout(function() {
                        t(), n.gTouchMove(e), e.preventDefault()
                    }, 0), e.preventDefault()
                }
            }, DayPilot.Scheduler.stopDragging = function() {
                if (n.drag && n.drag.schedulerSourceEvent) {
                    var e = n.drag.schedulerSourceEvent,
                        t = e.event.calendar;
                    DayPilot.Util.removeClass(e, t.q(i.bl.eventMovingSource))
                }
                if (n.dragStop(), n.resizing && (DayPilot.de(n.resizingShadow), n.resizing = null, n.resizingEvent = null, n.resizingShadow = null), n.moving) {
                    var i = n.movingEvent.calendar;
                    DayPilot.Util.removeClass(n.moving, i.q(i.bl.eventMovingSource)), DayPilot.de(n.movingShadow), n.moving = null, n.movingEvent = null, n.movingShadow = null
                }
                if (n.range) {
                    var i = n.range.calendar;
                    i.clearSelection(), n.range = null
                }
                document.body.style.cursor = "", DayPilot.list(n.registered).each(function(e) {
                    e.Gk(), e.sj(), e.Vh.resetMoving()
                })
            }, n.dragStop = function() {
                n.gShadow && (document.body.removeChild(n.gShadow), n.gShadow = null), n.drag = null
            }, n.register = function(e) {
                n.registered || (n.registered = DayPilot.list(), n.registered.out = function() {
                    n.registered.forEach(function(e) {
                        e.U()
                    })
                });
                for (var t = 0; t < n.registered.length; t++)
                    if (n.registered[t] === e) return;
                n.registered.push(e)
            }, n.unregister = function(e) {
                var t = n.registered;
                if (t) {
                    var i = DayPilot.indexOf(t, e);
                    i !== -1 && t.splice(i, 1), 0 === t.length && (t = null)
                }
                t || (DayPilot.ue(document, "mousemove", n.gMouseMove), DayPilot.ue(document, "mouseup", n.gMouseUp), DayPilot.ue(document, "mousedown", n.gMouseDown), DayPilot.ue(document, "touchmove", n.gTouchMove), DayPilot.ue(document, "touchend", n.gTouchEnd), DayPilot.ue(window, "keyup", n.gKeyUp), n.globalHandlers = !1)
            }, n.gTouchMove = function(i) {
                if (n.resizing) {
                    var a = n.resizing.event.calendar;
                    a.coords = a.Yb.relativeCoords(i), a.Yb.updateResizing(), i.preventDefault()
                }
                if (n.moving && !n.drag) {
                    var a = n.movingEvent.calendar;
                    a.coords = a.Yb.relativeCoords(i), a.Yb.updateMoving(), i.preventDefault(),
                        function() {
                            if (a.dragOutAllowed) {
                                var t = e(i);
                                if (!t || t !== a) {
                                    var o = n.movingEvent;
                                    n.drag = {};
                                    var r = n.drag;
                                    r.id = o.id(), r.text = o.text(), r.schedulerSourceEvent = n.moving, r.element = null, r.duration = (o.rawend().getTime() - o.start().getTime()) / 1e3, r.text = o.text(), r.id = o.id(), r.event = DayPilot.Util.copyProps(o), a.sj(), DayPilot.de(n.movingShadow), n.movingShadow.calendar = null, n.movingShadow = null, n.registered.out()
                                }
                            }
                        }()
                }
                if (n.drag) {
                    i.preventDefault();
                    var o = t(i),
                        a = e(i);
                    if (a) {
                        if (n.gShadow && document.body.removeChild(n.gShadow), n.gShadow = null, a.coords = a.Yb.relativeCoords(i), !n.movingShadow && a.rowlist.length > 0) {
                            if (!n.moving) {
                                n.moving = {};
                                var r = n.drag.event;
                                if (!r) {
                                    var l = a.itline[0].start,
                                        i = {
                                            "id": n.drag.id,
                                            "start": l,
                                            "end": l.addSeconds(n.drag.duration),
                                            "text": n.drag.text
                                        },
                                        s = n.drag.data;
                                    if (s) {
                                        var d = ["duration", "element", "remove", "id", "text"];
                                        for (var c in s) DayPilot.contains(d, c) || (i[c] = s[c])
                                    }
                                    r = new DayPilot.Event(i), r.calendar = a, r.part.duration = n.drag.duration
                                }
                                r.part.external = !0;
                                var h = n.drag.schedulerSourceEvent;
                                h && h.event && h.event.calendar === a && (r.part.external = !1), n.movingEvent = r
                            }
                            n.movingShadow = a.wa(n.movingEvent)
                        }
                        n.moving && a.Yb.updateMoving()
                    } else {
                        DayPilot.de(n.movingShadow);
                        var u = n.moving;
                        n.moving = null, n.movingEvent = null, n.movingShadow = null, n.gShadow || (n.gShadow = n.createGShadow(n.drag.data), n.gShadow.source = u);
                        var f = n.gShadow;
                        f.style.left = o.x + "px", f.style.top = o.y + "px", n.registered.out()
                    }
                }
            }, n.gTouchEnd = function(e) {
                n.gMouseUp(e)
            }, n.gMouseMove = function(e) {
                if ("undefined" != typeof n && !(DayPilot.Global.touch.active || DayPilot.Global.touch.start || (e = e || window.event, e.insideMainD || e.srcElement && e.srcElement.inside))) {
                    var t = DayPilot.mc(e);
                    if (n.drag) {
                        if (!n.drag.startFired) {
                            var i = n.drag.data || {},
                                a = i.onDragStart,
                                o = !1;
                            if ("function" == typeof a) {
                                var r = {};
                                r.data = i, r.preventDefault = function() {
                                    r.preventDefault.value = !0
                                }, a(r), o = r.preventDefault.value
                            }
                            if (n.drag.startFired = !0, o) return void(n.drag = null)
                        }
                        document.body.style.cursor = "move", n.gShadow || (n.gShadow = n.createGShadow(n.drag.data));
                        var l = n.gShadow;
                        if (l.style.left = t.x + "px", l.style.top = t.y + "px", n.moving = null, n.movingEvent = null, n.movingShadow) {
                            var s = n.movingShadow.calendar;
                            s && s.sj(), n.movingShadow.calendar = null, DayPilot.de(n.movingShadow), n.movingShadow = null
                        }
                    } else if (n.moving && n.movingEvent && n.movingEvent.calendar.dragOutAllowed && !n.drag) {
                        var s = n.movingEvent.calendar,
                            e = n.movingEvent;
                        document.body.style.cursor = "move", n.gShadow || (n.gShadow = n.createGShadow(e.data), n.gShadow.source = n.moving);
                        var l = n.gShadow;
                        l.style.left = t.x + "px", l.style.top = t.y + "px", n.drag = {};
                        var d = n.drag;
                        d.schedulerSourceEvent = n.moving, d.element = null, d.duration = (e.rawend().getTime() - e.start().getTime()) / 1e3, d.text = e.text(), d.id = e.value(), d.event = DayPilot.Util.copyProps(e), s.sj(), DayPilot.de(n.movingShadow), n.movingShadow.calendar = null, n.movingShadow = null
                    }
                    n.registered.out()
                }
            }, n.gUnload = function(e) {
                if (n.registered)
                    for (var t = 0; t < n.registered.length; t++) {
                        var i = n.registered[t];
                        n.unregister(i)
                    }
            }, n.gMouseDown = function(e) {
                n.editing && n.editing.blur()
            }, n.gKeyUp = function(e) {
                var e = e || window.event;
                17 === e.keyCode && n.rangeCalendar && n.rangeCalendar.Bl.dispatch()
            }, n.gMouseUp = function(e) {
                if (DayPilot.list(n.registered).each(function(e) {
                        e.Gk()
                    }), n.resizing) {
                    var t = function() {
                        var e = n.resizingEvent,
                            t = e.calendar;
                        document.body.style.cursor = "", n.resizing = null, n.resizingEvent = null, DayPilot.de(n.resizingShadow), n.resizingShadow = null, t.Gl()
                    };
                    if (setTimeout(function() {
                            n.preventEventClick = !1
                        }), !n.resizingShadow) return void t();
                    var i = n.resizingEvent,
                        o = i.calendar,
                        r = n.resizingShadow.finalStart,
                        d = n.resizingShadow.finalEnd,
                        c = n.resizingShadow.overlapping,
                        h = !n.resizingShadow.allowed;
                    if (o.sj(), o.kj.clear(), t(), c || h) return;
                    o.Ua(i, r, d)
                } else if (n.movingEvent) {
                    var t = function() {
                        DayPilot.Global.movingAreaData = null;
                        var e = n.movingShadow && n.movingShadow.calendar;
                        n.movingShadow && (DayPilot.de(n.movingShadow), n.movingShadow.calendar = null), document.body.style.cursor = "", n.moving = null, n.movingEvent = null, n.drag = null, e.Gl(), e && (e.Lc = null)
                    };
                    if (!n.movingShadow) return void t();
                    var i = n.movingEvent,
                        o = n.movingShadow.calendar;
                    if (!o) return void t();
                    if (DayPilot.Util.removeClass(n.moving, o.q(o.bl.eventMovingSource)), function() {
                            if (n.drag && n.drag.schedulerSourceEvent) {
                                var e = n.drag.schedulerSourceEvent,
                                    t = e.event.calendar;
                                DayPilot.Util.removeClass(e, t.q(o.bl.eventMovingSource))
                            }
                        }(), !n.movingShadow.row) return void t();
                    clearTimeout(o.Xi.timeout);
                    var r = n.movingShadow.start,
                        d = n.movingShadow.end,
                        u = "Days" !== o.viewType ? n.movingShadow.row.id : null,
                        f = !!n.drag && i.part.external,
                        v = n.movingShadow.line,
                        c = n.movingShadow.overlapping,
                        h = !n.movingShadow.allowed;
                    if (n.drag && (o.todo || (o.todo = {}), o.todo.del = n.drag.element, n.drag = null), n.movingShadow.calendar = null, document.body.style.cursor = "", n.moving = null, n.movingEvent = null, c || h || o.nj.forbidden || o.nj.invalid) return void o.Hh();
                    var e = e || window.event;
                    o.Xa(i, r, d, u, f, e, v), DayPilot.Global.movingAreaData = null
                } else if (n.range) {
                    e = e || window.event;
                    var p = DayPilot.Util.mouseButton(e),
                        g = n.range,
                        o = g.calendar,
                        t = function() {
                            o.Gl()
                        };
                    o.zl = null;
                    var m = e.ctrlKey || e.metaKey;
                    if (o.allowMultiRange) return o.elements.range2.overlapping ? (DayPilot.de(o.elements.range2), o.elements.range2 = null, o.sj()) : o.Bl.add(g), n.range = null, m || o.Bl.dispatch(), void t();
                    if (n.timeRangeTimeout) return clearTimeout(n.timeRangeTimeout), n.timeRangeTimeout = null, void t();
                    o.rangeHold = g, n.range = null;
                    var y = function(e) {
                            return function() {
                                n.timeRangeTimeout = null;
                                var t = o.elements.range2;
                                return t && t.overlapping ? void o.clearSelection() : (o.Ih(e), void("Hold" !== o.timeRangeSelectedHandling && "HoldForever" !== o.timeRangeSelectedHandling ? a() : o.rangeHold = e))
                            }
                        },
                        b = o.Al(g);
                    if (t(), !p.left) return void(n.timeRangeTimeout = null);
                    if (g.moved || "Disabled" === o.timeRangeDoubleClickHandling) {
                        y(b)();
                        var e = e || window.event;
                        return e.cancelBubble = !0, !1
                    }
                    n.timeRangeTimeout = setTimeout(y(b), o.doubleClickTimeout)
                } else if (l.row) {
                    var o = l.calendar;
                    o && o.Zj()
                } else if (s.source) {
                    var o = s.calendar;
                    o.ni.clearShadow(), o.ni.hideLinkpoints(), s.source = null, s.calendar = null
                } else if (n.splitting) {
                    var w = n.splitting;
                    w.cleanup(), n.splitting = null
                } else if (n.rectangleSelect) {
                    var D = n.rectangleSelect,
                        o = D.calendar;
                    if (!D.moved) return n.rectangleSelect = null, void(o.hl(o.coords) || o.Cl(e));
                    var k = D.x,
                        x = D.y,
                        P = D.width,
                        C = D.height,
                        S = !1,
                        A = o.Eg.eventsInRectangle(k, x, P, C).map(function(e) {
                            return e.event
                        });
                    if ("function" == typeof o.onRectangleEventSelect) {
                        var T = {};
                        if (T.events = A, T.append = !1, T.start = o.getDate(k, !0), T.end = o.getDate(k + P, !0), T.preventDefault = function() {
                                this.preventDefault.value = !0
                            }, o.onRectangleEventSelect(T), T.preventDefault.value) return o.rl.clear(), void(n.rectangleSelect = null);
                        S = T.append
                    }
                    if (S || o.multiselect.clear(!0), A.each(function(e) {
                            o.multiselect.add(e, !0)
                        }), o.multiselect.redraw(), setTimeout(function() {
                            o.rl.clear(), n.rectangleSelect = null
                        }, 0), "function" == typeof o.onRectangleEventSelected) {
                        var T = {};
                        T.events = A, o.onRectangleEventSelected(T)
                    }
                }
                if (n.drag && (n.drag.event && n.drag.event.part && delete n.drag.event.part.external, n.drag = null, document.body.style.cursor = ""), n.gShadow) {
                    if (n.gShadow.source) {
                        var o = n.gShadow.source.event.calendar;
                        DayPilot.Util.removeClass(n.gShadow.source, o.q(o.bl.eventMovingSource))
                    }
                    document.body.removeChild(n.gShadow), n.gShadow = null
                }
                n.moveOffsetX = null, n.moveDragStart = null
            }, n.createGShadow = function(e) {
                var e = e || {},
                    t = document.createElement("div");
                return t.setAttribute("unselectable", "on"), t.style.position = "absolute", t.style.width = "100px", t.style.height = "20px", t.style.zIndex = 101, t.style.pointerEvents = "none", t.style.backgroundColor = "#aaaaaa", t.style.opacity = .5, t.style.filter = "alpha(opacity=50)", t.className = e.externalCssClass, e.externalHtml && (t.innerHTML = e.externalHtml), document.body.appendChild(t), t
            };
            var l = {};
            DayPilot.Global.rowmoving = l;
            var s = {};
            DayPilot.Global.linking = s, "undefined" != typeof jQuery && ! function(e) {
                    e.fn.daypilotScheduler = function(e) {
                        var t = null,
                            i = this.each(function() {
                                if (!this.daypilot) {
                                    var i = new DayPilot.Scheduler(this.id || this, e);
                                    i.init(), this.daypilot = i, t || (t = i)
                                }
                            });
                        return 1 === this.length ? t : i
                    }
                }(jQuery),
                function() {
                    var e = DayPilot.am();
                    e && e.directive("daypilotScheduler", ["$parse", function(e) {
                        return {
                            "restrict": "E",
                            "template": "<div id='{{id}}'></div>",
                            "compile": function(t, i) {
                                return t[0].removeAttribute("id"), t[0].innerHTML = this["template"].replace("{{id}}", i["id"]),
                                    function(t, i, n) {
                                        var a = new DayPilot.Scheduler(i[0].firstChild);
                                        a.kd.scope = t;
                                        var o = n["id"];
                                        o && (t[o] = a);
                                        var r = n["publishAs"];
                                        if (r) {
                                            (0, e(r).assign)(t, a)
                                        }
                                        for (var l in n)
                                            if (0 === l.indexOf("on")) {
                                                var s = DayPilot.Util.shouldApply(l);
                                                s ? ! function(i) {
                                                    a[i] = function(a) {
                                                        var o = e(n[i]);
                                                        DayPilot.Util.safeApply(t, function() {
                                                            o(t, {
                                                                "args": a
                                                            })
                                                        })
                                                    }
                                                }(l) : ! function(i) {
                                                    a[i] = function(a) {
                                                        e(n[i])(t, {
                                                            "args": a
                                                        })
                                                    }
                                                }(l)
                                            } var d = t["$watch"],
                                            c = n["config"] || n["daypilotConfig"],
                                            h = n["events"] || n["daypilotEvents"];
                                        c || a.init(), d.call(t, c, function(e, t) {
                                            a.Ad(e), a.A ? (a.update(), a.yd()) : a.init()
                                        }, !0), h && d.call(t, h, function(e) {
                                            a.events.list = e, a.ld({
                                                "eventsOnly": !0
                                            })
                                        }, !0), t.$on("$destroy", function() {
                                            a.dispose()
                                        })
                                    }
                            }
                        }
                    }])
                }(), "undefined" != typeof Sys && Sys.Application && Sys.Application.notifyScriptLoaded && Sys.Application.notifyScriptLoaded()
        }
    }(), 

"undefined" == typeof DayPilot) var DayPilot = {};


////////////////////////////janek9//////////////////////

