if ("undefined" == typeof DayPilot) var DayPilot = {};
if ("undefined" == typeof DayPilot.Global && (DayPilot.Global = {}), function() {
        function e(e) {
            var t = DayPilot.Date.Cache.Ticks;
            if (t[e]) return DayPilot.Stats.cacheHitsTicks += 1, t[e];
            var i, n = new Date(e),
                a = n.getUTCMilliseconds();
            i = 0 === a ? "" : a < 10 ? ".00" + a : a < 100 ? ".0" + a : "." + a;
            var o = n.getUTCSeconds();
            o < 10 && (o = "0" + o);
            var r = n.getUTCMinutes();
            r < 10 && (r = "0" + r);
            var l = n.getUTCHours();
            l < 10 && (l = "0" + l);
            var s = n.getUTCDate();
            s < 10 && (s = "0" + s);
            var d = n.getUTCMonth() + 1;
            d < 10 && (d = "0" + d);
            var c = n.getUTCFullYear();
            if (c <= 0) throw "The minimum year supported is 1.";
            c < 10 ? c = "000" + c : c < 100 ? c = "00" + c : c < 1e3 && (c = "0" + c);
            var h = c + "-" + d + "-" + s + "T" + l + ":" + r + ":" + o + i;
            return t[e] = h, h
        }

        function t(e, t) {
            return !DayPilot.Util.isNullOrUndefined(e) && (!DayPilot.Util.isNullOrUndefined(t) && e.toLocaleLowerCase() === t.toLocaleLowerCase())
        }

        
        if ("undefined" == typeof DayPilot.$) {
            DayPilot.$ = function(e) {
                    return document.getElementById(e)
                },
                
            DayPilot.isKhtml = navigator && navigator.userAgent && navigator.userAgent.indexOf("KHTML") !== -1, DayPilot.isIE = navigator && navigator.userAgent && (navigator.userAgent.indexOf("MSIE") !== -1 || navigator.userAgent.indexOf("Trident") !== -1), DayPilot.isIEQuirks = DayPilot.isIE && document.compatMode && "BackCompat" === document.compatMode, DayPilot.browser = {}, DayPilot.browser.ie8 = function() {
                    var e = document.createElement("div");
                    return e.innerHTML = "<!--[if IE 8]><i></i><![endif]-->", 1 === e.getElementsByTagName("i").length
                }(), 
            DayPilot.browser.ie9 = function() {
                    var e = document.createElement("div");
                    return e.innerHTML = "<!--[if IE 9]><i></i><![endif]-->", 1 === e.getElementsByTagName("i").length
                }(),
            DayPilot.browser.ielt9 = function() {
                    var e = document.createElement("div");
                    return e.innerHTML = "<!--[if lt IE 9]><i></i><![endif]-->", 1 === e.getElementsByTagName("i").length
                }(), DayPilot.browser.ff = navigator && navigator.userAgent && navigator.userAgent.indexOf("Firefox") !== -1, DayPilot.browser.chrome = navigator && navigator.userAgent && navigator.userAgent.indexOf("Chrome") !== -1, DayPilot.browser.ie = DayPilot.isIE, DayPilot.browser.webkit = navigator && navigator.userAgent && navigator.userAgent.indexOf("WebKit") !== -1, DayPilot.browser.passiveEvents = !1,
                function() {
                    try {
                        window.addEventListener("test", null, Object.defineProperty({}, "passive", {
                            "get": function() {
                                DayPilot.browser.passiveEvents = !0
                            }
                        }))
                    } catch (e) {}
                }(), 
            DayPilot.libs = {}, DayPilot.libs.angularjs = "object" == typeof angularjs, DayPilot.touch = {}, DayPilot.touch.start = window.navigator.msPointerEnabled ? "MSPointerDown" : "touchstart", DayPilot.touch.move = window.navigator.msPointerEnabled ? "MSPointerMove" : "touchmove", DayPilot.touch.end = window.navigator.msPointerEnabled ? "MSPointerUp" : "touchend", DayPilot.mo2 = function(e, t) {
                    if (t = t || window.event, "undefined" != typeof t.offsetX) {
                        var i = {
                            x: t.offsetX + 1,
                            y: t.offsetY + 1
                        };
                        if (!e) return i;
                        for (var n = t.srcElement; n && n !== e;) "SPAN" !== n.tagName && (i.x += n.offsetLeft, n.offsetTop > 0 && (i.y += n.offsetTop - n.scrollTop)), n = n.offsetParent;
                        return n ? i : null
                    }
                    if ("undefined" != typeof t.layerX) {
                        var i = {
                            x: t.layerX,
                            y: t.layerY,
                            src: t.target
                        };
                        if (!e) return i;
                        for (var n = t.target; n && "absolute" !== n.style.position && "relative" !== n.style.position;) n = n.parentNode, DayPilot.isKhtml && (i.y += n.scrollTop);
                        for (; n && n !== e;) i.x += n.offsetLeft, i.y += n.offsetTop - n.scrollTop, n = n.offsetParent;
                        return n ? i : null
                    }
                    return null
                }, DayPilot.mo3 = function(e, t) {
                    t = t || window.event;
                    var i, n = DayPilot.page(t);
                    if (n)
                        if (e) {
                            var a = DayPilot.abs(e);
                            i = {
                                x: n.x - a.x,
                                y: n.y - a.y
                            }
                        } else i = {
                            x: n.x,
                            y: n.y
                        };
                    else i = DayPilot.mo2(e, t);
                    return i.shift = t.shiftKey, i.meta = t.metaKey, i.ctrl = t.ctrlKey, i.alt = t.altKey, i
                }, DayPilot.mc = function(e) {
                    return e.pageX || e.pageY ? {
                        x: e.pageX,
                        y: e.pageY
                    } : {
                        x: e.clientX + document.documentElement.scrollLeft,
                        y: e.clientY + document.documentElement.scrollTop
                    }
                }, DayPilot.Stats = {}, DayPilot.Stats.eventObjects = 0, DayPilot.Stats.dateObjects = 0, DayPilot.Stats.cacheHitsCtor = 0, DayPilot.Stats.cacheHitsParsing = 0, DayPilot.Stats.cacheHitsTicks = 0, DayPilot.Stats.print = function() {
                    console.log("DayPilot.Stats.eventObjects: " + DayPilot.Stats.eventObjects), console.log("DayPilot.Stats.dateObjects: " + DayPilot.Stats.dateObjects), console.log("DayPilot.Stats.cacheHitsCtor: " + DayPilot.Stats.cacheHitsCtor), console.log("DayPilot.Stats.cacheHitsParsing: " + DayPilot.Stats.cacheHitsParsing), console.log("DayPilot.Stats.cacheHitsTicks: " + DayPilot.Stats.cacheHitsTicks), console.log("DayPilot.Date.Cache.Ctor keys: " + Object.keys(DayPilot.Date.Cache.Ctor).length), console.log("DayPilot.Date.Cache.Parsing keys: " + Object.keys(DayPilot.Date.Cache.Parsing).length)
                }, DayPilot.list = function(e, t) {
                    if (e && e.isDayPilotList && !t) return e;
                    var i = DayPilot.isArray(e) || "[object NodeList]" === Object.prototype.toString.call(e),
                        n = !i && !DayPilot.Util.isNullOrUndefined(e),
                        a = [];
                    if (a.isDayPilotList = !0, a.clone = function() {
                            var e = DayPilot.list();
                            return a.each(function(t) {
                                e.push(t)
                            }), e
                        }, a.each = function(e) {
                            if (e) {
                                if (a.forEach) return void a.forEach(e);
                                for (var t = 0; t < this.length; t++) e(a[t], t, a)
                            }
                        }, a.addProps = function(e) {
                            var t = a.clone();
                            if (e)
                                for (var i in e) t[i] = e[i];
                            return t
                        }, a.last = function() {
                            return 0 === a.length ? null : a[a.length - 1]
                        }, a.first = function() {
                            return 0 === a.length ? null : a[0]
                        }, a.add = function(e) {
                            var t = a.clone();
                            return t.push(e), t
                        }, a.map = function(e) {
                            if ("function" != typeof e) throw "DayPilot.list().map(f): Function expected";
                            var t = DayPilot.list();
                            return a.each(function(i, n, a) {
                                t.push(e(i, n, a))
                            }), t
                        }, a.filter = function(e) {
                            var t = DayPilot.list();
                            if ("function" != typeof e) throw "DayPilot.list().filter(f): Function expected";
                            return a.each(function(i) {
                                e(i) && t.push(i)
                            }), t
                        }, a.concat = function(e) {
                            if (!e) return a;
                            for (var t = a.clone(), i = 0; i < e.length; i++) t.push(e[i]);
                            return t
                        }, a.find || (a.find = function(e) {
                            if ("function" != typeof e) throw "DayPilot.list().find(f): Function expected";
                            for (var t = 0; t < this.length; t++)
                                if (e(a[t])) return a[t]
                        }), a.findIndex || (a.findIndex = function(e) {
                            if ("function" != typeof e) throw "DayPilot.list().findIndex(f): Function expected";
                            for (var t = 0; t < this.length; t++)
                                if (e(a[t])) return t;
                            return -1
                        }), a.some || (a.some = function(e) {
                            if ("function" != typeof e) throw "DayPilot.list().some(f): Function expected";
                            for (var t = 0; t < this.length; t++)
                                if (e(a[t])) return !0;
                            return !1
                        }), a.reduce || (a.reduce = function(e, t) {
                            if ("function" != typeof e) throw "DayPilot.list().reduce(f): Function expected";
                            var i, n = 0;
                            if ("undefined" != typeof t) i = t;
                            else {
                                if (n = 1, 0 === a.length) throw "DayPilot.list().reduce(f): No initial value and empty list";
                                i = a[0]
                            }
                            for (var o = n; o < this.length; o++) i = e(i, a[o], o, this);
                            return i
                        }), a.isEmpty = function() {
                            return 0 === a.length
                        }, i)
                        for (var o = 0; o < e.length; o++) a[o] = e[o];
                    else n && (a[0] = e);
                    return a
                }, DayPilot.list.for = function(e, t) {
                    for (var i = DayPilot.list(), n = 0; n < e; n++) "function" == typeof t ? i.push(t(n)) : i.push(n);
                    return i
                }, DayPilot.line = function(e, t, i, n, a) {
                    var o = {
                            "x": e,
                            "y": t
                        },
                        r = {
                            "x": i,
                            "y": n,
                            "deg": DayPilot.deg(e, t, i, n)
                        },
                        l = !1;
                    if (t < n) {
                        var s = t;
                        t = n, n = s, s = e, e = i, i = s, l = !0
                    }
                    var d = DayPilot.deg(e, t, i, n),
                        c = {
                            "x": e,
                            "y": t
                        },
                        h = {
                            "x": i,
                            "y": n
                        },
                        u = function() {
                            var a = Math.abs(e - i),
                                o = Math.abs(t - n),
                                r = (e + i) / 2;
                            return r - Math.sqrt(a * a + o * o) / 2
                        }(),
                        f = function() {
                            return (t + n) / 2
                        }(),
                        v = DayPilot.distance(c, h),
                        p = document.createElement("div");
                    p.setAttribute("style", "border:1px solid black;width:" + v + "px;height:0px;-moz-transform:rotate(" + d + "deg);-webkit-transform:rotate(" + d + "deg);-ms-transform:rotate(" + d + "deg);transform:rotate(" + d + "deg);position:absolute;top:" + f + "px;left:" + u + "px;");
                    var g = document.createElement("div");
                    g.appendChild(p);
                    var m, y, d, b;
                    if (a) {
                        var m = r.y - 5,
                            y = r.x - 5,
                            d = d;
                        r.y > o.y && (d -= 180);
                        var b = document.createElement("div");
                        b.style.borderColor = "black", b.style.width = "10px", b.style.height = "10px", b.style.borderRadius = "10px", b.style.backgroundColor = "black", b.style.position = "absolute", b.style.left = y + "px", b.style.top = m + "px", g.appendChild(b)
                    }
                    return g
                }, DayPilot.deg = function(e, t, i, n) {
                    var a, o = Math.abs(e - i),
                        r = Math.abs(t - n),
                        l = (e + i) / 2,
                        s = (t + n) / 2,
                        d = Math.sqrt(o * o + r * r),
                        c = l - d / 2,
                        h = s;
                    o = d / 2, a = Math.abs(l - c), r = Math.sqrt(Math.abs(e - c) * Math.abs(e - c) + Math.abs(t - h) * Math.abs(t - h));
                    var u = (r * r - o * o - a * a) / (2 * o * a);
                    return 180 * Math.acos(u) / Math.PI
                }, DayPilot.complete = function(e) {
                    return "complete" === document.readyState ? void e() : (DayPilot.complete.list || (DayPilot.complete.list = [], DayPilot.re(document, "readystatechange", function() {
                        if ("complete" === document.readyState) {
                            for (var e = 0; e < DayPilot.complete.list.length; e++) {
                                (0, DayPilot.complete.list[e])()
                            }
                            DayPilot.complete.list = []
                        }
                    })), void DayPilot.complete.list.push(e))
                }, DayPilot.page = function(e) {
                    return e = e || window.event, "undefined" == typeof e.pageX || DayPilot.browser.ie ? "undefined" != typeof e.clientX ? {
                        x: e.clientX + document.body.scrollLeft + document.documentElement.scrollLeft,
                        y: e.clientY + document.body.scrollTop + document.documentElement.scrollTop
                    } : null : {
                        x: e.pageX,
                        y: e.pageY
                    }
                }, DayPilot.rel = function(e, t) {
                    var i = DayPilot.abs(e),
                        n = DayPilot.abs(t);
                    return {
                        "x": i.x - n.x,
                        "y": i.y - n.y,
                        "w": e.offsetWidth,
                        "h": e.offsetHeight
                    }
                }, DayPilot.abs = function(e, t) {
                    if (!e) return null;
                    if (e.getBoundingClientRect) {
                        var i = DayPilot.absBoundingClientBased(e);
                        if (t) {
                            var n = DayPilot.absOffsetBased(e, !1),
                                t = DayPilot.absOffsetBased(e, !0);
                            i.x += t.x - n.x, i.y += t.y - n.y, i.w = t.w, i.h = t.h
                        }
                        return i
                    }
                    return DayPilot.absOffsetBased(e, t)
                }, DayPilot.absBoundingClientBased = function(e) {
                    var t = e.getBoundingClientRect();
                    return {
                        x: t.left + window.pageXOffset,
                        y: t.top + window.pageYOffset,
                        w: e.clientWidth,
                        h: e.clientHeight,
                        toString: function() {
                            return "x:" + this.x + " y:" + this.y + " w:" + this.w + " h:" + this.h
                        }
                    }
                }, DayPilot.isArray = function(e) {
                    return "[object Array]" === Object.prototype.toString.call(e)
                }, DayPilot.absOffsetBased = function(e, t) {
                    for (var i = {
                            x: e.offsetLeft,
                            y: e.offsetTop,
                            w: e.clientWidth,
                            h: e.clientHeight,
                            toString: function() {
                                return "x:" + this.x + " y:" + this.y + " w:" + this.w + " h:" + this.h
                            }
                        }; DayPilot.op(e);) e = DayPilot.op(e), i.x -= e.scrollLeft, i.y -= e.scrollTop, t && (i.x < 0 && (i.w += i.x, i.x = 0), i.y < 0 && (i.h += i.y, i.y = 0), e.scrollLeft > 0 && i.x + i.w > e.clientWidth && (i.w -= i.x + i.w - e.clientWidth), e.scrollTop && i.y + i.h > e.clientHeight && (i.h -= i.y + i.h - e.clientHeight)), i.x += e.offsetLeft, i.y += e.offsetTop;
                    var n = DayPilot.pageOffset();
                    return i.x += n.x, i.y += n.y, i
                }, DayPilot.wd = function() {
                    var e = DayPilot.isIEQuirks,
                        t = document.documentElement.clientHeight;
                    e && (t = document.body.clientHeight);
                    var i = document.documentElement.clientWidth;
                    e && (i = document.body.clientWidth);
                    var n = document.documentElement && document.documentElement.scrollTop || document.body.scrollTop,
                        a = document.documentElement && document.documentElement.scrollLeft || document.body.scrollLeft,
                        o = {};
                    return o.width = i, o.height = t, o.scrollTop = n, o.scrollLeft = a, o
                }, DayPilot.op = function(e) {
                    try {
                        return e.offsetParent
                    } catch (e) {
                        return document.body
                    }
                }, DayPilot.distance = function(e, t) {
                    return Math.sqrt(Math.pow(e.x - t.x, 2) + Math.pow(e.y - t.y, 2))
                }, DayPilot.doc = function() {
                    var e = document.documentElement;
                    return e && e.clientHeight ? e : document.body
                }, DayPilot.pageOffset = function() {
                    if ("undefined" != typeof window.pageXOffset) return {
                        x: window.pageXOffset,
                        y: window.pageYOffset
                    };
                    var e = DayPilot.doc();
                    return {
                        x: e.scrollLeft,
                        y: e.scrollTop
                    }
                }, DayPilot.ac = function(e, t) {
                    if (!t) var t = [];
                    for (var i = 0; e.children && i < e.children.length; i++) t.push(e.children[i]), DayPilot.ac(e.children[i], t);
                    return t
                }, DayPilot.indexOf = function(e, t, i) {
                    if (!e || !e.length) return -1;
                    for (var n = 0; n < e.length; n++)
                        if (i) {
                            if (i(e[n], t)) return n
                        } else if (e[n] === t) return n;
                    return -1
                }, DayPilot.contains = function(e, t) {
                    return !!e && (e === t && !DayPilot.isArray(e) || DayPilot.indexOf(e, t) !== -1)
                }, DayPilot.rfa = function(e, t) {
                    var i = DayPilot.indexOf(e, t);
                    i !== -1 && e.splice(i, 1)
                }, DayPilot.sheet = function(e) {
                    e = e || window.document;
                    var t = e.createElement("style");
                    t.setAttribute("type", "text/css"), t.styleSheet || t.appendChild(e.createTextNode("")), (e.head || e.getElementsByTagName("head")[0]).appendChild(t);
                    var i = !!t.styleSheet,
                        n = {};
                    return n.rules = [], n.commit = function() {
                        try {
                            i && (t.styleSheet.cssText = this.rules.join("\n"))
                        } catch (e) {}
                    }, n.add = function(e, n, a) {
                        if (i) return void this.rules.push(e + "{" + n + "}");
                        if (t.sheet.insertRule) "undefined" == typeof a && (a = t.sheet.cssRules.length), t.sheet.insertRule(e + "{" + n + "}", a);
                        else {
                            if (!t.sheet.addRule) throw "No CSS registration method found";
                            t.sheet.addRule(e, n, a)
                        }
                    }, n
                }, DayPilot.Args = function(e) {
                    var t = this;
                    this.isArgs = !0, this.preventDefault = function() {
                        e && e.preventDefault(), t.preventDefault.value = !0
                    }
                }, DayPilot.Debug = function(e) {
                    var t = this;
                    this.printToBrowserConsole = !1, this.enabled = !1, this.messages = [], this.a = null, this.clear = function() {
                        this.messages = [], t.a && (t.a.innerHTML = "")
                    }, this.hide = function() {
                        DayPilot.de(t.a), t.a = null
                    }, this.show = function() {
                        t.a && t.hide();
                        var i = e.nav.top,
                            n = document.createElement("div");
                        n.style.position = "absolute", n.style.top = "0px", n.style.bottom = "0px", n.style.left = "0px", n.style.right = "0px", n.style.backgroundColor = "black", n.style.color = "#ccc", n.style.overflow = "auto", n.style.webkitUserSelect = "auto", n.style.MozUserSelect = "all", n.onclick = function() {
                            t.hide()
                        };
                        for (var a = 0; a < this.messages.length; a++) {
                            var o = t.messages[a],
                                r = o.b();
                            n.appendChild(r)
                        }
                        this.a = n, i.appendChild(n)
                    }, this.message = function(e, t) {
                        if (this.enabled) {
                            var i = {};
                            i.time = new DayPilot.Date, i.level = t || "debug", i.text = e, i.b = function() {
                                var e = document.createElement("div");
                                switch (e.innerHTML = i.time + " (" + i.level + "): " + i.text, i.level) {
                                    case "error":
                                        e.style.color = "red";
                                        break;
                                    case "warning":
                                        e.style.color = "orange";
                                        break;
                                    case "info":
                                        e.style.color = "white";
                                        break;
                                    case "debug":
                                }
                                return e
                            }, this.messages.push(i), this.printToBrowserConsole && "undefined" != typeof console && console.log(i)
                        }
                    }
                }, DayPilot.re = function(e, t, i) {
                    if (i && e)
                        if (e.addEventListener) e.addEventListener(t, i, !1);
                        else if (e.attachEvent) {
                        var n = function(t) {
                            i.call(e, t)
                        };
                        e.attachEvent("on" + t, n)
                    }
                }, DayPilot.ue = function(e, t, i) {
                    e.removeEventListener ? e.removeEventListener(t, i, !1) : e.detachEvent && e.detachEvent("on" + t, i)
                }, DayPilot.tr = function(e) {
                    return e ? "string" == typeof e ? e.replace(/^\s+|\s+$/g, "") : e : ""
                }, DayPilot.ds = function(e) {
                    return DayPilot.Date.toStringSortable(e)
                }, DayPilot.gs = function(e, t) {
                    var i = e;
                    if (i.currentStyle) var n = i.currentStyle[t];
                    else if (window.getComputedStyle) var n = document.defaultView.getComputedStyle(i, null).getPropertyValue(t);
                    return "undefined" == typeof n && (n = ""), n
                }, DayPilot.StyleReader = function(e) {
                    function t(e) {
                        return e.charAt(0).toUpperCase() + e.slice(1)
                    }
                    this.get = function(i) {
                        var n = DayPilot.list(i.split("-")),
                            a = n.reduce(function(e, i, n) {
                                return i = t(i), e + i
                            });
                        return e ? DayPilot.gs(e, a) || DayPilot.gs(e, i) : null
                    }, this.getPx = function(e) {
                        var t = this.get(e);
                        return t.indexOf("px") === -1 ? void 0 : parseInt(t, 10)
                    }, this.getFont = function() {
                        var e = "bold" === this.get("font-weight") ? "bold" : "",
                            t = "italic" === this.get("font-style") ? "italic" : "";
                        return {
                            "family": this.get("font-family"),
                            "size": this.get("font-size"),
                            "style": t + " " + e
                        }
                    }, this.getBackColor = function(e) {
                        "undefined" == typeof e && (e = "white");
                        var t = this.get("background-color");
                        return DayPilot.Util.isTransparentColor(t) ? e : t
                    }
                }, DayPilot.he = function(e) {
                    var t = e.replace(/&/g, "&amp;");
                    return t = t.replace(/</g, "&lt;"), t = t.replace(/>/g, "&gt;"), t = t.replace(/"/g, "&quot;")
                }, DayPilot.he2 = function(e) {
                    var t = document.createTextNode(e),
                        i = document.createElement("div");
                    return i.appendChild(t), i.innerHTML
                }, DayPilot.us = function(e) {
                    if (e) {
                        e.setAttribute("unselectable", "on"), e.style.userSelect = "none", e.style.MozUserSelect = "none", e.style.KhtmlUserSelect = "none", e.style.webkitUserSelect = "none";
                        for (var t = 0; t < e.childNodes.length; t++) 1 === e.childNodes[t].nodeType && DayPilot.us(e.childNodes[t])
                    }
                }, DayPilot.pu = function(e) {
                    var t, i, n, a = e.attributes;
                    if (a)
                        for (i = a.length, t = 0; t < i; t += 1) a[t] && (n = a[t].name, "function" == typeof e[n] && (e[n] = null));
                    if (a = e.childNodes)
                        for (i = a.length, t = 0; t < i; t += 1) {
                            DayPilot.pu(e.childNodes[t])
                        }
                }, DayPilot.puc = function(e) {
                    var t, i, n = e.childNodes;
                    if (n) {
                        var i = n.length;
                        for (t = 0; t < i; t += 1) DayPilot.pu(e.childNodes[t])
                    }
                }, DayPilot.de = function(e) {
                    if (e)
                        if (DayPilot.isArray(e))
                            for (var t = 0; t < e.length; t++) DayPilot.de(e[t]);
                        else e.parentNode && e.parentNode.removeChild(e)
                }, DayPilot.fade = function(e, t, i) {
                    if (e) {
                        clearTimeout(e.messageTimeout);
                        var n = "none" !== e.style.display,
                            a = t > 0,
                            o = t < 0;
                        if (0 !== t) {
                            if (a ? e.status = "in" : o && (e.status = "out"), a && !n) e.target = parseFloat(e.style.opacity), e.opacity = 0, e.style.opacity = 0, e.style.filter = "alpha(opacity=0)", e.style.display = "";
                            else if (o && !e.target) e.target = e.style.opacity;
                            else {
                                var r = e.opacity,
                                    l = Math.floor(10 * (r + t)) / 10;
                                a && l > e.target && (l = e.target), o && l < 0 && (l = 0);
                                var s = 100 * l;
                                e.opacity = l, e.style.opacity = l, e.style.filter = "alpha(opacity=" + s + ")"
                            }
                            if (a && (e.opacity >= e.target || e.opacity >= 1) || o && e.opacity <= 0) {
                                if (e.target = null, o) {
                                    e.style.opacity = e.target, e.opacity = e.target;
                                    var d = e.target ? "alpha(opacity=" + 100 * e.target + ")" : null;
                                    e.style.filter = d, e.style.display = "none"
                                }
                                i && "function" == typeof i && (e.status = null, i())
                            } else e.messageTimeout = setTimeout(function() {
                                DayPilot.fade(e, t, i)
                            }, 50)
                        }
                    }
                }, DayPilot.sw = function(e) {
                    return e ? e.offsetWidth - e.clientWidth : 0
                }, DayPilot.swa = function() {
                    var e = document.createElement("div");
                    e.style.position = "absolute", e.style.top = "-2000px", e.style.left = "-2000px", e.style.width = "200px", e.style.height = "100px", e.style.overflow = "auto";
                    var t = document.createElement("div");
                    t.style.width = "300px", t.style.height = "300px", e.appendChild(t), document.body.appendChild(e);
                    var i = DayPilot.sw(e);
                    return document.body.removeChild(e), i
                }, DayPilot.sh = function(e) {
                    return e ? e.offsetHeight - e.clientHeight : 0
                }, DayPilot.guid = function() {
                    var e = function() {
                        return (65536 * (1 + Math.random()) | 0).toString(16).substring(1)
                    };
                    return "" + e() + e() + "-" + e() + "-" + e() + "-" + e() + "-" + e() + e() + e()
                }, DayPilot.ua = function(e) {
                    if ("string" == typeof e || "number" == typeof e) return [e];
                    for (var t = {}, i = [], n = 0, a = e.length; n < a; ++n) e[n] in t || (i.push(e[n]), t[e[n]] = 1);
                    return i
                }, DayPilot.am = function() {
                    return "undefined" == typeof angular ? null : (DayPilot.am.cached || (DayPilot.am.cached = angular.module("daypilot", [])), DayPilot.am.cached)
                },
                function() {
                    function e(e, n) {
                        var a = {
                            w: e.offsetWidth,
                            h: e.offsetHeight,
                            x: parseInt(e.style.left),
                            y: parseInt(e.style.top)
                        };
                        a.height = e.style.height, a.width = e.style.width, a.top = e.style.top, a.left = e.style.left, a.toString = function() {
                            return "w: " + this.w + " h:" + this.h
                        };
                        var o = {};
                        if (o.finished = null, o.vertical = "center", o.horizontal = "center", n)
                            for (var r in n) o[r] = n[r];
                        e.style.visibility = "hidden", e.style.display = "";
                        var l = n.animation || "fast",
                            s = t(l);
                        s.div = e, s.i = 0, s.target = a, s.config = o, i(s)
                    }

                    function t(e) {
                        var t = function() {
                                var e = [];
                                e.time = 10;
                                var t, i = .08;
                                t = .1;
                                for (var n = t; n < 1.2; n += i) e.push(n), t = n;
                                i = .03;
                                for (var n = t; n > .8; n -= i) e.push(n), t = n;
                                for (var n = t; n <= 1; n += i) e.push(n), t = n;
                                return e
                            },
                            i = function() {
                                var e = [];
                                e.time = 10;
                                var t, i = .04;
                                t = .1;
                                for (var n = t; n <= 1; n += i) e.push(n), t = n;
                                return e
                            },
                            n = function() {
                                var e = [];
                                e.time = 10;
                                var t, i = .08;
                                t = .1;
                                for (var n = t; n <= 1; n += i) e.push(n), t = n;
                                return e
                            },
                            a = {
                                "fast": n,
                                "slow": i,
                                "jump": t
                            };
                        return a[e] || (e = "fast"), a[e]()
                    }

                    function i(e) {
                        var t, n = e.div,
                            a = e[e.i],
                            o = a * e.target.h;
                        switch (e.config.vertical) {
                            case "center":
                                t = e.target.y - (o - e.target.h) / 2;
                                break;
                            case "top":
                                t = e.target.y;
                                break;
                            case "bottom":
                                t = e.target.y - (o - e.target.h);
                                break;
                            default:
                                throw "Unexpected 'vertical' value."
                        }
                        var r, l = a * e.target.w;
                        switch (e.config.horizontal) {
                            case "left":
                                r = e.target.x;
                                break;
                            case "center":
                                r = e.target.x - (l - e.target.w) / 2;
                                break;
                            case "right":
                                r = e.target.x - (l - e.target.w);
                                break;
                            default:
                                throw "Unexpected 'horizontal' value."
                        }
                        var s = DayPilot.wd(),
                            d = s.height + s.scrollTop - (t + o);
                        d < 0 && (t += d);
                        var c = s.width - (r + l);
                        c < 0 && (r += c), n.style.height = o + "px", n.style.top = t + "px", n.style.width = l + "px", n.style.left = r + "px", n.style.visibility = "visible", e.i++, e.i < e.length - 1 ? setTimeout(function(e) {
                            return function() {
                                i(e)
                            }
                        }(e), e.time) : (n.style.width = e.target.width, n.style.height = e.target.height, n.style.top = e.target.top, n.style.left = e.target.left, "function" == typeof e.config.finished && e.config.finished())
                    }
                    DayPilot.pop = e
                }(), DayPilot.Util = {}, DayPilot.Util.addClass = function(e, t) {
                    if (t && e)
                        if (DayPilot.isArray(e))
                            for (var i = 0; i < e.length; i++) DayPilot.Util.addClass(e[i], t);
                        else {
                            if (!e.className) return void(e.className = t);
                            var n = new RegExp("(^|\\s)" + t + "($|\\s)");
                            n.test(e.className) || (e.className = e.className + " " + t)
                        }
                }, DayPilot.Util.normalizeColor = function(e) {
                    function t(e) {
                        return e = e.replace(/[^\d,]/g, "").split(","), "#" + ((1 << 24) + (+e[0] << 16) + (+e[1] << 8) + +e[2]).toString(16).slice(1)
                    }
                    return 0 === e.indexOf("rgb") ? t(e) : e
                }, DayPilot.Util.addClassToString = function(e, t) {
                    return e ? new RegExp("(^|\\s)" + t + "($|\\s)").test(e) ? e : e + " " + t : t
                }, DayPilot.Util.removeClassFromString = function(e, t) {
                    if (!e) return "";
                    var i = new RegExp("(^|\\s)" + t + "($|\\s)");
                    return e.replace(i, " ").replace(/^\s\s*/, "").replace(/\s\s*$/, "")
                }, DayPilot.Util.removeClass = function(e, t) {
                    if (e)
                        if (DayPilot.isArray(e))
                            for (var i = 0; i < e.length; i++) DayPilot.Util.removeClass(e[i], t);
                        else if (e.className) {
                        var n = new RegExp("(^|\\s)" + t + "($|\\s)");
                        e.className = e.className.replace(n, " ").replace(/^\s\s*/, "").replace(/\s\s*$/, "")
                    }
                }, DayPilot.Util.props = function(e) {
                    var t = [];
                    for (var i in e) t.push(i), t.push(e[i]);
                    return t.join("-")
                }, DayPilot.Util.propArray = function(e, t, i) {
                    return DayPilot.list(e).map(function(e) {
                        return e[t] ? e[t] : i
                    })
                }, DayPilot.Util.updatePropsFromArray = function(e, t, i) {
                    for (var n = 0; n < i.length; n++) e[n][t] = i[n]
                }, DayPilot.Util.copyProps = function(e, t, i) {
                    if (e) {
                        if (t || (t = {}), "undefined" == typeof i)
                            for (var n in e) e.hasOwnProperty(n) && "undefined" != typeof e[n] && (t[n] = e[n]);
                        else
                            for (var a = 0; a < i.length; a++) {
                                var n = i[a];
                                "undefined" != typeof e[n] && (t[n] = e[n])
                            }
                        return t
                    }
                }, DayPilot.Util.firstPropValue = function(e) {
                    for (var t in e) return e[t]
                }, DayPilot.Util.isOnlyProperty = function(e, t) {
                    if (!e) return !1;
                    for (var i in e)
                        if (e.hasOwnProperty(i) && i !== t) return !1;
                    return !0
                }, DayPilot.Util.createArrayCopy = function(e, t) {
                    if (!DayPilot.isArray(e)) return [];
                    for (var i = [], n = 0; n < e.length; n++) {
                        var a = {};
                        DayPilot.Util.copyProps(e[n], a, t), i.push(a)
                    }
                    return i
                }, DayPilot.Util.avg = function(e, t) {
                    return (e + t) / 2
                }, DayPilot.Util.div = function(e, t, i, n, a) {
                    var o = document.createElement("div");
                    return (t || i || n || a) && (n < 0 && (t += n, n *= -1), a < 0 && (i += a, a *= -1), o.style.position = "absolute", "number" == typeof t ? o.style.left = t + "px" : "string" == typeof t && (o.style.left = t), "number" == typeof i ? o.style.top = i + "px" : "string" == typeof i && (o.style.top = i), "number" == typeof n ? o.style.width = n + "px" : "string" == typeof n && (o.style.width = n), "number" == typeof a ? o.style.height = a + "px" : "string" == typeof a && (o.style.height = a)), e && e.appendChild(o), o
                }, DayPilot.Util.overlaps = function(e, t, i, n) {
                    return !(t <= i || e >= n)
                }, DayPilot.Util.isMouseEvent = function(e) {
                    return !!navigator.msPointerEnabled && (!!e.pointerType && ("mouse" === e.pointerType || 4 === e.pointerType))
                }, DayPilot.Util.mouseButton = function(e) {
                    var t = {};
                    if (e = e || window.event, "undefined" == typeof e.which) switch (e.button) {
                        case 1:
                            t.left = !0;
                            break;
                        case 4:
                            t.middle = !0;
                            break;
                        case 2:
                            t.right = !0;
                            break;
                        case 0:
                            t.unknown = !0
                    } else switch (e.which) {
                        case 1:
                            t.left = !0;
                            break;
                        case 2:
                            t.middle = !0;
                            break;
                        case 3:
                            t.right = !0
                    }
                    return t
                }, DayPilot.Util.membersPlain = function(e) {
                    var t = DayPilot.Util.members(e, 2),
                        i = function(e) {
                            for (var t = 0; t < e.length; t++) {
                                var i = e[t],
                                    n = i.name;
                                i.obsolete && (n += " (obsolete)"), i.noCssOnly && (n += " (!cssOnly)"), i.aspnet && (n += " (ASP.NET)"), i.mvc && (n += "(MVC)"), e[t] = n
                            }
                        };
                    return i(t.events), i(t.methods), i(t.properties), t
                }, DayPilot.Util.shouldApply = function(e) {
                    if (!e) return !1;
                    var t = e.toLowerCase();
                    return !(t.indexOf("before") >= 0) && (!(t.indexOf("after") >= 0) && ("onrowfilter" !== t && "oneventfilter" !== t))
                }, DayPilot.Util.safeApply = function(e, t) {
                    var i = e["$root"]["$$phase"];
                    "$apply" === i || "$digest" === i ? e["$eval"](t) : e["$apply"](t)
                }, DayPilot.Util.members = function(e, t) {
                    var i = DayPilot.list(),
                        n = DayPilot.list(),
                        a = DayPilot.list(),
                        o = e && e.members ? e.members.obsolete : [],
                        r = e && e.members ? e.members.noCssOnly : [],
                        l = e && e.members ? e.members.ignore : [],
                        s = e && e.members && e.members.ignoreFilter ? e.members.ignoreFilter : function() {
                            return !1
                        };
                    for (var d in e)
                        if (0 !== d.indexOf("$") && 0 !== d.indexOf("_") && 0 !== d.indexOf("number") && 0 !== d.indexOf("is") && "v" !== d && !DayPilot.contains(l, d) && !s(d))
                            if (0 !== d.indexOf("on"))
                                if ("function" != typeof e[d])
                                    if ("object" != typeof e[d]) a.push(d);
                                    else {
                                        var c = e[d];
                                        if (0 === t) {
                                            a.push(d);
                                            continue
                                        }
                                        if (c && c.nodeType > 0) {
                                            a.push(d);
                                            continue
                                        }
                                        if (c instanceof DayPilot.Bubble) {
                                            a.push(d);
                                            continue
                                        }
                                        if (c instanceof DayPilot.Date) {
                                            a.push(d);
                                            continue
                                        }
                                        if (c instanceof DayPilot.Menu) {
                                            a.push(d);
                                            continue
                                        }
                                        if (c instanceof DayPilot.Scheduler) {
                                            a.push(d);
                                            continue
                                        }
                                        if (DayPilot.isArray(c)) {
                                            a.push(d);
                                            continue
                                        }
                                        null === c && a.push(d);
                                        var h = null;
                                        "number" == typeof t && (h = t - 1);
                                        for (var u = DayPilot.Util.members(c, h), f = 0; f < u.events.length; f++) i.push(d + "." + u.events[f].name);
                                        for (var f = 0; f < u.methods.length; f++) n.push(d + "." + u.methods[f].name);
                                        for (var f = 0; f < u.properties.length; f++) a.push(d + "." + u.properties[f].name)
                                    }
                    else n.push(d);
                    else i.push(d);
                    i.sort(), n.sort(), a.sort();
                    var v = function(e) {
                        for (var t = 0; t < e.length; t++) {
                            var i = e[t],
                                n = {};
                            n.name = i, e[t] = n, DayPilot.contains(o, i) && (n.obsolete = !0), DayPilot.contains(r, i) && (n.noCssOnly = !0), i.indexOf("CallBack") !== -1 && (n.aspnet = !0, n.mvc = !0), i.indexOf("PostBack") !== -1 && (n.aspnet = !0), i.indexOf("Notify") !== -1 && (n.aspnet = !0, n.mvc = !0)
                        }
                    };
                    return v(i), v(n), v(a), {
                        "events": i,
                        "methods": n,
                        "properties": a
                    }
                }, DayPilot.Util.replaceCharAt = function(e, t, i) {
                    return e.substr(0, t) + i + e.substr(t + i.length)
                }, DayPilot.Util.evalVariable = function(e, t) {
                    if (t = t || ["object"], null === e || "undefined" == typeof e) return null;
                    if (DayPilot.indexOf(t, typeof e) !== -1) return e;
                    if ("string" != typeof e) throw "Unable to resolve a variable name (not a string).";
                    if (!/^[a-zA-Z_$][0-9a-zA-Z_$]*$/.test(e)) throw "Invalid variable name: " + e;
                    return eval(e)
                }, DayPilot.Util.evalFunction = function(e) {
                    if (null === e) return null;
                    if ("function" == typeof e) return e;
                    if ("string" != typeof e) throw "Unable to resolve a function (not a string).";
                    if (!/^\(?function/.test(e)) throw "Invalid function string";
                    return eval("(" + e + ")")
                }, DayPilot.Util.isNullOrUndefined = function(e) {
                    return null === e || "undefined" == typeof e
                }, DayPilot.Util.log = function(e) {
                    window.console && window.console.log && window.console.log(e)
                }, DayPilot.Util.adiff = function(e, t, i) {
                    var n = {};
                    return n.add = [], n.remove = [], n
                }, DayPilot.Util.workerFrom = function(e) {
                    var t = e.toString(),
                        i = t.slice(t.indexOf("{") + 1, t.lastIndexOf("}")),
                        n = new Blob([i]),
                        a = window.URL.createObjectURL(n);
                    return new Worker(a)
                }, DayPilot.Util.dataUriToBlob = function(e) {
                    if ("string" != typeof e) throw "DayPilot.Util.dataUriToBlob(): dataURI string expected";
                    for (var t = e.split(","), i = t[0], n = t[1], a = i.indexOf("base64") !== -1, o = a ? atob : decodeURI, r = i.split(":")[1].split(";")[0], l = o(n), s = new window.Uint8Array(l.length), d = 0; d < l.length; d++) s[d] = l.charCodeAt(d);
                    return new Blob([s], {
                        "type": r
                    })
                }, DayPilot.Util.downloadDataUri = function(e, t) {
                    var i = DayPilot.Util.dataUriToBlob(e);
                    DayPilot.Util.downloadBlob(i, t)
                }, DayPilot.Util.downloadBlob = function(e, t) {
                    var t = t || "download";
                    if (window.navigator.msSaveBlob) window.navigator.msSaveBlob(e, t);
                    else {
                        var i = window.URL.createObjectURL(e),
                            n = document.createElement("a");
                        n.download = t, n.href = i, n.style = "display:none", document.body.appendChild(n), n.click(), setTimeout(function() {
                            document.body.removeChild(n), window.URL.revokeObjectURL(i)
                        })
                    }
                }, DayPilot.Util.monitor = function(e, t, i) {
                    var n = e[t];
                    e[t] = function() {
                        i.apply(e, arguments), n.apply(e, arguments)
                    }
                }, DayPilot.Util.isTransparentColor = function(e) {
                    return "transparent" === e || "rgba(0, 0, 0, 0)" === e
                }, DayPilot.Util.stripTags = function(e) {
                    if (DayPilot.Util.isNullOrUndefined(e)) return e;
                    if ("string" == typeof e) return e.replace(/<(?:.|\n)*?>/gm, "");
                    throw new DayPilot.Exception("Unable to strip tags from a non-string")
                }, DayPilot.Areas = {}, DayPilot.Areas.attach = function(e, t, n) {
                    var n = n || {},
                        a = n.areas,
                        o = n.allowed || function() {
                            return !0
                        },
                        r = n.offsetX || 0;
                    a = i(t, a), a && DayPilot.isArray(a) && 0 !== a.length && (DayPilot.re(e, "mousemove", function(i) {
                        e.active || e.areasDisabled || !o() || DayPilot.Areas.showAreas(e, t, i, a, {
                            "offsetX": r,
                            "eventDiv": n.eventDiv
                        })
                    }), DayPilot.re(e, "mouseout", function(t) {
                        DayPilot.Areas.hideAreas(e, t)
                    }), DayPilot.list(a).each(function(i) {
                        if ("Visible" === (i.visibility || i.v || "Visible")) {
                            var a = DayPilot.Areas.createArea(e, t, i, {
                                "offsetX": r,
                                "eventDiv": n.eventDiv
                            });
                            e.appendChild(a)
                        }
                    }))
                }, DayPilot.Areas.disable = function(e) {
                    e.areasDisabled = !0, DayPilot.list(e.childNodes).filter(function(e) {
                        return e.isActiveArea && !e.area.start
                    }).each(function(e) {
                        e.style.display = "none"
                    })
                }, DayPilot.Areas.enable = function(e) {
                    e.areasDisabled = !1, DayPilot.list(e.childNodes).filter(function(e) {
                        return e.isActiveArea && !e.area.start
                    }).each(function(e) {
                        e.style.display = ""
                    })
                }, DayPilot.Areas.remove = function(e) {
                    var t = DayPilot.list(e.childNodes).filter(function(e) {
                        return e.isActiveArea
                    });
                    DayPilot.de(t)
                };
            var i = function(e, t) {
                return DayPilot.isArray(t) || (t = e.areas, t || (e.cache ? t = e.cache.areas : e.data && (t = e.data.areas))), t
            };
            DayPilot.Areas.showAreas = function(e, t, i, n, a) {
                if (!DayPilot.Global.resizing && !DayPilot.Global.moving) {
                    if (DayPilot.Areas.all && DayPilot.Areas.all.length > 0)
                        for (var o = 0; o < DayPilot.Areas.all.length; o++) {
                            var r = DayPilot.Areas.all[o];
                            r !== e && DayPilot.Areas.hideAreas(r, i)
                        }
                    if (!e.active && (e.active = {}, DayPilot.isArray(n) || (n = t.areas, n || (t.cache ? n = t.cache.areas : t.data && (n = t.data.areas))), n && 0 !== n.length && !(e.areas && e.areas.length > 0))) {
                        e.areas = [];
                        for (var o = 0; o < n.length; o++) {
                            var l = n[o];
                            if ("Hover" === (l.visibility || l.v || "Visible")) {
                                var s = DayPilot.Areas.createArea(e, t, l, a);
                                e.areas.push(s), e.appendChild(s), DayPilot.Areas.all.push(e)
                            }
                        }
                        e.active.children = DayPilot.ac(e)
                    }
                }
            }, DayPilot.Areas.createArea = function(e, t, i, n) {
                function a(e, t, i) {
                    DayPilot.Bubble && DayPilot.Bubble.touchPosition(i), e.calendar.bubble && e.calendar.bubble.showEvent(e, !0)
                }

                function o(e, t, i, n) {
                    DayPilot.Menu && DayPilot.Menu.touchPosition(n);
                    var a = i.menu;
                    if ("string" == typeof a ? a = DayPilot.Util.evalVariable(a) : t.isEvent && t.calendar.contextMenu ? a = DayPilot.Util.evalVariable(t.calendar.contextMenu) : t.isRow && t.calendar.contextMenuResource && (a = DayPilot.Util.evalVariable(t.calendar.contextMenuResource)), a && a.show) {
                        var o = {
                            "type": "area",
                            "div": e,
                            "e": t,
                            "area": i
                        };
                        a.show(t, {
                            "initiator": o
                        })
                    }
                }
                var n = n || {},
                    r = n.offsetX || 0,
                    l = n.eventDiv || e,
                    s = document.createElement("div");
                s.isActiveArea = !0, s.area = i, s.setAttribute("unselectable", "on");
                var d = i.w || i.width,
                    c = i.h || i.height,
                    h = i.cssClass || i.css || i.className;
                if ("undefined" != typeof i.style && s.setAttribute("style", i.style), s.style.position = "absolute", "undefined" != typeof d && (s.style.width = d + "px"), "undefined" != typeof c && (s.style.height = c + "px"), "undefined" != typeof i.right && (s.style.right = i.right + "px"), "undefined" != typeof i.top && (s.style.top = i.top + "px"), "undefined" != typeof i.left && (s.style.left = i.left + r + "px"), "undefined" != typeof i.bottom && (s.style.bottom = i.bottom + "px"), "undefined" != typeof i.html && i.html) s.innerHTML = i.html;
                else if (i.icon) {
                    var u = document.createElement("i");
                    u.className = i.icon, s.appendChild(u)
                } else if (i.image) {
                    var f = document.createElement("img");
                    f.src = i.image, s.appendChild(f)
                }
                if (h && (s.className = h), i.toolTip && s.setAttribute("title", i.toolTip), i.backColor && (s.style.background = i.backColor), i.background && (s.style.background = i.background), i.fontColor && (s.style.color = i.fontColor), i.padding && (s.style.padding = i.padding + "px", s.style.boxSizing = "border-box"), "ResizeEnd" === i.action || "ResizeStart" === i.action || "Move" === i.action) {
                    if (t.calendar.isCalendar) switch (i.action) {
                        case "ResizeEnd":
                            i.cursor = "s-resize", i.dpBorder = "bottom";
                            break;
                        case "ResizeStart":
                            i.cursor = "n-resize", i.dpBorder = "top";
                            break;
                        case "Move":
                            i.cursor = "move"
                    }
                    if (t.calendar.isScheduler || t.calendar.isMonth) switch (i.action) {
                        case "ResizeEnd":
                            i.cursor = "e-resize", i.dpBorder = "right";
                            break;
                        case "ResizeStart":
                            i.cursor = "w-resize", i.dpBorder = "left";
                            break;
                        case "Move":
                            i.cursor = "move"
                    }
                    s.onmousemove = function(e, t, i) {
                        return function(n) {
                            var n = n || window.event;
                            n.cancelBubble = !0, t.calendar.internal && t.calendar.internal.dragInProgress && t.calendar.internal.dragInProgress() || (e.style.cursor = i.cursor, i.dpBorder && (e.dpBorder = i.dpBorder))
                        }
                    }(l, t, i), s.onmouseout = function(e, t, i) {
                        return function(t) {
                            e.style.cursor = ""
                        }
                    }(l, t, i)
                }
                if (("ResizeEnd" === i.action || "ResizeStart" === i.action) && t.isEvent && t.calendar.internal.touch) {
                    var v = function(e, t, i) {
                        return function(n) {
                            n.cancelBubble = !0;
                            var a = t.calendar.internal.touch,
                                o = n.touches ? n.touches[0] : n,
                                r = {
                                    x: o.pageX,
                                    y: o.pageY
                                };
                            t.calendar.coords = a.relativeCoords(n), a.preventEventTap = !0, t.calendar.isScheduler ? a.startResizing(e, "ResizeEnd" === i.action ? "right" : "left") : t.calendar.isCalendar && a.startResizing(e, "ResizeEnd" === i.action ? "bottom" : "top", r)
                        }
                    }(l, t, i);
                    DayPilot.re(s, DayPilot.touch.start, v)
                }
                if ("ContextMenu" === i.action && t.isEvent && t.calendar.internal.touch) {
                    var v = function(e, t, i) {
                            return function(n) {
                                n.cancelBubble = !0, n.preventDefault(), o(e, t, i, n), t.calendar.internal.touch.preventEventTap = !0
                            }
                        }(l, t, i),
                        p = function(e, t, i) {
                            return function(e) {
                                e.cancelBubble = !0, e.preventDefault()
                            }
                        }(l, t, i);
                    DayPilot.re(s, DayPilot.touch.start, v), DayPilot.re(s, DayPilot.touch.end, p)
                }
                if ("Bubble" === i.action && t.isEvent && t.calendar.internal.touch) {
                    var v = function(e, t, i) {
                            return function(e) {
                                e.cancelBubble = !0, e.preventDefault(), a(t, i, e), t.calendar.internal.touch.preventEventTap = !0
                            }
                        }(l, t, i),
                        p = function(e, t, i) {
                            return function(e) {
                                e.cancelBubble = !0, e.preventDefault()
                            }
                        }(l, t, i);
                    DayPilot.re(s, DayPilot.touch.start, v), DayPilot.re(s, DayPilot.touch.end, p)
                }
                if ("Move" === i.action && t.isEvent && t.calendar.internal.touch) {
                    var v = function(e, t, i) {
                        return function(i) {
                            i.cancelBubble = !0;
                            var n = t.calendar.internal.touch,
                                a = i.touches ? i.touches[0] : i,
                                o = {
                                    x: a.pageX,
                                    y: a.pageY
                                };
                            t.calendar.coords = n.relativeCoords(i), n.preventEventTap = !0, n.startMoving(e, o)
                        }
                    }(l, t, i);
                    DayPilot.re(s, DayPilot.touch.start, v)
                }
                if ("Move" === i.action && t.isRow && t.calendar.internal.touch, "Bubble" === i.action && t.isEvent && (s.onmousemove = function(e, t, i) {
                        return function(e) {
                            i.bubble ? i.bubble.showEvent(t, !0) : t.calendar.bubble && t.calendar.bubble.showEvent(t, !0)
                        }
                    }(e, t, i), s.onmouseout = function(e, t, i) {
                        return function(e) {
                            "undefined" != typeof DayPilot.Bubble && (i.bubble ? i.bubble.hideOnMouseOut() : t.calendar.bubble && t.calendar.bubble.hideOnMouseOut())
                        }
                    }(e, t, i)), "Bubble" === i.action && t.isRow && (s.onmousemove = function(e, t, i) {
                        return function(e) {
                            i.bubble ? i.bubble.showResource(t, !0) : t.calendar.resourceBubble && t.calendar.resourceBubble.showResource(t, !0)
                        }
                    }(e, t, i), s.onmouseout = function(e, t, i) {
                        return function(e) {
                            "undefined" != typeof DayPilot.Bubble && (i.bubble ? i.bubble.hideOnMouseOut() : t.calendar.resourceBubble && t.calendar.resourceBubble.hideOnMouseOut())
                        }
                    }(e, t, i)), "HoverMenu" === i.action && (s.onmousemove = function(e, t, i) {
                        return function(e) {
                            var n = i.menu;
                            "string" == typeof n && (n = DayPilot.Util.evalVariable(n)), n && n.show && (n.visible ? n.source && "undefined" != typeof n.source.id && n.source.id !== t.id && n.show(t) : n.show(t), n.cancelHideTimeout())
                        }
                    }(e, t, i), s.onmouseout = function(e, t, i) {
                        return function(e) {
                            var t = i.menu;
                            "string" == typeof t && (t = DayPilot.Util.evalVariable(t)), t && t.hideOnMouseOver && t.delayedHide()
                        }
                    }(e, t, i)), "JavaScript" === i.action) {
                    var v = function(e, t, i) {
                            return function(e) {
                                if (!DayPilot.Util.isMouseEvent(e)) {
                                    e.cancelBubble = !0, e.preventDefault();
                                    t.calendar.internal.touch.start = !0
                                }
                            }
                        }(l, t, i),
                        p = function(e, t, i) {
                            return function(e) {
                                if (!DayPilot.Util.isMouseEvent(e)) {
                                    e.cancelBubble = !0, e.preventDefault();
                                    t.calendar.internal.touch.start = !1;
                                    var n = i.js;
                                    if ("string" == typeof n && (n = DayPilot.Util.evalFunction(i.js)), "function" == typeof n) {
                                        var a = i.target || t;
                                        n.call(this, a)
                                    }
                                }
                            }
                        }(l, t, i);
                    DayPilot.re(s, DayPilot.touch.start, v), DayPilot.re(s, DayPilot.touch.end, p)
                }
                return s.onmousedown = function(e, t, i) {
                    return function(n) {
                        if ("function" == typeof i.onmousedown && i.onmousedown(n), "function" == typeof i.mousedown) {
                            var a = {};
                            a.area = i, a.div = e, a.originalEvent = n, a.source = t, i.mousedown(a)
                        }
                        if ("Move" === i.action && t.isRow) {
                            var o = t.$.row;
                            t.calendar.internal.rowtools.startMoving(o)
                        }
                        "undefined" != typeof DayPilot.Bubble && DayPilot.Bubble.hideActive(), "Move" === i.action && (DayPilot.Global.movingAreaData = i.data);
                        "Move" !== i.action && "ResizeEnd" !== i.action && "ResizeStart" !== i.action && i.action && (n = n || window.event, n.preventDefault(), n.cancelBubble = !0)
                    }
                }(e, t, i), s.onclick = function(e, t, i) {
                    return function(n) {
                        var n = n || window.event,
                            a = {};
                        if (a.area = i, a.source = t, a.originalEvent = n, a.preventDefault = function() {
                                a.preventDefault.value = !0
                            }, "function" != typeof i.onClick || (i.onClick(a), !a.preventDefault.value)) {
                            switch (i.action) {
                                case "JavaScript":
                                    var r = i.js;
                                    if ("string" == typeof r && (r = DayPilot.Util.evalFunction(i.js)), "function" == typeof r) {
                                        var l = i.target || t;
                                        r.call(this, l)
                                    }
                                    n.cancelBubble = !0;
                                    break;
                                case "ContextMenu":
                                    o(e, t, i, n), n.cancelBubble = !0;
                                    break;
                                case "CallBack":
                                    alert("callback not implemented yet, id: " + i.id), n.cancelBubble = !0
                            }
                            "function" == typeof i.onClicked && i.onClicked(a)
                        }
                    }
                }(e, t, i), s
            }, DayPilot.Areas.all = [], DayPilot.Areas.hideAreas = function(e, t) {
                if (e && e && e.active) {
                    var i = e.active,
                        n = e.areas;
                    if (i && i.children) {
                        var t = t || window.event;
                        if (t) {
                            var a = t.toElement || t.relatedTarget;
                            if (~DayPilot.indexOf(i.children, a)) return
                        }
                    }
                    if (!n || 0 === n.length) return void(e.active = null);
                    DayPilot.de(n), e.active = null, e.areas = [], DayPilot.rfa(DayPilot.Areas.all, e), i.children = null
                }
            }, DayPilot.Areas.hideAll = function(e) {
                if (DayPilot.Areas.all && 0 !== DayPilot.Areas.all.length)
                    for (var t = 0; t < DayPilot.Areas.all.length; t++) DayPilot.Areas.hideAreas(DayPilot.Areas.all[t], e)
            }, DayPilot.Action = function(e, t, i, n) {
                var a = this;
                this.calendar = e, this.isAction = !0, this.action = t, this.params = i, this.data = n, this.notify = function() {
                    a.calendar.internal.invokeEvent("Immediate", this.action, this.params, this.data)
                }, this.auto = function() {
                    a.calendar.internal.invokeEvent("Notify", this.action, this.params, this.data)
                }, this.queue = function() {
                    a.calendar.queue.add(this)
                }, this.toJSON = function() {
                    var e = {};
                    return e.name = this.action, e.params = this.params, e.data = this.data, e
                }
            }, DayPilot.Selection = function(e, t, i, n) {
                this.menuType = "selection", this.start = new DayPilot.Date(e), this.end = new DayPilot.Date(t), this.resource = i, this.root = n, this.calendar = n, this.toJSON = function(e) {
                    var t = {};
                    return t.start = this.start, t.end = this.end, t.resource = this.resource, t
                }
            }, DayPilot.Link = function(e, t) {
                this.isLink = !0, this.data = e, this.calendar = t, this.to = function() {
                    return this.data.to
                }, this.from = function() {
                    return this.data.from
                }, this.type = function() {
                    return this.data.type
                }, this.id = function() {
                    return this.data.id
                }, this.toJSON = function() {
                    var e = {};
                    return e.from = this.data.from, e.to = this.data.to, e.id = this.data.id, e.type = this.data.type, e
                }
            }, DayPilot.Event = function(e, t, i) {
                var n = this;
                this.calendar = t, this.data = e ? e : {}, this.part = i ? i : {}, "undefined" == typeof this.data.id && (this.data.id = this.data.value);
                var a = {},
                    o = ["id", "text", "start", "end", "resource"];
                this.isEvent = !0, DayPilot.Stats.eventObjects += 1, this.temp = function() {
                    if (a.dirty) return a;
                    for (var e = 0; e < o.length; e++) a[o[e]] = n.data[o[e]];
                    return a.dirty = !0, a
                }, this.copy = function() {
                    var e = {};
                    return DayPilot.Util.copyProps(n.data, e), e
                }, this.commit = function() {
                    if (a.dirty) {
                        for (var e = 0; e < o.length; e++) n.data[o[e]] = a[o[e]];
                        a.dirty = !1
                    }
                }, this.dirty = function() {
                    return a.dirty
                }, this.id = function(e) {
                    return "undefined" == typeof e ? n.data.id : void(this.temp().id = e)
                }, this.value = function(e) {
                    return "undefined" == typeof e ? n.id() : void n.id(e)
                }, this.text = function(e) {
                    return "undefined" == typeof e ? n.data.text : (this.temp().text = e, void this.client.innerHTML(e))
                }, this.start = function(e) {
                    return "undefined" == typeof e ? new DayPilot.Date(n.data.start) : void(this.temp().start = new DayPilot.Date(e))
                }, this.end = function(e) {
                    return "undefined" == typeof e ? t && t.internal.adjustEndNormalize ? t.internal.adjustEndNormalize(new DayPilot.Date(n.data.end)) : new DayPilot.Date(n.data.end) : void(this.temp().end = new DayPilot.Date(e))
                }, this.duration = function() {
                    return new DayPilot.Duration(this.start(), this.end())
                }, this.rawend = function() {
                    return "undefined" == typeof val ? t && t.internal.adjustEndIn ? t.internal.adjustEndIn(new DayPilot.Date(n.data.end)) : new DayPilot.Date(n.data.end) : void(this.temp().end = new DayPilot.Date(val))
                }, this.partStart = function() {
                    return new DayPilot.Date(this.part.start)
                }, this.partEnd = function() {
                    return new DayPilot.Date(this.part.end)
                }, this.row = function() {
                    return this.resource()
                }, this.allday = function() {
                    return "undefined" == typeof val ? n.data.allday : void(this.temp().allday = val)
                }, this.isAllDay = this.allday, this.resource = function(e) {
                    return "undefined" == typeof e ? n.data.resource : void(this.temp().resource = e)
                }, this.recurrent = function() {
                    return n.data.recurrent
                }, this.recurrentMasterId = function() {
                    return n.data.recurrentMasterId
                }, this.useBox = function() {
                    return this.part.box
                }, this.staticBubbleHTML = function() {
                    return this.bubbleHtml()
                }, this.bubbleHtml = function() {
                    return n.cache ? n.cache.bubbleHtml || n.data.bubbleHtml : n.data.bubbleHtml
                }, this.tag = function(e) {
                    if (n.data.tags) return n.data.tags[e];
                    var t = n.data.tag;
                    if (!t) return null;
                    if ("undefined" == typeof e) return n.data.tag;
                    for (var i = n.calendar.tagFields, a = -1, o = 0; o < i.length; o++) e === i[o] && (a = o);
                    if (a === -1) throw "Field name not found.";
                    return t[a]
                }, this.client = {}, this.client.innerHTML = function(e) {
                    return "undefined" == typeof e ? n.cache && "undefined" != typeof n.cache.html ? n.cache.html : "undefined" != typeof n.data.html ? n.data.html : n.data.text : (n.data.html = e, void(n.cache && (n.cache.html = e)))
                }, this.client.html = this.client.innerHTML, this.client.header = function(e) {
                    return "undefined" == typeof e ? n.data.header : void(n.data.header = e)
                }, this.client.cssClass = function(e) {
                    return "undefined" == typeof e ? n.data.cssClass : void(n.data.cssClass = e)
                }, this.client.toolTip = function(e) {
                    return "undefined" == typeof e ? n.cache && "undefined" != typeof n.cache.toolTip ? n.cache.toolTip : "undefined" != typeof n.data.toolTip ? n.data.toolTip : n.data.text : void(n.data.toolTip = e)
                }, this.client.backColor = function(e) {
                    return "undefined" == typeof e ? n.cache && "undefined" != typeof n.cache.backColor ? n.cache.backColor : "undefined" != typeof n.data.backColor ? n.data.backColor : n.calendar.eventBackColor : void(n.data.backColor = e)
                }, this.client.borderColor = function(e) {
                    return "undefined" == typeof e ? n.cache && "undefined" != typeof n.cache.borderColor ? n.cache.borderColor : "undefined" != typeof n.data.borderColor ? n.data.borderColor : n.calendar.eventBorderColor : void(n.data.borderColor = e)
                }, this.client.barColor = function(e) {
                    return "undefined" == typeof e ? n.cache && "undefined" != typeof n.cache.barColor ? n.cache.barColor : "undefined" != typeof n.data.barColor ? n.data.barColor : n.calendar.durationBarColor : void(n.data.barColor = e)
                }, this.client.barVisible = function(e) {
                    return "undefined" == typeof e ? n.cache && "undefined" != typeof n.cache.barHidden ? !n.cache.barHidden : n.calendar.durationBarVisible && !n.data.barHidden : void(n.data.barHidden = !e)
                }, this.client.contextMenu = function(e) {
                    if ("undefined" == typeof e) {
                        if (n.oContextMenu) return n.oContextMenu;
                        var t = n.cache ? n.cache.contextMenu : n.data.contextMenu;
                        return t ? DayPilot.Util.evalVariable(t) : null
                    }
                    n.oContextMenu = e
                }, this.client.moveEnabled = function(e) {
                    return "undefined" == typeof e ? n.cache && "undefined" != typeof n.cache.moveDisabled ? !n.cache.moveDisabled : "Disabled" !== n.calendar.eventMoveHandling && !n.data.moveDisabled : void(n.data.moveDisabled = !e)
                }, this.client.resizeEnabled = function(e) {
                    return "undefined" == typeof e ? n.cache && "undefined" != typeof n.cache.resizeDisabled ? !n.cache.resizeDisabled : "Disabled" !== n.calendar.eventResizeHandling && !n.data.resizeDisabled : void(n.data.resizeDisabled = !e)
                }, this.client.rightClickEnabled = function(e) {
                    return "undefined" == typeof e ? n.cache && "undefined" != typeof n.cache.rightClickDisabled ? !n.cache.rightClickDisabled : "Disabled" !== n.calendar.rightClickHandling && !n.data.rightClickDisabled : void(n.data.rightClickDisabled = !e)
                }, this.client.clickEnabled = function(e) {
                    return "undefined" == typeof e ? n.cache && "undefined" != typeof n.cache.clickDisabled ? !n.cache.clickDisabled : "Disabled" !== n.calendar.clickHandling && !n.data.clickDisabled : void(n.data.clickDisabled = !e)
                }, this.client.deleteEnabled = function(e) {
                    return "undefined" == typeof e ? n.cache && "undefined" != typeof n.cache.deleteDisabled ? !n.cache.deleteDisabled : "Disabled" !== n.calendar.eventDeleteHandling && !n.data.deleteDisabled : void(n.data.deleteDisabled = !e)
                }, this.client.doubleClickEnabled = function(e) {
                    return "undefined" == typeof e ? n.cache && "undefined" != typeof n.cache.doubleClickDisabled ? !n.cache.doubleClickDisabled : "Disabled" !== n.calendar.eventDoubleClickHandling && !n.data.doubleClickDisabled : void(n.data.doubleClickDisabled = !e)
                }, this.client.deleteClickEnabled = function(e) {
                    return "undefined" == typeof e ? n.cache && "undefined" != typeof n.cache.deleteDisabled ? !n.cache.deleteDisabled : "Disabled" !== n.calendar.eventDeleteHandling && !n.data.deleteDisabled : void(n.data.deleteDisabled = !e)
                }, this.toJSON = function(e) {
                    var t = {};
                    if (t.value = this.id(), t.id = this.id(), t.text = this.text(), t.start = this.start().toJSON(), t.end = this.end().toJSON(), t.resource = this.resource(), t.isAllDay = !1, t.recurrentMasterId = this.recurrentMasterId(), t.join = this.data.join, t.tag = {}, n.data.tags)
                        for (var i in n.data.tags) n.data.tags.hasOwnProperty(i) && (t.tag[i] = "" + n.data.tags[i]);
                    else if (n.calendar && n.calendar.tagFields)
                        for (var a = n.calendar.tagFields, o = 0; o < a.length; o++) t.tag[a[o]] = this.tag(a[o]);
                    return t
                }
            }, DayPilot.Task = function(e, t) {
                if (!e) throw "Trying to initialize DayPilot.Task with null data parameter";
                var i = this,
                    n = null;
                if (e instanceof DayPilot.Event) n = e, this.data = e.data.task;
                else {
                    if (e instanceof DayPilot.Task) return e;
                    e.isTaskWrapper ? this.data = e.data : this.data = e
                }
                var a = {},
                    o = ["id", "text", "start", "end", "complete", "type"];
                this.isTask = !0, this.calendar = t, this.temp = function() {
                    if (a.dirty) return a;
                    for (var e = 0; e < o.length; e++) a[o[e]] = i.data[o[e]];
                    return a.dirty = !0, a
                }, this.copy = function() {
                    var e = {};
                    return DayPilot.Util.copyProps(i.data, e), e
                }, this.commit = function() {
                    if (a.dirty) {
                        for (var e = 0; e < o.length; e++) i.data[o[e]] = a[o[e]];
                        a.dirty = !1
                    }
                }, this.dirty = function() {
                    return a.dirty
                }, this.id = function(e) {
                    return "undefined" == typeof e ? i.data.id : void(this.temp().id = e)
                }, this.text = function(e) {
                    return "undefined" == typeof e ? i.data.text : (this.temp().text = e, void this.client.innerHTML(e))
                }, this.start = function(e) {
                    return "undefined" == typeof e ? new DayPilot.Date(i.data.start) : void(this.temp().start = new DayPilot.Date(e))
                }, this.duration = function() {
                    return new DayPilot.Duration(this.start(), this.end())
                }, this.end = function(e) {
                    return "undefined" == typeof e ? t && "Date" === t.eventEndSpec ? new DayPilot.Date(i.data.end).getDatePart().addDays(1) : new DayPilot.Date(i.data.end) : void(this.temp().end = new DayPilot.Date(e))
                }, this.type = function(e) {
                    return "undefined" == typeof e ? n ? n.data.type : i.data.type : void(this.temp().type = e)
                }, this.complete = function(e) {
                    return "undefined" == typeof e ? i.data.complete ? i.data.complete : 0 : void(this.temp().complete = e)
                }, this.children = function() {
                    var e = [];
                    e.add = function(e) {
                        var t = new DayPilot.Task(e);
                        this.data.children || (this.data.children = []), this.children.push(t.data)
                    };
                    for (var i = 0; this.data.children && i < this.data.children.length; i++) e.push(new DayPilot.Task(this.data.children[i], t));
                    return e
                }, this.toJSON = function(e) {
                    var t = {};
                    return t.id = this.id(), t.text = this.text(), t.start = this.start().toJSON(), t.end = this.end().toJSON(), t.type = this.type(), t.tags = {}, DayPilot.Util.copyProps(this.data.tags, t.tags), t
                }, this.row = {};
                var r = this.row;
                r.expanded = function(e) {
                    return "undefined" == typeof e ? !i.data.row || !i.data.row.collapsed : (i.data.row || (i.data.row = {}), !!i.data.row.collapsed != !e && t.internal.rowObjectForTaskData(i.data).toggle(), i.data.row.collapsed = !e, void 0)
                }, r.expand = function() {
                    r.expanded(!0)
                }, r.collapse = function() {
                    r.expanded(!1)
                }, r.toggle = function() {
                    r.expanded(!r.expanded())
                }
            }, DayPilot.request = function(e, t, i, n) {
                var a = DayPilot.createXmlHttp();
                a && (a.open("POST", e, !0), a.setRequestHeader("Content-type", "text/plain"), a.onreadystatechange = function() {
                    if (4 === a.readyState) return 200 !== a.status && 304 !== a.status ? void(n ? n(a) : window.console && console.log("HTTP error " + a.status)) : void t(a)
                }, 4 !== a.readyState && ("object" == typeof i && (i = JSON.stringify(i)), a.send(i)))
            }, DayPilot.ajax = function(e) {
                var t = DayPilot.createXmlHttp();
                if (t) {
                    var i = e.method || "GET",
                        n = e.success || function() {},
                        a = e.error || function() {},
                        o = e.data,
                        r = e.url,
                        l = e.contentType || "text/plain";
                    t.open(i, r, !0), t.setRequestHeader("Content-type", l), t.onreadystatechange = function() {
                        if (4 === t.readyState)
                            if (200 === t.status || 304 === t.status) {
                                var e = {};
                                e.request = t, n(e)
                            } else if (a) {
                            var e = {};
                            e.request = t, a(e)
                        } else window.console && console.log("HTTP error " + t.status)
                    }, 4 !== t.readyState && ("object" == typeof o && (o = JSON.stringify(o)), t.send(o))
                }
            }, DayPilot.createXmlHttp = function() {
                return new XMLHttpRequest
            }, DayPilot.Duration = function(e) {
                var t = this,
                    i = 864e5,
                    n = 36e5,
                    a = 6e4,
                    o = 1e3;
                if (2 === arguments.length) {
                    var r = arguments[0],
                        l = arguments[1];
                    if (!(r instanceof DayPilot.Date) && "string" != typeof r) throw "DayPilot.Duration(): Invalid start argument, DayPilot.Date expected";
                    if (!(l instanceof DayPilot.Date) && "string" != typeof l) throw "DayPilot.Duration(): Invalid end argument, DayPilot.Date expected";
                    "string" == typeof r && (r = new DayPilot.Date(r)), "string" == typeof l && (l = new DayPilot.Date(l)), e = l.getTime() - r.getTime()
                }
                return this.ticks = e, DayPilot.Date.Cache.DurationCtor["" + e] ? DayPilot.Date.Cache.DurationCtor["" + e] : (DayPilot.Date.Cache.DurationCtor["" + e] = this, this.toString = function(e) {
                    if (!e) return t.days() + "." + t.hours() + ":" + t.minutes() + ":" + t.seconds() + "." + t.milliseconds();
                    var i = t.minutes();
                    i = (i < 10 ? "0" : "") + i;
                    var n = e;
                    return n = n.replace("mm", i), n = n.replace("m", t.minutes()), n = n.replace("H", t.hours()), n = n.replace("h", t.hours()), n = n.replace("d", t.days()), n = n.replace("s", t.seconds())
                }, this.totalHours = function() {
                    return t.ticks / n
                }, this.totalDays = function() {
                    return t.ticks / i
                }, this.totalMinutes = function() {
                    return t.ticks / a
                }, this.totalSeconds = function() {
                    return t.ticks / o
                }, this.days = function() {
                    return Math.floor(t.totalDays())
                }, this.hours = function() {
                    var e = t.ticks - t.days() * i;
                    return Math.floor(e / n)
                }, this.minutes = function() {
                    var e = t.ticks - Math.floor(t.totalHours()) * n;
                    return Math.floor(e / a)
                }, this.seconds = function() {
                    var e = t.ticks - Math.floor(t.totalMinutes()) * a;
                    return Math.floor(e / o)
                }, void(this.milliseconds = function() {
                    return t.ticks % o
                }))
            }, DayPilot.Duration.weeks = function(e) {
                return new DayPilot.Duration(1e3 * e * 60 * 60 * 24 * 7)
            }, DayPilot.Duration.days = function(e) {
                return new DayPilot.Duration(1e3 * e * 60 * 60 * 24)
            }, DayPilot.Duration.hours = function(e) {
                return new DayPilot.Duration(1e3 * e * 60 * 60)
            }, DayPilot.Duration.minutes = function(e) {
                return new DayPilot.Duration(1e3 * e * 60)
            }, DayPilot.Duration.seconds = function(e) {
                return new DayPilot.Duration(1e3 * e)
            }, DayPilot.TimeSpan = function() {
                throw "Please use DayPilot.Duration class instead of DayPilot.TimeSpan."
            };
            try {
                DayPilot.TimeSpan.prototype = Object.create(DayPilot.Duration.prototype)
            } catch (e) {}
            DayPilot.Date = function(t, i) {
                if (t instanceof DayPilot.Date) return t;
                var n;
                DayPilot.Util.isNullOrUndefined(t) && (n = DayPilot.DateUtil.fromLocal().getTime(), t = n);
                var a = DayPilot.Date.Cache.Ctor;
                if (a[t]) return DayPilot.Stats.cacheHitsCtor += 1, a[t];
                var o = !1;
                if ("string" == typeof t) n = DayPilot.DateUtil.fromStringSortable(t, i).getTime(), o = !0;
                else if ("number" == typeof t) {
                    if (isNaN(t)) throw "Cannot create DayPilot.Date from NaN";
                    n = t
                } else {
                    if (!(t instanceof Date)) throw "Unrecognized parameter: use Date, number or string in ISO 8601 format";
                    n = i ? DayPilot.DateUtil.fromLocal(t).getTime() : t.getTime()
                }
                var r = e(n);
                return a[r] ? a[r] : (a[r] = this, a[n] = this, o && r !== t && DayPilot.DateUtil.hasTzSpec(t) && (a[t] = this), Object.defineProperty && !DayPilot.browser.ielt9 ? (Object.defineProperty(this, "ticks", {
                    get: function() {
                        return n
                    }
                }), Object.defineProperty(this, "value", {
                    "value": r,
                    "writable": !1,
                    "enumerable": !0
                })) : (this.ticks = n, this.value = r), DayPilot.Date.Config.legacyShowD && (this.d = new Date(n)), void(DayPilot.Stats.dateObjects += 1))
            }, DayPilot.Date.Config = {}, DayPilot.Date.Config.legacyShowD = !1, DayPilot.Date.Cache = {}, DayPilot.Date.Cache.Parsing = {}, DayPilot.Date.Cache.Ctor = {}, DayPilot.Date.Cache.Ticks = {}, DayPilot.Date.Cache.DurationCtor = {}, DayPilot.Date.Cache.clear = function() {
                DayPilot.Date.Cache.Parsing = {}, DayPilot.Date.Cache.Ctor = {}, DayPilot.Date.Cache.Ticks = {}, DayPilot.Date.Cache.DurationCtor = {}
            }, DayPilot.Date.prototype.addDays = function(e) {
                return new DayPilot.Date(this.ticks + 24 * e * 60 * 60 * 1e3)
            }, DayPilot.Date.prototype.addHours = function(e) {
                return this.addTime(60 * e * 60 * 1e3)
            }, DayPilot.Date.prototype.addMilliseconds = function(e) {
                return this.addTime(e)
            }, DayPilot.Date.prototype.addMinutes = function(e) {
                return this.addTime(60 * e * 1e3)
            }, DayPilot.Date.prototype.addMonths = function(e) {
                var t = new Date(this.ticks);
                if (0 === e) return this;
                var i = t.getUTCFullYear(),
                    n = t.getUTCMonth() + 1;
                if (e > 0) {
                    for (; e >= 12;) e -= 12, i++;
                    e > 12 - n ? (i++, n = e - (12 - n)) : n += e
                } else {
                    for (; e <= -12;) e += 12, i--;
                    n + e <= 0 ? (i--, n = 12 + n + e) : n += e
                }
                var a = new Date(t.getTime());
                a.setUTCDate(1), a.setUTCFullYear(i), a.setUTCMonth(n - 1);
                var o = new DayPilot.Date(a).daysInMonth();
                return a.setUTCDate(Math.min(o, t.getUTCDate())), new DayPilot.Date(a)
            }, DayPilot.Date.prototype.addSeconds = function(e) {
                return this.addTime(1e3 * e)
            }, DayPilot.Date.prototype.addTime = function(e) {
                return e instanceof DayPilot.Duration && (e = e.ticks), new DayPilot.Date(this.ticks + e)
            }, DayPilot.Date.prototype.addYears = function(e) {
                var t = new Date(this.ticks),
                    i = new Date(this.ticks),
                    n = this.getYear() + e,
                    a = this.getMonth();
                i.setUTCDate(1), i.setUTCFullYear(n), i.setUTCMonth(a);
                var o = new DayPilot.Date(i).daysInMonth();
                return i.setUTCDate(Math.min(o, t.getUTCDate())), new DayPilot.Date(i)
            }, DayPilot.Date.prototype.dayOfWeek = function() {
                return new Date(this.ticks).getUTCDay()
            }, DayPilot.Date.prototype.getDayOfWeek = function() {
                return new Date(this.ticks).getUTCDay()
            }, DayPilot.Date.prototype.daysInMonth = function() {
                var e = new Date(this.ticks),
                    t = e.getUTCMonth() + 1,
                    i = e.getUTCFullYear(),
                    n = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
                return 2 !== t ? n[t - 1] : i % 4 !== 0 ? n[1] : i % 100 === 0 && i % 400 !== 0 ? n[1] : n[1] + 1
            }, DayPilot.Date.prototype.daysInYear = function() {
                var e = this.getYear();
                return e % 4 !== 0 ? 365 : e % 100 === 0 && e % 400 !== 0 ? 365 : 366
            }, DayPilot.Date.prototype.dayOfYear = function() {
                return Math.ceil((this.getDatePart().getTime() - this.firstDayOfYear().getTime()) / 864e5) + 1
            }, DayPilot.Date.prototype.equals = function(e) {
                if (null === e) return !1;
                if (e instanceof DayPilot.Date) return this === e;
                throw "The parameter must be a DayPilot.Date object (DayPilot.Date.equals())"
            }, DayPilot.Date.prototype.firstDayOfMonth = function() {
                var e = new Date;
                return e.setUTCFullYear(this.getYear(), this.getMonth(), 1), e.setUTCHours(0), e.setUTCMinutes(0), e.setUTCSeconds(0), e.setUTCMilliseconds(0), new DayPilot.Date(e)
            }, DayPilot.Date.prototype.firstDayOfYear = function() {
                var e = this.getYear(),
                    t = new Date;
                return t.setUTCFullYear(e, 0, 1), t.setUTCHours(0), t.setUTCMinutes(0), t.setUTCSeconds(0), t.setUTCMilliseconds(0), new DayPilot.Date(t)
            }, DayPilot.Date.prototype.firstDayOfWeek = function(e) {
                var t = this;
                if (e instanceof DayPilot.Locale) e = e.weekStarts;
                else if ("string" == typeof e && DayPilot.Locale.find(e)) {
                    var i = DayPilot.Locale.find(e);
                    e = i.weekStarts
                } else e = e || 0;
                for (var n = t.dayOfWeek(); n !== e;) t = t.addDays(-1), n = t.dayOfWeek();
                return new DayPilot.Date(t)
            }, DayPilot.Date.prototype.getDay = function() {
                return new Date(this.ticks).getUTCDate()
            }, DayPilot.Date.prototype.getDatePart = function() {
                var e = new Date(this.ticks);
                return e.setUTCHours(0), e.setUTCMinutes(0), e.setUTCSeconds(0), e.setUTCMilliseconds(0), new DayPilot.Date(e)
            }, DayPilot.Date.prototype.getYear = function() {
                return new Date(this.ticks).getUTCFullYear()
            }, DayPilot.Date.prototype.getHours = function() {
                return new Date(this.ticks).getUTCHours()
            }, DayPilot.Date.prototype.getMilliseconds = function() {
                return new Date(this.ticks).getUTCMilliseconds()
            }, DayPilot.Date.prototype.getMinutes = function() {
                return new Date(this.ticks).getUTCMinutes()
            }, DayPilot.Date.prototype.getMonth = function() {
                return new Date(this.ticks).getUTCMonth()
            }, DayPilot.Date.prototype.getSeconds = function() {
                return new Date(this.ticks).getUTCSeconds()
            }, DayPilot.Date.prototype.getTotalTicks = function() {
                return this.getTime()
            }, DayPilot.Date.prototype.getTime = function() {
                return this.ticks
            }, DayPilot.Date.prototype.getTimePart = function() {
                var e = this.getDatePart();
                return DayPilot.DateUtil.diff(this, e)
            }, DayPilot.Date.prototype.lastDayOfMonth = function() {
                var e = new Date(this.firstDayOfMonth().getTime()),
                    t = this.daysInMonth();
                return e.setUTCDate(t), new DayPilot.Date(e)
            }, DayPilot.Date.prototype.weekNumber = function() {
                var e = this.firstDayOfYear(),
                    t = (this.getTime() - e.getTime()) / 864e5;
                return Math.ceil((t + e.dayOfWeek() + 1) / 7)
            }, DayPilot.Date.prototype.weekNumberISO = function() {
                var e = !1,
                    t = this.dayOfYear(),
                    i = this.firstDayOfYear().dayOfWeek(),
                    n = this.firstDayOfYear().addYears(1).addDays(-1).dayOfWeek();
                0 === i && (i = 7), 0 === n && (n = 7);
                var a = 8 - i;
                4 !== i && 4 !== n || (e = !0);
                var o = Math.ceil((t - a) / 7),
                    r = o;
                return a >= 4 && (r += 1), r > 52 && !e && (r = 1), 0 === r && (r = this.firstDayOfYear().addDays(-1).weekNumberISO()), r
            }, DayPilot.Date.prototype.toDateLocal = function() {
                var e = new Date(this.ticks),
                    t = new Date;
                return t.setFullYear(e.getUTCFullYear(), e.getUTCMonth(), e.getUTCDate()), t.setHours(e.getUTCHours()), t.setMinutes(e.getUTCMinutes()), t.setSeconds(e.getUTCSeconds()), t.setMilliseconds(e.getUTCMilliseconds()), t
            }, DayPilot.Date.prototype.toDate = function() {
                return new Date(this.ticks)
            }, DayPilot.Date.prototype.toJSON = function() {
                return this.value
            }, DayPilot.Date.prototype.toString = function(e, t) {
                return "undefined" == typeof e ? this.toStringSortable() : new a(e, t).print(this)
            }, DayPilot.Date.prototype.toStringSortable = function() {
                return e(this.ticks)
            }, DayPilot.Date.parse = function(e, t, i) {
                return new a(t, i).parse(e)
            };
            var n = 0;
            DayPilot.Date.today = function() {
                return new DayPilot.Date(DayPilot.DateUtil.localToday())
            }, DayPilot.Date.fromYearMonthDay = function(e, t, i) {
                t = t || 1, i = i || 1;
                var n = new Date(0);
                return n.setUTCFullYear(e), n.setUTCMonth(t - 1), n.setUTCDate(i), new DayPilot.Date(n)
            }, DayPilot.DateUtil = {}, DayPilot.DateUtil.fromStringSortable = function(e, t) {
                if (!e) throw "Can't create DayPilot.Date from an empty string";
                var i = e.length,
                    n = 10 === i,
                    a = 19 === i,
                    o = i > 19;
                if (!n && !a && !o) throw "Invalid string format (use '2010-01-01' or '2010-01-01T00:00:00'): " + e;
                if (DayPilot.Date.Cache.Parsing[e] && !t) return DayPilot.Stats.cacheHitsParsing += 1, DayPilot.Date.Cache.Parsing[e];
                var r = e.substring(0, 4),
                    l = e.substring(5, 7),
                    s = e.substring(8, 10),
                    d = new Date(0);
                if (d.setUTCFullYear(r, l - 1, s), n) return DayPilot.Date.Cache.Parsing[e] = d, d;
                var c = e.substring(11, 13),
                    h = e.substring(14, 16),
                    u = e.substring(17, 19);
                if (d.setUTCHours(c), d.setUTCMinutes(h), d.setUTCSeconds(u), a) return DayPilot.Date.Cache.Parsing[e] = d, d;
                var f = e[19],
                    v = 0;
                if ("." === f) {
                    var p = parseInt(e.substring(20, 23));
                    d.setUTCMilliseconds(p), v = DayPilot.DateUtil.getTzOffsetMinutes(e.substring(23))
                } else v = DayPilot.DateUtil.getTzOffsetMinutes(e.substring(19));
                var g = new DayPilot.Date(d);
                return t || (g = g.addMinutes(-v)), d = g.toDate(), DayPilot.Date.Cache.Parsing[e] = d, d
            }, DayPilot.DateUtil.getTzOffsetMinutes = function(e) {
                if (DayPilot.Util.isNullOrUndefined(e) || "" === e) return 0;
                if ("Z" === e) return 0;
                var t = e[0],
                    i = parseInt(e.substring(1, 3)),
                    n = parseInt(e.substring(4)),
                    a = 60 * i + n;
                if ("-" === t) return -a;
                if ("+" === t) return a;
                throw "Invalid timezone spec: " + e
            }, DayPilot.DateUtil.hasTzSpec = function(e) {
                return !!e.indexOf("+") || !!e.indexOf("-")
            }, DayPilot.DateUtil.daysDiff = function(e, t) {
                if (e && t || function() {
                        throw "two parameters required"
                    }(), e = new DayPilot.Date(e), t = new DayPilot.Date(t), e.getTime() > t.getTime()) return null;
                for (var i = 0, n = e.getDatePart(), a = t.getDatePart(); n < a;) n = n.addDays(1), i++;
                return i
            }, DayPilot.DateUtil.daysSpan = function(e, t) {
                if (e && t || function() {
                        throw "two parameters required"
                    }(), e = new DayPilot.Date(e), t = new DayPilot.Date(t), e === t) return 0;
                var i = DayPilot.DateUtil.daysDiff(e, t);
                return t == t.getDatePart() && i--, i
            }, DayPilot.DateUtil.diff = function(e, t) {
                if (!(e && t && e.getTime && t.getTime)) throw "Both compared objects must be Date objects (DayPilot.Date.diff).";
                return e.getTime() - t.getTime()
            }, DayPilot.DateUtil.fromLocal = function(e) {
                e || (e = new Date);
                var t = new Date;
                return t.setUTCFullYear(e.getFullYear(), e.getMonth(), e.getDate()), t.setUTCHours(e.getHours()), t.setUTCMinutes(e.getMinutes()), t.setUTCSeconds(e.getSeconds()), t.setUTCMilliseconds(e.getMilliseconds()), t
            }, DayPilot.DateUtil.localToday = function() {
                var e = new Date;
                return e.setUTCHours(0), e.setUTCMinutes(0), e.setUTCSeconds(0), e.setUTCMilliseconds(0), e
            }, DayPilot.DateUtil.hours = function(e, t) {
                var i = e.getUTCMinutes();
                i < 10 && (i = "0" + i);
                var n = e.getUTCHours();
                if (t) {
                    var a = n < 12,
                        n = n % 12;
                    0 === n && (n = 12);
                    return n + ":" + i + " " + (a ? "AM" : "PM")
                }
                return n + ":" + i
            }, DayPilot.DateUtil.max = function(e, t) {
                return e.getTime() > t.getTime() ? e : t
            }, DayPilot.DateUtil.min = function(e, t) {
                return e.getTime() < t.getTime() ? e : t
            };
            var a = function(e, i) {
                "string" == typeof i && (i = DayPilot.Locale.find(i));
                var i = i || DayPilot.Locale.US,
                    n = [{
                        "seq": "yyyy",
                        "expr": "[0-9]{4,4}",
                        "str": function(e) {
                            return e.getYear()
                        }
                    }, {
                        "seq": "yy",
                        "expr": "[0-9]{2,2}",
                        "str": function(e) {
                            return e.getYear() % 100
                        }
                    }, {
                        "seq": "mm",
                        "expr": "[0-9]{2,2}",
                        "str": function(e) {
                            var t = e.getMinutes();
                            return t < 10 ? "0" + t : t
                        }
                    }, {
                        "seq": "m",
                        "expr": "[0-9]{1,2}",
                        "str": function(e) {
                            return e.getMinutes()
                        }
                    }, {
                        "seq": "HH",
                        "expr": "[0-9]{2,2}",
                        "str": function(e) {
                            var t = e.getHours();
                            return t < 10 ? "0" + t : t
                        }
                    }, {
                        "seq": "H",
                        "expr": "[0-9]{1,2}",
                        "str": function(e) {
                            return e.getHours()
                        }
                    }, {
                        "seq": "hh",
                        "expr": "[0-9]{2,2}",
                        "str": function(e) {
                            var t = e.getHours(),
                                t = t % 12;
                            0 === t && (t = 12);
                            var i = t;
                            return i < 10 ? "0" + i : i
                        }
                    }, {
                        "seq": "h",
                        "expr": "[0-9]{1,2}",
                        "str": function(e) {
                            var t = e.getHours(),
                                t = t % 12;
                            return 0 === t && (t = 12), t
                        }
                    }, {
                        "seq": "ss",
                        "expr": "[0-9]{2,2}",
                        "str": function(e) {
                            var t = e.getSeconds();
                            return t < 10 ? "0" + t : t
                        }
                    }, {
                        "seq": "s",
                        "expr": "[0-9]{1,2}",
                        "str": function(e) {
                            return e.getSeconds()
                        }
                    }, {
                        "seq": "MMMM",
                        "expr": "[^\\s0-9]*",
                        "str": function(e) {
                            return i.monthNames[e.getMonth()]
                        },
                        "transform": function(e) {
                            var n = DayPilot.indexOf(i.monthNames, e, t);
                            return n < 0 ? null : n + 1
                        }
                    }, {
                        "seq": "MMM",
                        "expr": "[^\\s0-9]*",
                        "str": function(e) {
                            return i.monthNamesShort[e.getMonth()]
                        },
                        "transform": function(e) {
                            var n = DayPilot.indexOf(i.monthNamesShort, e, t);
                            return n < 0 ? null : n + 1
                        }
                    }, {
                        "seq": "MM",
                        "expr": "[0-9]{2,2}",
                        "str": function(e) {
                            var t = e.getMonth() + 1;
                            return t < 10 ? "0" + t : t
                        }
                    }, {
                        "seq": "M",
                        "expr": "[0-9]{1,2}",
                        "str": function(e) {
                            return e.getMonth() + 1
                        }
                    }, {
                        "seq": "dddd",
                        "expr": "[^\\s0-9]*",
                        "str": function(e) {
                            return i.dayNames[e.getDayOfWeek()]
                        }
                    }, {
                        "seq": "ddd",
                        "expr": "[^\\s0-9]*",
                        "str": function(e) {
                            return i.dayNamesShort[e.getDayOfWeek()]
                        }
                    }, {
                        "seq": "dd",
                        "expr": "[0-9]{2,2}",
                        "str": function(e) {
                            var t = e.getDay();
                            return t < 10 ? "0" + t : t
                        }
                    }, {
                        "seq": "%d",
                        "expr": "[0-9]{1,2}",
                        "str": function(e) {
                            return e.getDay()
                        }
                    }, {
                        "seq": "d",
                        "expr": "[0-9]{1,2}",
                        "str": function(e) {
                            return e.getDay()
                        }
                    }, {
                        "seq": "tt",
                        "expr": "(AM|PM|am|pm)",
                        "str": function(e) {
                            return e.getHours() < 12 ? "AM" : "PM"
                        },
                        "transform": function(e) {
                            return e.toUpperCase()
                        }
                    }],
                    a = function(e) {
                        return e.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, "\\$&")
                    };
                this.init = function() {
                    this.year = this.findSequence("yyyy"), this.month = this.findSequence("MMMM") || this.findSequence("MMM") || this.findSequence("MM") || this.findSequence("M"), this.day = this.findSequence("dd") || this.findSequence("d"), this.hours = this.findSequence("HH") || this.findSequence("H"), this.minutes = this.findSequence("mm") || this.findSequence("m"), this.seconds = this.findSequence("ss") || this.findSequence("s"), this.ampm = this.findSequence("tt"), this.hours12 = this.findSequence("hh") || this.findSequence("h")
                }, this.findSequence = function(t) {
                    function i(e) {
                        return parseInt(e)
                    }
                    return e.indexOf(t) === -1 ? null : {
                        "findValue": function(o) {
                            for (var r = a(e), l = null, s = 0; s < n.length; s++) {
                                var d = (n[s].length, t === n[s].seq),
                                    c = n[s].expr;
                                d && (c = "(" + c + ")", l = n[s].transform), r = r.replace(n[s].seq, c)
                            }
                            r = "^" + r + "$";
                            try {
                                var h = new RegExp(r),
                                    u = h.exec(o);
                                return u ? (l = l || i)(u[1]) : null
                            } catch (e) {
                                throw "unable to create regex from: " + r
                            }
                        }
                    }
                }, this.print = function(t) {
                    for (var i = function(e) {
                            for (var t = 0; t < n.length; t++)
                                if (n[t] && n[t].seq === e) return n[t];
                            return null
                        }, a = e.length <= 0, o = 0, r = []; !a;) {
                        var l = e.substring(o),
                            s = /%?(.)\1*/.exec(l);
                        if (s && s.length > 0) {
                            var d = s[0],
                                c = i(d);
                            c ? r.push(c) : r.push(d), o += d.length, a = e.length <= o
                        } else a = !0
                    }
                    for (var h = 0; h < r.length; h++) {
                        var u = r[h];
                        "string" != typeof u && (r[h] = u.str(t))
                    }
                    return r.join("")
                }, this.parse = function(e) {
                    var t = this.year.findValue(e);
                    if (!t) return null;
                    var i = this.month.findValue(e);
                    if (DayPilot.Util.isNullOrUndefined(i)) return null;
                    if (i > 12 || i < 1) return null;
                    var n = this.day.findValue(e),
                        a = DayPilot.Date.fromYearMonthDay(t, i).daysInMonth();
                    if (n < 1 || n > a) return null;
                    var o = this.hours ? this.hours.findValue(e) : 0,
                        r = this.minutes ? this.minutes.findValue(e) : 0,
                        l = this.seconds ? this.seconds.findValue(e) : 0,
                        s = this.ampm ? this.ampm.findValue(e) : null;
                    if (this.ampm && this.hours12) {
                        var d = this.hours12.findValue(e);
                        if (d < 1 || d > 12) return null;
                        o = "PM" === s ? 12 === d ? 12 : d + 12 : 12 === d ? 0 : d
                    }
                    if (o < 0 || o > 23) return null;
                    if (r < 0 || r > 59) return null;
                    if (l < 0 || l > 59) return null;
                    var c = new Date;
                    return c.setUTCFullYear(t, i - 1, n), c.setUTCHours(o), c.setUTCMinutes(r), c.setUTCSeconds(l), c.setUTCMilliseconds(0), new DayPilot.Date(c)
                }, this.init()
            };
            DayPilot.Canvas = function(e, t, i, n, a) {
                    function o(e, t, i) {
                        for (var n = t.split(" "), a = [], o = n[0], r = 1; r < n.length; r++) {
                            var l = n[r];
                            e.measureText(o + " " + l).width < i ? o += " " + l : (a.push(o), o = l)
                        }
                        return a.push(o), a
                    }
                    var n = n || 1,
                        r = document.createElement("canvas");
                    r.width = e * n, r.height = t * n;
                    var l = r.getContext("2d");
                    l.scale(n, n), this.defaultFileName = "image.png", "image/jpeg" === i && (this.defaultFileName = "image.jpg", a = a || .92), this.fillRect = function(e, t) {
                        l.save(), l.strokeStyle = "rgb(0,0,0,0)", l.fillStyle = t, l.fillRect(e.x + .5, e.y + .5, e.w - .5, e.h - .5), l.restore()
                    }, this.rect = function(e, t) {
                        l.save(), l.strokeStyle = t, l.beginPath(), l.rect(e.x + .5, e.y + .5, e.w - 1, e.h - 1), l.stroke(), l.restore()
                    }, this.text = function(e, t, i, n, a, r) {
                        if (t) {
                            if ("number" == typeof t && (t = "" + t), "string" != typeof t) throw "String expected, supplied: " + typeof t;
                            a = a || "left", r = r || 0, n = n || "#000", l.save();
                            var s = parseInt(i.size),
                                d = DayPilot.browser.ff ? 3 : 0;
                            d += .2 * s, d += DayPilot.browser.chrome ? 1 : 0, d += DayPilot.browser.ie ? 1 : 0, l.rect(e.x + .5 + r, e.y + .5 + r, e.w - 1 - r, e.h - 1 - r), l.clip(), l.fillStyle = n, l.font = i.style + " " + i.size + " " + i.family, l.textBaseline = "top", l.textAlign = a;
                            var c = e.x;
                            switch (a) {
                                case "center":
                                    c += e.w / 2;
                                    break;
                                case "right":
                                    c += e.w;
                            }
                            var h = 0;
                            c += h;
                            var u = 1.2 * parseInt(i.size),
                                f = t.split("\n"),
                                v = [];
                            DayPilot.list(f).each(function(t) {
                                var i = o(l, t, e.w);
                                DayPilot.list(i).each(function(e) {
                                    v.push(e)
                                })
                            });
                            for (var p = e.y + h + r + d, g = 0; g < v.length; g++) {
                                var m = v[g];
                                l.fillText(m, c + r, p), p += u
                            }
                            l.restore()
                        }
                    }, this.line = function(e, t, i, n, a) {
                        l.save(), l.lineWidth = 1, l.strokeStyle = a, l.beginPath(), l.moveTo(e + .5, t + .5), l.lineTo(i + .5, n + .5), l.stroke(), l.restore()
                    }, this.image = function(e, t) {
                        if (t) {
                            if ("string" == typeof t) {
                                var i = t,
                                    t = document.createElement("img");
                                if (0 !== i.indexOf("data:")) return t.onload = function() {
                                    l.save(), l.drawImage(t, e.x, e.y), l.restore()
                                }, void(t.src = i);
                                t.src = i
                            }
                            l.save(), l.drawImage(t, e.x, e.y), l.restore()
                        }
                    }, this.getWidth = function() {
                        return r.width
                    }, this.getHeight = function() {
                        return r.height
                    }, this.getElement = function() {
                        return r
                    }, this.getSource = function() {
                        return "<img src='" + r.toDataURL(i, a) + "' />"
                    }, this.getDataUri = function() {
                        return r.toDataURL(i, a)
                    }, this.getDefaultFileName = function() {
                        return this.defaultFileName
                    }, this.getBlob = function() {
                        return r.msToBlob ? r.msToBlob() : DayPilot.Util.dataUriToBlob(this.getDataUri())
                    }
                }, DayPilot.Svg = function(e, t) {
                    function i(e, t) {
                        var i = null;
                        if (e) {
                            if ("string" == typeof e) {
                                if (0 === e.indexOf("data:")) {
                                    var n = {};
                                    n.dataUri = e, t(n)
                                }
                                i = e
                            } else i = e.src;
                            var e = document.createElement("img");
                            e.onload = function() {
                                var e = document.createElement("canvas");
                                e.width = this.naturalWidth, e.height = this.naturalHeight, e.getContext("2d").drawImage(this, 0, 0);
                                var i = {};
                                i.dataUri = e.toDataURL("image/png"), t(i)
                            }, e.src = i
                        }
                    }
                    var n = "http://www.w3.org/2000/svg",
                        a = document.createElementNS(n, "svg");
                    a.outerHTML && a.setAttribute("xmlns", n), a.setAttribute("viewBox", "0 0 " + e + " " + t), this.defaultFileName = "image.svg", this.fillRect = function(e, t) {
                        var i = document.createElementNS(n, "rect");
                        i.setAttribute("x", e.x + .5), i.setAttribute("y", e.y + .5), i.setAttribute("width", e.w - 1), i.setAttribute("height", e.h - 1), i.setAttribute("fill", t), a.appendChild(i)
                    }, this.rect = function(e, t) {
                        var i = document.createElementNS(n, "rect");
                        i.setAttribute("x", e.x + .5), i.setAttribute("y", e.y + .5), i.setAttribute("width", e.w - 1), i.setAttribute("height", e.h - 1), i.setAttribute("stroke", t), i.setAttribute("stroke-width", "1"), i.setAttribute("fill", "transparent"), a.appendChild(i)
                    }, this.text = function(e, t, i, o, r, l) {
                        function s(e, t, r) {
                            var l = document.createElementNS(n, "text");
                            l.setAttribute("fill", o), l.setAttribute("font-family", i.family), l.setAttribute("font-size", i.size), l.setAttribute("font-style", i.style), l.setAttribute("x", t), l.setAttribute("y", r), l.setAttribute("dy", "1em"), l.setAttribute("text-anchor", h), l.setAttribute("clip-path", "url(#" + u.id + ")");
                            var s = document.createTextNode(e);
                            l.appendChild(s), a.appendChild(l)
                        }
                        if ("number" == typeof t && (t = "" + t), "string" != typeof t) throw "String expected";
                        var r = r || "left",
                            l = l || 0,
                            d = 3,
                            c = e.x,
                            h = "start";
                        switch (r) {
                            case "center":
                                c += e.w / 2, h = "middle";
                                break;
                            case "right":
                                c += e.w, h = "end"
                        }
                        var u = document.createElementNS(n, "clipPath");
                        u.id = "clip" + DayPilot.guid();
                        var f = document.createElementNS(n, "rect");
                        f.setAttribute("x", e.x), f.setAttribute("y", e.y), f.setAttribute("width", e.w), f.setAttribute("height", e.h), u.appendChild(f), a.appendChild(u);
                        for (var v = 1.2 * parseInt(i.size), p = t.split("\n"), g = e.y + l + d, m = 0; m < p.length; m++) {
                            s(p[m], c + l, g), g += v
                        }
                    }, this.line = function(e, t, i, o, r) {
                        var l = document.createElementNS(n, "line");
                        l.setAttribute("x1", e + .5), l.setAttribute("y1", t + .5), l.setAttribute("x2", i + .5), l.setAttribute("y2", o + .5), l.setAttribute("stroke", r), a.appendChild(l)
                    }, this.image = function(e, t) {
                        function o(t) {
                            var i = document.createElementNS(n, "image");
                            i.setAttributeNS("http://www.w3.org/1999/xlink", "href", t.dataUri), i.setAttribute("x", e.x), i.setAttribute("y", e.y), i.setAttribute("width", e.w), i.setAttribute("height", e.h), a.appendChild(i)
                        }
                        i(t, o)
                    }, this.getWidth = function() {
                        return e
                    }, this.getHeight = function() {
                        return t
                    }, this.getElement = function() {
                        return a
                    }, this.getSource = function() {
                        if (a.outerHTML) return a.outerHTML;
                        var e = document.createElement("div"),
                            t = a.cloneNode(!0);
                        return e.appendChild(t), e.innerHTML
                    }, this.getDataUri = function() {
                        return "data:application/octet-stream," + encodeURIComponent(this.getSource())
                    }, this.getDefaultFileName = function() {
                        return this.defaultFileName
                    }, this.getBlob = function() {
                        return new Blob([this.getSource()])
                    }
                }, DayPilot.Excel = function() {
                    var e = this,
                        t = null,
                        i = null,
                        n = null,
                        a = {};
                    a.xmlns = "urn:schemas-microsoft-com:office:spreadsheet", a.o = "urn:schemas-microsoft-com:office:office", a.x = "urn:schemas-microsoft-com:office:excel", a.ss = "urn:schemas-microsoft-com:office:spreadsheet", a.html = "http://www.w3.org/TR/REC-html40", this.init = function() {
                        t = document.implementation.createDocument(a.xmlns, "Workbook", null), t.documentElement.setAttribute("xmlns:o", a.o), t.documentElement.setAttribute("xmlns:x", a.x), t.documentElement.setAttribute("xmlns:ss", a.ss), t.documentElement.setAttribute("xmlns:html", a.html);
                        var o = t.createElement("x:ExcelWorkbook");
                        t.documentElement.appendChild(o), i = t.createElement("ss:Styles"), t.documentElement.appendChild(i), n = e.styles.create(), n.setId("Default"), n.setName("Normal")
                    }, this.worksheets = [], this.worksheets.create = function(t) {
                        var i = e.el(a.ss, "Worksheet"),
                            n = new o(i);
                        return n.setName(t), this.push(n), n
                    }, this.styles = [], this.styles.create = function(e) {
                        var n = t.createElement("ss:Style");
                        i.appendChild(n);
                        var a = new r(n);
                        return this.push(a), a
                    }, this.styles.getDefault = function() {
                        return n
                    }, this.el = function(e, i) {
                        var n = t.createElementNS(e, i);
                        return t.documentElement.appendChild(n), n
                    }, this.getSource = function() {
                        return '<?xml version="1.0"?>' + (new XMLSerializer).serializeToString(t)
                    }, this.getDataUri = function() {
                        return "data:application/vnd.ms-excel," + encodeURIComponent(this.getSource())
                    }, this.getElement = function() {
                        return t
                    }, this.getDefaultFileName = function() {
                        return "spreadsheet.xls"
                    }, this.getBlob = function() {
                        return new Blob([this.getSource()])
                    };
                    var o = function(e) {
                            var i = t.createElement("Table");
                            e.appendChild(i);
                            var n = t.createElement("x:WorksheetOptions");
                            e.appendChild(n), this.rows = [], this.setName = function(t) {
                                e.setAttribute("ss:Name", t)
                            }, this.cell = function(e, n) {
                                for (; n >= this.rows.length;) {
                                    var o = t.createElement("Row");
                                    i.appendChild(o), this.rows.push(new a(o))
                                }
                                return this.rows[n].cell(e)
                            }, this.enableGridlines = function(e) {
                                var i = DayPilot.list(n.childNodes).find(function(e) {
                                    return "x:DoNotDisplayGridlines" === e.tagName
                                });
                                if (!i && !e) {
                                    var a = t.createElement("x:DoNotDisplayGridlines");
                                    n.appendChild(a)
                                }
                                i && e && n.removeChild(i)
                            };
                            var a = function(e) {
                                this.cells = [], this.getElement = function() {
                                    return e
                                }, this.cell = function(n) {
                                    for (; n >= this.cells.length;) {
                                        var a = t.createElement("Cell");
                                        e.appendChild(a), this.cells.push(new i(a))
                                    }
                                    return this.cells[n]
                                };
                                var i = function(e) {
                                    this.setText = function(i) {
                                        var n = t.createElement("Data");
                                        n.setAttribute("ss:Type", "String");
                                        var a = t.createTextNode(i);
                                        for (n.appendChild(a); e.firstChild;) e.removeChild(e.firstChild);
                                        return e.appendChild(n), this
                                    }, this.setColspan = function(t) {
                                        return t > 1 ? e.setAttribute("ss:MergeAcross", t - 1) : e.removeAttribute("ss:MergeAcross"), this
                                    }, this.setRowspan = function(t) {
                                        return t > 1 ? e.setAttribute("ss:MergeDown", t - 1) : e.removeAttribute("ss:MergeDown"), this
                                    }, this.setStyle = function(t) {
                                        if (!(t instanceof r)) throw "Invalid argument, Style expected";
                                        return e.setAttribute("ss:StyleID", t.getId()), this
                                    }
                                }
                            }
                        },
                        r = function(e) {
                            var i = DayPilot.guid();
                            e.setAttribute("ss:ID", i);
                            var n = t.createElement("ss:Alignment");
                            e.appendChild(n);
                            var a = t.createElement("ss:Interior");
                            e.appendChild(a);
                            var o = t.createElement("ss:Borders");
                            e.appendChild(o), this.setHorizontalAlignment = function(e) {
                                n.setAttribute("ss:Horizontal", e)
                            }, this.setVerticalAlignment = function(e) {
                                n.setAttribute("ss:Vertical", e)
                            }, this.setBackColor = function(e) {
                                var t = DayPilot.Util.normalizeColor(e);
                                a.setAttribute("ss:Color", t), a.setAttribute("ss:Pattern", "Solid")
                            }, this.setBorderColor = function(e) {
                                function i(e, i) {
                                    var n = t.createElement("ss:Border");
                                    return n.setAttribute("ss:Position", e), n.setAttribute("ss:LineStyle", "Continuous"), n.setAttribute("ss:Color", DayPilot.Util.normalizeColor(i)), n
                                }
                                this.clearBorders();
                                var n = i("Left", e);
                                o.appendChild(n);
                                var a = i("Right", e);
                                o.appendChild(a);
                                var r = i("Top", e);
                                o.appendChild(r);
                                var l = i("Bottom", e);
                                o.appendChild(l)
                            }, this.clearBorders = function() {
                                for (; o.firstChild;) o.removeChild(o.firstChild)
                            }, this.getId = function() {
                                return i
                            }, this.setId = function(t) {
                                i = t, e.setAttribute("ss:ID", i)
                            }, this.setName = function(t) {
                                e.setAttribute("ss:Name", t)
                            }
                        };
                    this.init()
                }, DayPilot.Export = function(e) {
                    this.toElement = function() {
                        return e.getElement()
                    }, this.dimensions = function() {
                        return {
                            "width": e.getWidth(),
                            "height": e.getHeight()
                        }
                    }, this.toHtml = function() {
                        return e.getSource()
                    }, this.toDataUri = function() {
                        return e.getDataUri()
                    }, this.toBlob = function() {
                        return e.getBlob()
                    }, this.print = function(t) {
                        t = t || {}, t.orientation = t.orientation || "portrait";
                        var i = document.createElement("iframe");
                        i.setAttribute("width", 0), i.setAttribute("height", 0), i.setAttribute("frameborder", 0), i.setAttribute("src", "about:blank"), i.onload = function() {
                            var n = i.contentWindow.document;
                            if (n.body.appendChild(e.getElement()), "landscape" === t.orientation) {
                                var a = DayPilot.sheet(n);
                                a.add("@page", "size: landscape;"), a.commit()
                            }
                            i.contentWindow.document.execCommand("print", !1, null) || (i.contentWindow.focus(), i.contentWindow.print()), setTimeout(function() {
                                document.body.removeChild(i)
                            }, 10)
                        }, document.body.appendChild(i)
                    }, this.download = function(t) {
                        var t = t || e.getDefaultFileName(),
                            i = e.getBlob();
                        DayPilot.Util.downloadBlob(i, t)
                    }
                }, DayPilot.Exception = function(e) {
                    return new Error(e)
                }, DayPilot.Locale = function(e, t) {
                    if (this.id = e, this.dayNames = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"], this.dayNamesShort = ["Su", "Mo", "Tu", "We", "Th", "Fr", "Sa"], this.monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"], this.monthNamesShort = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"], this.datePattern = "M/d/yyyy", this.timePattern = "H:mm", this.dateTimePattern = "M/d/yyyy H:mm", this.timeFormat = "Clock12Hours", this.weekStarts = 0, t)
                        for (var i in t) this[i] = t[i]
                }, DayPilot.Locale.all = {}, DayPilot.Locale.find = function(e) {
                    if (!e) return null;
                    var t = e.toLowerCase();
                    return t.length > 2 && (t = DayPilot.Util.replaceCharAt(t, 2, "-")), DayPilot.Locale.all[t]
                }, DayPilot.Locale.register = function(e) {
                    DayPilot.Locale.all[e.id] = e
                }, DayPilot.Locale.register(new DayPilot.Locale("ca-es", {
                    "dayNames": ["diumenge", "dilluns", "dimarts", "dimecres", "dijous", "divendres", "dissabte"],
                    "dayNamesShort": ["dg", "dl", "dt", "dc", "dj", "dv", "ds"],
                    "monthNames": ["gener", "febrer", "març", "abril", "maig", "juny", "juliol", "agost", "setembre", "octubre", "novembre", "desembre", ""],
                    "monthNamesShort": ["gen.", "febr.", "març", "abr.", "maig", "juny", "jul.", "ag.", "set.", "oct.", "nov.", "des.", ""],
                    "timePattern": "H:mm",
                    "datePattern": "dd/MM/yyyy",
                    "dateTimePattern": "dd/MM/yyyy H:mm",
                    "timeFormat": "Clock24Hours",
                    "weekStarts": 1
                })), DayPilot.Locale.register(new DayPilot.Locale("cs-cz", {
                    "dayNames": ["neděle", "pondělí", "úterý", "středa", "čtvrtek", "pátek", "sobota"],
                    "dayNamesShort": ["ne", "po", "út", "st", "čt", "pá", "so"],
                    "monthNames": ["leden", "únor", "březen", "duben", "květen", "červen", "červenec", "srpen", "září", "říjen", "listopad", "prosinec", ""],
                    "monthNamesShort": ["I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX", "X", "XI", "XII", ""],
                    "timePattern": "H:mm",
                    "datePattern": "d. M. yyyy",
                    "dateTimePattern": "d. M. yyyy H:mm",
                    "timeFormat": "Clock24Hours",
                    "weekStarts": 1
                })), DayPilot.Locale.register(new DayPilot.Locale("da-dk", {
                    "dayNames": ["søndag", "mandag", "tirsdag", "onsdag", "torsdag", "fredag", "lørdag"],
                    "dayNamesShort": ["sø", "ma", "ti", "on", "to", "fr", "lø"],
                    "monthNames": ["januar", "februar", "marts", "april", "maj", "juni", "juli", "august", "september", "oktober", "november", "december", ""],
                    "monthNamesShort": ["jan", "feb", "mar", "apr", "maj", "jun", "jul", "aug", "sep", "okt", "nov", "dec", ""],
                    "timePattern": "HH:mm",
                    "datePattern": "dd-MM-yyyy",
                    "dateTimePattern": "dd-MM-yyyy HH:mm",
                    "timeFormat": "Clock24Hours",
                    "weekStarts": 1
                })), DayPilot.Locale.register(new DayPilot.Locale("de-at", {
                    "dayNames": ["Sonntag", "Montag", "Dienstag", "Mittwoch", "Donnerstag", "Freitag", "Samstag"],
                    "dayNamesShort": ["So", "Mo", "Di", "Mi", "Do", "Fr", "Sa"],
                    "monthNames": ["Jänner", "Februar", "März", "April", "Mai", "Juni", "Juli", "August", "September", "Oktober", "November", "Dezember", ""],
                    "monthNamesShort": ["Jän", "Feb", "Mär", "Apr", "Mai", "Jun", "Jul", "Aug", "Sep", "Okt", "Nov", "Dez", ""],
                    "timePattern": "HH:mm",
                    "datePattern": "dd.MM.yyyy",
                    "dateTimePattern": "dd.MM.yyyy HH:mm",
                    "timeFormat": "Clock24Hours",
                    "weekStarts": 1
                })), DayPilot.Locale.register(new DayPilot.Locale("de-ch", {
                    "dayNames": ["Sonntag", "Montag", "Dienstag", "Mittwoch", "Donnerstag", "Freitag", "Samstag"],
                    "dayNamesShort": ["So", "Mo", "Di", "Mi", "Do", "Fr", "Sa"],
                    "monthNames": ["Januar", "Februar", "März", "April", "Mai", "Juni", "Juli", "August", "September", "Oktober", "November", "Dezember", ""],
                    "monthNamesShort": ["Jan", "Feb", "Mrz", "Apr", "Mai", "Jun", "Jul", "Aug", "Sep", "Okt", "Nov", "Dez", ""],
                    "timePattern": "HH:mm",
                    "datePattern": "dd.MM.yyyy",
                    "dateTimePattern": "dd.MM.yyyy HH:mm",
                    "timeFormat": "Clock24Hours",
                    "weekStarts": 1
                })), DayPilot.Locale.register(new DayPilot.Locale("de-de", {
                    "dayNames": ["Sonntag", "Montag", "Dienstag", "Mittwoch", "Donnerstag", "Freitag", "Samstag"],
                    "dayNamesShort": ["So", "Mo", "Di", "Mi", "Do", "Fr", "Sa"],
                    "monthNames": ["Januar", "Februar", "März", "April", "Mai", "Juni", "Juli", "August", "September", "Oktober", "November", "Dezember", ""],
                    "monthNamesShort": ["Jan", "Feb", "Mrz", "Apr", "Mai", "Jun", "Jul", "Aug", "Sep", "Okt", "Nov", "Dez", ""],
                    "timePattern": "HH:mm",
                    "datePattern": "dd.MM.yyyy",
                    "dateTimePattern": "dd.MM.yyyy HH:mm",
                    "timeFormat": "Clock24Hours",
                    "weekStarts": 1
                })), DayPilot.Locale.register(new DayPilot.Locale("de-lu", {
                    "dayNames": ["Sonntag", "Montag", "Dienstag", "Mittwoch", "Donnerstag", "Freitag", "Samstag"],
                    "dayNamesShort": ["So", "Mo", "Di", "Mi", "Do", "Fr", "Sa"],
                    "monthNames": ["Januar", "Februar", "März", "April", "Mai", "Juni", "Juli", "August", "September", "Oktober", "November", "Dezember", ""],
                    "monthNamesShort": ["Jan", "Feb", "Mrz", "Apr", "Mai", "Jun", "Jul", "Aug", "Sep", "Okt", "Nov", "Dez", ""],
                    "timePattern": "HH:mm",
                    "datePattern": "dd.MM.yyyy",
                    "dateTimePattern": "dd.MM.yyyy HH:mm",
                    "timeFormat": "Clock24Hours",
                    "weekStarts": 1
                })), DayPilot.Locale.register(new DayPilot.Locale("en-au", {
                    "dayNames": ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
                    "dayNamesShort": ["Su", "Mo", "Tu", "We", "Th", "Fr", "Sa"],
                    "monthNames": ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December", ""],
                    "monthNamesShort": ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec", ""],
                    "timePattern": "h:mm tt",
                    "datePattern": "d/MM/yyyy",
                    "dateTimePattern": "d/MM/yyyy h:mm tt",
                    "timeFormat": "Clock12Hours",
                    "weekStarts": 1
                })), DayPilot.Locale.register(new DayPilot.Locale("en-ca", {
                    "dayNames": ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
                    "dayNamesShort": ["Su", "Mo", "Tu", "We", "Th", "Fr", "Sa"],
                    "monthNames": ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December", ""],
                    "monthNamesShort": ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec", ""],
                    "timePattern": "h:mm tt",
                    "datePattern": "yyyy-MM-dd",
                    "dateTimePattern": "yyyy-MM-dd h:mm tt",
                    "timeFormat": "Clock12Hours",
                    "weekStarts": 0
                })), DayPilot.Locale.register(new DayPilot.Locale("en-gb", {
                    "dayNames": ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
                    "dayNamesShort": ["Su", "Mo", "Tu", "We", "Th", "Fr", "Sa"],
                    "monthNames": ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December", ""],
                    "monthNamesShort": ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec", ""],
                    "timePattern": "HH:mm",
                    "datePattern": "dd/MM/yyyy",
                    "dateTimePattern": "dd/MM/yyyy HH:mm",
                    "timeFormat": "Clock24Hours",
                    "weekStarts": 1
                })), DayPilot.Locale.register(new DayPilot.Locale("en-us", {
                    "dayNames": ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
                    "dayNamesShort": ["Su", "Mo", "Tu", "We", "Th", "Fr", "Sa"],
                    "monthNames": ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December", ""],
                    "monthNamesShort": ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec", ""],
                    "timePattern": "h:mm tt",
                    "datePattern": "M/d/yyyy",
                    "dateTimePattern": "M/d/yyyy h:mm tt",
                    "timeFormat": "Clock12Hours",
                    "weekStarts": 0
                })), DayPilot.Locale.register(new DayPilot.Locale("es-es", {
                    "dayNames": ["domingo", "lunes", "martes", "miércoles", "jueves", "viernes", "sábado"],
                    "dayNamesShort": ["D", "L", "M", "X", "J", "V", "S"],
                    "monthNames": ["enero", "febrero", "marzo", "abril", "mayo", "junio", "julio", "agosto", "septiembre", "octubre", "noviembre", "diciembre", ""],
                    "monthNamesShort": ["ene.", "feb.", "mar.", "abr.", "may.", "jun.", "jul.", "ago.", "sep.", "oct.", "nov.", "dic.", ""],
                    "timePattern": "H:mm",
                    "datePattern": "dd/MM/yyyy",
                    "dateTimePattern": "dd/MM/yyyy H:mm",
                    "timeFormat": "Clock24Hours",
                    "weekStarts": 1
                })), DayPilot.Locale.register(new DayPilot.Locale("es-mx", {
                    "dayNames": ["domingo", "lunes", "martes", "miércoles", "jueves", "viernes", "sábado"],
                    "dayNamesShort": ["do.", "lu.", "ma.", "mi.", "ju.", "vi.", "sá."],
                    "monthNames": ["enero", "febrero", "marzo", "abril", "mayo", "junio", "julio", "agosto", "septiembre", "octubre", "noviembre", "diciembre", ""],
                    "monthNamesShort": ["ene.", "feb.", "mar.", "abr.", "may.", "jun.", "jul.", "ago.", "sep.", "oct.", "nov.", "dic.", ""],
                    "timePattern": "hh:mm tt",
                    "datePattern": "dd/MM/yyyy",
                    "dateTimePattern": "dd/MM/yyyy hh:mm tt",
                    "timeFormat": "Clock12Hours",
                    "weekStarts": 0
                })), DayPilot.Locale.register(new DayPilot.Locale("eu-es", {
                    "dayNames": ["igandea", "astelehena", "asteartea", "asteazkena", "osteguna", "ostirala", "larunbata"],
                    "dayNamesShort": ["ig", "al", "as", "az", "og", "or", "lr"],
                    "monthNames": ["urtarrila", "otsaila", "martxoa", "apirila", "maiatza", "ekaina", "uztaila", "abuztua", "iraila", "urria", "azaroa", "abendua", ""],
                    "monthNamesShort": ["urt.", "ots.", "mar.", "api.", "mai.", "eka.", "uzt.", "abu.", "ira.", "urr.", "aza.", "abe.", ""],
                    "timePattern": "H:mm",
                    "datePattern": "yyyy/MM/dd",
                    "dateTimePattern": "yyyy/MM/dd H:mm",
                    "timeFormat": "Clock24Hours",
                    "weekStarts": 1
                })), DayPilot.Locale.register(new DayPilot.Locale("fi-fi", {
                    "dayNames": ["sunnuntai", "maanantai", "tiistai", "keskiviikko", "torstai", "perjantai", "lauantai"],
                    "dayNamesShort": ["su", "ma", "ti", "ke", "to", "pe", "la"],
                    "monthNames": ["tammikuu", "helmikuu", "maaliskuu", "huhtikuu", "toukokuu", "kesäkuu", "heinäkuu", "elokuu", "syyskuu", "lokakuu", "marraskuu", "joulukuu", ""],
                    "monthNamesShort": ["tammi", "helmi", "maalis", "huhti", "touko", "kesä", "heinä", "elo", "syys", "loka", "marras", "joulu", ""],
                    "timePattern": "H:mm",
                    "datePattern": "d.M.yyyy",
                    "dateTimePattern": "d.M.yyyy H:mm",
                    "timeFormat": "Clock24Hours",
                    "weekStarts": 1
                })), DayPilot.Locale.register(new DayPilot.Locale("fr-be", {
                    "dayNames": ["dimanche", "lundi", "mardi", "mercredi", "jeudi", "vendredi", "samedi"],
                    "dayNamesShort": ["di", "lu", "ma", "me", "je", "ve", "sa"],
                    "monthNames": ["janvier", "février", "mars", "avril", "mai", "juin", "juillet", "août", "septembre", "octobre", "novembre", "décembre", ""],
                    "monthNamesShort": ["janv.", "févr.", "mars", "avr.", "mai", "juin", "juil.", "août", "sept.", "oct.", "nov.", "déc.", ""],
                    "timePattern": "HH:mm",
                    "datePattern": "dd-MM-yy",
                    "dateTimePattern": "dd-MM-yy HH:mm",
                    "timeFormat": "Clock24Hours",
                    "weekStarts": 1
                })), DayPilot.Locale.register(new DayPilot.Locale("fr-ca", {
                    "dayNames": ["dimanche", "lundi", "mardi", "mercredi", "jeudi", "vendredi", "samedi"],
                    "dayNamesShort": ["di", "lu", "ma", "me", "je", "ve", "sa"],
                    "monthNames": ["janvier", "février", "mars", "avril", "mai", "juin", "juillet", "août", "septembre", "octobre", "novembre", "décembre", ""],
                    "monthNamesShort": ["janv.", "févr.", "mars", "avr.", "mai", "juin", "juil.", "août", "sept.", "oct.", "nov.", "déc.", ""],
                    "timePattern": "HH:mm",
                    "datePattern": "yyyy-MM-dd",
                    "dateTimePattern": "yyyy-MM-dd HH:mm",
                    "timeFormat": "Clock24Hours",
                    "weekStarts": 0
                })), DayPilot.Locale.register(new DayPilot.Locale("fr-ch", {
                    "dayNames": ["dimanche", "lundi", "mardi", "mercredi", "jeudi", "vendredi", "samedi"],
                    "dayNamesShort": ["di", "lu", "ma", "me", "je", "ve", "sa"],
                    "monthNames": ["janvier", "février", "mars", "avril", "mai", "juin", "juillet", "août", "septembre", "octobre", "novembre", "décembre", ""],
                    "monthNamesShort": ["janv.", "févr.", "mars", "avr.", "mai", "juin", "juil.", "août", "sept.", "oct.", "nov.", "déc.", ""],
                    "timePattern": "HH:mm",
                    "datePattern": "dd.MM.yyyy",
                    "dateTimePattern": "dd.MM.yyyy HH:mm",
                    "timeFormat": "Clock24Hours",
                    "weekStarts": 1
                })), DayPilot.Locale.register(new DayPilot.Locale("fr-fr", {
                    "dayNames": ["dimanche", "lundi", "mardi", "mercredi", "jeudi", "vendredi", "samedi"],
                    "dayNamesShort": ["di", "lu", "ma", "me", "je", "ve", "sa"],
                    "monthNames": ["janvier", "février", "mars", "avril", "mai", "juin", "juillet", "août", "septembre", "octobre", "novembre", "décembre", ""],
                    "monthNamesShort": ["janv.", "févr.", "mars", "avr.", "mai", "juin", "juil.", "août", "sept.", "oct.", "nov.", "déc.", ""],
                    "timePattern": "HH:mm",
                    "datePattern": "dd/MM/yyyy",
                    "dateTimePattern": "dd/MM/yyyy HH:mm",
                    "timeFormat": "Clock24Hours",
                    "weekStarts": 1
                })), DayPilot.Locale.register(new DayPilot.Locale("fr-lu", {
                    "dayNames": ["dimanche", "lundi", "mardi", "mercredi", "jeudi", "vendredi", "samedi"],
                    "dayNamesShort": ["di", "lu", "ma", "me", "je", "ve", "sa"],
                    "monthNames": ["janvier", "février", "mars", "avril", "mai", "juin", "juillet", "août", "septembre", "octobre", "novembre", "décembre", ""],
                    "monthNamesShort": ["janv.", "févr.", "mars", "avr.", "mai", "juin", "juil.", "août", "sept.", "oct.", "nov.", "déc.", ""],
                    "timePattern": "HH:mm",
                    "datePattern": "dd/MM/yyyy",
                    "dateTimePattern": "dd/MM/yyyy HH:mm",
                    "timeFormat": "Clock24Hours",
                    "weekStarts": 1
                })), DayPilot.Locale.register(new DayPilot.Locale("gl-es", {
                    "dayNames": ["domingo", "luns", "martes", "mércores", "xoves", "venres", "sábado"],
                    "dayNamesShort": ["do", "lu", "ma", "mé", "xo", "ve", "sá"],
                    "monthNames": ["xaneiro", "febreiro", "marzo", "abril", "maio", "xuño", "xullo", "agosto", "setembro", "outubro", "novembro", "decembro", ""],
                    "monthNamesShort": ["xan", "feb", "mar", "abr", "maio", "xuño", "xul", "ago", "set", "out", "nov", "dec", ""],
                    "timePattern": "H:mm",
                    "datePattern": "dd/MM/yyyy",
                    "dateTimePattern": "dd/MM/yyyy H:mm",
                    "timeFormat": "Clock24Hours",
                    "weekStarts": 1
                })), DayPilot.Locale.register(new DayPilot.Locale("it-it", {
                    "dayNames": ["domenica", "lunedì", "martedì", "mercoledì", "giovedì", "venerdì", "sabato"],
                    "dayNamesShort": ["do", "lu", "ma", "me", "gi", "ve", "sa"],
                    "monthNames": ["gennaio", "febbraio", "marzo", "aprile", "maggio", "giugno", "luglio", "agosto", "settembre", "ottobre", "novembre", "dicembre", ""],
                    "monthNamesShort": ["gen", "feb", "mar", "apr", "mag", "giu", "lug", "ago", "set", "ott", "nov", "dic", ""],
                    "timePattern": "HH:mm",
                    "datePattern": "dd/MM/yyyy",
                    "dateTimePattern": "dd/MM/yyyy HH:mm",
                    "timeFormat": "Clock24Hours",
                    "weekStarts": 1
                })), DayPilot.Locale.register(new DayPilot.Locale("it-ch", {
                    "dayNames": ["domenica", "lunedì", "martedì", "mercoledì", "giovedì", "venerdì", "sabato"],
                    "dayNamesShort": ["do", "lu", "ma", "me", "gi", "ve", "sa"],
                    "monthNames": ["gennaio", "febbraio", "marzo", "aprile", "maggio", "giugno", "luglio", "agosto", "settembre", "ottobre", "novembre", "dicembre", ""],
                    "monthNamesShort": ["gen", "feb", "mar", "apr", "mag", "giu", "lug", "ago", "set", "ott", "nov", "dic", ""],
                    "timePattern": "HH:mm",
                    "datePattern": "dd.MM.yyyy",
                    "dateTimePattern": "dd.MM.yyyy HH:mm",
                    "timeFormat": "Clock24Hours",
                    "weekStarts": 1
                })), DayPilot.Locale.register(new DayPilot.Locale("ja-jp", {
                    "dayNames": ["日曜日", "月曜日", "火曜日", "水曜日", "木曜日", "金曜日", "土曜日"],
                    "dayNamesShort": ["日", "月", "火", "水", "木", "金", "土"],
                    "monthNames": ["1月", "2月", "3月", "4月", "5月", "6月", "7月", "8月", "9月", "10月", "11月", "12月", ""],
                    "monthNamesShort": ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", ""],
                    "timePattern": "H:mm",
                    "datePattern": "yyyy/MM/dd",
                    "dateTimePattern": "yyyy/MM/dd H:mm",
                    "timeFormat": "Clock24Hours",
                    "weekStarts": 0
                })), DayPilot.Locale.register(new DayPilot.Locale("nb-no", {
                    "dayNames": ["søndag", "mandag", "tirsdag", "onsdag", "torsdag", "fredag", "lørdag"],
                    "dayNamesShort": ["sø", "ma", "ti", "on", "to", "fr", "lø"],
                    "monthNames": ["januar", "februar", "mars", "april", "mai", "juni", "juli", "august", "september", "oktober", "november", "desember", ""],
                    "monthNamesShort": ["jan", "feb", "mar", "apr", "mai", "jun", "jul", "aug", "sep", "okt", "nov", "des", ""],
                    "timePattern": "HH:mm",
                    "datePattern": "dd.MM.yyyy",
                    "dateTimePattern": "dd.MM.yyyy HH:mm",
                    "timeFormat": "Clock24Hours",
                    "weekStarts": 1
                })), DayPilot.Locale.register(new DayPilot.Locale("nl-nl", {
                    "dayNames": ["zondag", "maandag", "dinsdag", "woensdag", "donderdag", "vrijdag", "zaterdag"],
                    "dayNamesShort": ["zo", "ma", "di", "wo", "do", "vr", "za"],
                    "monthNames": ["januari", "februari", "maart", "april", "mei", "juni", "juli", "augustus", "september", "oktober", "november", "december", ""],
                    "monthNamesShort": ["jan", "feb", "mrt", "apr", "mei", "jun", "jul", "aug", "sep", "okt", "nov", "dec", ""],
                    "timePattern": "HH:mm",
                    "datePattern": "d-M-yyyy",
                    "dateTimePattern": "d-M-yyyy HH:mm",
                    "timeFormat": "Clock24Hours",
                    "weekStarts": 1
                })), DayPilot.Locale.register(new DayPilot.Locale("nl-be", {
                    "dayNames": ["zondag", "maandag", "dinsdag", "woensdag", "donderdag", "vrijdag", "zaterdag"],
                    "dayNamesShort": ["zo", "ma", "di", "wo", "do", "vr", "za"],
                    "monthNames": ["januari", "februari", "maart", "april", "mei", "juni", "juli", "augustus", "september", "oktober", "november", "december", ""],
                    "monthNamesShort": ["jan", "feb", "mrt", "apr", "mei", "jun", "jul", "aug", "sep", "okt", "nov", "dec", ""],
                    "timePattern": "H:mm",
                    "datePattern": "d/MM/yyyy",
                    "dateTimePattern": "d/MM/yyyy H:mm",
                    "timeFormat": "Clock24Hours",
                    "weekStarts": 1
                })), DayPilot.Locale.register(new DayPilot.Locale("nn-no", {
                    "dayNames": ["søndag", "måndag", "tysdag", "onsdag", "torsdag", "fredag", "laurdag"],
                    "dayNamesShort": ["sø", "må", "ty", "on", "to", "fr", "la"],
                    "monthNames": ["januar", "februar", "mars", "april", "mai", "juni", "juli", "august", "september", "oktober", "november", "desember", ""],
                    "monthNamesShort": ["jan", "feb", "mar", "apr", "mai", "jun", "jul", "aug", "sep", "okt", "nov", "des", ""],
                    "timePattern": "HH:mm",
                    "datePattern": "dd.MM.yyyy",
                    "dateTimePattern": "dd.MM.yyyy HH:mm",
                    "timeFormat": "Clock24Hours",
                    "weekStarts": 1
                })), DayPilot.Locale.register(new DayPilot.Locale("pt-br", {
                    "dayNames": ["domingo", "segunda-feira", "terça-feira", "quarta-feira", "quinta-feira", "sexta-feira", "sábado"],
                    "dayNamesShort": ["D", "S", "T", "Q", "Q", "S", "S"],
                    "monthNames": ["janeiro", "fevereiro", "março", "abril", "maio", "junho", "julho", "agosto", "setembro", "outubro", "novembro", "dezembro", ""],
                    "monthNamesShort": ["jan", "fev", "mar", "abr", "mai", "jun", "jul", "ago", "set", "out", "nov", "dez", ""],
                    "timePattern": "HH:mm",
                    "datePattern": "dd/MM/yyyy",
                    "dateTimePattern": "dd/MM/yyyy HH:mm",
                    "timeFormat": "Clock24Hours",
                    "weekStarts": 0
                })), DayPilot.Locale.register(new DayPilot.Locale("pl-pl", {
                    "dayNames": ["niedziela", "poniedziałek", "wtorek", "środa", "czwartek", "piątek", "sobota"],
                    "dayNamesShort": ["N", "Pn", "Wt", "Śr", "Cz", "Pt", "So"],
                    "monthNames": ["styczeń", "luty", "marzec", "kwiecień", "maj", "czerwiec", "lipiec", "sierpień", "wrzesień", "październik", "listopad", "grudzień", ""],
                    "monthNamesShort": ["sty", "lut", "mar", "kwi", "maj", "cze", "lip", "sie", "wrz", "paź", "lis", "gru", ""],
                    "timePattern": "HH:mm",
                    "datePattern": "yyyy-MM-dd",
                    "dateTimePattern": "yyyy-MM-dd HH:mm",
                    "timeFormat": "Clock24Hours",
                    "weekStarts": 1
                })), DayPilot.Locale.register(new DayPilot.Locale("pt-pt", {
                    "dayNames": ["domingo", "segunda-feira", "terça-feira", "quarta-feira", "quinta-feira", "sexta-feira", "sábado"],
                    "dayNamesShort": ["D", "S", "T", "Q", "Q", "S", "S"],
                    "monthNames": ["janeiro", "fevereiro", "março", "abril", "maio", "junho", "julho", "agosto", "setembro", "outubro", "novembro", "dezembro", ""],
                    "monthNamesShort": ["jan", "fev", "mar", "abr", "mai", "jun", "jul", "ago", "set", "out", "nov", "dez", ""],
                    "timePattern": "HH:mm",
                    "datePattern": "dd/MM/yyyy",
                    "dateTimePattern": "dd/MM/yyyy HH:mm",
                    "timeFormat": "Clock24Hours",
                    "weekStarts": 0
                })), DayPilot.Locale.register(new DayPilot.Locale("ro-ro", {
                    "dayNames": ["duminică", "luni", "marți", "miercuri", "joi", "vineri", "sâmbătă"],
                    "dayNamesShort": ["D", "L", "Ma", "Mi", "J", "V", "S"],
                    "monthNames": ["ianuarie", "februarie", "martie", "aprilie", "mai", "iunie", "iulie", "august", "septembrie", "octombrie", "noiembrie", "decembrie", ""],
                    "monthNamesShort": ["ian.", "feb.", "mar.", "apr.", "mai.", "iun.", "iul.", "aug.", "sep.", "oct.", "nov.", "dec.", ""],
                    "timePattern": "H:mm",
                    "datePattern": "dd.MM.yyyy",
                    "dateTimePattern": "dd.MM.yyyy H:mm",
                    "timeFormat": "Clock24Hours",
                    "weekStarts": 1
                })), DayPilot.Locale.register(new DayPilot.Locale("ru-ru", {
                    "dayNames": ["воскресенье", "понедельник", "вторник", "среда", "четверг", "пятница", "суббота"],
                    "dayNamesShort": ["Вс", "Пн", "Вт", "Ср", "Чт", "Пт", "Сб"],
                    "monthNames": ["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь", ""],
                    "monthNamesShort": ["янв", "фев", "мар", "апр", "май", "июн", "июл", "авг", "сен", "окт", "ноя", "дек", ""],
                    "timePattern": "H:mm",
                    "datePattern": "dd.MM.yyyy",
                    "dateTimePattern": "dd.MM.yyyy H:mm",
                    "timeFormat": "Clock24Hours",
                    "weekStarts": 1
                })), DayPilot.Locale.register(new DayPilot.Locale("sk-sk", {
                    "dayNames": ["nedeľa", "pondelok", "utorok", "streda", "štvrtok", "piatok", "sobota"],
                    "dayNamesShort": ["ne", "po", "ut", "st", "št", "pi", "so"],
                    "monthNames": ["január", "február", "marec", "apríl", "máj", "jún", "júl", "august", "september", "október", "november", "december", ""],
                    "monthNamesShort": ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", ""],
                    "timePattern": "H:mm",
                    "datePattern": "d.M.yyyy",
                    "dateTimePattern": "d.M.yyyy H:mm",
                    "timeFormat": "Clock24Hours",
                    "weekStarts": 1
                })), DayPilot.Locale.register(new DayPilot.Locale("sv-se", {
                    "dayNames": ["söndag", "måndag", "tisdag", "onsdag", "torsdag", "fredag", "lördag"],
                    "dayNamesShort": ["sö", "må", "ti", "on", "to", "fr", "lö"],
                    "monthNames": ["januari", "februari", "mars", "april", "maj", "juni", "juli", "augusti", "september", "oktober", "november", "december", ""],
                    "monthNamesShort": ["jan", "feb", "mar", "apr", "maj", "jun", "jul", "aug", "sep", "okt", "nov", "dec", ""],
                    "timePattern": "HH:mm",
                    "datePattern": "yyyy-MM-dd",
                    "dateTimePattern": "yyyy-MM-dd HH:mm",
                    "timeFormat": "Clock24Hours",
                    "weekStarts": 1
                })), DayPilot.Locale.register(new DayPilot.Locale("tr-tr", {
                    "dayNames": ["Pazar", "Pazartesi", "Salı", "Çarşamba", "Perşembe", "Cuma", "Cumartesi"],
                    "dayNamesShort": ["Pz", "Pt", "Sa", "Ça", "Pe", "Cu", "Ct"],
                    "monthNames": ["Ocak", "Şubat", "Mart", "Nisan", "Mayıs", "Haziran", "Temmuz", "Ağustos", "Eylül", "Ekim", "Kasım", "Aralık", ""],
                    "monthNamesShort": ["Oca", "Şub", "Mar", "Nis", "May", "Haz", "Tem", "Ağu", "Eyl", "Eki", "Kas", "Ara", ""],
                    "timePattern": "HH:mm",
                    "datePattern": "d.M.yyyy",
                    "dateTimePattern": "d.M.yyyy HH:mm",
                    "timeFormat": "Clock24Hours",
                    "weekStarts": 1
                })), DayPilot.Locale.register(new DayPilot.Locale("zh-cn", {
                    "dayNames": ["星期日", "星期一", "星期二", "星期三", "星期四", "星期五", "星期六"],
                    "dayNamesShort": ["日", "一", "二", "三", "四", "五", "六"],
                    "monthNames": ["一月", "二月", "三月", "四月", "五月", "六月", "七月", "八月", "九月", "十月", "十一月", "十二月", ""],
                    "monthNamesShort": ["1月", "2月", "3月", "4月", "5月", "6月", "7月", "8月", "9月", "10月", "11月", "12月", ""],
                    "timePattern": "H:mm",
                    "datePattern": "yyyy/M/d",
                    "dateTimePattern": "yyyy/M/d H:mm",
                    "timeFormat": "Clock24Hours",
                    "weekStarts": 1
                })), DayPilot.Locale.US = DayPilot.Locale.find("en-us"), DayPilot.Switcher = function(e) {
                    function t(e, t, n) {
                        var a = {};
                        a.start = e, a.end = t, a.day = n, a.target = i.active.control, a.preventDefault = function() {
                            this.preventDefault.value = !0
                        };
                        var o = i.k;
                        o && o.start === a.start && o.end === a.end && o.day === a.day && o.target === a.target || (i.k = a, "function" == typeof i.onChange && (i.onChange(a), a.preventDefault.value) || "function" == typeof i.onTimeRangeSelect && (i.onTimeRangeSelect(a), a.preventDefault.value) || (i.active.sendNavigate(i.day), "function" == typeof i.onChanged && i.onChanged(a), "function" == typeof i.onTimeRangeSelected && i.onTimeRangeSelected(a)))
                    }
                    var i = this;
                    this.views = [], this.triggers = [], this.navigator = {}, this.selectedClass = null, this.active = null, this.day = DayPilot.Date.today(), this.onChange = null, this.onChanged = null, this.navigator.updateMode = function(e) {
                        var t = i.navigator.control;
                        t && (t.selectMode = e, t.select(i.day))
                    }, this.addView = function(e, t) {
                        var n;
                        if ("string" == typeof e) {
                            if (n = document.getElementById(e), !n) throw "Element not found: " + e
                        } else n = e;
                        var a = n,
                            o = {};
                        return o.isView = !0, o.id = a.id, o.control = a, o.options = t || {}, o.hide = function() {
                            a.hide ? a.hide() : a.nav && a.nav.top ? a.nav.top.style.display = "none" : a.style.display = "none"
                        }, o.sendNavigate = function(e) {
                            (function() {
                                return !!a.backendUrl || !("function" != typeof WebForm_DoCallback || !a.uniqueID)
                            })() ? a.commandCallBack && a.commandCallBack("navigate", {
                                "day": e
                            }): (a.startDate = e, a.update())
                        }, o.show = function() {
                            i.c(), a.show ? a.show() : a.nav && a.nav.top ? a.nav.top.style.display = "" : a.style.display = ""
                        }, o.selectMode = function() {
                            if (o.options.navigatorSelectMode) return o.options.navigatorSelectMode;
                            if (a.isCalendar) switch (a.viewType) {
                                case "Day":
                                    return "day";
                                case "Week":
                                    return "week";
                                case "WorkWeek":
                                    return "week";
                                default:
                                    return "day"
                            } else if (a.isMonth) switch (a.viewType) {
                                case "Month":
                                    return "month";
                                case "Weeks":
                                    return "week";
                                default:
                                    return "day"
                            }
                            return "day"
                        }, this.views.push(o), o
                    }, this.addTrigger = function(e, t) {
                        var n;
                        if ("string" == typeof e) {
                            if (n = document.getElementById(e), !n) throw "Element not found: " + e
                        } else n = e;
                        var a = this.f(t);
                        a || (a = this.addView(t));
                        var o = {};
                        return o.isTrigger = !0, o.element = n, o.id = n.id, o.view = a, o.onClick = function(e) {
                            i.show(o), i.g(o), e = e || window.event, e && (e.preventDefault ? e.preventDefault() : e.returnValue = !1)
                        }, DayPilot.re(n, "click", o.onClick), this.triggers.push(o), o
                    }, this.addButton = this.addTrigger, this.select = function(e) {
                        var t = this.j(e);
                        t ? t.onClick() : this.triggers.length > 0 && this.triggers[0].onClick()
                    }, this.j = function(e) {
                        for (var t = 0; t < this.triggers.length; t++) {
                            var i = this.triggers[t];
                            if (i.id === e) return i
                        }
                        return null
                    }, this.g = function(e) {
                        if (this.selectedClass) {
                            for (var t = 0; t < this.triggers.length; t++) {
                                var i = this.triggers[t];
                                DayPilot.Util.removeClass(i.element, this.selectedClass)
                            }
                            DayPilot.Util.addClass(e.element, this.selectedClass)
                        }
                    }, this.addNavigator = function(e) {
                        i.navigator.control = e, e.timeRangeSelectedHandling = "JavaScript", e.onTimeRangeSelected = function() {
                            var n, a, o;
                            if (1 === e.api) n = arguments[0], a = arguments[1], o = arguments[2];
                            else {
                                var r = arguments[0];
                                n = r.start, a = r.end, o = r.day
                            }
                            i.day = o, t(n, a, o)
                        }
                    }, this.show = function(e) {
                        var t, n;
                        if (e.isTrigger) n = e, t = n.view;
                        else if (t = e.isView ? e : this.f(e), this.active === t) return;
                        if (i.onSelect) {
                            var a = {};
                            a.source = n ? n.element : null, a.target = t.control, i.onSelect(a)
                        }
                        this.active = t, t.show();
                        var o = t.selectMode();
                        i.navigator.updateMode(o)
                    }, this.f = function(e) {
                        for (var t = 0; t < this.views.length; t++)
                            if (this.views[t].control === e) return this.views[t];
                        return null
                    }, this.c = function() {
                        for (var e = 0; e < this.views.length; e++) this.views[e].hide()
                    }, this.events = {}, this.events.load = function(e, t, n) {
                        if (!i.active || !i.active.control) throw "DayPilot.Switcher.events.load(): Active view not found";
                        i.active.control.events.load(e, t, n)
                    }, this.k = null, this.l = function() {
                        if (e)
                            for (var t in e) "triggers" === t ? DayPilot.list(e.triggers).each(function(e) {
                                i.addTrigger(e.id, e.view)
                            }) : "navigator" === t ? i.addNavigator(e.navigator) : i[t] = e[t]
                    }, this.l()
                },
                function() {
                    if (!DayPilot.Global.defaultCss) {
                        var e = DayPilot.sheet();
                        e.add(".bubble_default_main", "cursor: default;"), e.add(".bubble_default_main_inner", 'border-radius: 5px;font-size: 12px;padding: 4px;color: #666;background: #eeeeee; background: -webkit-gradient(linear, left top, left bottom, from(#ffffff), to(#eeeeee));background: -webkit-linear-gradient(top, #ffffff 0%, #eeeeee);background: -moz-linear-gradient(top, #ffffff 0%, #eeeeee);background: -ms-linear-gradient(top, #ffffff 0%, #eeeeee);background: -o-linear-gradient(top, #ffffff 0%, #eeeeee);background: linear-gradient(top, #ffffff 0%, #eeeeee);filter: progid:DXImageTransform.Microsoft.Gradient(startColorStr="#ffffff", endColorStr="#eeeeee");border: 1px solid #ccc;-moz-border-radius: 5px;-webkit-border-radius: 5px;border-radius: 5px;-moz-box-shadow:0px 2px 3px rgba(000,000,000,0.3),inset 0px 0px 2px rgba(255,255,255,0.8);-webkit-box-shadow:0px 2px 3px rgba(000,000,000,0.3),inset 0px 0px 2px rgba(255,255,255,0.8);box-shadow:0px 2px 3px rgba(000,000,000,0.3),inset 0px 0px 2px rgba(255,255,255,0.8);'), e.add(".calendar_default_main", "border: 1px solid #c0c0c0; font-family: Tahoma, Arial, Helvetica, sans-serif; font-size: 12px;"), e.add(".calendar_default_main *, .calendar_default_main *:before, .calendar_default_main *:after", "box-sizing: content-box;"), e.add(".calendar_default_rowheader_inner,.calendar_default_cornerright_inner,.calendar_default_corner_inner,.calendar_default_colheader_inner,.calendar_default_alldayheader_inner", "color: #333;background: #f3f3f3;"), e.add(".calendar_default_cornerright_inner", "position: absolute;top: 0px;left: 0px;bottom: 0px;right: 0px;\tborder-bottom: 1px solid #c0c0c0;"), e.add(".calendar_default_rowheader_inner", "font-size: 16pt;text-align: right; position: absolute;top: 0px;left: 0px;bottom: 0px;right: 0px;border-right: 1px solid #c0c0c0;border-bottom: 1px solid #c0c0c0;"), e.add(".calendar_default_corner_inner", "position: absolute;top: 0px;left: 0px;bottom: 0px;right: 0px;border-right: 1px solid #c0c0c0;border-bottom: 1px solid #c0c0c0;"), e.add(".calendar_default_rowheader_minutes", "font-size:10px;vertical-align: super;padding-left: 2px;padding-right: 2px;"), e.add(".calendar_default_colheader_inner", "text-align: center; position: absolute;top: 0px;left: 0px;bottom: 0px;right: 0px;border-right: 1px solid #c0c0c0;border-bottom: 1px solid #c0c0c0;"), e.add(".calendar_default_cell_inner", "position: absolute;top: 0px;left: 0px;bottom: 0px;right: 0px;border-right: 1px solid #ddd;border-bottom: 1px solid #ddd; background: #f9f9f9;"), e.add(".calendar_default_cell_business .calendar_default_cell_inner", "background: #fff"), e.add(".calendar_default_alldayheader_inner", "text-align: center;position: absolute;top: 0px;left: 0px;bottom: 0px;right: 0px;border-right: 1px solid #c0c0c0;border-bottom: 1px solid #c0c0c0;"), e.add(".calendar_default_message", "opacity: 0.9;filter: alpha(opacity=90);\tpadding: 10px; color: #ffffff;background: #ffa216;"), e.add(".calendar_default_alldayevent_inner,.calendar_default_event_inner", "color: #666; border: 1px solid #999;"), e.add(".calendar_default_event_bar", "top: 0px;bottom: 0px;left: 0px;width: 4px;background-color: #9dc8e8;"), e.add(".calendar_default_event_bar_inner", "position: absolute;width: 4px;background-color: #1066a8;"), e.add(".calendar_default_alldayevent_inner,.calendar_default_event_inner", 'background: #fff;background: -webkit-gradient(linear, left top, left bottom, from(#ffffff), to(#eeeeee));background: -webkit-linear-gradient(top, #ffffff 0%, #eeeeee);background: -moz-linear-gradient(top, #ffffff 0%, #eeeeee);background: -ms-linear-gradient(top, #ffffff 0%, #eeeeee);background: -o-linear-gradient(top, #ffffff 0%, #eeeeee);background: linear-gradient(top, #ffffff 0%, #eeeeee);filter: progid:DXImageTransform.Microsoft.Gradient(startColorStr="#ffffff", endColorStr="#eeeeee");'), e.add(".calendar_default_selected .calendar_default_event_inner", "background: #ddd;"), e.add(".calendar_default_alldayevent_inner", "position: absolute;top: 2px;bottom: 2px;left: 2px;right: 2px;padding: 2px;margin-right: 1px;font-size: 12px;"), e.add(".calendar_default_event_withheader .calendar_default_event_inner", "padding-top: 15px;"), e.add(".calendar_default_event", "cursor: default;"), e.add(".calendar_default_event_inner", "position: absolute;overflow: hidden;top: 0px;bottom: 0px;left: 0px;right: 0px;padding: 2px 2px 2px 6px;font-size: 12px;"), e.add(".calendar_default_shadow_inner", "position:absolute;top:0px;left:0px;right:0px;bottom:0px;background-color: #666666; opacity: 0.5;filter: alpha(opacity=50);"), e.add(".calendar_default_event_delete", "background: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAsAAAALCAYAAACprHcmAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAadEVYdFNvZnR3YXJlAFBhaW50Lk5FVCB2My41LjExR/NCNwAAAI5JREFUKFNtkLERgCAMRbmzdK8s4gAUlhYOYEHJEJYOYOEwDmGBPxC4kOPfvePy84MGR0RJ2N1A8H3N6DATwSQ57m2ql8NBG+AEM7D+UW+wjdfUPgerYNgB5gOLRHqhcasg84C2QxPMtrUhSqQIhg7ypy9VM2EUZPI/4rQ7rGxqo9sadTegw+UdjeDLAKUfhbaQUVPIfJYAAAAASUVORK5CYII=) center center no-repeat; opacity: 0.6; -ms-filter:'progid:DXImageTransform.Microsoft.Alpha(Opacity=60)'; cursor: pointer;"), e.add(".calendar_default_event_delete:hover", "opacity: 1;-ms-filter: none;"), e.add(".calendar_default_scroll_up", "background: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAoAAAAKCAYAAACNMs+9AAAAB3RJTUUH2wESDiYcrhwCiQAAAAlwSFlzAAAuIwAALiMBeKU/dgAAAARnQU1BAACxjwv8YQUAAACcSURBVHjaY2AgF9wWsTW6yGMlhi7OhC7AyMDQzMnBXIpFHAFuCtuaMTP+P8nA8P/b1x//FfW/HHuF1UQmxv+NUP1c3OxMVVhNvCVi683E8H8LXOY/w9+fTH81tF8fv4NiIpBRj+YoZtZ/LDUoJmKYhsVUpv0MDiyMDP96sIYV0FS2/8z9ICaLlOhvS4b/jC//MzC8xBG0vJeF7GQBlK0xdiUzCtsAAAAASUVORK5CYII=);"), e.add(".calendar_default_scroll_down", "background: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAoAAAAKCAYAAACNMs+9AAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAALiMAAC4jAXilP3YAAACqSURBVChTY7wpam3L9J+xmQEP+PGPKZZxP4MDi4zI78uMDIwa2NT+Z2DYovrmiC+TI8OBP/8ZmEqwGvif4e8vxr+FIDkmEKH25vBWBgbG0+iK/zEwLtF+ffwOXCGI8Y+BoRFFIdC030x/WmBiYBNhpgLdswNJ8RSYaSgmgk39z1gPUfj/29ef/9rwhQTDHRHbrbdEbLvRFcGthkkAra/9/uMvhkK8piNLAgCRpTnNn4AEmAAAAABJRU5ErkJggg==);"), e.add(".calendar_default_now", "background-color: red;"), e.add(".calendar_default_now:before", "content: ''; top: -5px; border-width: 5px; border-color: transparent transparent transparent red; border-style: solid; width: 0px; height:0px; position: absolute; -moz-transform: scale(.9999);"), e.add(".calendar_default_shadow_forbidden .calendar_default_shadow_inner", "background-color: red;"), e.add(".calendar_default_shadow_top", 'box-sizing: border-box; padding:2px;border:1px solid #ccc;background:#fff;background: -webkit-gradient(linear, left top, left bottom, from(#ffffff), to(#eeeeee));background: -webkit-linear-gradient(top, #ffffff 0%, #eeeeee);background: -moz-linear-gradient(top, #ffffff 0%, #eeeeee);background: -ms-linear-gradient(top, #ffffff 0%, #eeeeee);background: -o-linear-gradient(top, #ffffff 0%, #eeeeee);background: linear-gradient(top, #ffffff 0%, #eeeeee);filter: progid:DXImageTransform.Microsoft.Gradient(startColorStr="#ffffff", endColorStr="#eeeeee");'), e.add(".calendar_default_shadow_bottom", 'box-sizing: border-box; padding:2px;border:1px solid #ccc;background:#fff;background: -webkit-gradient(linear, left top, left bottom, from(#ffffff), to(#eeeeee));background: -webkit-linear-gradient(top, #ffffff 0%, #eeeeee);background: -moz-linear-gradient(top, #ffffff 0%, #eeeeee);background: -ms-linear-gradient(top, #ffffff 0%, #eeeeee);background: -o-linear-gradient(top, #ffffff 0%, #eeeeee);background: linear-gradient(top, #ffffff 0%, #eeeeee);filter: progid:DXImageTransform.Microsoft.Gradient(startColorStr="#ffffff", endColorStr="#eeeeee");'), e.add(".menu_default_main", "font-family: Tahoma, Arial, Helvetica, Sans-Serif;font-size: 12px;border: 1px solid #dddddd;background-color: white;padding: 0px;cursor: default;background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAABCAIAAABG0om7AAAAKXRFWHRDcmVhdGlvbiBUaW1lAHBvIDEwIDUgMjAxMCAyMjozMzo1OSArMDEwMGzy7+IAAAAHdElNRQfaBQoUJAesj4VUAAAACXBIWXMAAA7DAAAOwwHHb6hkAAAABGdBTUEAALGPC/xhBQAAABVJREFUeNpj/P//PwO1weMnT2RlZAAYuwX/4oA3BgAAAABJRU5ErkJggg==);background-repeat: repeat-y;xborder-radius: 5px;-moz-box-shadow:0px 2px 3px rgba(000,000,000,0.3),inset 0px 0px 2px rgba(255,255,255,0.8);-webkit-box-shadow:0px 2px 3px rgba(000,000,000,0.3),inset 0px 0px 2px rgba(255,255,255,0.8);box-shadow:0px 2px 3px rgba(000,000,000,0.3),inset 0px 0px 2px rgba(255,255,255,0.8);"), e.add(".menu_default_main, .menu_default_main *, .menu_default_main *:before, .menu_default_main *:after", "box-sizing: content-box;"), e.add(".menu_default_title", "background-color: #f2f2f2;border-bottom: 1px solid gray;padding: 4px 4px 4px 37px;"), e.add(".menu_default_main a", "padding: 2px 2px 2px 35px;color: black;text-decoration: none;cursor: default;"), e.add(".menu_default_main a img", "margin-left: 6px;margin-top: 2px;"), e.add(".menu_default_item_text", "display: block;height: 20px;line-height: 20px; overflow:hidden;padding-left: 2px;padding-right: 20px;"), e.add(".menu_default_main a:hover", "background-color: #f3f3f3;"), e.add(".menu_default_main div div", "border-top: 1px solid #dddddd;margin-top: 2px;margin-bottom: 2px;margin-left: 28px;"), e.add(".menu_default_main a.menu_default_item_disabled", "color: #ccc"), e.add(".menu_default_item_haschildren.menu_default_item_haschildren_active", 'background: #eeeeee;background: -webkit-gradient(linear, left top, left bottom, from(#efefef), to(#e6e6e6));background: -webkit-linear-gradient(top, #efefef 0%, #e6e6e6);background: -moz-linear-gradient(top, #efefef 0%, #e6e6e6);background: -ms-linear-gradient(top, #efefef 0%, #e6e6e6);background: -o-linear-gradient(top, #efefef 0%, #e6e6e6);background: linear-gradient(top, #efefef 0%, #e6e6e6);filter: progid:DXImageTransform.Microsoft.Gradient(startColorStr="#efefef", endColorStr="#e6e6e6");'), e.add(".menu_default_item_haschildren a:before", "content: ''; border-width: 6px; border-color: transparent transparent transparent black; border-style: solid; width: 0px; height:0px; position: absolute; right: 5px; margin-top: 4px;"), e.add(".menu_default_item_icon", "position: absolute; top:0px; left: 0px; padding: 2px 2px 2px 8px;"), e.add(".menu_default_item a i", "height: 20px;line-height: 20px;"), e.add(".menubar_default_main", "border-bottom: 1px solid #ccc; font-family: Tahoma, Arial, Helvetica, sans-serif; font-size: 12px;"), e.add(".menubar_default_item", "display: inline-block;  padding: 6px 10px; cursor: default;"), e.add(".menubar_default_item:hover", "background-color: #f2f2f2;"), e.add(".menubar_default_item_active", "background-color: #f2f2f2;"), e.add(".month_default_main", "border: 1px solid #c0c0c0;font-family: Tahoma, Arial, Helvetica, sans-serif; font-size: 12px;color: #333;"), e.add(".month_default_main *, .month_default_main *:before, .month_default_main *:after", "box-sizing: content-box;"), e.add(".month_default_cell_inner", "border-right: 1px solid #ddd;border-bottom: 1px solid #ddd;position: absolute;top: 0px;left: 0px;bottom: 0px;right: 0px;background-color: #f9f9f9;"), e.add(".month_default_cell_business .month_default_cell_inner", "background-color: #fff;"), e.add(".month_default_cell_header", "text-align: right;padding-right: 2px;"), e.add(".month_default_header_inner", "text-align: center; vertical-align: middle;position: absolute;top: 0px;left: 0px;bottom: 0px;right: 0px;border-right: 1px solid #c0c0c0;border-bottom: 1px solid #c0c0c0;cursor: default;color: #333;background: #f3f3f3;"), e.add(".month_default_message", 'padding: 10px;opacity: 0.9;filter: alpha(opacity=90);color: #ffffff;background: #ffa216;background: -webkit-gradient(linear, left top, left bottom, from(#ffa216), to(#ff8400));background: -webkit-linear-gradient(top, #ffa216 0%, #ff8400);background: -moz-linear-gradient(top, #ffa216 0%, #ff8400);background: -ms-linear-gradient(top, #ffa216 0%, #ff8400);background: -o-linear-gradient(top, #ffa216 0%, #ff8400);background: linear-gradient(top, #ffa216 0%, #ff8400);filter: progid:DXImageTransform.Microsoft.Gradient(startColorStr="#ffa216", endColorStr="#ff8400");'), e.add(".month_default_event_inner", 'position: absolute;top: 0px;bottom: 0px;left: 1px;right: 1px;overflow:hidden;padding: 2px;padding-left: 5px;font-size: 12px;color: #333;background: #fff;background: -webkit-gradient(linear, left top, left bottom, from(#ffffff), to(#eeeeee));background: -webkit-linear-gradient(top, #ffffff 0%, #eeeeee);background: -moz-linear-gradient(top, #ffffff 0%, #eeeeee);background: -ms-linear-gradient(top, #ffffff 0%, #eeeeee);background: -o-linear-gradient(top, #ffffff 0%, #eeeeee);background: linear-gradient(top, #ffffff 0%, #eeeeee);filter: progid:DXImageTransform.Microsoft.Gradient(startColorStr="#ffffff", endColorStr="#eeeeee");border: 1px solid #999;border-radius: 0px;'), e.add(".month_default_event_continueright .month_default_event_inner", "border-top-right-radius: 0px;border-bottom-right-radius: 0px;border-right-style: dotted;"), e.add(".month_default_event_continueleft .month_default_event_inner", "border-top-left-radius: 0px;border-bottom-left-radius: 0px;border-left-style: dotted;"), e.add(".month_default_event_hover .month_default_event_inner", 'background: #fff;background: -webkit-gradient(linear, left top, left bottom, from(#ffffff), to(#e8e8e8));background: -webkit-linear-gradient(top, #ffffff 0%, #e8e8e8);background: -moz-linear-gradient(top, #ffffff 0%, #e8e8e8);background: -ms-linear-gradient(top, #ffffff 0%, #e8e8e8);background: -o-linear-gradient(top, #ffffff 0%, #e8e8e8);background: linear-gradient(top, #ffffff 0%, #e8e8e8);filter: progid:DXImageTransform.Microsoft.Gradient(startColorStr="#ffffff", endColorStr="#e8e8e8");'), e.add(".month_default_selected .month_default_event_inner, .month_default_event_hover.month_default_selected .month_default_event_inner", "background: #ddd;"), e.add(".month_default_shadow_inner", "background-color: #666666;opacity: 0.5;filter: alpha(opacity=50);height: 100%;"), e.add(".month_default_event_delete", "background: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAsAAAALCAYAAACprHcmAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAadEVYdFNvZnR3YXJlAFBhaW50Lk5FVCB2My41LjExR/NCNwAAAI5JREFUKFNtkLERgCAMRbmzdK8s4gAUlhYOYEHJEJYOYOEwDmGBPxC4kOPfvePy84MGR0RJ2N1A8H3N6DATwSQ57m2ql8NBG+AEM7D+UW+wjdfUPgerYNgB5gOLRHqhcasg84C2QxPMtrUhSqQIhg7ypy9VM2EUZPI/4rQ7rGxqo9sadTegw+UdjeDLAKUfhbaQUVPIfJYAAAAASUVORK5CYII=) center center no-repeat; opacity: 0.6; -ms-filter:'progid:DXImageTransform.Microsoft.Alpha(Opacity=60)';cursor: pointer;"), e.add(".month_default_event_delete:hover", "opacity: 1;-ms-filter: none;"), e.add(".month_default_event_timeleft", "color: #ccc; font-size: 8pt"), e.add(".month_default_event_timeright", "color: #ccc; font-size: 8pt; text-align: right;"), e.add(".navigator_default_main", "border-left: 1px solid #c0c0c0;border-right: 1px solid #c0c0c0;border-bottom: 1px solid #c0c0c0;background-color: white;color: #000000; box-sizing: content-box;"), e.add(".navigator_default_main *, .navigator_default_main *:before, .navigator_default_main *:after", "box-sizing: content-box;"), e.add(".navigator_default_month", "font-family: Tahoma, Arial, Helvetica, sans-serif; font-size: 11px;"), e.add(".navigator_default_day", "color: black;"), e.add(".navigator_default_weekend", "background-color: #f0f0f0;"), e.add(".navigator_default_dayheader", "color: black;"), e.add(".navigator_default_line", "border-bottom: 1px solid #c0c0c0;"), e.add(".navigator_default_dayother", "color: gray;"), e.add(".navigator_default_todaybox", "border: 1px solid red;"), e.add(".navigator_default_title, .navigator_default_titleleft, .navigator_default_titleright", "border-top: 1px solid #c0c0c0;border-bottom: 1px solid #c0c0c0;color: #333;background: #f3f3f3;"), e.add(".navigator_default_busy", "font-weight: bold;"), e.add(".navigator_default_cell", "text-align: center;"), e.add(".navigator_default_select .navigator_default_cell_box", "background-color: #FFE794; opacity: 0.5;"), e.add(".navigator_default_title", "text-align: center;"), e.add(".navigator_default_titleleft, .navigator_default_titleright", "text-align: center;"), e.add(".navigator_default_dayheader", "text-align: center;"), e.add(".navigator_default_weeknumber", "text-align: center;"), e.add(".scheduler_default_main *, .scheduler_default_main *:before, .scheduler_default_main *:after", "box-sizing: content-box;"), e.add(".scheduler_default_main", "box-sizing: content-box; border: 1px solid #c0c0c0;font-family: Tahoma, Arial, Helvetica, sans-serif; font-size: 12px;"), e.add(".scheduler_default_selected .scheduler_default_event_inner", "background: #ddd;"), e.add(".scheduler_default_timeheader", "cursor: default;color: #333;"), e.add(".scheduler_default_message", "opacity: 0.9;filter: alpha(opacity=90);padding: 10px; color: #ffffff;background: #ffa216;"), e.add(".scheduler_default_timeheadergroup,.scheduler_default_timeheadercol", "color: #333;background: #f3f3f3;"), e.add(".scheduler_default_rowheader,.scheduler_default_corner", "color: #333;background: #f3f3f3;"), e.add(".scheduler_default_rowheader.scheduler_default_rowheader_selected", "background-color: #aaa;background-image: -webkit-gradient(linear, 0 100%, 100% 0,color-stop(.25, rgba(255, 255, 255, .2)), color-stop(.25, transparent),\tcolor-stop(.5, transparent), color-stop(.5, rgba(255, 255, 255, .2)), color-stop(.75, rgba(255, 255, 255, .2)), color-stop(.75, transparent), to(transparent));background-image: -webkit-linear-gradient(45deg, rgba(255, 255, 255, .2) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, .2) 50%, rgba(255, 255, 255, .2) 75%, transparent 75%, transparent);background-image: -moz-linear-gradient(45deg, rgba(255, 255, 255, .2) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, .2) 50%, rgba(255, 255, 255, .2) 75%, transparent 75%, transparent);background-image: -ms-linear-gradient(45deg, rgba(255, 255, 255, .2) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, .2) 50%, rgba(255, 255, 255, .2) 75%, transparent 75%, transparent);background-image: -o-linear-gradient(45deg, rgba(255, 255, 255, .2) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, .2) 50%, rgba(255, 255, 255, .2) 75%, transparent 75%, transparent);background-image: linear-gradient(45deg, rgba(255, 255, 255, .2) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, .2) 50%, rgba(255, 255, 255, .2) 75%, transparent 75%, transparent);-webkit-background-size: 20px 20px;-moz-background-size: 20px 20px;background-size: 20px 20px;"), e.add(".scheduler_default_rowheader_inner", "position: absolute;left: 0px;right: 0px;top: 0px;bottom: 0px;border-right: 1px solid #e0e0e0;padding: 2px;"), e.add(".scheduler_default_timeheadergroup, .scheduler_default_timeheadercol", "text-align: center;"), e.add(".scheduler_default_timeheadergroup_inner", "position: absolute;left: 0px;right: 0px;top: 0px;bottom: 0px;border-right: 1px solid #c0c0c0;border-bottom: 1px solid #c0c0c0;"), e.add(".scheduler_default_timeheadercol_inner", "position: absolute;left: 0px;right: 0px;top: 0px;bottom: 0px;border-right: 1px solid #c0c0c0;"), e.add(".scheduler_default_divider, .scheduler_default_splitter", "background-color: #c0c0c0;"), e.add(".scheduler_default_divider_horizontal", "background-color: #c0c0c0;"), e.add(".scheduler_default_matrix_vertical_line", "background-color: #eee;"), e.add(".scheduler_default_matrix_vertical_break", "background-color: #999;"), e.add(".scheduler_default_matrix_horizontal_line", "background-color: #eee;"), e.add(".scheduler_default_resourcedivider", "background-color: #c0c0c0;"), e.add(".scheduler_default_shadow_inner", "background-color: #666666;opacity: 0.5;filter: alpha(opacity=50);height: 100%;"), e.add(".scheduler_default_event", "font-size:12px;color:#333;"), e.add(".scheduler_default_event_inner", "position:absolute;top:0px;left:0px;right:0px;bottom:0px;padding:5px 2px 2px 2px;overflow:hidden;border:1px solid #ccc;"), e.add(".scheduler_default_event_bar", "top:0px;left:0px;right:0px;height:4px;background-color:#9dc8e8;"), e.add(".scheduler_default_event_bar_inner", "position:absolute;height:4px;background-color:#1066a8;"), e.add(".scheduler_default_event_inner", 'background:#fff;background: -webkit-gradient(linear, left top, left bottom, from(#ffffff), to(#eeeeee));background: -webkit-linear-gradient(top, #ffffff 0%, #eeeeee);background: -moz-linear-gradient(top, #ffffff 0%, #eeeeee);background: -ms-linear-gradient(top, #ffffff 0%, #eeeeee);background: -o-linear-gradient(top, #ffffff 0%, #eeeeee);background: linear-gradient(top, #ffffff 0%, #eeeeee);filter: progid:DXImageTransform.Microsoft.Gradient(startColorStr="#ffffff", endColorStr="#eeeeee");'), e.add(".scheduler_default_event_float_inner", "padding:6px 2px 2px 8px;"), e.add(".scheduler_default_event_float_inner:after", 'content:"";border-color: transparent #666 transparent transparent;border-style:solid;border-width:5px;width:0;height:0;position:absolute;top:8px;left:-4px;'), e.add(".scheduler_default_columnheader_inner", "font-weight: bold;"), e.add(".scheduler_default_columnheader_splitter", "box-sizing: border-box; border-right: 1px solid #c0c0c0;"), e.add(".scheduler_default_columnheader_splitter:hover", "background-color: #c0c0c0;"), e.add(".scheduler_default_columnheader_cell_inner", "padding: 2px;"), e.add(".scheduler_default_cell", "background-color: #f9f9f9;"), e.add(".scheduler_default_cell.scheduler_default_cell_business", "background-color: #fff;"), e.add(".scheduler_default_cell.scheduler_default_cell_business.scheduler_default_cell_selected,.scheduler_default_cell.scheduler_default_cell_selected", "background-color: #ccc;background-image: -webkit-gradient(linear, 0 100%, 100% 0,\tcolor-stop(.25, rgba(255, 255, 255, .2)), color-stop(.25, transparent),\tcolor-stop(.5, transparent), color-stop(.5, rgba(255, 255, 255, .2)), color-stop(.75, rgba(255, 255, 255, .2)), color-stop(.75, transparent), to(transparent));background-image: -webkit-linear-gradient(45deg, rgba(255, 255, 255, .2) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, .2) 50%, rgba(255, 255, 255, .2) 75%, transparent 75%, transparent);background-image: -moz-linear-gradient(45deg, rgba(255, 255, 255, .2) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, .2) 50%, rgba(255, 255, 255, .2) 75%, transparent 75%, transparent);background-image: -ms-linear-gradient(45deg, rgba(255, 255, 255, .2) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, .2) 50%, rgba(255, 255, 255, .2) 75%, transparent 75%, transparent);background-image: -o-linear-gradient(45deg, rgba(255, 255, 255, .2) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, .2) 50%, rgba(255, 255, 255, .2) 75%, transparent 75%, transparent);background-image: linear-gradient(45deg, rgba(255, 255, 255, .2) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, .2) 50%, rgba(255, 255, 255, .2) 75%, transparent 75%, transparent);-webkit-background-size: 20px 20px;-moz-background-size: 20px 20px;background-size: 20px 20px;"), e.add(".scheduler_default_tree_image_no_children", "background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAkAAAAJCAIAAABv85FHAAAAKXRFWHRDcmVhdGlvbiBUaW1lAHDhIDMwIEkgMjAwOSAwODo0NjozMSArMDEwMClDkt4AAAAHdElNRQfZAR4HLzEyzsCJAAAACXBIWXMAAA7CAAAOwgEVKEqAAAAABGdBTUEAALGPC/xhBQAAADBJREFUeNpjrK6s5uTl/P75OybJ0NLW8h8bAIozgeSxAaA4E1A7VjmgOL31MeLxHwCeXUT0WkFMKAAAAABJRU5ErkJggg==);"), e.add(".scheduler_default_tree_image_expand", "background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAkAAAAJCAIAAABv85FHAAAAKXRFWHRDcmVhdGlvbiBUaW1lAHDhIDMwIEkgMjAwOSAwODo0NjozMSArMDEwMClDkt4AAAAHdElNRQfZAR4HLyUoFBT0AAAACXBIWXMAAA7CAAAOwgEVKEqAAAAABGdBTUEAALGPC/xhBQAAAFJJREFUeNpjrK6s5uTl/P75OybJ0NLW8h8bAIozgeRhgJGREc4GijMBtTNgA0BxFog+uA4IA2gmUJwFog/IgUhAGBB9KPYhA3T74Jog+hjx+A8A1KRQ+AN5vcwAAAAASUVORK5CYII=);"), e.add(".scheduler_default_tree_image_collapse", "background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAkAAAAJCAIAAABv85FHAAAAKXRFWHRDcmVhdGlvbiBUaW1lAHDhIDMwIEkgMjAwOSAwODo0NjozMSArMDEwMClDkt4AAAAHdElNRQfZAR4HLxB+p9DXAAAACXBIWXMAAA7CAAAOwgEVKEqAAAAABGdBTUEAALGPC/xhBQAAAENJREFUeNpjrK6s5uTl/P75OybJ0NLW8h8bAIozgeSxAaA4E1A7VjmgOAtEHyMjI7IE0EygOAtEH5CDqY9c+xjx+A8ANndK9WaZlP4AAAAASUVORK5CYII=);"), e.add(".scheduler_default_event_move_left", 'box-sizing: border-box; padding:2px;border:1px solid #ccc;background:#fff;background: -webkit-gradient(linear, left top, left bottom, from(#ffffff), to(#eeeeee));background: -webkit-linear-gradient(top, #ffffff 0%, #eeeeee);background: -moz-linear-gradient(top, #ffffff 0%, #eeeeee);background: -ms-linear-gradient(top, #ffffff 0%, #eeeeee);background: -o-linear-gradient(top, #ffffff 0%, #eeeeee);background: linear-gradient(top, #ffffff 0%, #eeeeee);filter: progid:DXImageTransform.Microsoft.Gradient(startColorStr="#ffffff", endColorStr="#eeeeee");'), e.add(".scheduler_default_event_move_right", 'box-sizing: border-box; padding:2px;border:1px solid #ccc;background:#fff;background: -webkit-gradient(linear, left top, left bottom, from(#ffffff), to(#eeeeee));background: -webkit-linear-gradient(top, #ffffff 0%, #eeeeee);background: -moz-linear-gradient(top, #ffffff 0%, #eeeeee);background: -ms-linear-gradient(top, #ffffff 0%, #eeeeee);background: -o-linear-gradient(top, #ffffff 0%, #eeeeee);background: linear-gradient(top, #ffffff 0%, #eeeeee);filter: progid:DXImageTransform.Microsoft.Gradient(startColorStr="#ffffff", endColorStr="#eeeeee");'), e.add(".scheduler_default_event_delete", "background: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAsAAAALCAYAAACprHcmAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAadEVYdFNvZnR3YXJlAFBhaW50Lk5FVCB2My41LjExR/NCNwAAAI5JREFUKFNtkLERgCAMRbmzdK8s4gAUlhYOYEHJEJYOYOEwDmGBPxC4kOPfvePy84MGR0RJ2N1A8H3N6DATwSQ57m2ql8NBG+AEM7D+UW+wjdfUPgerYNgB5gOLRHqhcasg84C2QxPMtrUhSqQIhg7ypy9VM2EUZPI/4rQ7rGxqo9sadTegw+UdjeDLAKUfhbaQUVPIfJYAAAAASUVORK5CYII=) center center no-repeat; opacity: 0.6; -ms-filter:'progid:DXImageTransform.Microsoft.Alpha(Opacity=60)';cursor: pointer;"), e.add(".scheduler_default_event_delete:hover", "opacity: 1;-ms-filter: none;"), e.add(".scheduler_default_rowmove_handle", "background-repeat: no-repeat; background-position: center center; background-color: #ccc; background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAQAAAAKCAYAAACT+/8OAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAadEVYdFNvZnR3YXJlAFBhaW50Lk5FVCB2My41LjExR/NCNwAAAClJREFUGFdj+P//P4O9vX2Bg4NDP4gNFgBytgPxebgAMsYuQGMz/jMAAFsTZDPYJlDHAAAAAElFTkSuQmCC); cursor: move;"), e.add(".scheduler_default_rowmove_source", "background-color: black; opacity: 0.2;"), e.add(".scheduler_default_rowmove_position_before, .scheduler_default_rowmove_position_after", "background-color: #999; height: 2px;"), e.add(".scheduler_default_rowmove_position_child", "margin-left: 10px; background-color: #999; height: 2px;"), e.add(".scheduler_default_rowmove_position_child:before", "content: '+'; color: #999; position: absolute; top: -8px; left: -10px;"), e.add(".scheduler_default_rowmove_position_forbidden", "background-color: red; height: 2px; margin-left: 10px;"), e.add(".scheduler_default_rowmove_position_forbidden:before", "content: 'x'; color: red; position: absolute; top: -8px; left: -10px;"), e.add(".scheduler_default_link_horizontal", "border-bottom-style: solid; border-bottom-color: red"), e.add(".scheduler_default_link_vertical", "border-right-style: solid; border-right-color: red"), e.add(".scheduler_default_link_arrow_right:before", "content: ''; border-width: 6px; border-color: transparent transparent transparent red; border-style: solid; width: 0px; height:0px; position: absolute;"), e.add(".scheduler_default_link_arrow_left:before", "content: ''; border-width: 6px; border-color: transparent red transparent transparent; border-style: solid; width: 0px; height:0px; position: absolute;"), e.add(".scheduler_default_link_arrow_down:before", "content: ''; border-width: 6px; border-color: red transparent transparent transparent; border-style: solid; width: 0px; height:0px; position: absolute;"), e.add(".scheduler_default_link_arrow_up:before", "content: ''; border-width: 6px; border-color: transparent transparent red transparent; border-style: solid; width: 0px; height:0px; position: absolute;"), e.add(".scheduler_default_shadow_overlap .scheduler_default_shadow_inner", "background-color: red;"), e.add(".scheduler_default_block", "background-color: gray; opacity: 0.5; filter: alpha(opacity=50);"), e.add(".scheduler_default_main .scheduler_default_event_group", "box-sizing: border-box; font-size:12px; color:#666; padding:4px 2px 2px 2px; overflow:hidden; border:1px solid #ccc; background-color: #fff;"), e.add(".scheduler_default_main .scheduler_default_header_icon", "box-sizing: border-box; border: 1px solid #c0c0c0; background-color: #f5f5f5; color: #000;"), e.add(".scheduler_default_header_icon:hover", "background-color: #ccc;"), e.add(".scheduler_default_header_icon_hide:before", "content: '\\00AB';"), e.add(".scheduler_default_header_icon_show:before", "content: '\\00BB';"), e.add(".scheduler_default_row_new .scheduler_default_rowheader_inner", "padding-left: 10px; color: #666; cursor: text; background-position: 0px 5px; background-repeat: no-repeat; background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAoAAAAKCAYAAACNMs+9AAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAABUSURBVChTY0ACslAaK2CC0iCQDMSlECYmQFYIAl1AjFUxukIQwKoYm0IQwFCMSyEIaEJpMMClcD4Qp0CYEIBNIUzRPzAPCtAVYlWEDgyAGIdTGBgAbqEJYyjqa3oAAAAASUVORK5CYII=);"), e.add(".scheduler_default_row_new .scheduler_default_rowheader_inner:hover", "background: white; color: white;"), e.add(".scheduler_default_rowheader textarea", "padding: 3px;"), e.add(".scheduler_default_rowheader_scroll", "cursor: default;"), e.add(".scheduler_default_shadow_forbidden .scheduler_default_shadow_inner", "background-color: red;"), e.add(".scheduler_default_event_moving_source", "opacity: 0.5; filter: alpha(opacity=50);"), e.add(".scheduler_default_linkpoint", "background-color: white; border: 1px solid gray; border-radius: 5px;"),
                            e.add(".scheduler_default_linkpoint.scheduler_default_linkpoint_hover", "background-color: black;"), e.add(".scheduler_default_event.scheduler_default_event_version .scheduler_default_event_inner", "background-color: #cfdde8;background-image: -webkit-gradient(linear, 0 100%, 100% 0,\tcolor-stop(.25, rgba(255, 255, 255, .2)), color-stop(.25, transparent),\tcolor-stop(.5, transparent), color-stop(.5, rgba(255, 255, 255, .2)), color-stop(.75, rgba(255, 255, 255, .2)), color-stop(.75, transparent), to(transparent));background-image: -webkit-linear-gradient(45deg, rgba(255, 255, 255, .2) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, .2) 50%, rgba(255, 255, 255, .2) 75%, transparent 75%, transparent);background-image: -moz-linear-gradient(45deg, rgba(255, 255, 255, .2) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, .2) 50%, rgba(255, 255, 255, .2) 75%, transparent 75%, transparent);background-image: -ms-linear-gradient(45deg, rgba(255, 255, 255, .2) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, .2) 50%, rgba(255, 255, 255, .2) 75%, transparent 75%, transparent);background-image: -o-linear-gradient(45deg, rgba(255, 255, 255, .2) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, .2) 50%, rgba(255, 255, 255, .2) 75%, transparent 75%, transparent);background-image: linear-gradient(45deg, rgba(255, 255, 255, .2) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, .2) 50%, rgba(255, 255, 255, .2) 75%, transparent 75%, transparent);-webkit-background-size: 20px 20px;-moz-background-size: 20px 20px;background-size: 20px 20px;"), e.add(".scheduler_default_crosshair_vertical, .scheduler_default_crosshair_horizontal, .scheduler_default_crosshair_left, .scheduler_default_crosshair_top", "background-color: gray; opacity: 0.2; filter: alpha(opacity=20)"), e.add(".scheduler_default_link_dot", "border-radius: 10px; background-color: red"), e.add(".gantt_default_selected .gantt_default_event_inner", "background: #ddd;"), e.add(".gantt_default_main", "border: 1px solid #c0c0c0;font-family: Tahoma, Arial, Helvetica, sans-serif; font-size: 12px;"), e.add(".gantt_default_main *, .gantt_default_main *:before, .gantt_default_main *:after", "box-sizing: content-box;"), e.add(".gantt_default_timeheader", "cursor: default;color: #666;"), e.add(".gantt_default_message", "opacity: 0.9;filter: alpha(opacity=90);padding: 10px; color: #ffffff;background: #ffa216;"), e.add(".gantt_default_timeheadergroup,.gantt_default_timeheadercol", "color: #333;background: #f3f3f3;"), e.add(".gantt_default_rowheader,.gantt_default_corner", "color: #333;background: #f3f3f3;"), e.add(".gantt_default_rowheader.gantt_default_rowheader_selected", "background-color: #aaa;background-image: -webkit-gradient(linear, 0 100%, 100% 0,color-stop(.25, rgba(255, 255, 255, .2)), color-stop(.25, transparent),\tcolor-stop(.5, transparent), color-stop(.5, rgba(255, 255, 255, .2)), color-stop(.75, rgba(255, 255, 255, .2)), color-stop(.75, transparent), to(transparent));background-image: -webkit-linear-gradient(45deg, rgba(255, 255, 255, .2) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, .2) 50%, rgba(255, 255, 255, .2) 75%, transparent 75%, transparent);background-image: -moz-linear-gradient(45deg, rgba(255, 255, 255, .2) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, .2) 50%, rgba(255, 255, 255, .2) 75%, transparent 75%, transparent);background-image: -ms-linear-gradient(45deg, rgba(255, 255, 255, .2) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, .2) 50%, rgba(255, 255, 255, .2) 75%, transparent 75%, transparent);background-image: -o-linear-gradient(45deg, rgba(255, 255, 255, .2) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, .2) 50%, rgba(255, 255, 255, .2) 75%, transparent 75%, transparent);background-image: linear-gradient(45deg, rgba(255, 255, 255, .2) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, .2) 50%, rgba(255, 255, 255, .2) 75%, transparent 75%, transparent);-webkit-background-size: 20px 20px;-moz-background-size: 20px 20px;background-size: 20px 20px;"), e.add(".gantt_default_rowheader_inner", "position: absolute;left: 0px;right: 0px;top: 0px;bottom: 0px;border-right: 1px solid #c0c0c0;padding: 2px;"), e.add(".gantt_default_timeheadergroup, .gantt_default_timeheadercol", "text-align: center;"), e.add(".gantt_default_timeheadergroup_inner", "position: absolute;left: 0px;right: 0px;top: 0px;bottom: 0px;border-right: 1px solid #aaa;border-bottom: 1px solid #aaa;"), e.add(".gantt_default_timeheadercol_inner", "position: absolute;left: 0px;right: 0px;top: 0px;bottom: 0px;border-right: 1px solid #aaa;"), e.add(".gantt_default_divider, .gantt_default_splitter", "background-color: #aaa;"), e.add(".gantt_default_divider_horizontal", "background-color: #aaa;"), e.add(".gantt_default_matrix_vertical_line", "background-color: #eee;"), e.add(".gantt_default_matrix_vertical_break", "background-color: #000;"), e.add(".gantt_default_matrix_horizontal_line", "background-color: #eee;"), e.add(".gantt_default_resourcedivider", "background-color: #aaa;"), e.add(".gantt_default_shadow_inner", "background-color: #666666;opacity: 0.5;filter: alpha(opacity=50);height: 100%;"), e.add(".gantt_default_event", "font-size:12px;color:#666;"), e.add(".gantt_default_event_inner", "position:absolute;top:0px;left:0px;right:0px;bottom:0px;padding:5px 2px 2px 2px;overflow:hidden;border:1px solid #ccc;"), e.add(".gantt_default_event_bar", "top:0px;left:0px;right:0px;height:4px;background-color:#9dc8e8;"), e.add(".gantt_default_event_bar_inner", "position:absolute;height:4px;background-color:#1066a8;"), e.add(".gantt_default_event_inner", 'background:#fff;background: -webkit-gradient(linear, left top, left bottom, from(#ffffff), to(#eeeeee));background: -webkit-linear-gradient(top, #ffffff 0%, #eeeeee);background: -moz-linear-gradient(top, #ffffff 0%, #eeeeee);background: -ms-linear-gradient(top, #ffffff 0%, #eeeeee);background: -o-linear-gradient(top, #ffffff 0%, #eeeeee);background: linear-gradient(top, #ffffff 0%, #eeeeee);filter: progid:DXImageTransform.Microsoft.Gradient(startColorStr="#ffffff", endColorStr="#eeeeee");'), e.add(".gantt_default_event_float_inner", "padding:6px 2px 2px 8px;"), e.add(".gantt_default_event_float_inner:after", 'content:"";border-color: transparent #666 transparent transparent;border-style:solid;border-width:5px;width:0;height:0;position:absolute;top:8px;left:-4px;'), e.add(".gantt_default_columnheader_inner", "font-weight: bold;"), e.add(".gantt_default_columnheader_splitter", "background-color: #666;opacity: 0.5;filter: alpha(opacity=50);"), e.add(".gantt_default_columnheader_cell_inner", "padding: 2px;"), e.add(".gantt_default_cell", "background-color: #f9f9f9;"), e.add(".gantt_default_cell.gantt_default_cell_business", "background-color: #fff;"), e.add(".gantt_default_cell.gantt_default_cell_business.gantt_default_cell_selected,.gantt_default_cell.gantt_default_cell_selected", "background-color: #ccc;background-image: -webkit-gradient(linear, 0 100%, 100% 0,\tcolor-stop(.25, rgba(255, 255, 255, .2)), color-stop(.25, transparent),\tcolor-stop(.5, transparent), color-stop(.5, rgba(255, 255, 255, .2)), color-stop(.75, rgba(255, 255, 255, .2)), color-stop(.75, transparent), to(transparent));background-image: -webkit-linear-gradient(45deg, rgba(255, 255, 255, .2) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, .2) 50%, rgba(255, 255, 255, .2) 75%, transparent 75%, transparent);background-image: -moz-linear-gradient(45deg, rgba(255, 255, 255, .2) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, .2) 50%, rgba(255, 255, 255, .2) 75%, transparent 75%, transparent);background-image: -ms-linear-gradient(45deg, rgba(255, 255, 255, .2) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, .2) 50%, rgba(255, 255, 255, .2) 75%, transparent 75%, transparent);background-image: -o-linear-gradient(45deg, rgba(255, 255, 255, .2) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, .2) 50%, rgba(255, 255, 255, .2) 75%, transparent 75%, transparent);background-image: linear-gradient(45deg, rgba(255, 255, 255, .2) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, .2) 50%, rgba(255, 255, 255, .2) 75%, transparent 75%, transparent);-webkit-background-size: 20px 20px;-moz-background-size: 20px 20px;background-size: 20px 20px;"), e.add(".gantt_default_tree_image_no_children", "background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAkAAAAJCAIAAABv85FHAAAAKXRFWHRDcmVhdGlvbiBUaW1lAHDhIDMwIEkgMjAwOSAwODo0NjozMSArMDEwMClDkt4AAAAHdElNRQfZAR4HLzEyzsCJAAAACXBIWXMAAA7CAAAOwgEVKEqAAAAABGdBTUEAALGPC/xhBQAAADBJREFUeNpjrK6s5uTl/P75OybJ0NLW8h8bAIozgeSxAaA4E1A7VjmgOL31MeLxHwCeXUT0WkFMKAAAAABJRU5ErkJggg==);"), e.add(".gantt_default_tree_image_expand", "background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAkAAAAJCAIAAABv85FHAAAAKXRFWHRDcmVhdGlvbiBUaW1lAHDhIDMwIEkgMjAwOSAwODo0NjozMSArMDEwMClDkt4AAAAHdElNRQfZAR4HLyUoFBT0AAAACXBIWXMAAA7CAAAOwgEVKEqAAAAABGdBTUEAALGPC/xhBQAAAFJJREFUeNpjrK6s5uTl/P75OybJ0NLW8h8bAIozgeRhgJGREc4GijMBtTNgA0BxFog+uA4IA2gmUJwFog/IgUhAGBB9KPYhA3T74Jog+hjx+A8A1KRQ+AN5vcwAAAAASUVORK5CYII=);"), e.add(".gantt_default_tree_image_collapse", "background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAkAAAAJCAIAAABv85FHAAAAKXRFWHRDcmVhdGlvbiBUaW1lAHDhIDMwIEkgMjAwOSAwODo0NjozMSArMDEwMClDkt4AAAAHdElNRQfZAR4HLxB+p9DXAAAACXBIWXMAAA7CAAAOwgEVKEqAAAAABGdBTUEAALGPC/xhBQAAAENJREFUeNpjrK6s5uTl/P75OybJ0NLW8h8bAIozgeSxAaA4E1A7VjmgOAtEHyMjI7IE0EygOAtEH5CDqY9c+xjx+A8ANndK9WaZlP4AAAAASUVORK5CYII=);"), e.add(".gantt_default_event_move_left", 'box-sizing: border-box; padding:2px;border:1px solid #ccc;background:#fff;background: -webkit-gradient(linear, left top, left bottom, from(#ffffff), to(#eeeeee));background: -webkit-linear-gradient(top, #ffffff 0%, #eeeeee);background: -moz-linear-gradient(top, #ffffff 0%, #eeeeee);background: -ms-linear-gradient(top, #ffffff 0%, #eeeeee);background: -o-linear-gradient(top, #ffffff 0%, #eeeeee);background: linear-gradient(top, #ffffff 0%, #eeeeee);filter: progid:DXImageTransform.Microsoft.Gradient(startColorStr="#ffffff", endColorStr="#eeeeee");'), e.add(".gantt_default_event_move_right", 'box-sizing: border-box; padding:2px;border:1px solid #ccc;background:#fff;background: -webkit-gradient(linear, left top, left bottom, from(#ffffff), to(#eeeeee));background: -webkit-linear-gradient(top, #ffffff 0%, #eeeeee);background: -moz-linear-gradient(top, #ffffff 0%, #eeeeee);background: -ms-linear-gradient(top, #ffffff 0%, #eeeeee);background: -o-linear-gradient(top, #ffffff 0%, #eeeeee);background: linear-gradient(top, #ffffff 0%, #eeeeee);filter: progid:DXImageTransform.Microsoft.Gradient(startColorStr="#ffffff", endColorStr="#eeeeee");'), e.add(".gantt_default_event_delete", "background: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAsAAAALCAYAAACprHcmAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAadEVYdFNvZnR3YXJlAFBhaW50Lk5FVCB2My41LjExR/NCNwAAAI5JREFUKFNtkLERgCAMRbmzdK8s4gAUlhYOYEHJEJYOYOEwDmGBPxC4kOPfvePy84MGR0RJ2N1A8H3N6DATwSQ57m2ql8NBG+AEM7D+UW+wjdfUPgerYNgB5gOLRHqhcasg84C2QxPMtrUhSqQIhg7ypy9VM2EUZPI/4rQ7rGxqo9sadTegw+UdjeDLAKUfhbaQUVPIfJYAAAAASUVORK5CYII=) center center no-repeat; opacity: 0.6; -ms-filter:'progid:DXImageTransform.Microsoft.Alpha(Opacity=60)';cursor: pointer;"), e.add(".gantt_default_event_delete:hover", "opacity: 1;-ms-filter: none;"), e.add(".gantt_default_rowmove_handle", "background-repeat: no-repeat; background-position: center center; background-color: #ccc; background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAQAAAAKCAYAAACT+/8OAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAadEVYdFNvZnR3YXJlAFBhaW50Lk5FVCB2My41LjExR/NCNwAAAClJREFUGFdj+P//P4O9vX2Bg4NDP4gNFgBytgPxebgAMsYuQGMz/jMAAFsTZDPYJlDHAAAAAElFTkSuQmCC); cursor: move;"), e.add(".gantt_default_rowmove_source", "background-color: black; opacity: 0.2;"), e.add(".gantt_default_rowmove_position_before, .gantt_default_rowmove_position_after", "background-color: #999; height: 2px;"), e.add(".gantt_default_rowmove_position_child", "margin-left: 10px; background-color: #999; height: 2px;"), e.add(".gantt_default_rowmove_position_child:before", "content: '+'; color: #999; position: absolute; top: -8px; left: -10px;"), e.add(".gantt_default_rowmove_position_forbidden", "background-color: red; height: 2px; margin-left: 10px;");
                        e.add(".gantt_default_rowmove_position_forbidden:before", "content: 'x'; color: red; position: absolute; top: -8px; left: -10px;");
                        e.add(".gantt_default_task_group .gantt_default_event_inner", "position:absolute;top:5px;left:0px;right:0px;bottom:6px;overflow:hidden; background: #1155cc; filter: none; border: 0px none;"), e.add(".gantt_default_task_group.gantt_default_event:before", "content:''; border-color: transparent transparent transparent #1155cc; border-style: solid; border-width: 6px; position: absolute; bottom: 0px;"), e.add(".gantt_default_task_group.gantt_default_event:after", "content:''; border-color: transparent #1155cc transparent transparent; border-style: solid; border-width: 6px; position: absolute; bottom: 0px; right: 0px;"), e.add(".gantt_default_task_milestone .gantt_default_event_inner", "position:absolute;top:16%;left:16%;right:16%;bottom:16%; background: #38761d; border: 0px none; -webkit-transform: rotate(45deg);-moz-transform: rotate(45deg);-ms-transform: rotate(45deg);-o-transform: rotate(45deg); transform: rotate(45deg); filter: none;"), e.add(".gantt_default_browser_ie8 .gantt_default_task_milestone .gantt_default_event_inner", "-ms-filter: \"progid:DXImageTransform.Microsoft.Matrix(SizingMethod='auto expand', M11=0.7071067811865476, M12=-0.7071067811865475, M21=0.7071067811865475, M22=0.7071067811865476);\""), e.add(".gantt_default_event_left", "white-space: nowrap; padding-top: 5px; color: #666; cursor: default;"), e.add(".gantt_default_event_right", "white-space: nowrap; padding-top: 5px; color: #666; cursor: default;"), e.add(".gantt_default_link_horizontal", "border-bottom-style: solid; border-bottom-color: red;"), e.add(".gantt_default_link_vertical", "border-right-style: solid; border-right-color: red;"), e.add(".gantt_default_link_arrow_right:before", "content: ''; border-width: 6px; border-color: transparent transparent transparent red; border-style: solid; width: 0px; height:0px; position: absolute;"), e.add(".gantt_default_link_arrow_left:before", "content: ''; border-width: 6px; border-color: transparent red transparent transparent; border-style: solid; width: 0px; height:0px; position: absolute;"), e.add(".gantt_default_link_arrow_down:before", "content: ''; border-width: 6px; border-color: red transparent transparent transparent; border-style: solid; width: 0px; height:0px; position: absolute;"), e.add(".gantt_default_link_arrow_up:before", "content: ''; border-width: 6px; border-color: transparent transparent red transparent; border-style: solid; width: 0px; height:0px; position: absolute;"), e.add(".gantt_default_shadow_overlap .gantt_default_shadow_inner", "background-color: red;"), e.add(".gantt_default_block", "background-color: gray; opacity: 0.5; filter: alpha(opacity=50);"), e.add(".gantt_default_link_hover", "box-shadow: 0px 0px 2px 2px rgba(255, 0, 0, 0.3)"), e.add(".gantt_default_main .gantt_default_header_icon", "box-sizing: border-box; border: 1px solid #aaa; background-color: #f5f5f5; color: #000;"), e.add(".gantt_default_header_icon:hover", "background-color: #ccc;"), e.add(".gantt_default_header_icon_hide:before", "content: '\\00AB';"), e.add(".gantt_default_header_icon_show:before", "content: '\\00BB';"), e.add(".gantt_default_row_new .gantt_default_rowheader_inner", "cursor: text; background-position: 0 50%; background-repeat: no-repeat; background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAoAAAAKCAYAAACNMs+9AAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAABUSURBVChTY0ACslAaK2CC0iCQDMSlECYmQFYIAl1AjFUxukIQwKoYm0IQwFCMSyEIaEJpMMClcD4Qp0CYEIBNIUzRPzAPCtAVYlWEDgyAGIdTGBgAbqEJYyjqa3oAAAAASUVORK5CYII=);"), e.add(".gantt_default_row_new .gantt_default_rowheader_inner:hover", "background: white;"), e.add(".gantt_default_rowheader textarea", "padding: 5px;"), e.add(".gantt_default_rowheader_scroll", "cursor: default;"), e.add(".gantt_default_shadow_forbidden .gantt_default_shadow_inner", "background-color: red;"), e.add(".gantt_default_event_moving_source", "opacity: 0.5; filter: alpha(opacity=50);"), e.add(".gantt_default_event.gantt_default_event_version .gantt_default_event_inner", "background-color: #cfdde8;background-image: -webkit-gradient(linear, 0 100%, 100% 0,\tcolor-stop(.25, rgba(255, 255, 255, .2)), color-stop(.25, transparent),\tcolor-stop(.5, transparent), color-stop(.5, rgba(255, 255, 255, .2)), color-stop(.75, rgba(255, 255, 255, .2)), color-stop(.75, transparent), to(transparent));background-image: -webkit-linear-gradient(45deg, rgba(255, 255, 255, .2) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, .2) 50%, rgba(255, 255, 255, .2) 75%, transparent 75%, transparent);background-image: -moz-linear-gradient(45deg, rgba(255, 255, 255, .2) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, .2) 50%, rgba(255, 255, 255, .2) 75%, transparent 75%, transparent);background-image: -ms-linear-gradient(45deg, rgba(255, 255, 255, .2) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, .2) 50%, rgba(255, 255, 255, .2) 75%, transparent 75%, transparent);background-image: -o-linear-gradient(45deg, rgba(255, 255, 255, .2) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, .2) 50%, rgba(255, 255, 255, .2) 75%, transparent 75%, transparent);background-image: linear-gradient(45deg, rgba(255, 255, 255, .2) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, .2) 50%, rgba(255, 255, 255, .2) 75%, transparent 75%, transparent);-webkit-background-size: 20px 20px;-moz-background-size: 20px 20px;background-size: 20px 20px;"), e.add(".gantt_default_task_group.gantt_default_event_continueleft.gantt_default_event:before", "border:0;"), e.add(".gantt_default_task_group.gantt_default_event_continueleft.gantt_default_event .gantt_default_event_inner:before", 'content:"";border-color: transparent #fff transparent transparent;border-style:solid;border-width:5px;width:0;height:0;position:absolute;top:2px;left:-4px;'), e.add(".gantt_default_event_continueleft.gantt_default_event .gantt_default_event_inner", "padding-left: 8px;"), e.add(".gantt_default_event_continueleft.gantt_default_event .gantt_default_event_inner:before", 'content:"";border-color: transparent #000 transparent transparent;border-style:solid;border-width:5px;width:0;height:0;position:absolute;top:7px;left:-4px;'), e.add(".gantt_default_task_group.gantt_default_event_continueright.gantt_default_event:after", "border:0;"), e.add(".gantt_default_task_group.gantt_default_event_continueright.gantt_default_event .gantt_default_event_inner:after", 'content:"";border-color: transparent transparent transparent #fff;border-style:solid;border-width:5px;width:0;height:0;position:absolute;top:2px;right:-2px;'), e.add(".gantt_default_event_continueright.gantt_default_event .gantt_default_event_inner", "padding-right: 8px;"), e.add(".gantt_default_event_continueright.gantt_default_event .gantt_default_event_inner:after", 'content:"";border-color: transparent transparent transparent #000;border-style:solid;border-width:5px;width:0;height:0;position:absolute;top:7px;right:-2px;'), e.add(".gantt_default_linkpoint", "background-color: white; border: 1px solid gray; border-radius: 5px;"), e.add(".gantt_default_linkpoint.gantt_default_linkpoint_hover", "background-color: black;"), e.add(".gantt_default_crosshair_vertical, .gantt_default_crosshair_horizontal, .gantt_default_crosshair_left, .gantt_default_crosshair_top", "background-color: gray; opacity: 0.2; filter: alpha(opacity=20)"), e.add(".kanban_default_main *, .kanban_default_main *:before, .kanban_default_main *:after", "box-sizing: content-box;"), e.add(".kanban_default_main", "box-sizing: content-box; border: 1px solid #c0c0c0;font-family: Tahoma, Arial, Helvetica, sans-serif; font-size: 12px;"), e.add(".kanban_default_selected .kanban_default_event_inner", "background: #ddd;"), e.add(".kanban_default_timeheader", "cursor: default;color: #333;"), e.add(".kanban_default_message", "opacity: 0.9;filter: alpha(opacity=90);padding: 10px; color: #ffffff;background: #ffa216;"), e.add(".kanban_default_colheadercell", "color: #333;background: #f3f3f3;"), e.add(".kanban_default_rowheader,.kanban_default_corner", "color: #333;background: #f3f3f3;"), e.add(".kanban_default_rowheader.kanban_default_rowheader_selected", "background-color: #aaa;background-image: -webkit-gradient(linear, 0 100%, 100% 0,color-stop(.25, rgba(255, 255, 255, .2)), color-stop(.25, transparent),\tcolor-stop(.5, transparent), color-stop(.5, rgba(255, 255, 255, .2)), color-stop(.75, rgba(255, 255, 255, .2)), color-stop(.75, transparent), to(transparent));background-image: -webkit-linear-gradient(45deg, rgba(255, 255, 255, .2) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, .2) 50%, rgba(255, 255, 255, .2) 75%, transparent 75%, transparent);background-image: -moz-linear-gradient(45deg, rgba(255, 255, 255, .2) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, .2) 50%, rgba(255, 255, 255, .2) 75%, transparent 75%, transparent);background-image: -ms-linear-gradient(45deg, rgba(255, 255, 255, .2) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, .2) 50%, rgba(255, 255, 255, .2) 75%, transparent 75%, transparent);background-image: -o-linear-gradient(45deg, rgba(255, 255, 255, .2) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, .2) 50%, rgba(255, 255, 255, .2) 75%, transparent 75%, transparent);background-image: linear-gradient(45deg, rgba(255, 255, 255, .2) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, .2) 50%, rgba(255, 255, 255, .2) 75%, transparent 75%, transparent);-webkit-background-size: 20px 20px;-moz-background-size: 20px 20px;background-size: 20px 20px;"), e.add(".kanban_default_rowheader_inner", "position: absolute;left: 0px;right: 0px;top: 0px;bottom: 0px;border-right: 1px solid #f3f3f3;padding: 2px;"), e.add(".kanban_default_colheadercell", "text-align: center;"), e.add(".kanban_default_colheadercell_inner", "position: absolute;left: 0px;right: 0px;top: 0px;bottom: 0px;border-right: 1px solid #c0c0c0;"), e.add(".kanban_default_divider, .kanban_default_splitter", "background-color: #c0c0c0;"), e.add(".kanban_default_divider_horizontal", "background-color: #c0c0c0;"), e.add(".kanban_default_matrix_vertical_line", "background-color: #eee;"), e.add(".kanban_default_matrix_vertical_break", "background-color: #000;"), e.add(".kanban_default_matrix_horizontal_line", "background-color: #eee;"), e.add(".kanban_default_rowheaderdivider", "background-color: #c0c0c0;"), e.add(".kanban_default_shadow_inner", "background-color: #666666;opacity: 0.5;filter: alpha(opacity=50);height: 100%;"), e.add(".kanban_default_card", "font-size:12px;color:#333;"), e.add(".kanban_default_card_inner", "position:absolute;top:0px;left:0px;right:0px;bottom:0px;padding:5px 2px 2px 2px;overflow:hidden;border:1px solid #ccc;"), e.add(".kanban_default_card_inner", 'background:#fff;background: -webkit-gradient(linear, left top, left bottom, from(#ffffff), to(#eeeeee));background: -webkit-linear-gradient(top, #ffffff 0%, #eeeeee);background: -moz-linear-gradient(top, #ffffff 0%, #eeeeee);background: -ms-linear-gradient(top, #ffffff 0%, #eeeeee);background: -o-linear-gradient(top, #ffffff 0%, #eeeeee);background: linear-gradient(top, #ffffff 0%, #eeeeee);filter: progid:DXImageTransform.Microsoft.Gradient(startColorStr="#ffffff", endColorStr="#eeeeee");'), e.add(".kanban_default_cell", "background-color: #fff;"), e.add(".kanban_default_cell.kanban_default_cell_selected", "background-color: #ccc;background-image: -webkit-gradient(linear, 0 100%, 100% 0,\tcolor-stop(.25, rgba(255, 255, 255, .2)), color-stop(.25, transparent),\tcolor-stop(.5, transparent), color-stop(.5, rgba(255, 255, 255, .2)), color-stop(.75, rgba(255, 255, 255, .2)), color-stop(.75, transparent), to(transparent));background-image: -webkit-linear-gradient(45deg, rgba(255, 255, 255, .2) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, .2) 50%, rgba(255, 255, 255, .2) 75%, transparent 75%, transparent);background-image: -moz-linear-gradient(45deg, rgba(255, 255, 255, .2) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, .2) 50%, rgba(255, 255, 255, .2) 75%, transparent 75%, transparent);background-image: -ms-linear-gradient(45deg, rgba(255, 255, 255, .2) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, .2) 50%, rgba(255, 255, 255, .2) 75%, transparent 75%, transparent);background-image: -o-linear-gradient(45deg, rgba(255, 255, 255, .2) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, .2) 50%, rgba(255, 255, 255, .2) 75%, transparent 75%, transparent);background-image: linear-gradient(45deg, rgba(255, 255, 255, .2) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, .2) 50%, rgba(255, 255, 255, .2) 75%, transparent 75%, transparent);-webkit-background-size: 20px 20px;-moz-background-size: 20px 20px;background-size: 20px 20px;"), e.add(".kanban_default_tree_image_no_children", "background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAkAAAAJCAIAAABv85FHAAAAKXRFWHRDcmVhdGlvbiBUaW1lAHDhIDMwIEkgMjAwOSAwODo0NjozMSArMDEwMClDkt4AAAAHdElNRQfZAR4HLzEyzsCJAAAACXBIWXMAAA7CAAAOwgEVKEqAAAAABGdBTUEAALGPC/xhBQAAADBJREFUeNpjrK6s5uTl/P75OybJ0NLW8h8bAIozgeSxAaA4E1A7VjmgOL31MeLxHwCeXUT0WkFMKAAAAABJRU5ErkJggg==);"), e.add(".kanban_default_tree_image_expand", "background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAkAAAAJCAIAAABv85FHAAAAKXRFWHRDcmVhdGlvbiBUaW1lAHDhIDMwIEkgMjAwOSAwODo0NjozMSArMDEwMClDkt4AAAAHdElNRQfZAR4HLyUoFBT0AAAACXBIWXMAAA7CAAAOwgEVKEqAAAAABGdBTUEAALGPC/xhBQAAAFJJREFUeNpjrK6s5uTl/P75OybJ0NLW8h8bAIozgeRhgJGREc4GijMBtTNgA0BxFog+uA4IA2gmUJwFog/IgUhAGBB9KPYhA3T74Jog+hjx+A8A1KRQ+AN5vcwAAAAASUVORK5CYII=);"), e.add(".kanban_default_tree_image_collapse", "background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAkAAAAJCAIAAABv85FHAAAAKXRFWHRDcmVhdGlvbiBUaW1lAHDhIDMwIEkgMjAwOSAwODo0NjozMSArMDEwMClDkt4AAAAHdElNRQfZAR4HLxB+p9DXAAAACXBIWXMAAA7CAAAOwgEVKEqAAAAABGdBTUEAALGPC/xhBQAAAENJREFUeNpjrK6s5uTl/P75OybJ0NLW8h8bAIozgeSxAaA4E1A7VjmgOAtEHyMjI7IE0EygOAtEH5CDqY9c+xjx+A8ANndK9WaZlP4AAAAASUVORK5CYII=);"), e.add(".kanban_default_card_delete", "background: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAsAAAALCAYAAACprHcmAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAadEVYdFNvZnR3YXJlAFBhaW50Lk5FVCB2My41LjExR/NCNwAAAI5JREFUKFNtkLERgCAMRbmzdK8s4gAUlhYOYEHJEJYOYOEwDmGBPxC4kOPfvePy84MGR0RJ2N1A8H3N6DATwSQ57m2ql8NBG+AEM7D+UW+wjdfUPgerYNgB5gOLRHqhcasg84C2QxPMtrUhSqQIhg7ypy9VM2EUZPI/4rQ7rGxqo9sadTegw+UdjeDLAKUfhbaQUVPIfJYAAAAASUVORK5CYII=) center center no-repeat; opacity: 0.6; -ms-filter:'progid:DXImageTransform.Microsoft.Alpha(Opacity=60)';cursor: pointer;"), e.add(".kanban_default_card_delete:hover", "opacity: 1;-ms-filter: none;"), e.add(".kanban_default_rowmove_source", "background-color: black; opacity: 0.2;"), e.add(".kanban_default_rowmove_position_before, .kanban_default_rowmove_position_after", "background-color: #999; height: 2px;"), e.add(".kanban_default_rowmove_position_forbidden", "background-color: red; height: 2px; margin-left: 10px;"), e.add(".kanban_default_rowmove_position_forbidden:before", "content: 'x'; color: red; position: absolute; top: -8px; left: -10px;"), e.add(".kanban_default_block", "background-color: gray; opacity: 0.5; filter: alpha(opacity=50);"), e.add(".kanban_default_main .kanban_default_header_icon", "box-sizing: border-box; border: 1px solid #aaa; background-color: #f5f5f5; color: #000;"), e.add(".kanban_default_header_icon:hover", "background-color: #ccc;"), e.add(".kanban_default_header_icon_hide:before", "content: '\\00AB';"), e.add(".kanban_default_header_icon_show:before", "content: '\\00BB';"), e.add(".kanban_default_row_new .kanban_default_rowheader_inner", "cursor: text; background-position: 0px 5px; background-repeat: no-repeat; background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAoAAAAKCAYAAACNMs+9AAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAABUSURBVChTY0ACslAaK2CC0iCQDMSlECYmQFYIAl1AjFUxukIQwKoYm0IQwFCMSyEIaEJpMMClcD4Qp0CYEIBNIUzRPzAPCtAVYlWEDgyAGIdTGBgAbqEJYyjqa3oAAAAASUVORK5CYII=);"), e.add(".kanban_default_row_new .kanban_default_rowheader_inner:hover", "background: white;"), e.add(".kanban_default_rowheader textarea", "padding: 3px;"), e.add(".kanban_default_rowheader_scroll", "cursor: default;"), e.add(".kanban_default_card_moving_source", "opacity: 0.5; filter: alpha(opacity=50);"), e.add(".kanban_default_card .kanban_default_card_inner", "padding: 5px 5px 5px 10px;"), e.add(".kanban_default_cell", "background-color: #fff;"), e.add(".kanban_default_rowmove_handle", "opacity: 0.5; background-repeat: no-repeat; background-position: center center; background-color: #ccc; background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAQAAAAKCAYAAACT+/8OAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAadEVYdFNvZnR3YXJlAFBhaW50Lk5FVCB2My41LjExR/NCNwAAAClJREFUGFdj+P//P4O9vX2Bg4NDP4gNFgBytgPxebgAMsYuQGMz/jMAAFsTZDPYJlDHAAAAAElFTkSuQmCC); cursor: move;"), e.add(".kanban_default_card_header", "font-size: 120%; font-weight: bold;"), e.add(".kanban_default_card_bar", "background-color: #cc0000; cursor:move;"), e.add(".kanban_default_cell.kanban_default_collapsed", "background-color: #eee;"), e.add(".kanban_default_swimlane_collapse", "cursor:pointer; border: 1px solid #ccc; text-align:center;"), e.add(".kanban_default_swimlane_collapse:before", "content: '^';"), e.add(".kanban_default_swimlane_expand", "cursor:pointer; border: 1px solid #ccc; text-align:center;"), e.add(".kanban_default_swimlane_expand:before", "content: '>';"), e.add(".kanban_default_columnmove_handle", "opacity: 0.5; background-repeat: no-repeat; background-position: center center; background-color: #ccc; background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAQAAAAKCAYAAACT+/8OAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAadEVYdFNvZnR3YXJlAFBhaW50Lk5FVCB2My41LjExR/NCNwAAAClJREFUGFdj+P//P4O9vX2Bg4NDP4gNFgBytgPxebgAMsYuQGMz/jMAAFsTZDPYJlDHAAAAAElFTkSuQmCC); cursor: move;"), e.add(".kanban_default_columnmove_position", "background-color: #333;"), e.add(".kanban_default_crosshair_vertical, .kanban_default_crosshair_horizontal, .kanban_default_crosshair_left, .kanban_default_crosshair_top", "background-color: gray; opacity: 0.2; filter: alpha(opacity=20)"), e.commit(), DayPilot.Global.defaultCss = !0
                    }
                }();
            var o = function(e) {
                var t = this;
                this.id = e, this.widths = [], this.titles = [], this.height = null, this.splitterWidth = 3, this.css = {}, this.css.title = null, this.css.titleInner = null, this.css.splitter = null, this.blocks = [], this.drag = {}, this.updated = function() {}, this.updating = function() {}, this.init = function() {
                    var i;
                    if (!e) throw "error: id not provided";
                    if ("string" == typeof e) i = document.getElementById(e);
                    else {
                        if (!e.appendChild) throw "error: invalid object provided";
                        i = e
                    }
                    this.div = i, this.blocks = [];
                    for (var n = 0; n < this.widths.length; n++) {
                        var a = document.createElement("div");
                        a.style.display = "inline-block", null !== t.height ? a.style.height = t.height + "px" : a.style.height = "100%", a.style.width = this.widths[n] - this.splitterWidth + "px", a.style.overflow = "hidden", a.style.verticalAlign = "top", a.style.position = "relative", a.setAttribute("unselectable", "on"), a.className = this.css.title, i.appendChild(a);
                        var o = document.createElement("div");
                        o.innerHTML = this.titles[n], o.setAttribute("unselectable", "on"), o.className = this.css.titleInner, a.appendChild(o);
                        var r = document.createElement("div");
                        r.style.display = "inline-block", null !== t.height ? r.style.height = t.height + "px" : r.style.height = "100%", r.style.width = this.splitterWidth + "px", r.style.position = "relative", r.appendChild(document.createElement("div")), r.style.cursor = "col-resize", r.setAttribute("unselectable", "on"), r.className = this.css.splitter;
                        var l = {};
                        l.index = n, l.width = this.widths[n], r.data = l, r.onmousedown = function(e) {
                            t.drag.start = DayPilot.page(e),
                                t.drag.data = this.data, t.div.style.cursor = "col-resize", e = e || window.event, e.preventDefault ? e.preventDefault() : e.returnValue = !1
                        }, i.appendChild(r);
                        var s = {};
                        s.section = a, s.handle = r, this.blocks.push(s)
                    }
                    this.registerGlobalHandlers()
                }, this.updateWidths = function() {
                    for (var e = 0; e < this.blocks.length; e++) {
                        var t = this.blocks[e],
                            i = this.widths[e];
                        t.handle.data.width = i, this.m(e)
                    }
                }, this.m = function(e) {
                    var t = this.blocks[e],
                        i = this.widths[e];
                    t.section.style.width = i - this.splitterWidth + "px"
                }, this.totalWidth = function() {
                    for (var e = 0, t = 0; t < this.widths.length; t++) e += this.widths[t];
                    return e
                }, this.gMouseMove = function(e) {
                    if (t.drag.start) {
                        var i = t.drag.data,
                            n = DayPilot.page(e),
                            a = n.x - t.drag.start.x,
                            o = i.index;
                        t.widths[o] = Math.max(5, i.width + a), t.m(o);
                        var r = {};
                        r.widths = this.widths, r.index = i.index, t.updating(r)
                    }
                }, this.gMouseUp = function(e) {
                    if (t.drag.start) {
                        t.drag.start = null, document.body.style.cursor = "", t.div.style.cursor = "";
                        var i = t.drag.data;
                        i.width = t.widths[i.index];
                        var n = {};
                        n.widths = this.widths, n.index = i.index, t.updated(n)
                    }
                }, this.dispose = function() {
                    DayPilot.list(this.blocks).each(function(e) {
                        e.handle.onmousedown = null, DayPilot.de(e.section), DayPilot.de(e.handle)
                    }), this.unregisterGlobalHandlers()
                }, this.registerGlobalHandlers = function() {
                    DayPilot.re(document, "mousemove", this.gMouseMove), DayPilot.re(document, "mouseup", this.gMouseUp)
                }, this.unregisterGlobalHandlers = function() {
                    DayPilot.ue(document, "mousemove", this.gMouseMove), DayPilot.ue(document, "mouseup", this.gMouseUp)
                }
            };
            DayPilot.Splitter = o
        }
    }(),


  
    
    
    
    function() {
        "undefined" != typeof Sys && Sys.Application && Sys.Application.notifyScriptLoaded && Sys.Application.notifyScriptLoaded()
    }(), 
    "undefined" == typeof DayPilot) var DayPilot = {};

