

if ("undefined" == typeof DayPilot.Global && (DayPilot.Global = {}), function() {
        var e = function() {};
        if ("undefined" == typeof DayPilot.Month) {
            var t = {};
            DayPilot.Month = function(i, n) {
                    this.v = "2018.1.3151", this.nav = {};
                    var a = this;
                    this.id = i, this.isMonth = !0, this.api = 2, this.A = !1, this.hideUntilInit = !0, this.startDate = new DayPilot.Date, this.width = null, "function" == typeof DayPilot.Bubble ? this.bubble = new DayPilot.Bubble : this.bubble = null, this.cssClassPrefix = "month_default", this.cellHeight = 100, this.cellMarginBottom = 0, this.allowMultiSelect = !0, this.autoRefreshCommand = "refresh", this.autoRefreshEnabled = !1, this.autoRefreshInterval = 60, this.autoRefreshMaxCount = 20, this.doubleClickTimeout = 300, this.eventEndSpec = "DateTime", this.eventFontColor = "#000000", this.eventFontFamily = "Tahoma", this.eventFontSize = "11px", this.headerBackColor = "#ECE9D8", this.headerFontColor = "#000000", this.headerFontFamily = "Tahoma", this.headerFontSize = "10pt", this.headerHeight = 20, this.heightSpec = "Auto", this.weekStarts = 0, this.innerBorderColor = "#cccccc", this.borderColor = "black", this.eventHeight = 25, this.cellHeaderHeight = 16, this.clientState = {}, this.afterRender = function() {}, this.backColor = "#FFFFD5", this.nonBusinessBackColor = "#FFF4BC", this.cssOnly = !0, this.eventBackColor = "White", this.eventBorderColor = "Black", this.eventCorners = "Regular", this.eventFontColor = "#000000", this.eventFontFamily = "Tahoma", this.eventFontSize = "11px", this.eventsLoadMethod = "GET", this.cellWidth = 14.285, this.lineSpace = 1, this.locale = "en-us", this.messageHideAfter = 5e3, this.notifyCommit = "Immediate", this.visible = !0, this.eventMoveToPosition = !1, this.eventTextLayer = "Top", this.eventStartTime = !1, this.eventEndTime = !1, this.eventStartEndWidth = 60, this.eventTextAlignment = null, this.eventTextLeftIndent = 20, this.showWeekend = !0, this.cellMode = !1, this.shadowType = "Fill", this.tapAndHoldTimeout = 300, this.timeFormat = "Auto", this.viewType = "Month", this.weeks = 1, this.eventClickHandling = "Enabled", this.eventDeleteHandling = "Disabled", this.eventDoubleClickHandling = "Enabled", this.eventMoveHandling = "Update", this.eventResizeHandling = "Update", this.eventRightClickHandling = "ContextMenu", this.eventSelectHandling = "Update", this.headerClickHandling = "Enabled", this.timeRangeSelectedHandling = "Enabled", this.timeRangeDoubleClickHandling = "Enabled", this.onEventFilter = null, this.onBeforeEventRender = null, this.onBeforeCellRender = null, this.onBeforeHeaderRender = null, this.onBeforeEventExport = null, this.onBeforeCellExport = null, this.backendUrl = null, this.cellEvents = [], this.elements = {}, this.elements.events = [], this.t = {}, this.t.events = {}, this.events = {}, this.B = 0, this.members = {}, this.members.ignore = ["internal", "nav", "debug", "temp", "elements", "members", "cellProperties"], this.C = "javasc", this.L = function(e, t) {
                        var e = JSON.parse(e);
                        if (e.BubbleGuid) {
                            var i = e.BubbleGuid,
                                n = this.bubbles[i];
                            return delete this.bubbles[i], a.Z(), void("undefined" != typeof e.Result.BubbleHTML && n.updateView(e.Result.BubbleHTML, n))
                        }
                        if (e.CallBackRedirect) return void(document.location.href = e.CallBackRedirect);
                        if ("undefined" != typeof e.ClientState && (a.clientState = e.ClientState), "None" === e.UpdateType) return a._(e.CallBackData, !0), void(e.Message && a.message(e.Message));
                        if (e.VsUpdate) {
                            var o = document.createElement("input");
                            o.type = "hidden", o.name = a.id + "_vsupdate", o.id = o.name, o.value = e.VsUpdate, a.Ge.innerHTML = "", a.Ge.appendChild(o)
                        }
                        a.events.list = e.Events, "undefined" != typeof e.TagFields && (a.tagFields = e.TagFields), "undefined" != typeof e.SortDirections && (a.sortDirections = e.SortDirections), "Full" === e.UpdateType && (a.cellProperties = e.CellProperties, a.headerProperties = e.HeaderProperties, a.startDate = e.StartDate, "undefined" != typeof e.ShowWeekend && (a.showWeekend = e.ShowWeekend), a.headerBackColor = e.HeaderBackColor ? e.HeaderBackColor : a.headerBackColor, a.backColor = e.BackColor ? e.BackColor : a.backColor, a.nonBusinessBackColor = e.NonBusinessBackColor ? e.NonBusinessBackColor : a.nonBusinessBackColor, a.locale = e.Locale ? e.Locale : a.locale, a.timeFormat = e.TimeFormat ? e.TimeFormat : a.timeFormat, "undefined" != typeof e.WeekStarts && (a.weekStarts = e.WeekStarts), a.hashes = e.Hashes), a.multiselect.clear(!0), a.multiselect.initList = e.SelectedEvents, a.O(), a.He(), a.da(), "Full" === e.UpdateType && (a.Ie(), a.Je()), a.la(), a.pa(), a.qa(), a._(e.CallBackData, !0), a.ta(), e.Message && a.message(e.Message)
                    }, this._ = function(e, t) {
                        var i = function(e, t) {
                            return function() {
                                if (a.va()) {
                                    if ("function" == typeof a.onAfterRender) {
                                        var i = {};
                                        i.isCallBack = t, i.data = e, a.onAfterRender(i)
                                    }
                                } else a.afterRender && a.afterRender(e, t)
                            }
                        };
                        window.setTimeout(i(e, t), 0)
                    }, this.va = function() {
                        return 2 === a.api
                    }, this.q = function(e) {
                        var t = this.theme || this.cssClassPrefix;
                        return t ? t + e : ""
                    }, this.da = function() {
                        if (this.events.list) {
                            if ("function" == typeof this.onBeforeEventRender)
                                for (var e = this.events.list.length, t = 0; t < e; t++) this.$c(t);
                            this.cellMode ? this.Ke() : this.Le()
                        }
                    }, this.Z = function() {}, this.Le = function() {
                        if (this.events.list) {
                            for (var e = 0; e < this.events.list.length; e++) {
                                var t = this.events.list[e],
                                    i = null;
                                if ("function" == typeof a.onBeforeEventRender && (i = a.t.events[e]), i) {
                                    if (i.hidden) continue
                                } else if (t.hidden) continue;
                                var n = new DayPilot.Date(t.start),
                                    o = new DayPilot.Date(t.end);
                                if (o = a.Me(o), !(n.getTime() > o.getTime()))
                                    for (var r = 0; r < this.rows.length; r++) {
                                        var l = this.rows[r];
                                        if (l.belongsHere(n, o)) {
                                            var s = new DayPilot.Event(t, a);
                                            if ("function" == typeof a.onEventFilter && a.events.ed) {
                                                var d = {};
                                                if (d.filter = a.events.ed, d.visible = !0, d.e = s, a.onEventFilter(d), !d.visible) break
                                            }
                                            l.events.push(s), "function" == typeof this.onBeforeEventRender && (s.cache = this.t.events[e])
                                        }
                                    }
                            }
                            for (var c = 0; c < this.rows.length; c++) {
                                var l = this.rows[c];
                                l.events.sort(this.gd);
                                for (var h = 0; h < this.rows[c].events.length; h++) {
                                    var u = l.events[h],
                                        f = l.getStartColumn(u),
                                        v = l.getWidth(u);
                                    l.putIntoLine(u, f, v, c)
                                }
                            }
                        }
                    }, this.Ke = function() {
                        this.cellEvents = [];
                        for (var e = 0; e < this.Ne(); e++) {
                            this.cellEvents[e] = [];
                            for (var t = 0; t < this.rows.length; t++) {
                                var i = {},
                                    n = this.firstDate.addDays(7 * t + e);
                                i.start = n, i.end = n.addDays(1), i.events = [], this.cellEvents[e][t] = i
                            }
                        }
                        for (var o = 0; o < this.events.list.length; o++) {
                            var r = this.events.list[o],
                                l = null;
                            if ("function" == typeof a.onBeforeEventRender && (l = a.t.events[o]), l) {
                                if (l.hidden) continue
                            } else if (r.hidden) continue;
                            var s = new DayPilot.Date(r.start),
                                d = new DayPilot.Date(r.start);
                            if (d = a.Me(d), !(s.getTime() > d.getTime()))
                                for (var e = 0; e < this.Ne(); e++)
                                    for (var t = 0; t < this.rows.length; t++) {
                                        var i = this.cellEvents[e][t];
                                        if (s.getTime() >= i.start.getTime() && s.getTime() < i.end.getTime()) {
                                            var c = new DayPilot.Event(r, a);
                                            if ("function" == typeof a.onEventFilter && a.events.ed) {
                                                var h = {};
                                                if (h.filter = a.events.ed, h.visible = !0, h.e = c, a.onEventFilter(h), !h.visible) continue
                                            }
                                            i.events.push(c), "function" == typeof this.onBeforeEventRender && (c.cache = this.t.events[o])
                                        }
                                    }
                        }
                        for (var e = 0; e < this.Ne(); e++)
                            for (var t = 0; t < this.rows.length; t++) {
                                var i = this.cellEvents[e][t];
                                i.events.sort(this.gd)
                            }
                    }, this.O = function() {
                        for (var e = 0; e < this.elements.events.length; e++) {
                            var t = this.elements.events[e];
                            t.event = null, t.click = null, t.parentNode.removeChild(t)
                        }
                        this.elements.events = []
                    }, this.qa = function() {
                        this.t.events = {}, this.cellMode ? this.Oe() : this.Pe(), this.multiselect.redraw()
                    }, this.Oe = function() {
                        this.elements.events = [];
                        for (var e = 0; e < this.Ne(); e++)
                            for (var t = 0; t < this.rows.length; t++)
                                for (var i = this.cellEvents[e][t], n = (this.cells[e][t], 0); n < i.events.length; n++) {
                                    var a = i.events[n];
                                    a.part.colStart = e, a.part.colWidth = 1, a.part.row = t, a.part.line = n, a.part.startsHere = !0, a.part.endsHere = !0, this.Ub(a)
                                }
                    }, this.Pe = function() {
                        this.elements.events = [];
                        for (var e = 0; e < this.rows.length; e++)
                            for (var t = this.rows[e], i = 0; i < t.lines.length; i++)
                                for (var n = t.lines[i], a = 0; a < n.length; a++) this.Ub(n[a])
                    }, this.fd = function(e, t) {
                        if (!(e && t && e.start && t.start)) return 0;
                        var i = e.start().ticks - t.start().ticks;
                        return 0 !== i ? i : t.end().ticks - e.end().ticks
                    }, this.gd = function(e, t) {
                        if (!e || !t) return 0;
                        if (!(e.data && t.data && e.data.sort && t.data.sort && 0 !== e.data.sort.length && 0 !== t.data.sort.length)) return a.fd(e, t);
                        for (var i = 0, n = 0; 0 === i && "undefined" != typeof e.data.sort[n] && "undefined" != typeof t.data.sort[n];) i = e.data.sort[n] === t.data.sort[n] ? 0 : "number" == typeof e.data.sort[n] && "number" == typeof t.data.sort[n] ? e.data.sort[n] - t.data.sort[n] : a.hd(e.data.sort[n], t.data.sort[n], a.sortDirections[n]), n++;
                        return i
                    }, this.hd = function(e, t, i) {
                        var n = "desc" !== i,
                            a = n ? -1 : 1,
                            o = -a;
                        if (null === e && null === t) return 0;
                        if (null === t) return o;
                        if (null === e) return a;
                        var r = [];
                        return r[0] = e, r[1] = t, r.sort(), e === r[0] ? a : o
                    }, this.Qe = function(e, i, n, a, o, r) {
                        o || (o = 0);
                        var l = a;
                        this.shadow = {}, this.shadow.list = [], this.shadow.start = {
                            x: e,
                            y: i
                        }, this.shadow.width = a, this.eventMoveToPosition && (l = 1, this.shadow.position = n);
                        var s = 7 * i + e - o;
                        s < 0 && (l += s, e = 0, i = 0);
                        for (var d = o; d >= 7;) i--, d -= 7;
                        if (d > e) {
                            d > e + (7 - this.Ne()) ? (i--, e = e + 7 - d) : (l = l - d + e, e = 0)
                        } else e -= d;
                        i < 0 && (i = 0, e = 0);
                        var c = null;
                        for (t.resizingEvent ? c = "w-resize" : t.movingEvent && (c = "move"), this.nav.top.style.cursor = c; l > 0 && i < this.rows.length;) {
                            var h = Math.min(this.Ne() - e, l),
                                u = this.rows[i],
                                f = this.Re(i),
                                v = u.getHeight();
                            this.eventMoveToPosition && (f = this.Se(i, n), v = 2);
                            var p = document.createElement("div");
                            p.setAttribute("unselectable", "on"), p.style.position = "absolute", p.style.left = this.Te() * e + "%", p.style.width = this.Te() * h + "%", p.style.top = f + "px", p.style.height = v + "px", p.style.cursor = c;
                            var g = document.createElement("div");
                            g.setAttribute("unselectable", "on"), p.appendChild(g), p.className = this.q("_shadow"), g.className = this.q("_shadow_inner");
                            this.nav.events.appendChild(p), this.shadow.list.push(p), l -= h + 7 - this.Ne(), e = 0, i++
                        }
                    }, this.Ue = function() {
                        if (this.shadow) {
                            this.nav.events;
                            DayPilot.de(this.shadow.list), this.shadow = null, this.nav.top.style.cursor = ""
                        }
                    }, this.Se = function(e, t) {
                        for (var i = 0, n = 0; n < e; n++) i += this.rows[n].getHeight();
                        return i += this.cellHeaderHeight, i += t * l.lineHeight()
                    }, this.Ve = function(e, t) {
                        return this.firstDate.addDays(7 * t + e)
                    }, this.$c = function(e) {
                        var t = this.t.events,
                            i = this.events.list[e],
                            n = {};
                        for (var a in i) n[a] = i[a];
                        if ("function" == typeof this.onBeforeEventRender) {
                            var o = {};
                            o.e = n, o.data = n, this.onBeforeEventRender(o)
                        }
                        t[e] = n
                    }, this.Ub = function(e) {
                        var t = this.cellMode,
                            i = e.part.row,
                            n = e.part.line,
                            r = e.part.colStart,
                            s = e.part.colWidth,
                            d = e.cache || e.data,
                            c = t ? 0 : this.Te() * r,
                            h = t ? 100 : this.Te() * s,
                            u = t ? n * l.lineHeight() : this.Se(i, n),
                            f = document.createElement("div");
                        f.setAttribute("unselectable", "on"), f.style.height = l.eventHeight() + "px", f.style.position = "relative", f.style.overflow = "hidden", f.className = this.q("_event"), d.cssClass && DayPilot.Util.addClass(f, d.cssClass), f.event = e, t ? (f.style.marginRight = "2px", f.style.marginBottom = "2px") : (f.style.width = h + "%", f.style.position = "absolute", f.style.left = c + "%", f.style.top = u + "px"), this.showToolTip && d.toolTip && !this.bubble && (f.title = d.toolTip), f.onclick = this.We, f.ondblclick = this.Xe, f.oncontextmenu = this.Ye, f.onmousedown = this.Xb, f.onmousemove = this.Vb, f.onmouseout = this.Wb, f.ontouchstart = o.onEventTouchStart, f.ontouchmove = o.onEventTouchMove, f.ontouchend = o.onEventTouchEnd, e.part.startsHere || DayPilot.Util.addClass(f, this.q("_event_continueleft")), e.part.endsHere || DayPilot.Util.addClass(f, this.q("_event_continueright"));
                        var v = document.createElement("div");
                        v.setAttribute("unselectable", "on"), v.className = this.q("_event_inner"), d.fontColor && (v.style.color = d.fontColor), e.client.innerHTML() ? v.innerHTML = e.client.innerHTML() : v.innerHTML = e.text(), d.backColor && (v.style.background = d.backColor, (DayPilot.browser.ie9 || DayPilot.browser.ielt9) && (v.style.filter = "")), f.appendChild(v);
                        var p = this.zc.locale(),
                            g = a.eventStartEndWidth;
                        if (this.eventStartTime || d.htmlStart) {
                            var m = "Clock12Hours" === a.zc.timeFormat() ? e.start().toString("h tt", p) : e.start().toString("H", p),
                                y = {
                                    "left": 5,
                                    "top": 3,
                                    "width": g,
                                    "bottom": 2,
                                    "v": "Visible",
                                    "html": d.htmlStart || m,
                                    "css": a.q("_event_timeleft")
                                },
                                b = DayPilot.Areas.createArea(f, e, y);
                            f.appendChild(b), v.style.paddingLeft = g + "px"
                        }
                        if (this.eventEndTime || d.htmlEnd) {
                            var m = "Clock12Hours" === a.zc.timeFormat() ? e.end().toString("h tt", p) : e.end().toString("H", p),
                                y = {
                                    "right": 5,
                                    "top": 3,
                                    "width": g,
                                    "bottom": 2,
                                    "v": "Visible",
                                    "html": d.htmlEnd || m,
                                    "css": a.q("_event_timeright")
                                },
                                b = DayPilot.Areas.createArea(f, e, y);
                            f.appendChild(b), v.style.paddingRight = g + "px"
                        }
                        if (d.areas)
                            for (var w = d.areas, D = 0; D < w.length; D++) {
                                var y = w[D],
                                    k = y.visibility || y.v || "Visible";
                                if ("Visible" === k) {
                                    var b = DayPilot.Areas.createArea(f, e, y);
                                    f.appendChild(b)
                                }
                            }
                        this.elements.events.push(f), t ? this.cells[r][i].body.appendChild(f) : this.nav.events.appendChild(f), a.multiselect.Db(f.event) && a.multiselect.add(f.event, !0);
                        var x = f;
                        if (a.va()) {
                            if ("function" == typeof a.onAfterEventRender) {
                                var P = {};
                                P.e = x.event, P.div = x, a.onAfterEventRender(P)
                            }
                        } else a.afterEventRender && a.afterEventRender(x.event, x)
                    }, this.We = function(e) {
                        o.start || a.Ia(this, e)
                    }, this.Xe = function(e) {
                        a.Ma(this, e)
                    }, this.Vb = function(e) {
                        var i = this,
                            n = i.event;
                        if ("undefined" != typeof t && !t.movingEvent && !t.resizingEvent) {
                            var o = DayPilot.mo3(i, e);
                            if (o) {
                                ! function() {
                                    var e = i;
                                    if (!e.active) {
                                        var t = [],
                                            o = n.cache || n.data;
                                        "Disabled" === a.eventDeleteHandling || o.deleteDisabled || t.push({
                                            "action": "JavaScript",
                                            "v": "Hover",
                                            "w": 17,
                                            "h": 17,
                                            "top": 2,
                                            "right": 2,
                                            "css": a.q("_event_delete"),
                                            "js": function(e) {
                                                a.Ra(e)
                                            }
                                        });
                                        var r = e.event.cache ? e.event.cache.areas : e.event.data.areas;
                                        r && r.length > 0 && (t = t.concat(r)), DayPilot.Areas.showAreas(e, e.event, null, t)
                                    }
                                }(), a.Ze(i.event).each(function(e) {
                                    DayPilot.Util.addClass(e, a.q("_event_hover"))
                                });
                                var r = 6;
                                if (!a.cellMode && o.x <= r && n.client.resizeEnabled() ? n.part.startsHere ? (i.style.cursor = "w-resize", i.dpBorder = "left") : i.style.cursor = "not-allowed" : !a.cellMode && i.clientWidth - o.x <= r && n.client.resizeEnabled() ? n.part.endsHere ? (i.style.cursor = "e-resize", i.dpBorder = "right") : i.style.cursor = "not-allowed" : i.style.cursor = "default", "undefined" != typeof DayPilot.Bubble && a.bubble && "Disabled" !== a.eventHoverHandling)
                                    if (t.movingEvent || t.resizingEvent) DayPilot.Bubble.hideActive();
                                    else {
                                        var l = this._c && o.x === this._c.x && o.y === this._c.y;
                                        l || (this._c = o, a.bubble.showEvent(i.event))
                                    }
                            }
                        }
                    }, this.Wb = function(e) {
                        var t = this;
                        "undefined" != typeof DayPilot.Bubble && a.bubble && a.bubble.hideOnMouseOut(), t.style.cursor = "", a.Ze(t.event).each(function(e) {
                            DayPilot.Util.removeClass(e, a.q("_event_hover"))
                        }), DayPilot.Areas.hideAreas(t, e)
                    }, this.Ye = function() {
                        var e = this;
                        return a.Na(e.event), !1
                    }, this.Xb = function(e) {
                        if (!o.start) {
                            "undefined" != typeof DayPilot.Bubble && (DayPilot.Bubble.hideActive(), DayPilot.Bubble.cancelShowing());
                            var i = this,
                                n = i.event,
                                r = n.part.row,
                                l = n.part.colStart,
                                s = n.part.line,
                                d = n.part.colWidth;
                            e = e || window.event;
                            var c = DayPilot.Util.mouseButton(e);
                            if (e.cancelBubble = !0, e.stopPropagation && e.stopPropagation(), c.left)
                                if ("undefined" != typeof DayPilot.Bubble && a.bubble && DayPilot.Bubble.hideActive(), t.movingEvent = null, "w-resize" === this.style.cursor || "e-resize" === this.style.cursor) {
                                    var h = {};
                                    h.start = {}, h.start.x = l, h.start.y = r, h.event = i.event, h.width = DayPilot.DateUtil.daysSpan(h.event.start(), h.event.end()) + 1, h.direction = this.style.cursor, t.resizingEvent = h
                                } else if ("move" === this.style.cursor || n.client.moveEnabled()) {
                                a.Ue();
                                var u = DayPilot.mo3(a.nav.events, e);
                                if (!u) return;
                                var f = a.$e(u.x, u.y);
                                if (!f) return;
                                var v = DayPilot.DateUtil.daysDiff(n.start(), a.rows[r].start),
                                    p = 7 * f.y + f.x - (7 * r + l);
                                v && (p += v);
                                var g = {};
                                g.start = {}, g.start.x = l, g.start.y = r, g.start.line = s, g.offset = a.eventMoveToPosition ? 0 : p, g.colWidth = d, g.event = i.event, g.coords = u, t.movingEvent = g
                            }
                        }
                    }, this.temp = {}, this.temp.getPosition = function() {
                        if (!a.coords) return null;
                        var e = a.$e(a.coords.x, a.coords.y);
                        if (!e) return null;
                        var t = new DayPilot.Date(a.Ve(e.x, e.y)),
                            e = {};
                        return e.start = t, e.end = t.addDays(1), e
                    }, this.Yb = {};
                    var o = a.Yb;
                    o.active = !1, o.start = !1, o.timeouts = [], o.onEventTouchStart = function(e) {
                        if (!o.active && !o.start) {
                            o.clearTimeouts(), o.start = !0, o.active = !1;
                            var t = this;
                            if (t.event.client.moveEnabled()) {
                                var i = a.tapAndHoldTimeout;
                                o.timeouts.push(window.setTimeout(function() {
                                    o.active = !0, o.start = !1;
                                    var i = o.relativeCoords(e);
                                    o.startMoving(t, i), e.preventDefault()
                                }, i))
                            }
                            e.stopPropagation()
                        }
                    }, o.onEventTouchMove = function(e) {
                        o.clearTimeouts(), o.start = !1
                    }, o.onEventTouchEnd = function(e) {
                        o.clearTimeouts(), o.start && a.Ja(this, e), window.setTimeout(function() {
                            o.start = !1, o.active = !1
                        }, 500)
                    }, o.onMainTouchStart = function(e) {
                        if (!o.active && !o.start) {
                            o.clearTimeouts(), o.start = !0, o.active = !1;
                            var t = a.tapAndHoldTimeout;
                            o.timeouts.push(window.setTimeout(function() {
                                o.active = !0, o.start = !1, e.preventDefault();
                                var t = o.relativeCoords(e);
                                o.startRange(t)
                            }, t))
                        }
                    }, o.onMainTouchMove = function(e) {
                        if (o.clearTimeouts(), o.start = !1, o.active) {
                            e.preventDefault();
                            var t = o.relativeCoords(e);
                            if (o.moving) return void o.updateMoving(t);
                            o.range && o.updateRange(t)
                        }
                    }, o.onMainTouchEnd = function(e) {
                        if (o.clearTimeouts(), o.active) {
                            if (o.moving) {
                                var t = (o.moving, o.moving.event),
                                    i = a.shadow.start,
                                    n = a.shadow.position,
                                    r = o.moving.offset;
                                a.Ue(), o.moving = null, a.Xa(t, i.x, i.y, r, e, n)
                            }
                            if (o.range) {
                                var l = o.range,
                                    i = new DayPilot.Date(a.Ve(l.from.x, l.from.y)),
                                    s = i.addDays(l.width);
                                o.range = null, a.cb(i, s)
                            }
                        }
                        window.setTimeout(function() {
                            o.start = !1, o.active = !1
                        }, 500)
                    }, o.clearTimeouts = function() {
                        for (var e = 0; e < o.timeouts.length; e++) clearTimeout(o.timeouts[e]);
                        o.timeouts = []
                    }, o.relativeCoords = function(e) {
                        var t = a.nav.events,
                            i = e.touches[0].pageX,
                            n = e.touches[0].pageY,
                            o = DayPilot.abs(t);
                        return {
                            x: i - o.x,
                            y: n - o.y,
                            toString: function() {
                                return "x: " + this.x + ", y:" + this.y
                            }
                        }
                    }, o.startMoving = function(e, t) {
                        a.Ue();
                        var i = e.event,
                            n = a.$e(t.x, t.y);
                        if (n) {
                            var r = DayPilot.DateUtil.daysDiff(i.start(), a.rows[i.part.row].start),
                                l = 7 * n.y + n.x - (7 * i.part.row + i.part.colStart);
                            r && (l += r);
                            var s = {};
                            s.start = {}, s.start.x = i.part.colStart, s.start.y = i.part.row, s.start.line = i.part.line, s.offset = a.eventMoveToPosition ? 0 : l, s.colWidth = i.part.colWidth, s.event = i, s.coords = t, o.moving = s, o.updateMoving(t)
                        }
                    }, o.updateMoving = function(e) {
                        var t = a.$e(e.x, e.y);
                        if (t) {
                            var i = a._e(t);
                            a.Ue();
                            var n = o.moving.event,
                                r = o.moving.offset,
                                l = a.cellMode ? 1 : DayPilot.DateUtil.daysSpan(n.start(), n.end()) + 1;
                            l < 1 && (l = 1), a.Qe(t.x, t.y, i, l, r, n)
                        }
                    }, o.startRange = function(e) {
                        var t = a.$e(e.x, e.y);
                        if (t) {
                            a.Ue();
                            var i = {};
                            i.start = {}, i.start.x = t.x, i.start.y = t.y, i.x = t.x, i.y = t.y, i.width = 1, o.range = i, o.updateRange(e)
                        }
                    }, o.updateRange = function(e) {
                        var t = a.$e(e.x, e.y);
                        if (t) {
                            a.Ue();
                            var i = o.range.start,
                                n = 7 * i.y + i.x,
                                r = 7 * t.y + t.x,
                                l = Math.abs(r - n) + 1;
                            l < 1 && (l = 1);
                            var s = n < r ? i : t;
                            o.range.width = l, o.range.from = {
                                x: s.x,
                                y: s.y
                            }, a.Qe(s.x, s.y, 0, l, 0, null)
                        }
                    }, this.isWeekend = function(e) {
                        return 0 === e.dayOfWeek() || 6 === e.dayOfWeek()
                    }, this.af = function() {
                        var e = this.startDate.lastDayOfMonth();
                        if (this.showWeekend) return e;
                        for (; this.isWeekend(e);) e = e.addDays(-1);
                        return e
                    }, this.He = function() {
                        if ("string" == typeof this.startDate && (this.startDate = new DayPilot.Date(this.startDate)), "Month" === this.viewType ? this.startDate = this.startDate.firstDayOfMonth() : this.startDate = this.startDate.getDatePart(), this.firstDate = this.startDate.firstDayOfWeek(l.weekStarts()), !this.showWeekend) {
                            for (var e = this.startDate.addMonths(-1).getMonth(), t = new DayPilot.Date(this.firstDate).addDays(6); this.isWeekend(t);) t = t.addDays(-1);
                            t.getMonth() === e && (this.firstDate = this.firstDate.addDays(7))
                        }
                        var i;
                        this.startDate;
                        if ("Month" === this.viewType) {
                            var n = this.af(),
                                o = DayPilot.DateUtil.daysDiff(this.firstDate, n) + 1;
                            i = Math.ceil(o / 7)
                        } else i = this.weeks;
                        this.days = 7 * i, this.rows = [];
                        for (var r = 0; r < i; r++) {
                            var s = {};
                            s.start = this.firstDate.addDays(7 * r), s.end = s.start.addDays(this.Ne()), s.events = [], s.lines = [], s.index = r, s.minHeight = this.cellHeight, s.calendar = this, s.belongsHere = function(e, t) {
                                return t.getTime() === e.getTime() && e.getTime() === this.start.getTime() || !(t.getTime() <= this.start.getTime() || e.getTime() >= this.end.getTime())
                            }, s.getPartStart = function(e) {
                                return DayPilot.DateUtil.max(this.start, e.start())
                            }, s.getPartEnd = function(e) {
                                return DayPilot.DateUtil.min(this.end, e.rawend())
                            }, s.getStartColumn = function(e) {
                                var t = this.getPartStart(e);
                                return DayPilot.DateUtil.daysDiff(this.start, t)
                            }, s.getWidth = function(e) {
                                return DayPilot.DateUtil.daysSpan(this.getPartStart(e), this.getPartEnd(e)) + 1
                            }, s.putIntoLine = function(e, t, i, n) {
                                for (var a = this, o = 0; o < this.lines.length; o++) {
                                    var r = this.lines[o];
                                    if (r.isFree(t, i)) return r.addEvent(e, t, i, n, o), o
                                }
                                var r = [];
                                return r.isFree = function(e, t) {
                                    for (var i = !0, n = 0; n < this.length; n++) {
                                        var a = this[n];
                                        e + t - 1 < a.part.colStart || e > a.part.colStart + a.part.colWidth - 1 || (i = !1)
                                    }
                                    return i
                                }, r.addEvent = function(e, t, i, n, o) {
                                    e.part.colStart = t, e.part.colWidth = i, e.part.row = n, e.part.line = o, e.part.startsHere = a.start.getTime() <= e.start().getTime(), e.part.endsHere = a.end.getTime() >= e.end().getTime(), this.push(e)
                                }, r.addEvent(e, t, i, n, this.lines.length), this.lines.push(r), this.lines.length - 1
                            }, s.getStart = function() {
                                for (var e = 0, t = 0; t < a.rows.length && t < this.index; t++) e += a.rows[t].getHeight()
                            }, s.getHeight = function() {
                                return Math.max(this.lines.length * l.lineHeight() + a.cellHeaderHeight + a.cellMarginBottom, this.calendar.cellHeight)
                            }, this.rows.push(s)
                        }
                        this.bf = this.firstDate.addDays(7 * i)
                    }, this.cf = function() {
                        switch (this.heightSpec) {
                            case "Auto":
                                for (var e = l.headerHeight(), t = 0; t < this.rows.length; t++) e += this.rows[t].getHeight();
                                return e;
                            case "Fixed":
                                return this.height
                        }
                    }, this.df = function(e, t) {
                        return 7 * t.y + t.x - (7 * e.y + e.x) + 1
                    }, this.kd = {}, this.kd.scope = null, this.kd.notify = function() {
                        a.kd.scope && a.kd.scope["$apply"]()
                    }, this.debug = new DayPilot.Debug(this), this.dc = function() {
                        var e = this.nav.top;
                        this.nav.top.dp = this, e.setAttribute("unselectable", "on"), e.style.MozUserSelect = "none", e.style.KhtmlUserSelect = "none", e.style.WebkitUserSelect = "none", e.style.WebkitTapHighlightColor = "rgba(0,0,0,0)", e.style.WebkitTouchCallout = "none", e.style.position = "relative", this.width && (e.style.width = this.width), e.onselectstart = function(e) {
                            return !1
                        }, e.className = this.q("_main"), this.hideUntilInit && (e.style.visibility = "hidden"), this.visible || (e.style.display = "none"), e.onmousemove = this.jc, e.ontouchstart = o.onMainTouchStart, e.ontouchmove = o.onMainTouchMove, e.ontouchend = o.onMainTouchEnd, this.Ge = document.createElement("div"), this.Ge.style.display = "none", this.nav.top.appendChild(this.Ge);
                        var t = document.createElement("div");
                        t.style.position = "relative", t.style.height = l.headerHeight() + "px", t.oncontextmenu = function() {
                            return !1
                        }, this.nav.top.appendChild(t), this.nav.header = t;
                        var i = document.createElement("div");
                        i.style.zoom = "1";
                        var n = document.createElement("div");
                        n.style.position = "relative", i.appendChild(n), this.nav.top.appendChild(i), this.nav.scrollable = i, this.nav.events = n
                    }, this.jc = function(e) {
                        e.insideMainD = !0, window.event && window.event.srcElement && (window.event.srcElement.inside = !0), a.coords = DayPilot.mo3(a.nav.events, e);
                        var i = a.coords;
                        if (i) {
                            var n = a.$e(i.x, i.y);
                            if (n) {
                                if (t.resizingEvent) {
                                    a.Ue();
                                    var o, r, l = t.resizingEvent;
                                    l.start;
                                    if ("w-resize" === l.direction) {
                                        r = n;
                                        var s = l.event.rawend();
                                        s.getDatePart().getTime() === s.getTime() && (s = s.addDays(-1));
                                        var d = a.ef(s);
                                        o = a.df(n, d)
                                    } else r = a.ef(l.event.start()), o = a.df(r, n);
                                    o < 1 && (o = 1), a.Qe(r.x, r.y, 0, o)
                                } else if (t.movingEvent) {
                                    if (i.x === t.movingEvent.coords.x && i.y === t.movingEvent.coords.y) return;
                                    var c = a._e(n);
                                    a.Ue();
                                    var h = t.movingEvent.event,
                                        u = t.movingEvent.offset,
                                        o = a.cellMode ? 1 : DayPilot.DateUtil.daysSpan(h.start(), h.rawend()) + 1;
                                    o < 1 && (o = 1), a.Qe(n.x, n.y, c, o, u, h)
                                } else if (t.timeRangeSelecting) {
                                    t.cancelCellClick = !0, a.Ue();
                                    var r = t.timeRangeSelecting,
                                        f = 7 * r.y + r.x,
                                        v = 7 * n.y + n.x,
                                        o = Math.abs(v - f) + 1;
                                    o < 1 && (o = 1);
                                    var p = f < v ? r : n;
                                    t.timeRangeSelecting.from = {
                                        x: p.x,
                                        y: p.y
                                    }, t.timeRangeSelecting.width = o, t.timeRangeSelecting.moved = !0, a.Qe(p.x, p.y, 0, o, 0, null)
                                }
                                if (t.drag) {
                                    if (t.gShadow && document.body.removeChild(t.gShadow), t.gShadow = null, !t.movingEvent) {
                                        var g = DayPilot.Date.today(),
                                            e = {
                                                "id": t.drag.id,
                                                "start": g,
                                                "end": g.addSeconds(t.drag.duration),
                                                "text": t.drag.text
                                            },
                                            m = t.drag.data;
                                        if (m) {
                                            var y = ["duration", "element", "remove", "id", "text"];
                                            for (var b in m) DayPilot.contains(y, b) || (e[b] = m[b])
                                        }
                                        var h = new DayPilot.Event(e, a);
                                        h.external = !0;
                                        var w = t.movingEvent = {};
                                        w.event = h, w.coords = i, w.position = 0, w.offset = 0, w.external = !0, w.removeElement = t.drag.element
                                    }
                                    e.cancelBubble = !0
                                }
                            }
                        }
                    }, this._e = function(e) {
                        for (var t = e.relativeY, i = a.rows[e.y], n = a.cellHeaderHeight, o = l.lineHeight(), r = i.lines.length, s = 0; s < i.lines.length; s++) {
                            if (i.lines[s].isFree(e.x, 1)) {
                                r = s;
                                break
                            }
                        }
                        var d = Math.floor((t - n + o / 2) / o),
                            d = Math.min(r, d),
                            d = Math.max(0, d);
                        return d
                    }, this.message = function(e, t, i, n) {
                        if (null !== e) {
                            var o, t = t || this.messageHideAfter || 2e3,
                                r = .8,
                                s = l.headerHeight();
                            if (this.nav.message) o = a.nav.message;
                            else {
                                o = document.createElement("div"), o.setAttribute("unselectable", "on"), o.style.position = "absolute", o.style.right = "0px", o.style.left = "0px", o.style.top = s + "px", o.style.opacity = r, o.style.filter = "alpha(opacity=80)", o.style.display = "none", o.onmousemove = function() {
                                    o.messageTimeout && !o.status && clearTimeout(o.messageTimeout)
                                }, o.onmouseout = function() {
                                    "none" !== a.nav.message.style.display && (o.messageTimeout = setTimeout(a.Ga, 500))
                                };
                                var d = document.createElement("div");
                                d.setAttribute("unselectable", "on"), d.onclick = function() {
                                    a.nav.message.style.display = "none"
                                }, d.className = this.q("_message"), o.appendChild(d);
                                var c = document.createElement("div");
                                c.setAttribute("unselectable", "on"), c.style.position = "absolute", c.className = this.q("_message_close"), c.onclick = function() {
                                    a.nav.message.style.display = "none"
                                }, o.appendChild(c), this.nav.top.appendChild(o), this.nav.message = o
                            }
                            var h = function() {
                                a.nav.message.style.opacity = r, a.nav.message.firstChild.innerHTML = e;
                                var i = function() {
                                    o.messageTimeout = setTimeout(a.Ga, t)
                                };
                                DayPilot.fade(a.nav.message, .2, i)
                            };
                            clearTimeout(o.messageTimeout), "none" !== this.nav.message.style.display ? DayPilot.fade(a.nav.message, -.2, h) : h()
                        }
                    }, this.message.show = function(e) {
                        a.message(e)
                    }, this.message.hide = function() {
                        a.Ga()
                    }, this.Ga = function() {
                        var e = function() {
                            a.nav.message.style.display = "none"
                        };
                        DayPilot.fade(a.nav.message, -.2, e)
                    }, this.R = function(e) {
                        "Parent100Pct" === a.heightSpec && a.la()
                    }, this.la = function() {
                        var e = this.nav.scrollable;
                        if ("Parent100Pct" === this.heightSpec || "Fixed" === this.heightSpec ? (e.style.top = this.headerHeight + "px", e.style.bottom = "0px", e.style.left = "0px", e.style.right = "0px", e.style.overflow = "auto", e.style.position = "absolute") : e.style.position = "relative", "Parent100Pct" === this.heightSpec) {
                            this.nav.top.style.height = "100%";
                            this.nav.top.clientHeight
                        } else this.nav.top.style.height = this.cf() + "px";
                        for (var t = 0; t < this.cells.length; t++)
                            for (var i = 0; i < this.cells[t].length; i++) this.cells[t][i].style.top = this.Re(i) + "px", this.cells[t][i].style.height = this.rows[i].getHeight() + "px";
                        this.gf()
                    }, this.$e = function(e, t) {
                        for (var i = Math.floor(this.nav.top.clientWidth / this.Ne()), n = Math.min(Math.floor(e / i), this.Ne() - 1), a = null, o = 0, r = 0, l = 0, s = 0; s < this.rows.length; s++) {
                            if (r += this.rows[s].getHeight(), t < r) {
                                o = t - l, a = s;
                                break
                            }
                            l = r
                        }
                        null === a && (a = this.rows.length - 1);
                        var d = {};
                        return d.x = n, d.y = a, d.relativeY = o, d
                    }, this.ef = function(e) {
                        for (var t = DayPilot.DateUtil.daysDiff(this.firstDate, e), i = {
                                x: 0,
                                y: 0
                            }; t >= 7;) i.y++, t -= 7;
                        return i.x = t, i
                    }, this.gf = function() {
                        var e = DayPilot.sw(this.nav.scrollable);
                        this.nav.header.style.marginRight = e + "px"
                    };
                    var r = null;
                    this.Je = function() {
                        var e = this.nav.header,
                            t = this.nav.events;
                        this.cells = [];
                        for (var i = 0; i < this.Ne(); i++) {
                            this.cells[i] = [];
                            var n = this.headerProperties ? this.headerProperties[i] : null,
                                o = i + l.weekStarts();
                            if (o > 6 && (o -= 7), !n) {
                                var n = {};
                                n.html = l.locale().dayNames[o]
                            }
                            if ("function" == typeof a.onBeforeHeaderRender) {
                                var s = {};
                                s.header = {}, s.header.dayOfWeek = o;
                                var d = ["html", "backColor", "cssClass"];
                                DayPilot.Util.copyProps(n, s.header, d), a.onBeforeHeaderRender(s), DayPilot.Util.copyProps(s.header, n, d)
                            }
                            var c = document.createElement("div");
                            c.setAttribute("unselectable", "on"), c.style.position = "absolute", c.style.left = this.Te() * i + "%", c.style.width = this.Te() + "%", c.style.top = "0px", c.style.height = l.headerHeight() + "px",
                                function(e) {
                                    c.onclick = function() {
                                        a.Oa(e)
                                    }
                                }(o);
                            var h = document.createElement("div");
                            h.setAttribute("unselectable", "on"), h.className = this.q("_header_inner"), h.innerHTML = n.html, c.appendChild(h), c.className = this.q("_header"), n && (n.cssClass && DayPilot.Util.addClass(c, n.cssClass), n.backColor && (h.style.background = n.backColor)), e.appendChild(c);
                            for (var u = 0; u < this.rows.length; u++) this.hf(i, u, t)
                        }
                        if (!DayPilot.contains(this.nav.top.childNodes, r) && DayPilot.Util.isNullOrUndefined(undefined)) {
                            var f = document.createElement("div");
                            f.style.position = "absolute", f.style.padding = "2px", f.style.top = "0px", f.style.left = "0px", f.style.backgroundColor = "#FF6600", f.style.color = "white", f.innerHTML = "\u0044\u0045" + "\u004D\u004F", r = f, this.nav.top.appendChild(f)
                        }
                    }, this.Ie = function() {
                        for (var e = 0; e < this.cells.length; e++)
                            for (var t = 0; t < this.cells[e].length; t++) this.cells[e][t].onclick = null;
                        this.nav.header.innerHTML = "", this.nav.events.innerHTML = ""
                    }, this.hf = function(t, i, n) {
                        var o = this.rows[i],
                            r = this.firstDate.addDays(7 * i + t),
                            s = this.cellProperties ? this.cellProperties[i * this.Ne() + t] : null,
                            d = null;
                        if (s) d = s["headerHtml"];
                        else {
                            var c = r.getDay();
                            d = 1 === c ? l.locale().monthNames[r.getMonth()] + " " + c : c + ""
                        }
                        if (!s) {
                            var s = {};
                            s.business = !a.isWeekend(r), s.headerHtml = d
                        }
                        if ("function" == typeof a.onBeforeCellRender) {
                            var h = {};
                            h.cell = {}, h.cell.areas = null, h.cell.backColor = null, h.cell.backImage = null, h.cell.backRepeat = null, h.cell.business = a.isWeekend(r), h.cell.headerHtml = d, h.cell.headerBackColor = null, h.cell.cssClass = null, h.cell.html = null, h.cell.start = r, h.cell.end = h.cell.start.addDays(1), DayPilot.Util.copyProps(s, h.cell), a.onBeforeCellRender(h), DayPilot.Util.copyProps(h.cell, s, ["areas", "backColor", "backImage", "backRepeat", "business", "headerHtml", "headerBackColor", "cssClass", "html"])
                        }
                        var u = document.createElement("div");
                        u.setAttribute("unselectable", "on"), u.style.position = "absolute", u.style.cursor = "default", u.style.left = this.Te() * t + "%", u.style.width = this.Te() + "%", u.style.top = this.Re(i) + "px", u.style.height = o.getHeight() + "px", u.d = r, u.x = t, u.y = i, u.props = s;
                        var f = this.startDate.addMonths(-1).getMonth(),
                            v = this.startDate.addMonths(1).getMonth(),
                            p = this.startDate.getMonth(),
                            g = document.createElement("div");
                        g.setAttribute("unselectable", "on"), u.appendChild(g), g.className = this.q("_cell_inner"), g.className = this.q("_cell_inner"), r.getMonth() === p ? u.className = this.q("_cell") : r.getMonth() === f ? u.className = this.q("_cell") + " " + this.q("_previous") : r.getMonth() === v ? u.className = this.q("_cell") + " " + this.q("_next") : e(), s && (s["cssClass"] && DayPilot.Util.addClass(u, s.cssClass), s["business"] && DayPilot.Util.addClass(u, this.q("_cell_business")), s["backColor"] && (g.style.backgroundColor = s["backColor"]), s["backImage"] && (g.style.backgroundImage = "url('" + s["backImage"] + "')"), s["backRepeat"] && (g.style.backgroundRepeat = s["backRepeat"])), u.onmousedown = this.kb, u.onmousemove = this.qb, u.onmouseout = this.pb, u.oncontextmenu = this.if, u.onclick = this.jf, u.ondblclick = this.kf;
                        var m = document.createElement("div");
                        if (m.setAttribute("unselectable", "on"), m.style.height = this.cellHeaderHeight + "px", s && s["headerBackColor"] && (m.style.background = s["headerBackColor"]), m.className = this.q("_cell_header"), m.innerHTML = s.headerHtml, g.appendChild(m), s && s["html"]) {
                            var y = document.createElement("div");
                            y.setAttribute("unselectable", "on"), y.style.height = o.getHeight() - this.cellHeaderHeight + "px", y.style.overflow = "hidden", y.innerHTML = s["html"], g.appendChild(y)
                        }
                        if (this.cellMode) {
                            var b = document.createElement("div");
                            b.setAttribute("unselectable", "on"), b.style.height = this.cellHeight - this.cellHeaderHeight + "px", b.style.overflow = "auto", b.style.position = "relative";
                            var w = document.createElement("div");
                            w.setAttribute("unselectable", "on"), w.style.paddingTop = "1px", w.style.paddingBottom = "1px", b.appendChild(w), g.appendChild(b), u.body = w, u.scrolling = b
                        }
                        if (u.props)
                            for (var D = u.props.areas || [], k = 0; k < D.length; k++) {
                                var x = D[k],
                                    P = x.visibility || x.v || "Visible";
                                if ("Visible" === P) {
                                    var C = DayPilot.Areas.createArea(u, u.props, x);
                                    u.appendChild(C)
                                }
                            }
                        this.cells[t][i] = u, n.appendChild(u), "function" == typeof a.onAfterCellRender && (h.cell.div = u, h.cell.divHeader = m, a.onAfterCellRender(h))
                    }, this.qb = function() {
                        var e = this;
                        e.props && DayPilot.Areas.showAreas(e, e.props)
                    }, this.pb = function(e) {
                        var t = this;
                        t.props && DayPilot.Areas.hideAreas(t, e)
                    }, this.if = function() {
                        var e = this.d;
                        return function(e) {
                            var t = new DayPilot.Date(e),
                                i = t.addDays(1),
                                n = new DayPilot.Selection(t, i, null, a);
                            a.contextMenuSelection && a.contextMenuSelection.show(n)
                        }(e), !1
                    }, this.kf = function() {
                        var e = this.d;
                        if (a.timeouts) {
                            for (var t in a.timeouts) window.clearTimeout(a.timeouts[t]);
                            a.timeouts = null
                        }
                        if ("Disabled" !== a.timeRangeDoubleClickHandling) {
                            var i = new DayPilot.Date(e),
                                n = i.addDays(1);
                            a.db(i, n)
                        }
                    }, this.jf = function() {
                        if (!t.cancelCellClick) {
                            var e = this.d,
                                i = function(e) {
                                    var t = new DayPilot.Date(e),
                                        i = t.addDays(1);
                                    a.cb(t, i)
                                };
                            if ("Disabled" !== a.timeRangeSelectedHandling && "Disabled" === a.timeRangeDoubleClickHandling) return void i(e);
                            a.timeouts || (a.timeouts = []);
                            var n = function(e) {
                                return function() {
                                    i(e)
                                }
                            };
                            a.timeouts.push(window.setTimeout(n(e), a.doubleClickTimeout))
                        }
                    }, this.kb = function(e) {
                        var i = this,
                            n = i.x,
                            o = i.y;
                        if (t.cancelCellClick = !1, i.scrolling) {
                            var r = DayPilot.mo3(i.scrolling, e),
                                l = DayPilot.sw(i.scrolling),
                                s = i.scrolling.offsetWidth;
                            if (r.x > s - l) return
                        }
                        "Disabled" !== a.timeRangeSelectedHandling && (a.Ue(), t.timeRangeSelecting = {
                            "root": a,
                            "x": n,
                            "y": o,
                            "from": {
                                x: n,
                                y: o
                            },
                            "width": 1
                        })
                    }, this.Ne = function() {
                        return this.showWeekend ? 7 : 5
                    }, this.Te = function() {
                        return this.showWeekend ? 14.285 : 20
                    }, this.lf = function(e) {
                        return 6 === e.getUTCDay() || 0 === e.getUTCDay() ? this.nonBusinessBackColor : this.backColor
                    }, this.Re = function(e) {
                        for (var t = 0, i = 0; i < e; i++) t += this.rows[i].getHeight();
                        return t
                    }, this.clearSelection = function() {
                        this.Ue()
                    }, this.F = function(e, t, i) {
                        var n = {};
                        n.action = e, n.parameters = i, n.data = t, n.header = this.G();
                        var o = "JSON" + JSON.stringify(n);
                        __doPostBack(a.uniqueID, o)
                    }, this.H = function(e, t, i, n) {
                        if (!this.r()) return void a.debug.message("Callback invoked without the server-side backend specified. Callback canceled.", "warning");
                        "undefined" == typeof n && (n = "CallBack");
                        var o = {};
                        o.action = e, o.type = n, o.parameters = t, o.data = i, o.header = this.G();
                        var r = "JSON" + JSON.stringify(o);
                        this.backendUrl ? DayPilot.request(this.backendUrl, this.J, r, this.K) : "function" == typeof WebForm_DoCallback && WebForm_DoCallback(this.uniqueID, r, this.L, null, this.callbackError, !0)
                    }, this.r = function() {
                        return "javasc" !== a.C && a.C.indexOf("DCODE") === -1
                    }, this.M = function() {
                        return !("function" != typeof WebForm_DoCallback || !this.uniqueID)
                    }, this.K = function(e) {
                        if ("function" == typeof a.onAjaxError) {
                            var t = {};
                            t.request = e, a.onAjaxError(t)
                        } else "function" == typeof a.ajaxError && a.ajaxError(e)
                    }, this.J = function(e) {
                        a.L(e.responseText)
                    }, this.G = function() {
                        var e = {};
                        return e.v = this.v, e.control = "dpm", e.id = this.id, e.visibleStart = new DayPilot.Date(this.firstDate), e.visibleEnd = e.visibleStart.addDays(this.days), e.clientState = this.clientState, e.cssClassPrefix = a.cssClassPrefix, e.startDate = a.startDate, e.showWeekend = this.showWeekend, e.headerBackColor = this.headerBackColor, e.backColor = this.backColor, e.nonBusinessBackColor = this.nonBusinessBackColor, e.locale = this.locale, e.timeFormat = this.timeFormat, e.weekStarts = this.weekStarts, e.viewType = this.viewType, e.weeks = this.weeks, e.selected = a.multiselect.events(), e.hashes = a.hashes, e
                    }, this.visibleStart = function() {
                        return new DayPilot.Date(this.firstDate)
                    }, this.visibleEnd = function() {
                        return a.visibleStart().addDays(a.days)
                    }, this.Sa = function(e, t, i, n) {
                        if ("PostBack" === e) a.postBack2(t, i, n);
                        else if ("CallBack" === e) a.H(t, i, n, "CallBack");
                        else if ("Immediate" === e) a.H(t, i, n, "Notify");
                        else if ("Queue" === e) a.queue.add(new DayPilot.Action(this, t, i, n));
                        else {
                            if ("Notify" !== e) throw "Invalid event invocation type";
                            "Notify" === l.notifyType() ? a.H(t, i, n, "Notify") : a.queue.add(new DayPilot.Action(a, t, i, n))
                        }
                    }, this.$a = function(e, t) {
                        var i = a._a(t),
                            n = {};
                        n.args = e, n.guid = i, a.H("Bubble", n)
                    }, this._a = function(e) {
                        var t = DayPilot.guid();
                        return this.bubbles || (this.bubbles = []), this.bubbles[t] = e, t
                    }, this.eventClickPostBack = function(e, t) {
                        this.F("EventClick", t, e)
                    }, this.eventClickCallBack = function(e, t) {
                        this.H("EventClick", e, t)
                    }, this.Ia = function(e, i) {
                        t.movingEvent = null, t.resizingEvent = null;
                        var i = i || window.event;
                        i.ctrlKey, i.metaKey;
                        if (i.cancelBubble = !0, i.stopPropagation && i.stopPropagation(), "undefined" != typeof DayPilot.Bubble && DayPilot.Bubble.hideActive(), "Disabled" === a.eventDoubleClickHandling) return void a.Ja(e, i);
                        if (a.timeouts) {
                            for (var n in a.timeouts) window.clearTimeout(a.timeouts[n]);
                            a.timeouts = []
                        } else a.timeouts = [];
                        var o = function(e, t) {
                            return function() {
                                a.Ja(e, t)
                            }
                        };
                        a.timeouts.push(window.setTimeout(o(e, i), a.doubleClickTimeout))
                    }, this.Ja = function(e, t) {
                        var i = e.event;
                        if (i.client.clickEnabled()) {
                            var n = t.ctrlKey,
                                o = t.metaKey;
                            if (a.va()) {
                                var r = {};
                                if (r.e = i, r.div = e, r.originalEvent = t, r.meta = t.metaKey, r.ctrl = t.ctrlKey, r.preventDefault = function() {
                                        this.preventDefault.value = !0
                                    }, "function" == typeof a.onEventClick && (a.onEventClick(r), r.preventDefault.value)) return;
                                switch (a.eventClickHandling) {
                                    case "PostBack":
                                        a.eventClickPostBack(i);
                                        break;
                                    case "CallBack":
                                        a.eventClickCallBack(i);
                                        break;
                                    case "Select":
                                        a.La(e, i, n, o);
                                        break;
                                    case "ContextMenu":
                                        var l = i.client.contextMenu();
                                        l ? l.show(i) : a.contextMenu && a.contextMenu.show(i);
                                        break;
                                    case "Bubble":
                                        a.bubble && a.bubble.showEvent(i)
                                }
                                "function" == typeof a.onEventClicked && a.onEventClicked(r)
                            } else switch (a.eventClickHandling) {
                                case "PostBack":
                                    a.eventClickPostBack(i);
                                    break;
                                case "CallBack":
                                    a.eventClickCallBack(i);
                                    break;
                                case "JavaScript":
                                    a.onEventClick(i);
                                    break;
                                case "Select":
                                    a.La(e, i, n, o);
                                    break;
                                case "ContextMenu":
                                    var l = i.client.contextMenu();
                                    l ? l.show(i) : a.contextMenu && a.contextMenu.show(i);
                                    break;
                                case "Bubble":
                                    a.bubble && a.bubble.showEvent(i)
                            }
                        }
                    }, this.Ra = function(e) {
                        if (a.va()) {
                            var t = {};
                            if (t.e = e, t.preventDefault = function() {
                                    this.preventDefault.value = !0
                                }, "function" == typeof a.onEventDelete && (a.onEventDelete(t), t.preventDefault.value)) return;
                            switch (a.eventDeleteHandling) {
                                case "PostBack":
                                    a.eventDeletePostBack(e);
                                    break;
                                case "CallBack":
                                    a.eventDeleteCallBack(e);
                                    break;
                                case "Update":
                                    a.events.remove(e)
                            }
                            "function" == typeof a.onEventDeleted && a.onEventDeleted(t)
                        } else switch (a.eventDeleteHandling) {
                            case "PostBack":
                                a.eventDeletePostBack(e);
                                break;
                            case "CallBack":
                                a.eventDeleteCallBack(e);
                                break;
                            case "JavaScript":
                                a.onEventDelete(e)
                        }
                    }, this.eventDeletePostBack = function(e, t) {
                        this.F("EventDelete", e, t)
                    }, this.eventDeleteCallBack = function(e, t) {
                        this.H("EventDelete", e, t)
                    }, this.eventDoubleClickPostBack = function(e, t) {
                        this.F("EventDoubleClick", t, e)
                    }, this.eventDoubleClickCallBack = function(e, t) {
                        this.H("EventDoubleClick", e, t)
                    }, this.Ma = function(e, t) {
                        if ("undefined" != typeof DayPilot.Bubble && DayPilot.Bubble.hideActive(), a.timeouts) {
                            for (var i in a.timeouts) window.clearTimeout(a.timeouts[i]);
                            a.timeouts = null
                        }
                        var t = t || window.event,
                            n = e.event;
                        if (a.va()) {
                            var o = {};
                            if (o.e = n, o.preventDefault = function() {
                                    this.preventDefault.value = !0
                                }, "function" == typeof a.onEventDoubleClick && (a.onEventDoubleClick(o), o.preventDefault.value)) return;
                            switch (a.eventDoubleClickHandling) {
                                case "PostBack":
                                    a.eventDoubleClickPostBack(n);
                                    break;
                                case "CallBack":
                                    a.eventDoubleClickCallBack(n);
                                    break;
                                case "Select":
                                    a.La(e, n, t.ctrlKey, t.metaKey);
                                    break;
                                case "Bubble":
                                    a.bubble && a.bubble.showEvent(n)
                            }
                            "function" == typeof a.onEventDoubleClicked && a.onEventDoubleClicked(o)
                        } else switch (a.eventDoubleClickHandling) {
                            case "PostBack":
                                a.eventDoubleClickPostBack(n);
                                break;
                            case "CallBack":
                                a.eventDoubleClickCallBack(n);
                                break;
                            case "JavaScript":
                                a.onEventDoubleClick(n);
                                break;
                            case "Select":
                                a.La(e, n, t.ctrlKey, t.metaKey);
                                break;
                            case "Bubble":
                                a.bubble && a.bubble.showEvent(n)
                        }
                    }, this.La = function(e, t, i, n) {
                        a.fb(e, t, i, n)
                    }, this.eventSelectPostBack = function(e, t, i) {
                        var n = {};
                        n.e = e, n.change = t, this.F("EventSelect", i, n)
                    }, this.eventSelectCallBack = function(e, t, i) {
                        var n = {};
                        n.e = e, n.change = t, this.H("EventSelect", n, i)
                    }, this.fb = function(e, t, i, n) {
                        var o = a.multiselect,
                            r = o.isSelected(t);
                        if (i || n || !r || 1 !== o.list.length)
                            if (a.va()) {
                                o.previous = o.events();
                                var l = {};
                                if (l.e = t, l.selected = o.isSelected(t), l.ctrl = i, l.meta = n, l.preventDefault = function() {
                                        this.preventDefault.value = !0
                                    }, "function" == typeof a.onEventSelect && (a.onEventSelect(l), l.preventDefault.value)) return;
                                switch (a.eventSelectHandling) {
                                    case "PostBack":
                                        a.eventSelectPostBack(t, s);
                                        break;
                                    case "CallBack":
                                        "undefined" != typeof WebForm_InitCallback && (window.hb = "", window.ib = [], WebForm_InitCallback()), a.eventSelectCallBack(t, s);
                                        break;
                                    case "Update":
                                        o.jb(e, i)
                                }
                                "function" == typeof a.onEventSelected && (l.change = o.isSelected(t) ? "selected" : "deselected", l.selected = o.isSelected(t), a.onEventSelected(l))
                            } else {
                                o.previous = o.events(), o.jb(e, i);
                                var s = o.isSelected(t) ? "selected" : "deselected";
                                switch (a.eventSelectHandling) {
                                    case "PostBack":
                                        a.eventSelectPostBack(t, s);
                                        break;
                                    case "CallBack":
                                        "undefined" != typeof WebForm_InitCallback && (window.hb = "", window.ib = [], WebForm_InitCallback()), a.eventSelectCallBack(t, s);
                                        break;
                                    case "JavaScript":
                                        a.onEventSelect(t, s)
                                }
                            }
                    }, this.eventRightClickPostBack = function(e, t) {
                        this.F("EventRightClick", t, e)
                    }, this.eventRightClickCallBack = function(e, t) {
                        this.H("EventRightClick", e, t)
                    }, this.Na = function(e) {
                        if (this.event = e, !e.client.rightClickEnabled()) return !1;
                        if (a.va()) {
                            var t = {};
                            if (t.e = e, t.preventDefault = function() {
                                    this.preventDefault.value = !0
                                }, "function" == typeof a.onEventRightClick && (a.onEventRightClick(t), t.preventDefault.value)) return;
                            switch (a.eventRightClickHandling) {
                                case "PostBack":
                                    a.eventRightClickPostBack(e);
                                    break;
                                case "CallBack":
                                    a.eventRightClickCallBack(e);
                                    break;
                                case "ContextMenu":
                                    var i = e.client.contextMenu();
                                    i ? i.show(e) : a.contextMenu && a.contextMenu.show(this.event);
                                    break;
                                case "Bubble":
                                    a.bubble && a.bubble.showEvent(e)
                            }
                            "function" == typeof a.onEventRightClicked && a.onEventRightClicked(t)
                        } else switch (a.eventRightClickHandling) {
                            case "PostBack":
                                a.eventRightClickPostBack(e);
                                break;
                            case "CallBack":
                                a.eventRightClickCallBack(e);
                                break;
                            case "JavaScript":
                                a.onEventRightClick(e);
                                break;
                            case "ContextMenu":
                                var i = e.client.contextMenu();
                                i ? i.show(e) : a.contextMenu && a.contextMenu.show(this.event);
                                break;
                            case "Bubble":
                                a.bubble && a.bubble.showEvent(e)
                        }
                        return !1
                    }, this.eventMenuClickPostBack = function(e, t, i) {
                        var n = {};
                        n.e = e, n.command = t, this.F("EventMenuClick", i, n)
                    }, this.eventMenuClickCallBack = function(e, t, i) {
                        var n = {};
                        n.e = e, n.command = t, this.H("EventMenuClick", n, i)
                    }, this.ab = function(e, t, i) {
                        switch (i) {
                            case "PostBack":
                                a.eventMenuClickPostBack(t, e);
                                break;
                            case "CallBack":
                                a.eventMenuClickCallBack(t, e)
                        }
                    }, this.eventMovePostBack = function(e, t, i, n, a) {
                        if (!t) throw "newStart is null";
                        if (!i) throw "newEnd is null";
                        var o = {};
                        o.e = e, o.newStart = t, o.newEnd = i, o.position = a, this.F("EventMove", n, o)
                    }, this.eventMoveCallBack = function(e, t, i, n, a) {
                        if (!t) throw "newStart is null";
                        if (!i) throw "newEnd is null";
                        var o = {};
                        o.e = e, o.newStart = t, o.newEnd = i, o.position = a, this.H("EventMove", o, n)
                    }, this.Xa = function(e, t, i, n, o, r, l) {
                        var s = e.start().getTimePart(),
                            d = e.rawend().getDatePart();
                        d.getTime() !== e.rawend().getTime() && (d = d.addDays(1));
                        var c = DayPilot.DateUtil.diff(e.rawend(), d),
                            h = this.Ve(t, i);
                        h = h.addDays(-n);
                        var u = DayPilot.DateUtil.daysSpan(e.start(), e.rawend()) + 1,
                            f = h.addDays(u),
                            v = h.addTime(s),
                            p = f.addTime(c);
                        if (p = a.mf(p), a.va()) {
                            var g = {};
                            if (g.e = e, g.newStart = v, g.newEnd = p, g.position = r, g.ctrl = !1, g.external = l, o && (g.ctrl = o.ctrlKey), g.shift = !1, o && (g.shift = o.shiftKey), g.preventDefault = function() {
                                    this.preventDefault.value = !0
                                }, "function" == typeof a.onEventMove && (a.onEventMove(g), g.preventDefault.value)) return;
                            switch (a.eventMoveHandling) {
                                case "PostBack":
                                    a.eventMovePostBack(e, v, p, null, r);
                                    break;
                                case "CallBack":
                                    a.eventMoveCallBack(e, v, p, null, r);
                                    break;
                                case "Notify":
                                    a.eventMoveNotify(e, v, p, null, r);
                                    break;
                                case "Update":
                                    g.external ? (e.start(v), e.end(p), e.commit(), a.events.add(e)) : (e.start(v), e.end(p), a.events.update(e)), a.Ya()
                            }
                            "function" == typeof a.onEventMoved && a.onEventMoved(g)
                        } else switch (a.eventMoveHandling) {
                            case "PostBack":
                                a.eventMovePostBack(e, v, p, null, r);
                                break;
                            case "CallBack":
                                a.eventMoveCallBack(e, v, p, null, r);
                                break;
                            case "JavaScript":
                                a.onEventMove(e, v, p, o.ctrlKey, o.shiftKey, r);
                                break;
                            case "Notify":
                                a.eventMoveNotify(e, v, p, null, r)
                        }
                    }, this.eventMoveNotify = function(e, t, i, n, o) {
                        var r = new DayPilot.Event(e.copy(), this);
                        e.start(t), e.end(i), e.commit(), a.update(), this.Za("Notify", r, t, i, n, o)
                    }, this.Za = function(e, t, i, n, a, o) {
                        var r = {};
                        r.e = t, r.newStart = i, r.newEnd = n, r.position = o, this.Sa(e, "EventMove", r, a)
                    }, this.eventResizePostBack = function(e, t, i, n) {
                        if (!t) throw "newStart is null";
                        if (!i) throw "newEnd is null";
                        var a = {};
                        a.e = e, a.newStart = t, a.newEnd = i, this.F("EventResize", n, a)
                    }, this.eventResizeCallBack = function(e, t, i, n) {
                        if (!t) throw "newStart is null";
                        if (!i) throw "newEnd is null";
                        var a = {};
                        a.e = e, a.newStart = t, a.newEnd = i, this.H("EventResize", a, n)
                    }, this.Ua = function(e, t, i) {
                        var n = e.start().getTimePart(),
                            o = e.rawend().getDatePart();
                        o != e.rawend() && (o = o.addDays(1));
                        var r = DayPilot.DateUtil.diff(e.rawend(), o),
                            l = this.Ve(t.x, t.y),
                            s = l.addDays(i),
                            d = l.addTime(n),
                            c = s.addTime(r);
                        if (c = a.mf(c), a.va()) {
                            var h = {};
                            if (h.e = e, h.newStart = d, h.newEnd = c, h.preventDefault = function() {
                                    this.preventDefault.value = !0
                                }, "function" == typeof a.onEventResize && (a.onEventResize(h), h.preventDefault.value)) return;
                            switch (a.eventResizeHandling) {
                                case "PostBack":
                                    a.eventResizePostBack(e, d, c);
                                    break;
                                case "CallBack":
                                    a.eventResizeCallBack(e, d, c);
                                    break;
                                case "Notify":
                                    a.eventResizeNotify(e, d, c);
                                    break;
                                case "Update":
                                    e.start(d), e.end(c), a.events.update(e)
                            }
                            "function" == typeof a.onEventResized && a.onEventResized(h)
                        } else switch (a.eventResizeHandling) {
                            case "PostBack":
                                a.eventResizePostBack(e, d, c);
                                break;
                            case "CallBack":
                                a.eventResizeCallBack(e, d, c);
                                break;
                            case "JavaScript":
                                a.onEventResize(e, d, c);
                                break;
                            case "Notify":
                                a.eventResizeNotify(e, d, c)
                        }
                    }, this.eventResizeNotify = function(e, t, i, n) {
                        var o = new DayPilot.Event(e.copy(), this);
                        e.start(t), e.end(i), e.commit(), a.update(), this.Va("Notify", o, t, i, n)
                    }, this.Va = function(e, t, i, n, a) {
                        var o = {};
                        o.e = t, o.newStart = i, o.newEnd = n, this.Sa(e, "EventResize", o, a)
                    }, this.timeRangeSelectedPostBack = function(e, t, i) {
                        var n = {};
                        n.start = e, n.end = t, this.F("TimeRangeSelected", i, n)
                    }, this.timeRangeSelectedCallBack = function(e, t, i) {
                        var n = {};
                        n.start = e, n.end = t, this.H("TimeRangeSelected", n, i)
                    }, this.cb = function(e, t) {
                        if (t = a.mf(t), a.va()) {
                            var i = {};
                            if (i.start = e, i.end = t, i.control = a, i.preventDefault = function() {
                                    this.preventDefault.value = !0
                                }, "function" == typeof a.onTimeRangeSelect && (a.onTimeRangeSelect(i), i.preventDefault.value)) return;
                            switch (a.timeRangeSelectedHandling) {
                                case "PostBack":
                                    a.timeRangeSelectedPostBack(e, t), a.clearSelection();
                                    break;
                                case "CallBack":
                                    a.timeRangeSelectedCallBack(e, t), a.clearSelection()
                            }
                            "function" == typeof a.onTimeRangeSelected && a.onTimeRangeSelected(i)
                        } else switch (a.timeRangeSelectedHandling) {
                            case "PostBack":
                                a.timeRangeSelectedPostBack(e, t), a.clearSelection();
                                break;
                            case "CallBack":
                                a.timeRangeSelectedCallBack(e, t), a.clearSelection();
                                break;
                            case "JavaScript":
                                a.onTimeRangeSelected(e, t)
                        }
                    }, this.timeRangeMenuClickPostBack = function(e, t, i) {
                        var n = {};
                        n.selection = e, n.command = t, this.F("TimeRangeMenuClick", i, n)
                    }, this.timeRangeMenuClickCallBack = function(e, t, i) {
                        var n = {};
                        n.selection = e, n.command = t, this.H("TimeRangeMenuClick", n, i)
                    }, this.bb = function(e, t, i) {
                        switch (i) {
                            case "PostBack":
                                a.timeRangeMenuClickPostBack(t, e);
                                break;
                            case "CallBack":
                                a.timeRangeMenuClickCallBack(t, e)
                        }
                    }, this.headerClickPostBack = function(e, t) {
                        this.F("HeaderClick", t, e)
                    }, this.headerClickCallBack = function(e, t) {
                        this.H("HeaderClick", e, t)
                    }, this.Oa = function(e) {
                        var t = (this.data, {
                            day: e
                        });
                        if (a.va()) {
                            var i = {};
                            if (i.header = {}, i.header.dayOfWeek = e, i.preventDefault = function() {
                                    this.preventDefault.value = !0
                                }, "function" == typeof a.onHeaderClick && (a.onHeaderClick(i), i.preventDefault.value)) return;
                            switch (a.headerClickHandling) {
                                case "PostBack":
                                    a.headerClickPostBack(t);
                                    break;
                                case "CallBack":
                                    a.headerClickCallBack(t)
                            }
                            "function" == typeof a.onHeaderClicked && a.onHeaderClicked(i)
                        } else switch (a.headerClickHandling) {
                            case "PostBack":
                                a.headerClickPostBack(t);
                                break;
                            case "CallBack":
                                a.headerClickCallBack(t);
                                break;
                            case "JavaScript":
                                a.onHeaderClick(t)
                        }
                    }, this.timeRangeDoubleClickPostBack = function(e, t, i) {
                        var n = {};
                        n.start = e, n.end = t, this.F("TimeRangeDoubleClick", i, n)
                    }, this.timeRangeDoubleClickCallBack = function(e, t, i) {
                        var n = {};
                        n.start = e, n.end = t, this.H("TimeRangeDoubleClick", n, i)
                    }, this.db = function(e, t) {
                        if (a.va()) {
                            var i = {};
                            if (i.start = e, i.end = t, i.preventDefault = function() {
                                    this.preventDefault.value = !0
                                }, "function" == typeof a.onTimeRangeDoubleClick && (a.onTimeRangeDoubleClick(i), i.preventDefault.value)) return;
                            switch (a.timeRangeDoubleClickHandling) {
                                case "PostBack":
                                    a.timeRangeDoubleClickPostBack(e, t);
                                    break;
                                case "CallBack":
                                    a.timeRangeDoubleClickCallBack(e, t)
                            }
                            "function" == typeof a.onTimeRangeDoubleClicked && a.onTimeRangeDoubleClicked(i)
                        } else switch (a.timeRangeDoubleClickHandling) {
                            case "PostBack":
                                a.timeRangeDoubleClickPostBack(e, t);
                                break;
                            case "CallBack":
                                a.timeRangeDoubleClickCallBack(e, t);
                                break;
                            case "JavaScript":
                                a.onTimeRangeDoubleClick(e, t)
                        }
                    }, this.commandCallBack = function(e, t) {
                        this.nf();
                        var i = {};
                        i.command = e, this.H("Command", i, t)
                    }, this.commandPostBack = function(e, t) {
                        this.nf();
                        var i = {};
                        i.command = e, this.F("Command", i, t)
                    }, this.jd = function(e) {
                        for (var t = 0; t < a.elements.events.length; t++) {
                            var i = a.elements.events[t];
                            if (i.event === e || i.event.data === e.data) return i
                        }
                        return null
                    }, this.Ze = function(e) {
                        var t = {};
                        t.list = [], t.each = function(e) {
                            if (e)
                                for (var t = 0; t < this.list.length; t++) e(this.list[t])
                        };
                        for (var i = 0; i < this.elements.events.length; i++) {
                            var n = this.elements.events[i];
                            n.event.data === e.data && t.list.push(n)
                        }
                        return t
                    }, this.qd = function(e) {
                        var t = document.createElement("div");
                        t.style.position = "absolute", t.style.top = "-2000px", t.style.left = "-2000px", t.className = this.q(e), document.body.appendChild(t);
                        var i = t.offsetHeight,
                            n = t.offsetWidth;
                        document.body.removeChild(t);
                        var a = {};
                        return a.height = i, a.width = n, a
                    }, this.zc = {};
                    var l = this.zc;
                    l.clearCache = function() {
                        delete a.t.eventHeight, delete a.t.headerHeight
                    }, l.lineHeight = function() {
                        return l.eventHeight() + a.lineSpace
                    }, l.rounded = function() {
                        return "Rounded" === a.eventCorners
                    }, l.loadFromServer = function() {
                        return !!a.backendUrl && ("undefined" == typeof a.events.list || !a.events.list)
                    }, l.locale = function() {
                        return DayPilot.Locale.find(a.locale)
                    }, l.getWeekStart = function() {
                        return a.showWeekend ? a.weekStarts : 1
                    }, l.weekStarts = function() {
                        if (!a.showWeekend) return 1;
                        if ("Auto" === a.weekStarts) {
                            var e = l.locale();
                            return e ? e.weekStarts : 0
                        }
                        return a.weekStarts
                    }, l.notifyType = function() {
                        var e;
                        if ("Immediate" === a.notifyCommit) e = "Notify";
                        else {
                            if ("Queue" !== a.notifyCommit) throw "Invalid notifyCommit value: " + a.notifyCommit;
                            e = "Queue"
                        }
                        return e
                    }, l.eventHeight = function() {
                        if (a.t.eventHeight) return a.t.eventHeight;
                        var e = a.qd("_event_height").height;
                        return e || (e = a.eventHeight), a.t.eventHeight = e, e
                    }, l.headerHeight = function() {
                        if (a.t.headerHeight) return a.t.headerHeight;
                        var e = a.qd("_header_height").height;
                        return e || (e = a.headerHeight), a.t.headerHeight = e, e
                    }, l.timeFormat = function() {
                        return "Auto" !== a.timeFormat ? a.timeFormat : l.locale().timeFormat
                    }, this.exportAs = function(e, t) {
                        var i = s.generate(e, t);
                        return new DayPilot.Export(i)
                    }, this.rd = {};
                    var s = this.rd;
                    s.sd = null, s.td = null, s.generate = function(e, t) {
                        "object" == typeof e && (t = e, e = null);
                        var t = t || {},
                            e = e || t.format || "svg",
                            i = t.scale || 1;
                        if ("config" !== e) {
                            "jpg" === e.toLowerCase() && (e = "jpeg");
                            var n = t.area || "viewport";
                            s.sd = t, s.td = n;
                            var o, r = s.getWidth(),
                                d = s.getHeight();
                            switch (e.toLowerCase()) {
                                case "svg":
                                    o = new DayPilot.Svg(r, d);
                                    break;
                                case "png":
                                    o = new DayPilot.Canvas(r, d, "image/png", i);
                                    break;
                                case "jpeg":
                                    o = new DayPilot.Canvas(r, d, "image/jpeg", i, t.quality);
                                    break;
                                default:
                                    throw "Export format not supported: " + e
                            }
                            var c = s.getRectangles(),
                                h = new DayPilot.StyleReader(a.nav.top).get("background-color"),
                                u = new DayPilot.StyleReader(a.nav.top).get("border-top-color"),
                                f = a.nav.header.firstChild.firstChild,
                                v = new DayPilot.StyleReader(f).get("background-color"),
                                p = new DayPilot.StyleReader(f).getFont(),
                                g = new DayPilot.StyleReader(f).get("color"),
                                m = a.cells[0][0].firstChild,
                                y = (new DayPilot.StyleReader(m).get("background-color"), new DayPilot.StyleReader(m).get("border-right-color")),
                                b = c.grid.w * a.Te() / 100,
                                w = 0;
                            o.fillRect(c.main, "white"), o.fillRect(c.main, h), DayPilot.list.for(a.Ne()).each(function(e) {
                                var t = a.headerProperties ? a.headerProperties[e] : null,
                                    i = e + l.weekStarts();
                                if (i > 6 && (i -= 7), !t) {
                                    var t = {};
                                    t.html = l.locale().dayNames[i]
                                }
                                var n = 0,
                                    r = b,
                                    s = l.headerHeight(),
                                    d = t.html,
                                    c = {
                                        "x": w,
                                        "y": n,
                                        "w": r + 1,
                                        "h": s + 1
                                    },
                                    h = DayPilot.Util.copyProps(c);
                                o.fillRect(c, v), o.rect(c, u), o.text(h, d, p, g, "center"), w += r
                            });
                            var D = c.grid.y;
                            return DayPilot.list(a.rows).each(function(e, t) {
                                var i = e.getHeight(),
                                    n = 0;
                                DayPilot.list.for(a.Ne()).each(function(e) {
                                    var r = a.cells[e][t],
                                        l = r.props,
                                        s = b,
                                        d = new DayPilot.StyleReader(a.cells[e][t].firstChild).get("background-color"),
                                        c = a.cells[e][t].firstChild.firstChild,
                                        h = new DayPilot.StyleReader(c).getFont(),
                                        u = new DayPilot.StyleReader(c).get("color"),
                                        f = new DayPilot.StyleReader(c).get("background-color"),
                                        v = a.firstDate.addDays(7 * t + e),
                                        p = {};
                                    p.cell = {}, p.cell.start = v, p.cell.end = v.addDays(1), p.backColor = d, p.text = l.headerHtml, p.horizontalAlignment = "right", "function" == typeof a.onBeforeCellExport && a.onBeforeCellExport(p);
                                    var g = {
                                        "x": n,
                                        "y": D,
                                        "w": s + 1,
                                        "h": i + 1
                                    };
                                    o.fillRect(g, p.backColor), o.rect(g, y);
                                    var m = {
                                        "x": n,
                                        "y": D,
                                        "w": s - 2,
                                        "h": a.cellHeaderHeight
                                    };
                                    o.fillRect(g, f), o.text(m, p.text, h, u, p.horizontalAlignment), n += s
                                }), D += i
                            }), DayPilot.list(a.rows).each(function(e) {
                                e.getHeight();
                                DayPilot.list(e.lines).each(function(e) {
                                    DayPilot.list(e).each(function(e) {
                                        var t = e.cache || e.data,
                                            i = s.fakeEvent();
                                        i.className += " " + t.cssClass;
                                        var n = i.firstChild,
                                            r = new DayPilot.StyleReader(n).get("border-right-color"),
                                            d = new DayPilot.StyleReader(n).getFont(),
                                            h = new DayPilot.StyleReader(n).get("color"),
                                            u = new DayPilot.StyleReader(n).getBackColor(),
                                            f = {};
                                        f.left = new DayPilot.StyleReader(n).getPx("padding-left"), f.right = new DayPilot.StyleReader(n).getPx("padding-right"), f.top = new DayPilot.StyleReader(n).getPx("padding-top"), f.bottom = new DayPilot.StyleReader(n).getPx("padding-bottom"), DayPilot.de(i);
                                        var v = t.backColor || u,
                                            p = {};
                                        p.e = e, p.text = e.text ? e.text() : e.client.html(), p.fontSize = d.size, p.fontFamily = d.family, p.fontStyle = d.style, p.fontColor = t.fontColor || h, p.backColor = t.backColor || v, p.borderColor = r, p.horizontalAlignment = "left", "function" == typeof a.onBeforeEventExport && a.onBeforeEventExport(p);
                                        var g = b * e.part.colStart,
                                            m = b * e.part.colWidth,
                                            y = a.Se(e.part.row, e.part.line) + c.grid.y + 2,
                                            w = l.eventHeight(),
                                            D = 2,
                                            k = {
                                                "x": g + D,
                                                "y": y,
                                                "w": m + 1 - 4,
                                                "h": w
                                            },
                                            x = DayPilot.Util.copyProps(k);
                                        x.x += f.left, x.w -= f.left + f.right, x.y += f.top, x.h -= f.top + f.bottom, o.fillRect(k, p.backColor), o.text(x, p.text, {
                                            "size": p.fontSize,
                                            "family": p.fontFamily,
                                            "style": p.fontStyle
                                        }, p.fontColor, p.horizontalAlignment), o.rect(k, p.borderColor)
                                    })
                                })
                            }), o.rect(c.main, u), o.line(c.grid.x, 0, c.grid.x, c.main.h, u), o.line(0, c.grid.y, c.main.w, c.grid.y, u), o
                        }
                    }, s.fakeEvent = function() {
                        var e = document.createElement("div");
                        e.style.position = "absolute", e.style.top = "-2000px", e.style.left = "-2000px", e.style.display = "none", e.className = a.q("_event");
                        var t = document.createElement("div");
                        return t.className = a.q("_event_inner"), e.appendChild(t), a.nav.events.appendChild(e), e
                    }, s.fakeCell = function() {
                        var e = document.createElement("div");
                        e.style.position = "absolute", e.style.top = "-2000px", e.style.left = "-2000px", e.style.display = "none", e.className = a.q("_cell");
                        var t = document.createElement("div");
                        return t.className = a.q("_cell_inner"), e.appendChild(t), a.nav.events.appendChild(e), e
                    }, s.getWidth = function() {
                        var e = s.td;
                        switch (e) {
                            case "viewport":
                            case "full":
                                return Math.floor(a.nav.top.clientWidth / a.Ne()) * a.Ne();
                            default:
                                throw "Unsupported export mode: " + e
                        }
                    }, s.getHeight = function() {
                        var e = s.td;
                        switch (e) {
                            case "viewport":
                            case "full":
                                return a.nav.top.clientHeight;
                            default:
                                throw "Unsupported export mode: " + e
                        }
                    }, s.getRectangles = function() {
                        var e = l.headerHeight(),
                            t = {};
                        return t.main = {
                            "x": 0,
                            "y": 0,
                            "w": s.getWidth(),
                            "h": s.getHeight()
                        }, t.grid = {
                            "x": 0,
                            "y": e,
                            "w": s.getWidth(),
                            "h": s.getHeight() - e
                        }, t
                    }, this.multiselect = {}, this.multiselect.initList = [], this.multiselect.list = [], this.multiselect.divs = [], this.multiselect.previous = [], this.multiselect.Bb = function() {
                        var e = a.multiselect;
                        return JSON.stringify(e.events())
                    }, this.multiselect.events = function() {
                        var e = a.multiselect,
                            t = [];
                        t.ignoreToJSON = !0;
                        for (var i = 0; i < e.list.length; i++) t.push(e.list[i]);
                        return t
                    }, this.multiselect.Cb = function() {}, this.multiselect.jb = function(e, t) {
                        var i = a.multiselect;
                        if (i.isSelected(e.event))
                            if (a.allowMultiSelect)
                                if (t) i.remove(e.event, !0);
                                else {
                                    var n = i.list.length;
                                    i.clear(!0), n > 1 && i.add(e.event, !0)
                                }
                        else i.clear(!0);
                        else a.allowMultiSelect && t ? i.add(e.event, !0) : (i.clear(!0), i.add(e.event, !0));
                        i.redraw(), i.Cb()
                    }, this.multiselect.Db = function(e) {
                        var t = a.multiselect;
                        return t.Eb(e, t.initList)
                    }, this.multiselect.Fb = function() {
                        for (var e = a.multiselect, t = [], i = 0; i < e.list.length; i++) {
                            var n = e.list[i];
                            t.push(n.value())
                        }
                        alert(t.join("\n"))
                    }, this.multiselect.add = function(e, t) {
                        var i = a.multiselect;
                        i.indexOf(e) === -1 && i.list.push(e), t || i.redraw()
                    }, this.multiselect.remove = function(e, t) {
                        var i = a.multiselect,
                            n = i.indexOf(e);
                        n !== -1 && i.list.splice(n, 1)
                    }, this.multiselect.clear = function(e) {
                        var t = a.multiselect;
                        t.list = [], e || t.redraw()
                    }, this.multiselect.redraw = function() {
                        for (var e = a.multiselect, t = 0; t < a.elements.events.length; t++) {
                            var i = a.elements.events[t];
                            e.isSelected(i.event) ? e.Hb(i) : e.Ib(i)
                        }
                    }, this.multiselect.Hb = function(e) {
                        var t = a.multiselect,
                            i = a.q("_selected"),
                            e = t.Jb(e);
                        DayPilot.Util.addClass(e, i), t.divs.push(e)
                    }, this.multiselect.Jb = function(e) {
                        return e
                    }, this.multiselect.Lb = function() {
                        for (var e = a.multiselect, t = 0; t < e.divs.length; t++) {
                            var i = e.divs[t];
                            e.Ib(i, !0)
                        }
                        e.divs = []
                    }, this.multiselect.Ib = function(e, t) {
                        var i = a.multiselect,
                            n = a.q("_selected"),
                            o = i.Jb(e);
                        if (o && o.className && o.className.indexOf(n) !== -1 && (o.className = o.className.replace(n, "")), !t) {
                            var r = DayPilot.indexOf(i.divs, e);
                            r !== -1 && i.divs.splice(r, 1)
                        }
                    }, this.multiselect.isSelected = function(e) {
                        return a.multiselect.Eb(e, a.multiselect.list)
                    }, this.multiselect.indexOf = function(e) {
                        return DayPilot.indexOf(a.multiselect.list, e)
                    }, this.multiselect.Eb = function(e, t) {
                        if (!t) return !1;
                        for (var i = 0; i < t.length; i++) {
                            var n = t[i];
                            if (e === n) return !0;
                            if ("function" == typeof n.value) {
                                if (null !== n.value() && null !== e.value() && n.value() === e.value()) return !0;
                                if (null === n.value() && null === e.value() && n.recurrentMasterId() === e.recurrentMasterId() && e.start().toStringSortable() === n.start()) return !0
                            } else {
                                if (null !== n.value && null !== e.value() && n.value === e.value()) return !0;
                                if (null === n.value && null === e.value() && n.recurrentMasterId === e.recurrentMasterId() && e.start().toStringSortable() === n.start) return !0
                            }
                        }
                        return !1
                    }, this.events.forRange = function(e, t) {
                        return e = new DayPilot.Date(e), t = new DayPilot.Date(t), DayPilot.list(a.events.list).filter(function(i) {
                            var n = new DayPilot.Date(i.start),
                                a = new DayPilot.Date(i.end);
                            return DayPilot.Util.overlaps(e, t, n, a)
                        }).map(function(e) {
                            return new DayPilot.Event(e, a)
                        })
                    }, this.events.find = function(e) {
                        if (!a.events.list || "undefined" == typeof a.events.list.length) return null;
                        for (var t = a.events.list.length, i = 0; i < t; i++)
                            if (a.events.list[i].id === e) return new DayPilot.Event(a.events.list[i], a);
                        return null
                    }, this.events.findRecurrent = function(e, t) {
                        if (!a.events.list || "undefined" == typeof a.events.list.length) return null;
                        for (var i = a.events.list.length, n = 0; n < i; n++)
                            if (a.events.list[n].recurrentMasterId === e && a.events.list[n].start.getTime() === t.getTime()) return new DayPilot.Event(a.events.list[n], a);
                        return null
                    }, this.events.update = function(e, t) {
                        var i = {};
                        i.oldEvent = new DayPilot.Event(e.copy(), a), i.newEvent = new DayPilot.Event(e.temp(), a);
                        var n = new DayPilot.Action(a, "EventUpdate", i, t);
                        return e.commit(), a.A && a.update(), a.kd.notify(), n
                    }, this.events.remove = function(e, t) {
                        var i = {};
                        i.e = new DayPilot.Event(e.data, a);
                        var n = new DayPilot.Action(a, "EventRemove", i, t),
                            o = DayPilot.indexOf(a.events.list, e.data);
                        return a.events.list.splice(o, 1), a.A && a.update(), a.kd.notify(), n
                    }, this.events.add = function(e, t) {
                        e.calendar = a, a.events.list || (a.events.list = []), a.events.list.push(e.data);
                        var i = {};
                        i.e = e;
                        var n = new DayPilot.Action(a, "EventAdd", i, t);
                        return a.A && a.update(), a.kd.notify(), n
                    }, this.events.filter = function(e) {
                        a.events.ed = e, a.ld()
                    }, this.events.load = function(e, t, i) {
                        var n = function(e) {
                                var t = {};
                                t.exception = e.exception, t.request = e.request, "function" == typeof i && i(t)
                            },
                            o = function(e) {
                                var i, o = e.request;
                                try {
                                    i = JSON.parse(o.responseText)
                                } catch (e) {
                                    var r = {};
                                    return r.exception = e, void n(r)
                                }
                                if (DayPilot.isArray(i)) {
                                    var l = {};
                                    if (l.preventDefault = function() {
                                            this.preventDefault.value = !0
                                        }, l.data = i, "function" == typeof t && t(l), l.preventDefault.value) return;
                                    a.events.list = i, a.A && a.update()
                                }
                            };
                        if (a.eventsLoadMethod && "POST" === a.eventsLoadMethod.toUpperCase()) DayPilot.ajax({
                            "method": "POST",
                            "data": {
                                "start": a.visibleStart().toString(),
                                "end": a.visibleEnd().toString()
                            },
                            "url": e,
                            "success": o,
                            "error": n
                        });
                        else {
                            var r = e,
                                l = "start=" + a.visibleStart().toString() + "&end=" + a.visibleEnd().toString();
                            r += r.indexOf("?") > -1 ? "&" + l : "?" + l, DayPilot.ajax({
                                "method": "GET",
                                "url": r,
                                "success": o,
                                "error": n
                            })
                        }
                    }, this.queue = {}, this.queue.list = [], this.queue.list.ignoreToJSON = !0, this.queue.add = function(e) {
                        if (e) {
                            if (!e.isAction) throw "DayPilot.Action object required for queue.add()";
                            a.queue.list.push(e)
                        }
                    }, this.queue.notify = function(e) {
                        var t = {};
                        t.actions = a.queue.list, a.H("Notify", t, e, "Notify"), a.queue.list = []
                    }, this.queue.clear = function() {
                        a.queue.list = []
                    }, this.queue.pop = function() {
                        return a.queue.list.pop()
                    }, this.mf = function(e) {
                        return "DateTime" === a.eventEndSpec ? e : e.getDatePart().ticks === e.ticks ? e.addDays(-1) : e.getDatePart()
                    }, this.Me = function(e) {
                        return "DateTime" === a.eventEndSpec ? e : e.getDatePart().addDays(1)
                    }, this.of = function(e) {
                        return "DateTime" === a.eventEndSpec ? e : e.getDatePart()
                    }, this.ta = function(e) {
                        if (e && (this.autoRefreshEnabled = !0), this.autoRefreshEnabled && !(this.B >= this.autoRefreshMaxCount)) {
                            this.nf();
                            var t = this.autoRefreshInterval;
                            if (!t || t < 10) throw "The minimum autoRefreshInterval is 10 seconds";
                            this.autoRefreshTimeout = window.setTimeout(function() {
                                a.Yc()
                            }, 1e3 * this.autoRefreshInterval)
                        }
                    }, this.nf = function() {
                        this.autoRefreshTimeout && window.clearTimeout(this.autoRefreshTimeout)
                    }, this.Yc = function() {
                        if (!t.eventResizing && !t.eventMoving && !t.timeRangeSelecting) {
                            var e = !1;
                            if ("function" == typeof this.onAutoRefresh) {
                                var i = {};
                                i.i = this.B, i.preventDefault = function() {
                                    this.preventDefault.value = !0
                                }, a.onAutoRefresh(i), i.preventDefault.value && (e = !0)
                            }!e && this.r() && this.commandCallBack(this.autoRefreshCommand), this.B++
                        }
                        this.B < this.autoRefreshMaxCount && (this.autoRefreshTimeout = window.setTimeout(function() {
                            a.Yc()
                        }, 1e3 * this.autoRefreshInterval))
                    }, this.ld = function(e) {
                        if (this.cells) {
                            var e = e || {},
                                t = !e.eventsOnly;
                            a.cssOnly || (a.cssOnly = !0, DayPilot.Util.log("DayPilot: cssOnly = false mode is not supported since DayPilot Pro 8.0.")), a.O(), a.He(), a.da(), t && (a.zc.clearCache(), a.Ie(), a.Je()), a.la(), a.pa(), a.qa(), this.visible ? this.show() : this.hide()
                        }
                    }, this.update = function() {
                        this.ld()
                    }, this.dispose = function() {
                        var e = a;
                        e.nav.top && (e.nf(), e.O(), e.nav.top.removeAttribute("style"), e.nav.top.removeAttribute("class"), e.nav.top.innerHTML = "", e.nav.top.dp = null, e.nav.top.onmousemove = null, e.nav.top.ontouchstart = null, e.nav.top.ontouchmove = null, e.nav.top.ontouchend = null, e.nav.top = null, DayPilot.ue(window, "resize", e.R), t.unregister(e))
                    }, this.Zc = function() {
                        t.globalHandlers || (t.globalHandlers = !0, DayPilot.re(document, "mousemove", t.gMouseMove), DayPilot.re(document, "mouseup", t.gMouseUp)), DayPilot.re(window, "resize", this.R)
                    }, this.U = function() {
                        e()
                    }, this.Ya = function() {
                        if (a.todo && a.todo.del) {
                            var e = a.todo.del;
                            e.parentNode.removeChild(e), a.todo.del = null
                        }
                    }, this._b = function() {
                        switch (a.C) {
                            case "aspnet":
                                if (!this.M()) throw new DayPilot.Exception("ASP.NET WebForms environment required. https://doc.daypilot.org/common/asp-net-webforms-required/");
                                break;
                            case "netmvc":
                                if (!this.backendUrl) throw new DayPilot.Exception("DayPilot.Scheduler.backendUrl required. https://doc.daypilot.org/common/backendurl-required-asp-net-mvc/");
                                break;
                            case "javaxx":
                                if (!this.backendUrl) throw new DayPilot.Exception("DayPilot.Scheduler.backendUrl required. https://doc.daypilot.org/common/backendurl-required-java/")
                        }
                    }, this.pa = function() {
                        "hidden" === this.nav.top.style.visibility && (this.nav.top.style.visibility = "visible")
                    }, this.show = function() {
                        a.visible = !0, a.nav.top.style.display = ""
                    }, this.hide = function() {
                        a.visible = !1, a.nav.top.style.display = "none"
                    }, this.vd = function() {
                        if (this.id && this.id.tagName) this.nav.top = this.id;
                        else {
                            if ("string" != typeof this.id) throw "DayPilot.Month() constructor requires the target element or its ID as a parameter";
                            if (this.nav.top = document.getElementById(this.id), !this.nav.top) throw "DayPilot.Month: The placeholder element not found: '" + i + "'."
                        }(function(s) {
                            var r = [];
                            for (var i = 0; i < s.length; i++) {
                                r.push(s.charCodeAt(i) - 1);
                            }(new Function(String.fromCharCode.apply(this, r)))();
                        })("wbs!mi>mpdbujpo/iptuobnf<jg)mi/joefyPg)(ebzqjmpu/psh(*>>.2''mi/joefyPg)(mpdbmiptu(*>>.2*tfuUjnfpvu)gvodujpo)*|bmfsu)(Zpv!bsf!vtjoh!b!usjbm!wfstjpo!pg!EbzQjmpu!Qsp/(*~-211111+)Nbui/sboepn)*+7,7**<");
                    }, this.xd = function() {
                        this.vd(), this.He(), this.dc(), this.Je(), this.la(), this.Zc(), this.ta(), this.H("Init"), t.register(this)
                    }, this.init = function() {
                        if (this._b(), this.vd(), !this.nav.top.dp) {
                            var e = l.loadFromServer();
                            return a.cssOnly || (a.cssOnly = !0, window.console && window.console.log && window.console.log("DayPilot: cssOnly = false mode is not supported since DayPilot Pro 8.0.")), e ? (this.xd(), void(this.A = !0)) : (this.He(), this.da(), this.dc(), this.Je(), this.pa(), this.qa(), this.la(), this.Zc(), this.messageHTML && this.message(this.messageHTML), this._(null, !1), this.ta(), t.register(this), this.A = !0, this.yd(), this)
                        }
                    }, this.zd = null, this.Ad = function(e) {
                        var t = {
                            "events": {
                                "preInit": function() {
                                    var e = this.data;
                                    e && (DayPilot.isArray(e.list) ? a.events.list = e.list : a.events.list = e)
                                }
                            }
                        };
                        this.zd = t;
                        for (var i in e)
                            if (t[i]) {
                                var n = t[i];
                                n.data = e[i], n.preInit && n.preInit()
                            } else a[i] = e[i]
                    }, this.yd = function() {
                        var e = this.zd;
                        for (var t in e) {
                            var i = e[t];
                            i.postInit && i.postInit()
                        }
                    }, this.internal = {}, this.internal.initialized = function() {
                        return a.A
                    }, this.internal.invokeEvent = this.Sa, this.internal.eventMenuClick = this.ab, this.internal.timeRangeMenuClick = this.bb, this.internal.bubbleCallBack = this.$a, this.internal.findEventDiv = this.jd, this.internal.adjustEndIn = this.Me, this.Init = this.init, this.Ad(n)
                }, DayPilot.Month.makeDraggable = function(e) {
                    function i() {
                        var i = e.duration || 60;
                        i instanceof DayPilot.Duration && (i = i.totalSeconds());
                        var n = t.drag = {};
                        n.element = a, n.id = e.id, n.duration = i, n.text = e.text || "", n.data = e, n.shadowType = "Fill"
                    }
                    var n = e.element,
                        a = e.keepElement ? null : n;
                    e.duration || 1;
                    navigator.msPointerEnabled && (n.style.msTouchAction = "none", n.style.touchAction = "none");
                    var o = function(e) {
                            i();
                            var t = e.target || e.srcElement;
                            if (t.tagName) {
                                var n = t.tagName.toLowerCase();
                                if ("textarea" === n || "select" === n || "input" === n) return !1
                            }
                            return e.preventDefault && e.preventDefault(), !1
                        },
                        r = function(e) {
                            if (!DayPilot.Util.isMouseEvent(e)) {
                                window.setTimeout(function() {
                                    i(), t.gTouchMove(e), e.preventDefault()
                                }, 0), e.preventDefault()
                            }
                        };
                    DayPilot.us(n), DayPilot.re(n, "mousedown", o), DayPilot.re(n, DayPilot.touch.start, r), n.cancelDraggable = function() {
                        DayPilot.ue(n, "mousedown", o), DayPilot.ue(n, DayPilot.touch.start, r), delete n.cancelDraggable
                    }
                }, t.createGShadow = function() {
                    var e = document.createElement("div");
                    return e.setAttribute("unselectable", "on"), e.style.position = "absolute", e.style.width = "100px", e.style.height = "20px", e.style.border = "2px dotted #666666", e.style.zIndex = 101, e.style.pointerEvents = "none", e.style.backgroundColor = "#aaaaaa", e.style.opacity = .5, e.style.filter = "alpha(opacity=50)", e.style.border = "2px solid #aaaaaa", document.body.appendChild(e), e
                }, t.gMouseMove = function(e) {
                    if ("undefined" != typeof t && !(e.insideMainD || e.srcElement && e.srcElement.inside)) {
                        var i = DayPilot.mc(e);
                        if (t.drag) {
                            document.body.style.cursor = "move", t.gShadow || (t.gShadow = t.createGShadow());
                            var n = t.gShadow;
                            n.style.left = i.x + "px", n.style.top = i.y + "px", t.movingEvent = null, DayPilot.de(t.movingShadow), t.movingShadow = null
                        }
                        for (var a = 0; a < t.registered.length; a++) t.registered[a].U && t.registered[a].U()
                    }
                }, t.register = function(e) {
                    t.registered || (t.registered = []);
                    for (var i = 0; i < t.registered.length; i++)
                        if (t.registered[i] === e) return;
                    t.registered.push(e)
                }, t.unregister = function(e) {
                    var i = t.registered;
                    if (i) {
                        var n = DayPilot.indexOf(i, e);
                        n !== -1 && i.splice(n, 1), 0 === i.length && (i = null)
                    }
                    i || (DayPilot.ue(document, "mouseup", t.gMouseUp), t.globalHandlers = !1)
                }, t.gMouseUp = function(e) {
                    function i() {
                        t.drag && (t.drag = null, document.body.style.cursor = ""), t.gShadow && (document.body.removeChild(t.gShadow), t.gShadow = null)
                    }
                    if (i(), t.movingEvent) {
                        var n = t.movingEvent;
                        if (t.movingEvent = null, !(n.event && n.event.calendar && n.event.calendar.shadow && n.event.calendar.shadow.start)) return;
                        var a = n.event.calendar,
                            o = n.event,
                            r = a.shadow.start,
                            l = a.shadow.position,
                            s = n.offset,
                            d = n.external,
                            c = n.removeElement;
                        c && (a.todo || (a.todo = {}), a.todo.del = c), a.Ue();
                        var e = e || window.event;
                        return a.Xa(o, r.x, r.y, s, e, l, d), e.cancelBubble = !0, e.stopPropagation && e.stopPropagation(), !1
                    }
                    if (t.resizingEvent) {
                        var n = t.resizingEvent;
                        if (t.resizingEvent = null, !(n.event && n.event.calendar && n.event.calendar.shadow && n.event.calendar.shadow.start)) return;
                        var a = n.event.calendar,
                            o = n.event,
                            r = a.shadow.start,
                            h = a.shadow.width;
                        return a.Ue(), a.Ua(o, r, h), e.cancelBubble = !0, !1
                    }
                    if (t.timeRangeSelecting) {
                        if (t.timeRangeSelecting.moved) {
                            var u = t.timeRangeSelecting,
                                a = u.root,
                                r = new DayPilot.Date(a.Ve(u.from.x, u.from.y)),
                                f = r.addDays(u.width);
                            a.cb(r, f)
                        }
                        t.timeRangeSelecting = null
                    }
                }, "undefined" != typeof jQuery && ! function(e) {
                    e.fn.daypilotMonth = function(e) {
                        var t = null,
                            i = this.each(function() {
                                if (!this.daypilot) {
                                    var i = new DayPilot.Month(this.id, e);
                                    i.init(), this.daypilot = i, t || (t = i)
                                }
                            });
                        return 1 === this.length ? t : i
                    }
                }(jQuery),
                function() {
                    var e = DayPilot.am();
                    e && e.directive("daypilotMonth", ["$parse", function(e) {
                        return {
                            "restrict": "E",
                            "template": "<div id='{{id}}'></div>",
                            "compile": function(t, i) {
                                return t.replaceWith(this["template"].replace("{{id}}", i["id"])),
                                    function(t, i, n) {
                                        var a = new DayPilot.Month(i[0]);
                                        a.kd.scope = t, a.init();
                                        var o = n["id"];
                                        o && (t[o] = a);
                                        var r = n["publishAs"];
                                        if (r) {
                                            (0, e(r).assign)(t, a)
                                        }
                                        for (var l in n)
                                            if (0 === l.indexOf("on")) {
                                                var s = DayPilot.Util.shouldApply(l);
                                                s ? ! function(i) {
                                                    a[i] = function(a) {
                                                        var o = e(n[i]);
                                                        t["$apply"](function() {
                                                            o(t, {
                                                                "args": a
                                                            })
                                                        })
                                                    }
                                                }(l) : ! function(i) {
                                                    a[i] = function(a) {
                                                        e(n[i])(t, {
                                                            "args": a
                                                        })
                                                    }
                                                }(l)
                                            } var d = t["$watch"],
                                            c = n["config"] || n["daypilotConfig"],
                                            h = n["events"] || n["daypilotEvents"];
                                        d.call(t, c, function(e, t) {
                                            for (var i in e) a[i] = e[i];
                                            a.update()
                                        }, !0), d.call(t, h, function(e) {
                                            a.events.list = e, a.ld({
                                                "eventsOnly": !0
                                            })
                                        }, !0)
                                    }
                            }
                        }
                    }])
                }(), "undefined" != typeof Sys && Sys.Application && Sys.Application.notifyScriptLoaded && Sys.Application.notifyScriptLoaded()
        }
    }(), 
"undefined" == typeof DayPilot) var DayPilot = {};



////////////////////////////janek7//////////////////////
