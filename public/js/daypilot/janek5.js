



if ("undefined" == typeof DayPilot.Global && (DayPilot.Global = {}), function() {
        if ("undefined" == typeof DayPilot.Menu) {
            var e = function() {},
                t = {};
            t.mouse = null, t.menu = null, t.handlersRegistered = !1, t.hideTimeout = null, t.waitingSubmenu = null, DayPilot.Menu = function(i) {
                var n = this,
                    a = null;
                this.v = "2018.1.3151", this.zIndex = 10, this.cssClassPrefix = "menu_default", this.cssOnly = !0, this.menuTitle = null, this.showMenuTitle = !1, this.hideOnMouseOut = !1, this.theme = null, this.onShow = null, this.n = function() {}, i && DayPilot.isArray(i) && (this.items = i), this.show = function(i, o) {
                    var o = o || {},
                        r = null;
                    if (i ? "string" == typeof i.id || "number" == typeof i.id ? r = i.id : "function" == typeof i.id ? r = i.id() : "function" == typeof i.value && (r = i.value()) : r = null, "undefined" != typeof DayPilot.Bubble && DayPilot.Bubble.hideActive(), o.submenu || t.menuClean(), this.n.submenu = null, null !== t.mouse) {
                        n.cssOnly || (n.cssOnly = !0, DayPilot.Util.log("DayPilot: cssOnly = false mode is not supported since DayPilot Pro 8.0."));
                        var l = null;
                        if (i && i.isRow && i.$.row.task ? (l = new DayPilot.Task(i.$.row.task, i.calendar), l.menuType = "resource") : l = i && i.isEvent && i.data.task ? new DayPilot.Task(i, i.calendar) : i, "function" == typeof n.onShow) {
                            var s = {};
                            if (s.source = l, s.menu = n, s.preventDefault = function() {
                                    s.preventDefault.value = !0
                                }, n.onShow(s), s.preventDefault.value) return
                        }
                        var d = document.createElement("div");
                        if (d.style.position = "absolute", d.style.top = "0px", d.style.left = "0px", d.style.display = "none", d.style.overflow = "hidden", d.style.zIndex = this.zIndex + 1, d.className = this.applyCssClass("main"), d.onclick = function(e) {
                                e.cancelBubble = !0, this.parentNode.removeChild(this)
                            }, this.hideOnMouseOut && (d.onmousemove = function(e) {
                                clearTimeout(t.hideTimeout)
                            }, d.onmouseout = function(e) {
                                n.delayedHide()
                            }), !this.items || 0 === this.items.length) throw "No menu items defined.";
                        if (this.showMenuTitle) {
                            var c = document.createElement("div");
                            c.innerHTML = this.menuTitle, c.className = this.applyCssClass("title"), d.appendChild(c)
                        }
                        for (var h = 0; h < this.items.length; h++) {
                            var u = this.items[h],
                                f = document.createElement("div");
                            if (DayPilot.Util.addClass(f, this.applyCssClass("item")), u.items && DayPilot.Util.addClass(f, this.applyCssClass("item_haschildren")), "undefined" != typeof u && !u.hidden) {
                                if ("-" === u.text) {
                                    var v = document.createElement("div");
                                    f.appendChild(v)
                                } else {
                                    var p = document.createElement("a");
                                    if (p.style.position = "relative", p.style.display = "block", u.cssClass && DayPilot.Util.addClass(p, u.cssClass), u.disabled) DayPilot.Util.addClass(p, n.applyCssClass("item_disabled"));
                                    else {
                                        if (u.onclick || u.onClick) {
                                            p.item = u, p.onclick = function(e, t) {
                                                return function(i) {
                                                    if ("function" == typeof e.onClick) {
                                                        var n = {};
                                                        if (n.item = e, n.source = t.source, n.originalEvent = i, n.preventDefault = function() {
                                                                n.preventDefault.value = !0
                                                            }, e.onClick(n), n.preventDefault.value) return
                                                    }
                                                    e.onclick && e.onclick.call(t, i)
                                                }
                                            }(u, p);
                                            var g = function(e, i) {
                                                return function(n) {
                                                    n.stopPropagation(), n.preventDefault();
                                                    var a = function() {
                                                        window.setTimeout(function() {
                                                            i.source.calendar.internal.touch.active = !1
                                                        }, 500)
                                                    };
                                                    if ("function" == typeof e.onClick) {
                                                        var o = {};
                                                        if (o.item = e, o.originalEvent = n, o.preventDefault = function() {
                                                                o.preventDefault.value = !0
                                                            }, e.onClick(o), o.preventDefault.value) return void a()
                                                    }
                                                    e.onclick && e.onclick.call(i, n), t.menuClean(), a()
                                                }
                                            };
                                            p.ontouchstart = function(e) {
                                                e.stopPropagation(), e.preventDefault(), p.source.calendar.internal.touch.active = !0
                                            }, p.ontouchend = g(u, p)
                                        }
                                        if (u.onclick) e();
                                        else if (u.href) p.href = u.href.replace(/\x7B0\x7D/gim, r), u.target && p.setAttribute("target", u.target);
                                        else if (u.command) {
                                            var m = function(e, t) {
                                                return function(i) {
                                                    var n = t.source,
                                                        a = e;
                                                    a.action = a.action ? a.action : "CallBack";
                                                    var o = n.calendar || n.root;
                                                    if (n instanceof DayPilot.Link) return void o.internal.linkMenuClick(a.command, n, a.action);
                                                    if (n instanceof DayPilot.Selection) return void o.internal.timeRangeMenuClick(a.command, n, a.action);
                                                    if (n instanceof DayPilot.Event) return void o.internal.eventMenuClick(a.command, n, a.action);
                                                    if (n instanceof DayPilot.Selection) return void o.internal.timeRangeMenuClick(a.command, n, a.action);
                                                    if (n instanceof DayPilot.Task) return void("resource" === n.menuType ? o.internal.resourceHeaderMenuClick(a.command, t.menuSource, a.action) : o.internal.eventMenuClick(a.command, t.menuSource, a.action));
                                                    switch (n.menuType) {
                                                        case "resource":
                                                            return void o.internal.resourceHeaderMenuClick(a.command, n, a.action);
                                                        case "selection":
                                                            return void o.internal.timeRangeMenuClick(a.command, n, a.action);
                                                        default:
                                                            return void o.internal.eventMenuClick(a.command, n, a.action)
                                                    }
                                                    i.preventDefault()
                                                }
                                            };
                                            p.onclick = m(u, p), p.ontouchend = m(u, p)
                                        }
                                    }
                                    p.source = l, p.menuSource = i;
                                    var y = document.createElement("span");
                                    if (y.className = n.applyCssClass("item_text"), y.innerHTML = u.text, p.appendChild(y), u.image) {
                                        var b = document.createElement("img");
                                        b.src = u.image, b.style.position = "absolute", b.style.top = "0px", b.style.left = "0px", p.appendChild(b)
                                    }
                                    if (u.icon) {
                                        var w = document.createElement("span");
                                        w.className = n.applyCssClass("item_icon");
                                        var D = document.createElement("i");
                                        D.className = u.icon, w.appendChild(D), p.appendChild(w)
                                    }
                                    var k = function(e, i) {
                                        return function() {
                                            var a = i.source,
                                                o = e,
                                                r = t.waitingSubmenu;
                                            if (r) {
                                                if (r.parent === o) return;
                                                clearTimeout(r.timeout), t.waitingSubmenu = null
                                            }
                                            t.waitingSubmenu = {}, t.waitingSubmenu.parent = o, t.waitingSubmenu.timeout = setTimeout(function() {
                                                if (t.waitingSubmenu = null, (!n.n.submenu || n.n.submenu.item !== o) && (n.n.submenu && n.n.submenu.item !== o && (DayPilot.Util.removeClass(n.n.submenu.link.parentNode, n.applyCssClass("item_haschildren_active")), n.n.submenu.menu.hide(), n.n.submenu = null), o.items)) {
                                                    var r = n.cloneOptions();
                                                    r.items = o.items, n.n.submenu = {}, n.n.submenu.menu = new DayPilot.Menu(r), n.n.submenu.menu.show(a, {
                                                        "submenu": !0,
                                                        "parentLink": i,
                                                        "parentItem": e
                                                    }), n.n.submenu.item = o, n.n.submenu.link = i, DayPilot.Util.addClass(i.parentNode, n.applyCssClass("item_haschildren_active"))
                                                }
                                            }, 300)
                                        }
                                    };
                                    p.onmouseover = k(u, p), f.appendChild(p)
                                }
                                d.appendChild(f)
                            }
                        }
                        var x = function(e) {
                            window.setTimeout(function() {
                                t.menuClean(), DayPilot.MenuBar.deactivate()
                            }, 100)
                        };
                        d.onclick = x, d.ontouchend = x, d.onmousedown = function(e) {
                            e = e || window.event, e.cancelBubble = !0, e.stopPropagation && e.stopPropagation()
                        }, d.oncontextmenu = function() {
                            return !1
                        }, document.body.appendChild(d), n.n.visible = !0, n.n.source = i, d.style.display = "";
                        var P = d.offsetHeight,
                            C = d.offsetWidth;
                        d.style.display = "none";
                        var S = document.documentElement.clientHeight,
                            A = document.documentElement.clientWidth,
                            T = "number" == typeof o.windowMargin ? o.windowMargin : 5;
                        if (function() {
                                var e = o.initiator;
                                if (e) {
                                    var t = e.div,
                                        i = e.e,
                                        n = e.area,
                                        r = DayPilot.Areas.createArea(t, i, n);
                                    t.appendChild(r), a = r;
                                    var l = DayPilot.abs(r);
                                    o.x = l.x, o.y = l.y + l.h + 2
                                }
                            }(), function() {
                                var e = "number" == typeof o.x ? o.x : t.mouse.x + 1,
                                    i = "number" == typeof o.y ? o.y : t.mouse.y + 1,
                                    n = document.body.scrollTop || document.documentElement.scrollTop,
                                    a = document.body.scrollLeft || document.documentElement.scrollLeft;
                                if (i - n > S - P && 0 !== S) {
                                    var r = i - n - (S - P) + T;
                                    d.style.top = i - r + "px"
                                } else d.style.top = i + "px";
                                if ("right" === o.align && (e -= C), e - a > A - C && 0 !== A) {
                                    var l = e - a - (A - C) + T;
                                    d.style.left = e - l + "px"
                                } else d.style.left = e + "px"
                            }(), o.parentLink) {
                            var E = o.parentLink,
                                M = parseInt(new DayPilot.StyleReader(d).get("border-top-width")),
                                H = DayPilot.abs(o.parentLink.parentNode),
                                _ = H.x + E.offsetWidth,
                                R = H.y - M;
                            _ + C > A && (_ = Math.max(0, H.x - C));
                            var B = document.body.scrollTop + document.documentElement.scrollTop;
                            R + P - B > S && (R = Math.max(0, S - P + B)), d.style.left = _ + "px", d.style.top = R + "px"
                        }
                        d.style.display = "", this.addShadow(d), this.n.div = d, o.submenu || (DayPilot.Menu.active = this)
                    }
                }, this.applyCssClass = function(e) {
                    var t = this.theme || this.cssClassPrefix,
                        i = this.cssOnly ? "_" : "";
                    return t ? t + i + e : ""
                }, this.cloneOptions = function() {
                    for (var e = {}, t = ["cssOnly", "cssClassPrefix", "useShadow", "zIndex"], i = 0; i < t.length; i++) {
                        var n = t[i];
                        e[n] = this[n]
                    }
                    return e
                }, this.hide = function() {
                    this.n.submenu && this.n.submenu.menu.hide(), this.removeShadow(), this.n.div && this.n.div.parentNode === document.body && document.body.removeChild(this.n.div), a && (DayPilot.de(a), a = null), n.n.visible = !1, n.n.source = null
                }, this.delayedHide = function() {
                    t.hideTimeout = setTimeout(function() {
                        n.hide()
                    }, 200)
                }, this.cancelHideTimeout = function() {
                    clearTimeout(t.hideTimeout)
                }, this.init = function(e) {
                    return t.mouseMove(e), this
                }, this.addShadow = function(e) {}, this.removeShadow = function() {
                    if (this.n.shadows) {
                        for (var e = 0; e < this.n.shadows.length; e++) document.body.removeChild(this.n.shadows[e]);
                        this.n.shadows = []
                    }
                };
                var o = DayPilot.isArray(i) ? null : i;
                if (o)
                    for (var r in o) this[r] = o[r]
            }, DayPilot.MenuBar = function(e, t) {
                var i = this,
                    t = t || {};
                this.items = [], this.theme = "menubar_default", this.windowMargin = 0, this.nav = {}, this.elements = {}, this.elements.items = DayPilot.list(), this.$d = null;
                for (var n in t) this[n] = t[n];
                this._d = function(e) {
                    return this.theme + "_" + e
                }, this.pa = function() {
                    this.nav.top = document.getElementById(e);
                    var t = this.nav.top;
                    t.className = this._d("main"), DayPilot.list(i.items).each(function(e) {
                        var n = document.createElement("span");
                        n.innerHTML = e.text, n.className = i._d("item"), n.data = e, n.onclick = function() {
                            return i.active && i.active.item === e ? void i.ae() : void(e.children && i.be(n))
                        }, n.onmousedown = function(e) {
                            e.stopPropagation()
                        }, n.onmouseover = function() {
                            i.active && i.active.item !== e && i.be(n)
                        }, t.appendChild(n), i.elements.items.push(n)
                    })
                }, this.ae = function() {
                    var e = i._d("item_active");
                    i.elements.items.each(function(t) {
                        DayPilot.Util.removeClass(t, e)
                    }), i.active && i.active.menu && i.active.menu.hide(), i.active = null
                }, this.ce = function(e) {
                    return !!i.active && i.active.item === e.data
                }, this.be = function(e) {
                    if (!i.ce(e)) {
                        i.ae();
                        var t = e.data,
                            n = i.active = {};
                        n.item = t, n.div = e;
                        var a = i._d("item_active");
                        DayPilot.Util.addClass(e, a);
                        var o = DayPilot.abs(e);
                        t.children && (n.menu = new DayPilot.Menu({
                            "items": t.children
                        }), n.menu.show(null, {
                            "x": o.x + o.w,
                            "y": o.y + o.h,
                            "align": t.align,
                            "windowMargin": i.windowMargin
                        })), DayPilot.MenuBar.active = i
                    }
                }, this.init = function() {
                    return this.pa(), this
                }
            }, DayPilot.MenuBar.deactivate = function() {
                DayPilot.MenuBar.active && (DayPilot.MenuBar.active.ae(), DayPilot.MenuBar.active = null)
            }, t.menuClean = function() {
                "undefined" != typeof DayPilot.Menu.active && DayPilot.Menu.active && (DayPilot.Menu.active.hide(), DayPilot.Menu.active = null)
            }, t.mouseDown = function(e) {
                "undefined" != typeof t && (t.menuClean(), DayPilot.MenuBar.deactivate())
            }, t.mouseMove = function(e) {
                "undefined" != typeof t && (t.mouse = t.mousePosition(e))
            }, t.touchMove = function(e) {
                "undefined" != typeof t && (t.mouse = t.touchPosition(e))
            }, t.touchStart = function(e) {
                "undefined" != typeof t && (t.mouse = t.touchPosition(e))
            }, t.touchEnd = function(e) {}, t.touchPosition = function(e) {
                if (!e || !e.touches) return null;
                var t = e.touches[0],
                    i = {};
                return i.x = t.pageX, i.y = t.pageY, i
            }, t.mousePosition = function(e) {
                return DayPilot.mo3(document.body, e)
            }, DayPilot.Menu.touchPosition = function(e) {
                e.touches && (t.mouse = t.touchPosition(e))
            }, t.handlersRegistered || (DayPilot.re(document, "mousemove", t.mouseMove), DayPilot.re(document, "mousedown", t.mouseDown), DayPilot.re(document, "touchmove", t.touchMove), DayPilot.re(document, "touchstart", t.touchStart), DayPilot.re(document, "touchend", t.touchEnd), t.handlersRegistered = !0), "undefined" != typeof Sys && Sys.Application && Sys.Application.notifyScriptLoaded && Sys.Application.notifyScriptLoaded()
        }
    }



    
//"undefined" == typeof DayPilot && (DayPilot = {}), 




function() {



        DayPilot.ModalStatic = {}, DayPilot.ModalStatic.list = [], DayPilot.ModalStatic.hide = function() {
            if (this.list.length > 0) {
                var e = this.list.pop();
                e && e.hide()
            }
        }, 
        DayPilot.ModalStatic.remove = function(e) {
            for (var t = DayPilot.ModalStatic.list, i = 0; i < t.length; i++)
                if (t[i] === e) return void t.splice(i, 1)
        }, 
        DayPilot.ModalStatic.close = function(e) {
            DayPilot.ModalStatic.result(e), DayPilot.ModalStatic.hide()
        }, 
        DayPilot.ModalStatic.result = function(e) {
            var t = DayPilot.ModalStatic.list;
            t.length > 0 && (t[t.length - 1].result = e)
        }, 
        DayPilot.ModalStatic.displayed = function(e) {
            for (var t = DayPilot.ModalStatic.list, i = 0; i < t.length; i++)
                if (t[i] === e) return !0;
            return !1
        }, 
        DayPilot.ModalStatic.stretch = function() {
            if (this.list.length > 0) {
                var e = this.list[this.list.length - 1];
                e && e.stretch()
            }
        }, 
        DayPilot.ModalStatic.last = function() {
            var e = DayPilot.ModalStatic.list;
            return e.length > 0 ? e[e.length - 1] : null
        };
        var e = function() {
                var e = document.createElement("style");
                e.setAttribute("type", "text/css"), e.styleSheet || e.appendChild(document.createTextNode("")), (document.head || document.getElementsByTagName("head")[0]).appendChild(e);
                var t = !!e.styleSheet,
                    i = {};
                return i.rules = [], i.commit = function() {
                    try {
                        t && (e.styleSheet.cssText = this.rules.join("\n"))
                    } catch (e) {}
                }, i.add = function(i, n, a) {
                    if (t) return void this.rules.push(i + "{" + n + "}");
                    if (e.sheet.insertRule) "undefined" == typeof a && (a = e.sheet.cssRules.length), e.sheet.insertRule(i + "{" + n + "}", a);
                    else {
                        if (!e.sheet.addRule) throw "No CSS registration method found";
                        e.sheet.addRule(i, n, a)
                    }
                }, i
            },
       
        t = new e;
        t.add(".modal_default_main", "border: 10px solid #ccc;"), t.add(".modal_default_inner", "padding: 10px;"), t.add(".modal_default_input", "padding: 10px 0px;"), t.add(".modal_default_buttons", "padding: 10px 0px;"), t.add(".modal_default_background", "opacity: 0.3; background-color: #000;"), t.add(".modal_min_main", "border: 1px solid #ccc;"), t.add(".modal_min_background", "opacity: 0.3; background-color: #000;"), t.commit(), DayPilot.Modal = function(e) {
            this.autoStretch = !0, this.autoStretchFirstLoadOnly = !1, this.className = null, this.theme = "modal_default", this.disposeOnClose = !0, this.dragDrop = !0, this.loadingHtml = null, this.maxHeight = null, this.scrollWithPage = !0, this.useIframe = !0, this.zIndex = 99999, this.left = null, this.width = 500, this.top = 20, this.height = 200, this.closed = null, this.onClosed = null, this.onShow = null;
            var t = this;
            this.id = "_" + (new Date).getTime() + "n" + 10 * Math.random(), this.ee = !1, this.fe = null, this.ge = null, this.showHtml = function(e) {
                if (DayPilot.ModalStatic.displayed(this)) throw "This modal dialog is already displayed.";
                if (this.div || this.je(), this.ld(), this.useIframe) {
                    var t = function(e, t) {
                        return function() {
                            e.setInnerHTML(e.id + "iframe", t)
                        }
                    };
                    window.setTimeout(t(this, e), 0)
                } else e.nodeType ? this.div.appendChild(e) : this.div.innerHTML = e;
                this.ld(), this.ke(), this.le()
            }, this.me = function() {
                return this.corners && "rounded" === this.corners.toLowerCase()
            }, this.showUrl = function(e) {
                if (DayPilot.ModalStatic.displayed(this)) throw "This modal dialog is already displayed.";
                if (this.useIframe) {
                    this.div || this.je();
                    var i = this.loadingHtml;
                    i && (this.iframe.src = "about:blank", this.setInnerHTML(this.id + "iframe", i)), this.re(this.iframe, "load", this.ne), this.iframe.src = e, this.ld(), this.ke(), this.le()
                } else t.oe({
                    "url": e,
                    "success": function(e) {
                        var i = e.request.responseText;
                        t.showHtml(i)
                    },
                    "error": function(e) {
                        t.showHtml("Error loading the modal dialog")
                    }
                })
            }, this.le = function() {
                if ("function" == typeof t.onShow) {
                    var e = {};
                    e.root = t.pe(), t.onShow(e)
                }
            }, this.pe = function() {
                return t.iframe ? t.iframe.contentWindow.document : t.div
            }, this.oe = function(e) {
                var t = new XMLHttpRequest;
                if (t) {
                    var i = e.method || "GET",
                        n = e.success || function() {},
                        a = e.error || function() {},
                        o = e.data,
                        r = e.url;
                    t.open(i, r, !0), t.setRequestHeader("Content-type", "text/plain"), t.onreadystatechange = function() {
                        if (4 === t.readyState)
                            if (200 === t.status || 304 === t.status) {
                                var e = {};
                                e.request = t, n(e)
                            } else if (a) {
                            var e = {};
                            e.request = t, a(e)
                        } else window.console && console.log("HTTP error " + t.status)
                    }, 4 !== t.readyState && ("object" == typeof o && (o = JSON.stringify(o)), t.send(o))
                }
            }, this.ld = function() {
                var e = window,
                    i = document,
                    n = e.pageYOffset ? e.pageYOffset : i.documentElement && i.documentElement.scrollTop ? i.documentElement.scrollTop : i.body.scrollTop;
                this.theme && (this.hideDiv.className = this.theme + "_background"), this.zIndex && (this.hideDiv.style.zIndex = this.zIndex), this.hideDiv.style.display = "", window.setTimeout(function() {
                    t.hideDiv.onclick = function() {
                        t.hide({
                            "backgroundClick": !0
                        })
                    }
                }, 500), this.theme ? this.div.className = this.theme + "_main" : this.div.className = "", this.className && (this.div.className += " " + this.className), this.left ? this.div.style.left = this.left + "px" : this.div.style.marginLeft = "-" + Math.floor(this.width / 2) + "px", this.div.style.position = "absolute", this.div.style.boxSizing = "content-box", this.div.style.top = n + this.top + "px", this.div.style.width = this.width + "px", this.zIndex && (this.div.style.zIndex = this.zIndex), this.height && (this.useIframe || !this.autoStretch ? this.div.style.height = this.height + "px" : this.div.style.height = ""), this.useIframe && this.height && (this.iframe.style.height = this.height + "px"), this.div.style.display = "", DayPilot.ModalStatic.list.push(this)
            }, this.ne = function() {
                t.iframe.contentWindow.modal = t, t.autoStretch && t.stretch()
            }, this.stretch = function() {
                var e = function() {
                        return t.qe().y
                    },
                    i = function() {
                        return t.qe().x
                    };
                if (this.useIframe) {
                    for (var n = i() - 40, a = this.width; a < n && this.se(); a += 10) this.div.style.width = a + "px", this.div.style.marginLeft = "-" + Math.floor(a / 2) + "px";
                    for (var o = this.maxHeight || e() - 2 * this.top, r = this.height; r < o && this.te(); r += 10) this.iframe.style.height = r + "px", this.div.style.height = r + "px";
                    this.autoStretchFirstLoadOnly && this.ue(this.iframe, "load", this.ne)
                } else this.div.style.height = ""
            }, this.se = function() {
                for (var e = this.iframe.contentWindow.document, t = "BackCompat" === e.compatMode ? e.body : e.documentElement, i = t.scrollWidth, n = e.body.children, a = 0; a < n.length; a++) {
                    var o = n[a].offsetLeft + n[a].offsetWidth;
                    i = Math.max(i, o)
                }
                return i > t.clientWidth
            }, this.te = function() {
                for (var e = this.iframe.contentWindow.document, t = "BackCompat" === e.compatMode ? e.body : e.documentElement, i = t.scrollHeight, n = e.body.children, a = 0; a < n.length; a++) {
                    var o = n[a].offsetTop + n[a].offsetHeight;
                    i = Math.max(i, o)
                }
                return i > t.clientHeight
            }, this.qe = function() {
                var e = document;
                if ("CSS1Compat" === e.compatMode && e.documentElement && e.documentElement.clientWidth) {
                    var t = e.documentElement.clientWidth,
                        i = e.documentElement.clientHeight;
                    return {
                        x: t,
                        y: i
                    }
                }
                var t = e.body.clientWidth,
                    i = e.body.clientHeight;
                return {
                    x: t,
                    y: i
                }
            }, this.ke = function() {
                this.ee || (this.re(window, "resize", this.ve), this.re(window, "scroll", this.we), this.dragDrop && (this.re(document, "mousemove", this.xe), this.re(document, "mouseup", this.ye)), this.ee = !0)
            }, this.ze = function(e) {
                e.target === t.div && (t.div.style.cursor = "move", t.Ae(), t.ge = t.mc(e || window.event), t.fe = {
                    x: t.div.offsetLeft,
                    y: t.div.offsetTop
                })
            }, this.xe = function(e) {
                if (t.ge) {
                    var e = e || window.event,
                        i = t.mc(e),
                        n = i.x - t.ge.x,
                        a = i.y - t.ge.y;
                    t.div.style.marginLeft = "0px", t.div.style.top = t.fe.y + a + "px", t.div.style.left = t.fe.x + n + "px"
                }
            }, this.ye = function(e) {
                t.ge && (t.Be(), t.div.style.cursor = null, t.ge = null)
            }, this.Ae = function() {
                if (this.useIframe) {
                    var e = document.createElement("div");
                    e.style.backgroundColor = "#ffffff", e.style.filter = "alpha(opacity=80)", e.style.opacity = "0.80", e.style.width = "100%", e.style.height = this.height + "px", e.style.position = "absolute", e.style.left = "0px", e.style.top = "0px", this.div.appendChild(e), this.mask = e
                }
            }, this.Be = function() {
                this.useIframe && (this.div.removeChild(this.mask), this.mask = null)
            }, this.ve = function() {
                t.Ce
            }, this.we = function() {
                t.Ce
            }, this.Ce = function() {
                if (t.hideDiv && t.div && "none" !== t.hideDiv.style.display && "none" !== t.div.style.display) {
                    var e = window.pageYOffset ? window.pageYOffset : document.documentElement && document.documentElement.scrollTop ? document.documentElement.scrollTop : document.body.scrollTop;
                    t.scrollWithPage || (t.div.style.top = e + t.top + "px")
                }
            }, this.re = function(e, t, i) {
                e.addEventListener ? e.addEventListener(t, i, !1) : e.attachEvent && e.attachEvent("on" + t, i)
            }, this.ue = function(e, t, i) {
                e.removeEventListener ? e.removeEventListener(t, i, !1) : e.detachEvent && e.detachEvent("on" + t, i)
            }, this.mc = function(e) {
                return e.pageX || e.pageY ? {
                    x: e.pageX,
                    y: e.pageY
                } : {
                    x: e.clientX + document.documentElement.scrollLeft,
                    y: e.clientY + document.documentElement.scrollTop
                }
            }, this.abs = function(e) {
                for (var t = {
                        x: e.offsetLeft,
                        y: e.offsetTop
                    }; e.offsetParent;) e = e.offsetParent, t.x += e.offsetLeft, t.y += e.offsetTop;
                return t
            }, this.je = function() {
                var e = document.createElement("div");
                e.id = this.id + "hide", e.style.position = "fixed", e.style.left = "0px", e.style.top = "0px", e.style.right = "0px", e.style.bottom = "0px", e.oncontextmenu = function() {
                    return !1
                }, e.onmousedown = function() {
                    return !1
                }, document.body.appendChild(e);
                var t = document.createElement("div");
                t.id = this.id + "popup", t.style.position = "fixed", t.style.left = "50%", t.style.top = "0px", t.style.backgroundColor = "white", t.style.width = "50px", t.style.height = "50px", this.dragDrop && (t.onmousedown = this.ze);
                var i = null;
                this.useIframe && (i = document.createElement("iframe"), i.id = this.id + "iframe", i.name = this.id + "iframe", i.frameBorder = "0", i.style.width = "100%", i.style.height = "50px", t.appendChild(i)), document.body.appendChild(t), this.div = t, this.iframe = i, this.hideDiv = e
            }, this.setInnerHTML = function(e, i) {
                var n = window.frames[e],
                    a = n.contentWindow || n.document || n.contentDocument;
                a.document && (a = a.document), null == a.body && a.write("<body></body>"), i.nodeType ? a.body.appendChild(i) : a.body.innerHTML = i, t.autoStretch && (t.autoStretchFirstLoadOnly && t.De || (t.stretch(), t.De = !0))
            }, this.close = function(e) {
                this.result = e, this.hide()
            }, this.hide = function(e) {
                e = e || {};
                var i = {};
                i.backgroundClick = !!e.backgroundClick, i.result = this.result, i.preventDefault = function() {
                    this.preventDefault.value = !0
                }, "function" == typeof this.onClose && (this.onClose(i), i.preventDefault.value) || (this.div && (this.div.style.display = "none", this.hideDiv.style.display = "none", this.useIframe || (this.div.innerHTML = null)), window.focus(), DayPilot.ModalStatic.remove(this), "function" == typeof this.onClosed ? this.onClosed(i) : this.closed && this.closed(), this.result = null, this.disposeOnClose && (t.Ee(t.div), t.Ee(t.hideDiv), t.div = null, t.hideDiv = null, t.iframe = null))
            }, this.Ee = function(e) {
                e && e.parentNode && e.parentNode.removeChild(e)
            }, this.Fe = function() {
                if (e)
                    for (var t in e) this[t] = e[t]
            }, this.Fe()
        }, 
        
        
        DayPilot.Modal.alert = function(e, t) {
            t = t || {}, t.height = t.height || 40, t.useIframe = !1;
            var i = t.okText || "OK";
            t.cancelText || "Cancel";
            return DayPilot.getPromise(function(n, a) {
                t.onClosed = function(e) {
                    n(e)
                };
                var o = new DayPilot.Modal(t),
                    r = document.createElement("div");
                r.className = o.theme + "_inner";
                var l = document.createElement("div");
                l.className = o.theme + "_content", l.innerHTML = e;
                var s = document.createElement("div");
                s.className = o.theme + "_buttons";
                var d = document.createElement("button");
                d.innerText = i, d.className = o.theme + "_ok", d.onclick = function(e) {
                    DayPilot.ModalStatic.close("OK")
                }, s.appendChild(d), r.appendChild(l), r.appendChild(s), o.showHtml(r), d.focus()
            })
        }, 
        
        DayPilot.Modal.confirm = function(e, t) {
            t = t || {}, t.height = t.height || 40, t.useIframe = !1;
            var i = t.okText || "OK",
                n = t.cancelText || "Cancel";
            return DayPilot.getPromise(function(a, o) {
                t.onClosed = function(e) {
                    a(e)
                };
                var r = new DayPilot.Modal(t),
                    l = document.createElement("div");
                l.className = r.theme + "_inner";
                var s = document.createElement("div");
                s.className = r.theme + "_content", s.innerHTML = e;
                var d = document.createElement("div");
                d.className = r.theme + "_buttons";
                var c = document.createElement("button");
                c.innerText = i, c.className = r.theme + "_ok", c.onclick = function(e) {
                    DayPilot.ModalStatic.close("OK")
                };
                var h = document.createTextNode(" "),
                    u = document.createElement("button");
                u.innerText = n, u.className = r.theme + "_cancel", u.onclick = function(e) {
                    DayPilot.ModalStatic.close()
                }, d.appendChild(c), d.appendChild(h), d.appendChild(u), l.appendChild(s), l.appendChild(d), r.showHtml(l), c.focus()
            })
        }, 
        
        
        DayPilot.Modal.prompt = function(e, t, i) {
            "object" == typeof t && (i = t), i = i || {}, i.height = i.height || 40, i.useIframe = !1;
            var n = i.okText || "OK",
                a = i.cancelText || "Cancel",
                o = t || "";
            return DayPilot.getPromise(function(t, r) {
                i.onClosed = function(e) {
                    t(e)
                };
                var l = new DayPilot.Modal(i),
                    s = document.createElement("div");
                s.className = l.theme + "_inner";
                var d = document.createElement("div");
                d.className = l.theme + "_content", d.innerHTML = e;
                var c = document.createElement("div");
                c.className = l.theme + "_input";
                var h = document.createElement("input");
                h.value = o, h.style.width = "100%", h.onkeydown = function(e) {
                    switch (e.keyCode) {
                        case 13:
                            l.close(this.value);
                            break;
                        case 27:
                            l.close()
                    }
                }, c.appendChild(h);
                var u = document.createElement("div");
                u.className = l.theme + "_buttons";
                var f = document.createElement("button");
                f.innerText = n, f.className = l.theme + "_ok", f.onclick = function(e) {
                    l.close(h.value)
                };
                var v = document.createTextNode(" "),
                    p = document.createElement("button");
                p.innerText = a, p.className = l.theme + "_cancel", p.onclick = function(e) {
                    l.close()
                }, u.appendChild(f), u.appendChild(v), u.appendChild(p), s.appendChild(d), s.appendChild(c), s.appendChild(u), l.showHtml(s), h.focus()
            })
        }, 
        
        
        DayPilot.Modal.close = function(e) {
            if (!(parent && parent.DayPilot && parent.DayPilot.ModalStatic)) throw "Unable to close DayPilot.Modal dialog.";
            parent.DayPilot.ModalStatic.close(e)
        }, 
        
        
        DayPilot.Modal.closeSerialized = function() {
            for (var e = DayPilot.Modal.opener() || DayPilot.ModalStatic.last(), t = e.pe(), i = t.querySelectorAll("input, textarea, select"), n = {}, a = 0; a < i.length; a++) {
                var o = i[a],
                    r = o.name;
                if (r) {
                    var l = o.value;
                    n[r] = l
                }
            }
            DayPilot.Modal.close(n)
        }, 
        
        DayPilot.Modal.opener = function() {
            return parent && parent.DayPilot && parent.DayPilot.ModalStatic && parent.DayPilot.ModalStatic.list[parent.DayPilot.ModalStatic.list.length - 1]
        }, 
        
        "undefined" == typeof DayPilot.getPromise && (DayPilot.getPromise = function(e) {
            return "undefined" != typeof Promise ? new Promise(e) : (DayPilot.Promise = function(e) {
                var t = this;
                this.then = function(t, i) {
                    return t = t || function() {}, i = i || function() {}, e(t, i), DayPilot.getPromise(e)
                }, this["catch"] = function(i) {
                    return t.then(null, i), DayPilot.getPromise(e)
                }
            }, new DayPilot.Promise(e))
        })
    }(), "undefined" == typeof DayPilot) var DayPilot = {};


////////////////////////////janek6//////////////////////
