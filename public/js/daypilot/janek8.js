
if ("undefined" == typeof DayPilot.Global && (DayPilot.Global = {}), function() {
        if ("undefined" == typeof DayPilot.Navigator) {
            DayPilot.Navigator = function(e, t) {
                    this.v = "2018.1.3151";
                    var i = this;
                    this.id = e, this.api = 2, this.isNavigator = !0, this.autoFocusOnClick = !0, this.weekStarts = "Auto", this.selectMode = "day", this.titleHeight = 20, this.dayHeaderHeight = 20, this.bound = null, this.cellWidth = 20, this.cellHeight = 20, this.cssOnly = !0, this.cssClassPrefix = "navigator_default", this.freeHandSelectionEnabled = !1, this.selectionStart = (new DayPilot.Date).getDatePart(), this.selectionEnd = null, this.selectionDay = null, this.showMonths = 1, this.skipMonths = 1, this.command = "navigate", this.year = (new DayPilot.Date).getYear(), this.month = (new DayPilot.Date).getMonth() + 1, this.showWeekNumbers = !1, this.weekNumberAlgorithm = "Auto", this.rowsPerMonth = "Six", this.orientation = "Vertical", this.locale = "en-us", this.visible = !0, this.timeRangeSelectedHandling = "Bind", this.visibleRangeChangedHandling = "Enabled", this.onVisibleRangeChange = null, this.onVisibleRangeChanged = null, this.onTimeRangeSelect = null, this.onTimeRangeSelected = null, this.nav = {}, this.t = {}, this.pf = function() {
                        this.root.dp = this, this.cssOnly ? this.root.className = this.q("_main") : this.root.className = this.q("main"), "Horizontal" === this.orientation ? (this.root.style.width = this.showMonths * (7 * o.cellWidth() + this.qf()) + "px", this.root.style.height = 6 * this.cellHeight + this.titleHeight + this.dayHeaderHeight + "px") : this.root.style.width = 7 * o.cellWidth() + this.qf() + "px", this.root.style.position = "relative", this.visible || (this.root.style.display = "none");
                        var e = document.createElement("input");
                        e.type = "hidden", e.name = i.id + "_state", e.id = e.name, this.root.appendChild(e), this.state = e, this.startDate ? this.startDate = new DayPilot.Date(this.startDate).firstDayOfMonth() : this.startDate = DayPilot.Date.fromYearMonthDay(this.year, this.month), this.calendars = [], this.selected = [], this.months = []
                    }, this.va = function() {
                        return 2 === i.api
                    }, this.Ie = function() {
                        this.root.innerHTML = ""
                    }, this.q = function(e) {
                        var t = this.theme || this.cssClassPrefix;
                        return t ? t + e : ""
                    }, this.rf = function(e, t) {
                        var i = this.cssOnly ? this.q("_" + t) : this.q(t);
                        DayPilot.Util.addClass(e, i)
                    }, this.sf = function(e, t) {
                        var i = this.cssOnly ? this.q("_" + t) : this.q(t);
                        DayPilot.Util.removeClass(e, i)
                    }, this.Je = function(e, t) {
                        var n = {};
                        n.cells = [], n.days = [], n.weeks = [];
                        var a = this.startDate.addMonths(e),
                            r = t.before,
                            l = t.after,
                            s = a.firstDayOfMonth(),
                            d = s.firstDayOfWeek(o.weekStarts()),
                            c = s.addMonths(1),
                            h = DayPilot.DateUtil.daysDiff(d, c),
                            u = "Auto" === this.rowsPerMonth ? Math.ceil(h / 7) : 6;
                        n.rowCount = u;
                        var f = (new DayPilot.Date).getDatePart(),
                            v = 7 * o.cellWidth() + this.qf();
                        n.width = v;
                        var p = this.cellHeight * u + this.titleHeight + this.dayHeaderHeight;
                        n.height = p;
                        var g = document.createElement("div");
                        if (g.style.width = v + "px", g.style.height = p + "px", "Horizontal" === this.orientation) g.style.position = "absolute", g.style.left = v * e + "px", g.style.top = "0px", n.top = 0, n.left = v * e;
                        else {
                            g.style.position = "relative";
                            var m = e > 0 ? i.months[e - 1].top + i.months[e - 1].height : 0;
                            n.top = m, n.left = 0
                        }
                        this.cssOnly ? g.className = this.q("_month") : g.className = this.q("month"), g.style.cursor = "default", g.style.MozUserSelect = "none", g.style.KhtmlUserSelect = "none", g.style.WebkitUserSelect = "none", g.month = n, this.root.appendChild(g);
                        var y = this.titleHeight + this.dayHeaderHeight,
                            b = document.createElement("div");
                        b.style.position = "absolute", b.style.left = "0px", b.style.top = "0px", b.style.width = o.cellWidth() + "px", b.style.height = this.titleHeight + "px", b.style.lineHeight = this.titleHeight + "px", b.setAttribute("unselectable", "on"), this.cssOnly ? b.className = this.q("_titleleft") : b.className = this.q("titleleft"), t.left && (b.style.cursor = "pointer", b.innerHTML = "<span>&lt;</span>", b.onclick = this.tf), g.appendChild(b), this.tl = b;
                        var w = document.createElement("div");
                        w.style.position = "absolute", w.style.left = o.cellWidth() + "px", w.style.top = "0px", w.style.width = 5 * o.cellWidth() + this.qf() + "px", w.style.height = this.titleHeight + "px", w.style.lineHeight = this.titleHeight + "px", w.setAttribute("unselectable", "on"), this.cssOnly ? w.className = this.q("_title") : w.className = this.q("title"), w.innerHTML = o.locale().monthNames[a.getMonth()] + " " + a.getYear(), g.appendChild(w), this.ti = w;
                        var D = document.createElement("div");
                        D.style.position = "absolute", D.style.left = 6 * o.cellWidth() + this.qf() + "px", D.style.top = "0px", D.style.width = o.cellWidth() + "px", D.style.height = this.titleHeight + "px", D.style.lineHeight = this.titleHeight + "px", D.setAttribute("unselectable", "on"), this.cssOnly ? D.className = this.q("_titleright") : D.className = this.q("titleright"), t.right && (D.style.cursor = "pointer", D.innerHTML = "<span>&gt;</span>", D.onclick = this.uf), g.appendChild(D), this.tr = D;
                        var k = this.qf();
                        if (this.showWeekNumbers)
                            for (var x = 0; x < u; x++) {
                                var P = d.addDays(7 * x),
                                    C = null;
                                switch (this.weekNumberAlgorithm) {
                                    case "Auto":
                                        C = 1 === o.weekStarts() ? P.weekNumberISO() : P.weekNumber();
                                        break;
                                    case "US":
                                        C = P.weekNumber();
                                        break;
                                    case "ISO8601":
                                        C = P.weekNumberISO();
                                        break;
                                    default:
                                        throw "Unknown weekNumberAlgorithm value."
                                }
                                var S = document.createElement("div");
                                S.style.position = "absolute", S.style.left = "0px", S.style.top = x * this.cellHeight + y + "px", S.style.width = o.cellWidth() + "px", S.style.height = this.cellHeight + "px", S.style.lineHeight = this.cellHeight + "px", S.setAttribute("unselectable", "on"), this.cssOnly ? S.className = this.q("_weeknumber") : S.className = this.q("weeknumber"), S.innerHTML = "<span>" + C + "</span>", g.appendChild(S), n.weeks.push(S)
                            }
                        for (var A = 0; A < 7; A++) {
                            n.cells[A] = [];
                            var S = document.createElement("div");
                            S.style.position = "absolute", S.style.left = A * o.cellWidth() + k + "px", S.style.top = this.titleHeight + "px", S.style.width = o.cellWidth() + "px", S.style.height = this.dayHeaderHeight + "px", S.style.lineHeight = this.dayHeaderHeight + "px", S.setAttribute("unselectable", "on"), this.cssOnly ? S.className = this.q("_dayheader") : S.className = this.q("dayheader"), S.innerHTML = "<span>" + this.vf(A) + "</span>", g.appendChild(S), n.days.push(S);
                            for (var x = 0; x < u; x++) {
                                var P = d.addDays(7 * x + A),
                                    T = this.wf(P) && "none" !== this.xf(),
                                    E = P.firstDayOfMonth() === a,
                                    M = P < a,
                                    H = P >= a.addMonths(1);
                                if ("month" === this.xf()) T = T && E;
                                else if ("day" === this.xf()) T = T && (E || r && M || l && H);
                                else if ("week" === this.xf()) {
                                    var _ = P.firstDayOfMonth() === a;
                                    T = T && (_ || r && M || l && H)
                                }
                                var R = document.createElement("div");
                                n.cells[A][x] = R;
                                var B = i.yf(A, x),
                                    N = B.x,
                                    U = B.y;
                                R.day = P, R.x = A, R.y = x, R.left = N, R.top = U, R.isCurrentMonth = E, R.isNextMonth = H, R.isPrevMonth = M, R.showBefore = r, R.showAfter = l, this.cssOnly ? R.className = this.q(E ? "_day" : "_dayother") : R.className = this.q(E ? "day" : "dayother"), i.rf(R, "cell"), P.getTime() === f.getTime() && E && this.rf(R, "today"), 0 !== P.dayOfWeek() && 6 !== P.dayOfWeek() || this.rf(R, "weekend"), R.style.position = "absolute", R.style.left = N + "px", R.style.top = U + "px", R.style.width = o.cellWidth() + "px", R.style.height = this.cellHeight + "px", R.style.lineHeight = this.cellHeight + "px";
                                var z = document.createElement("div");
                                z.style.position = "absolute", this.cssOnly ? z.className = P.getTime() === f.getTime() && E ? this.q("_todaybox") : this.q("_daybox") : z.className = P.getTime() === f.getTime() && E ? this.q("todaybox") : this.q("daybox"), i.rf(z, "cell_box"), z.style.left = "0px", z.style.top = "0px", z.style.right = "0px", z.style.bottom = "0px", R.appendChild(z);
                                var L = null;
                                if (this.cells && this.cells[P.toStringSortable()] && (L = this.cells[P.toStringSortable()]), "function" == typeof i.onBeforeCellRender) {
                                    var O = {};
                                    O.cell = L || {}, O.cell.day = P, O.cell.isCurrentMonth = E, O.cell.isToday = P.getTime() === f.getTime() && E, O.cell.isWeekend = 0 === P.dayOfWeek() || 6 === P.dayOfWeek(), L ? (O.cell.html = L.html || P.getDay(), O.cell.cssClass = L.css) : (O.cell.html = P.getDay(), O.cell.cssClass = null), i.onBeforeCellRender(O), L = O.cell
                                }
                                if (L && DayPilot.Util.addClass(R, L.cssClass || L.css), E || r && M || l && H) {
                                    var I = document.createElement("div");
                                    I.innerHTML = P.getDay(), I.style.position = "absolute", I.style.left = "0px", I.style.top = "0px", I.style.right = "0px", I.style.bottom = "0px", i.rf(I, "cell_text"), R.style.cursor = "pointer", R.isClickable = !0, L && L.html && (I.innerHTML = L.html), R.appendChild(I)
                                }
                                R.setAttribute("unselectable", "on"), R.onclick = this.zf, g.appendChild(R), T && (i.Af(g, A, x), this.selected.push(R))
                            }
                        }
                        var F = document.createElement("div");
                        F.style.position = "absolute", F.style.left = "0px", F.style.top = y - 2 + "px", F.style.width = 7 * o.cellWidth() + this.qf() + "px", F.style.height = "1px", F.style.fontSize = "1px", F.style.lineHeight = "1px", this.cssOnly ? F.className = this.q("_line") : F.className = this.q("line"), g.appendChild(F), this.months.push(n)
                    }, this.yf = function(e, t) {
                        var i = this.titleHeight + this.dayHeaderHeight,
                            n = this.qf();
                        return {
                            "x": e * o.cellWidth() + n,
                            "y": t * this.cellHeight + i
                        }
                    }, this.Af = function(e, t, n) {
                        var a = e.month.cells[t][n];
                        i.rf(a, "select")
                    }, this.Bf = function(e, t, n) {
                        var a = e.month.cells[t][n];
                        i.sf(a, "select")
                    }, this.qf = function() {
                        return this.showWeekNumbers ? o.cellWidth() : 0
                    }, this.Cf = function() {
                        if (this.items)
                            for (var e = 0; e < this.showMonths; e++)
                                for (var t = 0; t < 7; t++)
                                    for (var i = 0; i < 6; i++) {
                                        var n = this.months[e].cells[t][i];
                                        n && (1 === this.items[n.day.toStringSortable()] ? this.rf(n, "busy") : this.sf(n, "busy"))
                                    }
                    }, this.Df = function() {
                        var e = {};
                        e.startDate = i.startDate, e.selectionStart = i.selectionStart, e.selectionEnd = i.selectionEnd.addDays(1), i.state.value = JSON.stringify(e)
                    }, this.xf = function() {
                        return (this.selectMode || "").toLowerCase()
                    }, this.Ef = function() {
                        var e = this.selectionDay || this.selectionStart;
                        e || (e = DayPilot.Date.today());
                        var e = new DayPilot.Date(e);
                        switch (this.xf()) {
                            case "day":
                                this.selectionDay = e, this.selectionEnd = e;
                                break;
                            case "week":
                                this.selectionDay = e, this.selectionStart = e.firstDayOfWeek(o.weekStarts()), this.selectionEnd = this.selectionStart.addDays(6);
                                break;
                            case "month":
                                this.selectionDay = e, this.selectionStart = e.firstDayOfMonth(), this.selectionEnd = this.selectionStart.lastDayOfMonth();
                                break;
                            case "none":
                                this.selectionEnd = e;
                                break;
                            default:
                                throw "Unknown selectMode value."
                        }
                    }, this.select = function(e, t) {
                        var i = !0,
                            n = !0;
                        "object" == typeof t ? (t.dontFocus && (i = !1), t.dontNotify && (n = !1)) : "boolean" == typeof t && (i = !t);
                        var a = this.selectionStart,
                            o = this.selectionEnd;
                        this.selectionStart = new DayPilot.Date(e).getDatePart(), this.selectionDay = this.selectionStart;
                        var r = !1;
                        if (i) {
                            var l = this.startDate;
                            (this.selectionStart < this.Ff() || this.selectionStart >= this.Gf()) && (l = this.selectionStart.firstDayOfMonth()), l.toStringSortable() !== this.startDate.toStringSortable() && (r = !0), this.startDate = l
                        }
                        this.Ef(), this.Ie(), this.pf(), this.Hf(), this.Cf(), this.Df(), !n || a.equals(this.selectionStart) && o.equals(this.selectionEnd) || this.cb(), r && this.If()
                    }, this.update = function() {
                        i.cssOnly || (i.cssOnly = !0, DayPilot.Util.log("DayPilot: cssOnly = false mode is not supported since DayPilot Pro 8.0.")), this.Ie(), this.pf(), this.Ef(), this.Hf(), this.da(), this.Cf(), this.Df(), this.visible ? this.show() : this.hide()
                    }, this.H = function(e, t, i) {
                        var n = {};
                        n.action = e, n.parameters = i, n.data = t, n.header = this.G();
                        var a = "JSON" + JSON.stringify(n);
                        this.backendUrl ? DayPilot.request(this.backendUrl, this.J, a, this.K) : WebForm_DoCallback(this.uniqueID, a, this.L, null, this.callbackError, !0)
                    }, this.K = function(e) {
                        if ("function" == typeof i.onAjaxError) {
                            var t = {};
                            t.request = e, i.onAjaxError(t)
                        } else "function" == typeof i.ajaxError && i.ajaxError(e)
                    }, this.J = function(e) {
                        i.L(e.responseText)
                    }, this.F = function(e, t, n) {
                        var a = {};
                        a.action = e, a.parameters = n, a.data = t, a.header = this.G();
                        var o = "JSON" + JSON.stringify(a);
                        __doPostBack(i.uniqueID, o)
                    }, this.G = function() {
                        var e = {};
                        return e.v = this.v, e.startDate = this.startDate, e.selectionStart = this.selectionStart, e.showMonths = this.showMonths, e
                    }, this.Jf = function(e, t) {
                        "refresh" === e && this.If()
                    }, this.vf = function(e) {
                        var t = e + o.weekStarts();
                        return t > 6 && (t -= 7), o.locale().dayNamesShort[t]
                    }, this.wf = function(e) {
                        return null !== this.selectionStart && null !== this.selectionEnd && (this.selectionStart.getTime() <= e.getTime() && e.getTime() <= this.selectionEnd.getTime())
                    }, this.Kf = function(e) {
                        for (var t = 0; t < i.months.length; t++) {
                            var n = i.months[t];
                            if (!n) return null;
                            if (e.x < n.left || n.width < e.x) return null;
                            i.months[t].height;
                            if (n.top <= e.y && e.y < n.top + n.height) return t
                        }
                        return null
                    }, this.Lf = function(e) {
                        var t = DayPilot.mo3(i.nav.top, e),
                            n = i.Kf(t);
                        if (null === n) return null;
                        var a = i.months[n],
                            o = this.titleHeight + this.dayHeaderHeight;
                        if (a.top <= t.y && t.y < a.top + o) return {
                            "month": n,
                            "x": 0,
                            "y": 0,
                            "coords": t,
                            "header": !0
                        };
                        for (var r = 0; r < a.cells.length; r++)
                            for (var l = 0; l < a.cells[r].length; l++) {
                                var s = a.cells[r][l],
                                    d = s.top + a.top,
                                    c = s.left + a.left;
                                if (c <= t.x && t.x < c + i.cellWidth && d <= t.y && t.y < d + i.cellHeight) return {
                                    "month": n,
                                    "x": r,
                                    "y": l,
                                    "coords": t
                                }
                            }
                        return null
                    }, this.Mf = function(e) {
                        if (i.freeHandSelectionEnabled) {
                            var t = i.Lf(e);
                            t && !t.header && (n.start = t), i.months[t.month].cells[t.x][t.y], e.preventDefault()
                        }
                    }, this.Nf = function(e) {
                        if (n.start) {
                            var t = i.Lf(e);
                            if (n.end) n.end = t;
                            else if (t) {
                                var a = 3,
                                    o = DayPilot.distance(n.start.coords, t.coords);
                                o > a && (n.end = t)
                            }
                            n.end && (n.clear(), n.draw())
                        }
                    }, this.Of = {};
                    var n = this.Of;
                    n.start = null, n.drawCell = function(e) {
                        var t = i.months[e.month],
                            a = i.yf(e.x, e.y),
                            o = t.top + a.y,
                            r = t.left + a.x,
                            l = document.createElement("div");
                        l.style.position = "absolute", l.style.left = r + "px", l.style.top = o + "px", l.style.height = i.cellHeight + "px",
                            l.style.width = i.cellWidth + "px", l.style.backgroundColor = "#ccc", l.style.opacity = .5, l.style.cursor = "default", i.nav.preselection.appendChild(l), n.cells.push(l)
                    }, n.clear = function() {
                        if (n.cells) {
                            for (var e = 0; e < n.cells.length; e++) i.nav.preselection.removeChild(n.cells[e]);
                            n.cells = []
                        }
                    }, n.draw = function() {
                        var e = n.ordered(),
                            t = new a(e.start),
                            o = e.end;
                        if (o) {
                            if (o === n.end && o.header && o.month > 0) {
                                o.month -= 1;
                                var r = i.months[o.month];
                                o.x = 6, o.y = r.rowCount - 1
                            }
                            for (n.cells = []; !t.is(o);) {
                                n.drawCell(t);
                                var l = new a(t).next();
                                if (!l) return;
                                t.month = l.month, t.x = l.x, t.y = l.y
                            }
                            n.drawCell(t)
                        }
                    }, n.ordered = function() {
                        var e = n.start,
                            t = n.end,
                            i = {};
                        return !t || new a(e).before(t) ? (i.start = e, i.end = t) : (i.start = t, i.end = e), i
                    };
                    var a = function(e, t, n) {
                        if (e instanceof a) return e;
                        if ("object" == typeof e) {
                            var o = e;
                            this.month = o.month, this.x = o.x, this.y = o.y
                        } else this.month = e, this.x = t, this.y = n;
                        this.is = function(e) {
                            return this.month === e.month && this.x === e.x && this.y === e.y
                        }, this.next = function() {
                            var e = this;
                            if (e.x < 6) return {
                                "month": e.month,
                                "x": e.x + 1,
                                "y": e.y
                            };
                            var t = i.months[e.month];
                            return e.y < t.rowCount - 1 ? {
                                "month": e.month,
                                "x": 0,
                                "y": e.y + 1
                            } : e.month < i.months.length - 1 ? {
                                "month": e.month + 1,
                                "x": 0,
                                "y": 0
                            } : null
                        }, this.visible = function() {
                            var e = this.cell();
                            return !!e.isCurrentMonth || (!(!e.isPrevMonth || !e.showBefore) || !(!e.isNextMonth || !e.showAfter))
                        }, this.nextVisible = function() {
                            for (var e = this; !e.visible();) {
                                var t = e.next();
                                if (!t) return null;
                                e = new a(t)
                            }
                            return e
                        }, this.previous = function() {
                            var e = this;
                            if (e.x > 0) return {
                                "month": e.month,
                                "x": e.x - 1,
                                "y": e.y
                            };
                            i.months[e.month];
                            if (e.y > 0) return {
                                "month": e.month,
                                "x": 6,
                                "y": e.y - 1
                            };
                            if (e.month > 0) {
                                var t = i.months[e.month - 1];
                                return {
                                    "month": e.month - 1,
                                    "x": 6,
                                    "y": t.rowCount - 1
                                }
                            }
                            return null
                        }, this.previousVisible = function() {
                            for (var e = this; !e.visible();) {
                                var t = e.previous();
                                if (!t) return null;
                                e = new a(t)
                            }
                            return e
                        }, this.cell = function() {
                            return i.months[this.month].cells[this.x][this.y]
                        }, this.date = function() {
                            return this.cell().day
                        }, this.before = function(e) {
                            return this.date() < new a(e).date()
                        }
                    };
                    this.zf = function(e) {
                        var t = this.parentNode,
                            n = this.parentNode.month,
                            a = this.x,
                            o = this.y,
                            r = n.cells[a][o].day;
                        if (n.cells[a][o].isClickable) {
                            i.clearSelection(), i.selectionDay = r;
                            var r = i.selectionDay;
                            switch (i.xf()) {
                                case "none":
                                    i.selectionStart = r, i.selectionEnd = r;
                                    break;
                                case "day":
                                    if (i.autoFocusOnClick) {
                                        var l = r;
                                        if (r < i.Ff() || r >= i.Gf()) return void i.select(r)
                                    }
                                    var s = n.cells[a][o];
                                    i.Af(t, a, o), i.selected.push(s), i.selectionStart = s.day, i.selectionEnd = s.day;
                                    break;
                                case "week":
                                    if (i.autoFocusOnClick) {
                                        var l = n.cells[0][o].day,
                                            d = n.cells[6][o].day;
                                        if (l.firstDayOfMonth() === d.firstDayOfMonth() && (l < i.Ff() || d >= i.Gf())) return void i.select(r)
                                    }
                                    for (var c = 0; c < 7; c++) i.Af(t, c, o), i.selected.push(n.cells[c][o]);
                                    i.selectionStart = n.cells[0][o].day, i.selectionEnd = n.cells[6][o].day;
                                    break;
                                case "month":
                                    if (i.autoFocusOnClick) {
                                        var l = r;
                                        if (r < i.Ff() || r >= i.Gf()) return void i.select(r)
                                    }
                                    for (var l = null, d = null, o = 0; o < 6; o++)
                                        for (var a = 0; a < 7; a++) {
                                            var s = n.cells[a][o];
                                            s && s.day.getYear() === r.getYear() && s.day.getMonth() === r.getMonth() && (i.Af(t, a, o), i.selected.push(s), null === l && (l = s.day), d = s.day)
                                        }
                                    i.selectionStart = l, i.selectionEnd = d;
                                    break;
                                default:
                                    throw "unknown selectMode"
                            }
                            i.Df(), i.cb()
                        }
                    }, this.cb = function(e) {
                        var t = i.selectionStart,
                            n = i.selectionEnd.addDays(1),
                            a = DayPilot.DateUtil.daysDiff(t, n),
                            o = i.selectionDay;
                        if (e = e || {}, i.va()) {
                            var r = {};
                            if (r.start = t, r.end = n, r.day = o, r.days = a, r.mode = e.mode || i.selectMode, r.preventDefault = function() {
                                    this.preventDefault.value = !0
                                }, "function" == typeof i.onTimeRangeSelect && (i.onTimeRangeSelect(r), r.preventDefault.value)) return;
                            switch (i.timeRangeSelectedHandling) {
                                case "Bind":
                                    var l = DayPilot.Util.evalVariable(i.bound);
                                    if (l) {
                                        var s = {};
                                        s.start = t, s.end = n, s.days = a, s.day = o, l.commandCallBack(i.command, s)
                                    }
                                    break;
                                case "None":
                                    break;
                                case "PostBack":
                                    i.timeRangeSelectedPostBack(t, n, o)
                            }
                            "function" == typeof i.onTimeRangeSelected && i.onTimeRangeSelected(r)
                        } else switch (i.timeRangeSelectedHandling) {
                            case "Bind":
                                var l = DayPilot.Util.evalVariable(i.bound);
                                if (l) {
                                    var s = {};
                                    s.start = t, s.end = n, s.days = a, s.day = o, l.commandCallBack(i.command, s)
                                }
                                break;
                            case "JavaScript":
                                i.onTimeRangeSelected(t, n, o);
                                break;
                            case "None":
                                break;
                            case "PostBack":
                                i.timeRangeSelectedPostBack(t, n, o)
                        }
                    }, this.timeRangeSelectedPostBack = function(e, t, i, n) {
                        var a = {};
                        a.start = e, a.end = t, a.day = n, this.F("TimeRangeSelected", i, a)
                    }, this.uf = function(e) {
                        i.Pf(i.skipMonths)
                    }, this.tf = function(e) {
                        i.Pf(-i.skipMonths)
                    }, this.Pf = function(e) {
                        this.startDate = this.startDate.addMonths(e), this.Ie(), this.pf(), this.Hf(), this.Df(), this.If(), this.Cf()
                    }, this.Ff = function() {
                        return i.startDate.firstDayOfMonth()
                    }, this.Gf = function() {
                        return i.startDate.firstDayOfMonth().addMonths(this.showMonths)
                    }, this.visibleStart = function() {
                        return i.startDate.firstDayOfMonth().firstDayOfWeek(o.weekStarts())
                    }, this.visibleEnd = function() {
                        return i.startDate.firstDayOfMonth().addMonths(this.showMonths - 1).firstDayOfWeek(o.weekStarts()).addDays(42)
                    }, this.If = function() {
                        var e = this.visibleStart(),
                            t = this.visibleEnd();
                        if (i.va()) {
                            var n = {};
                            if (n.start = e, n.end = t, n.preventDefault = function() {
                                    this.preventDefault.value = !0
                                }, "function" == typeof i.onVisibleRangeChange && (i.onVisibleRangeChange(n), n.preventDefault.value)) return;
                            switch (this.visibleRangeChangedHandling) {
                                case "CallBack":
                                    this.visibleRangeChangedCallBack(null);
                                    break;
                                case "PostBack":
                                    this.visibleRangeChangedPostBack(null);
                                    break;
                                case "Disabled":
                            }
                            "function" == typeof i.onVisibleRangeChanged && i.onVisibleRangeChanged(n)
                        } else switch (this.visibleRangeChangedHandling) {
                            case "CallBack":
                                this.visibleRangeChangedCallBack(null);
                                break;
                            case "PostBack":
                                this.visibleRangeChangedPostBack(null);
                                break;
                            case "JavaScript":
                                this.onVisibleRangeChanged(e, t);
                                break;
                            case "Disabled":
                        }
                    }, this.visibleRangeChangedCallBack = function(e) {
                        var t = {};
                        this.H("Visible", e, t)
                    }, this.visibleRangeChangedPostBack = function(e) {
                        var t = {};
                        this.F("Visible", e, t)
                    }, this.L = function(e, t) {
                        var e = JSON.parse(e);
                        i.items = e.Items, i.cells = e.Cells, i.cells ? i.update() : i.Cf()
                    }, this.Hf = function() {
                        for (var e = 0; e < this.showMonths; e++) {
                            var t = this.Qf(e);
                            this.Je(e, t)
                        }
                        this.root.style.height = this.cf() + "px", this.nav.preselection = document.createElement("div"), this.nav.preselection.style.position = "absolute", this.nav.preselection.style.left = "0px", this.nav.preselection.style.top = "0px", this.root.appendChild(this.nav.preselection)
                    }, this.cf = function() {
                        if ("Horizontal" === this.orientation) {
                            for (var e = 0, t = 0; t < this.months.length; t++) {
                                var i = this.months[t];
                                i.height > e && (e = i.height)
                            }
                            return e
                        }
                        for (var n = 0, t = 0; t < this.months.length; t++) {
                            var i = this.months[t];
                            n += i.height
                        }
                        return n
                    }, this.Qf = function(e) {
                        if (this.internal.showLinks) return this.internal.showLinks;
                        var t = {};
                        return t.left = 0 === e, t.right = 0 === e, t.before = 0 === e, t.after = e === this.showMonths - 1, "Horizontal" === this.orientation && (t.right = e === this.showMonths - 1), t
                    }, this.kd = {}, this.kd.scope = null, this.kd.notify = function() {
                        i.kd.scope && i.kd.scope["$apply"]()
                    }, this.internal = {}, this.internal.initialized = function() {
                        return i.A
                    }, this.zc = {};
                    var o = this.zc;
                    o.locale = function() {
                        return DayPilot.Locale.find(i.locale)
                    }, o.weekStarts = function() {
                        if ("Auto" === i.weekStarts) {
                            var e = o.locale();
                            return e ? e.weekStarts : 0
                        }
                        return i.weekStarts
                    }, o.cellWidth = function() {
                        if (i.t.cellWidth) return i.t.cellWidth;
                        var e = i.qd("_cell_dimensions").width;
                        return e || (e = i.cellWidth), i.t.cellWidth = e, e
                    }, this.clearSelection = function() {
                        for (var e = 0; e < this.selected.length; e++) {
                            var t = this.selected[e];
                            i.Bf(t.parentNode, t.x, t.y)
                        }
                        this.selected = []
                    }, this.ud = function() {
                        return !!this.backendUrl && ("undefined" == typeof i.items || !i.items)
                    }, this.events = {}, this.da = function() {
                        if (DayPilot.isArray(this.events.list)) {
                            this.items = {};
                            for (var e = 0; e < this.events.list.length; e++) {
                                var t = this.events.list[e],
                                    i = this.Rf(t);
                                for (var n in i) this.items[n] = 1
                            }
                        }
                    }, this.qd = function(e) {
                        var t = document.createElement("div");
                        t.style.position = "absolute", t.style.top = "-2000px", t.style.left = "-2000px", t.className = this.q(e);
                        var n = i.root || document.body;
                        n.appendChild(t);
                        var a = t.offsetHeight,
                            o = t.offsetWidth;
                        n.removeChild(t);
                        var r = {};
                        return r.height = a, r.width = o, r
                    }, this.Rf = function(e) {
                        for (var t = new DayPilot.Date(e.start), i = new DayPilot.Date(e.end), n = {}, a = t.getDatePart(); a.getTime() < i.getTime();) n[a.toStringSortable()] = 1, a = a.addDays(1);
                        return n
                    }, this.show = function() {
                        i.visible = !0, i.root.style.display = ""
                    }, this.hide = function() {
                        i.visible = !1, i.root.style.display = "none"
                    }, this.vd = function() {
                        if (this.id && this.id.tagName) this.nav.top = this.id;
                        else {
                            if ("string" != typeof this.id) throw "DayPilot.Navigator() constructor requires the target element or its ID as a parameter";
                            if (this.nav.top = document.getElementById(this.id), !this.nav.top) throw "DayPilot.Navigator: The placeholder element not found: '" + e + "'."
                        }
                        this.root = this.nav.top
                    }, this.init = function() {
                        if (this.vd(), !this.root.dp) {
                            i.cssOnly || (i.cssOnly = !0, DayPilot.Util.log("DayPilot: cssOnly = false mode is not supported since DayPilot Pro 8.0.")), this.Ef(), this.pf(), this.Hf(), this.da(), this.Cf(), this.S(), this.Sf(), this.Zc();
                            return this.ud() && this.If(), this.A = !0, this
                        }
                    }, this.Sf = function() {
                        i.nav.top.onmousedown = this.Mf, i.nav.top.onmousemove = this.Nf
                    }, this.Zc = function() {
                        DayPilot.re(document, "mouseup", i.Yd)
                    }, this.Yd = function(e) {
                        if (n.start && n.end) {
                            var t = DayPilot.mo3(i.nav.top, e);
                            if (t.x === n.start.coords.x && t.y === n.start.coords.y) return n.start = null, void n.clear();
                            n.clear();
                            var o = n.ordered();
                            o.start = new a(o.start).nextVisible(), o.end = new a(o.end).previousVisible(), i.selectionDay = new a(o.start).date(), i.selectionStart = i.selectionDay, i.selectionEnd = new a(o.end).date(), n.start = null, n.end = null, i.Ie(), i.pf(), i.Hf(), i.Cf(), i.Df();
                            i.cb({
                                "mode": "freehand"
                            })
                        }
                        n.start = null, n.end = null
                    }, this.dispose = function() {
                        var e = i;
                        e.root && (e.root.removeAttribute("style"), e.root.removeAttribute("class"), e.root.dp = null, e.root.innerHTML = null, e.root = null)
                    }, this.S = function() {
                        this.root.dispose = this.dispose
                    }, this.Init = this.init
                }, "undefined" != typeof jQuery && ! function(e) {
                    e.fn.daypilotNavigator = function(e) {
                        var t = null,
                            i = this.each(function() {
                                if (!this.daypilot) {
                                    var i = new DayPilot.Navigator(this.id);
                                    this.daypilot = i;
                                    for (var n in e) i[n] = e[n];
                                    i.Init(), t || (t = i)
                                }
                            });
                        return 1 === this.length ? t : i
                    }
                }(jQuery),
                function() {
                    var e = DayPilot.am();
                    e && e.directive("daypilotNavigator", ["$parse", function(e) {
                        return {
                            "restrict": "E",
                            "template": "<div id='{{id}}'></div>",
                            "compile": function(t, i) {
                                return t.replaceWith(this["template"].replace("{{id}}", i["id"])),
                                    function(t, i, n) {
                                        var a = new DayPilot.Navigator(i[0]);
                                        a.kd.scope = t, a.init();
                                        var o = n["id"];
                                        o && (t[o] = a);
                                        var r = n["publishAs"];
                                        if (r) {
                                            (0, e(r).assign)(t, a)
                                        }
                                        for (var l in n)
                                            if (0 === l.indexOf("on")) {
                                                var s = DayPilot.Util.shouldApply(l);
                                                s ? ! function(i) {
                                                    a[i] = function(a) {
                                                        var o = e(n[i]);
                                                        t["$apply"](function() {
                                                            o(t, {
                                                                "args": a
                                                            })
                                                        })
                                                    }
                                                }(l) : ! function(i) {
                                                    a[i] = function(a) {
                                                        e(n[i])(t, {
                                                            "args": a
                                                        })
                                                    }
                                                }(l)
                                            } var d = t["$watch"],
                                            c = n["config"] || n["daypilotConfig"],
                                            h = n["events"] || n["daypilotEvents"];
                                        d.call(t, c, function(e, t) {
                                            for (var i in e) a[i] = e[i];
                                            a.update()
                                        }, !0), d.call(t, h, function(e) {
                                            a.events.list = e, a.da(), a.Cf()
                                        }, !0)
                                    }
                            }
                        }
                    }])
                }(), "undefined" != typeof Sys && Sys.Application && Sys.Application.notifyScriptLoaded && Sys.Application.notifyScriptLoaded()
        }
    }(), 
"undefined" == typeof DayPilot) var DayPilot = {};

////////////////////////////janek8//////////////////////

