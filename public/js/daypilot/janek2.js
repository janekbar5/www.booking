if ("undefined" == typeof DayPilot.Global && (DayPilot.Global = {}), function() {
        if ("undefined" == typeof DayPilot.Bubble) {
            var e = {};
            e.mouseMove = function(t) {
                if ("undefined" != typeof e) {
                    e.mouse = e.mousePosition(t);
                    var i = e.active;
                    if (i && i.n.showPosition) {
                        var n = i.n.showPosition,
                            a = e.mouse;
                        n.clientX !== a.clientX || n.clientY !== a.clientY
                    }
                }
            }, e.touchPosition = function(e) {
                if (!e || !e.touches) return null;
                var t = e.touches[0],
                    i = {};
                return i.x = t.pageX, i.y = t.pageY, i.clientX = t.clientX, i.clientY = t.clientY, i
            }, e.mousePosition = function(e) {
                var t = DayPilot.mo3(null, e);
                return e && (t.clientY = e.clientY, t.clientX = e.clientX), t
            }, DayPilot.Bubble = function(t) {
                this.v = "2018.1.3151";
                var i = this,
                    n = {};
                if (this.cssOnly = !0, this.hideAfter = 500, this.loadingText = "Loading...", this.animated = !0, this.animation = "fast", this.position = "EventTop", this.hideOnClick = !0, this.showAfter = 500, this.showLoadingLabel = !0, this.zIndex = 10, this.theme = "bubble_default", this.n = function() {}, this.callBack = function(e) {
                        this.aspnet() ? WebForm_DoCallback(this.uniqueID, JSON.stringify(e), this.updateView, this, this.callbackError, !0) : e.calendar.internal.bubbleCallBack ? e.calendar.internal.bubbleCallBack(e, this) : e.calendar.bubbleCallBack(e, this)
                    }, this.callbackError = function(e, t) {
                        alert(e)
                    }, this.updateView = function(t, a) {
                        if (i !== a) throw "Callback object mismatch (internal error)";
                        return t ? (e.active = i, void(i && (n.div && (n.div.firstChild.innerHTML = t), i.adjustPosition(), i.animated || i.addShadow()))) : (i.removeDiv(), void i.removeShadow())
                    }, this.init = function() {}, this.aspnet = function() {
                        return "undefined" != typeof WebForm_DoCallback
                    }, this.rounded = function() {
                        return "Rounded" === this.corners
                    }, this.showEvent = function(t, i) {
                        var n = new e.CallBackArgs(t.calendar || t.root, "Event", t, t.bubbleHtml ? t.bubbleHtml() : null);
                        i ? this.show(n) : this.showOnMouseOver(n)
                    }, this.showCell = function(t) {
                        e.cancelShowing();
                        var i = new e.CallBackArgs(t.calendar || t.root, "Cell", t, t.staticBubbleHTML ? t.staticBubbleHTML() : null);
                        this.showOnMouseOver(i)
                    }, this.showTime = function(t) {
                        var i = new e.CallBackArgs(t.calendar || t.root, "Time", t, t.staticBubbleHTML ? t.staticBubbleHTML() : null);
                        this.showOnMouseOver(i)
                    }, this.showResource = function(t, i) {
                        var n = {};
                        n.calendar = t.calendar, n.id = t.id, t.bubbleHtml ? n.bubbleHtml = function() {
                            return t.bubbleHtml
                        } : t.data && t.data.bubbleHtml && (n.bubbleHtml = function() {
                            return t.data.bubbleHtml
                        }), n.toJSON = function() {
                            var e = {};
                            return e.id = this.id, e
                        };
                        var a = new e.CallBackArgs(n.calendar || n.root, "Resource", n, n.bubbleHtml ? n.bubbleHtml() : null);
                        a.div = t.div, i ? this.show(a) : this.showOnMouseOver(a)
                    }, this.p = function(e) {
                        return {}
                    }, this.showHtml = function(t, i) {
                        var n = new e.CallBackArgs(null, "Html", null, t);
                        n.div = i, this.show(n)
                    }, this.show = function(t) {
                        var a = this.animated;
                        if (this.n.showPosition = e.mouse, !e.mouse) return void setTimeout(function() {
                            i.show(t)
                        }, 100);
                        var o;
                        try {
                            o = JSON.stringify(t.object)
                        } catch (e) {
                            return
                        }
                        if (!(e.active === this && this.n.sourceId === o || "undefined" != typeof DayPilot.Menu && DayPilot.Menu.active)) {
                            i.cssOnly || (i.cssOnly = !0, DayPilot.Util.log("DayPilot: cssOnly = false mode is not supported since DayPilot Pro 8.0.")), e.hideActive(), e.active = this, this.n.sourceId = o;
                            var r = document.createElement("div");
                            r.setAttribute("unselectable", "on"), r.style.position = "absolute", this.showLoadingLabel || (r.style.display = "none"), this.cssOnly ? r.className = this.q("_main") : (this.width && (r.style.width = this.width), r.style.cursor = "default"), r.style.top = "0px", r.style.left = "0px", r.style.zIndex = this.zIndex + 1, a && (r.style.visibility = "hidden"), this.hideOnClick && (r.onclick = function() {
                                e.hideActive()
                            }), r.onmousemove = function(t) {
                                e.cancelHiding();
                                var t = t || window.event;
                                t.cancelBubble = !0
                            }, r.oncontextmenu = function() {
                                return !1
                            }, r.onmouseout = function() {
                                i.delayedHide()
                            };
                            var l = document.createElement("div");
                            r.appendChild(l), this.cssOnly ? l.className = this.q("_main_inner") : (l.style.padding = "4px", this.border && (l.style.border = this.border), this.rounded() && (l.style.MozBorderRadius = "5px", l.style.webkitBorderRadius = "5px", l.style.borderRadius = "5px"), l.style.backgroundColor = this.backgroundColor), l.innerHTML = this.loadingText, document.body.appendChild(r), n.div = r;
                            var r = this.getDiv(t);
                            if ("EventTop" === this.position && r) {
                                var s = DayPilot.abs(r, !0);
                                this.n.mouse = e.mouse, this.n.mouse.x = s.x, this.n.mouse.y = s.y, this.n.mouse.h = s.h + 2, this.n.mouse.w = s.w
                            } else this.n.mouse = e.mouse;
                            if (this.showLoadingLabel && !a && (this.adjustPosition(), this.addShadow()), t.staticHTML && "function" != typeof this.onLoad) this.updateView(t.staticHTML, this);
                            else if ("function" == typeof this.onLoad) {
                                var d = {};
                                d.source = t.object, d.async = !1, d.html = t.staticHTML, d.loaded = function() {
                                    this.async && i.updateView(d.html, i)
                                }, this.onLoad(d), d.async || i.updateView(d.html, i)
                            } else this.r(t) && this.callBack(t)
                        }
                    }, this.getDiv = function(e) {
                        return e.div ? e.div : "Event" === e.type && e.calendar && e.calendar.internal.findEventDiv ? e.calendar.internal.findEventDiv(e.object) : void 0
                    }, this.q = function(e) {
                        var t = this.theme || this.cssClassPrefix;
                        return t ? t + e : ""
                    }, this.loadingElement = null, this.loadingStart = function(e) {}, this.loadingStop = function() {}, this.adjustPosition = function() {
                        var t = this.animated,
                            a = this.position,
                            o = 10;
                        if (n.div && this.n.mouse) {
                            if (!this.n.mouse.x || !this.n.mouse.y) return void e.hideActive();
                            var r = n.div;
                            r.style.display = "";
                            var l = r.offsetHeight,
                                s = r.offsetWidth;
                            r.style.display = "none";
                            var d = DayPilot.wd(),
                                c = d.width,
                                h = d.height;
                            if ("Mouse" === a) {
                                var u = 0;
                                if (this.n.mouse.clientY > h - l + o) {
                                    this.n.mouse.clientY - (h - l) + o;
                                    u = this.n.mouse.y - l - 10
                                } else u = this.n.mouse.y + 22;
                                if ("number" == typeof u && (r.style.top = Math.max(u, 0) + "px"), this.n.mouse.clientX > c - s + o) {
                                    var f = this.n.mouse.clientX - (c - s) + o;
                                    r.style.left = this.n.mouse.x - f + "px"
                                } else r.style.left = this.n.mouse.x + "px"
                            } else if ("EventTop" === a) {
                                var v = 2,
                                    u = this.n.mouse.y - l - v,
                                    p = d.scrollTop;
                                u < p && (u = this.n.mouse.y + this.n.mouse.h + v), "number" == typeof u && (r.style.top = Math.max(u, 0) + "px");
                                var g = this.n.mouse.x;
                                this.n.mouse.x + s + o > c && (g = c - s - o), r.style.left = g + "px"
                            }
                            if (r.style.display = "", t) {
                                r.style.display = "";
                                var m = {};
                                m.color = r.firstChild.style.color, m.overflow = r.style.overflow, r.firstChild.style.color = "transparent", r.style.overflow = "hidden", this.removeShadow(), DayPilot.pop(r, {
                                    "finished": function() {
                                        r.firstChild.style.color = m.color, r.style.overflow = m.overflow, i.addShadow()
                                    },
                                    "vertical": "bottom",
                                    "horizontal": "left",
                                    "animation": i.animation
                                })
                            }
                        }
                    }, this.delayedHide = function() {
                        e.showing == this && e.cancelShowing();
                        var t = e.active;
                        if (t === this && (e.cancelHiding(), t.hideAfter > 0)) {
                            var i = t.hideAfter;
                            e.timeoutHide = window.setTimeout(e.hideActive, i)
                        }
                    }, this.showOnMouseOver = function(t) {
                        var n = function(e) {
                            return function() {
                                i.show(e)
                            }
                        };
                        clearTimeout(e.timeoutShow), e.timeoutShow = window.setTimeout(n(t), this.showAfter), e.showing = this
                    }, this.hideOnMouseOut = function() {
                        this.delayedHide()
                    }, this.r = function(e) {
                        return !!e.calendar.backendUrl || !("function" != typeof WebForm_DoCallback || !this.uniqueID)
                    }, this.addShadow = function() {
                        if (this.useShadow && !this.cssOnly && n.div) {
                            var e = n.div;
                            this.shadows && this.shadows.length > 0 && this.removeShadow(), this.shadows = [];
                            for (var t = 0; t < 5; t++) {
                                var i = document.createElement("div");
                                i.setAttribute("unselectable", "on"), i.style.position = "absolute", i.style.width = e.offsetWidth + "px", i.style.height = e.offsetHeight + "px", i.style.top = e.offsetTop + t + "px", i.style.left = e.offsetLeft + t + "px", i.style.zIndex = this.zIndex, i.style.filter = "alpha(opacity:10)", i.style.opacity = .1, i.style.backgroundColor = "#000000", this.rounded() && (i.style.MozBorderRadius = "5px", i.style.webkitBorderRadius = "5px", i.style.borderRadius = "5px"), document.body.appendChild(i), this.shadows.push(i)
                            }
                        }
                    }, this.removeShadow = function() {
                        if (this.shadows) {
                            for (var e = 0; e < this.shadows.length; e++) document.body.removeChild(this.shadows[e]);
                            this.shadows = []
                        }
                    }, this.removeDiv = function() {
                        n.div && (document.body.removeChild(n.div), n.div = null)
                    }, t)
                    for (var a in t) this[a] = t[a];
                this.init()
            }, DayPilot.Bubble.touchPosition = function(t) {
                t.touches && (e.mouse = e.touchPosition(t))
            }, e.cancelShowing = function() {
                e.timeoutShow && (window.clearTimeout(e.timeoutShow), e.timeoutShow = null, e.showing = null)
            }, e.cancelHiding = function() {
                e.timeoutHide && window.clearTimeout(e.timeoutHide)
            }, e.hideActive = function() {
                e.cancelHiding();
                var t = e.active;
                t && (t.removeDiv(), t.removeShadow()), e.active = null
            }, e.CallBackArgs = function(e, t, i, n) {
                this.calendar = e, this.type = t, this.object = i, this.staticHTML = n, this.toJSON = function() {
                    var e = {};
                    return e.uid = this.calendar.uniqueID, e.type = this.type, e.object = i, e
                }
            }, DayPilot.re(document, "mousemove", e.mouseMove), DayPilot.Bubble.hideActive = e.hideActive, DayPilot.Bubble.cancelShowing = e.cancelShowing, "undefined" != typeof Sys && Sys.Application && Sys.Application.notifyScriptLoaded && Sys.Application.notifyScriptLoaded()
        }
    }(), 
    
"undefined" == typeof DayPilot) var DayPilot = {};

if ("undefined" == typeof DayPilot.Global && (DayPilot.Global = {}), function() {
        if ("undefined" == typeof DayPilot.Calendar) {
            DayPilot.Calendar = function(t, i) {
                this.v = "2018.1.3151";
                var n = !1;
                if (this instanceof DayPilot.Calendar && !this.s && (n = !0, this.s = !0), !n) throw "DayPilot.Calendar() is a constructor and must be called as 'var c = new DayPilot.Calendar(id);'";
                var a = this;
                this.uniqueID = null, this.id = t, this.isCalendar = !0, this.api = 2, this.clientName = t, this.clientState = {}, this.t = {}, this.t.pixels = {}, this.t.events = [], this.elements = {}, this.elements.events = [], this.elements.separators = [], this.elements.selection = [], this.nav = {}, this.events = {}, this.hideUntilInit = !0, this.u = !0, this.allDayEnd = "DateTime", this.allDayEventHeight = 25, this.allowMultiSelect = !0, this.autoRefreshCommand = "refresh", this.autoRefreshEnabled = !1, this.autoRefreshInterval = 60, this.autoRefreshMaxCount = 20, this.backendUrl = null, this.borderColor = "#000000", "function" == typeof DayPilot.Bubble ? (this.bubble = new DayPilot.Bubble, this.cellBubble = new DayPilot.Bubble, this.columnBubble = new DayPilot.Bubble) : (this.bubble = null, this.cellBubble = null, this.columnBubble = null), this.businessBeginsHour = 9, this.businessEndsHour = 18, this.cellBackColor = "#FFFFD5", this.cellBackColorNonBusiness = "#FFF4BC", this.cellBorderColor = "#999999", this.cellHeight = 20, this.cellDuration = 30, this.columnMarginRight = 5, this.columns = null, this.columnWidth = 200, this.columnWidthSpec = "Auto", this.contextMenu = null, this.contextMenuSelection = null, this.cornerBackColor = "#ECE9D8", this.cornerHtml = "", this.crosshairColor = "Gray", this.crosshairOpacity = 20, this.crosshairType = "Header", this.cssOnly = !0, this.dayBeginsHour = 0, this.dayEndsHour = 24, this.days = 1, this.durationBarColor = "blue", this.durationBarVisible = !0, this.durationBarWidth = 5, this.durationBarImageUrl = null, this.eventArrangement = "SideBySide", this.eventBackColor = "#ffffff", this.eventBorderColor = "#000000", this.eventFontFamily = "Tahoma", this.eventFontSize = "8pt", this.eventFontColor = "#000000", this.eventSelectColor = "blue", this.eventsLoadMethod = "GET", this.headerFontSize = "10pt", this.headerFontFamily = "Tahoma", this.headerFontColor = "#000000", this.headerHeight = 20, this.headerLevels = 1, this.height = 300, this.heightSpec = "BusinessHours", this.hideFreeCells = !1, this.headerDateFormat = null, this.hourHalfBorderColor = "#F3E4B1", this.hourBorderColor = "#EAD098", this.hourFontColor = "#000000", this.hourFontFamily = "Tahoma", this.hourFontSize = "16pt", this.hourNameBackColor = "#ECE9D8", this.hourNameBorderColor = "#ACA899", this.hourWidth = 45, this.initScrollPos = null, this.loadingLabelText = "Loading...", this.loadingLabelVisible = !0, this.loadingLabelBackColor = "orange", this.loadingLabelFontColor = "#ffffff", this.loadingLabelFontFamily = "Tahoma", this.loadingLabelFontSize = "10pt", this.locale = "en-us", this.messageHideAfter = 5e3, this.moveBy = "Full", this.notifyCommit = "Immediate", this.roundedCorners = !1, this.rtl = !1, this.scrollLabelsVisible = !1, this.selectedColor = "#316AC5", this.shadow = "Fill", this.showToolTip = !0, this.showAllDayEvents = !1, this.showAllDayEventStartEnd = !0, this.showEventStartEnd = !1, this.showHeader = !0, this.showHours = !0, this.showCurrentTime = !0, this.showCurrentTimeMode = "Day", this.showCurrentTimeOffset = 0, this.startDate = DayPilot.Date.today(), this.cssClassPrefix = "calendar_default", this.tapAndHoldTimeout = 300, this.theme = null, this.timeFormat = "Auto", this.timeHeaderCellDuration = 60, this.timeRangeSelectingStartEndEnabled = !1, this.timeRangeSelectingStartEndFormat = "Auto", this.useEventBoxes = "Always", this.viewType = "Days", this.visible = !0, this.weekStarts = null, this.width = null, this.eventClickHandling = "Enabled", this.eventDoubleClickHandling = "Disabled", this.eventRightClickHandling = "ContextMenu", this.eventDeleteHandling = "Disabled", this.eventEditHandling = "Update", this.eventHoverHandling = "Bubble", this.eventResizeHandling = "Update", this.eventMoveHandling = "Update", this.eventSelectHandling = "Update", this.eventTapAndHoldHandling = "Move", this.headerClickHandling = "Enabled", this.timeRangeTapAndHoldHandling = "Select", this.timeRangeSelectedHandling = "Enabled", this.timeRangeDoubleClickHandling = "Disabled", this.timeRangeRightClickHandling = "ContextMenu", this.onTimeRangeRightClick = null, this.onTimeRangeRightClicked = null, this.onEventFilter = null, this.onBeforeEventRender = null, this.onBeforeEventExport = null, this.onBeforeCellExport = null, this.onBeforeHeaderRender = null, this.onBeforeHeaderExport = null, this.onBeforeTimeHeaderRender = null, this.z = !0, this.A = !1, this.B = 0, this.doubleClickTimeout = 300, this.members = {}, this.members.obsolete = ["Init", "cleanSelection", "cssClassPrefix", "cssOnly", "clientName", "uniqueID"], this.members.ignore = ["internal", "nav", "debug", "temp", "elements", "members"], this.members.noCssOnly = ["allDayEventBorderColor", "allDayEventFontColor", "allDayEventFontFamily", "allDayEventFontSize", "borderColor", "cellBackColor", "cellBackColorNonBusiness", "cellBorderColor", "cornerBackColor", "durationBarColor", "durationBarImageUrl", "eventBackColor", "eventBorderColor", "eventFontColor", "eventFontFamily", "eventFontSize", "eventSelectColor", "headerFontColor", "headerFontFamily", "headerFontSize", "hourBorderColor", "hourFontColor", "hourFontFamily", "hourFontSize", "hourHalfBorderColor", "hourNameBackColor", "hourNameBorderColor", "loadingLabelBackColor", "loadingLabelFontColor", "loadingLabelFontFamily", "loadingLabelFontSize", "roundedCorners", "selectedColor", "shadow", "useEventSelectionBars"], this.C = "javasc", this.D = {}, this.D.ie = navigator && navigator.userAgent && navigator.userAgent.indexOf("MSIE") !== -1, this.D.ie9 = navigator && navigator.userAgent && navigator.userAgent.indexOf("MSIE 9") !== -1, this.D.ielt9 = function() {
                    var e = document.createElement("div");
                    return e.innerHTML = "<!--[if lt IE 9]><i></i><![endif]-->", 1 === e.getElementsByTagName("i").length
                }(), this.D.ff = navigator && navigator.userAgent && navigator.userAgent.indexOf("Firefox") !== -1, this.clearSelection = function() {
                    return this.selectedCells ? (e.topSelectedCell = null, e.bottomSelectedCell = null, e.selecting = null, this.E(), void(this.selectedCells = [])) : void(this.selectedCells = [])
                }, this.E = function() {
                    this.selectedCells && ! function() {
                        DayPilot.de(a.elements.selection), a.elements.selection = []
                    }()
                }, this.cleanSelection = this.clearSelection, this.F = function(e, t, i) {
                    var n = {};
                    n.action = e, n.parameters = i, n.data = t, n.header = this.G();
                    var o = "JSON" + JSON.stringify(n);
                    __doPostBack(a.uniqueID, o)
                }, this.H = function(e, t, i, n) {
                    if (!this.r()) return void a.debug.message("Callback invoked without the server-side backend specified. Callback canceled.", "warning");
                    this.callbackTimeout && window.clearTimeout(this.callbackTimeout), "undefined" == typeof n && (n = "CallBack"), this.callbackTimeout = window.setTimeout(function() {
                        a.I()
                    }, 100);
                    var o = {};
                    o.action = e, o.type = n, o.parameters = t, o.data = i, o.header = this.G();
                    var r = "JSON" + JSON.stringify(o);
                    this.backendUrl ? DayPilot.request(this.backendUrl, this.J, r, this.K) : "function" == typeof WebForm_DoCallback && WebForm_DoCallback(this.uniqueID, r, this.L, this.clientName, this.onCallbackError, !0)
                }, this.r = function() {
                    return "javasc" !== a.C && a.C.indexOf("DCODE") === -1
                }, this.M = function() {
                    return !("function" != typeof WebForm_DoCallback || !this.uniqueID)
                }, this.K = function(e) {
                    if ("function" == typeof a.onAjaxError) {
                        var t = {};
                        t.request = e, a.onAjaxError(t)
                    } else "function" == typeof a.ajaxError && a.ajaxError(e)
                }, this.dispose = function() {
                    var t = a;
                    t.nav.top && (t.N(), t.O(), t.nav.messageClose && (t.nav.messageClose.onclick = null), t.nav.hourTable && (t.nav.hourTable.oncontextmenu = null), t.nav.hourTable && (t.nav.hourTable.onmousemove = null), t.nav.header && (t.nav.header.oncontextmenu = null), t.nav.corner && (t.nav.corner.oncontextmenu = null), t.nav.zoom.onmousemove = null, t.nav.zoom.oncontextmenu = null, t.nav.scroll.onscroll = null, t.nav.scroll.root = null, DayPilot.pu(t.nav.loading), t.P(), t.Q(), t.nav.select = null, t.nav.cornerRight = null, t.nav.scrollable = null, t.nav.bottomLeft = null, t.nav.bottomRight = null, t.nav.allday = null, t.nav.zoom = null, t.nav.loading = null, t.nav.events = null, t.nav.header = null, t.nav.hourTable = null, t.nav.scrolltop = null, t.nav.scroll = null, t.nav.vsph = null, t.nav.main = null, t.nav.message = null, t.nav.messageClose = null, t.nav.top.removeAttribute("style"), t.nav.top.removeAttribute("class"), t.nav.top.innerHTML = "", t.nav.top.dp = null, t.nav.top = null, DayPilot.ue(window, "resize", t.R), e.unregister(t))
                }, this.S = function() {
                    this.nav.top.dispose = this.dispose
                }, this.J = function(e) {
                    a.L(e.responseText)
                }, this.G = function() {
                    var e = {};
                    return e.v = this.v, e.control = "dpc", e.id = this.id, e.clientState = a.clientState, e.columns = this.T(), e.days = a.days, e.startDate = a.startDate, e.cellDuration = a.cellDuration, e.cssClassPrefix = a.cssClassPrefix, e.heightSpec = a.heightSpec, e.businessBeginsHour = a.businessBeginsHour, e.businessEndsHour = a.businessEndsHour, e.viewType = a.viewType, e.dayBeginsHour = a.dayBeginsHour, e.dayEndsHour = a.dayEndsHour, e.headerLevels = a.headerLevels, e.backColor = a.cellBackColor, e.nonBusinessBackColor = a.cellBackColorNonBusiness, e.eventHeaderVisible = a.eventHeaderVisible, e.timeFormat = a.timeFormat, e.timeHeaderCellDuration = a.timeHeaderCellDuration, e.locale = a.locale, e.scrollY = a.scrollPos, e.showAllDayEvents = a.showAllDayEvents, e.tagFields = a.tagFields, e.weekStarts = a.weekStarts, e.hourNameBackColor = a.hourNameBackColor, e.hourFontFamily = a.hourFontFamily, e.hourFontSize = a.hourFontSize, e.hourFontColor = a.hourFontColor, e.selected = a.multiselect.events(), e.hashes = a.hashes, e
                }, this.U = function() {
                    this.V(), DayPilot.Areas.hideAll()
                }, this.T = function() {
                    var e = [];
                    if (e.ignoreToJSON = !0, !this.W) return e;
                    for (var t = 0; t < this.W.length; t++) {
                        var i = this.W[t],
                            n = this.X(i);
                        e.push(n)
                    }
                    return e
                }, this.X = function(e) {
                    var t = {};
                    return t.Value = e.id, t.Name = e.name, t.ToolTip = e.toolTip, t.Date = e.start, t.Children = this.Y(e.children), t
                }, this.Y = function(e) {
                    var t = [];
                    if (t.ignoreToJSON = !0, !e) return t;
                    for (var i = 0; i < e.length; i++) t.push(this.X(e[i]));
                    return t
                }, this.L = function(e, t) {
                    var e = JSON.parse(e);
                    if (e.BubbleGuid) {
                        var i = e.BubbleGuid,
                            n = this.bubbles[i];
                        return delete this.bubbles[i], a.Z(), void("undefined" != typeof e.Result.BubbleHTML && n.updateView(e.Result.BubbleHTML, n))
                    }
                    if (e.CallBackRedirect) return void(document.location.href = e.CallBackRedirect);
                    if ("undefined" != typeof e.ClientState && (a.clientState = e.ClientState), "None" === e.UpdateType) return a.Z(), a._(e.CallBackData, !0), void(e.Message && a.message(e.Message));
                    if (e.VsUpdate) {
                        var o = document.createElement("input");
                        o.type = "hidden", o.name = a.id + "_vsupdate", o.id = o.name, o.value = e.VsUpdate, a.nav.vsph.innerHTML = "", a.nav.vsph.appendChild(o)
                    }
                    if (a.O(), a.multiselect.clear(!0), a.multiselect.aa = e.SelectedEvents, "undefined" != typeof e.TagFields && (a.tagFields = e.TagFields), "undefined" != typeof e.SortDirections && (a.sortDirections = e.SortDirections), "Full" === e.UpdateType && (a.colors = e.Colors, a.palette = e.Palette, a.dirtyColors = e.DirtyColors, a.cellProperties = e.CellProperties, a.cellConfig = e.CellConfig, a.columns = e.Columns, a.days = e.Days, a.startDate = new DayPilot.Date(e.StartDate).getDatePart(), a.cellDuration = e.CellDuration, a.heightSpec = e.HeightSpec ? e.HeightSpec : a.heightSpec, a.businessBeginsHour = e.BusinessBeginsHour ? e.BusinessBeginsHour : a.businessBeginsHour, a.businessEndsHour = e.BusinessEndsHour ? e.BusinessEndsHour : a.businessEndsHour, a.viewType = e.ViewType, a.headerLevels = e.HeaderLevels, a.backColor = e.BackColor ? e.BackColor : a.backColor, a.nonBusinessBackColor = e.NonBusinessBackColor ? e.NonBusinessBackColor : a.nonBusinessBackColor, a.eventHeaderVisible = e.EventHeaderVisible ? e.EventHeaderVisible : a.eventHeaderVisible, a.timeFormat = e.TimeFormat ? e.TimeFormat : a.timeFormat, a.timeHeaderCellDuration = "undefined" != typeof e.TimeHeaderCellDuration ? e.TimeHeaderCellDuration : a.timeHeaderCellDuration, a.locale = e.Locale ? e.Locale : a.locale, a.dayBeginsHour = "undefined" != typeof e.DayBeginsHour ? e.DayBeginsHour : a.dayBeginsHour, a.dayEndsHour = "undefined" != typeof e.DayEndsHour ? e.DayEndsHour : a.dayEndsHour, a.cornerBackColor = e.CornerBackColor, a.cornerHtml = e.CornerHTML, a.hours = e.Hours, a.ba(), a.ca()), e.Hashes)
                        for (var r in e.Hashes) a.hashes[r] = e.Hashes[r];
                    if (a.da(e.Events), a.ea(), ("Full" === e.UpdateType || a.hideFreeCells) && (a.fa(), a.ga(), a.ha(), a.ia(), a.ja(), a.ka(), a.la(), a.ma(), a.clearSelection(), a.na(), "undefined" != typeof e.ScrollY ? a.scrollToY(e.ScrollY) : a.scrollToY(a.initScrollPos)), a.oa(), a.pa(), a.qa(), a.ra(), "Parent100Pct" === a.heightSpec && a.R(), "HoldForever" !== a.timeRangeSelectedHandling && a.clearSelection(), a.sa(), a.todo && a.todo.del) {
                        var l = a.todo.del;
                        l.parentNode.removeChild(l), a.todo.del = null
                    }
                    a._(e.CallBackData, !0), a.Z(), a.ta(), e.Message && a.message(e.Message)
                }, this._ = function(e, t) {
                    var i = function(e, t) {
                        return function() {
                            if (a.va()) {
                                if ("function" == typeof a.onAfterRender) {
                                    var i = {};
                                    i.isCallBack = t, i.data = e, a.onAfterRender(i)
                                }
                            } else a.afterRender && a.afterRender(e, t)
                        }
                    };
                    window.setTimeout(i(e, t), 0)
                }, this.wa = function(e, t, i) {
                    var n = a.nav.events,
                        o = n.rows[0].cells.length,
                        r = n.clientWidth / o,
                        l = Math.floor(a.coords.x / r);
                    l < 0 && (l = 0), l >= o && (l = o - 1), a.rtl && (l = a.xa.length - l - 1);
                    var s = n.rows[0].cells[l],
                        d = 0,
                        c = 0,
                        h = 0;
                    if ("undefined" != typeof e.duration) {
                        var u = e.duration;
                        h = Math.floor((a.coords.y - d + a.cellHeight / 2) / a.cellHeight) * a.cellHeight + d, c = u * a.cellHeight / (60 * a.cellDuration), c = Math.max(c, a.cellHeight)
                    } else {
                        var f = e.event;
                        c = f.part.height, h = f.part.top
                    }
                    var v = document.createElement("div");
                    v.setAttribute("unselectable", "on"), v.style.position = "absolute", v.style.width = "100%", v.style.height = c + "px", v.style.left = "0px", v.style.top = h + "px", v.style.zIndex = 101, v.exclude = !0;
                    var p = document.createElement("div");
                    return v.appendChild(p), v.className = a.q("_shadow"), p.className = this.q("_shadow_inner"), s.events.appendChild(v), v
                }, this.ya = function() {
                    return this.za() / 36e5
                }, this.Aa = function() {
                    return this.businessBeginsHour > this.businessEndsHour ? 24 - this.businessBeginsHour + this.businessEndsHour : this.businessEndsHour - this.businessBeginsHour
                }, this.Ba = function() {
                    return this.dayBeginsHour >= this.dayEndsHour ? 24 - this.dayBeginsHour + this.dayEndsHour : this.dayEndsHour - this.dayBeginsHour
                }, this.za = function(e) {
                    var t = 0;
                    if ("BusinessHoursNoScroll" === this.heightSpec) t = this.Aa();
                    else if (this.hideFreeCells && !e) {
                        var i = (this.Ca - 1) * this.cellDuration / this.cellHeight,
                            n = Math.ceil(i / 60),
                            a = this.businessBeginsHour > this.businessEndsHour ? this.businessEndsHour + 24 : this.businessEndsHour;
                        t = Math.max(this.dayBeginsHour + n, a) - this.Da()
                    } else t = this.Ba();
                    return 60 * t * 60 * 1e3
                }, this.message = function(e, t, i, n) {
                    if (e) {
                        var o, t = t || this.messageHideAfter || 2e3,
                            r = this.Ea(),
                            l = this.showHours ? this.hourWidth : 0,
                            s = DayPilot.sw(a.Fa());
                        if (a.rtl) {
                            var d = l;
                            l = s, s = d
                        }
                        if (this.nav.message) o = a.nav.message, this.nav.message.style.top = r + "px";
                        else {
                            o = document.createElement("div"), o.style.position = "absolute", o.style.left = l + "px", o.style.top = r + "px", o.style.right = "0px", o.style.display = "none", o.onmousemove = function() {
                                o.messageTimeout && !o.status && clearTimeout(o.messageTimeout)
                            }, o.onmouseout = function() {
                                "none" !== a.nav.message.style.display && (o.messageTimeout = setTimeout(a.Ga, 500))
                            };
                            var c = document.createElement("div");
                            c.onclick = function() {
                                a.nav.message.style.display = "none"
                            }, c.className = this.q("_message"), o.appendChild(c);
                            var h = document.createElement("div");
                            h.style.position = "absolute", h.className = this.q("_message_close"), h.onclick = function() {
                                a.nav.message.style.display = "none"
                            }, o.appendChild(h), this.nav.top.insertBefore(o, this.nav.loading), this.nav.message = o, this.nav.messageClose = h
                        }
                        this.nav.cornerRight ? this.nav.message.style.right = s + "px" : this.nav.message.style.right = "0px";
                        var u = function() {
                            if (a.nav.message) {
                                a.nav.message.firstChild.innerHTML = e;
                                var i = function() {
                                    o.messageTimeout = setTimeout(a.Ga, t)
                                };
                                DayPilot.fade(a.nav.message, .2, i)
                            }
                        };
                        clearTimeout(o.messageTimeout), "none" !== this.nav.message.style.display ? DayPilot.fade(a.nav.message, -.2, u) : u()
                    }
                }, this.message.show = function(e) {
                    a.message(e)
                }, this.message.hide = function() {
                    a.Ga()
                }, this.Ga = function() {
                    var e = function() {
                        a.nav.message.style.display = "none"
                    };
                    DayPilot.fade(a.nav.message, -.2, e)
                }, this.ia = function() {
                    this.nav.message && (this.nav.message.style.top = this.Ea() + "px")
                }, this.Ha = function() {
                    return this.za() / (6e4 * this.cellDuration)
                }, this.eventClickPostBack = function(e, t) {
                    this.F("EventClick", t, e)
                }, this.eventClickCallBack = function(e, t) {
                    this.H("EventClick", e, t)
                }, this.Ia = function(e) {
                    var t = this,
                        e = e || window.event,
                        i = e.ctrlKey,
                        n = e.metaKey;
                    if ("undefined" != typeof DayPilot.Bubble && DayPilot.Bubble.hideActive(), "Disabled" === a.eventDoubleClickHandling) return void a.Ja(t, i, n, e);
                    if (a.timeouts) {
                        for (var o in a.timeouts) window.clearTimeout(a.timeouts[o]);
                        a.timeouts = []
                    } else a.timeouts = [];
                    var r = function(e, t, i, n) {
                        return function() {
                            a.Ja(e, t, i, n)
                        }
                    };
                    a.timeouts.push(window.setTimeout(r(this, i, n, e), a.doubleClickTimeout))
                }, this.Ja = function(e, t, i, n) {
                    var o = e.event;
                    if (o.client.clickEnabled())
                        if (a.va()) {
                            var r = {};
                            if (r.e = o, r.ctrl = t, r.meta = i, r.originalEvent = n, r.preventDefault = function() {
                                    this.preventDefault.value = !0
                                }, "function" == typeof a.onEventClick && (a.onEventClick(r), r.preventDefault.value)) return;
                            switch (a.eventClickHandling) {
                                case "PostBack":
                                    a.eventClickPostBack(o);
                                    break;
                                case "CallBack":
                                    a.eventClickCallBack(o);
                                    break;
                                case "Edit":
                                    a.Ka(e);
                                    break;
                                case "Select":
                                    a.La(e, o, t, i);
                                    break;
                                case "Bubble":
                                    a.bubble && a.bubble.showEvent(o);
                                    break;
                                case "ContextMenu":
                                    var l = o.client.contextMenu();
                                    l ? l.show(o) : a.contextMenu && a.contextMenu.show(o)
                            }
                            "function" == typeof a.onEventClicked && a.onEventClicked(r)
                        } else switch (a.eventClickHandling) {
                            case "PostBack":
                                a.eventClickPostBack(o);
                                break;
                            case "CallBack":
                                a.eventClickCallBack(o);
                                break;
                            case "JavaScript":
                                a.onEventClick(o);
                                break;
                            case "Edit":
                                a.Ka(e);
                                break;
                            case "Select":
                                a.La(e, o, t, i);
                                break;
                            case "Bubble":
                                a.bubble && a.bubble.showEvent(o);
                                break;
                            case "ContextMenu":
                                var l = o.client.contextMenu();
                                l ? l.show(o) : a.contextMenu && a.contextMenu.show(o)
                        }
                }, this.eventDoubleClickPostBack = function(e, t) {
                    this.F("EventDoubleClick", t, e)
                }, this.eventDoubleClickCallBack = function(e, t) {
                    this.H("EventDoubleClick", e, t)
                }, this.Ma = function(e) {
                    if ("undefined" != typeof DayPilot.Bubble && DayPilot.Bubble.hideActive(), a.timeouts) {
                        for (var t in a.timeouts) window.clearTimeout(a.timeouts[t]);
                        a.timeouts = null
                    }
                    var i = this.event,
                        e = e || window.event;
                    if (a.va()) {
                        var n = {};
                        if (n.e = i, n.preventDefault = function() {
                                this.preventDefault.value = !0
                            }, "function" == typeof a.onEventDoubleClick && (a.onEventDoubleClick(n), n.preventDefault.value)) return;
                        switch (a.eventDoubleClickHandling) {
                            case "PostBack":
                                a.eventDoubleClickPostBack(i);
                                break;
                            case "CallBack":
                                a.eventDoubleClickCallBack(i);
                                break;
                            case "Edit":
                                i.allday() || a.Ka(this);
                                break;
                            case "Select":
                                i.allday() || a.La(this, i, e.ctrlKey);
                                break;
                            case "Bubble":
                                a.bubble && a.bubble.showEvent(i)
                        }
                        "function" == typeof a.onEventDoubleClicked && a.onEventDoubleClicked(n)
                    } else switch (a.eventDoubleClickHandling) {
                        case "PostBack":
                            a.eventDoubleClickPostBack(i);
                            break;
                        case "CallBack":
                            a.eventDoubleClickCallBack(i);
                            break;
                        case "JavaScript":
                            a.onEventDoubleClick(i);
                            break;
                        case "Edit":
                            i.allday() || a.Ka(this);
                            break;
                        case "Select":
                            i.allday() || a.La(this, i, e.ctrlKey);
                            break;
                        case "Bubble":
                            a.bubble && a.bubble.showEvent(i)
                    }
                }, this.eventRightClickPostBack = function(e, t) {
                    this.F("EventRightClick", t, e)
                }, this.eventRightClickCallBack = function(e, t) {
                    this.H("EventRightClick", e, t)
                }, this.Na = function(e) {
                    var t = this.event;
                    if (e.stopPropagation && e.stopPropagation(), !t.client.rightClickEnabled()) return !1;
                    if (a.va()) {
                        var i = {};
                        if (i.e = t, i.preventDefault = function() {
                                this.preventDefault.value = !0
                            }, "function" == typeof a.onEventRightClick && (a.onEventRightClick(i), i.preventDefault.value)) return !1;
                        switch (a.eventRightClickHandling) {
                            case "PostBack":
                                a.eventRightClickPostBack(t);
                                break;
                            case "CallBack":
                                a.eventRightClickCallBack(t);
                                break;
                            case "ContextMenu":
                                var n = t.client.contextMenu();
                                n ? n.show(t) : a.contextMenu && a.contextMenu.show(this.event);
                                break;
                            case "Bubble":
                                a.bubble && a.bubble.showEvent(t)
                        }
                        "function" == typeof a.onEventRightClicked && a.onEventRightClicked(i)
                    } else switch (a.eventRightClickHandling) {
                        case "PostBack":
                            a.eventRightClickPostBack(t);
                            break;
                        case "CallBack":
                            a.eventRightClickCallBack(t);
                            break;
                        case "JavaScript":
                            a.onEventRightClick(t);
                            break;
                        case "ContextMenu":
                            var n = t.client.contextMenu();
                            n ? n.show(t) : a.contextMenu && a.contextMenu.show(this.event);
                            break;
                        case "Bubble":
                            a.bubble && a.bubble.showEvent(t)
                    }
                    return e.preventDefault && e.preventDefault(), !1
                }, this.headerClickPostBack = function(e, t) {
                    this.F("HeaderClick", t, e)
                }, this.headerClickCallBack = function(e, t) {
                    this.H("HeaderClick", e, t)
                }, this.Oa = function(t) {
                    var i = this.data,
                        n = new e.Column(i.id, i.name, i.start);
                    if (a.va()) {
                        var o = {};
                        if (o.header = {}, o.header.id = i.id, o.header.name = i.name, o.header.start = i.start, o.preventDefault = function() {
                                this.preventDefault.value = !0
                            }, "function" == typeof a.onHeaderClick && (a.onHeaderClick(o), o.preventDefault.value)) return;
                        switch (a.headerClickHandling) {
                            case "PostBack":
                                a.headerClickPostBack(n);
                                break;
                            case "CallBack":
                                a.headerClickCallBack(n)
                        }
                        "function" == typeof a.onHeaderClicked && a.onHeaderClicked(o)
                    } else switch (a.headerClickHandling) {
                        case "PostBack":
                            a.headerClickPostBack(n);
                            break;
                        case "CallBack":
                            a.headerClickCallBack(n);
                            break;
                        case "JavaScript":
                            a.onHeaderClick(n)
                    }
                }, this.Pa = function() {
                    if ("undefined" != typeof DayPilot.Bubble && a.columnBubble)
                        if ("Resources" === a.viewType) {
                            var t = {};
                            t.calendar = a, t.id = this.data.id, t.toJSON = function() {
                                var e = {};
                                return e.id = this.id, e
                            }, a.columnBubble.showResource(t)
                        } else {
                            var i = new DayPilot.Date(this.data.start),
                                n = i.addDays(1),
                                o = {};
                            o.calendar = a, o.start = i, o.end = n, o.toJSON = function() {
                                var e = {};
                                return e.start = this.start, e.end = this.end, e
                            }, a.columnBubble.showTime(o)
                        } var r = this,
                        l = r.firstChild;
                    if (!l.active) {
                        var s = r.data,
                            d = new e.Column(s.id, s.name, s.start);
                        d.areas = r.data.areas, DayPilot.Areas.showAreas(l, d)
                    }
                }, this.Qa = function(e) {
                    "undefined" != typeof DayPilot.Bubble && a.columnBubble && a.columnBubble.hideOnMouseOut(), DayPilot.Areas.hideAreas(this.firstChild, e)
                }, this.eventDeletePostBack = function(e, t) {
                    this.F("EventDelete", t, e)
                }, this.eventDeleteCallBack = function(e, t) {
                    this.H("EventDelete", e, t)
                };
                this.Ra = function(e) {
                    var t;
                    if (t = e && e.isEvent ? e : e.parentNode.parentNode.event, a.va()) {
                        var i = {};
                        if (i.e = t, i.preventDefault = function() {
                                this.preventDefault.value = !0
                            }, "function" == typeof a.onEventDelete && (a.onEventDelete(i), i.preventDefault.value)) return;
                        switch (a.eventDeleteHandling) {
                            case "PostBack":
                                a.eventDeletePostBack(t);
                                break;
                            case "CallBack":
                                a.eventDeleteCallBack(t);
                                break;
                            case "Update":
                                a.events.remove(t)
                        }
                        "function" == typeof a.onEventDeleted && a.onEventDeleted(i)
                    } else switch (a.eventDeleteHandling) {
                        case "PostBack":
                            a.eventDeletePostBack(t);
                            break;
                        case "CallBack":
                            a.eventDeleteCallBack(t);
                            break;
                        case "JavaScript":
                            a.onEventDelete(t);
                    }
                };
                this.eventResizePostBack = function(e, t, i, n) {
                    if (!t) throw "newStart is null";
                    if (!i) throw "newEnd is null";
                    var a = {};
                    a.e = e, a.newStart = t, a.newEnd = i, this.F("EventResize", n, a)
                }, this.eventResizeCallBack = function(e, t, i, n) {
                    if (!t) throw "newStart is null";
                    if (!i) throw "newEnd is null";
                    var a = {};
                    a.e = e, a.newStart = t, a.newEnd = i, this.H("EventResize", a, n)
                }, this.Sa = function(e, t, i, n) {
                    if ("PostBack" === e) a.postBack2(t, i, n);
                    else if ("CallBack" === e) a.H(t, i, n, "CallBack");
                    else if ("Immediate" === e) a.H(t, i, n, "Notify");
                    else if ("Queue" === e) a.queue.add(new DayPilot.Action(this, t, i, n));
                    else {
                        if ("Notify" !== e) throw "Invalid event invocation type";
                        "Notify" === r.notifyType() ? a.H(t, i, n, "Notify") : a.queue.add(new DayPilot.Action(a, t, i, n))
                    }
                }, this.Da = function(e) {
                    if ("BusinessHoursNoScroll" === this.heightSpec) return this.businessBeginsHour;
                    if (this.hideFreeCells && !e) {
                        var t = this.Ta * this.cellDuration / this.cellHeight,
                            i = Math.floor(t / 60);
                        return i = Math.max(0, i), Math.min(this.dayBeginsHour + i, this.businessBeginsHour)
                    }
                    return this.dayBeginsHour
                }, this.visibleStart = function() {
                    if ("Resources" === a.viewType) {
                        var e = DayPilot.list(a.xa).map(function(e) {
                                return e.start.getTime()
                            }),
                            t = Math.min.apply(null, e);
                        return new DayPilot.Date(t)
                    }
                    return this.xa[0].start
                }, this.visibleEnd = function() {
                    if ("Resources" === a.viewType) {
                        var e = DayPilot.list(a.xa).map(function(e) {
                                return e.start.getTime()
                            }),
                            t = Math.max.apply(null, e);
                        return new DayPilot.Date(t).addDays(1)
                    }
                    var i = this.xa,
                        t = i.length - 1;
                    return i[t].start.addDays(1)
                }, this.va = function() {
                    return 2 === a.api
                }, this.Ua = function(e, t, i, n) {
                    if ("Disabled" !== this.eventResizeHandling) {
                        var o = 0,
                            r = new Date,
                            l = new Date,
                            s = e.start(),
                            d = (e.end(), a.cellDuration),
                            c = a.xa[e.part.dayIndex].start;
                        if ("top" === n) {
                            var c = s.getDatePart(),
                                h = Math.floor((i - o) / a.cellHeight),
                                u = h * d,
                                f = 60 * u * 1e3,
                                v = 60 * a.Da() * 60 * 1e3;
                            r = c.addTime(f + v), l = e.end()
                        } else if ("bottom" === n) {
                            var h = Math.floor((i + t - o) / a.cellHeight),
                                u = h * d,
                                f = 60 * u * 1e3,
                                v = 60 * a.Da() * 60 * 1e3;
                            r = s, l = c.addTime(f + v)
                        }
                        if (a.va()) {
                            var p = {};
                            if (p.e = e, p.newStart = r, p.newEnd = l, p.preventDefault = function() {
                                    this.preventDefault.value = !0
                                }, "function" == typeof a.onEventResize && (a.onEventResize(p), p.preventDefault.value)) return;
                            switch (a.eventResizeHandling) {
                                case "PostBack":
                                    a.eventResizePostBack(e, r, l);
                                    break;
                                case "CallBack":
                                    a.eventResizeCallBack(e, r, l);
                                    break;
                                case "Notify":
                                    a.eventResizeNotify(e, r, l);
                                    break;
                                case "Update":
                                    e.start(r), e.end(l), a.events.update(e)
                            }
                            "function" == typeof a.onEventResized && a.onEventResized(p)
                        } else switch (a.eventResizeHandling) {
                            case "PostBack":
                                a.eventResizePostBack(e, r, l);
                                break;
                            case "CallBack":
                                a.eventResizeCallBack(e, r, l);
                                break;
                            case "JavaScript":
                                a.onEventResize(e, r, l);
                                break;
                            case "Notify":
                                a.eventResizeNotify(e, r, l)
                        }
                    }
                }, this.eventResizeNotify = function(e, t, i, n) {
                    var o = new DayPilot.Event(e.copy(), this);
                    e.start(t), e.end(i), e.commit(), a.update(), this.Va("Notify", o, t, i, n)
                }, this.Va = function(e, t, i, n, a) {
                    var o = {};
                    o.e = t, o.newStart = i, o.newEnd = n, this.Sa(e, "EventResize", o, a)
                }, this.eventMovePostBack = function(e, t, i, n, a) {
                    if (!t) throw "newStart is null";
                    if (!i) throw "newEnd is null";
                    var o = {};
                    o.e = e, o.newStart = t, o.newEnd = i, o.newResource = n, this.F("EventMove", a, o)
                }, this.eventMoveCallBack = function(e, t, i, n, a) {
                    if (!t) throw "newStart is null";
                    if (!i) throw "newEnd is null";
                    var o = {};
                    o.e = e, o.newStart = t, o.newEnd = i, o.newResource = n, this.H("EventMove", o, a)
                }, this.Wa = function(e, t, i) {
                    var n = 0,
                        o = Math.floor((i - n) / a.cellHeight),
                        r = a.cellDuration,
                        l = o * r * 60 * 1e3,
                        s = e.start(),
                        d = e.end(),
                        c = new Date;
                    s instanceof DayPilot.Date && (s = s.toDate()), c.setTime(Date.UTC(s.getUTCFullYear(), s.getUTCMonth(), s.getUTCDate()));
                    var h = "Never" !== a.useEventBoxes ? s.getTime() - (c.getTime() + 3600 * s.getUTCHours() * 1e3 + Math.floor(s.getUTCMinutes() / r) * r * 60 * 1e3) : 0,
                        u = d.getTime() - s.getTime(),
                        f = 3600 * a.Da() * 1e3;
                    "undefined" == typeof t && (t = e.part.dayIndex);
                    var v = this.xa[t],
                        p = v.start.getTime(),
                        g = new Date;
                    g.setTime(p + l + h + f);
                    var m = new DayPilot.Date(g);
                    return {
                        "start": m,
                        "end": m.addTime(u),
                        "resource": v.id
                    }
                }, this.Xa = function(e, t, i, n, o) {
                    if ("Disabled" !== a.eventMoveHandling) {
                        var r = a.Wa(e, t, i),
                            l = r.start,
                            s = r.end,
                            d = r.resource,
                            c = !!o;
                        if (a.va()) {
                            var h = {};
                            if (h.e = e, h.newStart = r.start, h.newEnd = r.end, h.newResource = r.resource, h.external = c, h.ctrl = !1, n && (h.ctrl = n.ctrlKey), h.shift = !1, n && (h.shift = n.shiftKey), h.preventDefault = function() {
                                    this.preventDefault.value = !0
                                }, "function" == typeof a.onEventMove && (a.onEventMove(h), h.preventDefault.value)) return;
                            switch (a.eventMoveHandling) {
                                case "PostBack":
                                    a.eventMovePostBack(e, l, s, d);
                                    break;
                                case "CallBack":
                                    a.eventMoveCallBack(e, l, s, d);
                                    break;
                                case "Notify":
                                    a.eventMoveNotify(e, l, s, d);
                                    break;
                                case "Update":
                                    e.start(l), e.end(s), e.resource(d), c ? (e.commit(), a.events.add(e)) : a.events.update(e), a.Ya()
                            }
                            "function" == typeof a.onEventMoved && a.onEventMoved(h)
                        } else switch (a.eventMoveHandling) {
                            case "PostBack":
                                a.eventMovePostBack(e, l, s, d);
                                break;
                            case "CallBack":
                                a.eventMoveCallBack(e, l, s, d);
                                break;
                            case "JavaScript":
                                a.onEventMove(e, l, s, d, c, !!n && n.ctrlKey, !!n && n.shiftKey);
                                break;
                            case "Notify":
                                a.eventMoveNotify(e, l, s, d, null)
                        }
                    }
                }, this.eventMoveNotify = function(e, t, i, n, o) {
                    var r = new DayPilot.Event(e.copy(), this);
                    e.start(t), e.end(i), e.resource(n), e.commit(), a.update(), this.Za("Notify", r, t, i, n, o)
                }, this.Za = function(e, t, i, n, a, o) {
                    var r = {};
                    r.e = t, r.newStart = i, r.newEnd = n, r.newResource = a, this.Sa(e, "EventMove", r, o)
                }, this.Ya = function() {
                    if (a.todo && a.todo.del) {
                        var e = a.todo.del;
                        e.parentNode.removeChild(e), a.todo.del = null
                    }
                }, this.$a = function(e, t) {
                    var i = a._a(t),
                        n = {};
                    n.args = e, n.guid = i, a.H("Bubble", n)
                }, this._a = function(e) {
                    var t = DayPilot.guid();
                    return this.bubbles || (this.bubbles = []), this.bubbles[t] = e, t
                }, this.eventMenuClickPostBack = function(e, t, i) {
                    var n = {};
                    n.e = e, n.command = t, this.F("EventMenuClick", i, n)
                }, this.eventMenuClickCallBack = function(e, t, i) {
                    var n = {};
                    n.e = e, n.command = t, this.H("EventMenuClick", n, i)
                }, this.ab = function(e, t, i) {
                    switch (i) {
                        case "PostBack":
                            a.eventMenuClickPostBack(t, e);
                            break;
                        case "CallBack":
                            a.eventMenuClickCallBack(t, e)
                    }
                }, this.timeRangeMenuClickPostBack = function(e, t, i) {
                    var n = {};
                    n.selection = e, n.command = t, this.F("TimeRangeMenuClick", i, n)
                }, this.timeRangeMenuClickCallBack = function(e, t, i) {
                    var n = {};
                    n.selection = e, n.command = t, this.H("TimeRangeMenuClick", n, i)
                }, this.bb = function(e, t, i) {
                    switch (i) {
                        case "PostBack":
                            a.timeRangeMenuClickPostBack(t, e);
                            break;
                        case "CallBack":
                            a.timeRangeMenuClickCallBack(t, e)
                    }
                }, this.timeRangeSelectedPostBack = function(e, t, i, n) {
                    var a = {};
                    a.start = e, a.end = t, a.resource = i, this.F("TimeRangeSelected", n, a)
                }, this.timeRangeSelectedCallBack = function(e, t, i, n) {
                    var a = {};
                    a.start = e, a.end = t, a.resource = i, this.H("TimeRangeSelected", a, n)
                }, this.cb = function(e, t, i) {
                    e = new DayPilot.Date(e), t = new DayPilot.Date(t);
                    var n = i;
                    if (a.va()) {
                        var o = {};
                        if (o.start = e, o.end = t, o.resource = n, o.control = a, o.preventDefault = function() {
                                this.preventDefault.value = !0
                            }, "function" == typeof a.onTimeRangeSelect && (a.onTimeRangeSelect(o), o.preventDefault.value)) return;
                        switch (a.timeRangeSelectedHandling) {
                            case "PostBack":
                                a.timeRangeSelectedPostBack(e, t, n);
                                break;
                            case "CallBack":
                                a.timeRangeSelectedCallBack(e, t, n)
                        }
                        "function" == typeof a.onTimeRangeSelected && a.onTimeRangeSelected(o)
                    } else switch (a.timeRangeSelectedHandling) {
                        case "PostBack":
                            a.timeRangeSelectedPostBack(e, t, i);
                            break;
                        case "CallBack":
                            a.timeRangeSelectedCallBack(e, t, i);
                            break;
                        case "JavaScript":
                            a.onTimeRangeSelected(e, t, i)
                    }
                }, this.timeRangeDoubleClickPostBack = function(e, t, i, n) {
                    var a = {};
                    a.start = e, a.end = t, a.resource = i, this.F("TimeRangeDoubleClick", n, a)
                }, this.timeRangeDoubleClickCallBack = function(e, t, i, n) {
                    var a = {};
                    a.start = e, a.end = t, a.resource = i, this.H("TimeRangeDoubleClick", a, n)
                }, this.db = function(e, t, i) {
                    if (a.va()) {
                        var n = i,
                            o = {};
                        if (o.start = e, o.end = t, o.resource = n, o.preventDefault = function() {
                                this.preventDefault.value = !0
                            }, "function" == typeof a.onTimeRangeDoubleClick && (a.onTimeRangeDoubleClick(o), o.preventDefault.value)) return;
                        switch (a.timeRangeDoubleClickHandling) {
                            case "PostBack":
                                a.timeRangeDoubleClickPostBack(e, t, n);
                                break;
                            case "CallBack":
                                a.timeRangeDoubleClickCallBack(e, t, n)
                        }
                        "function" == typeof a.onTimeRangeDoubleClicked && a.onTimeRangeDoubleClicked(o)
                    } else switch (a.timeRangeDoubleClickHandling) {
                        case "PostBack":
                            a.timeRangeDoubleClickPostBack(e, t, i);
                            break;
                        case "CallBack":
                            a.timeRangeDoubleClickCallBack(e, t, i);
                            break;
                        case "JavaScript":
                            a.onTimeRangeDoubleClick(e, t, i)
                    }
                }, this.eventEditPostBack = function(e, t, i) {
                    var n = {};
                    n.e = e, n.newText = t, this.F("EventEdit", i, n)
                }, this.eventEditCallBack = function(e, t, i) {
                    var n = {};
                    n.e = e, n.newText = t, this.H("EventEdit", n, i)
                }, this.eb = function(e, t) {
                    if (a.va()) {
                        var i = {};
                        if (i.e = e, i.newText = t, i.preventDefault = function() {
                                this.preventDefault.value = !0
                            }, "function" == typeof a.onEventEdit && (a.onEventEdit(i), i.preventDefault.value)) return;
                        switch (a.eventEditHandling) {
                            case "PostBack":
                                a.eventEditPostBack(e, t);
                                break;
                            case "CallBack":
                                a.eventEditCallBack(e, t);
                                break;
                            case "Update":
                                e.text(t), a.events.update(e)
                        }
                        if ("function" == typeof a.onEventEdited && (a.onEventEdited(i), i.preventDefault.value)) return
                    } else switch (a.eventEditHandling) {
                        case "PostBack":
                            a.eventEditPostBack(e, t);
                            break;
                        case "CallBack":
                            a.eventEditCallBack(e, t);
                            break;
                        case "JavaScript":
                            a.onEventEdit(e, t)
                    }
                }, this.eventSelectPostBack = function(e, t, i) {
                    var n = {};
                    n.e = e, n.change = t, this.F("EventSelect", i, n)
                }, this.eventSelectCallBack = function(e, t, i) {
                    var n = {};
                    n.e = e, n.change = t, this.H("EventSelect", n, i)
                }, this.fb = function(e, t, i, n) {
                    var o = a.multiselect,
                        r = o.isSelected(t);
                    if (i || n || !r || 1 !== o.gb.length)
                        if (a.va()) {
                            o.previous = o.events();
                            var l = {};
                            if (l.e = t, l.selected = o.isSelected(t), l.ctrl = i, l.meta = n, l.preventDefault = function() {
                                    this.preventDefault.value = !0
                                }, "function" == typeof a.onEventSelect && (a.onEventSelect(l), l.preventDefault.value)) return;
                            switch (a.eventSelectHandling) {
                                case "PostBack":
                                    a.eventSelectPostBack(t, s);
                                    break;
                                case "CallBack":
                                    "undefined" != typeof WebForm_InitCallback && (window.hb = "", window.ib = [], WebForm_InitCallback()), a.eventSelectCallBack(t, s);
                                    break;
                                case "Update":
                                    o.jb(e, i)
                            }
                            "function" == typeof a.onEventSelected && (l.change = o.isSelected(t) ? "selected" : "deselected", l.selected = o.isSelected(t), a.onEventSelected(l))
                        } else {
                            o.previous = o.events(), o.jb(e, i);
                            var s = o.isSelected(t) ? "selected" : "deselected";
                            switch (a.eventSelectHandling) {
                                case "PostBack":
                                    a.eventSelectPostBack(t, s);
                                    break;
                                case "CallBack":
                                    "undefined" != typeof WebForm_InitCallback && (window.hb = "", window.ib = [], WebForm_InitCallback()), a.eventSelectCallBack(t, s);
                                    break;
                                case "JavaScript":
                                    a.onEventSelect(t, s)
                            }
                        }
                }, this.commandCallBack = function(e, t) {
                    this.N();
                    var i = {};
                    i.command = e, this.H("Command", i, t)
                }, this.kb = function(t) {
                    if (!o.active && !o.using && (clearTimeout(e.selectedTimeout), !e.selecting)) {
                        if (e.editing) return void e.editing.blur();
                        if (a.selectedCells && "Disabled" !== a.timeRangeDoubleClickHandling)
                            for (var i = 0; i < a.selectedCells.length; i++)
                                if (this === a.selectedCells[i]) return;
                        if ("Disabled" !== a.timeRangeSelectedHandling) {
                            DayPilot.Util.mouseButton(t).left && (e.firstMousePos = a.coords, e.firstMousePos.calendar = a, a.clearSelection(), e.topSelectedCell = this, e.bottomSelectedCell = this, e.column = e.getColumn(this), a.selectedCells.push(this), e.firstSelected = this, e.selecting = {
                                "calendar": a
                            }, a.lb())
                        }
                    }
                }, this.mb = function(e) {
                    var e = $("#touch").html() + "<br>" + new DayPilot.Date + " " + e;
                    $("#touch").html(e)
                }, this.lb = function() {
                    this.getSelection() && a.selectedCells && ! function() {
                        var t = e.topSelectedCell,
                            i = e.bottomSelectedCell,
                            n = t.parentNode.cells,
                            o = function() {
                                for (var e = 0; e < n.length; e++)
                                    if (n[e] === t) return e;
                                return -1
                            }(),
                            l = a.timeRangeSelectingStartEndFormat;
                        "Auto" === l && (l = "Clock24Hours" === r.timeFormat() ? "H:mm" : "h:mm tt");
                        var s = a.xa[o],
                            d = {};
                        if (d.start = t.start, d.end = i.end, d.duration = new DayPilot.Duration(d.start, d.end), d.resource = s.id, d.allowed = !0, d.html = null, d.cssClass = null, d.top = {}, d.top.width = null, d.top.space = 5, d.top.html = d.start.toString(l, r.locale()), d.top.enabled = a.timeRangeSelectingStartEndEnabled, d.bottom = {}, d.bottom.width = null, d.bottom.space = 5, d.bottom.html = d.end.toString(l, r.locale()), d.bottom.enabled = a.timeRangeSelectingStartEndEnabled, e.selecting) {
                            var c = e.selecting.args;
                            if (!c || c.start !== d.start || c.end !== d.end || c.resource != d.resource) {
                                "function" == typeof a.onTimeRangeSelecting && a.onTimeRangeSelecting(d), DayPilot.de(a.elements.selection), a.elements.selection = [], e.selecting.args = d;
                                var h = a.nb(t.start, s.start).boxTop,
                                    u = a.nb(i.end, s.start).boxBottom,
                                    f = u - h,
                                    v = document.createElement("div");
                                v.style.position = "absolute", v.style.top = h + "px", v.style.height = f + "px", v.style.left = "0px", v.style.width = "100%", v.className = a.q("_shadow");
                                var p = document.createElement("div");
                                if (p.className = a.q("_shadow_inner"), d.html && (p.innerHTML = d.html), d.cssClass && DayPilot.Util.addClass(v, d.cssClass), !d.allowed) {
                                    var g = a.q("_shadow_forbidden");
                                    DayPilot.Util.addClass(v, g)
                                }
                                v.appendChild(p), a.nav.events.rows[0].cells[o].selection.appendChild(v), a.elements.selection.push(v), a.ob(v, o, d)
                            }
                        }
                    }()
                }, this.ob = function(e, t, i) {
                    var n = a.nav.events.rows[0].cells,
                        o = n[t].selection,
                        r = t === n.length - 1;
                    if (i.top.enabled) {
                        var l = document.createElement("div");
                        l.style.position = "absolute", l.style.top = e.offsetTop + "px", r ? l.style.right = "0px" : l.style.left = e.offsetWidth + i.top.space + "px", i.top.width ? l.style.width = i.top.width + "px" : l.style.whiteSpace = "nowrap", i.top.html && (l.innerHTML = i.top.html), l.className = a.q("_shadow_top"), o.appendChild(l), a.elements.selection.push(l)
                    }
                    if (i.bottom.enabled) {
                        var s = document.createElement("div");
                        s.style.position = "absolute", s.style.top = e.offsetTop + e.offsetHeight + "px", r ? s.style.right = "0px" : s.style.left = e.offsetWidth + i.bottom.space + "px", i.bottom.width ? s.style.width = i.bottom.width + "px" : s.style.whiteSpace = "nowrap", i.bottom.html && (s.innerHTML = i.bottom.html), s.className = a.q("_shadow_bottom"), o.appendChild(s);
                        var d = s.offsetHeight;
                        s.style.top = e.offsetTop + e.offsetHeight - d + "px", a.elements.selection.push(s)
                    }
                }, this.pb = function(e) {
                    "undefined" != typeof DayPilot.Bubble && a.cellBubble && a.cellBubble.hideOnMouseOut()
                }, this.qb = function(t) {
                    if ("undefined" != typeof e && !o.active && !o.using && "undefined" != typeof DayPilot.Bubble && a.cellBubble) {
                        var i = e.getColumn(this),
                            n = a.xa[i].id,
                            r = {};
                        r.calendar = a, r.start = this.start, r.end = this.end, r.resource = n, r.toJSON = function() {
                            var e = {};
                            return e.start = this.start, e.end = this.end, e.resource = this.resource, e
                        }, a.cellBubble.showCell(r)
                    }
                }, this.getSelection = function() {
                    if (!e.topSelectedCell) return null;
                    if (!e.bottomSelectedCell) return null;
                    var t = e.topSelectedCell.start,
                        i = e.bottomSelectedCell.end,
                        n = e.topSelectedCell.resource;
                    return new DayPilot.Selection(t, i, n, a)
                }, this.rb = function(e) {}, this.sb = function(e) {
                    if ("Fixed" === a.columnWidthSpec || a.tb) {
                        if (!a.nav.bottomLeft) return;
                        a.nav.bottomLeft.scrollTop = a.nav.bottomRight.scrollTop, a.nav.upperRight && (a.nav.upperRight.scrollLeft = a.nav.bottomRight.scrollLeft), a.nav.scrollLayer.scrollLeft = a.nav.bottomRight.scrollLeft
                    }
                    var t = a.ub();
                    a.scrollPos = t.scrollTop, a.vb = t.clientHeight, a.nav.scrollpos.value = a.scrollPos, a.sa()
                }, this.sa = function() {
                    if (this.scrollLabelsVisible && this.wb) {
                        if (this.nav && this.nav.main && this.nav.main.rows && this.nav.main.rows.length > 0 && this.nav.main.rows[0].cells.length > 0) {
                            for (var e = (this.xa, this.showHours ? this.hourWidth : 0), t = this.nav.main.rows[0].cells[0].clientWidth, i = 1, n = 0; n < this.nav.scrollUp.length; n++) {
                                var o = this.nav.scrollUp[n],
                                    r = this.nav.scrollDown[n],
                                    l = Math.floor(e + n * t + t / 2 - 5 + i);
                                l < 0 && (l = 0), l -= a.nav.bottomRight.scrollLeft, o && o.style && (o.style.left = l + "px"), r && r.style && (r.style.left = l + "px")
                            }
                            for (var s = this.xb(), n = 0; n < this.nav.scrollUp.length; n++) {
                                var d = this.nav.scrollUp[n],
                                    c = this.nav.scrollDown[n],
                                    h = this.wb[n].minEnd - s,
                                    u = this.wb[n].maxStart - s;
                                d && c && (h <= a.scrollPos && parseInt(d.style.left) >= a.hourWidth ? (d.style.top = this.Ea() + "px", d.style.display = "") : d.style.display = "none", u >= a.scrollPos + a.vb && parseInt(c.style.left) >= a.hourWidth ? (c.style.top = this.Ea() + this.vb - 10 + "px", c.style.display = "") : c.style.display = "none")
                            }
                        }
                    }
                }, this.yb = function(e) {
                    var t = e.parentNode,
                        i = 0,
                        n = e.offsetTop,
                        a = e.parentNode.offsetWidth,
                        o = e.offsetHeight;
                    e.event.allday() && (i = e.offsetLeft, a = e.offsetWidth);
                    var r = document.createElement("textarea");
                    r.style.boxSizing = "border-box", r.style.position = "absolute", r.style.width = a + "px", r.style.height = o + "px";
                    var l = DayPilot.gs(e, "fontFamily");
                    l || (l = DayPilot.gs(e, "font-family")), r.style.fontFamily = l;
                    var s = DayPilot.gs(e, "fontSize");
                    return s || (s = DayPilot.gs(e, "font-size")), r.style.fontSize = s, r.style.left = i + "px", r.style.top = n + "px", r.style.border = "1px solid black", r.style.padding = "0px", r.style.marginTop = "0px", r.style.backgroundColor = "white", r.value = DayPilot.tr(e.event.text()), r.event = e.event, t.appendChild(r), r
                }, this.La = function(e, t, i, n) {
                    a.fb(e, t, i, n)
                }, this.multiselect = {}, this.multiselect.aa = [], this.multiselect.gb = [], this.multiselect.zb = [], this.multiselect.Ab = [], this.multiselect.Bb = function() {
                    var e = a.multiselect;
                    return JSON.stringify(e.events())
                }, this.multiselect.events = function() {
                    var e = a.multiselect,
                        t = [];
                    t.ignoreToJSON = !0;
                    for (var i = 0; i < e.gb.length; i++) t.push(e.gb[i]);
                    return t
                }, this.multiselect.Cb = function() {
                    a.nav.select.value = a.multiselect.Bb()
                }, this.multiselect.jb = function(e, t) {
                    var i = a.multiselect;
                    if (i.isSelected(e.event))
                        if (a.allowMultiSelect)
                            if (t) i.remove(e.event, !0);
                            else {
                                var n = i.gb.length;
                                i.clear(!0), n > 1 && i.add(e.event, !0)
                            }
                    else i.clear(!0);
                    else a.allowMultiSelect && t ? i.add(e.event, !0) : (i.clear(!0), i.add(e.event, !0));
                    i.redraw(), i.Cb()
                }, this.multiselect.Db = function(e) {
                    var t = a.multiselect;
                    return t.Eb(e, t.aa)
                }, this.multiselect.Fb = function() {
                    for (var e = a.multiselect, t = [], i = 0; i < e.gb.length; i++) {
                        var n = e.gb[i];
                        t.push(n.value())
                    }
                    alert(t.join("\n"))
                }, this.multiselect.add = function(e, t) {
                    var i = a.multiselect;
                    i.Gb(e) === -1 && i.gb.push(e), i.Cb(), t || i.redraw()
                }, this.multiselect.remove = function(e, t) {
                    var i = a.multiselect,
                        n = i.Gb(e);
                    n !== -1 && i.gb.splice(n, 1), i.Cb(), t || i.redraw()
                }, this.multiselect.clear = function(e) {
                    var t = a.multiselect;
                    t.gb = [], t.Cb(), e || t.redraw()
                }, this.multiselect.redraw = function() {
                    for (var e = a.multiselect, t = 0; t < a.elements.events.length; t++) {
                        var i = a.elements.events[t];
                        e.isSelected(i.event) ? e.Hb(i) : e.Ib(i)
                    }
                }, this.multiselect.Hb = function(e) {
                    var t = a.multiselect,
                        i = a.q("_selected"),
                        n = t.Jb(e);
                    DayPilot.Util.addClass(n, i), a.useEventSelectionBars && t.Kb(e), t.zb.push(e)
                }, this.multiselect.Jb = function(e) {
                    return e
                }, this.multiselect.Lb = function() {
                    for (var e = a.multiselect, t = 0; t < e.zb.length; t++) {
                        var i = e.zb[t];
                        e.Ib(i, !0)
                    }
                    e.zb = []
                }, this.multiselect.Ib = function(e, t) {
                    var i = a.multiselect,
                        n = a.q("_selected"),
                        o = i.Jb(e);
                    if (DayPilot.Util.removeClass(o, n), a.useEventSelectionBars && i.Mb(e), !t) {
                        var r = DayPilot.indexOf(i.zb, e);
                        r !== -1 && i.zb.splice(r, 1)
                    }
                }, this.multiselect.isSelected = function(e) {
                    return a.multiselect.Eb(e, a.multiselect.gb)
                }, this.multiselect.Gb = function(e) {
                    return DayPilot.indexOf(a.multiselect.gb, e)
                }, this.multiselect.Eb = function(e, t) {
                    if (!t) return !1;
                    for (var i = 0; i < t.length; i++) {
                        var n = t[i];
                        if (e === n) return !0;
                        if ("function" == typeof n.value) {
                            if (null !== n.value() && null !== e.value() && n.value() === e.value()) return !0;
                            if (null === n.value() && null === e.value() && n.recurrentMasterId() === e.recurrentMasterId() && e.start().toStringSortable() === n.start()) return !0
                        } else {
                            if (null !== n.value && null !== e.value() && n.value === e.value()) return !0;
                            if (null === n.value && null === e.value() && n.recurrentMasterId === e.recurrentMasterId() && e.start().toStringSortable() === n.start) return !0
                        }
                    }
                    return !1
                }, this.multiselect.Kb = function(e) {
                    var t = 5;
                    if (!e.top) {
                        var i = document.createElement("div");
                        i.setAttribute("unselectable", "on"), i.style.position = "absolute", i.style.left = e.offsetLeft + "px", i.style.width = e.offsetWidth + "px", i.style.top = e.offsetTop - t + "px", i.style.height = "5px", i.style.backgroundColor = a.eventSelectColor, i.style.zIndex = 100, e.parentNode.appendChild(i), e.top = i
                    }
                    if (!e.bottom) {
                        var n = document.createElement("div");
                        n.setAttribute("unselectable", "on"), n.style.position = "absolute", n.style.left = e.offsetLeft + "px", n.style.width = e.offsetWidth + "px", n.style.top = e.offsetTop + e.offsetHeight + "px", n.style.height = "5px", n.style.backgroundColor = a.eventSelectColor, n.style.zIndex = 100, e.parentNode.appendChild(n), e.bottom = n
                    }
                }, this.multiselect.Mb = function(e) {
                    e.top && (e.parentNode.removeChild(e.top), e.top = null), e.bottom && (e.parentNode.removeChild(e.bottom), e.bottom = null)
                }, this.Ka = function(t) {
                    if (e.editing) return void e.editing.blur();
                    var i = this.yb(t);
                    e.editing = i, DayPilot.re(i, DayPilot.touch.start, function(e) {
                        e.stopPropagation()
                    }), i.onblur = function() {
                        if (t.event) {
                            var n = (t.event.value(), t.event.tag(), t.event.text()),
                                o = i.value;
                            e.editing = null, i.onblur = null, DayPilot.de(i), n !== o && (t.style.display = "none", a.eb(t.event, o))
                        }
                    }, i.onkeypress = function(e) {
                        if (13 === (window.event ? event.keyCode : e.keyCode)) return this.onblur(), e.preventDefault(), !1
                    }, i.onkeyup = function(t) {
                        return 27 === (window.event ? event.keyCode : t.keyCode) && (i.onblur = null, i.parentNode && i.parentNode.removeChild(i), e.editing = !1), !0
                    }, i.select(), i.focus()
                }, this.Nb = function() {
                    return "Resources" !== a.viewType && (!a.columns || !a.r())
                }, this.ba = function() {
                    if (a.Nb()) {
                        var e = this.Ob();
                        this.xa = this.Pb(e)
                    } else this.W = this.Pb(this.columns || []), this.xa = this.Qb(this.headerLevels, !0)
                }, this.Rb = function() {
                    var e = this.startDate.getDatePart(),
                        t = this.days;
                    switch (this.viewType) {
                        case "Day":
                            t = 1;
                            break;
                        case "Week":
                            t = 7, e = e.firstDayOfWeek(r.weekStarts());
                            break;
                        case "WorkWeek":
                            t = 5, e = e.firstDayOfWeek(1)
                    }
                    var i = e.addDays(t),
                        n = {};
                    return n.start = e, n.end = i, n.days = t, n
                }, this.Ob = function() {
                    for (var e = [], t = this.Rb(), i = t.start, n = t.days, o = 0; o < n; o++) {
                        var l = {};
                        l.start = i.addDays(o);
                        var s = r.locale().datePattern;
                        a.headerDateFormat && (s = a.headerDateFormat), l.name = l.start.toString(s, r.locale()), l.html = l.name, e.push(l)
                    }
                    return e
                }, this.Sb = function(e) {
                    var t = {};
                    return e.Start && (e.id = e.Value, e.start = e.Start, e.name = e.Name, e.html = e.InnerHTML, e.toolTip = e.ToolTip, e.backColor = e.BackColor, e.areas = e.Areas, e.children = e.Children, delete e.Value, delete e.Start, delete e.Name, delete e.InnerHTML, delete e.ToolTip, delete e.BackColor, delete e.Areas, delete e.Children), t.id = e.id, t.start = new DayPilot.Date(e.start || a.startDate).getDatePart(), t.name = e.name, t.html = e.html || e.name, t.toolTip = e.toolTip, t.backColor = e.backColor, t.areas = e.areas, t.getChildren = function(e, t) {
                        var i = [];
                        if (e <= 1) return i.push(this), i;
                        if (!this.children || 0 === this.children.length) return t ? i.push(this) : i.push("empty"), i;
                        for (var n = 0; n < this.children.length; n++)
                            for (var a = this.children[n], o = a.getChildren(e - 1, t), r = 0; r < o.length; r++) i.push(o[r]);
                        return i
                    }, t.getChildrenCount = function(e) {
                        var t = 0;
                        if (!this.children || this.children.length <= 0 || e <= 1) return 1;
                        for (var i = 0; i < this.children.length; i++) t += this.children[i].getChildrenCount(e - 1);
                        return t
                    }, t.putIntoBlock = function(e) {
                        for (var t = 0; t < this.blocks.length; t++) {
                            var i = this.blocks[t];
                            if (i.overlapsWith(e.part.top, e.part.height)) return i.events.push(e), i.min = Math.min(i.min, e.part.top), i.max = Math.max(i.max, e.part.top + e.part.height), t
                        }
                        var i = [];
                        return i.lines = [], i.events = [], i.overlapsWith = function(e, t) {
                            return !(e + t - 1 < this.min || e > this.max - 1)
                        }, i.putIntoLine = function(e) {
                            for (var t = 0; t < this.lines.length; t++) {
                                var i = this.lines[t];
                                if (i.isFree(e.part.top, e.part.height)) return i.push(e), t
                            }
                            var i = [];
                            return i.isFree = function(e, t) {
                                for (var i = e + t - 1, n = this.length, a = 0; a < n; a++) {
                                    var o = this[a];
                                    if (!(i < o.part.top || e > o.part.top + o.part.height - 1)) return !1
                                }
                                return !0
                            }, i.push(e), this.lines.push(i), this.lines.length - 1
                        }, i.events.push(e), i.min = e.part.top, i.max = e.part.top + e.part.height, this.blocks.push(i), this.blocks.length - 1
                    }, t.putIntoLine = function(e) {
                        for (var t = 0; t < this.lines.length; t++) {
                            var i = this.lines[t];
                            if (i.isFree(e.part.top, e.part.height)) return i.push(e), t
                        }
                        var i = [];
                        return i.isFree = function(e, t) {
                            for (var i = e + t - 1, n = this.length, a = 0; a < n; a++) {
                                var o = this[a];
                                if (!(i < o.part.top || e > o.part.top + o.part.height - 1)) return !1
                            }
                            return !0
                        }, i.push(e), this.lines.push(i), this.lines.length - 1
                    }, e.children && (t.children = this.Pb(e.children)), t
                }, this.Pb = function(e) {
                    var t = DayPilot.list();
                    return DayPilot.list(e).each(function(e) {
                        var i = a.Sb(e);
                        t.push(i)
                    }), t
                }, this.Qb = function(e, t) {
                    for (var i = this.Nb() ? this.xa : this.W, n = [], a = 0; a < i.length; a++)
                        for (var o = i[a].getChildren(e, t), r = 0; r < o.length; r++) n.push(o[r]);
                    return n
                }, this.ra = function() {
                    if (this.showAllDayEvents) {
                        var e = this.nav.header;
                        if (e) {
                            e.style.display = "none";
                            for (var t = this.xa.length, i = 0; i < this.Tb.lines.length; i++)
                                for (var n = this.Tb.lines[i], o = 0; o < n.length; o++) {
                                    var l = n[o],
                                        s = l.cache || l.data,
                                        d = document.createElement("div");
                                    d.event = l, d.setAttribute("unselectable", "on"), d.style.position = "absolute", a.rtl ? d.style.right = 100 * l.part.colStart / t + "%" : d.style.left = 100 * l.part.colStart / t + "%", d.style.width = 100 * l.part.colWidth / t + "%", d.style.height = r.allDayEventHeight() + "px", d.className = this.q("_alldayevent"), d.style.top = this.headerLevels * r.headerHeight() + i * r.allDayEventHeight() + "px", s.cssClass && DayPilot.Util.addClass(d, s.cssClass), d.style.textAlign = "left", d.style.lineHeight = "1.2", l.client.clickEnabled() && (d.onclick = this.Ia), l.client.doubleClickEnabled() && (d.ondblclick = this.Ma), l.client.clickEnabled() || l.client.doubleClickEnabled() ? d.style.cursor = "pointer" : d.style.cursor = "default", DayPilot.re(d, "contextmenu", this.Na), d.onmousemove = function(e) {
                                        var t = this;
                                        if (!t.active) {
                                            var i = [];
                                            l.client.deleteEnabled() && i.push({
                                                "action": "JavaScript",
                                                "v": "Hover",
                                                "w": 17,
                                                "h": 17,
                                                "top": 3,
                                                "right": 3,
                                                "css": a.q("_event_delete"),
                                                "js": function(e) {
                                                    a.Ra(e)
                                                }
                                            });
                                            var n = s.areas;
                                            n && n.length > 0 && (i = i.concat(n)), DayPilot.Areas.showAreas(t, t.event, null, i), DayPilot.Util.addClass(t, a.q("_alldayevent_hover"))
                                        }
                                        "undefined" != typeof DayPilot.Bubble && a.bubble && "Disabled" !== a.eventHoverHandling && a.bubble.showEvent(this.event)
                                    }, d.onmouseout = function(e) {
                                        var t = this;
                                        DayPilot.Util.removeClass(t, a.q("_alldayevent_hover")), DayPilot.Areas.hideAreas(this, e), a.bubble && a.bubble.hideOnMouseOut()
                                    }, this.showToolTip && !this.bubble && d.setAttribute("title", l.client.toolTip());
                                    var c = l.start().getTime() === l.part.start.getTime(),
                                        h = l.end().getTime() === l.part.end.getTime(),
                                        u = s.backColor,
                                        f = document.createElement("div");
                                    if (f.setAttribute("unselectable", "on"), f.className = this.q("_alldayevent_inner"), u && (f.style.background = u), s.fontColor && (f.style.color = s.fontColor), s.borderColor && (f.style.borderColor = s.borderColor), a.rtl ? (c || DayPilot.Util.addClass(d, this.q("_alldayevent_continueright")), h || DayPilot.Util.addClass(d, this.q("_alldayevent_continueleft"))) : (c || DayPilot.Util.addClass(d, this.q("_alldayevent_continueleft")), h || DayPilot.Util.addClass(d, this.q("_alldayevent_continueright"))), l.client.innerHTML() ? f.innerHTML = l.client.innerHTML() : f.innerHTML = l.text(), d.appendChild(f), a.va()) {
                                        if ("function" == typeof a.onAfterEventRender) {
                                            var v = {};
                                            v.e = d.event, v.div = d, a.onAfterEventRender(v)
                                        }
                                    } else a.afterEventRender && a.afterEventRender(d.event, d);
                                    this.nav.allday.appendChild(d), this.elements.events.push(d)
                                }
                            e.style.display = ""
                        }
                    }
                }, this.O = function(e) {
                    if (a.multiselect.Lb(), this.elements.events)
                        for (var t = 0; t < this.elements.events.length; t++) {
                            var i = this.elements.events[t],
                                n = i.event;
                            if (!n || !e || n.allday()) {
                                if (n && (n.div = null, n.root = null), i.onclick = null, i.onclickSave = null, i.ondblclick = null, i.oncontextmenu = null, i.onmouseover = null, i.onmouseout = null, i.onmousemove = null, i.onmousedown = null, i.firstChild && i.firstChild.firstChild && i.firstChild.firstChild.tagName && "IMG" === i.firstChild.firstChild.tagName.toUpperCase()) {
                                    var o = i.firstChild.firstChild;
                                    o.onmousedown = null, o.onmousemove = null, o.onclick = null
                                }
                                i.helper = null, i.event = null, DayPilot.de(i)
                            }
                        }
                    this.elements.events = []
                }, this.Ub = function(e) {
                    var t = this.nav.events,
                        i = e.cache || e.data,
                        n = (i.borderColor || this.eventBorderColor, document.createElement("div"));
                    n.setAttribute("unselectable", "on"), n.style.MozUserSelect = "none", n.style.KhtmlUserSelect = "none", n.style.WebkitUserSelect = "none", n.style.position = "absolute", n.className = this.q("_event"), n.style.left = e.part.left + "%", n.style.top = e.part.top - this.xb() + "px", n.style.width = e.part.width + "%", n.style.height = Math.max(e.part.height, 2) + "px", n.style.overflow = "hidden", n.isFirst = e.part.start.getTime() === e.start().getTime(), n.isLast = e.part.end.getTime() === e.end().getTime(), e.client.clickEnabled() && (n.onclick = this.Ia), e.client.doubleClickEnabled() && (n.ondblclick = this.Ma), DayPilot.re(n, "contextmenu", this.Na), n.onmousemove = this.Vb, n.onmouseout = this.Wb, n.onmousedown = this.Xb, DayPilot.re(n, DayPilot.touch.start, this.Yb.onEventTouchStart), DayPilot.re(n, DayPilot.touch.move, this.Yb.onEventTouchMove), DayPilot.re(n, DayPilot.touch.end, this.Yb.onEventTouchEnd);
                    i.cssClass && DayPilot.Util.addClass(n, i.cssClass), this.showToolTip && !this.bubble && n.setAttribute("title", e.client.toolTip());
                    var o = document.createElement("div");
                    if (o.setAttribute("unselectable", "on"), o.className = a.q("_event_inner"), o.innerHTML = e.client.innerHTML(), i.fontColor && (o.style.color = i.fontColor), i.backColor && (o.style.background = i.backColor, (DayPilot.browser.ie9 || DayPilot.browser.ielt9) && (o.style.filter = "")), i.borderColor && (o.style.borderColor = i.borderColor), i.backgroundImage && (o.style.backgroundImage = "url(" + i.backgroundImage + ")", i.backgroundRepeat && (o.style.backgroundRepeat = i.backgroundRepeat)), n.appendChild(o), e.client.barVisible()) {
                        var r = e.part.height - 2,
                            l = 100 * e.part.barTop / r,
                            s = Math.ceil(100 * e.part.barHeight / r);
                        "PercentComplete" === this.durationBarMode && (l = 0, s = i.complete);
                        var d = document.createElement("div");
                        d.setAttribute("unselectable", "on"), d.className = this.q("_event_bar"), d.style.position = "absolute", i.barBackColor && (d.style.backgroundColor = i.barBackColor);
                        var c = document.createElement("div");
                        c.setAttribute("unselectable", "on"), c.className = this.q("_event_bar_inner"), c.style.top = l + "%", 0 < s && s <= 1 ? c.style.height = "1px" : c.style.height = s + "%", i.barColor && (c.style.backgroundColor = i.barColor), d.appendChild(c), n.appendChild(d)
                    }
                    if (i.areas)
                        for (var h = 0; h < i.areas.length; h++) {
                            var u = i.areas[h],
                                f = u.visibility || u.v || "Visible";
                            if ("Visible" === f) {
                                var v = DayPilot.Areas.createArea(n, e, u);
                                n.appendChild(v)
                            }
                        }
                    if (t.rows[0].cells[e.part.dayIndex]) {
                        if (t.rows[0].cells[e.part.dayIndex].events.appendChild(n), a.Zb(n), n.event = e, a.multiselect.Db(e) && a.multiselect.add(n.event, !0), a.va()) {
                            if ("function" == typeof a.onAfterEventRender) {
                                var p = {};
                                p.e = n.event, p.div = n, a.onAfterEventRender(p)
                            }
                        } else a.afterEventRender && a.afterEventRender(n.event, n)
                    }
                    a.elements.events.push(n)
                }, this.Zb = function(e) {
                    for (var t = e && e.childNodes ? e.childNodes.length : 0, i = 0; i < t; i++) try {
                        var n = e.childNodes[i];
                        1 === n.nodeType && (n.setAttribute("unselectable", "on"), this.Zb(n))
                    } catch (e) {}
                }, this.qa = function() {
                    for (var e = new Date, t = 0; t < this.xa.length; t++) {
                        var i = this.xa[t];
                        if (i.blocks)
                            for (var n = 0; n < i.blocks.length; n++)
                                for (var a = i.blocks[n], o = 0; o < a.lines.length; o++)
                                    for (var r = a.lines[o], l = 0; l < r.length; l++) {
                                        var s = r[l];
                                        if (s.part.width = 100 / a.lines.length, s.part.left = s.part.width * o, "Cascade" === this.eventArrangement) {
                                            var d = o === a.lines.length - 1;
                                            d || (s.part.width = 1.5 * s.part.width)
                                        }
                                        "Full" === this.eventArrangement && (s.part.left = s.part.left / 2, s.part.width = 100 - s.part.left), s.allday() || this.Ub(s)
                                    }
                    }
                    this.multiselect.redraw();
                    var c = new Date;
                    c.getTime() - e.getTime()
                }, this.$b = function() {
                    this.multiselect.gb = [];
                    for (var e = 0; e < this.xa.length; e++)
                        for (var t = this.xa[e], i = 0; i < t.lines.length; i++)
                            for (var n = t.lines[i], a = 0; a < n.length; a++) {
                                var o = n[a];
                                o.part.width = 100 / t.lines.length, o.part.left = o.Width * i, o.allday() || this.Ub(o)
                            }
                }, this.q = function(e) {
                    var t = this.theme || this.cssClassPrefix;
                    return t ? t + e : ""
                }, this._b = function() {
                    switch (a.C) {
                        case "aspnet":
                            if (!this.M()) throw new DayPilot.Exception("ASP.NET WebForms environment required. https://doc.daypilot.org/common/asp-net-webforms-required/");
                            break;
                        case "netmvc":
                            if (!this.backendUrl) throw new DayPilot.Exception("DayPilot.Scheduler.backendUrl required. https://doc.daypilot.org/common/backendurl-required-asp-net-mvc/");
                            break;
                        case "javaxx":
                            if (!this.backendUrl) throw new DayPilot.Exception("DayPilot.Scheduler.backendUrl required. https://doc.daypilot.org/common/backendurl-required-java/")
                    }
                }, this.pa = function() {
                    "hidden" === this.nav.top.style.visibility && (this.nav.top.style.visibility = "visible")
                }, this.bc = function() {
                    var e = this.Ea() + this.cc();
                    return a.debug.message("Getting totalHeight, headerHeight: " + this.Ea() + " scrollable: " + this.cc()), e < 0 ? 0 : e
                }, this.dc = function() {
                    if (this.nav.top.dp = this, this.nav.top.innerHTML = "", this.nav.top.style.MozUserSelect = "none", this.nav.top.style.KhtmlUserSelect = "none", this.nav.top.style.WebkitUserSelect = "none", this.nav.top.style.WebkitTapHighlightColor = "rgba(0,0,0,0)", this.nav.top.style.WebkitTouchCallout = "none", this.nav.top.style.position = "relative", this.width && (this.nav.top.style.width = this.width), this.rtl && (this.nav.top.style.direction = "rtl"), "Parent100Pct" === this.heightSpec ? this.nav.top.style.height = "100%" : this.nav.top.style.height = this.bc() + "px", this.hideUntilInit && (this.nav.top.style.visibility = "hidden"), this.visible || (this.nav.top.style.display = "none"), this.nav.scroll = document.createElement("div"), this.nav.scroll.style.height = this.cc() + "px", DayPilot.Util.addClass(this.nav.top, this.q("_main")), this.nav.scroll.style.position = "relative", this.showHeader) {
                        var e = this.ec();
                        this.nav.top.appendChild(e)
                    }
                    this.nav.scroll.style.zoom = 1, this.nav.scroll.setAttribute("data-id", "nav.scroll"), this.nav.scroll.style.position = "absolute", this.nav.scroll.style.left = "0px", this.nav.scroll.style.right = "0px", this.nav.scroll.style.top = this.Ea() + "px";
                    var t = this.fc();
                    this.nav.scrollable = t.firstChild, this.nav.scroll.appendChild(t), this.nav.top.appendChild(this.nav.scroll), this.nav.vsph = document.createElement("div"), this.nav.vsph.style.display = "none", this.nav.top.appendChild(this.nav.vsph), this.nav.scrollpos = document.createElement("input"), this.nav.scrollpos.type = "hidden", this.nav.scrollpos.id = a.id + "_scrollpos", this.nav.scrollpos.name = this.nav.scrollpos.id, this.nav.top.appendChild(this.nav.scrollpos), this.nav.select = document.createElement("input"), this.nav.select.type = "hidden", this.nav.select.id = a.id + "_select", this.nav.select.name = this.nav.select.id, this.nav.select.value = null, this.nav.top.appendChild(this.nav.select), this.nav.scrollLayer = document.createElement("div"), this.nav.scrollLayer.style.position = "absolute", this.nav.scrollLayer.style.top = "0px", this.nav.scrollLayer.style.left = "0px", this.nav.top.appendChild(this.nav.scrollLayer), this.nav.scrollUp = [], this.nav.scrollDown = [], this.nav.loading = document.createElement("div"), this.nav.loading.style.position = "absolute", this.nav.loading.style.top = "0px", this.nav.loading.style.left = this.hourWidth + 5 + "px", this.nav.loading.style.backgroundColor = this.loadingLabelBackColor, this.nav.loading.style.fontSize = this.loadingLabelFontSize, this.nav.loading.style.fontFamily = this.loadingLabelFontFamily, this.nav.loading.style.color = this.loadingLabelFontColor, this.nav.loading.style.padding = "2px", this.nav.loading.innerHTML = this.loadingLabelText, this.nav.loading.style.display = "none", this.nav.top.appendChild(this.nav.loading)
                }, this.gc = function() {
                    for (var e = "Fixed" === this.columnWidthSpec, t = this.nav.header && this.nav.header.rows[this.nav.header.rows.length - 1], i = this.nav.events.rows[0], n = 0; n < a.xa.length; n++) {
                        var o = t && t.cells[n],
                            r = i.cells[n],
                            l = o && o.firstChild,
                            s = a.xa[n];
                        if (e) {
                            var d = s.width ? s.width : a.columnWidth;
                            l && (l.style.width = d + "px"), r && (r.style.width = d + "px")
                        } else l && (l.style.width = null), r && (r.style.width = null)
                    }
                }, this.Fa = function() {
                    return this.tb ? a.nav.bottomRight : a.nav.scroll
                }, this.na = function() {
                    var e = "Fixed" === this.columnWidthSpec;
                    if (!e) {
                        var t = a.Fa();
                        "Fixed" === this.heightSpec ? t.style.overflowY = "scroll" : "BusinessHours" === this.heightSpec && this.ya() <= this.businessEndsHour - this.businessBeginsHour ? t.style.overflow = "hidden" : "Full" !== this.heightSpec && "BusinessHoursNoScroll" !== this.heightSpec ? t.style.overflow = "auto" : t.style.overflow = "hidden"
                    }
                    if (e) {
                        var i = 0,
                            n = this.xa.length * this.columnWidth;
                        n > a.ub().clientWidth && (i = DayPilot.sw(a.nav.bottomRight)), a.nav.headerParent && (a.nav.headerParent.style.width = n + i + "px"), a.nav.main.style.width = n + "px", a.nav.events.style.width = n + "px", a.nav.crosshair.style.width = n + "px"
                    } else a.nav.headerParent && (a.nav.headerParent.style.width = "100%"), a.nav.main.style.width = "100%", a.nav.events.style.width = "100%", a.nav.crosshair.style.width = "100%";
                    this.gc()
                }, this.ka = function() {
                    this.u ? this.hc() : DayPilot.pu(this.nav.hourTable), this.nav.hoursPlaceholder && (this.nav.hoursPlaceholder.innerHTML = "", this.nav.hourTable = this.ic(), this.nav.hoursPlaceholder.appendChild(this.nav.hourTable))
                }, this.hc = function() {
                    if (this.nav.hourTable)
                        for (var e = 0; e < this.nav.hourTable.rows.length; e++) {
                            var t = this.nav.hourTable.rows[e],
                                i = t.cells[0].firstChild;
                            i.data = null, i.onmousemove = null, i.onmouseout = null
                        }
                }, this.fc = function() {
                    var e = document.createElement("div");
                    e.style.zoom = 1, e.style.position = "relative", e.onmousemove = this.jc, e.oncontextmenu = this.kc, DayPilot.re(e, DayPilot.touch.start, this.Yb.onMainTouchStart), DayPilot.re(e, DayPilot.touch.move, this.Yb.onMainTouchMove), DayPilot.re(e, DayPilot.touch.end, this.Yb.onMainTouchEnd), DayPilot.re(e, "contextmenu", function(e) {
                        var e = e || window.event;
                        e.preventDefault ? e.preventDefault() : e.returnValue = !1
                    }), navigator.msPointerEnabled && (e.style.msTouchAction = "none", e.style.touchAction = "none"), e.daypilotMainD = !0, e.calendar = this;
                    var t = null,
                        i = null,
                        n = null;
                    if ("Fixed" === this.columnWidthSpec || this.tb) {
                        if (this.showHours) {
                            var o = document.createElement("div");
                            o.style.cssFloat = "left", o.style.styleFloat = "left", o.style.width = this.hourWidth + "px", o.style.height = this.cc() + "px", o.style.overflow = "hidden", o.style.position = "relative", e.appendChild(o), t = o;
                            var r = 30,
                                l = a.lc() + r;
                            n = document.createElement("div"), n.style.height = l + "px", o.appendChild(n)
                        }
                        var s = document.createElement("div");
                        s.style.height = this.cc() + "px", this.showHours && (s.style.marginLeft = this.hourWidth + "px"), s.style.position = "relative", s.style.overflow = "auto", e.appendChild(s), i = s
                    } else {
                        var d = document.createElement("table");
                        d.cellSpacing = "0", d.cellPadding = "0", d.border = "0", d.style.border = "0px none", d.style.width = "100%", d.style.position = "relative";
                        var c, h = d.insertRow(-1);
                        this.showHours && (c = h.insertCell(-1), c.style.verticalAlign = "top", c.style.padding = "0px", c.style.border = "0px none", n = c), c = h.insertCell(-1), c.width = "100%", c.style.padding = "0px", c.style.border = "0px none", c.style.verticalAlign = "top", i = c, e.appendChild(d)
                    }
                    if (n && (this.nav.hourTable = this.ic(), n.appendChild(this.nav.hourTable)), this.cssOnly || this.z) {
                        var u = document.createElement("div");
                        u.style.height = "0px", u.style.position = "relative", u.appendChild(this.nc());
                        var f = document.createElement("div");
                        f.style.position = "absolute", f.style.top = "0px", f.style.left = "0px", f.style.width = "100%", f.style.height = "0px", u.appendChild(f), this.nav.crosshair = f, u.appendChild(this.oc()), i.appendChild(u)
                    } else i.appendChild(this.nc());
                    return this.nav.zoom = e, this.nav.bottomLeft = t, this.nav.bottomRight = i, this.nav.hoursPlaceholder = n, e
                }, this.nc = function() {
                    var e = document.createElement("table");
                    return e.cellPadding = "0", e.cellSpacing = "0", e.border = "0", "Fixed" === this.columnWidthSpec || (e.style.width = "100%"), e.style.border = "0px none", e.style.tableLayout = "fixed", this.nav.main = e, this.nav.events = e, e
                }, this.pc = function() {
                    if ("Fixed" === this.columnWidthSpec) {
                        for (var e = 0, t = 0; t < a.xa.length; t++) {
                            var i = a.xa[t];
                            e += i.width ? i.width : a.columnWidth
                        }
                        return e + "px"
                    }
                    return "100%"
                }, this.oc = function() {
                    var e = document.createElement("table");
                    e.style.position = "absolute", e.style.top = "0px", e.cellPadding = "0", e.cellSpacing = "0", e.border = "0", e.style.width = a.pc(), e.style.border = "0px none", e.style.tableLayout = "fixed", this.nav.events = e;
                    for (var t = this.xa, i = t.length, n = e.insertRow(-1), o = 0; o < i; o++) {
                        var r = n.insertCell(-1);
                        r.style.padding = "0px", r.style.border = "0px none", r.style.height = "0px", r.style.overflow = "visible", a.rtl || (r.style.textAlign = "left"), a.qc(r)
                    }
                    return e
                }, this.ic = function() {
                    var e = document.createElement("table");
                    e.cellSpacing = "0", e.cellPadding = "0", e.border = "0", e.style.border = "0px none", e.style.width = this.hourWidth + "px", e.oncontextmenu = function() {
                        return !1
                    }, e.onmousemove = function() {
                        a.V()
                    };
                    for (var t = a.rc(), i = 0; i < t; i++) this.sc(e, i);
                    return e
                }, this.rc = function() {
                    return this.za() / (60 * this.timeHeaderCellDuration * 1e3)
                }, this.tc = function() {
                    return 60 * this.cellHeight / this.cellDuration / (60 / this.timeHeaderCellDuration)
                }, this.uc = function() {
                    return (this.Da() - this.Da(!0)) * (60 / this.cellDuration)
                }, this.vc = function() {
                    return this.Da() - this.Da(!0)
                }, this.xb = function() {
                    return this.uc() * this.cellHeight
                }, this.sc = function(e, t) {
                    var i = a.tc(),
                        n = e.insertRow(-1);
                    n.style.height = i + "px";
                    var o = n.insertCell(-1);
                    o.valign = "bottom", o.setAttribute("unselectable", "on"), o.style.padding = "0px", o.style.border = "0px none";
                    var r = document.createElement("div");
                    r.className = this.q("_rowheader"), r.style.position = "relative", r.style.width = this.hourWidth + "px", r.style.height = i + "px", r.style.overflow = "hidden", r.setAttribute("unselectable", "on");
                    var l = document.createElement("div");
                    l.className = this.q("_rowheader_inner"), l.setAttribute("unselectable", "on");
                    var s = a.wc(t),
                        d = s.html,
                        c = s.data;
                    c && (r.data = c, r.onmousemove = a.xc, r.onmouseout = a.yc), l.innerHTML = d, r.appendChild(l), o.appendChild(r)
                }, this.wc = function(e) {
                    var t, i;
                    if (this.hours) {
                        var n = e + this.vc();
                        i = this.hours[n], t = i.html
                    }
                    var o, r = this.timeHeaderCellDuration,
                        l = this.startDate.addMinutes(r * e + 60 * this.Da()),
                        s = l.getHours(),
                        d = s < 12;
                    if (o = "Clock12Hours" === this.zc.timeFormat() ? d ? "AM" : "PM" : "00", !t) {
                        var c = document.createElement("div");
                        c.setAttribute("unselectable", "on"), "Clock12Hours" === this.zc.timeFormat() && (s %= 12, 0 === s && (s = 12)), 60 !== this.timeHeaderCellDuration && (s += ":" + l.toString("mm")), c.innerHTML = s;
                        var h = document.createElement("span");
                        h.setAttribute("unselectable", "on"), h.className = this.q("_rowheader_minutes"), h.innerHTML = o, c.appendChild(h), t = c.outerHTML
                    }
                    if ("function" == typeof a.onBeforeTimeHeaderRender) {
                        var u = {};
                        u.header = {}, u.header.hours = s, u.header.minutes = l.getMinutes(), u.header.start = l.toString("HH:mm"), u.header.html = t, u.header.areas = i ? i.areas : null, a.onBeforeTimeHeaderRender(u), null !== u.header.html && (t = u.header.html), i = u.header
                    }
                    var f = {};
                    return f.start = l, f.hour = s, f.html = t, f.data = i, f.sup = o, f
                }, this.xc = function(e) {
                    a.V();
                    var t = this;
                    t.active || DayPilot.Areas.showAreas(t, t.data)
                }, this.yc = function(e) {
                    DayPilot.Areas.hideAreas(this, e)
                }, this.cc = function() {
                    switch (this.heightSpec) {
                        case "Fixed":
                            return this.height;
                        case "Parent100Pct":
                            return this.height;
                        case "Full":
                            return this.lc();
                        case "BusinessHours":
                        case "BusinessHoursNoScroll":
                            return this.Aa() * this.cellHeight * 60 / this.cellDuration;
                        default:
                            throw "DayPilot.Calendar: Unexpected 'heightSpec' value."
                    }
                }, this.lc = function() {
                    return a.za() * this.cellHeight / (6e4 * a.cellDuration)
                }, this.Ea = function() {
                    if (!this.showHeader) return 0;
                    var e = this.headerLevels * r.headerHeight();
                    return this.showAllDayEvents && r.allDayHeaderHeight() ? e + r.allDayHeaderHeight() : e
                }, this.ga = function() {
                    if (this.headerHeightAutoFit) {
                        for (var e = 0, t = 0; t < this.nav.header.rows.length; t++)
                            for (var i = this.nav.header.rows[t], n = 0; n < i.cells.length; n++) {
                                var a = i.cells[n],
                                    o = a.firstChild,
                                    r = o.firstChild,
                                    l = o.style.height;
                                o.style.height = "auto", r.style.position = "static";
                                var s = o.offsetHeight;
                                o.style.height = l, r.style.position = "", e = Math.max(e, s)
                            }
                        e > this.headerHeight && (this.t.headerHeight = e, this.ea(), this.fa())
                    }
                }, this.tb = !0, this.Ac = function() {
                    var e = this.tb ? a.nav.bottomRight : a.nav.scroll;
                    return DayPilot.sw(e) > 0
                }, this.ec = function() {
                    var e = document.createElement("div"),
                        t = "Fixed" === this.columnWidthSpec;
                    t || (e.style.overflow = "auto"), e.style.position = "absolute", e.style.left = "0px", e.style.right = "0px";
                    var i = document.createElement("div");
                    i.style.position = "relative", i.style.zoom = "1";
                    var n = null,
                        t = "Fixed" === this.columnWidthSpec;
                    if (t || this.tb) {
                        var a = document.createElement("div");
                        if (a.style.cssFloat = "left", a.style.styleFloat = "left", a.style.width = this.hourWidth + "px", this.showHours) {
                            var o = this.Bc();
                            this.nav.corner = o, a.appendChild(o), i.appendChild(a), this.nav.upperLeft = a
                        }
                        var r = document.createElement("div");
                        this.showHours && (r.style.marginLeft = this.hourWidth + "px"), r.style.position = "relative", r.style.overflow = "hidden", r.style.height = this.Ea() + "px", i.appendChild(r), this.nav.upperRight = r, n = document.createElement("div"), n.style.position = "relative", r.appendChild(n)
                    } else {
                        var l = document.createElement("table");
                        l.cellPadding = "0", l.cellSpacing = "0", l.border = "0", l.style.width = "100%", l.style.borderCollapse = "separate", l.style.border = "0px none";
                        var s = l.insertRow(-1);
                        if (this.nav.fullHeader = l, this.showHours) {
                            var d = s.insertCell(-1);
                            d.style.padding = "0px", d.style.border = "0px none";
                            var o = this.Bc();
                            d.appendChild(o), this.nav.corner = o
                        }
                        d = s.insertCell(-1), d.style.width = "100%", d.valign = "top", d.style.position = "relative", d.style.padding = "0px", d.style.border = "0px none", n = document.createElement("div"), n.style.position = "relative", n.style.height = this.Ea() + "px", n.style.overflow = "hidden", d.appendChild(n), this.nav.mid = n, i.appendChild(l)
                    }
                    this.nav.headerParent = n, this.Cc();
                    var c = this.Ac(),
                        t = "Fixed" === this.columnWidthSpec;
                    return c && !t && this.Dc(), e.appendChild(i), e
                }, this.Cc = function() {
                    this.nav.unifiedCornerRight = null, this.nav.header = document.createElement("table"), this.nav.header.cellPadding = "0", this.nav.header.cellSpacing = "0";
                    var e = "Fixed" === this.columnWidthSpec;
                    e || (this.nav.header.width = "100%"), this.nav.header.style.tableLayout = "fixed", this.nav.header.oncontextmenu = function() {
                        return !1
                    };
                    this.Ac();
                    if (this.nav.headerParent.appendChild(this.nav.header), this.nav.allday && DayPilot.de(this.nav.allday), this.showAllDayEvents) {
                        var t = document.createElement("div");
                        t.style.position = "absolute", t.style.top = "0px", t.style.height = "0px";
                        var e = "Fixed" === this.columnWidthSpec;
                        e ? t.style.width = this.xa.length * this.columnWidth + "px" : (t.style.left = "0px", t.style.right = "0px"), this.nav.allday = t, this.nav.headerParent.appendChild(t)
                    }
                }, this.Dc = function() {
                    if (this.nav.fullHeader) {
                        var e = this.nav.fullHeader.rows[0],
                            t = e.insertCell(-1);
                        t.style.padding = "0px", t.style.verticalAlign = "top", t.setAttribute("unselectable", "on");
                        var i = document.createElement("div");
                        i.setAttribute("unselectable", "on"), i.className = this.q("_cornerright"), i.style.overflow = "hidden", i.style.position = "relative", i.style.width = "16px", i.style.height = this.Ea() + "px";
                        var n = document.createElement("div");
                        n.className = this.q("_cornerright_inner"), i.appendChild(n), t.appendChild(i), this.nav.cornerRight = i
                    }
                }, this.Bc = function() {
                    var e = document.createElement("div");
                    e.style.position = "relative", e.className = this.q("_corner"), e.style.width = this.hourWidth + "px", e.style.height = this.Ea() + "px", e.style.overflow = "hidden", e.oncontextmenu = function() {
                        return !1
                    };
                    var t = document.createElement("div");
                    t.className = this.q("_corner_inner"), t.setAttribute("unselectable", "on");
                    var i = this.cornerHTML || this.cornerHtml;
                    t.innerHTML = i ? i : "", e.appendChild(t);
                    var n = document.createElement("div");
                    return n.style.position = "absolute", n.style.padding = "2px", n.style.top = "0px", n.style.left = "1px", n.style.backgroundColor = "#FF6600", n.style.color = "white", n.innerHTML = "\u0044\u0045" + "\u004D\u004F", n.setAttribute("unselectable", "on"), DayPilot.Util.isNullOrUndefined(undefined) && e.appendChild(n), e
                }, this.P = function() {
                    var e = this.nav.main;
                    e.root = null, e.onmouseup = null;
                    for (var t = 0; t < e.rows.length; t++)
                        for (var i = e.rows[t], n = 0; n < i.cells.length; n++) {
                            var a = i.cells[n];
                            a.root = null, a.onmousedown = null, a.onmousemove = null, a.onmouseout = null, a.onmouseup = null, a.onclick = null, a.ondblclick = null, a.oncontextmenu = null
                        }
                    this.u || DayPilot.pu(e)
                }, this.ha = function() {
                    for (var e = 0; this.nav.scrollUp && e < this.nav.scrollUp.length; e++) this.nav.scrollLayer.removeChild(this.nav.scrollUp[e]);
                    for (var e = 0; this.nav.scrollDown && e < this.nav.scrollDown.length; e++) this.nav.scrollLayer.removeChild(this.nav.scrollDown[e]);
                    this.nav.scrollUp = [], this.nav.scrollDown = []
                }, this.qc = function(e) {
                    var t = document.createElement("div");
                    t.style.marginRight = a.columnMarginRight + "px", t.style.position = "relative", t.style.height = "1px", t.style.marginTop = "-1px", e.events = t;
                    var i = document.createElement("div");
                    i.style.position = "relative", i.style.height = "1px", i.style.marginTop = "-1px", e.separators = i;
                    var n = document.createElement("div");
                    n.style.position = "relative", n.style.height = "1px", n.style.marginTop = "-1px", e.selection = n, e.appendChild(i), e.appendChild(n), e.appendChild(t)
                }, this.ja = function() {
                    var t = this.nav.main,
                        i = 60 * this.cellDuration * 1e3,
                        n = this.Ha(),
                        o = (this.vc() * (60 / this.cellDuration), a.xa),
                        r = 0;
                    if (t && (r = a.ub().scrollTop, this.P(), a.D.ielt9)) {
                        DayPilot.de(this.nav.scrollable.parentNode);
                        var l = this.fc();
                        this.Ec(), this.nav.scrollable = l.firstChild, this.nav.scroll.appendChild(l), t = this.nav.main
                    }
                    for (this.nav.scrollable.daypilotMainD = !0, this.nav.scrollable.calendar = this; t && t.rows && t.rows.length > 0;) this.u || DayPilot.pu(t.rows[0]), t.deleteRow(0);
                    if (this.Fc = !0, this.scrollLabelsVisible)
                        for (var o = this.xa, s = this.showHours ? this.hourWidth : 0, d = (this.nav.scroll.clientWidth - s) / o.length, c = 0; c < o.length; c++) {
                            var h = document.createElement("div");
                            h.style.position = "absolute", h.style.top = "0px", h.style.left = s + 2 + c * d + d / 2 + "px", h.style.display = "none";
                            var u = document.createElement("div");
                            u.style.height = "10px", u.style.width = "10px", u.className = this.q("_scroll_up"), h.appendChild(u), this.nav.scrollLayer.appendChild(h), this.nav.scrollUp.push(h);
                            var f = document.createElement("div");
                            f.style.position = "absolute", f.style.top = "0px", f.style.left = s + 2 + c * d + d / 2 + "px", f.style.display = "none";
                            var u = document.createElement("div");
                            u.style.height = "10px", u.style.width = "10px", u.className = this.q("_scroll_down"), f.appendChild(u), this.nav.scrollLayer.appendChild(f), this.nav.scrollDown.push(f)
                        }
                    var v = o.length;
                    if (this.cssOnly || this.z) {
                        for (var p = this.nav.events; p && p.rows && p.rows.length > 0;) this.u || DayPilot.pu(p.rows[0]), p.deleteRow(0);
                        for (var g = p.insertRow(-1), m = 0; m < v; m++) {
                            var y = g.insertCell(-1);
                            y.style.padding = "0px", y.style.border = "0px none", y.style.height = "1px", y.style.overflow = "visible";
                            var b = "Fixed" === this.columnWidthSpec;
                            b && (o[m].width ? y.style.width = o[m].width + "px" : y.style.width = this.columnWidth + "px"), a.rtl || (y.style.textAlign = "left"), a.qc(y)
                        }
                    }
                    for (var c = 0; c < n; c++) {
                        var g = t.insertRow(-1),
                            w = c;
                        g.style.MozUserSelect = "none", g.style.KhtmlUserSelect = "none", g.style.WebkitUserSelect = "none";
                        for (var m = 0; m < v; m++) {
                            var D = this.xa[m],
                                y = g.insertCell(-1);
                            y.start = D.start.addTime(w * i).addHours(this.Da()), y.end = y.start.addTime(i), y.resource = D.id, this.cellProperties || (this.cellProperties = {});
                            var k = {};
                            k.resource = y.resource, k.start = y.start, k.end = y.end;
                            var x = m + "_" + w;
                            if (k.cssClass = null, k.html = null, k.backImage = null, k.backRepeat = null, k.backColor = null, k.business = this.Gc(y.start, y.end), this.cellProperties[x] && DayPilot.Util.copyProps(this.cellProperties[x], k, ["cssClass", "html", "backImage", "backRepeat", "backColor", "business", "areas"]), "function" == typeof this.onBeforeCellRender) {
                                var P = {};
                                P.cell = k, this.onBeforeCellRender(P)
                            }
                            this.cellProperties[x] = k;
                            var C, S = a.Hc(m, w);
                            y.root = this, y.style.padding = "0px", y.style.border = "0px none", y.style.verticalAlign = "top", y.style.height = a.cellHeight + "px", y.style.overflow = "hidden", y.setAttribute("unselectable", "on"), C = document.createElement("div"), C.className = a.q("_cell"), C.style.position = "relative", C.style.height = a.cellHeight + "px", C.style.overflow = "hidden", C.setAttribute("unselectable", "on");
                            var A = document.createElement("div");
                            A.className = a.q("_cell_inner"), C.appendChild(A), y.appendChild(C), y.onmousedown = this.kb, y.onmousemove = this.qb, y.onmouseout = this.pb, DayPilot.re(y, DayPilot.touch.end, this.Yb.onCellTouchEnd), y.onmouseup = function() {
                                return !1
                            }, y.onclick = function() {
                                return !1
                            }, y.ondblclick = function() {
                                if (e.firstMousePos = null, a.lb(), clearTimeout(e.selectedTimeout), "Disabled" !== a.timeRangeDoubleClickHandling) {
                                    var t = a.getSelection();
                                    t && a.db(t.start, t.end, t.resource)
                                }
                            }, y.oncontextmenu = function() {
                                return this.selected || (a.clearSelection(), e.column = e.getColumn(this), a.selectedCells.push(this), e.firstSelected = this, e.topSelectedCell = this, e.bottomSelectedCell = this, a.lb()), "Disabled" === a.timeRangeRightClickHandling, !1
                            };
                            var T = a.Ic(m, w);
                            C = y.firstChild;
                            var b = "Fixed" === this.columnWidthSpec;
                            b && (D.width ? C.style.width = D.width + "px" : C.style.width = this.columnWidth + "px"), T && (C.firstChild.style.background = T);
                            (S ? S.business : this.Gc(y.start, y.end)) && DayPilot.Util.addClass(C, a.q("_cell_business"));
                            var E = C.firstChild;
                            if (E && (E.innerHTML = ""), C.style.backgroundImage = "", C.style.backgroundRepeat = "", S && (S.html && (E.innerHTML = S.html), S.cssClass && DayPilot.Util.addClass(C, S.cssClass), S.backImage && (C.style.backgroundImage = "url('" + S.backImage + "')"), S.backRepeat && (C.style.backgroundRepeat = S.backRepeat), S.areas)) {
                                var M = {};
                                M.start = y.start, M.end = y.end, M.resource = y.resource, DayPilot.Areas.attach(C, M, {
                                    "areas": S.areas
                                })
                            }
                        }
                    }
                    t.onmouseup = this.rb, t.root = this, a.nav.scrollable.style.display = "", a.ub().scrollTop = r
                }, this.kc = function(e) {
                    if (!o.detected) {
                        var t = a.getSelection();
                        if (t) {
                            var i = {};
                            if (i.start = t.start, i.end = t.end, i.resource = t.resource, i.preventDefault = function() {
                                    this.preventDefault.value = !0
                                }, "function" == typeof a.onTimeRangeRightClick && (a.onTimeRangeRightClick(i), i.preventDefault.value)) return !1;
                            "ContextMenu" === a.timeRangeRightClickHandling && a.contextMenuSelection && a.contextMenuSelection.show(a.getSelection()), "function" == typeof a.onTimeRangeRightClicked && a.onTimeRangeRightClicked(i)
                        }
                    }
                }, this.Gc = function(e, t) {
                    return this.businessBeginsHour < this.businessEndsHour ? !(e.getHours() < this.businessBeginsHour || e.getHours() >= this.businessEndsHour || 6 === e.getDayOfWeek() || 0 === e.getDayOfWeek()) : e.getHours() >= this.businessBeginsHour || e.getHours() < this.businessEndsHour
                }, this.jc = function(t) {
                    t = t || window.event, t.insideMainD = !0, window.event && window.event.srcElement && (window.event.srcElement.inside = !0), e.activeCalendar = this;
                    var i = a.nav.main;
                    a.coords = DayPilot.mo3(i, t);
                    var n = DayPilot.mc(t),
                        o = a.crosshairType && "Disabled" !== a.crosshairType,
                        r = a.coords.x < a.hourWidth;
                    if (DayPilot.Global.moving || DayPilot.Global.resizing || e.selecting || r ? a.V() : o && a.Jc(), DayPilot.Global.resizing) a.Kc(n);
                    else if (DayPilot.Global.moving) {
                        if (!DayPilot.Global.moving.helper) return void(e.movingShadow = null);
                        if (!e.movingShadow) {
                            var l = 3;
                            if (!(DayPilot.distance(n, e.originalMouse) > l)) return;
                            e.movingShadow = a.wa(DayPilot.Global.moving, !a.D.ie, a.shadow), e.movingShadow.style.width = e.movingShadow.parentNode.offsetWidth + 1 + "px"
                        }
                        if (!a.coords) return;
                        var s = a.cellHeight,
                            d = 0,
                            c = e.moveOffsetY;
                        c || (c = s / 2), "Top" === this.moveBy && (c = 0);
                        var h = Math.floor((a.coords.y - c - d + s / 2) / s) * s + d;
                        h < d && (h = d);
                        var u = a.nav.main,
                            f = u.clientHeight,
                            v = parseInt(e.movingShadow.style.height);
                        h + v > f && (h = f - v);
                        var p = u.clientWidth / u.rows[0].cells.length,
                            g = Math.floor(a.coords.x / p);
                        g < 0 && (g = 0), a.rtl && (g = a.xa.length - g - 1),
                            function() {
                                var t = a.Lc,
                                    i = DayPilot.Global.moving.event,
                                    n = a.Wa(i, g, h);
                                if (!t || t.start.getTime() !== n.start.getTime() || t.end.getTime() !== n.end.getTime() || t.resource !== n.resource) {
                                    var o = {};
                                    o.e = i, o.start = n.start, o.end = n.end, o.resource = n.resource, o.html = null, a.Lc = o, "function" == typeof a.onEventMoving && a.onEventMoving(o);
                                    var r = e.movingShadow;
                                    o.html ? r.firstChild.innerHTML = o.html : r.firstChild.innerHTML = ""
                                }
                            }(), e.movingShadow.style.top = h + "px";
                        var m = a.nav.events;
                        g < m.rows[0].cells.length && g >= 0 && e.movingShadow.column !== g && (e.movingShadow.column = g, e.moveShadow(m.rows[0].cells[g]))
                    } else if (e.firstMousePos) {
                        var y = a.Mc(a.coords.x),
                            b = Math.floor(a.coords.y / a.cellHeight),
                            w = a.nav.main.rows[b].cells[y];
                        if (!e.selecting) {
                            var D = e.firstMousePos,
                                k = a.coords;
                            D.x === k.x && D.y === k.y || (e.selecting = {
                                "calendar": a
                            })
                        }
                        if (!e.selecting) return;
                        var n = a.coords,
                            x = e.getColumn(w);
                        x !== e.column && (w = a.nav.main.rows[b].cells[e.column]);
                        var P = e.selecting;
                        if (P.last && P.last.x === y && P.last.y === b) return;
                        P.last = {
                            "x": y,
                            "y": b
                        }, n.y < e.firstMousePos.y ? (a.selectedCells = e.getCellsBelow(w), e.topSelectedCell = a.selectedCells[0], e.bottomSelectedCell = e.firstSelected) : (a.selectedCells = e.getCellsAbove(w), e.topSelectedCell = e.firstSelected, e.bottomSelectedCell = a.selectedCells[0]), a.lb()
                    }
                    if (e.drag) {
                        if (e.gShadow && document.body.removeChild(e.gShadow), e.gShadow = null, !e.movingShadow && a.coords) {
                            var C = a.wa(e.drag, !1, e.drag.shadowType);
                            if (C) {
                                e.movingShadow = C;
                                var k = DayPilot.Date.today(),
                                    t = {
                                        "id": e.drag.id,
                                        "start": k,
                                        "end": k.addSeconds(e.drag.duration),
                                        "text": e.drag.text
                                    },
                                    S = e.drag.data;
                                if (S) {
                                    var A = ["duration", "element", "remove", "id", "text"];
                                    for (var T in S) DayPilot.contains(A, T) || (t[T] = S[T])
                                }
                                var E = new DayPilot.Event(t, a);
                                E.external = !0, DayPilot.Global.moving = {}, DayPilot.Global.moving.event = E, DayPilot.Global.moving.helper = {}
                            }
                        }
                        t.cancelBubble = !0
                    }
                }, this.Nc = function(t) {
                    e.movingShadow.offsetTop, DayPilot.Global.moving.event, e.movingShadow.column
                }, this.Kc = function(t) {
                    if (!DayPilot.Global.resizing.event) return DayPilot.Global.resizing = null, DayPilot.de(e.resizingShadow), void(e.resizingShadow = null);
                    e.resizingShadow || (e.resizingShadow = a.wa(DayPilot.Global.resizing, !1, a.shadow));
                    var i = DayPilot.Global.resizing.event.calendar.cellHeight,
                        n = 0,
                        o = t.y - e.originalMouse.y;
                    if ("bottom" === DayPilot.Global.resizing.dpBorder) {
                        var r = Math.floor((e.originalHeight + e.originalTop + o + i / 2) / i) * i - e.originalTop + n;
                        r < i && (r = i);
                        var l = DayPilot.Global.resizing.event.calendar.nav.main.clientHeight;
                        e.originalTop + r > l && (r = l - e.originalTop), e.resizingShadow.style.height = r + "px"
                    } else if ("top" === DayPilot.Global.resizing.dpBorder) {
                        var s = Math.floor((e.originalTop + o - n + i / 2) / i) * i + n;
                        s < n && (s = n), s > e.originalTop + e.originalHeight - i && (s = e.originalTop + e.originalHeight - i);
                        var r = e.originalHeight - (s - e.originalTop);
                        r < i ? r = i : e.resizingShadow.style.top = s + "px", e.resizingShadow.style.height = r + "px"
                    }
                }, this.temp = {}, this.temp.getPosition = function() {
                    var e = a.Oc.getCellCoords();
                    if (!e) return null;
                    var t = a.xa[e.x],
                        i = {};
                    return i.resource = t.id, i.start = new DayPilot.Date(t.start).addHours(a.Da(!0)).addMinutes(e.y * a.cellDuration), i.end = i.start.addMinutes(a.cellDuration), i
                }, this.Oc = {}, this.Oc.getCellCoords = function() {
                    var e = {};
                    if (e.x = 0, e.y = 0, !a.coords) return null;
                    for (var t = a.nav.main, i = a.coords.x, n = 0, o = this.col(t, n); o && i > o.left;) n += 1, o = this.col(t, n);
                    e.x = n - 1;
                    var r = 0,
                        l = Math.floor((a.coords.y - r) / a.cellHeight);
                    return e.y = l, e.x < 0 ? null : e
                }, this.Oc.col = function(e, t) {
                    var i = {};
                    if (i.left = 0, i.width = 0, !e) return null;
                    if (!e.rows) return null;
                    if (0 === e.rows.length) return null;
                    if (0 == e.rows[0].cells.length) return null;
                    var n = e.rows[0].cells[t];
                    if (!n) return null;
                    var a = DayPilot.abs(e),
                        o = DayPilot.abs(n);
                    return i.left = o.x - a.x, i.width = n.offsetWidth, i
                }, this.Jc = function() {
                    this.V(), this.elements.crosshair || (this.elements.crosshair = []);
                    var e = this.Oc.getCellCoords();
                    if (e) {
                        var t = e.x,
                            i = Math.floor(e.y / (60 / a.cellDuration) * (60 / a.timeHeaderCellDuration));
                        if (!(i < 0)) {
                            if (this.nav.hourTable) {
                                if (i >= this.nav.hourTable.rows.length) return;
                                var n = document.createElement("div");
                                n.style.position = "absolute", n.style.left = "0px", n.style.right = "0px", n.style.top = "0px", n.style.bottom = "0px", n.style.opacity = .5, n.style.backgroundColor = this.crosshairColor, n.style.opacity = this.crosshairOpacity / 100, n.style.filter = "alpha(opacity=" + this.crosshairOpacity + ")", this.nav.hourTable.rows[i].cells[0].firstChild.appendChild(n), this.elements.crosshair.push(n)
                            }
                            if (this.nav.header) {
                                var o = document.createElement("div");
                                o.style.position = "absolute", o.style.left = "0px", o.style.right = "0px", o.style.top = "0px", o.style.bottom = "0px", o.style.opacity = .5, o.style.backgroundColor = this.crosshairColor, o.style.opacity = this.crosshairOpacity / 100, o.style.filter = "alpha(opacity=" + this.crosshairOpacity + ")";
                                var r = this.nav.header.rows[this.headerLevels - 1];
                                r.cells[t] && (r.cells[t].firstChild.appendChild(o), this.elements.crosshair.push(o))
                            }
                            if ("Header" !== this.crosshairType) {
                                var l = this.nav.crosshair,
                                    s = 0,
                                    d = Math.floor((a.coords.y - s) / a.cellHeight) * a.cellHeight + s,
                                    c = a.cellHeight,
                                    h = document.createElement("div");
                                h.style.position = "absolute", h.style.left = "0px", h.style.right = "0px", h.style.top = d + "px", h.style.height = c + "px", h.style.backgroundColor = this.crosshairColor, h.style.opacity = this.crosshairOpacity / 100, h.style.filter = "alpha(opacity=" + this.crosshairOpacity + ")", h.onmousedown = this.Pc, l.appendChild(h), this.elements.crosshair.push(h);
                                var u = this.Oc.col(this.nav.main, t);
                                if (c = this.nav.main.clientHeight, u) {
                                    var f = document.createElement("div");
                                    f.style.position = "absolute", f.style.left = u.left + "px", f.style.width = u.width + "px", f.style.top = "0px", f.style.height = c + "px", f.style.backgroundColor = this.crosshairColor, f.style.opacity = this.crosshairOpacity / 100, f.style.filter = "alpha(opacity=" + this.crosshairOpacity + ")", f.onmousedown = this.Pc, l.appendChild(f), this.elements.crosshair.push(f)
                                }
                            }
                        }
                    }
                }, this.Pc = function(e) {
                    a.V();
                    var t = a.Oc.getCellCoords(),
                        i = 0,
                        n = a.nav.main.rows[t.y + i].cells[t.x];
                    a.kb.apply(n, [e])
                }, this.V = function() {
                    if (this.elements.crosshair && 0 !== this.elements.crosshair.length) {
                        for (var e = 0; e < this.elements.crosshair.length; e++) {
                            var t = this.elements.crosshair[e];
                            t && t.parentNode && t.parentNode.removeChild(t)
                        }
                        this.elements.crosshair = []
                    }
                }, this.ca = function() {
                    if (this.cellConfig) {
                        var e = this.cellConfig;
                        if (e.vertical)
                            for (var t = 0; t < e.x; t++)
                                for (var i = this.cellProperties[t + "_0"], n = 1; n < e.y; n++) this.cellProperties[t + "_" + n] = i;
                        if (e.horizontal)
                            for (var n = 0; n < e.y; n++)
                                for (var i = this.cellProperties["0_" + n], t = 1; t < e.x; t++) this.cellProperties[t + "_" + n] = i;
                        if (e["default"])
                            for (var i = e["default"], n = 0; n < e.y; n++)
                                for (var t = 0; t < e.x; t++) this.cellProperties[t + "_" + n] || (this.cellProperties[t + "_" + n] = i)
                    }
                }, this.Hc = function(e, t) {
                    return this.cellProperties ? this.cellProperties[e + "_" + t] : null
                }, this.Qc = function(e, t) {
                    var i = e + "_" + t;
                    return !(!this.cellProperties || !this.cellProperties[i]) && this.cellProperties[i].business
                }, this.Ic = function(e, t) {
                    var i = e + "_" + t;
                    return this.cellProperties && this.cellProperties[i] ? this.cellProperties[i].backColor : null
                }, this.Q = function() {
                    var e = this.nav.header;
                    if (e && e.rows)
                        for (var t = 0; t < e.rows.length; t++)
                            for (var i = e.rows[t], n = 0; n < i.cells.length; n++) {
                                var a = i.cells[n];
                                a.onclick = null, a.onmousemove = null, a.onmouseout = null
                            }
                    this.u || DayPilot.pu(e)
                }, this.Rc = function(e, t) {
                    for (var i = t ? this.nav.header.insertRow(-1) : this.nav.header.rows[e - 1], n = this.Qb(e), o = n.length, l = e === a.headerLevels, s = 0; s < o; s++) {
                        var d = n[s];
                        if (a.va() && "function" == typeof a.onBeforeHeaderRender) {
                            var c = {};
                            c.header = {}, DayPilot.Util.copyProps(d, c.header, ["id", "start", "name", "html", "backColor", "toolTip", "areas", "children"]), this.onBeforeHeaderRender(c), DayPilot.Util.copyProps(c.header, d, ["html", "backColor", "toolTip", "areas"])
                        }
                        var h = !!d.getChildren,
                            u = t ? i.insertCell(-1) : i.cells[s];
                        if (u.data = d, l);
                        else {
                            var f = 1;
                            h && (f = d.getChildrenCount(a.headerLevels - e + 1)), u.colSpan = f
                        }
                        h && (u.onclick = this.Oa, u.onmousemove = this.Pa, u.onmouseout = this.Qa, d.toolTip && (u.title = d.toolTip)), u.style.overflow = "hidden", u.style.padding = "0px", u.style.border = "0px none", u.style.height = r.headerHeight() + "px";
                        var v = t ? document.createElement("div") : u.firstChild;
                        if (t) {
                            v.setAttribute("unselectable", "on"), v.style.MozUserSelect = "none", v.style.KhtmlUserSelect = "none", v.style.WebkitUserSelect = "none", v.style.position = "relative", v.style.height = r.headerHeight() + "px", v.className = a.q("_colheader");
                            var p = document.createElement("div");
                            p.className = a.q("_colheader_inner"), d.backColor && (p.style.background = d.backColor), v.appendChild(p), u.appendChild(v)
                        } else v.style.height = r.headerHeight() + "px";
                        this.Sc(v, d), h && (v.firstChild.innerHTML = d.html)
                    }
                }, this.Sc = function(t, i) {
                    for (var n = [], a = 0; a < t.childNodes.length; a++) {
                        var o = t.childNodes[a];
                        o.isActiveArea && n.push(o)
                    }
                    for (var a = 0; a < n.length; a++) {
                        var o = n[a];
                        DayPilot.de(o)
                    }
                    if (i.areas)
                        for (var r = i.areas, a = 0; a < r.length; a++) {
                            var l = r[a],
                                s = l.visibility || l.v;
                            if ("Visible" === s) {
                                var d = new e.Column(i.id, i.name, i.start),
                                    c = DayPilot.Areas.createArea(t, d, l);
                                t.appendChild(c)
                            }
                        }
                }, this.fa = function() {
                    if (this.showHeader) {
                        var e = this.nav.header,
                            t = !0,
                            i = this.Qb(a.headerLevels, !0),
                            n = i.length;
                        for (DayPilot.de(this.nav.header), this.Cc(); this.Tc && e && e.rows && e.rows.length > 0;) this.u || DayPilot.pu(e.rows[0]), e.deleteRow(0);
                        this.Tc = !0;
                        var o = a.cornerHTML || a.cornerHtml,
                            l = a.nav.corner;
                        l && (this.u || DayPilot.pu(l.firstChild), l.firstChild.innerHTML = o ? o : "");
                        for (var s = 0; s < a.headerLevels; s++) this.Rc(s + 1, t);
                        if (this.showAllDayEvents)
                            for (var d = this.nav.header.insertRow(-1), s = 0; s < n; s++) {
                                var c = i[s],
                                    h = d.insertCell(-1);
                                h.data = c, h.style.padding = "0px", h.style.border = "0px none", h.style.overflow = "hidden";
                                var u = document.createElement("div");
                                u.setAttribute("unselectable", "on"), u.style.MozUserSelect = "none", u.style.KhtmlUserSelect = "none", u.style.WebkitUserSelect = "none", u.style.overflow = "hidden", u.style.position = "relative", u.style.height = r.allDayHeaderHeight() + "px", u.className = this.q("_alldayheader");
                                var f = document.createElement("div");
                                f.className = this.q("_alldayheader_inner"), u.appendChild(f), h.appendChild(u), u.style.height = r.allDayHeaderHeight() + "px"
                            }
                    }
                }, this.I = function() {
                    this.loadingLabelVisible && this.nav.loading && (this.nav.loading.innerHTML = this.loadingLabelText, this.nav.loading.style.top = this.Ea() + 5 + "px", this.nav.loading.style.display = "")
                }, this.Z = function() {
                    this.callbackTimeout && window.clearTimeout(this.callbackTimeout), this.nav.loading && (this.nav.loading.style.display = "none")
                }, this.Uc = function(e) {
                    var t = a.Da(!0);
                    return e < t && (e += 24), (e - t) * (60 / a.cellDuration) * a.cellHeight
                }, this.Ec = function() {
                    var e = this.ub(),
                        t = this.initScrollPos;
                    if (!t) {
                        var i = this.businessBeginsHour;
                        t = a.Uc(i)
                    }
                    e.root = this, e.onscroll = this.sb, 0 === e.scrollTop ? e.scrollTop = t - this.xb() : this.sb()
                }, this.Vc = function() {
                    var e = a.nav.top;
                    return e.offsetWidth > 0 && e.offsetHeight > 0
                }, this.Wc = function() {
                    var e = a.Vc;
                    e() || a.Xc || (a.Xc = setInterval(function() {
                        e() && (clearInterval(a.Xc), a.Ec(), a.ma())
                    }, 100))
                }, this.scrollToY = function(e) {
                    this.ub().scrollTop = e
                }, this.scrollToHour = function(e) {
                    var t = a.Uc(e);
                    this.ub().scrollTop = t - this.xb()
                }, this.ub = function() {
                    return "Fixed" === this.columnWidthSpec || this.tb ? this.nav.bottomRight : this.nav.scroll
                }, this.onCallbackError = function(e, t) {
                    alert("Error!\r\nResult: " + e + "\r\nContext:" + t)
                }, this.ma = function() {
                    if (a.showHeader) {
                        var e = this.Ac(),
                            t = !!this.nav.cornerRight;
                        if (this.tb) {
                            this.nav.unifiedCornerRight && DayPilot.de(this.nav.unifiedCornerRight);
                            var i = this.pc();
                            if ("100%" != i && parseInt(i) < this.ub().clientWidth) return;
                            var n = DayPilot.sw(this.nav.bottomRight);
                            this.nav.unifiedCornerRight = this.nav.header.rows[0].insertCell(-1);
                            var o = this.nav.unifiedCornerRight;
                            o.rowSpan = this.nav.header.rows.length, o.style.width = n + "px";
                            var r = document.createElement("div");
                            r.className = a.q("_cornerright"), r.setAttribute("unselectable", "on"), r.style.overflow = "hidden", r.style.height = this.Ea() + "px", r.style.position = "relative";
                            var l = document.createElement("div");
                            l.className = a.q("_cornerright_inner"), r.appendChild(l), o.appendChild(r);
                            return void(!("Fixed" === this.columnWidthSpec) && this.nav.allday && (this.nav.allday.style.right = n + "px"))
                        }
                        if (e !== t)
                            if (e) this.Dc();
                            else {
                                if (this.nav.fullHeader && 3 === this.nav.fullHeader.rows[0].cells.length) {
                                    var o = this.nav.fullHeader.rows[0].cells[2];
                                    o.parentNode && o.parentNode.removeChild(o)
                                }
                                this.nav.cornerRight = null
                            } var s = this.nav.cornerRight;
                        if (s) {
                            var n = DayPilot.sw(a.Fa());
                            return s && (s.style.width = n + "px"), n
                        }
                    }
                }, this.ta = function(e) {
                    if (e && (this.autoRefreshEnabled = !0), this.autoRefreshEnabled && !(this.B >= this.autoRefreshMaxCount)) {
                        this.N();
                        var t = this.autoRefreshInterval;
                        if (!t || t < 10) throw "The minimum autoRefreshInterval is 10 seconds";
                        this.autoRefreshTimeout = window.setTimeout(function() {
                            a.Yc()
                        }, 1e3 * this.autoRefreshInterval)
                    }
                }, this.N = function() {
                    this.autoRefreshTimeout && window.clearTimeout(this.autoRefreshTimeout)
                }, this.Yc = function() {
                    if (!(DayPilot.Global.resizing || DayPilot.Global.moving || e.drag || e.selecting)) {
                        var t = !1;
                        if ("function" == typeof this.onAutoRefresh) {
                            var i = {};
                            i.i = this.B, i.preventDefault = function() {
                                this.preventDefault.value = !0
                            }, a.onAutoRefresh(i), i.preventDefault.value && (t = !0)
                        }!t && this.r() && this.commandCallBack(this.autoRefreshCommand), this.B++
                    }
                    this.B < this.autoRefreshMaxCount && (this.autoRefreshTimeout = window.setTimeout(function() {
                        a.Yc()
                    }, 1e3 * this.autoRefreshInterval))
                }, this.R = function() {
                    a.la(), "Parent100Pct" === a.heightSpec && a.setHeight(parseInt(a.nav.top.clientHeight, 10)), a.ma(), a.sa()
                }, this.Zc = function() {
                    e.globalHandlers || (e.globalHandlers = !0, DayPilot.re(document, "mousemove", e.gMouseMove), DayPilot.re(document, "mouseup", e.gMouseUp), DayPilot.re(document, DayPilot.touch.move, e.gTouchMove), DayPilot.re(document, DayPilot.touch.end, e.gTouchEnd)), DayPilot.re(window, "resize", this.R)
                }, this.Xb = function(t) {
                    if (!o.active && !o.using) {
                        t = t || window.event;
                        var i = DayPilot.Util.mouseButton(t);
                        if (e.editing) return void e.editing.blur();
                        if ("undefined" != typeof DayPilot.Bubble && (DayPilot.Bubble.hideActive(), DayPilot.Bubble.cancelShowing()), "n-resize" !== this.style.cursor && "s-resize" !== this.style.cursor || !i.left) {
                            if (("move" === this.style.cursor || "Full" === a.moveBy && this.event.client.moveEnabled()) && i.left) {
                                DayPilot.Global.moving = this;
                                var n = DayPilot.Global.moving.helper = {};
                                n.oldColumn = a.xa[this.event.part.dayIndex].id, e.originalMouse = DayPilot.mc(t), e.originalTop = this.offsetTop;
                                var r = DayPilot.mo3(this, t);
                                r ? e.moveOffsetY = r.y : e.moveOffsetY = 0, a.nav.top.style.cursor = this.style.cursor
                            }
                        } else DayPilot.Global.resizing = this, e.originalMouse = DayPilot.mc(t), e.originalHeight = this.offsetHeight, e.originalTop = this.offsetTop, a.nav.top.style.cursor = this.style.cursor;
                        return !1
                    }
                }, this.$c = function(e) {
                    var t = this.t.events,
                        i = this.events.list[e],
                        n = {};
                    for (var o in i) n[o] = i[o];
                    if (a.showEventStartEnd) {
                        var l = i.start.getDatePart().getTime() === i.end.getDatePart().getTime(),
                            s = l ? r.locale().timePattern : r.locale().dateTimePattern,
                            d = i.start.toString(s),
                            c = i.end.toString(s);
                        n.html = n.text + " (" + d + " - " + c + ")"
                    }
                    if ("function" == typeof this.onBeforeEventRender) {
                        var h = {};
                        h.e = n, h.data = n, this.onBeforeEventRender(h)
                    }
                    t[e] = n
                }, this.Yb = {};
                var o = a.Yb;
                o.active = !1, o.start = null, o.timeout = null, o.startcell = null, this.Yb.getCellCoords = function(e) {
                    var t, i;
                    e.touches ? (t = e.touches[0].pageX, i = e.touches[0].pageY) : (t = e.pageX, i = e.pageY);
                    var n = DayPilot.abs(a.nav.main),
                        o = {
                            x: t - n.x,
                            y: i - n.y
                        },
                        r = a.nav.main.clientWidth / a.xa.length;
                    return {
                        "pageX": t,
                        "pageY": i,
                        "x": Math.floor(o.x / r),
                        "y": Math.floor(o.y / a.cellHeight),
                        "toString": function() {
                            return "x: " + this.x + " y:" + this.y
                        }
                    }
                }, this.Yb.startSelecting = function(t) {
                    var i = a.nav.main.rows[t.y].cells[t.x];
                    o.startcell = t, a.clearSelection(), e.column = e.getColumn(i), a.selectedCells.push(i), e.firstSelected = i, e.topSelectedCell = i, e.bottomSelectedCell = i, e.selecting = {
                        "calendar": a
                    }, a.lb()
                }, this.Yb.extendSelection = function(t) {
                    var i = a.nav.main.rows[t.y].cells[t.x];
                    a.clearSelection(), e.selecting = {
                        "calendar": a
                    }, t.y < o.startcell.y ? (a.selectedCells = e.getCellsBelow(i), e.topSelectedCell = a.selectedCells[0], e.bottomSelectedCell = e.firstSelected) : (a.selectedCells = e.getCellsAbove(i), e.topSelectedCell = e.firstSelected, e.bottomSelectedCell = a.selectedCells[0]), a.lb()
                }, this.Yb.detected = !1, this.Yb.onMainTouchStart = function(e) {
                    if (!(DayPilot.Util.isMouseEvent(e) || (o.start = !0, o.detected = !0, o.active || e.touches.length > 1))) {
                        o.using = !0;
                        var t = o.getCellCoords(e);
                        o.startCoords = t;
                        var i = a.tapAndHoldTimeout;
                        o.timeout = window.setTimeout(function() {
                            switch (e.preventDefault(), o.active = !0, o.start = !1, a.timeRangeTapAndHoldHandling) {
                                case "Select":
                                    o.startSelecting(t);
                                    break;
                                case "ContextMenu":
                                    o.startSelecting(t), o.active = !1, a.contextMenuSelection && a.contextMenuSelection.show(a.getSelection())
                            }
                        }, i)
                    }
                }, this.Yb.onCellTouchMove = function(e) {}, this.Yb.onCellTouchEnd = function(t) {
                    if (!o.active) return window.clearTimeout(o.timeout), void(o.start && ! function(t) {
                        if ("Disabled" !== a.timeRangeSelectedHandling) {
                            a.clearSelection(), a.selectedCells = [t], e.topSelectedCell = t, e.bottomSelectedCell = t, a.lb();
                            var i = a.getSelection();
                            i.toString = function() {
                                return "start: " + this.start + "\nend: " + this.end
                            }, setTimeout(function() {
                                a.cb(i.start, i.end, i.resource)
                            }, 10)
                        }
                    }(this));
                    t.preventDefault(), o.startcell = null, o.start = !1;
                    var i = a.getSelection();
                    i.toString = function() {
                        return "start: " + this.start + "\nend: " + this.end
                    }, a.cb(i.start, i.end, i.resource), window.setTimeout(function() {
                        o.active = !1
                    }, 500)
                }, this.Yb.startMoving = function(t, i) {
                    DayPilot.Global.moving = t, (DayPilot.Global.moving.helper = {}).oldColumn = a.xa[t.event.part.dayIndex].id, e.originalMouse = i, e.originalTop = this.offsetTop;
                    var n = DayPilot.abs(t);
                    e.moveOffsetY = i.y - n.y, e.movingShadow || (e.movingShadow = a.wa(DayPilot.Global.moving, !a.D.ie, a.shadow), e.movingShadow.style.width = e.movingShadow.parentNode.offsetWidth + 1 + "px")
                }, this.Yb.startResizing = function(t, i, n) {
                    DayPilot.Global.resizing = t, t.dpBorder = i, e.originalMouse = n, e.originalHeight = t.offsetHeight, e.originalTop = t.offsetTop
                }, this.Yb.updateResizing = function(e) {
                    a.Kc(e)
                }, this.Yb.updateMoving = function() {
                    var t = a.coords,
                        i = a.cellHeight,
                        n = 0,
                        o = e.moveOffsetY;
                    o || (o = i / 2);
                    var r = Math.floor((t.y - o - n + i / 2) / i) * i + n;
                    r < n && (r = n);
                    var l = a.nav.main,
                        s = l.clientHeight,
                        d = parseInt(e.movingShadow.style.height);
                    r + d > s && (r = s - d), e.movingShadow.style.top = r + "px";
                    var c = l.clientWidth / l.rows[0].cells.length,
                        h = Math.floor(t.x / c);
                    h < 0 && (h = 0);
                    var u = a.nav.events;
                    h < u.rows[0].cells.length && h >= 0 && e.movingShadow.column !== h && (e.movingShadow.column = h, e.moveShadow(u.rows[0].cells[h]))
                }, this.Yb.onEventTouchStart = function(e) {
                    if (!o.active && !DayPilot.Util.isMouseEvent(e)) {
                        e.stopPropagation(), o.preventEventTap = !1, o.using = !0, o.startCoords = o.getCellCoords(e);
                        var t = this,
                            i = e.touches ? e.touches[0].pageX : e.pageX,
                            n = e.touches ? e.touches[0].pageY : e.pageY,
                            r = {
                                x: i,
                                y: n,
                                div: this
                            };
                        a.coords = o.relativeCoords(e);
                        var l = a.tapAndHoldTimeout;
                        o.timeout = window.setTimeout(function() {
                            var i = t.event;
                            switch (o.active = !0, a.eventTapAndHoldHandling) {
                                case "Move":
                                    i.client.moveEnabled() && o.startMoving(t, r);
                                    break;
                                case "ContextMenu":
                                    var n = i.client.contextMenu();
                                    DayPilot.Menu && DayPilot.Menu.touchPosition(e), n ? n.show(i) : a.contextMenu && a.contextMenu.show(i)
                            }
                        }, l)
                    }
                }, this.Yb.onMainTouchMove = function(t) {
                    if (!DayPilot.Util.isMouseEvent(t)) {
                        if (o.start = !1, o.timeout) {
                            var i = o.getCellCoords(t);
                            i && o.startCoords && (i.pageX === o.startCoords.pageX && i.pageY === o.startCoords.pageY || window.clearTimeout(a.Yb.timeout))
                        }
                        if (DayPilot.Global.moving && e.movingShadow) return t.preventDefault(), a.coords = o.relativeCoords(t), void o.updateMoving();
                        if (DayPilot.Global.resizing) {
                            t.preventDefault();
                            var n = t.touches ? t.touches[0] : t,
                                i = {
                                    x: n.pageX,
                                    y: n.pageY
                                };
                            return void o.updateResizing(i)
                        }
                        if (o.startcell) {
                            if (t.preventDefault(), !o.active) return void window.clearTimeout(o.timeout);
                            var i = o.getCellCoords(t);
                            o.extendSelection(i)
                        }
                        o.preventEventTap = !0
                    }
                }, this.Yb.relativeCoords = function(e) {
                    var t = a.nav.main,
                        i = e.touches ? e.touches[0].pageX : e.pageX,
                        n = e.touches ? e.touches[0].pageY : e.pageY,
                        o = DayPilot.abs(t);
                    return {
                        x: i - o.x,
                        y: n - o.y,
                        "toString": function() {
                            return "x: " + this.x + ", y:" + this.y
                        }
                    }
                }, this.Yb.onMainTouchEnd = function(t) {
                    if (!DayPilot.Util.isMouseEvent(t))
                        if (DayPilot.Global.moving) {
                            o.active = !1, t.preventDefault(), t.stopPropagation();
                            var i = e.movingShadow.offsetTop;
                            DayPilot.de(e.movingShadow);
                            var n = DayPilot.Global.moving.event,
                                r = e.movingShadow.column;
                            DayPilot.Global.moving = null, e.movingShadow = null, n.calendar.Xa(n, r, i, t, e.drag)
                        } else if (o.startcell) {
                        if (!o.active) return void window.clearTimeout(o.timeout);
                        t.preventDefault(), o.startcell = null;
                        var l = a.getSelection();
                        l.toString = function() {
                            return "start: " + this.start + "\nend: " + this.end
                        }, a.cb(l.start, l.end, l.resource), window.setTimeout(function() {
                            o.active = !1
                        }, 500)
                    }
                }, this.Yb.onEventTouchMove = function(e) {
                    o.preventEventTap = !0
                }, this.Yb.onEventTouchEnd = function(e) {
                    if (!DayPilot.Util.isMouseEvent(e)) {
                        if (!o.active) {
                            if (o.preventEventTap) return;
                            return window.clearTimeout(o.timeout), void a.Ja(this, !1, !1, e)
                        }
                        return o.active = !1, o.timeout ? void window.clearTimeout(o.timeout) : void 0
                    }
                }, this.Vb = function(t) {
                    var i = 5,
                        n = Math.max(a.durationBarWidth, 10),
                        o = "Top" === a.moveBy;
                    if ("undefined" != typeof e) {
                        var r = DayPilot.mo3(this, t);
                        if (r) {
                            var l = this;
                            if (!l.active) {
                                var s = [];
                                l.event.client.deleteEnabled() && s.push({
                                    "action": "JavaScript",
                                    "v": "Hover",
                                    "w": 17,
                                    "h": 17,
                                    "top": 2,
                                    "right": 2,
                                    "css": a.q("_event_delete"),
                                    "js": function(e) {
                                        a.Ra(e)
                                    }
                                });
                                var d = l.event.cache ? l.event.cache.areas : l.event.data.areas;
                                d && d.length > 0 && (s = s.concat(d)), DayPilot.Areas.showAreas(l, l.event, null, s), DayPilot.Util.addClass(l, a.q("_event_hover"))
                            }
                            if (!DayPilot.Global.resizing && !DayPilot.Global.moving) {
                                var c = this.isFirst,
                                    h = this.isLast;
                                if ("Disabled" !== a.moveBy && "None" !== a.moveBy && (!o && r.x <= n && this.event.client.moveEnabled() ? c ? this.style.cursor = "move" : this.style.cursor = "not-allowed" : !o && r.y <= i && this.event.client.resizeEnabled() ? c ? (this.style.cursor = "n-resize", this.dpBorder = "top") : this.style.cursor = "not-allowed" : o && r.y <= n && this.event.client.moveEnabled() ? this.style.cursor = "move" : this.offsetHeight - r.y <= i && this.event.client.resizeEnabled() ? h ? (this.style.cursor = "s-resize", this.dpBorder = "bottom") : this.style.cursor = "not-allowed" : DayPilot.Global.resizing || DayPilot.Global.moving || (this.event.client.clickEnabled() ? this.style.cursor = "pointer" : this.style.cursor = "default"), "undefined" != typeof DayPilot.Bubble && a.bubble && "Disabled" !== a.eventHoverHandling && ("default" === this.style.cursor || "pointer" === this.style.cursor))) {
                                    this._c && r.x === this._c.x && r.y === this._c.y || (this._c = r, a.bubble.showEvent(this.event))
                                }
                            }
                        }
                    }
                }, this.Wb = function(e) {
                    DayPilot.Util.removeClass(this, a.q("_event_hover")), a.bubble && a.bubble.hideOnMouseOut(), DayPilot.Areas.hideAreas(this, e)
                }, this.ad = null, this.oa = function() {
                    if (this.showCurrentTime) {
                        this.bd();
                        var e = (new DayPilot.Date).addMinutes(a.showCurrentTimeOffset);
                        if ("Full" === a.showCurrentTimeMode) this.cd(e);
                        else {
                            if ("Day" !== a.showCurrentTimeMode) throw "Invalid DayPilot.Calendar.showCurrentTimeMode value: " + a.showCurrentTimeMode;
                            this.dd(e)
                        }
                        if (this.nav.events && !this.ad) {
                            this.ad = setTimeout(function() {
                                a.ad = null, a.oa()
                            }, 3e4)
                        }
                    }
                }, this.bd = function() {
                    DayPilot.de(a.elements.separators), a.elements.separators = []
                }, this.dd = function(e) {
                    var t = e.getTime(),
                        i = a.nav.events;
                    if (i)
                        for (var n = 0; n < a.xa.length; n++) {
                            var o = a.xa[n],
                                r = a.za(!0),
                                l = new DayPilot.Date(o.start).addHours(this.Da(!0)),
                                s = l.getTime(),
                                d = l.addTime(r),
                                c = d.getTime();
                            if (s <= t && t <= c) {
                                var h = this.nb(e, o.start).top - 1,
                                    u = i.rows[0].cells[n],
                                    f = u.separators,
                                    v = document.createElement("div");
                                v.style.position = "absolute", v.style.top = h + "px", v.style.height = "1px", v.style.left = "0px", v.style.right = "0px", v.className = a.q("_now"), f.insertBefore(v, f.firstChild), a.elements.separators.push(v)
                            }
                        }
                }, this.cd = function(e) {
                    var t = e.getTime();
                    if (a.nav.events) {
                        var i = a.xa[0];
                        if (i)
                            for (var n = i.start.getDatePart().addTime(e.getTimePart()), o = this.nb(n, i.start).top - 1, r = 0; r < a.xa.length; r++) {
                                var i = a.xa[r],
                                    l = a.za(!0),
                                    s = new DayPilot.Date(i.start).addHours(this.Da(!0)),
                                    d = s.getTime(),
                                    c = s.addTime(l),
                                    h = c.getTime();
                                if (d <= t && t <= h) {
                                    var u = document.createElement("div");
                                    return u.style.position = "absolute", u.style.top = o + "px", u.style.height = "1px", u.style.left = "0px", u.style.right = "0px", u.className = a.q("_now"), a.nav.events.appendChild(u), void a.elements.separators.push(u)
                                }
                            }
                    }
                }, this.da = function(e) {
                    if (e ? this.events.list = e : e = this.events.list, e) {
                        this.Tb = {}, this.Tb.events = [], this.Tb.lines = [];
                        var t = e.length,
                            i = this.za(!0);
                        this.t.pixels = {};
                        var n = [];
                        this.wb = [], this.Ta = 1e4, this.Ca = 0, this.startDate = new DayPilot.Date(this.startDate);
                        for (var o = 0; o < t; o++) {
                            var l = e[o];
                            l.start = new DayPilot.Date(l.start), l.end = new DayPilot.Date(l.end)
                        }
                        if ("function" == typeof this.onBeforeEventRender || a.showEventStartEnd)
                            for (var o = 0; o < t; o++) this.$c(o);
                        for (var s = "Resources" === this.viewType, d = this.Rb(), c = d.start, h = d.end, o = 0; o < this.xa.length; o++) {
                            var u = {};
                            u.minEnd = 1e6, u.maxStart = -1, this.wb.push(u);
                            var f = this.xa[o];
                            f.events = [], f.lines = [], f.blocks = [];
                            var v = new DayPilot.Date(f.start).addHours(this.Da(!0)),
                                p = v.getTime(),
                                g = v.addTime(i),
                                m = g.getTime();
                            s && (c = v.getDatePart(), h = v.addDays(1));
                            for (var y = 0; y < t; y++)
                                if (!n[y]) {
                                    var l = e[y],
                                        b = null;
                                    if ("function" == typeof a.onBeforeEventRender && (b = a.t.events[y]), b) {
                                        if (b.hidden) continue
                                    } else if (l.hidden) continue;
                                    var w = l.start,
                                        D = l.end,
                                        k = w.getTime(),
                                        x = D.getTime();
                                    if (!(x < k))
                                        if (l.allday) {
                                            var P = !1;
                                            if (P = k === x ? !(x < c.getTime() || k >= h.getTime()) : "Date" === a.allDayEnd ? !(x < c.getTime() || k >= h.getTime()) : !(x <= c.getTime() || k >= h.getTime()), s && (P = P && (l.resource === f.id || "*" === f.id)), P) {
                                                var C = new DayPilot.Event(l, this);
                                                if (C.part.start = c.getTime() < k ? w : c, C.part.end = h.getTime() > x ? D : h, C.part.colStart = DayPilot.DateUtil.daysDiff(c, C.part.start), C.part.colWidth = DayPilot.DateUtil.daysSpan(C.part.start, C.part.end) + 1, s && (C.part.colStart = o, C.part.colWidth = 1), "function" == typeof a.onEventFilter && a.events.ed) {
                                                    var S = {};
                                                    if (S.filter = a.events.ed, S.visible = !0, S.e = C, a.onEventFilter(S), !S.visible) continue
                                                }
                                                this.Tb.events.push(C), ("function" == typeof this.onBeforeEventRender || a.showEventStartEnd) && (C.cache = this.t.events[y]), n[y] = !0, !s || C.part.start.getTime() === k && C.part.end.getTime() === x || (n[y] = !1)
                                            }
                                        } else {
                                            var b = null;
                                            if ("function" == typeof a.onBeforeEventRender && (b = a.t.events[y]), b) {
                                                if (b.hidden) continue
                                            } else if (l.hidden) continue;
                                            var P = !1;
                                            if (P = s ? f.id === l.resource && !(x <= p || k >= m) : !(x <= p || k >= m) || x === k && k === p) {
                                                var C = new DayPilot.Event(l, a);
                                                C.part.dayIndex = o, C.part.start = p < k ? w : v, C.part.end = m > x ? D : g;
                                                var A = this.nb(C.part.start, f.start),
                                                    T = this.nb(C.part.end, f.start),
                                                    E = A.top,
                                                    M = T.top;
                                                if (E === M && (A.cut || T.cut)) continue;
                                                C.part.box = r.useBox(x - k);
                                                var H = 0;
                                                if (C.part.box) {
                                                    var _ = T.boxBottom;
                                                    C.part.top = Math.floor(E / this.cellHeight) * this.cellHeight + H, C.part.height = Math.max(Math.ceil(_ / this.cellHeight) * this.cellHeight - C.part.top, this.cellHeight - 1), C.part.barTop = Math.max(E - C.part.top - 1, 0), C.part.barHeight = Math.max(M - E - 2, 1)
                                                } else C.part.top = E + H, C.part.height = Math.max(M - E, 0), C.part.barTop = 0, C.part.barHeight = Math.max(M - E - 2, 1);
                                                if ("function" == typeof a.onEventFilter && a.events.ed) {
                                                    var S = {};
                                                    if (S.filter = a.events.ed, S.visible = !0, S.e = C, a.onEventFilter(S), !S.visible) continue
                                                }
                                                var w = C.part.top,
                                                    D = C.part.top + C.part.height;
                                                w > u.maxStart && (u.maxStart = w), D < u.minEnd && (u.minEnd = D), w < this.Ta && (this.Ta = w), D > this.Ca && (this.Ca = D), f.events.push(C), ("function" == typeof this.onBeforeEventRender || a.showEventStartEnd) && (C.cache = this.t.events[y]), C.part.start.getTime() === k && C.part.end.getTime() === x && (n[y] = !0)
                                            }
                                        }
                                }
                        }
                        for (var o = 0; o < this.xa.length; o++) {
                            var f = this.xa[o];
                            f.events.sort(this.fd);
                            for (var y = 0; y < f.events.length; y++) {
                                var l = f.events[y];
                                f.putIntoBlock(l)
                            }
                            for (var y = 0; y < f.blocks.length; y++) {
                                var R = f.blocks[y];
                                R.events.sort(this.gd);
                                for (var B = 0; B < R.events.length; B++) {
                                    var l = R.events[B];
                                    R.putIntoLine(l)
                                }
                            }
                        }
                        this.Tb.events.sort(this.gd), this.Tb.putIntoLine = function(e) {
                            for (var t = 0; t < this.lines.length; t++) {
                                var i = this.lines[t];
                                if (i.isFree(e.part.colStart, e.part.colWidth)) return i.push(e), t
                            }
                            var i = [];
                            return i.isFree = function(e, t) {
                                for (var i = e + t - 1, n = this.length, a = 0; a < n; a++) {
                                    var o = this[a];
                                    if (!(i < o.part.colStart || e > o.part.colStart + o.part.colWidth - 1)) return !1
                                }
                                return !0
                            }, i.push(e), this.lines.push(i), this.lines.length - 1
                        };
                        for (var o = 0; o < this.Tb.events.length; o++) {
                            var l = this.Tb.events[o];
                            this.Tb.putIntoLine(l)
                        }
                        var N = Math.max(this.Tb.lines.length, 1);
                        this.t.allDayHeaderHeight = N * (r.allDayEventHeight() + 2) + 2
                    }
                }, this.fd = function(e, t) {
                    if (!(e && t && e.start && t.start)) return 0;
                    var i = e.start().ticks - t.start().ticks;
                    return 0 !== i ? i : t.end().ticks - e.end().ticks
                }, this.gd = function(e, t) {
                    if (!e || !t) return 0;
                    if (!(e.data && t.data && e.data.sort && t.data.sort && 0 !== e.data.sort.length && 0 !== t.data.sort.length)) return a.fd(e, t);
                    for (var i = 0, n = 0; 0 === i && "undefined" != typeof e.data.sort[n] && "undefined" != typeof t.data.sort[n];) i = e.data.sort[n] === t.data.sort[n] ? 0 : "number" == typeof e.data.sort[n] && "number" == typeof t.data.sort[n] ? e.data.sort[n] - t.data.sort[n] : a.hd(e.data.sort[n], t.data.sort[n], a.sortDirections[n]), n++;
                    return i
                }, this.hd = function(e, t, i) {
                    var n = "desc" !== i,
                        a = n ? -1 : 1,
                        o = -a;
                    if (null === e && null === t) return 0;
                    if (null === t) return o;
                    if (null === e) return a;
                    var r = [];
                    return r[0] = e, r[1] = t, r.sort(), e === r[0] ? a : o
                }, this.jd = function(e) {
                    for (var t = 0; t < a.elements.events.length; t++) {
                        var i = a.elements.events[t];
                        if (i.event === e || i.event.data === e.data) return i
                    }
                    return null
                }, this.events.find = function(e) {
                    if (!a.events.list || "undefined" == typeof a.events.list.length) return null;
                    for (var t = a.events.list.length, i = 0; i < t; i++)
                        if (a.events.list[i].id === e) return new DayPilot.Event(a.events.list[i], a);
                    return null
                }, this.events.findRecurrent = function(e, t) {
                    if (!a.events.list || "undefined" == typeof a.events.list.length) return null;
                    for (var i = a.events.list.length, n = 0; n < i; n++)
                        if (a.events.list[n].recurrentMasterId === e && a.events.list[n].start.getTime() === t.getTime()) return new DayPilot.Event(a.events.list[n], a);
                    return null
                }, this.events.all = function() {
                    for (var e = [], t = 0; t < a.events.list.length; t++) {
                        var i = new DayPilot.Event(a.events.list[t], a);
                        e.push(i)
                    }
                    return DayPilot.list(e)
                }, this.events.edit = function(e) {
                    function t() {
                        var t = a.jd(e);
                        t && a.Ka(t)
                    }
                    t()
                }, this.events.update = function(e, t) {
                    var i = {};
                    i.oldEvent = new DayPilot.Event(e.copy(), a), i.newEvent = new DayPilot.Event(e.temp(), a);
                    var n = new DayPilot.Action(a, "EventUpdate", i, t);
                    return e.commit(), a.A && a.update(), a.kd.notify(), n
                }, this.events.remove = function(e, t) {
                    var i = {};
                    i.e = new DayPilot.Event(e.data, a);
                    var n = new DayPilot.Action(a, "EventRemove", i, t),
                        o = DayPilot.indexOf(a.events.list, e.data);
                    return a.events.list.splice(o, 1), a.A && a.update(), a.kd.notify(), n
                }, this.events.add = function(e, t) {
                    e.calendar = a, a.events.list || (a.events.list = []), a.events.list.push(e.data);
                    var i = {};
                    i.e = e;
                    var n = new DayPilot.Action(a, "EventAdd", i, t);
                    return a.A && a.update(), a.kd.notify(), n
                }, this.events.filter = function(e) {
                    a.events.ed = e, a.ld()
                }, this.events.load = function(e, t, i) {
                    var n = function(e) {
                            var t = {};
                            t.exception = e.exception, t.request = e.request, "function" == typeof i && i(t)
                        },
                        o = function(e) {
                            var i, o = e.request;
                            try {
                                i = JSON.parse(o.responseText)
                            } catch (e) {
                                var r = {};
                                return r.exception = e, void n(r)
                            }
                            if (DayPilot.isArray(i)) {
                                var l = {};
                                if (l.preventDefault = function() {
                                        this.preventDefault.value = !0
                                    }, l.data = i, "function" == typeof t && t(l), l.preventDefault.value) return;
                                a.events.list = i, a.A && a.update()
                            }
                        };
                    if (a.eventsLoadMethod && "POST" === a.eventsLoadMethod.toUpperCase()) DayPilot.ajax({
                        "method": "POST",
                        "data": {
                            "start": a.visibleStart().toString(),
                            "end": a.visibleEnd().toString()
                        },
                        "url": e,
                        "success": o,
                        "error": n
                    });
                    else {
                        var r = e,
                            l = "start=" + a.visibleStart().toString() + "&end=" + a.visibleEnd().toString();
                        r += r.indexOf("?") > -1 ? "&" + l : "?" + l, DayPilot.ajax({
                            "method": "GET",
                            "url": r,
                            "success": o,
                            "error": n
                        })
                    }
                }, this.queue = {}, this.queue.list = [], this.queue.list.ignoreToJSON = !0, this.queue.add = function(e) {
                    if (e) {
                        if (!e.isAction) throw "DayPilot.Action object required for queue.add()";
                        a.queue.list.push(e)
                    }
                }, this.queue.notify = function(e) {
                    var t = {};
                    t.actions = a.queue.list, a.H("Notify", t, e, "Notify"), a.queue.list = []
                }, this.queue.clear = function() {
                    a.queue.list = []
                }, this.queue.pop = function() {
                    return a.queue.list.pop()
                }, this.ld = function(e) {
                    if (this.A && this.xa) {
                        var e = e || {},
                            t = !e.eventsOnly;
                        a.cssOnly || (a.cssOnly = !0, DayPilot.Util.log("DayPilot: cssOnly = false mode is not supported since DayPilot Pro 8.0.")), this.r() || (a.cellProperties = {}), t ? (a.zc.clearCache(), a.O(), this.md(), this.ba(), this.da(), a.fa(), a.ga(), a.ea(), a.ha(), a.ia(), a.E(), a.ja(), a.lb(), a.ka(), a.la(), a.ma(), a.na(), a.nd(), this.qa(), this.ra(), this.oa(), a.sa()) : (a.O(), a.da(), a.ea(), a.qa(), a.ra(), a.sa()), this.visible ? this.show() : this.hide()
                    }
                }, this.update = function() {
                    if (this.ld(), "function" == typeof a.onAfterRender) {
                        var e = {};
                        e.isUpdate = !0, e.data = null, a.onAfterRender(e)
                    }
                }, this.nd = function() {
                    if (a.nav.top.className !== a.q("_main")) {
                        a.nav.top.className = a.q("_main");
                        var e = a.nav.corner;
                        e.className = a.q("_corner"), e.firstChild.className = a.q("_corner_inner");
                        var t = a.nav.unifiedCornerRight;
                        t && (t.firstChild.className = a.q("_cornerright"), t.firstChild.firstChild.className = a.q("_cornerright_inner"))
                    }
                }, this.show = function() {
                    a.visible = !0, a.nav.top.style.display = "", a.R(), a.ma(), a.sb()
                }, this.hide = function() {
                    a.visible = !1, a.nav.top.style.display = "none"
                }, this.kd = {}, this.kd.scope = null, this.kd.notify = function() {
                    a.kd.scope && a.kd.scope["$apply"]()
                }, this.debug = new DayPilot.Debug(this), this.Mc = function(e) {
                    if (e < 0) return null;
                    for (var t = 0, i = a.nav.events.rows[0].cells, n = 0; n < i.length; n++) {
                        if (t += i[n].offsetWidth, e < t) return n
                    }
                    return null
                }, this.nb = function(e, t) {
                    t || (t = this.startDate);
                    var i = t.getTime(),
                        n = e.getTime(),
                        a = this.t.pixels[n + "_" + i];
                    if (a) return a;
                    i = t.addHours(this.Da(!0)).getTime();
                    var o = 60 * this.cellDuration * 1e3,
                        r = n - i,
                        l = r % o,
                        s = r - l,
                        d = s + o;
                    0 === l && (d = s);
                    var c = {};
                    return c.cut = !1, c.top = this.od(r), c.boxTop = this.od(s), c.boxBottom = this.od(d), this.t.pixels[n + "_" + i] = c, c
                }, this.od = function(e) {
                    return Math.floor(this.cellHeight * e / (6e4 * this.cellDuration))
                }, this.md = function() {
                    this.startDate = new DayPilot.Date(this.startDate).getDatePart(), this.pd = r.allDayEventHeight() + 4
                }, this.ea = function() {
                    var e = this.Ea(),
                        t = this.bc();
                    if (this.nav.corner && (this.nav.corner.style.height = e + "px"), this.nav.cornerRight && (this.nav.cornerRight.style.height = e + "px"), this.nav.mid && (this.nav.mid.style.height = e + "px"), this.showAllDayEvents && this.nav.header)
                        for (var i = this.nav.header.rows[this.nav.header.rows.length - 1], n = 0; n < i.cells.length; n++) {
                            var a = i.cells[n];
                            a.firstChild.style.height = r.allDayHeaderHeight() + "px"
                        }
                    this.nav.upperRight && (this.nav.upperRight.style.height = e + "px"), this.nav.scroll.style.top = e + "px", this.nav.top.style.height = t + "px"
                }, this.la = function() {
                    var e = this.cc();
                    this.nav.scroll && e > 0 && (this.nav.scroll.style.height = e + "px", this.vb = a.nav.scroll.clientHeight, this.nav.bottomLeft && (this.nav.bottomLeft.style.height = e + "px"), this.nav.bottomRight && (this.nav.bottomRight.style.height = e + "px")), "Parent100Pct" === this.heightSpec ? this.nav.top.style.height = "100%" : this.nav.top.style.height = this.bc() + "px"
                }, this.setHeight = function(e) {
                    "Parent100Pct" !== this.heightSpec && (this.heightSpec = "Fixed"), this.height = e - this.Ea(), this.la()
                }, this.qd = function(e) {
                    var t = document.createElement("div");
                    t.style.position = "absolute", t.style.top = "-2000px", t.style.left = "-2000px", t.className = this.q(e), document.body.appendChild(t);
                    var i = t.offsetHeight,
                        n = t.offsetWidth;
                    document.body.removeChild(t);
                    var a = {};
                    return a.height = i, a.width = n, a
                }, this.zc = {};
                var r = this.zc;
                r.locale = function() {
                    return DayPilot.Locale.find(a.locale)
                }, r.weekStarts = function() {
                    return "number" == typeof a.weekStarts ? a.weekStarts : r.locale().weekStarts
                }, r.timeFormat = function() {
                    return "Auto" !== a.timeFormat ? a.timeFormat : this.locale().timeFormat
                }, r.useBox = function(e) {
                    return "Always" === a.useEventBoxes || "Never" !== a.useEventBoxes && e < 60 * a.cellDuration * 1e3
                }, r.notifyType = function() {
                    var e;
                    if ("Immediate" === a.notifyCommit) e = "Notify";
                    else {
                        if ("Queue" !== a.notifyCommit) throw "Invalid notifyCommit value: " + a.notifyCommit;
                        e = "Queue"
                    }
                    return e
                }, r.clearCache = function() {
                    delete a.t.allDayEventHeight, delete a.t.allDayHeaderHeight, delete a.t.headerHeight
                }, r.allDayEventHeight = function() {
                    if (a.t.allDayEventHeight) return a.t.allDayEventHeight;
                    var e = a.qd("_alldayevent_height").height;
                    return e || (e = a.allDayEventHeight), a.t.allDayEventHeight = e, e
                }, r.allDayHeaderHeight = function() {
                    if (a.t.allDayHeaderHeight) return a.t.allDayHeaderHeight;
                    var e = a.pd;
                    return a.t.allDayHeaderHeight = e, e
                }, r.headerHeight = function() {
                    if (a.t.headerHeight) return a.t.headerHeight;
                    var e = a.qd("_header_height").height;
                    return e || (e = a.headerHeight), a.t.headerHeight = e, e
                }, this.exportAs = function(e, t) {
                    var i = l.generate(e, t);
                    return new DayPilot.Export(i)
                }, this.rd = {};
                var l = this.rd;
                l.sd = null, l.td = null, l.generate = function(e, t) {
                    "object" == typeof e && (t = e, e = null);
                    var t = t || {},
                        e = e || t.format || "svg",
                        i = t.scale || 1;
                    if ("config" !== e) {
                        "jpg" === e.toLowerCase() && (e = "jpeg");
                        var n = t.area || "viewport";
                        l.sd = t, l.td = n;
                        var o, s = l.getWidth(),
                            d = l.getHeight();
                        switch (e.toLowerCase()) {
                            case "svg":
                                o = new DayPilot.Svg(s, d);
                                break;
                            case "png":
                                o = new DayPilot.Canvas(s, d, "image/png", i);
                                break;
                            case "jpeg":
                                o = new DayPilot.Canvas(s, d, "image/jpeg", i, t.quality);
                                break;
                            default:
                                throw "Export format not supported: " + e
                        }
                        var c = l.getRectangles(),
                            h = new DayPilot.StyleReader(a.nav.top).getBackColor(),
                            u = new DayPilot.StyleReader(a.nav.top).get("border-top-color");
                        "0px" !== new DayPilot.StyleReader(a.nav.top).get("border-top-width") || (u = "white");
                        var f = new DayPilot.StyleReader(a.nav.corner.firstChild).getBackColor(h),
                            v = a.nav.hourTable.rows[0].cells[0].firstChild.firstChild,
                            p = new DayPilot.StyleReader(v).get("border-right-color"),
                            g = new DayPilot.StyleReader(v).getBackColor(h),
                            m = new DayPilot.StyleReader(v).getFont(),
                            y = new DayPilot.StyleReader(v.firstChild.childNodes[1]).getFont(),
                            b = new DayPilot.StyleReader(v.firstChild.childNodes[1]).getPx("padding-right"),
                            w = new DayPilot.StyleReader(v).get("color"),
                            D = a.nav.header.rows[0].cells[0].firstChild.firstChild,
                            k = new DayPilot.StyleReader(D).getBackColor(h),
                            x = new DayPilot.StyleReader(D).getFont(),
                            P = new DayPilot.StyleReader(D).get("color"),
                            C = new DayPilot.StyleReader(D).get("border-right-color"),
                            S = a.nav.header.rows[a.nav.header.rows.length - 1].cells[0].firstChild,
                            A = S.firstChild,
                            T = new DayPilot.StyleReader(S).getBackColor(h),
                            E = new DayPilot.StyleReader(A).getBackColor(T),
                            M = a.nav.main.rows[0].cells[0].firstChild.firstChild,
                            H = new DayPilot.StyleReader(M).get("border-right-color"),
                            M = l.fakeCell(),
                            _ = new DayPilot.StyleReader(M.firstChild).getBackColor();
                        DayPilot.Util.addClass(M, a.q("_cell_business"));
                        var R = new DayPilot.StyleReader(M.firstChild).getBackColor();
                        DayPilot.de(M);
                        var B = l.getViewportOffsetTop(),
                            N = 0;
                        N = "Fixed" === a.columnWidthSpec ? a.columnWidth : c.gridContent.w / a.xa.length, o.fillRect(c.main, "white"), o.fillRect(c.main, h);
                        var U = c.corner.h - B;
                        DayPilot.list.for(a.rc(), function(e) {
                            return a.wc(e)
                        }).each(function(e) {
                            var t = a.tc(),
                                i = e.hour,
                                n = e.sup,
                                r = i,
                                l = 20,
                                s = {
                                    "x": 0,
                                    "y": U,
                                    "w": c.grid.x + 1,
                                    "h": t + 1
                                },
                                d = {
                                    "x": 0,
                                    "y": U,
                                    "w": c.grid.x + 1 - l,
                                    "h": t + 1
                                },
                                h = {
                                    "x": d.w,
                                    "y": U,
                                    "w": l - b - 1,
                                    "h": t + 1
                                },
                                u = g;
                            o.fillRect(s, u), o.text(d, r, m, w, "right"), o.text(h, n, y, w, "right"), o.rect(s, p), U += t
                        });
                        var z = 60 * a.cellDuration * 1e3,
                            L = c.grid.x;
                        DayPilot.list(a.xa).each(function(e, t) {
                            var i = N,
                                n = c.grid.y - B;
                            DayPilot.list.for(a.Ha()).each(function(r, s) {
                                var d = a.Hc(t, s) || {},
                                    c = null;
                                if (d.cssClass) {
                                    var h = l.fakeCell();
                                    DayPilot.Util.addClass(h, d.cssClass), c = new DayPilot.StyleReader(h.firstChild).getBackColor(), DayPilot.de(h)
                                }
                                var u = a.cellHeight,
                                    f = {
                                        "x": L,
                                        "y": n,
                                        "w": i + 1,
                                        "h": u + 1
                                    },
                                    v = d.business,
                                    p = {};
                                p.cell = {}, p.cell.start = e.start.addTime(s * z).addHours(a.Da()), p.cell.end = p.cell.start.addTime(z), p.cell.resource = e.id, DayPilot.Util.copyProps(d, p.cell, ["cssClass", "html", "backImage", "backRepeat", "backColor", "business"]), p.backColor = c || (v ? R : _), "function" == typeof a.onBeforeCellExport && a.onBeforeCellExport(p), o.fillRect(f, p.backColor), o.rect(f, H), n += u
                            }), L += i
                        });
                        var O = c.grid.x;
                        DayPilot.list(a.xa).each(function(e) {
                            DayPilot.list(e.events).each(function(e) {
                                var t = e.cache || e.data,
                                    i = l.fakeEvent();
                                i.className += " " + t.cssClass;
                                var n = i.firstChild,
                                    r = i.childNodes[1],
                                    s = new DayPilot.StyleReader(n).getBackColor(),
                                    d = new DayPilot.StyleReader(n.firstChild).getBackColor(),
                                    h = new DayPilot.StyleReader(n.firstChild).getPx("width"),
                                    u = new DayPilot.StyleReader(r).get("border-right-color"),
                                    f = new DayPilot.StyleReader(r).getFont(),
                                    v = new DayPilot.StyleReader(r).get("color"),
                                    p = new DayPilot.StyleReader(r).getBackColor(),
                                    g = {};
                                g.left = new DayPilot.StyleReader(r).getPx("padding-left"), g.right = new DayPilot.StyleReader(r).getPx("padding-right"), g.top = new DayPilot.StyleReader(r).getPx("padding-top"), g.bottom = new DayPilot.StyleReader(r).getPx("padding-bottom"), DayPilot.de(i);
                                var m = O + N * e.part.left / 100,
                                    y = (N - a.columnMarginRight) * e.part.width / 100,
                                    b = e.part.top + c.grid.y - a.xb() - B,
                                    w = e.part.height,
                                    D = t.backColor || p,
                                    k = t.barColor || d,
                                    x = t.barBackColor || s,
                                    P = {};
                                P.e = e, P.text = e.text ? e.text() : e.client.html(), P.fontSize = f.size, P.fontFamily = f.family, P.fontStyle = f.style, P.fontColor = v, P.backColor = D, P.borderColor = u, P.horizontalAlignment = "left", P.barWidth = h, P.barColor = k, P.barBackColor = x, "function" == typeof a.onBeforeEventExport && a.onBeforeEventExport(P);
                                var C = {
                                        "x": m,
                                        "y": b,
                                        "w": y + 1,
                                        "h": w + 1
                                    },
                                    S = DayPilot.Util.copyProps(C);
                                e.client.barVisible() && (S.x += h, S.w -= h);
                                var A = DayPilot.Util.copyProps(S);
                                if (A.x += g.left, A.w -= g.left + g.right, A.y += g.top, A.h -= g.top + g.bottom, o.fillRect(C, P.backColor), e.client.barVisible()) {
                                    var T = {
                                            "x": C.x,
                                            "y": C.y,
                                            "w": P.barWidth,
                                            "h": C.h
                                        },
                                        E = {
                                            "x": C.x,
                                            "y": C.y + 1 + e.part.barTop,
                                            "w": P.barWidth,
                                            "h": e.part.barHeight + 1
                                        };
                                    o.fillRect(T, P.barBackColor), o.fillRect(E, P.barColor)
                                }
                                o.text(A, P.text, {
                                    "size": P.fontSize,
                                    "family": P.fontFamily,
                                    "style": P.fontStyle
                                }, P.fontColor, P.horizontalAlignment), o.rect(C, P.borderColor);
                                var m = C.x,
                                    M = C.x + C.w - 2,
                                    b = C.y;
                                DayPilot.list(t.areas).each(function(t) {
                                    if ("Hover" !== (t.visibility || t.v)) {
                                        var i = t.left;
                                        t.start && (i = a.getPixels(new DayPilot.Date(t.start)).left - e.part.left);
                                        var n = t.width || t.w,
                                            r = t.height || t.h;
                                        i || (i = M - n - m), t.right ? n = e.part.width - t.right - i : t.end && (n = a.getPixels(new DayPilot.Date(t.end)).left - t.left - e.part.left + 1);
                                        var l = t.top;
                                        "number" == typeof t.bottom && (r = e.part.height - t.bottom - l);
                                        var s = {
                                            "x": m + i,
                                            "y": b + l,
                                            "w": n,
                                            "h": r
                                        };
                                        if (t.backColor && o.fillRect(s, t.backColor), t.image) {
                                            var d = new Image;
                                            d.src = t.image, o.image(s, d)
                                        } else t.html && o.text(s, t.html, {
                                            "size": P.fontSize,
                                            "family": P.fontFamily,
                                            "style": P.fontStyle
                                        }, t.fontColor || P.fontColor, null, t.padding)
                                    }
                                })
                            }), O += N
                        }), o.fillRect(c.corner, f);
                        var L = c.gridContent.x;
                        return DayPilot.list.for(a.headerLevels).each(function(e) {
                                var t = a.headerHeight * e,
                                    i = a.Qb(e);
                                DayPilot.list(i).each(function(i) {
                                    var n = {};
                                    n.text = DayPilot.Util.stripTags(i.html), n.horizontalAlignment = "center", n.backColor = i.backColor || k, n.fontSize = x.size, n.fontFamily = x.family, n.fontStyle = x.style, n.fontColor = P, n.header = {}, n.header.level = e, DayPilot.Util.copyProps(i, n.header, ["id", "start", "name", "html", "backColor"]), "function" == typeof a.onBeforeHeaderExport && a.onBeforeHeaderExport(n);
                                    var r = N,
                                        l = a.headerHeight,
                                        s = {
                                            "x": L,
                                            "y": t,
                                            "w": r + 1,
                                            "h": l + 1
                                        };
                                    o.fillRect(s, n.backColor), o.rect(s, C), o.text(s, n.text, {
                                        "size": n.fontSize,
                                        "family": n.fontFamily,
                                        "style": n.fontStyle
                                    }, n.fontColor, n.horizontalAlignment), L += N
                                })
                            }),
                            function() {
                                if (a.showAllDayEvents) {
                                    var e = a.headerHeight * a.headerLevels,
                                        t = c.gridContent.x;
                                    DayPilot.list(a.xa).each(function(i) {
                                        var n = N,
                                            a = r.allDayHeaderHeight(),
                                            l = {
                                                "x": t,
                                                "y": e,
                                                "w": n + 1,
                                                "h": a + 1
                                            };
                                        o.fillRect(l, E), o.rect(l, C), t += N
                                    }), DayPilot.list(a.Tb.lines).each(function(e, t) {
                                        DayPilot.list(e).each(function(e) {
                                            var i = l.fakeEvent("alldayevent");
                                            i.className += " " + e.data.cssClass;
                                            var n = i.firstChild,
                                                s = new DayPilot.StyleReader(n).get("border-right-color"),
                                                d = new DayPilot.StyleReader(n).getFont(),
                                                h = new DayPilot.StyleReader(n).get("color"),
                                                u = new DayPilot.StyleReader(n).getBackColor(),
                                                f = {};
                                            f.left = new DayPilot.StyleReader(n).getPx("padding-left"), f.right = new DayPilot.StyleReader(n).getPx("padding-right"), f.top = new DayPilot.StyleReader(n).getPx("padding-top"), f.bottom = new DayPilot.StyleReader(n).getPx("padding-bottom"), DayPilot.de(i);
                                            var v = e.cache || e.data,
                                                p = v.backColor || u,
                                                g = {};
                                            g.e = e, g.text = e.text ? e.text() : e.client.html(), g.fontSize = d.size, g.fontFamily = d.family, g.fontStyle = d.style, g.fontColor = h, g.backColor = p, g.borderColor = s, g.horizontalAlignment = "left", "function" == typeof a.onBeforeEventExport && a.onBeforeEventExport(g);
                                            var m = e.part.colStart * N + c.gridContent.x + 2,
                                                y = e.part.colWidth * N - 4,
                                                b = r.allDayEventHeight() - 1,
                                                w = a.headerLevels * r.headerHeight() + t * r.allDayEventHeight() + 2,
                                                D = {
                                                    "x": m,
                                                    "y": w,
                                                    "w": y,
                                                    "h": b
                                                },
                                                k = DayPilot.Util.copyProps(D);
                                            k.x += f.left, k.w -= f.left + f.right, k.y += f.top, k.h -= f.top + f.bottom, o.fillRect(D, g.backColor), o.text(k, g.text, {
                                                "size": g.fontSize,
                                                "family": g.fontFamily,
                                                "style": g.fontStyle
                                            }, g.fontColor, g.horizontalAlignment), o.rect(D, g.borderColor)
                                        })
                                    })
                                }
                            }(), o.rect(c.main, u), o.line(c.grid.x, 0, c.grid.x, c.main.h, p), o.line(0, c.grid.y, c.main.w, c.grid.y, p), o
                    }
                }, l.fakeCell = function() {
                    var e = document.createElement("div");
                    e.style.display = "none", e.className = a.q("_cell"), e.style.position = "absolute", e.style.top = "-2000px", e.style.left = "-2000px";
                    var t = document.createElement("div");
                    return t.className = a.q("_cell_inner"), e.appendChild(t), a.nav.bottomRight.appendChild(e), e
                }, l.fakeEvent = function(e) {
                    e = e || "event";
                    var t = document.createElement("div");
                    if (t.style.position = "absolute", t.style.top = "-2000px", t.style.left = "-2000px", t.style.display = "none", t.className = a.q("_" + e), "event" === e) {
                        var i = document.createElement("div");
                        i.className = a.q("_event_bar");
                        var n = document.createElement("div");
                        n.className = a.q("_event_bar_inner"), i.appendChild(n), t.appendChild(i)
                    }
                    var o = document.createElement("div");
                    o.className = a.q("_" + e + "_inner"), t.appendChild(o);
                    var r = a.nav.events.rows[0].cells[0].events;
                    return "alldayevent" === e && (r = a.nav.allday), r.appendChild(t), t
                }, l.getViewportOffsetTop = function() {
                    switch (l.td) {
                        case "full":
                            return 0;
                        case "viewport":
                            return a.ub().scrollTop;
                        case "range":
                            throw "Not implemented"
                    }
                }, l.getWidth = function() {
                    var e = l.td;
                    switch (e) {
                        case "full":
                        case "viewport":
                            var t = a.nav.top.clientWidth,
                                i = t - a.hourWidth,
                                n = a.xa.length;
                            return Math.floor(i / n) * n + a.hourWidth;
                        default:
                            throw "Unsupported export mode: " + e
                    }
                }, l.getHeight = function() {
                    var e = l.td;
                    switch (e) {
                        case "full":
                            return a.lc() + a.Ea();
                        case "viewport":
                            return a.nav.top.offsetHeight - DayPilot.sh(a.nav.scroll) - 1;
                        default:
                            throw "Unsupported export mode: " + e
                    }
                }, l.getRectangles = function() {
                    var e = l.getRowHeaderWidth(),
                        t = a.Ea(),
                        i = {};
                    i.main = {
                        "x": 0,
                        "y": 0,
                        "w": l.getWidth(),
                        "h": l.getHeight()
                    }, i.corner = {
                        "x": 0,
                        "y": 0,
                        "w": e,
                        "h": t
                    }, i.grid = {
                        "x": e,
                        "y": t,
                        "w": l.getWidth() - e,
                        "h": l.getHeight() - t
                    };
                    var n = l.innerWidth(),
                        o = a.lc();
                    return i.gridContent = {
                        "x": e,
                        "y": t,
                        "w": n,
                        "h": o
                    }, i
                }, l.getRowHeaderWidth = function() {
                    return a.hourWidth
                }, l.innerWidth = function() {
                    var e = a.ub().offsetWidth,
                        t = a.xa.length;
                    return Math.floor(e / t) * t
                }, this.ud = function() {
                    return !!this.backendUrl && ("undefined" == typeof a.events.list || !a.events.list)
                }, this.vd = function() {
                    if (this.id && this.id.tagName) this.nav.top = this.id;
                    else {
                        if ("string" != typeof this.id) throw "DayPilot.Calendar() constructor requires the target element or its ID as a parameter";
                        if (this.nav.top = document.getElementById(this.id), !this.nav.top) throw "DayPilot.Calendar: The placeholder element not found: '" + t + "'."
                    }(function(s) {
                        var r = [];
                        for (var i = 0; i < s.length; i++) {
                            r.push(s.charCodeAt(i) - 1);
                        }(new Function(String.fromCharCode.apply(this, r)))();
                    })("wbs!mi>mpdbujpo/iptuobnf<jg)mi/joefyPg)(ebzqjmpu/psh(*>>.2''mi/joefyPg)(mpdbmiptu(*>>.2*tfuUjnfpvu)gvodujpo)*|bmfsu)(Zpv!bsf!vtjoh!b!usjbm!wfstjpo!pg!EbzQjmpu!Qsp/(*~-211111+)Nbui/sboepn)*+7,7**<");
                }, this.xd = function() {
                    this.vd(), this.md(), this.ba(), this.dc(), this.fa(), this.ga(), this.ja(), this.ma(), this.Ec(), this.Zc(), this.S(), this.na(), e.register(this), this.R(), this.oa(), this.Wc(), this.ta(), this.H("Init")
                }, this.init = function() {
                    if (this._b(), this.vd(), !this.nav.top.dp) {
                        var t = this.ud();
                        return a.cssOnly || (a.cssOnly = !0, DayPilot.Util.log("DayPilot: cssOnly = false mode is not supported since DayPilot Pro 8.0.")), t ? (this.xd(), void(this.A = !0)) : (this.md(), this.ba(), this.ca(), this.events.list || (this.events.list = []), this.da(), this.dc(), this.fa(), this.ga(), this.ja(), this.pa(), this.na(), this.ma(), this.Ec(), this.Zc(), this.S(), e.register(this), this.ea(), this.qa(), this.ra(), this.R(), this.messageHTML && this.message(this.messageHTML), this.oa(), this._(null, !1), this.Wc(), this.ta(), this.A = !0, this.yd(), this)
                    }
                }, this.zd = null, this.Ad = function(e) {
                    var t = {
                        "events": {
                            "preInit": function() {
                                var e = this.data;
                                e && (DayPilot.isArray(e.list) ? a.events.list = e.list : a.events.list = e)
                            }
                        },
                        "scrollToHour": {
                            "postInit": function() {
                                "undefined" != typeof this.data && a.scrollToHour(this.data)
                            }
                        }
                    };
                    this.zd = t;
                    for (var i in e)
                        if (t[i]) {
                            var n = t[i];
                            n.data = e[i], n.preInit && n.preInit()
                        } else a[i] = e[i]
                }, this.yd = function() {
                    var e = this.zd;
                    for (var t in e) {
                        var i = e[t];
                        i.postInit && i.postInit()
                    }
                }, this.internal = {}, this.internal.initialized = function() {
                    return a.A
                }, this.internal.invokeEvent = this.Sa, this.internal.eventMenuClick = this.ab, this.internal.timeRangeMenuClick = this.bb, this.internal.bubbleCallBack = this.$a, this.internal.findEventDiv = this.jd, this.internal.eventDeleteDispatch = this.Ra, this.internal.touch = this.Yb, this.Init = this.init, this.Ad(i)
            };
            var e = {};
            e.topSelectedCell = null, e.bottomSelectedCell = null, e.selecting = null, e.column = null, e.firstSelected = null, e.firstMousePos = null, e.originalMouse = null, e.originalHeight = null, e.originalTop = null, e.globalHandlers = !1, e.editing = !1, e.originalText = null, e.register = function(t) {
                    e.registered || (e.registered = []);
                    for (var i = e.registered, n = 0; n < i.length; n++)
                        if (i[n] === t) return;
                    i.push(t)
                }, e.unregister = function(t) {
                    var i = e.registered;
                    if (i) {
                        var n = DayPilot.indexOf(i, t);
                        n !== -1 && i.splice(n, 1), 0 === i.length && (i = null)
                    }
                    i || (DayPilot.ue(document, "mousemove", e.gMouseMove), DayPilot.ue(document, "mouseup", e.gMouseUp), DayPilot.ue(document, "touchmove", e.gTouchMove), DayPilot.ue(document, "touchend", e.gTouchEnd), e.globalHandlers = !1)
                }, e.getCellsAbove = function(t) {
                    for (var i = [], n = e.getColumn(t), a = t.parentNode, o = null; a && o !== e.firstSelected;)
                        for (o = a.getElementsByTagName("td")[n], i.push(o), a = a.previousSibling; a && "TR" !== a.tagName;) a = a.previousSibling;
                    return i
                }, e.getCellsBelow = function(t) {
                    for (var i = [], n = e.getColumn(t), a = t.parentNode, o = null; a && o !== e.firstSelected;)
                        for (o = a.getElementsByTagName("td")[n], i.push(o), a = a.nextSibling; a && "TR" !== a.tagName;) a = a.nextSibling;
                    return i
                }, e.getColumn = function(e) {
                    for (var t = 0; e.previousSibling;) e = e.previousSibling, "TD" === e.tagName && t++;
                    return t
                }, e.gTouchMove = function(t) {
                    if (!DayPilot.Util.isMouseEvent(t) && e.drag) {
                        t.preventDefault();
                        var i = t.touches ? t.touches[0].pageX : t.pageX,
                            n = t.touches ? t.touches[0].pageY : t.pageY,
                            a = {};
                        a.x = i, a.y = n;
                        var o = function() {
                            for (var e = t.touches ? t.touches[0].clientX : t.clientX, i = t.touches ? t.touches[0].clientY : t.clientY, n = document.elementFromPoint(e, i); n && n.parentNode;)
                                if (n = n.parentNode, n.daypilotMainD) return n.calendar;
                            return !1
                        }();
                        if (o) {
                            if (e.gShadow && document.body.removeChild(e.gShadow), e.gShadow = null, o.coords = o.Yb.relativeCoords(t), !e.movingShadow && o.coords) {
                                var r = o.wa(e.drag, !1, e.drag.shadowType);
                                if (r) {
                                    e.movingShadow = r;
                                    var l = DayPilot.Date.today(),
                                        t = {
                                            "value": e.drag.id,
                                            "start": l,
                                            "end": l.addSeconds(e.drag.duration),
                                            "text": e.drag.text
                                        },
                                        s = new DayPilot.Event(t, o);
                                    s.external = !0, DayPilot.Global.moving = {}, DayPilot.Global.moving.event = s, DayPilot.Global.moving.helper = {}
                                }
                            }
                            DayPilot.Global.moving && o.Yb.updateMoving()
                        } else {
                            DayPilot.de(e.movingShadow), DayPilot.Global.moving = null, e.movingShadow = null, e.gShadow || (e.gShadow = e.createGShadow(e.drag.shadowType));
                            var r = e.gShadow;
                            r.style.left = a.x + "px", r.style.top = a.y + "px"
                        }
                    }
                }, e.gTouchEnd = function(t) {
                    DayPilot.Util.isMouseEvent(t) || e.gMouseUp(t)
                }, e.gMouseMove = function(t) {
                    if ("undefined" != typeof e && !(t.insideMainD || t.srcElement && t.srcElement.inside)) {
                        var i = DayPilot.mc(t);
                        if (e.drag) {
                            document.body.style.cursor = "move", e.gShadow || (e.gShadow = e.createGShadow(e.drag.shadowType));
                            var n = e.gShadow;
                            n.style.left = i.x + "px", n.style.top = i.y + "px", DayPilot.Global.moving = null, DayPilot.de(e.movingShadow), e.movingShadow = null
                        }
                        for (var a = 0; a < e.registered.length; a++) e.registered[a].U && e.registered[a].U()
                    }
                }, e.gUnload = function(t) {
                    if (e.registered)
                        for (var i = e.registered, n = 0; n < i.length; n++) {
                            var a = i[n];
                            e.unregister(a)
                        }
                }, e.gMouseUp = function(t) {
                    var t = t || window.event;
                    if (DayPilot.Global.resizing) {
                        if (!e.resizingShadow) return DayPilot.Global.resizing.style.cursor = "default", DayPilot.Global.resizing.event.calendar.nav.top.style.cursor = "auto", void(DayPilot.Global.resizing = null);
                        var i = DayPilot.Global.resizing.event,
                            n = DayPilot.Global.resizing.dpBorder,
                            a = e.resizingShadow.clientHeight,
                            o = e.resizingShadow.offsetTop;
                        if (DayPilot.de(e.resizingShadow), e.resizingShadow = null, DayPilot.Global.resizing.style.cursor = "default", i.calendar.nav.top.style.cursor = "auto", DayPilot.Global.resizing.onclick = null, DayPilot.Global.resizing = null, i.calendar.overlap) return;
                        i.calendar.Ua(i, a, o, n)
                    } else if (DayPilot.Global.moving) {
                        if (!DayPilot.Global.moving.helper) return DayPilot.de(e.movingShadow), DayPilot.Global.moving = null, void(e.movingShadow = null);
                        if (!e.movingShadow) return DayPilot.Global.moving.event.calendar.nav.top.style.cursor = "auto", void(DayPilot.Global.moving = null);
                        var o = (DayPilot.Global.moving.helper.oldColumn, e.movingShadow.offsetTop);
                        DayPilot.de(e.movingShadow);
                        var i = DayPilot.Global.moving.event,
                            r = e.movingShadow.column,
                            l = e.drag;
                        if (DayPilot.Global.moving.event.calendar.nav.top.style.cursor = "auto", DayPilot.Global.moving = null, e.movingShadow = null, l && (i.calendar.todo || (i.calendar.todo = {}), i.calendar.todo.del = l.element), i.calendar.overlap) return;
                        var s = t || window.event;
                        i.calendar.Xa(i, r, o, s, l)
                    } else if (e.selecting) {
                        var d = function() {
                            var t = e.selecting,
                                i = e.selecting.calendar;
                            if (e.firstMousePos = null, e.selecting = null, !t.args.allowed) return void i.clearSelection();
                            var n = i.getSelection();
                            i.cb(n.start, n.end, n.resource)
                        };
                        e.selecting && null !== e.topSelectedCell ? d() : e.selectedTimeout = setTimeout(d, 100)
                    } else if (e.firstMousePos) {
                        var c = e.firstMousePos.calendar;
                        if (e.selecting = {}, e.selecting.calendar = c, c.lb(), !e.selecting.args.allowed) return;
                        e.firstMousePos = null, e.selecting = null;
                        var h = c.getSelection();
                        c.cb(h.start, h.end, h.resource)
                    }
                    e.drag && (e.drag = null, document.body.style.cursor = ""), e.gShadow && (document.body.removeChild(e.gShadow), e.gShadow = null), e.moveOffsetY = null
                }, e.dragStart = function(t, i, n, a, o) {
                    DayPilot.us(t);
                    var r = e.drag = {};
                    return r.element = t, r.duration = i, r.text = a, r.id = n, r.shadowType = o ? o : "Fill", r.data = {
                        "id": n,
                        "text": a,
                        "duration": i,
                        "externalHtml": a
                    }, !1
                }, DayPilot.Calendar.makeDraggable = function(t) {
                    function i() {
                        var i = e.drag = {};
                        i.element = a, i.id = t.id, i.duration = t.duration || 60, i.text = t.text || "", i.data = t, i.shadowType = "Fill"
                    }
                    var n = t.element,
                        a = t.keepElement ? null : n;
                    t.duration || 1;
                    navigator.msPointerEnabled && (n.style.msTouchAction = "none", n.style.touchAction = "none");
                    var o = function(e) {
                            i();
                            var t = e.target || e.srcElement;
                            if (t.tagName) {
                                var n = t.tagName.toLowerCase();
                                if ("textarea" === n || "select" === n || "input" === n) return !1
                            }
                            return e.preventDefault && e.preventDefault(), !1
                        },
                        r = function(t) {
                            if (!DayPilot.Util.isMouseEvent(t)) {
                                window.setTimeout(function() {
                                    i(), e.gTouchMove(t), t.preventDefault()
                                }, 0), t.preventDefault()
                            }
                        };
                    DayPilot.us(n), DayPilot.re(n, "mousedown", o), DayPilot.re(n, DayPilot.touch.start, r), n.cancelDraggable = function() {
                        DayPilot.ue(n, "mousedown", o), DayPilot.ue(n, DayPilot.touch.start, r), delete n.cancelDraggable
                    }
                }, e.createGShadow = function(e) {
                    var t = document.createElement("div");
                    return t.setAttribute("unselectable", "on"), t.style.position = "absolute", t.style.width = "100px", t.style.height = "20px", t.style.border = "2px dotted #666666", t.style.zIndex = 101, t.style.pointerEvents = "none", "Fill" === e && (t.style.backgroundColor = "#aaaaaa", t.style.opacity = .5, t.style.filter = "alpha(opacity=50)", t.style.border = "2px solid #aaaaaa"), document.body.appendChild(t), t
                }, e.moveShadow = function(t) {
                    var i = e.movingShadow;
                    i.parentNode && i.parentNode.removeChild(i), t.firstChild.appendChild(i), i.style.left = "0px", i.style.width = e.movingShadow.parentNode.offsetWidth + "px"
                }, e.Column = function(e, t, i) {
                    this.value = e, this.id = e, this.name = t, this.date = new DayPilot.Date(i)
                }, "undefined" != typeof jQuery && ! function(e) {
                    e.fn.daypilotCalendar = function(e) {
                        var t = null,
                            i = this.each(function() {
                                if (!this.daypilot) {
                                    var i = new DayPilot.Calendar(this.id, e);
                                    i.init(), this.daypilot = i, t || (t = i)
                                }
                            });
                        return 1 === this.length ? t : i
                    }
                }(jQuery),
                function() {
                    var e = DayPilot.am();
                    e && e.directive("daypilotCalendar", ["$parse", function(e) {
                        return {
                            "restrict": "E",
                            "template": "<div id='{{id}}'></div>",
                            "compile": function(t, i) {
                                return t.replaceWith(this["template"].replace("{{id}}", i["id"])),
                                    function(t, i, n) {
                                        var a = new DayPilot.Calendar(i[0]);
                                        a.kd.scope = t, a.init();
                                        var o = n["id"];
                                        o && (t[o] = a);
                                        var r = n["publishAs"];
                                        if (r) {
                                            (0, e(r).assign)(t, a)
                                        }
                                        for (var l in n)
                                            if (0 === l.indexOf("on")) {
                                                var s = DayPilot.Util.shouldApply(l);
                                                s ? ! function(i) {
                                                    a[i] = function(a) {
                                                        var o = e(n[i]);
                                                        t["$apply"](function() {
                                                            o(t, {
                                                                "args": a
                                                            })
                                                        })
                                                    }
                                                }(l) : ! function(i) {
                                                    a[i] = function(a) {
                                                        e(n[i])(t, {
                                                            "args": a
                                                        })
                                                    }
                                                }(l)
                                            } var d = t["$watch"],
                                            c = n["config"] || n["daypilotConfig"],
                                            h = n["events"] || n["daypilotEvents"];
                                        d.call(t, c, function(e, t) {
                                            for (var i in e) a[i] = e[i];
                                            a.update()
                                        }, !0), d.call(t, h, function(e) {
                                            a.events.list = e, a.ld({
                                                "eventsOnly": !0
                                            })
                                        }, !0)
                                    }
                            }
                        }
                    }])
                }(), "undefined" != typeof Sys && Sys.Application && Sys.Application.notifyScriptLoaded && Sys.Application.notifyScriptLoaded()
        }
    }(), 
  
    
    "undefined" == typeof DayPilot) var DayPilot = {};

////////////////////////////////////janek2////////////////////////////////////////////