





if ("undefined" == typeof DayPilot.Global && (DayPilot.Global = {}), function() {
        if ("undefined" == typeof DayPilot.Kanban) {
            var e = function() {};
            DayPilot.Kanban = function(t, i) {
                    this.v = "2018.1.3151";
                    var n = this;
                    this.barWidth = 5, this.cardAutoHeight = !0, this.cardHeight = 60, this.cardMarginLeft = 5, this.cardMarginRight = 5, this.cardMarginBottom = 5, this.columnHeaderHeight = 20, this.crosshairColor = "gray", this.height = 300, this.heightSpec = "Auto", this.theme = "kanban_default", this.cellMarginBottom = 4, this.cellMarginTop = 5, this.columnWidthSpec = "Auto", this.columnWidth = 200, this.rowMinHeight = 0, this.swimlaneCollapsingEnabled = !1, this.swimlaneHeaderWidth = 80, this.visible = !0, this.swimlanes = {}, this.swimlanes.list = [], this.columns = {}, this.columns.list = [], this.cards = {}, this.cards.list = [], this.cardDeleteHandling = "Disabled", this.cardMoveHandling = "Update", this.columnMoveHandling = "Disabled", this.swimlaneMoveHandling = "Disabled", this.onBeforeCellRender = null, this.onCardClick = null, this.onCardClicked = null, this.onCardDelete = null, this.onCardDeleted = null, this.onCardMove = null, this.onCardMoved = null, this.onColumnMove = null, this.onColumnMoved = null, this.onHeightChanged = null, this.onSwimlaneMove = null, this.onSwimlaneMoved = null;
                    var a = new DayPilot.Scheduler(t);
                    this.scheduler = a;
                    var o = {},
                        r = 15,
                        l = 15;
                    e();
                    var s = new DayPilot.Date("2000-01-01");
                    o.translate = function() {
                        var e = a;
                        e.startDate = s, e.scale = "Day", e.timeHeaders = [{
                            groupBy: "Cell",
                            format: "d"
                        }], e.heightSpec = n.heightSpec, e.cellWidthSpec = n.columnWidthSpec, e.cellWidth = n.columnWidth, e.cellsMarkBusiness = !1, e.crosshairColor = n.crosshairColor, e.dynamicEventRendering = "Disabled", e.floatingEvents = !1, e.floatingTimeHeaders = !1, e.rowMinHeight = n.rowMinHeight, e.rowMarginTop = n.cellMarginTop, e.rowMarginBottom = n.cellMarginBottom, e.eventMoveToPosition = !0, e.headerHeight = n.columnHeaderHeight, e.eventResizeHandling = "Disabled", e.timeRangeSelectedHandling = "Disabled", e.eventHeight = n.cardHeight, e.eventMarginLeft = n.cardMarginLeft, e.eventMarginRight = n.cardMarginRight, e.eventMarginBottom = n.cardMarginBottom, e.eventTextWrappingEnabled = !0, e.cellStacking = !0, e.cellStackingAutoHeight = n.cardAutoHeight, e.durationBarVisible = !1, e.theme = n.theme, e.visible = n.visible, e.rowHeaderWidth = n.swimlaneHeaderWidth, e.eventDeleteHandling = n.cardDeleteHandling, e.eventMoveHandling = n.cardMoveHandling, e.rowMoveHandling = DayPilot.list(n.swimlanes.list).isEmpty() ? "Disabled" : n.swimlaneMoveHandling, e.onHeightChanged = n.onHeightChanged, e.internal.cssNames.timeheadercol = "_colheadercell", e.internal.cssNames.timeheadercolInner = "_colheadercell_inner", e.internal.cssNames.resourcedivider = "_rowheaderdivider", e.internal.cssNames.event = "_card", e.internal.cssNames.eventInner = "_card_inner", e.internal.cssNames.eventDelete = "_card_delete", e.internal.cssNames.eventMovingSource = "_card_moving_source", n.swimlaneCollapsingEnabled && (e.rowHeaderWidthMarginRight = 15), e.onBeforeTimeHeaderRender = function(e) {
                            var t = n.Ud(e.header.start);
                            if (e.header.html = t.name, e.header.toolTip = t.toolTip || "", e.header.areas = DayPilot.list(t.areas).map(function(e) {
                                    var t = [];
                                    return DayPilot.Util.copyProps(e, t), t
                                }), DayPilot.list(e.header.areas).each(function(e) {
                                    e.target = new DayPilot.Column(t, n)
                                }), "Disabled" !== n.columnMoveHandling) {
                                var i = n.theme + "_columnmove_handle";
                                e.header.areas.push({
                                    "left": 0,
                                    "top": 0,
                                    "width": 20,
                                    "height": 20,
                                    "css": i,
                                    "mousedown": n.Vd,
                                    "action": "None"
                                })
                            }
                        }, e.onBeforeCellRender = function(t) {
                            var i = {};
                            i.cell = {}, i.cell.column = new DayPilot.Column(n.Ud(t.cell.start), n), i.cell.swimlane = null;
                            var a = n.Wd(t.cell.resource);
                            a && (i.cell.swimlane = new DayPilot.Swimlane(a, n)), i.cell.areas = [], i.cell.cssClass = null, i.cell.html = null, i.cell.backImage = null, i.cell.backRepeat = null, i.cell.backColor = null, "function" == typeof n.onBeforeCellRender && (n.onBeforeCellRender(i), DayPilot.Util.copyProps(i.cell, t.cell, ["areas", "cssClass", "html", "backImage", "backRepeat", "backColor"]), DayPilot.list(t.cell.areas).each(function(e) {
                                e.target = {
                                    "swimlane": i.cell.swimlane,
                                    "column": i.cell.column
                                }
                            }));
                            var o = e.rows.find(t.cell.resource);
                            e.resources[o.index].collapsed && (t.cell.cssClass || (t.cell.cssClass = ""), t.cell.cssClass += " " + n.theme + "_collapsed")
                        }, e.onEventClick = function(e) {
                            var t = new DayPilot.Args(e);
                            t.card = new DayPilot.Card(e.e.data.card, n), "function" == typeof n.onCardClick && n.onCardClick(t), e.wrapped = t
                        }, e.onEventClicked = function(e) {
                            var t = e.wrapped;
                            "function" == typeof n.onCardClicked && n.onCardClicked(t)
                        }, e.onEventDelete = function(e) {
                            var t = new DayPilot.Args(e);
                            t.card = new DayPilot.Card(e.e.data.card, n), "function" == typeof n.onCardDelete && n.onCardDelete(t), e.wrapped = t
                        }, e.onEventDeleted = function(e) {
                            DayPilot.rfa(n.cards.list, e.e.data.card);
                            var t = e.wrapped;
                            "function" == typeof n.onCardDeleted && n.onCardDeleted(t)
                        }, e.onEventMove = function(e) {
                            var t = new DayPilot.Args(e);
                            t.card = new DayPilot.Card(e.e.data.card, n), t.column = new DayPilot.Column(n.Ud(e.newStart)), t.position = e.position, "*" !== e.newResource ? t.swimlane = new DayPilot.Swimlane(n.Wd(e.newResource), n) : t.swimlane = null;
                            for (var i = a.cells.find(e.newStart, e.newResource)[0].events(), r = e.position, l = null, s = r - 1; i[s];) {
                                if (i[s].data !== e.e.data) {
                                    l = new DayPilot.Card(i[s]);
                                    break
                                }
                                s -= 1
                            }
                            for (var d = null, c = r; i[c];) {
                                if (i[c].data !== e.e.data) {
                                    d = new DayPilot.Card(i[c]);
                                    break
                                }
                                c += 1
                            }
                            if (t.previous = l, t.next = d, "function" == typeof n.onCardMove && n.onCardMove(t), e.wrapped = t, !e.preventDefault.value) {
                                var h = 0;
                                d ? h = d.wrapped.data.sort[0] : l && (h = l.wrapped.data.sort[0]), e.e.data.sort = [h, (new DayPilot.Date).toString()], DayPilot.rfa(n.cards.list, e.e.data.card);
                                var u = 0;
                                d ? u = DayPilot.indexOf(n.cards.list, d.data) : l && (u = DayPilot.indexOf(n.cards.list, l.data) + 1), n.cards.list.splice(u, 0, e.e.data.card), o.updateSort()
                            }
                        }, e.onEventMoved = function(e) {
                            var t = e.wrapped,
                                i = DayPilot.list(n.cards.list).find(function(t) {
                                    return e.e.data.card === t
                                });
                            i.swimlane = t.swimlane ? t.swimlane.data.id : null, i.column = t.column.data.id, "function" == typeof n.onCardMoved && n.onCardMoved(t)
                        }, e.onBeforeResHeaderRender = function(e) {
                            if (n.swimlaneCollapsingEnabled) {
                                if (!DayPilot.list(n.swimlanes.list).isEmpty()) {
                                    var t = n.theme + "_swimlane_collapse";
                                    e.resource.collapsed && (t = n.theme + "_swimlane_expand"), e.resource.areas = [{
                                        "right": 0,
                                        "top": 0,
                                        "height": l,
                                        "width": r,
                                        "action": "JavaScript",
                                        "css": t,
                                        "js": function() {
                                            n.Xd(e.resource.index)
                                        }
                                    }]
                                }
                            }
                        }, e.onRowMove = function(e) {
                            var t = new DayPilot.Args(e);
                            e.wrapped = t, t.swimlane = new DayPilot.Swimlane(e.source.$.row.swimlane);
                            var i = 0;
                            switch (e.position) {
                                case "before":
                                    i = e.target.index;
                                    break;
                                case "after":
                                    i = e.target.index + 1;
                                    break;
                                default:
                                    throw "Unexpected target swimlane position: " + e.position
                            }
                            t.position = i, t.previous = null;
                            for (var o = i - 1; a.rowlist[o] && a.rowlist[o].swimlane !== t.swimlane.data;) t.previous = new DayPilot.Swimlane(a.rowlist[o].swimlane), o -= 1;
                            t.next = null;
                            for (var r = i; a.rowlist[r] && a.rowlist[r].swimlane !== t.swimlane.data;) t.next = new DayPilot.Swimlane(a.rowlist[r].swimlane), r += 1;
                            "function" == typeof n.onSwimlaneMove && n.onSwimlaneMove(t)
                        }, e.onRowMoved = function(e) {
                            var t = e.wrapped;
                            DayPilot.rfa(n.swimlanes.list, t.swimlane.data);
                            var i = 0;
                            t.next ? i = DayPilot.indexOf(n.swimlanes.list, t.next.data) : t.previous && (i = DayPilot.indexOf(n.swimlanes.list, t.previous.data) + 1), n.swimlanes.list.splice(i, 0, t.swimlane.data), "function" == typeof n.onSwimlaneMoved && n.onSwimlaneMoved(t)
                        }, e.treePreventParentUsage = !0
                    }, o.loadCards = function() {
                        var e = n.theme + "_card_header",
                            t = n.theme + "_card_body",
                            i = (new DayPilot.Date).toString();
                        a.sortDirections = ["asc", "desc"], a.events.list = DayPilot.list(n.cards.list).map(function(a, o) {
                            var r = a.html || a.text || "",
                                l = a.name || "",
                                d = {};
                            return d.id = a.id, d.text = a.text, d.html = "<div class='" + e + "'>" + l + "</div><div class='" + t + "'>" + r + "</div>", d.resource = a.swimlane, d.start = s.addDays(DayPilot.list(n.columns.list).findIndex(function(e) {
                                return a.column === e.id
                            })), d.end = d.start.addDays(1), d.card = a, d.height = a.height, d.cssClass = a.cssClass, d.sort = [o, i], d.areas = DayPilot.list(a.areas).map(function(e) {
                                var t = {};
                                return DayPilot.Util.copyProps(e, t), e.js && (t.js = function(t) {
                                    e.js(new DayPilot.Card(t.data.card, n))
                                }), t
                            }), d.areas.push({
                                "left": 0,
                                "width": 5,
                                "top": 0,
                                "bottom": 0,
                                "backColor": a.barColor,
                                "cssClass": n.theme + "_card_bar"
                            }), d
                        })
                    }, o.updateSort = function() {
                        var e = (new DayPilot.Date).toString();
                        DayPilot.list(n.cards.list).each(function(t, i) {
                            var n = o.findSchedulerEventData(t);
                            n && (n.sort = [i, e])
                        })
                    }, o.findSchedulerEventData = function(e) {
                        return DayPilot.list(a.events.list).find(function(t) {
                            return t.card === e
                        })
                    }, o.loadSwimlanes = function() {
                        var e = DayPilot.list(n.swimlanes.list);
                        a.resources = e.map(function(e) {
                            var t = {};
                            return t.name = e.name, t.id = e.id, t.swimlane = e, e.collapsed && (t.collapsed = !0, t.eventHeight = 20, t.hideEvents = !0), t
                        }), e.isEmpty() && (a.resources = [{
                            name: "",
                            id: "*"
                        }])
                    }, o.loadColumns = function() {
                        var e = DayPilot.list(n.columns.list);
                        a.days = e.length
                    };
                    var d = {};
                    d.active = null, d.div = null, d.update = function() {
                        d.clear();
                        var e = d.findPosition();
                        d.draw(e)
                    }, d.clear = function() {
                        DayPilot.de(d.div), d.div = null
                    }, d.draw = function(e) {
                        var t = 0;
                        if (e >= a.itline.length) {
                            var i = a.itline[a.itline.length - 1];
                            t = i.left + i.width - 3
                        } else t = a.itline[e].left;
                        var o = document.createElement("div");
                        o.style.position = "absolute", o.style.left = t + "px", o.style.width = "3px", o.style.top = 0, o.style.height = a.headerHeight + "px", o.className = n.theme + "_columnmove_position", d.div = o, a.nav.timeHeader.appendChild(o)
                    }, d.findPosition = function() {
                        if (n.coords.x < 0) return 0;
                        var e = DayPilot.list(a.itline).findIndex(function(e) {
                            var t = e.left + a.rowHeaderWidth,
                                i = e.width;
                            return n.coords.x < t + i / 2
                        });
                        return e === -1 ? a.itline.length : e
                    }, this.Vd = function(e) {
                        d.active = e
                    }, this.Ud = function(e) {
                        return n.columns.list[DayPilot.DateUtil.daysDiff(s, e)]
                    }, this.Wd = function(e) {
                        return DayPilot.list(n.swimlanes.list).find(function(t) {
                            return t.id === e
                        })
                    }, this.message = function(e, t, i, n) {
                        a.message(e, t, i, n)
                    }, this.show = function() {
                        n.visible = !0, a.show()
                    }, this.hide = function() {
                        n.visible = !1, a.hide()
                    }, this.cards.add = function(e) {
                        var t = null;
                        t = e && e instanceof DayPilot.Card ? e.data : e, e && (n.cards.list.push(t), n.update())
                    }, this.cards.remove = function(e) {
                        var t = null;
                        t = e && e instanceof DayPilot.Card ? e.data : e, e && (DayPilot.rfa(n.cards.list, e), n.update())
                    }, this.cards.update = function(e) {
                        n.update()
                    }, this.Xd = function(e) {
                        DayPilot.list(n.swimlanes.list).isEmpty() || (n.swimlanes.list[e].collapsed = !n.swimlanes.list[e].collapsed), n.update()
                    }, this.init = function() {
                        return o.translate(), o.loadSwimlanes(), o.loadColumns(), o.loadCards(), a.init(), n.Zc(), this.A = !0, this
                    }, this.Kd = !1, this.dispose = function() {
                        a.dispose(), n.Kd = !0
                    }, this.update = function() {
                        if (n.Kd) throw new DayPilot.Exception("This DayPilot.Gantt instance has been disposed.");
                        o.translate(), o.loadSwimlanes(), o.loadColumns(), o.loadCards(), a.update()
                    }, this.Zc = function() {
                        DayPilot.re(document, "mouseup", n.Yd), DayPilot.re(document, "mousemove", n.Zd)
                    }, this.kd = {}, this.kd.scope = null, this.kd.notify = function() {
                        n.kd.scope && n.kd.scope["$apply"]()
                    }, this.zd = null, this.Ad = function(e) {
                        var t = {
                            "cards": {
                                "preInit": function() {
                                    var e = this.data;
                                    e && (DayPilot.isArray(e.list) ? n.cards.list = e.list : n.cards.list = e)
                                }
                            },
                            "columns": {
                                "preInit": function() {
                                    var e = this.data;
                                    e && (DayPilot.isArray(e.list) ? n.columns.list = e.list : n.columns.list = e)
                                }
                            },
                            "swimlanes": {
                                "preInit": function() {
                                    var e = this.data;
                                    e && (DayPilot.isArray(e.list) ? n.swimlanes.list = e.list : n.swimlanes.list = e)
                                }
                            }
                        };
                        this.zd = t, n.columns.list = [], n.swimlanes.list = [], n.cards.list = [];
                        for (var i in e)
                            if (t[i]) {
                                var a = t[i];
                                a.data = e[i], a.preInit && a.preInit()
                            } else n[i] = e[i]
                    }, this.yd = function() {
                        var e = this.zd;
                        for (var t in e) {
                            var i = e[t];
                            i.postInit && i.postInit()
                        }
                    }, this.internal = {}, this.internal.loadOptions = n.Ad, this.Ad(i), this.Vc = function() {
                        var e = dp.nav.top;
                        return !!e && (e.offsetWidth > 0 && e.offsetHeight > 0)
                    }, this.Wc = function() {
                        var e = n,
                            t = e.Vc;
                        t() || e.Xc || (e.Xc = setInterval(function() {
                            t() && (e.update(), clearInterval(e.Xc))
                        }, 100))
                    }, this.Zd = function(e) {
                        n.coords = DayPilot.mo3(a.nav.top, e), d.active && d.update()
                    }, this.Yd = function(e) {
                        if (d.active) {
                            var t = d.active.source,
                                i = d.findPosition(),
                                a = n.Ud(t.start),
                                o = new DayPilot.Args;
                            o.column = new DayPilot.Column(a, n), o.position = i, o.previous = null, o.next = null;
                            for (var r = null, l = o.position; 0 !== l && !r;) a !== n.columns.list[l - 1] && (r = n.columns.list[l - 1]), l -= 1;
                            r && (o.previous = new DayPilot.Column(r, n));
                            var s = null;
                            for (l = o.position; l < n.columns.list.length - 1 && !s;) a !== n.columns.list[l] && (s = n.columns.list[l]), l += 1;
                            if (s && (o.next = new DayPilot.Column(s, n)), "function" == typeof n.onColumnMove && n.onColumnMove(o), !o.preventDefault.value) {
                                if ("Update" === n.columnMoveHandling) {
                                    DayPilot.rfa(n.columns.list, o.column.data);
                                    var c = 0;
                                    o.next ? c = DayPilot.indexOf(n.columns.list, o.next.data) : o.previous && (c = DayPilot.indexOf(n.columns.list, o.previous.data) + 1), n.columns.list.splice(c, 0, o.column.data), n.update()
                                }
                                "function" == typeof n.onColumnMoved && n.onColumnMove(o)
                            }
                            d.active = null, d.clear()
                        }
                    }
                }, DayPilot.Card = function(e, t) {
                    if (e instanceof DayPilot.Event) {
                        var i = e;
                        this.data = i.data.card, this.wrapped = e
                    } else this.data = e;
                    this.control = t
                }, DayPilot.Column = function(e, t) {
                    this.data = e, this.control = t
                }, DayPilot.Swimlane = function(e, t) {
                    this.data = e, this.control = t
                },
                function() {
                    var e = DayPilot.am();
                    e && e.directive("daypilotKanban", ["$parse", function(e) {
                        return {
                            "restrict": "E",
                            "template": "<div id='{{id}}'></div>",
                            "compile": function(t, i) {
                                return t.replaceWith(this["template"].replace("{{id}}", i["id"])),
                                    function(t, i, n) {
                                        var a = new DayPilot.Kanban(i[0]);
                                        a.kd.scope = t, a.init();
                                        var o = n["id"];
                                        o && (t[o] = a);
                                        var r = n["publishAs"];
                                        if (r) {
                                            (0, e(r).assign)(t, a)
                                        }
                                        for (var l in n)
                                            if (0 === l.indexOf("on")) {
                                                var s = DayPilot.Util.shouldApply(l);
                                                s ? ! function(i) {
                                                    a[i] = function(a) {
                                                        var o = e(n[i]);
                                                        t["$apply"](function() {
                                                            o(t, {
                                                                "args": a
                                                            })
                                                        })
                                                    }
                                                }(l) : ! function(i) {
                                                    a[i] = function(a) {
                                                        e(n[i])(t, {
                                                            "args": a
                                                        })
                                                    }
                                                }(l)
                                            } var d = t["$watch"],
                                            c = n["config"] || n["daypilotConfig"];
                                        d.call(t, c, function(e, t) {
                                            a.Ad(e), a.update()
                                        }, !0)
                                    }
                            }
                        }
                    }])
                }()
        }
    }(), 
"undefined" == typeof DayPilot) var DayPilot = {};


////////////////////////////janek4//////////////////////


