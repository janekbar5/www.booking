<aside class="main-sidebar sidebar-dark-primary elevation-4">
  <!-- Brand Logo -->
  <a href="index3.html" class="brand-link">
    <img src="https://adminlte.io/themes/dev/AdminLTE/dist/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
      style="opacity: .8">
    <span class="brand-text font-weight-light">AdminLTE 3</span>
  </a>

  <!-- Sidebar -->
  <div class="sidebar">
    <!-- Sidebar user panel (optional) -->
    <div class="user-panel mt-3 pb-3 mb-3 d-flex">
      <div class="image">
        <img src="https://adminlte.io/themes/dev/AdminLTE/dist/img/user2-160x160.jpg" class="img-circle elevation-2"
          alt="User Image">
      </div>
      <div class="info">
        <a href="#" class="d-block">Alexander Pierce</a>
      </div>
    </div>

    <!-- Sidebar Menu -->
    <nav class="mt-2">
      <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="true">
       
        <!-- <li class="nav-item">
          <router-link to="/examplecomponent" class="nav-link">
            <i class="nav-icon fas fa-user blue"></i>
            <p> Example Component</p>
          </router-link>
        </li>
        <li class="nav-item">
          <router-link to="/examplecomponent2" class="nav-link">
            <i class="fa fa-list" aria-hidden="true"></i>
            <p> Example Component 2</p>
          </router-link>
        </li> -->

        <li class="nav-item">
          <router-link to="/dashboard" class="nav-link">
            <i class="nav-icon fas fa-tachometer-alt orange"></i> <p>Dashboard </p>
          </router-link>
        </li>

        <li class="nav-item has-treeview">
          <a href="#" class="nav-link"><i class="nav-icon fa fa-calendar orange"></i><p>Reservations<i class="right fas fa-angle-left"></i></p> </a>
          <ul class="nav nav-treeview">
                <li class="nav-item">
                  <router-link to="/bookingcalendar" class="nav-link sub"><i class="fa fa-calendar" aria-hidden="true"></i>  <p> Calendar </p> </router-link>
                  <router-link to="/bookings" class="nav-link sub"><i class="fa fa-list" aria-hidden="true"></i>  <p> List </p> </router-link>
                </li>
            <!--<li class="nav-item">
              <router-link to="/dashboard4" class="nav-link sub"><i class="fa fa-list" aria-hidden="true"></i>  <p> Bookings List</p>
              </router-link>
            </li>-->
          </ul>
        </li>

        <li class="nav-item has-treeview">
          <a href="#" class="nav-link"><i class="nav-icon fa fa-th-large orange" ></i><p>Groups<i class="right fas fa-angle-left"></i></p> </a>
          <ul class="nav nav-treeview">
              <li class="nav-item">
                  <router-link to="/groups/create" class="nav-link sub"><i class="fa fa-plus" aria-hidden="true"></i> <p>Create</p>
                  </router-link>
                </li>
            <li class="nav-item">
              <router-link to="/groups" class="nav-link sub"><i class="fa fa-list" aria-hidden="true"></i> <p> List</p>
              </router-link>
            </li>        
          </ul>
        </li>

      

        <li class="nav-item has-treeview">
          <a href="#" class="nav-link"><i class="nav-icon fa fa-cubes orange" ></i><p>Seasons<i class="right fas fa-angle-left"></i></p>       
            
          </a>

          <ul class="nav nav-treeview">
              <li class="nav-item">
                  <router-link to="/seasongroups/create" class="nav-link sub"><i class="fa fa-plus" aria-hidden="true"></i>  <p> Create </p>
                  </router-link>
                </li>
                <li class="nav-item">
                  <router-link to="/seasongroups" class="nav-link sub"><i class="fa fa-list" aria-hidden="true"></i>  <p> List </p>
                  </router-link>
                </li>
            
          </ul>
        </li>



        <li class="nav-item has-treeview">
          <a href="#" class="nav-link"><i class="nav-icon fa fa-home orange"></i><p>Properties<i class="fas fa-angle-left right"></i>                           
            </p>
          </a>
          <ul class="nav nav-treeview">
           
            
            <li class="nav-item">
              <router-link to="/properties/create" class="nav-link sub"><i class="fa fa-plus" aria-hidden="true"></i> <p>Create</p>
              </router-link>
            </li>
            <li class="nav-item">
              <router-link to="/properties" class="nav-link sub"><i class="fa fa-list" aria-hidden="true"></i> <p> List</p>
              </router-link>
            </li>             
            
          </ul>
        </li>


        <li class="nav-item has-treeview">
          <a href="#" class="nav-link"><i class="nav-icon fa fa-credit-card orange" aria-hidden="true"></i> <p>Invoices <i class="fas fa-angle-left right"></i>                           
            </p>
          </a>
          <ul class="nav nav-treeview">
            <li class="nav-item">
              <router-link to="/invoices/create" class="nav-link sub">
                <i class="fa fa-plus" aria-hidden="true"></i> <p>Create</p>
              </router-link>
            </li>
            <li class="nav-item">
              <router-link to="/invoices" class="nav-link sub">
                <i class="fa fa-list" aria-hidden="true"></i> <p> List</p>
              </router-link>
            </li>
            
          </ul>
        </li>

        <li class="nav-item has-treeview">
            <a href="#" class="nav-link"><i class="nav-icon fa fa-users orange" aria-hidden="true"></i> <p>Customers <i class="fas fa-angle-left right"></i>                           
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <router-link to="/customers/create" class="nav-link sub">
                  <i class="fa fa-plus" aria-hidden="true"></i> <p>Create</p>
                </router-link>
              </li>
              <li class="nav-item">
                <router-link to="/customers" class="nav-link sub">
                  <i class="fa fa-list" aria-hidden="true"></i> <p>List</p>
                </router-link>
              </li>
              
            </ul>
          </li>

          <li class="nav-item has-treeview">
              <a href="#" class="nav-link"><i class="nav-icon fa fa-cogs orange" aria-hidden="true"></i> <p>Inventory <i class="fas fa-angle-left right"></i>                           
                </p>
              </a>
              <ul class="nav nav-treeview">
                <li class="nav-item">
                  <router-link to="/inventories/create" class="nav-link sub">
                    <i class="fa fa-plus" aria-hidden="true"></i> <p>Create</p>
                  </router-link>
                </li>
                <li class="nav-item">
                  <router-link to="/inventories" class="nav-link sub">
                    <i class="fa fa-list" aria-hidden="true"></i> <p>List</p>
                  </router-link>
                </li>
                
              </ul>
            </li>

            <li class="nav-item has-treeview">
                <a href="#" class="nav-link"><i class="nav-icon fa fa-thumbs-up orange" aria-hidden="true"></i> <p>Advantages <i class="fas fa-angle-left right"></i>                           
                  </p>
                </a>
                <ul class="nav nav-treeview">
                  <li class="nav-item">
                    <router-link to="/invoices/create" class="nav-link sub">
                      <i class="fa fa-plus" aria-hidden="true"></i> <p>Create</p>
                    </router-link>
                  </li>
                  <li class="nav-item">
                    <router-link to="/invoices" class="nav-link sub">
                      <i class="fa fa-list" aria-hidden="true"></i> <p>List</p>
                    </router-link>
                  </li>
                  
                </ul>
              </li>

              <li class="nav-item has-treeview">
                  <a href="#" class="nav-link"><i class="nav-icon fa fa-flag orange" aria-hidden="true"></i> <p>Statuses <i class="fas fa-angle-left right"></i>                           
                    </p>
                  </a>
                  <ul class="nav nav-treeview">
                    <li class="nav-item">
                      <router-link to="/bookingstatuses/create" class="nav-link sub">
                        <i class="fa fa-plus" aria-hidden="true"></i> <p>Create</p>
                      </router-link>
                    </li>
                    <li class="nav-item">
                      <router-link to="/bookingstatuses" class="nav-link sub">
                        <i class="fa fa-list" aria-hidden="true"></i> <p>List</p>
                      </router-link>
                    </li>
                    
                  </ul>
                </li>

                <li class="nav-item has-treeview">
                  <a href="#" class="nav-link"><i class="nav-icon fa fa-user orange" aria-hidden="true"></i> <p> Users <i class="fas fa-angle-left right"></i>                           
                    </p>
                  </a>
                  <ul class="nav nav-treeview">
                    <li class="nav-item">
                      <router-link to="/users/create" class="nav-link sub">
                        <i class="fa fa-plus" aria-hidden="true"></i> <p>Create</p>
                      </router-link>
                    </li>
                    <li class="nav-item">
                      <router-link to="/users" class="nav-link sub">
                        <i class="fa fa-list" aria-hidden="true"></i> <p>List</p>
                      </router-link>
                    </li>
                    
                  </ul>
                </li>

                <li class="nav-item has-treeview">
                  <a href="#" class="nav-link"><i class="nav-icon fa fa-cubes orange" aria-hidden="true"></i> <p> Users Roles <i class="fas fa-angle-left right"></i>                           
                    </p>
                  </a>
                  <ul class="nav nav-treeview">
                    <li class="nav-item">
                      <router-link to="/roles/create" class="nav-link sub">
                        <i class="fa fa-plus" aria-hidden="true"></i> <p>Create</p>
                      </router-link>
                    </li>
                    <li class="nav-item">
                      <router-link to="/roles" class="nav-link sub">
                        <i class="fa fa-list" aria-hidden="true"></i> <p>List</p>
                      </router-link>
                    </li>
                    
                  </ul>
                </li>


                <li class="nav-item has-treeview">
                  <a href="#" class="nav-link"><i class="nav-icon fa fa-cubes orange" aria-hidden="true"></i> <p> Users Roles 2 <i class="fas fa-angle-left right"></i>                           
                    </p>
                  </a>
                  <ul class="nav nav-treeview">
                    <li class="nav-item">
                      <router-link to="/roles2/create" class="nav-link sub">
                        <i class="fa fa-plus" aria-hidden="true"></i> <p>Create</p>
                      </router-link>
                    </li>
                    <li class="nav-item">
                      <router-link to="/roles2" class="nav-link sub">
                        <i class="fa fa-list" aria-hidden="true"></i> <p>List</p>
                      </router-link>
                    </li>
                    
                  </ul>
                </li>
         
        

        



<!-- 
        <li class="nav-item">
          <router-link to="/users-old" class="nav-link">
            <i class="nav-icon fas fa-user blue"></i>
            <p> Users OLD</p>
          </router-link>
        </li>

        <li class="nav-item">
          <router-link to="/usersindex" class="nav-link">
            <i class="nav-icon fas fa-user blue"></i>
            <p> Users Index</p>
          </router-link>
        </li> -->
        
       

        <!-- <li class="nav-item">
          <router-link to="/roles" class="nav-link">
            <i class="nav-icon fas fa-cubes blue"></i>
            <p> Roles</p>
          </router-link>
        </li> -->

        <li class="nav-item">
          <a href="{{ route('logout') }}" class="nav-link" onclick="event.preventDefault();document.getElementById('logout-form').submit();">
            <i class="nav-icon fas fa-power-off"></i>
            <p>
              {{ __('Logout') }}
            </p>
          </a>
          <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            @csrf
          </form>
        </li>




       
        
        

      </ul>
    </nav>
    <!-- /.sidebar-menu -->
  </div>
  <!-- /.sidebar -->
</aside>