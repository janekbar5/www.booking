
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
import Vue from 'vue'
import { Form, HasError, AlertError } from 'vform' 
Vue.component(HasError.name, HasError)
Vue.component(AlertError.name, AlertError)
/////////////////////////////////////////////////////4 vform
window.Form = Form; ///access to form on evry place in app

////////////////////////////////////////////////////////////////////////////// Global event bus
Vue.prototype.$eventHub = new Vue(); 

import moment from 'moment';  //MOMENT JS   
import VueProgressBar from 'vue-progressbar' // vue-progressbar
////////////////////////////////////////////////////////////////////////// sweetalert2
import swal from 'sweetalert2' // sweetalert2
window.swal = swal;
const toast = swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 3000
  });
  window.toast = toast;
//////////////////////////////////////////////// 1 import router
import VueRouter from 'vue-router'
Vue.use(VueRouter)
////////////////////////////////////////////////////// Custom Event
window.Fire =  new Vue();

const Foo = { template: '<div>foo</div>' }

import App from './components/App.vue'

import Dashboard from './views/dashboard/Dashboard.vue'

import BookingScheduler from './views/bookings/BookingScheduler.vue'
import BookingIndex from './views/bookings/BookingIndex.vue'



import InvoicesIndex from './components/invoices/InvoicesIndex'
import InvoicesShow from './components/invoices/InvoicesShow'
import InvoicesEdit from './components/invoices/InvoicesEdit'

// import PropertiesIndex from './components/properties/PropertiesIndex'
// import PropertiesEdit from './components/properties/PropertiesEdit'

import CustomersIndex from './views/customers/CustomersIndex'
import CustomersEdit from './views/customers/CustomersEdit'

//import PropertiesEdit2 from './components/properties/PropertiesEdit2'
import ExampleComponent from './components/ExampleComponent'
import ExampleComponent2 from './components/ExampleComponent2'

import GroupsIndex from './views/groups/GroupsIndex'
import GroupsShow from './views/groups/GroupsShow'
import GroupsEdit from './views/groups/GroupsEdit'

import PropertiesIndex from './views/properties/PropertiesIndex'
import PropertiesShow from './views/properties/PropertiesShow'
import PropertiesEdit from './views/properties/PropertiesEdit'

import SeasonGroupsIndex from './views/seasongroups/SeasonGroupsIndex'
import SeasonGroupsShow from './views/seasongroups/SeasonGroupsShow'
import SeasonGroupsEdit from './views/seasongroups/SeasonGroupsEdit'

import InventoriesIndex from './views/inventories/InventoriesIndex'
import InventoriesShow from './views/inventories/InventoriesShow'
import InventoriesEdit from './views/inventories/InventoriesEdit'

import BookingStatusesIndex from './views/bookingstatuses/BookingStatusesIndex'
import BookingStatusesShow from './views/bookingstatuses/BookingStatusesShow'
import BookingStatusesEdit from './views/bookingstatuses/BookingStatusesEdit'

import UsersIndex from './views/users/UsersIndex'
import UsersShow from './views/users/UsersShow'
import UsersEdit from './views/users/UsersEdit'

import RolesIndex2 from './components/roles/RolesIndex2'
import RolesShow2 from './components/roles/RolesShow2'
import RolesEdit2 from './components/roles/RolesEdit2'

import RolesIndex from './views/roles/RolesIndex'
import RolesShow from './views/roles/RolesShow'
import RolesEdit from './views/roles/RolesEdit'

// import Users from './components/Users.vue'

// import UsersIndex2 from './components/users/UsersIndex2'
// import UsersShow2 from './components/users/UsersShow2'
// import UsersEdit2 from './components/users/UsersEdit2'

//////////////////////////////////////////////// 2 routers
let routes = [
// { path: '/users-old', name: 'Users', component: Users }, 

// { path: '/usersindex', name: 'UsersIndex2', component: UsersIndex2 }, 
// { path: '/usersindex/create', name: 'UsersIndex2', component: UsersEdit2 }, 
// { path: '/usersindex/:id/edit', name: 'UsersEdit2', component: UsersEdit2, meta: {mode: 'edit'} },
// { path: '/usersindex/:id', name: 'UsersShow2', component: UsersShow2 },

{ path: '/users', name: 'UsersIndex', component:UsersIndex, meta: {mode: 'list'} },
{ path: '/users/create', name: 'UsersEdit', component: UsersEdit, meta: {mode: 'create'} },
{ path: '/users/:id/edit', name: 'UsersEdit', component: UsersEdit, meta: {mode: 'edit'} },
{ path: '/users/:id',  name: 'UsersShow', component: UsersShow, meta: {mode: 'view'} },

//{ path: '/app', name: 'App', component: App },
{ path: '/dashboard', name: 'Dashboard', component: Dashboard },
{ path: '/bookingcalendar', name: 'BookingScheduler', component: BookingScheduler },
{ path: '/bookings', name: 'BookingIndex', component: BookingIndex, meta: {mode: 'list'} },


{ path: '/roles2', name: 'RolesIndex2', component: RolesIndex2 }, 
{ path: '/roles2/:id', name: 'RolesShow2', component: RolesShow2 },
{ path: '/roles2/:id/edit', name: 'RolesEdit2', component: RolesEdit2, meta: {mode: 'edit'} },

{ path: '/roles', name: 'RolesIndex', component: RolesIndex, meta: {mode: 'list'} },
{ path: '/roles/create', name: 'RolesEdit', component: RolesEdit, meta: {mode: 'create'} },
{ path: '/roles/:id/edit', name: 'RolesEdit', component: RolesEdit, meta: {mode: 'edit'} },
{ path: '/roles/:id',  name: 'RolesShow', component: RolesShow, meta: {mode: 'view'} },  


{ path: '/invoices', name: 'InvoicesIndex', component: InvoicesIndex },
{ path: '/invoices/create', name: 'InvoicesEdit', component: InvoicesEdit },
{ path: '/invoices/:id/edit', name: 'InvoicesEdit', component: InvoicesEdit, meta: {mode: 'edit'} },
{ path: '/invoices/:id',  name: 'InvoicesShow', component: InvoicesShow },

// { path: '/customers', name: 'CustomersIndex', component: CustomersIndex },
// { path: '/customers/create', name: 'CustomersEdit', component: CustomersEdit },
// { path: '/customers/edit/:id', name: 'CustomersEdit', component: CustomersEdit, meta: {mode: 'edit'} },
// { path: '/customers/:id', name: 'CustomersShow', component: CustomersShow },

{ path: '/customers', name: 'CustomersIndex', component: CustomersIndex, meta: {mode: 'list'} },
{ path: '/customers/create', name: 'CustomersEdit', component: CustomersEdit, meta: {mode: 'create'} },
{ path: '/customers/:id/edit', name: 'CustomersEdit', component: CustomersEdit, meta: {mode: 'edit'} },
//{ path: '/customers/:id',  name: 'CustomersShow', component: CustomersShow, meta: {mode: 'view'} }, 


{ path: '/examplecomponent', name: 'ExampleComponent', component: ExampleComponent }, 
{ path: '/examplecomponent2', name: 'ExampleComponent2', component: ExampleComponent2 },

{ path: '/groups', name: 'GroupsIndex', component: GroupsIndex, meta: {mode: 'list'} },
{ path: '/groups/create', name: 'GroupsEdit', component: GroupsEdit, meta: {mode: 'create'} },
{ path: '/groups/:id/edit', name: 'GroupsEdit', component: GroupsEdit, meta: {mode: 'edit'} },
{ path: '/groups/:id',  name: 'GroupsShow', component: GroupsShow, meta: {mode: 'view'} }, 

{ path: '/properties', name: 'PropertiesIndex', component: PropertiesIndex, meta: {mode: 'list'} },
{ path: '/properties/create', name: 'PropertiesEdit', component: PropertiesEdit, meta: {mode: 'create'} },
{ path: '/properties/:id/edit', name: 'PropertiesEdit', component: PropertiesEdit, meta: {mode: 'edit'} },
{ path: '/properties/:id',  name: 'PropertiesShow', component: PropertiesShow, meta: {mode: 'view'} },   

{ path: '/seasongroups', name: 'SeasonGroupsIndex', component: SeasonGroupsIndex, meta: {mode: 'list'} },
{ path: '/seasongroups/create', name: 'SeasonGroupsEdit', component: SeasonGroupsEdit, meta: {mode: 'create'} },
{ path: '/seasongroups/:id/edit', name: 'SeasonGroupsEdit', component: SeasonGroupsEdit, meta: {mode: 'edit'} },
{ path: '/seasongroups/:id',  name: 'SeasonGroupsShow', component: SeasonGroupsShow, meta: {mode: 'view'} },  

{ path: '/inventories', name: 'InventoriesIndex', component: InventoriesIndex, meta: {mode: 'list'} },
{ path: '/inventories/create', name: 'InventoriesEdit', component: InventoriesEdit, meta: {mode: 'create'} },
{ path: '/inventories/:id/edit', name: 'InventoriesEdit', component: InventoriesEdit, meta: {mode: 'edit'} },
{ path: '/inventories/:id',  name: 'InventoriesShow', component: InventoriesShow, meta: {mode: 'view'} },

{ path: '/bookingstatuses', name: 'BookingStatusesIndex', component: BookingStatusesIndex, meta: {mode: 'list'} },
{ path: '/bookingstatuses/create', name: 'BookingStatusesEdit', component: BookingStatusesEdit, meta: {mode: 'create'} },
{ path: '/bookingstatuses/:id/edit', name: 'BookingStatusesEdit', component: BookingStatusesEdit, meta: {mode: 'edit'} },
{ path: '/bookingstatuses/:id',  name: 'BookingStatusesShow', component: BookingStatusesShow, meta: {mode: 'view'} }, 



]

////////////////////////////////////////////////3 define router
const router = new VueRouter({    
    mode: 'history',
    routes
});



///////////////////////////////////////////////////////5 filters
Vue.filter('upText', function(text){
    //return text.toUpperCase()
    return text.charAt(0).toUpperCase() + text.slice(1)
});
Vue.filter('myDate',function(created){
    return moment(created).format('MMMM Do YYYY');
});
///////////invoices
Vue.filter('formatMoney', (value) => {
    return Number(value)
        .toFixed(2)
        .replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")
})
//////////////////////////////////////////////////////////6 progress bar
const options = {
    color: '#bffaf3',
    failedColor: '#874b4b',
    thickness: '5px',
    transition: {
      speed: '0.2s',
      opacity: '0.6s',
      termination: 300
    },
    autoRevert: true,
    location: 'top',
    inverse: false
  }
Vue.use(VueProgressBar, options)

///////////////////////////////////////////////////////////////////7 passport
/* Vue.component(
    'passport-clients',
     require('../../js/components/passport/Clients.vue')
);

Vue.component(
    'passport-authorized-clients',
    require('../../js/components/passport/AuthorizedClients.vue')
);

Vue.component(
    'passport-personal-access-tokens',
     require('../../js/components/passport/PersonalAccessTokens.vue')
); */

// Vue.component(
//     'passport-clients',
//     require('./components/passport/Clients.vue').default
// );

// Vue.component(
//     'passport-authorized-clients',
//     require('./components/passport/AuthorizedClients.vue').default
// );

// Vue.component(
//     'passport-personal-access-tokens',
//     require('./components/passport/PersonalAccessTokens.vue').default
// );

//Vue.component('example-component', require('./components/ExampleComponent.vue'));

/*new Vue({
  render: h => h(App)
}).$mount('#app')*/

const app = new Vue({
    el: '#app',
    router,
    //////
     
    /////
    
  

});


