<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

// http://www.booking.test/getPricesByGroupId

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/getPricesByGroupId', 'BookingController@getPricesByGroupId')->name('home');



/////////////////////////////////////////////////////////////////////////////////////API

/* Route::post('/api/user/1', 'PropertyController@updateTest');
Route::get('v1/api/invoices', 'InvoiceController@index');
Route::resource('/api/invoices', 'InvoiceController');
Route::get('/api/products', 'ProductController@search');
Route::get('/api/customers', 'CustomerController@search'); */

   

 Route::group(['middleware' => ['auth']], function() {
     /////////////////////////////////////////////////////////////////////////////////////////////////BOOKING
     Route::get('v1/api/bookings/index', 'BookingController@index'); //list   
     Route::get('v1/api/bookings/{id}/edit', 'BookingController@edit'); 
    Route::post('v1/api/bookings/store', 'BookingController@store');  //new
    Route::post('v1/api/bookings/update', 'BookingController@update'); //update
    //////////
    Route::post('v1/api/bookings/domoveorresize',  'BookingController@doMoveorResize');
    Route::post('v1/api/bookings/doDeleteBooking', 'BookingController@doDeleteBooking');
    Route::post('v1/api/bookings/getSeasonPrices', 'BookingController@getSeasonPrices');
    //Route::post('v1/api/bookings/delete/{id}', 'BookingController@destroy'); //update  post    
     Route::get('v1/api/bookings/getEventsResources', 'BookingController@getEventsResources');   
  Route::delete('v1/api/bookings/delete/{id}', 'BookingController@destroy'); //update  post 
    //////////////////////////////////////////////////////////////////////////////////////////////////PROPERTY
     Route::get('v1/api/properties/index', 'PropertyController@index'); //list    
     Route::get('v1/api/properties/{id}/edit', 'PropertyController@edit'); //edit
     Route::get('v1/api/properties/create', 'PropertyController@create'); //create     
    Route::post('v1/api/properties/create', 'PropertyController@store'); //create post
    Route::post('v1/api/properties/update/{id}', 'PropertyController@update'); //update  post
     Route::put('v1/api/properties/update/{id}', 'PropertyController@update'); //update  post 
  Route::delete('v1/api/properties/delete/{id}', 'PropertyController@destroy'); //update  post
    ///////////////////////////////////////////////////////////////////////////////////////////////////CUSTOMERS  
     Route::get('v1/api/customers/search', 'CustomerController@search'); //list 
	 Route::get('v1/api/customers/index', 'CustomerController@index'); //list 
     Route::get('v1/api/customers/{id}/edit', 'CustomerController@edit'); //edit
     Route::get('v1/api/customers/create', 'CustomerController@create'); //create     
     Route::post('v1/api/customers/create', 'CustomerController@store'); //create post
     Route::post('v1/api/customers/update/{id}', 'CustomerController@update'); //update  post
      Route::put('v1/api/customers/update/{id}', 'CustomerController@update'); //update  post 
   Route::delete('v1/api/customers/delete/{id}', 'CustomerController@destroy'); //update  post
     ////////////////////////////////////////////////////////////////////////////////////////////////////GROUP
    //  Route::get('v1/api/groups/index', 'GroupController@index'); //list
    //  Route::get('v1/api/groups/edit/{id}', 'GroupController@edit'); //edit
    // Route::put('v1/api/groups/update/{id}', 'GroupController@update'); //update  post     
    Route::get('v1/api/groups/index', 'GroupController@index'); //list
    Route::get('v1/api/groups/{id}/edit', 'GroupController@edit'); //edit
    Route::get('v1/api/groups/create', 'GroupController@create'); //create     
    Route::post('v1/api/groups/create', 'GroupController@store'); //create post
    Route::post('v1/api/groups/update/{id}', 'GroupController@update'); //update  post
     Route::put('v1/api/groups/update/{id}', 'GroupController@update'); //update  post 
  Route::delete('v1/api/groups/delete/{id}', 'GroupController@destroy'); //update  post

    /////////////////////////////////////////////////////////////////////////////////////////////////////SEASON GROUPS
     Route::get('v1/api/seasongroups/index', 'SeasonGroupController@index'); //list
     Route::get('v1/api/seasongroups/{id}/edit', 'SeasonGroupController@edit'); //edit
     Route::get('v1/api/seasongroups/create', 'SeasonGroupController@create'); //create     
     Route::post('v1/api/seasongroups/create', 'SeasonGroupController@store'); //create post
     Route::post('v1/api/seasongroups/update/{id}', 'SeasonGroupController@update'); //update  post
      Route::put('v1/api/seasongroups/update/{id}', 'SeasonGroupController@update'); //update  post 
   Route::delete('v1/api/seasongroups/delete/{id}', 'SeasonGroupController@destroy'); //update  post
   //////////////////////////////////////////////////////////////////////////////////////////////////////SEASONS
   
      Route::get('v1/api/season/getSeasonByGroupId/{id}', 'SeasonController@getSeasonByGroupId'); //list
      Route::get('v1/api/season/index', 'SeasonController@index'); //list
      Route::get('v1/api/season/edit/{id}', 'SeasonController@edit'); //edit

      Route::post('v1/api/season/create', 'SeasonController@store'); //create post
      Route::post('v1/api/season/update/{id}', 'SeasonController@update'); //update  post
      Route::post('v1/api/season/domoveorresize',  'SeasonController@doMoveorResize');

     ////////////////////////////////////////////////////////////////////////////////////////////////////IMAGES 
     //Route::get('/images/photoslist/{id}', 'ImageController@photosList');
    Route::post('/v1/api/images/index', 'ImageController@index');
  Route::delete('/v1/api/images/delete/{id}', 'ImageController@destroy');
    Route::post('/images/post', 'ImageController@post');
    ////////////////////////////////////////////////////////////////////////////////////////////////////INVENTORIES
    Route::get('v1/api/inventories/index', 'InventoryController@index'); //list    
    Route::get('v1/api/inventories/{id}/edit', 'InventoryController@edit'); //edit
    Route::get('v1/api/inventories/create', 'InventoryController@create'); //create     
   Route::post('v1/api/inventories/create', 'InventoryController@store'); //create post
   Route::post('v1/api/inventories/update/{id}', 'InventoryController@update'); //update  post
    Route::put('v1/api/inventories/update/{id}', 'InventoryController@update'); //update  post 
 Route::delete('v1/api/inventories/delete/{id}', 'InventoryController@destroy'); //update  post
    /////////////////////////////////////////////////////////////////////////////////////////////////////STATUSES
    Route::get('v1/api/bookingstatuses/index', 'BookingStatusController@index'); //list
    Route::get('v1/api/bookingstatuses/{id}/edit', 'BookingStatusController@edit'); //edit
    Route::get('v1/api/bookingstatuses/create', 'BookingStatusController@create'); //create     
    Route::post('v1/api/bookingstatuses/create', 'BookingStatusController@store'); //create post
    Route::post('v1/api/bookingstatuses/update/{id}', 'BookingStatusController@update'); //update  post
     Route::put('v1/api/bookingstatuses/update/{id}', 'BookingStatusController@update'); //update  post 
  Route::delete('v1/api/bookingstatuses/delete/{id}', 'BookingStatusController@destroy'); //update  post
    /////////////////////////////////////////////////////////////////////////////////////////////////////PAYMENTS
    Route::get('v1/api/bookingpayments/index', 'BookingPaymentController@index'); //list 
  /////////////////////////////////////////////////////////////////////////////////////////////////////USERS
    Route::get('user/index', 'UserController@index');
    Route::get('v1/api/users/index', 'UserController@index');
    Route::get('v1/api/users/{id}/edit', 'UserController@edit'); //edit

    Route::get('v1/api/users/create', 'UserController@create'); //create     
    Route::post('v1/api/users/create', 'UserController@store'); //create post
    Route::post('v1/api/users/update/{id}', 'UserController@update'); //update  post
     Route::put('v1/api/users/update/{id}', 'UserController@update'); //update  post 
  Route::delete('v1/api/users/delete/{id}', 'UserController@destroy'); //update  post
/////////////////////////////////////////////////////////////////////////////////////////////////////ROLES
     Route::get('v1/api/roles/index', 'RoleController@index'); //list    
     Route::get('v1/api/roles/{id}/edit', 'RoleController@edit'); //edit
     Route::get('v1/api/roles/create', 'RoleController@create'); //create     
    Route::post('v1/api/roles/create', 'RoleController@store'); //create post
    Route::post('v1/api/roles/update/{id}', 'RoleController@update'); //update  post
     Route::put('v1/api/roles/update/{id}', 'RoleController@update'); //update  post 
  Route::delete('v1/api/roles/delete/{id}', 'RoleController@destroy'); //update  post

  Route::get('/roles/index', 'RoleController@index');
  Route::get('/roles/index/{id}', 'RoleController@show');
  Route::get('/roles/index/{id}/edit', 'RoleController@edit');
  Route::put('/roles/update/{id}', 'RoleController@update');

});







//

//



Route::get('{path}','HomeController@index')->where( 'path', '([A-z\d-/_.]+)?' ); //match any route in VUE
//Route::get('{path}','UserController@index')->where( 'path', '([A-z\d-/_.]+)?' ); //match any route in VUE

